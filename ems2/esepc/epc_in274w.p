/****************************************************************************************************************************/
/* Criada em : 23/04/2023 - epc_in274w.p - Grava tabela de log de altera da tabela ordem-compra                            **/
/*             09/05/2023 - inserido rotian para gravar ordem e compra que nasceu de uma copia , onde o campo              **/
/**                         substring(char-2,190,10) grava o numero da ordem que foi copiada                               **/
/****************************************************************************************************************************/

/* \\srvtotvs12\dts-12\ERP\especifico\fastplas inserir no propath */

DEF PARAMETER BUFFER    bf_ordem-compra_new     FOR     ordem-compra. 
DEF PARAMETER BUFFER    bf_ordem-compra_old     FOR     ordem-compra. 

def new Global shared var c-seg-usuario         as Character        format "x(12)"  no-undo.

DEF VAR i-hora              AS  int.
DEF VAR c-result-compare    AS  CHAR    INITIAL ''.
DEF VAR c-situacao          as  char    FORMAT 'x(14)' EXTENT 6  INITIAL ['Nao Confirmada','Confirmada','Cotada','Eliminada','Em Cotacao','Terminada'].      
DEF VAR c-cod-icms          as  char    FORMAT 'x(16)' EXTENT 2  INITIAL ['Consumo','Industrializacao'].
DEF VAR c-natureza          as  char    FORMAT 'x(14)' EXTENT 3  INITIAL ['Compra','Servicos','Beneficiamento'].
def VAR c-origem            as  char    FORMAT 'x(11)' EXTENT 3  INITIAL ['Manual','Dependente','Idependente'].
DEF VAR c-campo             AS CHAR     FORMAT 'X(50)' .
DEF VAR c-campo-de          AS CHAR     FORMAT 'X(50)' .
DEF VAR c-campo-para        AS CHAR     FORMAT 'X(50)' .

DEF TEMP-TABLE tt-ordem-compra_old LIKE ordem-compra.

ASSIGN i-hora = TIME.


IF NOT NEW (bf_ordem-compra_new) THEN
    DO:
            
            IF  bf_ordem-compra_old.it-codigo             <> bf_ordem-compra_new.it-codigo          THEN 
                DO:
                        ASSIGN  c-campo  =   'ITEM'
                                c-campo-de      =   bf_ordem-compra_old.it-codigo
                                c-campo-para    =   bf_ordem-compra_new.it-codigo.

                         RUN grava-registro.
                            
                END.

            if  bf_ordem-compra_old.cod-estabel           <> bf_ordem-compra_new.cod-estabel        THEN      
                DO:

                        ASSIGN  c-campo  =   'ESTABELECIMENTO'
                                c-campo-de      =   bf_ordem-compra_old.cod-estabel
                                c-campo-para    =   bf_ordem-compra_new.cod-estabel.
                        
                        RUN grava-registro.
                      
                END.
            IF      bf_ordem-compra_old.nr-processo           <> bf_ordem-compra_new.nr-processo        THEN 
                DO:
                        
                        ASSIGN  c-campo  =   'NR PROCESSO'
                                c-campo-de      =   string(bf_ordem-compra_old.nr-processo)
                                c-campo-para    =   string(bf_ordem-compra_new.nr-processo).

                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.data-emissao          <> bf_ordem-compra_new.data-emissao       THEN   
                DO:
                        ASSIGN  c-campo  =   'DT EMISSAO'
                                c-campo-de      =   string(bf_ordem-compra_old.data-emissao, '99/99/9999')
                                c-campo-para    =   string(bf_ordem-compra_new.data-emissao, '99/99/9999').

                        RUN grava-registro.
                        
                END.
            IF      bf_ordem-compra_old.nr-contrato           <> bf_ordem-compra_new.nr-contrato        THEN
                DO:
                        ASSIGN  c-campo  =   'NR CONTRATO'
                                c-campo-de      =   string(bf_ordem-compra_old.nr-contrato)
                                c-campo-para    =   string(bf_ordem-compra_new.nr-contrato).

                        RUN grava-registro.
                        
                END.
            IF      bf_ordem-compra_old.num-seq-item          <> bf_ordem-compra_new.num-seq-item       THEN 
                DO:
                        ASSIGN  c-campo  =   'SEQ ITEM'
                                c-campo-de     =    string(bf_ordem-compra_old.num-seq-item) 
                                c-campo-para    =   string(bf_ordem-compra_new.num-seq-item) .

                        RUN grava-registro.
                       
                END.
            IF      bf_ordem-compra_old.qt-solic              <> bf_ordem-compra_new.qt-solic           THEN      
                DO:
                        ASSIGN  c-campo  =   'QTDE SOLIC'
                                c-campo-de      =   string(bf_ordem-compra_old.qt-solic, '>>>>>.9999')
                                c-campo-para    =   string(bf_ordem-compra_new.qt-solic, '>>>>>.9999').

                        RUN grava-registro.
                        
                END.
            IF      bf_ordem-compra_old.situacao              <> bf_ordem-compra_new.situacao           THEN   
                DO:
                        ASSIGN  c-campo  =   'SITUACAO'
                                c-campo-de      =   c-situacao[INT(bf_ordem-compra_old.situacao)]
                                c-campo-para    =   c-situacao[INT(bf_ordem-compra_new.situacao)].
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.ordem-emitida         <> bf_ordem-compra_new.ordem-emitida      THEN  
                DO:
                        ASSIGN  c-campo  =   'NR PROCESSO'
                                c-campo-de      =   string(bf_ordem-compra_old.ordem-emitida)
                                c-campo-para    =   string(bf_ordem-compra_new.ordem-emitida).
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.requisitante          <> bf_ordem-compra_new.requisitante       THEN     
                DO:
                        ASSIGN  c-campo  =   'REQUISITANTE'
                                c-campo-de      =   bf_ordem-compra_old.requisitante
                                c-campo-para    =   bf_ordem-compra_new.requisitante.
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.cod-comprado          <> bf_ordem-compra_new.cod-comprado       THEN   
                DO:
                        ASSIGN  c-campo  =   'COMPRADOR'    
                                c-campo-de      =   bf_ordem-compra_old.cod-comprado
                                c-campo-para    =   bf_ordem-compra_new.cod-comprado.
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.num-ord-inv           <> bf_ordem-compra_new.num-ord-inv        THEN    
                DO:
                        ASSIGN  c-campo  =   'NIVEL'
                                c-campo-de      =   string(bf_ordem-compra_old.num-ord-inv)
                                c-campo-para    =   string(bf_ordem-compra_new.num-ord-inv).

                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.impr-ficha            <> bf_ordem-compra_new.impr-ficha         THEN 
                DO:
                        ASSIGN  c-campo  =   'IMPR FICHA'
                                c-campo-de      =   string(bf_ordem-compra_old.impr-ficha)
                                c-campo-para    =   string(bf_ordem-compra_new.impr-ficha).
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.codigo-icm            <> bf_ordem-compra_new.codigo-icm         THEN     
                DO:
                        ASSIGN  c-campo  =   'COD ICMS'
                                c-campo-de      =   c-cod-icms[INT(bf_ordem-compra_old.codigo-icm)] 
                                c-campo-para    =   c-cod-icms[INT(bf_ordem-compra_new.codigo-icm)].
                        RUN grava-registro.

                END.

            IF      bf_ordem-compra_old.natureza              <> bf_ordem-compra_new.natureza           THEN     
                DO:
                        ASSIGN  c-campo  =   'NATUREZA'
                                c-campo-de      =   c-natureza[INT(bf_ordem-compra_old.natureza)]
                                c-campo-para    =   c-natureza[INT(bf_ordem-compra_new.natureza)].

                        RUN grava-registro.
                END.
            IF      bf_ordem-compra_old.origem                <> bf_ordem-compra_new.origem             THEN    
                DO:
                        ASSIGN  c-campo  =   'ORIGEM'
                                c-campo-de      =   c-origem[bf_ordem-compra_old.origem] 
                                c-campo-para    =   c-origem[bf_ordem-compra_new.origem]. 
                        RUN grava-registro.

               END.
            IF      bf_ordem-compra_old.ordem-servic          <> bf_ordem-compra_new.ordem-servic       THEN   
                DO:
                       ASSIGN   c-campo  =   'NR ORDEM SERVICO'
                                c-campo-de      =   string(bf_ordem-compra_old.ordem-servic)
                                c-campo-para    =   string(bf_ordem-compra_new.ordem-servic).
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.ct-codigo             <> bf_ordem-compra_new.ct-codigo          THEN   
                DO:
                       ASSIGN   c-campo  =   'CONTA CONTABIL'
                                c-campo-de      =   bf_ordem-compra_old.ct-codigo
                                c-campo-para    =   bf_ordem-compra_new.ct-codigo.
                        RUN grava-registro.
                END.
            IF      bf_ordem-compra_old.sc-codigo             <> bf_ordem-compra_new.sc-codigo          THEN     
                DO:
                        ASSIGN  c-campo  =   'CENTRO DE CUSTO'
                                c-campo-de      =   bf_ordem-compra_old.sc-codigo 
                                c-campo-para    =   bf_ordem-compra_new.sc-codigo.

                        RUN grava-registro.
                END.
            IF      bf_ordem-compra_old.tp-despesa            <> bf_ordem-compra_new.tp-despesa         THEN   
                DO:
                        ASSIGN  c-campo  =   'TP DESPESA'
                                c-campo-de      =   string(bf_ordem-compra_old.tp-despesa)
                                c-campo-para    =   STRING(bf_ordem-compra_new.tp-despesa).
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.op-codigo             <> bf_ordem-compra_new.op-codigo          THEN  
                DO:
                        ASSIGN  c-campo  =   'OP CODIGO'
                                c-campo-de      =   STRING(bf_ordem-compra_old.op-codigo)
                                c-campo-para    =   STRING(bf_ordem-compra_new.op-codigo).
                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.dep-almoxar           <> bf_ordem-compra_new.dep-almoxar        THEN  
                DO:
                        ASSIGN  c-campo  =   'DEP ALMOXARIFADO'
                                c-campo-de      =   bf_ordem-compra_old.dep-almoxar
                                c-campo-para    =   bf_ordem-compra_new.dep-almoxar.
                        RUN grava-registro.
                END.
            IF      bf_ordem-compra_old.cod-estab-gestor      <> bf_ordem-compra_new.cod-estab-gestor   THEN   
                DO:
                        ASSIGN  c-campo  =   'ESTAB GESTOR'
                                c-campo-de      =   bf_ordem-compra_old.cod-estab-gestor 
                                c-campo-para    =   bf_ordem-compra_new.cod-estab-gestor .
                        RUN grava-registro.
                END.
            IF      bf_ordem-compra_old.cod-unid-negoc        <> bf_ordem-compra_new.cod-unid-negoc     THEN 
                DO:
                        ASSIGN  c-campo  =   'UN NEGOCIO'
                                c-campo-de      =   bf_ordem-compra_old.cod-unid-negoc
                                c-campo-para    =   bf_ordem-compra_new.cod-unid-negoc.

                        RUN grava-registro.

                END.
            IF      bf_ordem-compra_old.narrativa             <> bf_ordem-compra_new.narrativa          THEN
                DO:
                       ASSIGN   c-campo  =   'NARRATIVA'
                                c-campo-de      =  TRIM(bf_ordem-compra_old.narrativa)
                                c-campo-para    =  TRIM(bf_ordem-compra_new.narrativa).

                        RUN grava-registro.
                END.
  
    END. /* IF NOT NEW (bf_ordem-compra_new) */  /* inserido em 09/05/2023 */
ELSE
    IF  NEW (bf_ordem-compra_new) 
        and
        substring(bf_ordem-compra_new.char-2,190,10)    <>  ''  THEN
        DO:

            FIND    LAST   es-alter-ordem-compra                                                              
                    WHERE   es-alter-ordem-compra.numero-ordem   =   bf_ordem-compra_new.numero-ordem 
                      AND   es-alter-ordem-compra.data-alteracao =   TODAY                            
                      AND   es-alter-ordem-compra.hora-alteracao =   i-hora
                      AND   es-alter-ordem-compra.campo          =   c-campo
                    EXCLUSIVE-LOCK                                                                       
                    NO-ERROR.                                                                            
                                                                                                         
        IF  NOT AVAIL   es-alter-ordem-compra    THEN                                                 
            DO:                                                                                          
                                                                                                         
                CREATE   es-alter-ordem-compra.                                                       
                                                                                                         
                ASSIGN  es-alter-ordem-compra.numero-ordem   =   bf_ordem-compra_new.numero-ordem     
                        es-alter-ordem-compra.data-alteracao =   TODAY                                
                        es-alter-ordem-compra.hora-alteracao =   i-hora   
                        es-alter-ordem-compra.campo          =   c-campo
                        es-alter-ordem-compra.Cod-usuario    =   c-seg-usuario. 
            END.

        END. /*  IF  NEW (bf_ordem-compra_new) */

/* final inserido em 09/05/2023 */

PROCEDURE grava-registro:

    FIND    LAST   es-alter-ordem-compra                                                              
                    WHERE   es-alter-ordem-compra.numero-ordem   =   bf_ordem-compra_old.numero-ordem 
                      AND   es-alter-ordem-compra.data-alteracao =   TODAY                            
                      AND   es-alter-ordem-compra.hora-alteracao =   i-hora
                      AND   es-alter-ordem-compra.campo          =   c-campo
                    EXCLUSIVE-LOCK                                                                       
                    NO-ERROR.                                                                            
                                                                                                         
        IF  NOT AVAIL   es-alter-ordem-compra    THEN                                                 
            DO:                                                                                          
                                 
                CREATE   es-alter-ordem-compra.                                                       
                                                                                                         
                ASSIGN  es-alter-ordem-compra.numero-ordem   =   bf_ordem-compra_old.numero-ordem     
                        es-alter-ordem-compra.data-alteracao =   TODAY                                
                        es-alter-ordem-compra.hora-alteracao =   i-hora   
                        es-alter-ordem-compra.campo          =   c-campo
                        es-alter-ordem-compra.Cod-usuario    =   c-seg-usuario. 
            END.

           ASSIGN es-alter-ordem-compra.campo-de    = c-campo-de   
                  es-alter-ordem-compra.campo-para  = c-campo-para   
                  c-campo-de                        = ''
                  c-campo-para                      = ''.

END PROCEDURE. /* grava registro */ 
