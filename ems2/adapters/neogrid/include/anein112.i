DEFINE VARIABLE HostName AS CHARACTER NO-UNDO.
DEFINE VARIABLE QueueName AS CHARACTER NO-UNDO.
DEFINE VARIABLE c-authorization AS CHARACTER NO-UNDO.

DEFINE VARIABLE vSocket AS HANDLE NO-UNDO.   
DEFINE VARIABLE wstatus AS LOGICAL NO-UNDO.
DEFINE VARIABLE vStr AS CHARACTER NO-UNDO.
DEFINE VARIABLE lBuffer AS INTEGER NO-UNDO.
DEFINE VARIABLE wloop AS LOGICAL NO-UNDO.

DEFINE VARIABLE mBuffer AS MEMPTR NO-UNDO.

PROCEDURE HTTPChannel:
   DEFINE INPUT PARAMETER pHost AS CHAR NO-UNDO.
   DEFINE INPUT PARAMETER pQueue AS CHAR NO-UNDO.
   DEFINE INPUT-OUTPUT PARAMETER pBuffer AS MEMPTR NO-UNDO.
   DEFINE OUTPUT PARAMETER pStatus AS INTEGER NO-UNDO.
   
   DEFINE VARIABLE hBuffer AS MEMPTR NO-UNDO.
   DEFINE VARIABLE vHost AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vPort AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vStr AS CHARACTER NO-UNDO.
   DEFINE VARIABLE urlHost AS CHARACTER NO-UNDO.

   DEFINE VARIABLE i-max-msg AS INT INIT 5242880 NO-UNDO.

   FIND FIRST ngadapterparms NO-LOCK NO-ERROR.
   IF  AVAIL ngadapterparms THEN
       ASSIGN i-max-msg = IF  INTEGER(SUBSTR(ngadapterparms.queuename,6,5)) >= 1 THEN (INTEGER(SUBSTR(ngadapterparms.queuename,6,5)) * 1048576) ELSE (1048576).
   
   SET-SIZE(mBuffer) = 0.
   SET-SIZE(mBuffer) = i-max-msg.
   lBuffer = 1.

   /* HTTP,http://www.magazineluiza.com.br:8080/servlet@60.134.222.112:8080 */
   
   IF  NUM-ENTRIES(pHost, "@") > 1 THEN /* Via HTTP Proxy */
       ASSIGN vHost = ENTRY(2, pHost, "@")
              urlHost = ENTRY(1, pHost, "@")
              pHost = urlHost.
   ELSE DO: /* Direct HTTP */
       IF  pHost BEGINS "http://" THEN
           ASSIGN SUBSTRING(pHost,1,7) = "".
       IF NUM-ENTRIES(pHost,"/") > 1 THEN
           ASSIGN vHost = ENTRY(1, pHost, "/")
                  urlHost = "/" + ENTRY(2, pHost, "/").
       ELSE
           ASSIGN vHost = pHost
                  urlHost = "".
   END.

   IF  pHost BEGINS "http://" THEN
       ASSIGN SUBSTRING(pHost,1,7) = "".
   IF  NUM-ENTRIES(pHost,"/") > 1 THEN
       ASSIGN pHost = ENTRY(1,pHost, "/").
   IF  NUM-ENTRIES(pHost, ":") > 1 THEN
       ASSIGN pHost = ENTRY(1, pHost, ":").

   IF  vHost BEGINS "http://" THEN
       ASSIGN SUBSTRING(vHost,1,7) = "".
   IF  NUM-ENTRIES(vHost, ":") > 1 THEN 
       ASSIGN vPort = ENTRY(2, vHost, ":")
              vHost = ENTRY(1, vHost, ":").
   ELSE 
       ASSIGN vPort = "80".

   ASSIGN wloop = YES.
      
   CREATE SOCKET vSocket.
   vSocket:SET-READ-RESPONSE-PROCEDURE ("readHandler", THIS-PROCEDURE).
   wstatus = vSocket:CONNECT("-H " + vHost + " -S " + vPort) NO-ERROR.
   IF wstatus = NO THEN DO:
      pStatus = -1.
      DELETE OBJECT vSocket.
      SET-SIZE(mBuffer) = 0.
      RETURN.
   END.
   SET-SIZE(hBuffer) = 0.
   SET-SIZE(hBuffer) = 1000.

   IF GET-SIZE(pBuffer) = 0 THEN
   DO:
       vStr = "GET " + urlHost + "/NeoGridHTTPChannel?queuename=" + pQueue + " HTTP/1.0~n" +
           "Host: " + pHost + "~n" +
           "Accept: text/xml~n" + 
           IF  c-authorization = "" THEN "~n" ELSE ("NeoGrid-authorization: " + c-authorization + "~n~n").
       PUT-STRING(hBuffer,1) = vStr.
       vSocket:WRITE(hBuffer, 1, LENGTH(vStr)).
   END.
   ELSE DO:
       vStr = "POST " + urlHost + "/NeoGridHTTPChannel HTTP/1.0~n" +
              "Host: " + pHost + "~n" +
              "Content-length: " + STRING(GET-SIZE(pBuffer) - 1) + "~n" +
              "Content-type: text/xml~n" +
              "NeoGrid-queuename: " + pQueue + "~n" + 
              IF  c-authorization = "" THEN "~n" ELSE ("NeoGrid-authorization: " + c-authorization + "~n~n").
       PUT-STRING(hBuffer,1) = vStr.
       vSocket:WRITE(hBuffer, 1, LENGTH(vStr)).
       vSocket:WRITE(pBuffer, 1, (GET-SIZE(pBuffer) - 1)).
   END.
   SET-SIZE(hBuffer) = 0.
   
   DO WHILE wloop:
      WAIT-FOR READ-RESPONSE OF vSocket.
   END.
   
   vSocket:DISCONNECT().
   DELETE OBJECT vSocket.
   
   DEF VAR aux AS INTEGER NO-UNDO.
   DEF VAR ini AS INTEGER INITIAL 1 NO-UNDO.
   DEF VAR lin AS CHAR NO-UNDO.
   DEF VAR iMessage AS INTEGER NO-UNDO.
   iMessage = 1.
   
   DO aux = 1 TO (lBuffer - 1):
       IF GET-STRING(mBuffer,aux,2) = (CHR(13) + CHR(10)) THEN
       DO:
           lin = GET-STRING(mBuffer,ini,aux - ini).
           ini = aux + 2.
           IF lin BEGINS "HTTP" THEN
           DO:
               pStatus = DECIMAL(ENTRY(2,lin," ")).
           END.
           ELSE
               IF lin = "" THEN
               DO:
                   iMessage = aux + 2.
                   LEAVE.
               END.
       END.
   END.

   IF GET-SIZE(pBuffer) = 0 THEN DO:
       SET-SIZE(pBuffer) = (lBuffer - iMessage) + 100.
       ini = 1.
       DO aux = iMessage TO lBuffer BY 30000:
           IF (lBuffer - aux + 1) < 30000 THEN
               PUT-BYTES(pBuffer,ini) = GET-BYTES(mBuffer,aux,(lBuffer - aux) + 1).
           ELSE
               PUT-BYTES(pBuffer,ini) = GET-BYTES(mBuffer,aux,30000).
           ini = ini + 30000.
       END.
   END.
   SET-SIZE(mBuffer) = 0.

END PROCEDURE.

PROCEDURE readHandler:
   DEFINE VARIABLE pLen AS INTEGER NO-UNDO.
   DEFINE VARIABLE rBuffer AS MEMPTR NO-UNDO.
   IF vSocket:CONNECTED() THEN DO:
       pLen = vSocket:GET-BYTES-AVAILABLE().   
       IF pLen > 0 THEN DO:
           SET-SIZE(rBuffer) = 0.
           SET-SIZE(rBuffer) = pLen + 1.
           vSocket:READ(rBuffer, 1, pLen, 1).
           pLen = vSocket:BYTES-READ.
           IF pLen > 0 THEN DO:
               PUT-BYTES(mBuffer,lBuffer) = GET-BYTES(rBuffer,1,pLen).
               lBuffer = lBuffer + pLen.
           END.
           SET-SIZE(rBuffer) = 0.
       END.
       wloop = YES.
   END.
   ELSE DO:
      wloop = NO.
      vSocket:DISCONNECT().
   END.
END.
