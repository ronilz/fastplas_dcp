/*************************************************************************
**
** ANEIN001.I
**
**************************************************************************/

DEF TEMP-TABLE tt-attribute-{1} NO-UNDO
    FIELD elementid AS INT
    FIELD name      AS CHAR
    FIELD val       AS CHAR INIT "".
