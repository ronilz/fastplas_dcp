/* variáveis */

def var h-api        as handle no-undo.
def var Hobj         as int    no-undo.  /* Object handle */
def var Hconn        as int    no-undo.  /* Connection handle */
def var CompCode     as int    no-undo.  /* Completion code */
def var Reason       as int    no-undo.  /* Reason code qualifying CompCode */
def var QueueManager as char   no-undo.
def var QueueName    as char   no-undo.
def var l-final      as logical no-undo initial no.
