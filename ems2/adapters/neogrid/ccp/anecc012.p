/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/ 

{include/i-prgvrs.i ANECC012 2.00.00.001}  /*** 010001 ***/
/******************************************************************************
**  Programa: Partner.p
**  Funcao  : Sender de Fornecedor
**  Data    : 06/2002
**  Autor   : Anderson Silvano
**  Versao  :
**  I.00.000 - Anderson Silvano
******************************************************************************/

/* Todos os programas que forem exclusivos do projeto NeoGrid (b2b), somente
   rodarao em clientes com versao do Progress igual ou superior a 8. E' um
   pre-requisito para instalar o sistema.
   O programa abaixo foi digitado no editor da 9, por isso apresenta as
   quebras de linha. */

DEF INPUT PARAM p-rowid        AS ROWID                   NO-UNDO.
DEF INPUT PARAM c-cod-estabel  LIKE estabelec.cod-estabel NO-UNDO.
DEF INPUT PARAM i-cod-emitente LIKE emitente.cod-emitente NO-UNDO.

DEF VAR c-root                           AS CHAR NO-UNDO.
DEF VAR c-message                        AS CHAR NO-UNDO.
DEF VAR c-campo                          AS CHAR NO-UNDO.
DEF VAR c-FromRole                       AS CHAR NO-UNDO.
DEF VAR c-ToRole                         AS CHAR NO-UNDO.
DEF VAR c-UpdatePartner                  AS CHAR NO-UNDO.
DEF VAR c-PartnerCollection              AS CHAR NO-UNDO.
DEF VAR c-UpdateEstablishment            AS CHAR NO-UNDO.
DEF VAR c-PartnerInformation             AS CHAR NO-UNDO.
DEF VAR c-EstablishmentRoles             AS CHAR NO-UNDO.
DEF VAR c-EstabIndentificCollection      AS CHAR NO-UNDO.
DEF VAR c-EstablishmentAddressCollection AS CHAR NO-UNDO.
DEF VAR c-EstablishmentContactCollection AS CHAR NO-UNDO.
DEF VAR c-Legal                          AS CHAR NO-UNDO.
DEF VAR c-EstablishmentIdentification    AS CHAR NO-UNDO.
DEF VAR c-EstablishmentAddress           AS CHAR NO-UNDO.
DEF VAR c-Address                        AS CHAR NO-UNDO.
DEF VAR c-ContactNumberCollection        AS CHAR NO-UNDO.
DEF VAR c-ContactNumber                  AS CHAR NO-UNDO.
DEF VAR c-EstablishmentContact           AS CHAR NO-UNDO.
DEF VAR c-ContactInformation             AS CHAR NO-UNDO.

{adapters/neogrid/include/anein004.i} /*sender*/

RUN pi-cabecalho-msg.

IF  p-rowid <> ? THEN DO:
    FIND emitente WHERE ROWID(emitente) = p-rowid NO-LOCK NO-ERROR.
    IF  AVAIL emitente THEN
        RUN   pi-corpo-msg.
END.
ELSE
    FOR EACH  emitente NO-LOCK
        WHERE emitente.identific <> 1:
        RUN  pi-corpo-msg.
    END.

RUN pi-rodape-msg.
WRITExml().

PROCEDURE pi-cabecalho-msg:
    ASSIGN c-root = CreateNeoGridDoc("UpdatePartners","1.01.001","1")
             c-message = AddElement(c-root,"Message")
               c-UpdatePartner = AddElement(c-message,"UpdatePartners")
                 c-PartnerCollection = AddElement(c-UpdatePartner,"PartnerCollection")
                 c-FromRole = AddElement(c-UpdatePartner,"FromRole")
                 c-ToRole = AddElement(c-UpdatePartner,"ToRole").
END PROCEDURE.

PROCEDURE pi-corpo-msg:
    ASSIGN c-UpdateEstablishment = AddElement(c-PartnerCollection,"UpdateEstablishment").

    RUN pi-PartnerInformation-msg.
    RUN pi-PartnerRoles-msg.
    RUN pi-IdentificationCollection-msg.
    RUN pi-AddressCollection-msg.
    RUN pi-ContactCollection-msg.

END PROCEDURE.

PROCEDURE pi-PartnerInformation-msg:
    DEF VAR c-EstablishmentInformation AS CHAR NO-UNDO.
    DEF VAR c-Individual               AS CHAR NO-UNDO.

    ASSIGN c-EstablishmentInformation = AddElement(c-UpdateEstablishment,"EstablishmentInformation")
           c-EstablishmentRoles = AddElement(c-UpdateEstablishment,"EstablishmentRoles")
           c-EstabIndentificCollection = AddElement(c-UpdateEstablishment,"EstablishmentIdentificationCollection")
           c-EstablishmentAddressCollection = AddElement(c-UpdateEstablishment,"EstablishmentAddressCollection")
             c-EstablishmentContactCollection = AddElement(c-UpdateEstablishment,"EstablishmentContactCollection")
             c-campo = AddEleInt(c-EstablishmentInformation,"InternalCode",emitente.cod-emitente)
             c-campo = AddEleChar(c-EstablishmentInformation,"LegalName",emitente.nome-emit)
             c-campo = AddEleInt(c-EstablishmentInformation,"Status",1)
             c-campo = AddEleChar(c-EstablishmentInformation,"URL",emitente.home-page).

    CASE emitente.natureza:
        WHEN 1 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentInformation,"MainIdentification","CNPJ").
        WHEN 2 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentInformation,"MainIdentification","CPF").
        OTHERWISE   ASSIGN c-campo = AddEleChar(c-EstablishmentInformation,"MainIdentification","CNPJ").
    END CASE.

    ASSIGN c-Legal = AddElement(c-EstablishmentInformation,"Legal") 
           c-campo = AddEleChar(c-Legal,"ShortName",emitente.nome-abrev).
END PROCEDURE.

PROCEDURE pi-PartnerRoles-msg:
    ASSIGN c-campo = AddEleChar(c-EstablishmentRoles,"Portal","FALSE")
           c-campo = AddEleChar(c-EstablishmentRoles,"Carrier","FALSE").

    CASE emitente.identific:
        WHEN 1 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentRoles,"Customer","TRUE")
                           c-campo = AddEleChar(c-EstablishmentRoles,"Supplier","FALSE").
        WHEN 2 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentRoles,"Customer","FALSE")
                           c-campo = AddEleChar(c-EstablishmentRoles,"Supplier","TRUE").
        WHEN 3 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentRoles,"Customer","TRUE")
                           c-campo = AddEleChar(c-EstablishmentRoles,"Supplier","TRUE").
    END CASE.

    ASSIGN c-campo = AddEleChar(c-EstablishmentRoles,"PurchaseCentral","FALSE")
           c-campo = AddEleChar(c-EstablishmentRoles,"Distributor","FALSE")
           c-campo = AddEleChar(c-EstablishmentRoles,"Establishment","FALSE").
END PROCEDURE.

PROCEDURE pi-IdentificationCollection-msg:
    ASSIGN c-EstablishmentIdentification = AddElement(c-EstabIndentificCollection,"EstablishmentIdentification")
             c-campo = AddEleChar(c-EstablishmentIdentification,"IdentificationNumber",emitente.cgc).

    CASE emitente.natureza:
        WHEN 1 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentIdentification,"IdentificationType","CNPJ").
        WHEN 2 THEN ASSIGN c-campo = AddEleChar(c-EstablishmentIdentification,"IdentificationType","CPF").
        OTHERWISE   ASSIGN c-campo = AddEleChar(c-EstablishmentIdentification,"IdentificationType","CNPJ").
    END CASE.
END PROCEDURE.

PROCEDURE pi-AddressCollection-msg:
    ASSIGN c-EstablishmentAddress = AddElement(c-EstablishmentAddressCollection,"EstablishmentAddress")
             c-Address = AddElement(c-EstablishmentAddress,"Address")
               c-campo = AddEleChar(c-Address,"AddressLine1",emitente.endereco)
               c-campo = AddEleChar(c-Address,"CityName",emitente.cidade)
               c-campo = AddEleChar(c-Address,"DistrictName",emitente.bairro)
               c-campo = AddEleChar(c-Address,"StateCode",emitente.estado)
               c-campo = AddEleChar(c-Address,"NationalPostalCode",emitente.cep)
               c-campo = AddEleChar(c-Address,"GlobalCountryCode",emitente.pais)
               c-campo = AddEleChar(c-Address,"EmailAddress",emitente.e-mail)
               c-ContactNumberCollection = AddElement(c-Address,"ContactNumberCollection") 
             c-campo = AddEleInt(c-EstablishmentAddress,"AddressType",8).

    IF  emitente.telefone[1] <> "" THEN 
        ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                 c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefone[1])
                 c-campo = AddEleInt(c-ContactNumber,"NumberType",1).
     
    IF  emitente.telefone[2] <> "" THEN
        ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                 c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefone[2])
                 c-campo = AddEleInt(c-ContactNumber,"NumberType",1).

    IF  emitente.telefax <> "" THEN
        ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                 c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefax)
                 c-campo = AddEleInt(c-ContactNumber,"NumberType",2).
                                   
    IF  emitente.endereco-cob <> "" THEN DO:
        ASSIGN c-EstablishmentAddress = AddElement(c-EstablishmentAddressCollection,"EstablishmentAddress")
                 c-Address = AddElement(c-EstablishmentAddress,"Address")
                   c-campo = AddEleChar(c-Address,"AddressLine1",emitente.endereco-cob)
                   c-campo = AddEleChar(c-Address,"CityName",emitente.cidade-cob)
                   c-campo = AddEleChar(c-Address,"DistrictName",emitente.bairro-cob)
                   c-campo = AddEleChar(c-Address,"StateCode",emitente.estado-cob)
                   c-campo = AddEleChar(c-Address,"NationalPostalCode",emitente.cep-cob)
                   c-campo = AddEleChar(c-Address,"GlobalCountryCode",emitente.pais-cob)
                   c-campo = AddEleChar(c-Address,"EmailAddress",emitente.e-mail)
                   c-ContactNumberCollection = AddElement(c-Address,"ContactNumberCollection")
                 c-campo = AddEleInt(c-EstablishmentAddress,"AddressType",3).
    
        IF  emitente.telefone[1] <> "" THEN
            ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                     c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefone[1])
                     c-campo = AddEleInt(c-ContactNumber,"NumberType",1).
         
        IF  emitente.telefone[2] <> "" THEN
            ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                     c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefone[2])
                     c-campo = AddEleInt(c-ContactNumber,"NumberType",1).
    
        IF  emitente.telefax <> "" THEN
            ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                     c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefax)
                     c-campo = AddEleInt(c-ContactNumber,"NumberType",2).
    END.

    FOR EACH  loc-entr NO-LOCK
        WHERE loc-entr.nome-abrev = emitente.nome-abrev:

        ASSIGN c-EstablishmentAddress = AddElement(c-EstablishmentAddressCollection,"EstablishmentAddress")
                 c-Address = AddElement(c-EstablishmentAddress,"Address")
                   c-campo = AddEleChar(c-Address,"AddressLine1",loc-entr.endereco)
                   c-campo = AddEleChar(c-Address,"CityName",loc-entr.cidade)
                   c-campo = AddEleChar(c-Address,"DistrictName",loc-entr.bairro)
                   c-campo = AddEleChar(c-Address,"StateCode",loc-entr.estado)
                   c-campo = AddEleChar(c-Address,"NationalPostalCode",loc-entr.cep)
                   c-campo = AddEleChar(c-Address,"GlobalCountryCode",loc-entr.pais)
                   c-campo = AddEleChar(c-Address,"EmailAddress",loc-entr.e-mail)
                   c-ContactNumberCollection  = AddElement(c-Address,"ContactNumberCollection")
                 c-campo = AddEleInt(c-EstablishmentAddress,"AddressType",2).
    
        IF  emitente.telefone[1] <> "" THEN
            ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                     c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefone[1])
                     c-campo = AddEleInt(c-ContactNumber,"NumberType",1).
         
        IF  emitente.telefone[2] <> "" THEN
            ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                     c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefone[2])
                     c-campo = AddEleInt(c-ContactNumber,"NumberType",1).
    
        IF  emitente.telefax <> "" THEN
            ASSIGN c-ContactNumber = AddElement(c-ContactNumberCollection,"ContactNumber")
                     c-campo = AddEleChar(c-ContactNumber,"Number",emitente.telefax)
                     c-campo = AddEleInt(c-ContactNumber,"NumberType",2).
     END.
END PROCEDURE.

PROCEDURE pi-ContactCollection-msg:
    FOR EACH  cont-emit NO-LOCK
        WHERE cont-emit.cod-emitente = emitente.cod-emitente:

        ASSIGN c-EstablishmentContact = AddElement(c-EstablishmentContactCollection,"EstablishmentContact")
                 c-ContactInformation = AddElement(c-EstablishmentContact,"ContactInformation")
                   c-campo = AddEleChar(c-ContactInformation,"ContactName",cont-emit.nome)
                   c-campo = AddEleChar(c-ContactInformation,"TelephoneNumber",cont-emit.telefone)
                   c-campo = AddEleChar(c-ContactInformation,"EmailAddress",cont-emit.e-mail)
                 c-campo = AddEleInt(c-EstablishmentContact,"ContactType",2).
    END.
END PROCEDURE.

PROCEDURE pi-rodape-msg:
    ASSIGN   c-campo  = AddEleChar(c-fromrole,"LegacyLocationIdentifier",c-cod-estabel)
             c-campo  = AddEleChar(c-fromrole,"GlobalPartnerRoleClassificationCode","BUYER")
             c-campo  = AddEleChar(c-torole,"LegacyLocationIdentifier",c-cod-estabel)
             c-campo  = AddEleChar(c-torole,"GlobalPartnerRoleClassificationCode","SELLER").
END.
