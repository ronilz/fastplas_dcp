&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0585RP 0.00.04.001}
/***********************************************************************
**
** li0585.p  - Converte EXCELL para TXT                    
**
**             07/04/2005 -  BENE   Programa novo.
**                        LAYOUT
**             A 1a. linha do arquivo EXCELL deve conter as instrucoes
**             . Campo COMO ESTA_______________________________Ciii-ttt
**             . Campo ALFA____________________________________Xiii-ttt
**             . Campo Numerico______________________sem sinal_Niii-ttt
**             . Campo Numerico______________________Com sinal_Siii-ttt
**             . Campo NUMERICO com VIRGULA(decimal)_sem sinal_Niii-tttVqqq
**             . Campo NUMERICO com VIRGULA(decimal)_Com sinal_Siii-tttVqqq
**             . Campo DATA____________________________________Diii-mmddaa[aa]
**                                                          ou Diii-ddmmaa[aa]
**                                                          ou Diii-aammdd    
**         onde: 
**               iii = numero da posicao INICIO do dado no arq. de saida
**               ttt = numero com o TAMANHO do dado no arq. de saida
**               qqq = numero com a QUANTIDADE de decimais no arq. de saida
**               campos S geram sinal na ultima posicao ou branco se positivo.
**
**
**             28/01/11 - CONVERTIDO PARA EMS206
***********************************************************************/


/* INFORMA��ES IMPORTANTES REFERENTE AO ARQUIVO CSV
   MESSAGE "FORMATO do arq.EXCELL deve ser CSV e a 1a.linha deve conter as instru��es:"
            SKIP(2) 
            "Exemplo"
            SKIP(1)  "Primeira linha:"
            SKIP     "c001-013;n015-004"
            SKIP(1)     "Demais linhas do arquivo:"
            SKIP     "1003.0066.749;0"
            SKIP     "1003.0066.759;0"
 
            SKIP(2) "Veja abaixo o tipo e formato do campo na primeira linha:"
            SKIP    ".Campo COMO EST�                       Ciii-ttt"
            SKIP    ".Campo ALFA                                   Xiii-ttt"
            SKIP    ".Campo NUMERICO                         Niii-ttt [sem sinal] ou Siii-ttt [com sinal]"
            SKIP    ".Campo NUMERICO com VIRGULA  Niii-tttVqqq [sem sinal] ou Siii-tttVqqq [com sinal]"
            SKIP    ".Campo DATA                                  Diii-ddmm[aa]aa ou Diii-[aa]aammdd etc."
            SKIP(1) "ONDE:"
            SKIP(1) "iii = numero da posi��o INICIO do dado no arq. TXT"
            SKIP    "ttt = numero com o tamanho do dado no arquivo TXT"
            SKIP    "qqq = numero com a QUANTIDADE de decimais no arquivo TXT"
            SKIP    "campos S geram sinal na �ltima posi��o"
            SKIP    "ou branco se positivo."
 ----------------------------------------------------------------------*/           

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD warqse           AS CHAR FORMAT "X(50)"
    FIELD warqsai          AS CHAR FORMAT "X(50)".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */
/*
DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li0585".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "       Converte EXCELL para TXT       ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)". */
/*DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)". */
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".
/*DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wvia       AS CHAR FORMAT "!".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)".           */         

DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)" NO-UNDO.
/*
DEFINE VARIABLE warqse      AS CHAR FORMAT "x(40)" NO-UNDO.      
DEFINE VARIABLE warqsai     AS CHAR FORMAT "x(40)" NO-UNDO.      
*/
DEFINE VARIABLE wreg        AS CHAR FORMAT "x(999)" NO-UNDO.      


DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".

DEFINE VARIABLE wnumreg     AS INTE.

DEFINE VARIABLE wncampo     AS INTE.                  
DEFINE VARIABLE wtbcampo    AS CHAR FORMAT "x(10)" EXTENT 200 NO-UNDO.
DEFINE VARIABLE wxcampo     AS CHAR FORMAT "x(10)" NO-UNDO.

DEFINE VARIABLE wndado      AS INTE.                  
DEFINE VARIABLE wtbdado     AS CHAR FORMAT "x(100)" EXTENT 200 NO-UNDO.
DEFINE VARIABLE wxdado      AS CHAR FORMAT "x(16)" NO-UNDO.

DEFINE VARIABLE wctdvirg    AS INTE.
DEFINE VARIABLE wlinha      AS CHAR FORMAT "x(999)" NO-UNDO.

DEFINE VARIABLE wctdbarra   AS INTE.
DEFINE VARIABLE wxdd        AS CHAR FORMAT "x(2)".
DEFINE VARIABLE wxmm        AS CHAR FORMAT "x(2)".
DEFINE VARIABLE wxaa        AS CHAR FORMAT "x(2)".
DEFINE VARIABLE wxaaaa      AS CHAR FORMAT "x(4)".
DEFINE VARIABLE wxano       AS CHAR FORMAT "x(4)".

DEFINE VARIABLE wctdhifem   AS INTE.

DEFINE VARIABLE wxinteiro   AS CHAR FORMAT "x(20)".
DEFINE VARIABLE wxdecimal   AS CHAR FORMAT "x(20)".
DEFINE VARIABLE wxsinal     AS CHAR FORMAT "x(1)".


DEFINE VARIABLE wcoluna     AS INTE.
DEFINE VARIABLE wpos        AS INTE.
DEFINE VARIABLE wttcampo    AS INTE.
DEFINE VARIABLE wquebra     AS INTE.


DEFINE VARIABLE wchar       AS CHAR FORMAT "x(1)".
DEFINE VARIABLE wletra1     AS CHAR FORMAT "x(1)".
DEFINE VARIABLE wletra2     AS CHAR FORMAT "x(1)".

DEFINE VARIABLE wini        AS CHAR FORMAT "x(3)".
DEFINE VARIABLE wtam1       AS CHAR FORMAT "x(3)".
DEFINE VARIABLE wtam2       AS CHAR FORMAT "x(3)".

DEFINE VARIABLE wletradatant  AS CHAR FORMAT "x(1)".
 
DEFINE VARIABLE wletradata1 AS CHAR FORMAT "x(4)".
DEFINE VARIABLE wletradata2 AS CHAR FORMAT "x(4)".
DEFINE VARIABLE wletradata3 AS CHAR FORMAT "x(4)".

DEFINE VARIABLE wtamtot    AS INTE.
DEFINE VARIABLE wtamdec    AS INTE.

DEFINE VARIABLE wxvalor    AS CHAR FORMAT "x(30)".

DEFINE STREAM witens.
/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "CONVERTE EXCELL PARA TXT".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).*/
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
 
  /*.........................................inicio verifica arquivos.....*/
  ASSIGN wrel = tt-param.warqsai.
  IF SEARCH(wrel) = wrel THEN DO:
    MESSAGE "ja existe arquivo com este nome: " tt-param.warqsai.
    NEXT.
    END.
  ASSIGN wrel = tt-param.warqse.   
  IF SEARCH(wrel) = ? THEN DO:
    MESSAGE "arquivo nao encontrado: " tt-param.warqse.
    NEXT.
    END.
  /*.........................................fim de verifica arquivos.....*/


  /*...............................................inicio arq.saida........*/
  ASSIGN wrel = tt-param.warqsai.
  OUTPUT STREAM witens TO VALUE(wrel) PAGE-SIZE 0.

  /*.......................................inicio arq.entrada..........*/
  ASSIGN wnumreg = 0.
  INPUT FROM VALUE(tt-param.warqse) NO-ECHO.
  REPEAT:

  
    ASSIGN wlinha = ""
           wreg   = "".

    IMPORT UNFORMATTED wlinha.
    run pi-acompanhar in h-acomp ("Linha: " +  STRING(wlinha)).
    ASSIGN wndado   = 1
           wncampo  = 1 
           wctdvirg = 0. 
               
    ASSIGN wnumreg = wnumreg + 1.

    /*.......................................inicio monta tab de campos...*/
    IF wnumreg = 1 THEN DO:
      DO wcoluna = 1 TO LENGTH(wlinha):
        IF SUBSTRING(wlinha,wcoluna,1) = ";"
        THEN DO:
          ASSIGN wctdvirg = wctdvirg + 1
                 wncampo  = wncampo  + 1.
          NEXT.
          END.

        ASSIGN wtbcampo[wncampo]
             = wtbcampo[wncampo] + SUBSTRING(wlinha,wcoluna,1).

        ASSIGN wttcampo = wncampo.
        NEXT.
        END.
      NEXT.
      END.
    /*.......................................fim de monta tab de campos...*/


    /*.......................................inicio monta tab de dados....*/
    ASSIGN wtbdado = "".
    DO wcoluna = 1 TO LENGTH(wlinha):
      ASSIGN wchar = SUBSTRING(wlinha,wcoluna,1).

      IF wchar = ";"
      THEN DO:
        ASSIGN wctdvirg = wctdvirg + 1
               wndado   = wndado   + 1.
        NEXT.
        END.

      ASSIGN wtbdado[wndado]
           = wtbdado[wndado] + wchar.                        
      NEXT.
      END.
    /*.......................................fim de monta tab de dados....*/



    /*............................................inicio trata campos.....*/
    DO wncampo = 1 TO wttcampo:

      /*..............................inicio determina formato...*/
      ASSIGN wini         = ""
             wletra1      = "" 
             wletra2      = ""
             wtam1        = ""   
             wtam2        = ""
             wletradatant = ""
             wletradata1  = ""
             wletradata2  = ""
             wletradata3  = "".
      ASSIGN wxcampo = STRING(wtbcampo[wncampo]).
      ASSIGN wletra1 = SUBSTRING(wxcampo,1,1).
      ASSIGN wquebra = 0
             wctdhifem = 0.
      DO wcoluna = 2 TO LENGTH(wxcampo):

        ASSIGN wchar = SUBSTRING(wxcampo,wcoluna,1).

        IF wchar = "-" THEN DO:
          ASSIGN wctdhifem = wctdhifem + 1.
          NEXT.
          END.
        IF wctdhifem = 0 THEN DO:
          ASSIGN wini = wini + wchar.
          NEXT.
          END.


        IF wletra1 NE "D" THEN DO:

          IF wchar = "V" THEN DO:
            ASSIGN wletra2 = wchar
                   wquebra = wquebra + 1.
            NEXT.
            END.

          IF  INTE(STRING(wchar)) GE 0
          AND INTE(STRING(wchar)) LE 9
          THEN DO:
            IF wquebra = 0 THEN DO:
              ASSIGN wtam1 = wtam1 + wchar.
              NEXT.
              END.
            IF wquebra GT 0 THEN DO:
              ASSIGN wtam2 = wtam2 + wchar.
              NEXT.
              END.
            END.

          END.


        IF wletra1 = "D" THEN DO:
          IF wchar NE wletradatant THEN DO:
            ASSIGN wquebra = wquebra + 1
                   wletradatant = wchar.
            END.
          IF wquebra = 1 THEN DO:
            ASSIGN wletradata1 = wletradata1 + wchar.
            NEXT.
            END.

          IF wquebra = 2 THEN DO:
            ASSIGN wletradata2 = wletradata2 + wchar.
            NEXT.
            END.
          IF wquebra = 3 THEN DO:
            ASSIGN wletradata3 = wletradata3 + wchar.
            NEXT.
            END.
          END.

        END.
      /*..............................fim de determina formato...*/



      /*.................................inicio consitencia do formato...*/
      IF  wletra1 NE "C"
      AND wletra1 NE "X"
      AND wletra1 NE "N"
      AND wletra1 NE "S"
      AND wletra1 NE "D"
      THEN DO:
       DISPLAY  "* Erro 001:" 
                "Campo" wncampo "com letra" wletra1 "da instrucao" wxcampo
                "invalida" WITH FRAME f-relat.
       DOWN WITH FRAME f-relat.

        NEXT.
        END.
      
      IF INTE(wini) LT 1
      OR INTE(wini) GT 999 THEN DO:
        DISPLAY  "* Erro 002:" 
                "Campo" wncampo "com inicio" wini "da instrucao" wxcampo
                "invalida" WITH FRAME f-relat.
        DOWN WITH FRAME f-relat.
        NEXT.
        END.

      IF wletra1 NE "D" THEN DO:
        IF INTE(wtam1) LT 1
        OR INTE(wtam1) GT 100 THEN DO:
          DISPLAY "* Erro 003:"
                  "Campo" wncampo "com tamanho" wtam1 "da instrucao" wxcampo
                  "invalida" WITH FRAME f-relat.
          DOWN WITH FRAME f-relat.
          NEXT.
          END.
        END.

      IF (wletra1 = "N" AND wletra2 = "V")
      OR (wletra1 = "S" AND wletra2 = "V")
      THEN DO:
        IF INTE(wtam2) LT 1
        OR INTE(wtam2) GT 100 THEN DO:
          DISPLAY  "* Erro 004:"
                  "Campo" wncampo "com qtde de decimais" wtam2 
                  "da instrucao" wxcampo
                  "invalida" WITH FRAME f-relat.
          DOWN WITH FRAME f-relat.
          NEXT.
          END.
        END.
      
      IF  wletra1 = "D"
      THEN DO:

        IF  wletradata1 NE "dd"
        AND wletradata1 NE "mm"
        AND wletradata1 NE "aa"
        AND wletradata1 NE "aaaa"
        THEN DO:
        DISPLAY "* Erro 005:"
                "Campo" wncampo "com formato de data" wletradata1 
                "da instrucao" wxcampo
                "invalida" WITH FRAME f-relat.
          DOWN WITH FRAME f-relat.
          NEXT.
          END.

        IF  wletradata2 NE "dd"
        AND wletradata2 NE "mm"
        AND wletradata2 NE "aa"
        AND wletradata2 NE "aaaa"
        THEN DO:
        DISPLAY  "* Erro 006:"
                "Campo" wncampo "com formato de data" wletradata2 
                "da instrucao" wxcampo
                "invalida" WITH FRAME f-relat.
        DOWN WITH FRAME f-relat.
          NEXT.
          END.

        IF  wletradata3 NE "dd"
        AND wletradata3 NE "mm"
        AND wletradata3 NE "aa"
        AND wletradata3 NE "aaaa"
        THEN DO:
        DISPLAY "* Erro 007:"
                "Campo" wncampo "com formato de data" wletradata3 
                "da instrucao" wxcampo
                "invalida" WITH FRAME f-relat.
        DOWN WITH FRAME f-relat.
          NEXT.
          END.

        END.
      /*.................................fim de consitencia do formato...*/



      /*.......................................inicio de monta dados...*/
      ASSIGN wpos = INTE(wini)
             wxdado = wtbdado[wncampo].

      IF wletra1 = "C"
      THEN DO:
        ASSIGN SUBSTRING(wreg,wpos,INTE(wtam1))
             = SUBSTRING(wxdado,1,INTE(wtam1)).
        END.
        
      IF wletra1 = "X"
      THEN DO:
        ASSIGN SUBSTRING(wreg,wpos,INTE(wtam1))
             = STRING(wxdado).
        END.


      /*............................................inicio trata numeros...*/
      IF wletra1 = "N"
      OR wletra1 = "S"
      THEN DO:
        
        /*...................................inicio obtem numeros...*/
        ASSIGN wctdvirg  = 0
               wxinteiro = ""
               wxdecimal = ""
               wxsinal   = " ".
               
        DO wcoluna = 1 TO LENGTH(wxdado):
          ASSIGN wchar = SUBSTRING(wxdado,wcoluna,1).
          
          IF wchar = "," THEN DO:
            ASSIGN wctdvirg = wctdvirg + 1.
            NEXT.
            END.
            
          IF wchar = "-" THEN DO:
            ASSIGN wxsinal = wchar.          
            NEXT.
            END.

          IF wchar = "0"              
          OR wchar = "1"              
          OR wchar = "2"              
          OR wchar = "3"              
          OR wchar = "4"              
          OR wchar = "5"              
          OR wchar = "6"              
          OR wchar = "7"              
          OR wchar = "8"              
          OR wchar = "9"              
          THEN DO:
            IF wctdvirg = 0
            THEN ASSIGN wxinteiro = wxinteiro + wchar.
            
            IF wctdvirg = 1
            THEN ASSIGN wxdecimal = wxdecimal + wchar.

            END.
          END.
        /*...................................fim de obtem numeros...*/


        /*...................................inicio monta numeros...*/
        ASSIGN wtamtot = 0
               wtamdec = 0.
        IF wtam1 NE "" THEN ASSIGN wtamtot = INTE(wtam1).
        IF wtam2 NE "" THEN ASSIGN wtamdec = INTE(wtam2).

        IF wletra1 = "N" THEN ASSIGN wxsinal = "".

        ASSIGN wxvalor
        = FILL("0",(wtamtot - LENGTH(wxinteiro) - wtamdec - LENGTH(wxsinal)))
        + wxinteiro + wxdecimal 
        + FILL("0",(wtamdec - LENGTH(wxdecimal)))
        + wxsinal.
      

        ASSIGN SUBSTRING(wreg,wpos,wtamtot) = wxvalor.
        /*...................................fim de monta numeros...*/

        END.
      /*............................................fim de trata numeros...*/



      /*..............................................inicio trata datas....*/
      IF wletra1 = "D"
      THEN DO:
        
        /*.....................................inicio obtem dd mm aa ...*/
        ASSIGN wctdbarra = 0
               wxdd      = ""
               wxmm      = ""
               wxano     = ""
               wxaa      = ""
               wxaaaa    = "".

        DO wcoluna = 1 TO LENGTH(wxdado):
          IF SUBSTRING(wxdado,wcoluna,1) = "/"
          THEN DO:
            ASSIGN wctdbarra = wctdbarra + 1.
            NEXT.
            END.
          
          IF wctdbarra = 0
          THEN ASSIGN wxdd   = wxdd   + SUBSTRING(wxdado,wcoluna,1).

          IF wctdbarra = 1
          THEN ASSIGN wxmm = wxmm + SUBSTRING(wxdado,wcoluna,1).

          IF wctdbarra = 2
          THEN ASSIGN wxano = wxano + SUBSTRING(wxdado,wcoluna,1).

          END.

        IF LENGTH(wxdd)  = 1 THEN ASSIGN wxdd  = "0" + STRING(wxdd).
        IF LENGTH(wxmm)  = 1 THEN ASSIGN wxmm  = "0" + STRING(wxmm).
        IF LENGTH(wxano) = 1 THEN ASSIGN wxano = "0" + STRING(wxano).

        IF LENGTH(wxano) = 4 THEN DO:
          ASSIGN wxaaaa = wxano
                 wxaa   = SUBSTRING(wxano,3,2).
          END.

        IF LENGTH(wxano) = 2 THEN DO:
          ASSIGN wxaa = wxano.
          IF INTE(wxaa) GT 50 THEN DO:
            ASSIGN wxaaaa = "19" + STRING(wxaa).
            END.
          IF INTE(wxaa) LE 50 THEN DO:
            ASSIGN wxaaaa = "20" + STRING(wxaa).
            END.
          END.
        /*.....................................fim de obtem dd mm aa ...*/


        /*.....................................inicio monta datas...*/
        
        /*...............................inicio primeiras...*/
        IF wletradata1 = "dd" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxdd)
                 wpos = wpos + 2.
          END.
        IF wletradata1 = "mm" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxmm)
                 wpos = wpos + 2.
          END.
        IF wletradata1 = "aa" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxaa)
                 wpos = wpos + 2.
          END.
        IF wletradata1 = "aaaa" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,4) = STRING(wxaaaa)
                 wpos = wpos + 4.
          END.
        /*...............................fim de primeiras...*/


        /*...............................inicio meio.....*/
        IF wletradata2 = "dd" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxdd)
                 wpos = wpos + 2.
          END.
        IF wletradata2 = "mm" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxmm)
                 wpos = wpos + 2.
          END.
        IF wletradata2 = "aa" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxaa)
                 wpos = wpos + 2.
          END.
        IF wletradata2 = "aaaa" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,4) = STRING(wxaaaa)
                 wpos = wpos + 4.
          END.
        /*...............................fim de meio.....*/


        /*...............................inicio restante.....*/
        IF wletradata3 = "dd" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxdd)
                 wpos = wpos + 2.
          END.
        IF wletradata3 = "mm" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxmm)
                 wpos = wpos + 2.
          END.
        IF wletradata3 = "aa" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,2) = STRING(wxaa)
                 wpos = wpos + 2.
          END.
        IF wletradata3 = "aaaa" THEN DO:
          ASSIGN SUBSTRING(wreg,wpos,4) = STRING(wxaaaa)
                 wpos = wpos + 4.
          END.
        /*...............................fim de restante.....*/

        /*.....................................fim de monta datas...*/


        END.
      /*..............................................fim de trata datas....*/


      /*.......................................fim de de monta dados...*/


      END.
    /*............................................fim de trata campos.....*/

    PUT STREAM witens  UNFORMATTED wreg SKIP.
      
    END.

  INPUT CLOSE.
  /*.......................................fim de arq.entrada..........*/

  OUTPUT CLOSE.

  DISPLAY    "O ARQUIVO  FOI CONVERTIDO PARA " AT 01 tt-param.warqsai WITH NO-LABEL.
             

  /*...............................................fim de arq.saida........*/
/* IF OPSYS = "WIN32"  THEN DOS SILENT  NOTEPAD.EXE VALUE(tt-param.warqsai).*/
END PROCEDURE.





