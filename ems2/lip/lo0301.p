/******************************************************************
**   lo0301.p - Atualiza Complemento Familia Comercial     
**              atualiza��o SKIP - PARA BORTOLETO  
**
**              Bene 29/06/1999  Programa Novo.                       
***********************************************************************/
ASSIGN DEFAULT-WINDOW:VIRTUAL-WIDTH-CHARS = 120
       DEFAULT-WINDOW:VIRTUAL-HEIGHT-CHARS = 30
       DEFAULT-WINDOW:WIDTH-CHARS = 120
       DEFAULT-WINDOW:HEIGHT-CHARS = 24
       DEFAULT-WINDOW:TITLE = "TESTE".

DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "lo0301".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I01".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "  Atualiza Complemento Fam.Comercial  ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

DEFINE VARIABLE wmsg       AS CHAR FORMAT "X(78)".
DEFINE VARIABLE wrel       AS CHAR FORMAT "X(30)".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)" INITIAL "listafam".
DEFINE VARIABLE wvideo     AS LOGI FORMAT "Video/Arquivo".

DEFINE VARIABLE wfm-cod-com     LIKE lo-clifam.fm-cod-com.  
DEFINE VARIABLE wfa-skid           LIKE lo-clifam.fa-skid.   
DEFINE VARIABLE wfa-injecao     LIKE lo-clifam.fa-injecao.        
DEFINE VARIABLE wfa-primer             LIKE lo-clifam.fa-primer.
DEFINE VARIABLE wfa-esmalte           LIKE lo-clifam.fa-esmalte.
DEFINE VARIABLE wfa-operacao    LIKE lo-clifam.fa-operacao.

DEFINE VARIABLE wfm-cod-comi    LIKE lo-clifam.fm-cod-com.  
DEFINE VARIABLE wfa-skidi          LIKE lo-clifam.fa-skid.   
DEFINE VARIABLE wfa-injecaoi    LIKE lo-clifam.fa-injecao.        
DEFINE VARIABLE wfa-primeri            LIKE lo-clifam.fa-primer.
DEFINE VARIABLE wfa-esmaltei          LIKE lo-clifam.fa-esmalte.
DEFINE VARIABLE wfa-operacaoi   LIKE lo-clifam.fa-operacao.

DEFINE VARIABLE wfm-cod-comf    LIKE lo-clifam.fm-cod-com.  
DEFINE VARIABLE wfa-skidf          LIKE lo-clifam.fa-skid.   
DEFINE VARIABLE wfa-injecaof    LIKE lo-clifam.fa-injecao.        
DEFINE VARIABLE wfa-primerf            LIKE lo-clifam.fa-primer.
DEFINE VARIABLE wfa-esmaltef          LIKE lo-clifam.fa-esmalte.
DEFINE VARIABLE wfa-operacaof   LIKE lo-clifam.fa-operacao.

DEFINE VARIABLE r-registro AS RECID NO-UNDO.
DEFINE VARIABLE l-resposta AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE c-opcao    AS CHAR.
DEFINE VARIABLE c-comando  AS CHAR EXTENT 08 NO-UNDO
       INITIAL ["Prox",
                "Anterior",
                "Busca",
                "Inclui",
                "Modifica",
                "Elimina",
                "Lista",
                "Fim"].

FORM c-comando[01] FORMAT "X(04)"
     c-comando[02] FORMAT "X(08)"
     c-comando[03] FORMAT "X(05)"
     c-comando[04] FORMAT "X(06)"
     c-comando[05] FORMAT "X(08)"
     c-comando[06] FORMAT "X(07)"
     c-comando[07] FORMAT "X(05)"
     c-comando[08] FORMAT "X(03)"
     WITH ROW 21 CENTERED NO-LABELS NO-BOX ATTR-SPACE FRAME f-comando.

FORM
SKIP(04)
SKIP "   Familia Comercial:" wfm-cod-comi                      
SKIP "Pecas por Skid/Molde:" wfa-skidi                    
SKIP "  Moldes injecao P/H:" wfa-injecaoi                   
SKIP "    Skids primer P/H:" wfa-primeri                    
SKIP "   Skids esmalte P/H:" wfa-esmaltei                   
SKIP "            Operacao:" wfa-operacaoi
WITH NO-LABELS ROW 09 COLUMN 02 WIDTH 80 OVERLAY FRAME f-busca.  

FORM
SKIP(04)
SKIP "   Familia Comercial:" wfm-cod-comi "/" wfm-cod-comf
SKIP "Pecas por Skid/Molde:" wfa-skidi    "/" wfa-skidf
SKIP "  Moldes injecao P/H:" wfa-injecaoi "/" wfa-injecaof
SKIP "    Skids primer P/H:" wfa-primeri  "/" wfa-primerf
SKIP "   Skids esmalte P/H:" wfa-esmaltei "/" wfa-esmaltef
SKIP "            Operacao:" wfa-operacaoi "/" wfa-operacaof
SKIP "    Video ou Arquivo:" wvideo
WITH NO-LABELS ROW 08 COLUMN 02 WIDTH 80 OVERLAY FRAME f-limites.

FORM
SKIP(04) "             Familia Comercial:" wfm-cod-com
SKIP     "                               "             
SKIP     "          Pecas por Skid/Molde:" wfa-skid
SKIP     "            Moldes injecao P/H:" wfa-injecao
SKIP     "              Skids primer P/H:" wfa-primer 
SKIP     "             Skids esmalte P/H:" wfa-esmalte
SKIP     "                      Operacao:" wfa-operacao
SKIP     "                                                                   "
SKIP(03)  
WITH ROW 04 COL 01 NO-LABELS WIDTH 80 FRAME f-grupo.

FORM
    WITH FRAME f-lista ROW 04 COLUMN 01 WIDTH 80 CENTERED 14 DOWN.

HIDE ALL NO-PAUSE.
DEFINE VARIABLE new-win AS WIDGET-HANDLE.
CREATE WINDOW new-win
      ASSIGN TITLE = wpgm + " - " + wtit.
    
CURRENT-WINDOW = new-win.

opcao:
REPEAT FOR lo-clifam ON ENDKEY UNDO,RETRY:

/*RUN lop/lohead.r.*/
  IF (wbdd NE wbdd-integri) THEN DO:
    PAUSE.
    QUIT.
    END.

  PUT SCREEN ROW 24     COLUMN 50
  "                              ".
  VIEW FRAME f-grupo.
  DISPLAY c-comando WITH FRAME f-comando.
  CHOOSE FIELD c-comando AUTO-RETURN WITH FRAME f-comando.
  c-opcao = FRAME-VALUE.
  HIDE MESSAGE NO-PAUSE.

  IF c-opcao = "Prox" THEN DO:
    IF r-registro = ? THEN FIND FIRST lo-clifam NO-LOCK NO-ERROR.
    ELSE DO:
      FIND NEXT lo-clifam NO-LOCK NO-ERROR.
      IF NOT AVAILABLE lo-clifam THEN DO:
        BELL.
        MESSAGE "Busca apos o fim de arquivo".
        FIND PREV lo-clifam NO-LOCK NO-ERROR.
        IF NOT AVAILABLE lo-clifam THEN NEXT opcao.
        END.
      END.
    r-registro = RECID(lo-clifam).
    END.

  IF c-opcao = "Anterior" THEN DO:
    FIND PREV lo-clifam NO-LOCK NO-ERROR.
    IF NOT AVAILABLE lo-clifam THEN DO:
      BELL.
      MESSAGE "Busca antes do inicio do arquivo".
      FIND NEXT lo-clifam NO-LOCK NO-ERROR.
      IF NOT AVAILABLE lo-clifam THEN NEXT opcao.
      END.
    r-registro = RECID(lo-clifam).
    END.

  IF c-opcao = "Busca" THEN DO:
    ASSIGN wfm-cod-comi = "        "
           wfa-skidi    = 0
           wfa-injecaoi = 0           
           wfa-primeri  = 0      
           wfa-esmaltei = 0
           wfa-operacaoi = "".           
    STATUS INPUT "informe o limite inicial para busca".
    UPDATE wfm-cod-comi             
           wfa-skidi        
           wfa-injecaoi             
           wfa-primeri     
           wfa-esmaltei        
           wfa-operacaoi
           WITH FRAME f-busca.    

    IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.
    FIND FIRST lo-clifam
         WHERE lo-clifam.fm-cod-com GE wfm-cod-comi
         AND   lo-clifam.fa-skid    GE wfa-skidi
         AND   lo-clifam.fa-injecao GE wfa-injecaoi
         AND   lo-clifam.fa-primer  GE wfa-primeri
         AND   lo-clifam.fa-esmalte GE wfa-esmaltei
         AND   lo-clifam.fa-operacao GE wfa-operacaoi
         NO-LOCK
         NO-ERROR.             

    IF NOT AVAILABLE lo-clifam THEN DO:
      BELL.
      MESSAGE "Busca apos fim do arquivo".
      FIND PREV lo-clifam NO-LOCK NO-ERROR.
      IF NOT AVAILABLE lo-clifam THEN NEXT opcao.
      END.
    r-registro = RECID(lo-clifam).
    END.

  IF c-opcao = "Inclui" THEN REPEAT TRANSACTION WITH FRAME f-grupo:
    ASSIGN wfm-cod-com = ""
           wfa-skid    = 0
           wfa-injecao = 0
           wfa-primer  = 0
           wfa-esmalte = 0
           wfa-operacao = "".
    CLEAR FRAME f-grupo   NO-PAUSE.
    UPDATE wfm-cod-com        
           WITH FRAME f-grupo.
    FIND        lo-clifam 
          WHERE lo-clifam.fm-cod-com = wfm-cod-com
          NO-ERROR.
    IF AVAILABLE lo-clifam THEN DO:
      BELL.
      MESSAGE "Complemento de Familia Comercial ja cadastrado".
      NEXT.
      END.
    UPDATE wfa-skid
           wfa-injecao
           wfa-primer
           wfa-esmalte
           wfa-operacao
           WITH FRAME f-grupo.
    CREATE lo-clifam.
    ASSIGN lo-clifam.fm-cod-com = wfm-cod-com          
           lo-clifam.fa-skid       = wfa-skid          
           lo-clifam.fa-injecao = wfa-injecao
           lo-clifam.fa-primer         = wfa-primer
           lo-clifam.fa-esmalte       = wfa-esmalte
           lo-clifam.fa-operacao = wfa-operacao.
           
    MESSAGE "Inclusao OK -".
    r-registro = RECID(lo-clifam).
    END.

  IF c-opcao = "Modifica" THEN REPEAT TRANSACTION WITH FRAME f-grupo:
    ASSIGN wfm-cod-com = ""
           wfa-skid    = 0
           wfa-injecao = 0
           wfa-primer  = 0
           wfa-esmalte = 0
           wfa-operacao = "".
    CLEAR FRAME f-grupo   NO-PAUSE.

    FIND lo-clifam WHERE RECID(lo-clifam) = r-registro
    NO-LOCK
    NO-ERROR.
    IF AVAILABLE lo-clifam THEN DO:
      ASSIGN wfm-cod-com = lo-clifam.fm-cod-com
             wfa-skid    = lo-clifam.fa-skid
             wfa-injecao = lo-clifam.fa-injecao
             wfa-primer  = lo-clifam.fa-primer
             wfa-esmalte = lo-clifam.fa-esmalte
             wfa-operacao = lo-clifam.fa-operacao.
      END.
    DISPLAY wfm-cod-com
            wfa-skid
            wfa-injecao
            wfa-primer
            wfa-esmalte
            wfa-operacao
            WITH FRAME f-grupo.
    UPDATE wfm-cod-com        
           WITH FRAME f-grupo.

    FIND        lo-clifam 
          WHERE lo-clifam.fm-cod-com = wfm-cod-com
          NO-ERROR.
    IF NOT AVAILABLE lo-clifam THEN DO:
      BELL.
      MESSAGE "Complemento de Familia Comercial nao cadastrado".
      NEXT.
      END.
    IF AVAILABLE lo-clifam THEN DO:
      ASSIGN wfm-cod-com = lo-clifam.fm-cod-com
             wfa-skid    = lo-clifam.fa-skid
             wfa-injecao = lo-clifam.fa-injecao
             wfa-primer  = lo-clifam.fa-primer
             wfa-esmalte = lo-clifam.fa-esmalte
             wfa-operacao = lo-clifam.fa-operacao.
      END.
    DISPLAY wfa-skid
            wfa-injecao
            wfa-primer
            wfa-esmalte
            wfa-operacao
            WITH FRAME f-grupo.
    UPDATE wfa-skid
           wfa-injecao
           wfa-primer
           wfa-esmalte
           wfa-operacao
           WITH FRAME f-grupo.
    ASSIGN lo-clifam.fa-skid       = wfa-skid          
           lo-clifam.fa-injecao = wfa-injecao
           lo-clifam.fa-primer         = wfa-primer
           lo-clifam.fa-esmalte       = wfa-esmalte
           lo-clifam.fa-operacao = wfa-operacao.              
    MESSAGE "Modificacao OK -".
    r-registro = RECID(lo-clifam).
    END.

  IF c-opcao = "Elimina" THEN REPEAT TRANSACTION WITH FRAME f-grupo:
    ASSIGN wfm-cod-com = ""
           wfa-skid    = 0
           wfa-injecao = 0
           wfa-primer  = 0
           wfa-esmalte = 0
           wfa-operacao = "".
    IF r-registro = ?  THEN DO:
      CLEAR FRAME f-grupo   NO-PAUSE.
      UPDATE wfm-cod-com        
             WITH FRAME f-grupo.
      FIND        lo-clifam 
            WHERE lo-clifam.fm-cod-com = wfm-cod-com
            NO-ERROR.
      IF NOT AVAILABLE lo-clifam THEN DO:
        BELL.
        MESSAGE "Complemento de Familia Comercial nao cadastrado".
        NEXT.
        END.
      ASSIGN r-registro = RECID(lo-clifam).
      ASSIGN l-resposta = false.
      MESSAGE "Confirma eliminacao (Sim/Nao): " UPDATE l-resposta.
      IF l-resposta THEN DO:
        DELETE lo-clifam.
        CLEAR FRAME f-grupo.
        ASSIGN r-registro = ?.
        FIND NEXT  lo-clifam
             NO-ERROR.
        IF NOT AVAILABLE lo-clifam THEN DO:
          FIND PREV  lo-clifam
               NO-ERROR.
          IF NOT AVAILABLE lo-clifam THEN NEXT opcao.
          END.
        DISPLAY lo-clifam.fm-cod-com  @ wfm-cod-com
                lo-clifam.fa-skid     @ wfa-skid
                lo-clifam.fa-injecao  @ wfa-injecao
                lo-clifam.fa-primer   @ wfa-primer
                lo-clifam.fa-esmalte  @ wfa-esmalte
                lo-clifam.fa-operacao @ wfa-operacao
                WITH FRAME f-grupo.
        ASSIGN r-registro = RECID(lo-clifam).
        END.
      ELSE DO:
        UPDATE wfm-cod-com        
               WITH FRAME f-grupo.
        NEXT.
        END.
      END.

    IF r-registro <> ? THEN DO:
      FIND lo-clifam WHERE RECID(lo-clifam) = r-registro NO-ERROR.
      IF NOT AVAILABLE lo-clifam THEN DO:
        BELL.
        MESSAGE "Complemento de Familia Comercial nao cadastrado".
        NEXT.
        END.
      ASSIGN r-registro = RECID(lo-clifam).
      ASSIGN l-resposta = false.
      MESSAGE "Confirma eliminacao (Sim/Nao): " UPDATE l-resposta.
      IF l-resposta THEN DO:
        DELETE lo-clifam.
        CLEAR FRAME f-grupo.
        ASSIGN r-registro = ?.
        FIND NEXT lo-clifam NO-ERROR.
        IF NOT AVAILABLE lo-clifam THEN DO:
          FIND PREV lo-clifam NO-ERROR.
          IF NOT AVAILABLE lo-clifam THEN NEXT opcao.
          END.
        DISPLAY lo-clifam.fm-cod-com   @ wfm-cod-com
                lo-clifam.fa-skid      @ wfa-skid
                lo-clifam.fa-injecao   @ wfa-injecao
                lo-clifam.fa-primer    @ wfa-primer
                lo-clifam.fa-esmalte   @ wfa-esmalte
                lo-clifam.fa-operacao  @ wfa-operacao
                WITH FRAME f-grupo.
        ASSIGN r-registro = RECID(lo-clifam).
        END.
      ELSE DO:
        UPDATE wfm-cod-com        
               WITH FRAME f-grupo.
        NEXT.
        END.
      END.
    END.

  IF c-opcao = "Lista" THEN DO:
    PUT SCREEN ROW 23 COLUMN 01 "                                       ".
    CLEAR FRAME f-comando NO-PAUSE.
    ASSIGN wfm-cod-comi = "        "
           wfm-cod-comf = "ZZZZZZZZ"
           wfa-skidi    = 0
           wfa-skidf    = 99.9
           wfa-injecaoi = 0            
           wfa-injecaof = 9999.99       
           wfa-primeri  = 0      
           wfa-primerf  = 9999.99
           wfa-esmaltei = 0         
           wfa-esmaltef = 9999.99    
           wfa-operacaoi = "   "
           wfa-operacaof = "ZZZ"
           wvideo = yes.
    STATUS INPUT "ifa-primerorme os limites para selecao".
    UPDATE wfm-cod-comi wfm-cod-comf
           wfa-skidi wfa-skidf
           wfa-injecaoi wfa-injecaof
           wfa-primeri wfa-primerf
           wfa-esmaltei wfa-esmaltef
           wfa-operacaoi wfa-operacaof
           wvideo
           WITH FRAME f-limites.

    IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.
    IF  wvideo THEN DO:
      wmsg  = "  Video".
      CLEAR FRAME f-txtvideo ALL NO-PAUSE.
      END.
    IF NOT wvideo THEN DO:
      wmsg = "  Arquivo".
      STATUS INPUT "infome o nome do arquivo".
      MESSAGE COLOR NORMAL wmsg UPDATE warqtele.
      IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.
      wmsg = wmsg + " " + STRING(warqtele).
      CLEAR FRAME f-txtarqui ALL NO-PAUSE.
      wrel = "./spool/" + warqtele + ".lst".
      OUTPUT TO VALUE(wrel) PAGE-SIZE 60.
      END.

    DO WITH FRAME f-lh:
      FORM HEADER
        wpgm    AT 01
        wdtd    AT 08
        whra    AT 19
        "  Lista Complemento Familia Comercial " AT 29
        "PG."   AT 69
        PAGE-NUMBER FORMAT ">>>>9"
        SKIP(1) wmsg
        SKIP(1)
        WITH DOWN            ROW 01 FRAME f-lh.

      FOR EACH lo-clifam
         WHERE lo-clifam.fm-cod-com GE wfm-cod-comi
         AND   lo-clifam.fm-cod-com LE wfm-cod-comf
         AND   lo-clifam.fa-skid    GE wfa-skidi
         AND   lo-clifam.fa-skid    LE wfa-skidf
         AND   lo-clifam.fa-injecao GE wfa-injecaoi
         AND   lo-clifam.fa-injecao LE wfa-injecaof
         AND   lo-clifam.fa-primer  GE wfa-primeri
         AND   lo-clifam.fa-primer  LE wfa-primerf
         AND   lo-clifam.fa-esmalte GE wfa-esmaltei
         AND   lo-clifam.fa-esmalte LE wfa-esmaltef
         AND   lo-clifam.fa-operacao GE wfa-operacaoi
         AND   lo-clifam.fa-operacao LE wfa-operacaof
         NO-LOCK
         WITH FRAME f-lh:

        DISPLAY SPACE(18)
                lo-clifam.fm-cod-com                 
                lo-clifam.fa-skid                      
                lo-clifam.fa-injecao
                lo-clifam.fa-primer                         
                lo-clifam.fa-esmalte                       
                lo-clifam.fa-operacao
                WITH FRAME f-lh.
        DOWN 1 WITH FRAME f-lh.
        END.
      END.
    IF NOT wvideo THEN DO:
      OUTPUT CLOSE.
      END.
    ASSIGN r-registro = RECID(lo-clifam).
    END.


  IF c-opcao = "Fim" THEN DO:
    HIDE ALL NO-PAUSE.
    RETURN.
    END.

  IF r-registro <> ? THEN DO:
    FIND lo-clifam WHERE RECID(lo-clifam) = r-registro
         NO-LOCK
         NO-ERROR.      
    DISPLAY lo-clifam.fm-cod-com   @ wfm-cod-com
            lo-clifam.fa-skid      @ wfa-skid
            lo-clifam.fa-injecao   @ wfa-injecao
            lo-clifam.fa-primer    @ wfa-primer
            lo-clifam.fa-esmalte   @ wfa-esmalte
            lo-clifam.fa-operacao  @ wfa-operacao
            WITH FRAME f-grupo.
    END.
  ELSE
    CLEAR FRAME f-grupo NO-PAUSE.
  END.

