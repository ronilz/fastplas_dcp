&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0410RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define NEW SHARED temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wfami            AS CHAR FORMAT "x(08)"
    FIELD wfamf            AS CHAR FORMAT "x(08)"
    FIELD wite             AS CHAR FORMAT "x(16)"
    FIELD wcli             AS CHAR FORMAT "x(12)"
    FIELD wdtdd            AS DATE FORMAT "99/99/9999"
    FIELD wctrle           AS CHAR FORMAT "x(10)"
    FIELD wper             AS CHAR FORMAT "x(10)"
    FIELD wexi             AS CHAR FORMAT "x(10)"
    FIELD wdr              AS CHAR FORMAT "x(10)"
    FIELD wprv             AS CHAR FORMAT "x(18)"
    FIELD wori             AS CHAR FORMAT "x(03)".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* DEFINI��ES DO BENE */ 
DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li0410".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I01".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "     Exibe Previsoes por Familia      ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".


DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

/*........................variaveis para subprograma liwcli.r...........*/
DEFINE NEW SHARED VARIABLE wqualpgm AS CHAR FORMAT "!" INITIAL "U".
DEFINE NEW SHARED VARIABLE wdtpgm   LIKE lo-clipgm.data-pgm INITIAL TODAY.



DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wvia       AS CHAR FORMAT "!".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)".                    

DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    

DEFINE BUFFER   bff-prd     FOR mgfas.lo-cliprd.

DEFINE VARIABLE wttespaco   AS INTE.                   
DEFINE VARIABLE wlinha      AS CHAR FORMAT "x(34)".

DEFINE NEW SHARED VARIABLE wcodrefaux  AS CHAR FORMAT "x(20)".
DEFINE NEW SHARED VARIABLE witemaux  LIKE mgind.item.it-codigo.
DEFINE VARIABLE wctd1       AS INTE.
DEFINE VARIABLE wposrefaux  AS INTE.

DEFINE VARIABLE wctdrepeat  AS INTE.

DEFINE VARIABLE wctdprcli   AS INTE FORMAT "99".
DEFINE VARIABLE wctdecod    AS INTE FORMAT "99".
DEFINE VARIABLE wctdmsk     AS INTE FORMAT "99".
DEFINE VARIABLE wqtdfont    AS INTE FORMAT "99".
DEFINE VARIABLE wqtdfixa    AS INTE FORMAT "99".
DEFINE VARIABLE wchrfixo    AS CHAR FORMAT "x".

DEFINE VARIABLE wdesc2      AS CHAR      FORMAT "x(78)".
DEFINE VARIABLE wdesc3      AS CHARACTER FORMAT "x(78)".
DEFINE VARIABLE wdescprv    AS CHARACTER FORMAT "x(25)".
DEFINE VARIABLE wdescobj    AS CHARACTER FORMAT "x(15)".
DEFINE VARIABLE wdescope    AS CHARACTER FORMAT "x(10)".
DEFINE NEW SHARED VARIABLE wfa-operacao   LIKE mgfas.lo-clifam.fa-operacao.
DEFINE VARIABLE wfam        AS CHAR      FORMAT "x(3)".
DEFINE VARIABLE wfami       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wfamf       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wite        LIKE mgind.item.it-codigo.   
DEFINE VARIABLE waamm       AS CHARACTER FORMAT "999999".
DEFINE VARIABLE wdtpesqnf   AS DATE      FORMAT "99/99/9999".
DEFINE NEW SHARED VARIABLE wdtdd       AS DATE      FORMAT "99/99/9999".
DEFINE VARIABLE wdtaux      AS DATE      FORMAT "99/99/9999".
DEFINE VARIABLE wdtestoq    AS DATE      FORMAT "99/99/9999".
DEFINE NEW SHARED VARIABLE wdata AS DATE      FORMAT "99/99/9999" EXTENT 5.

DEFINE VARIABLE wcod-pgmmsk    AS CHARACTER FORMAT "x(06)".
DEFINE VARIABLE wnome-abrevant LIKE mgfas.lo-clipgm.nome-abrev.
DEFINE NEW SHARED VARIABLE wcli AS CHARACTER FORMAT "x(12)".
DEFINE VARIABLE witemant       LIKE mgfas.lo-cliprd.it-codigo.
DEFINE NEW SHARED VARIABLE wxx-ltime      LIKE mgfas.lo-cliprd.pr-ltime.
DEFINE VARIABLE wxx-skid       LIKE mgfas.lo-cliprd.pr-skid.
DEFINE VARIABLE wxx-ipe        LIKE mgfas.lo-cliprd.pr-injecao.

DEFINE VARIABLE wjatem        AS INTE.

DEFINE NEW SHARED WORKFILE showcli
          FIELD  numreg       AS INTE
          FIELD  codarq       AS CHAR FORMAT "X(10)"                       
          FIELD  loclista     AS CHAR                                    
          FIELD  geri         AS CHAR FORMAT "X(12)"                       
          FIELD  gerf         AS CHAR FORMAT "X(12)"                     
          FIELD  progatual    LIKE lo-rn01.progatual
          FIELD  cod-emitente AS INTE FORMAT ">>>>>9"                     
          FIELD  nome-abrev   AS CHAR FORMAT "X(12)"                      
          FIELD  data-pgm     AS DATE      FORMAT "99/99/9999"            
          FIELD  cod-pgm      AS CHAR      FORMAT "x(6)"                  
          FIELD  tipo-pgm     AS CHAR      FORMAT "X(1)"                  
          FIELD  dtnf         AS DATE                                        
          FIELD  nf           AS INTE      FORMAT ">>>>>>9"              
          FIELD  atrsd        AS INTE      FORMAT ">>>>>>>>9-"           
          FIELD  ne           AS INTE      FORMAT ">>>>>>>>9-" EXTENT 4.


DEFINE VARIABLE wqd-item    AS INTE FORMAT ">>>>>>>>9-".
DEFINE VARIABLE wctper      AS INTE.
DEFINE VARIABLE wlclipgm    AS CHAR    FORMAT "x(20)".
DEFINE VARIABLE wdescper    AS CHAR    FORMAT "x(10)" EXTENT 5.
DEFINE VARIABLE wlitpre     AS CHAR    FORMAT "x(10)".

DEFINE VARIABLE wctx        AS INTE FORMAT "99".
DEFINE VARIABLE widentx     AS CHAR FORMAT "x"             EXTENT 6.
DEFINE VARIABLE wident      AS CHAR FORMAT "x(6)".

DEFINE VARIABLE whide       AS INTE FORMAT "9".
DEFINE VARIABLE wiano       AS INTE FORMAT "9".
DEFINE VARIABLE wctd        AS INTE FORMAT "99".
DEFINE VARIABLE wctq        AS INTE FORMAT "99".
DEFINE VARIABLE wqde        AS INTE FORMAT "99".
DEFINE NEW SHARED VARIABLE westoq      AS INTE FORMAT ">>>>>>>-".
DEFINE VARIABLE wsem        AS CHAR FORMAT "x(2)".
DEFINE VARIABLE wdia        AS INTE FORMAT ">>9".
DEFINE VARIABLE wdesca      AS CHAR FORMAT "x(05)" EXTENT 4.
DEFINE VARIABLE wdescb      AS CHAR FORMAT "x(05)" EXTENT 4.
DEFINE VARIABLE wctr        AS CHAR FORMAT "x(36)".
DEFINE VARIABLE wctrle      AS CHAR FORMAT "!".
DEFINE VARIABLE wper        AS CHAR FORMAT "!".
DEFINE VARIABLE wexi        AS CHAR FORMAT "!".
DEFINE VARIABLE wdr      AS CHAR FORMAT "!".
DEFINE NEW SHARED VARIABLE wprv        AS CHAR FORMAT "X(18)".
DEFINE VARIABLE wope        AS CHAR FORMAT "!".
DEFINE VARIABLE wori        AS CHAR FORMAT "x(3)".
DEFINE VARIABLE wchok       AS LOGI.
DEFINE VARIABLE wper2       AS CHAR FORMAT "X(08)".
DEFINE VARIABLE wrest       AS INTE FORMAT "99999".
DEFINE VARIABLE wqdedias    AS INTE FORMAT "99" EXTENT 12
       INITIAL [31,28,31,30,31,30,31,31,30,31,30,31].
DEFINE VARIABLE wmes       AS CHAR FORMAT "x(3)" EXTENT 12
                           INITIAL ["JAN","FEV","MAR","ABR","MAI","JUN",
                                    "JUL","AGO","SET","OUT","NOV","DEZ"].

DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(131)".
DEFINE VARIABLE wmsg1       AS CHAR FORMAT "x(131)".
DEFINE VARIABLE wlinha1     AS CHAR FORMAT "x(131)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wcab1       AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wcab2       AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wcab3       AS CHAR FORMAT "x(78)".


DEFINE VARIABLE wdatunit    LIKE mgdis.tb-preco.dt-inival.
DEFINE VARIABLE wvalunit    LIKE mgdis.preco-item.preco-venda.
DEFINE VARIABLE wpreco      AS DECI FORMAT "zzzzzzzzz9.99" EXTENT 6.
DEFINE VARIABLE wttpre      AS DECI FORMAT "zzzzzzzzz9.99" EXTENT 6.
DEFINE VARIABLE wqdehn      AS DECI FORMAT "zzz9.9" EXTENT 5.
DEFINE VARIABLE wtttdn      AS DECI FORMAT "zz9.99" EXTENT 5.
DEFINE VARIABLE wtttpn      AS DECI FORMAT "zzzz9"  EXTENT 5.
DEFINE VARIABLE wttghn      AS DECI FORMAT "zzz9.9" EXTENT 5.
DEFINE VARIABLE wttfqn      AS DECI FORMAT "->>>>>" EXTENT 5.
DEFINE VARIABLE wdisut      AS CHAR FORMAT "x(6)"   EXTENT 5.
DEFINE VARIABLE wdisdn      AS CHAR FORMAT "x(6)"   EXTENT 5.
DEFINE VARIABLE wdisoc      AS CHAR FORMAT "x(6)"   EXTENT 5.
DEFINE VARIABLE wdispn      AS CHAR FORMAT "x(6)"   EXTENT 5.
DEFINE VARIABLE wdishu      AS CHAR FORMAT "x(6)"   EXTENT 5.

DEFINE VARIABLE wchcab      AS INTE FORMAT "9".


/*..............................................inicio considera..........*/
DEFINE VARIABLE westoqsn  AS   LOGI FORMAT "Sim/Nao" INITIAL no.

/* DEFINI��ES LIP/LIWCLIX.P */
DEFINE VARIABLE wlocabrev     AS CHAR   FORMAT "x(12)".
DEFINE VARIABLE wgeri         AS CHAR FORMAT "x(12)".
DEFINE VARIABLE wgerf         AS CHAR FORMAT "x(12)".
DEFINE VARIABLE wgerpedida    AS CHAR FORMAT "x(12)".
DEFINE VARIABLE wprogatual    LIKE lo-rn01.progatual.
DEFINE VARIABLE wseculo       AS CHAR FORMAT "x(02)".
DEFINE VARIABLE wdtx          AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wnfx          AS INTE FORMAT ">>>>>>9".
DEFINE VARIABLE wregcli       AS INTE.             

/* DEFINI��ES LIP/LIWCLY.P */
DEFINE VARIABLE wputscreen  AS CHARACTER FORMAT "x(22)".
DEFINE VARIABLE wdtne       AS DATE      FORMAT "99/99/9999".
DEFINE VARIABLE wqdne       AS INTE      FORMAT "zzzzzz9".
DEFINE VARIABLE wttreme     AS INTE FORMAT "ZZ9".     
DEFINE VARIABLE wpasso      AS INTE FORMAT "ZZ9". 
DEFINE VARIABLE wdiainic    AS INTE FORMAT "9".  

FORM wmsg SKIP wmsg1 WITH FRAME f-cabec.
/* fim bene */


create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Plano Mensal por Fam�lia".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
        ASSIGN wmsg = "Fam�lia: "  + STRING(tt-param.wfami) 
   + "-"
   + STRING(tt-param.wfamf)
   + " / Item: "      + tt-param.wite
   + " / Cliente: "   + tt-param.wcli
   + " / Controle: "  + tt-param.wctrle
   + " / Data: "      + STRING(tt-param.wdtdd,"99/99/9999")
   + " / Periodo: "   + tt-param.wper
   + " / Exibe: "     + tt-param.wexi.


   ASSIGN wmsg1 = 
   "Total: "          + tt-param.wdr
   + " / Previs�o: "  + tt-param.wprv
   + " / Origem: "    + tt-param.wori.

   ASSIGN wlinha1 = FILL ("_",131).
    view frame f-cabec.

    DISPLAY wmsg wmsg1 wlinha1 WITH FRAME f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME






PROCEDURE roda_prog.
/***********************************************************************
**
** li0410.p   - Exibe Previsoes por Familia
**
**              19/09/91  09:22  -  BENE   Programa novo.
**
**              20/11/95  08:50  -  VALERIA - alteracao efetuada para
**                                  passar a considerar a natureza de
**                                  operacao 611 - solicitacao feita
**                                  pelo Bene
**              30/06/98  10:50  -  BENE - Reinicio das NFs
**              25/07/2005       - CONVES�O EMS
***********************************************************************/

/*.................................................fim de entrada padrao..*/


ASSIGN whide    = 1
       wope     = "Todas".


ASSIGN wctdrepeat = 0.
  ASSIGN wctdrepeat = wctdrepeat + 1.
  IF wctdrepeat GT 1 THEN QUIT.

  /*
  RUN lip/liheat.r.
  */
    IF tt-param.wdtdd LT DATE(12,31,1990) THEN DO:
      MESSAGE "consulta permitida a partir de 31/12/1991" VIEW-AS ALERT-BOX.
      NEXT.
      END.

  
    wdtestoq = DATE(MONTH(tt-param.wdtdd),1,YEAR(tt-param.wdtdd)).
    waamm = SUBSTRING(STRING(INTEGER(YEAR(tt-param.wdtdd)),"9999"),1,4)
          + STRING(MONTH(tt-param.wdtdd),"99").


  IF tt-param.wper = "Di�rio" THEN DO:
     wper2 = "DIARIO  ".
     wdtaux = tt-param.wdtdd - 1.
     wctd = 1.
     REPEAT WHILE wctd LE 5:
       wdtaux = wdtaux + 1.
       IF WEEKDAY(wdtaux) = 1 THEN NEXT.
       wdata[wctd] = wdtaux.
       wctd = wctd + 1.
       END.
     REPEAT wctd = 1 TO 4:
       wdesca[wctd] =   STRING(DAY(wdata[wctd]),"99") + "/"
                    + STRING(MONTH(wdata[wctd]),"99").
       wdescb[wctd] = " " + STRING(YEAR(wdata[wctd]),"9999").
       END.
     END.
  IF tt-param.wper = "Semanal" THEN DO:
     wper2 = "SEMANAL ".
     wdata[1] = tt-param.wdtdd.
     wdata[2] = wdata[1] + (8 - (WEEKDAY(wdata[1]))).
     wdata[3] = wdata[2] + 7.
     wdata[4] = wdata[3] + 7.
     wdata[5] = wdata[4] + 7.
     REPEAT wctd = 1 TO 4:
       wiano = (WEEKDAY(DATE(1,1,YEAR(wdata[wctd]))) - 1).
       wqdedias[2] = 28.
       IF (YEAR(wdata[wctd]) MODULO 4) = 0 THEN wqdedias[2] = 29.
       wdia = wiano + DAY(wdata[wctd]).
       REPEAT wqde = 1 TO (INTEGER(MONTH(wdata[wctd])) - 1):
         wdia = wdia + wqdedias[wqde].
         END.
       wrest = wdia MODULO 7.
       IF wrest <> 0 THEN wsem = STRING(TRUNCATE(wdia / 7 + 1,0),"99").
       IF wrest  = 0 THEN wsem = STRING((wdia / 7),"99").
       wdesca[wctd] = " SM" + STRING(wsem,"99").
       wdescb[wctd] = " " + STRING(YEAR(wdata[wctd]),"9999").
       END.
     END.
  IF tt-param.wper = "Mensal" THEN DO:
     wper2 = "MENSAL  ".
     wdata[1] = tt-param.wdtdd.
     wdata[2] = DATE(INTEGER(MONTH(wdata[1])),1,INTEGER(YEAR(wdata[1]))) + 45.
     wdata[2] = wdata[2] - (INTEGER(DAY(wdata[2])) - 1).
     wdata[3]
     = DATE(INTEGER(MONTH(wdata[2] + 45)),1,INTEGER(YEAR(wdata[2] + 45))).
     wdata[4] 
     = DATE(INTEGER(MONTH(wdata[3] + 45)),1,INTEGER(YEAR(wdata[3] + 45))).
     wdata[5] 
     = DATE(INTEGER(MONTH(wdata[4] + 45)),1,INTEGER(YEAR(wdata[4] + 45))).
     REPEAT wctd = 1 TO 4:
       wdesca[wctd] = "  " + wmes[INTEGER(MONTH(wdata[wctd]))].
       wdescb[wctd] = " " + STRING(YEAR(wdata[wctd]),"9999").
       END.
     END.

  /*.........................................inicio descricao do controle...*/
  ASSIGN wdesc3 = FILL(" ",37).                             
  IF tt-param.wdr NE "Fam�lia" THEN DO:
    IF tt-param.wctrle = "Item" THEN DO:
      ASSIGN wdesc3 = "Familia  Item        Descricao       ".
      END.
    IF tt-param.wctrle = "Pe�a" THEN DO:
      ASSIGN wdesc3 = "Familia  Peca        Descricao       ".
      END.
    IF tt-param.wctrle = "C�digos" THEN DO:
      ASSIGN wdesc3 = "Familia  Item        Peca            ".
      END.
    IF tt-param.wprv = "Entrega (GM-RND012)" 
    THEN DO:
      ASSIGN wdesc3 = wdesc3 + "   Estoq ".
      END.
    IF tt-param.wprv = "Produ��o(GM-RND001)" THEN DO:
      ASSIGN wdesc3 = wdesc3 + "         ".
      END.
    END.
  IF tt-param.wdr = "Fam�lia" THEN DO:
    ASSIGN   wdesc3 = "Familia                              ".
    ASSIGN   wdesc3 = wdesc3 + "         ".
    END.


  ASSIGN wdesc2 = STRING(wdescobj,"x(15)")
          + " " + FILL(" ",09)
          + " " + STRING(wper2,"x(08)")
          + " " + STRING(tt-param.wdtdd,"99/99/9999").

  IF tt-param.wexi = "Vertical" THEN DO:
    ASSIGN wdesc3 = wdesc3 + "Qde ".
    END.
  IF tt-param.wexi = "Horizontal" THEN DO:

    ASSIGN wdesc2 = wdesc2 + "     "
                    + "  " + STRING(wdesca[1],"x(5)")
                    + "  " + STRING(wdesca[2],"x(5)")
                    + "  " + STRING(wdesca[3],"x(5)")
                    + "  " + STRING(wdesca[4],"x(5)").

    ASSIGN wdesc3 = wdesc3 + "Atra"
                    + "so" + STRING(wdescb[1],"x(5)")
                    + "  " + STRING(wdescb[2],"x(5)")
                    + "  " + STRING(wdescb[3],"x(5)")
                    + "  " + STRING(wdescb[4],"x(5)").


    END.
  /*.........................................fim da descricao do controle...*/

  /*.........................................inicio montagem do titulo......*/
  DO wctper = 1 TO 5:
    IF wctper = 1 THEN DO:
      ASSIGN wdescper[wctper] = "    ATRASO".
      END.
    IF wctper GT 1 THEN DO:
      ASSIGN wdescper[wctper] = STRING(wdesca[wctper - 1])
                              + STRING(wdescb[wctper - 1]).
      END.
    END.
  /*.........................................fim da montagem do titulo......*/


  /*.........................................inicio montagem do cabecalho...*/
  ASSIGN wcab1 = STRING(wdescprv,"x(25)")
         + " " + STRING(wdescope,"x(10)").

  ASSIGN wcab2 = wdesc2.                    

  ASSIGN wcab3 = wdesc3.

  ASSIGN wchcab = 0.
  /*.........................................fim da montagem do cabecalho...*/


  ASSIGN wdescarq = "                ".
                                              
 
    DO wctq = 1 TO 6:
      wttpre[wctq] = 0.
      END.
    DO wctq = 1 TO 5:
      wttghn[wctq] = 0.
      END.
    witemant = "".

    /*.......................................INICIO TRATAMENTO DA FAMILIA...*/
    FOR EACH mgdis.fam-comerc
       WHERE mgdis.fam-comerc.fm-cod-com GE tt-param.wfami
       AND   mgdis.fam-comerc.fm-cod-com LE tt-param.wfamf
       NO-LOCK:

   

      ASSIGN wchok        = no
             wfa-operacao = "".
      FIND  lo-clifam
      WHERE lo-clifam.fm-cod-com = fam-comerc.fm-cod-com
      NO-LOCK
      NO-ERROR.
      IF AVAILABLE lo-clifam THEN DO: 
        ASSIGN wfa-operacao = lo-clifam.fa-operacao.
        IF lo-clifam.fa-operacao MATCHES(tt-param.wori) THEN ASSIGN wchok = yes.
        END.
      IF NOT wchok THEN NEXT.  

      DO wctq = 1 TO 5:
        wttfqn[wctq] = 0.
        wtttdn[wctq] = 0.
        END.

      /*.......................................INICIO TRATAMENTO DOS ITEMS...*/
      FOR EACH mgind.item 
         WHERE mgind.item.fm-cod-com = mgdis.fam-comerc.fm-cod-com
         AND   mgind.item.cod-obsoleto NE 4
         AND   mgind.item.it-codigo MATCHES(tt-param.wite)
         NO-LOCK:

        ASSIGN witemaux = mgind.item.it-codigo.


        /*.............................inicio montagem cod referencia....*/
        ASSIGN wcodrefaux = "".
        DO wctd1 = 1 TO 20:
          IF  (SUBSTRING(mgind.item.codigo-refer,wctd1,1) NE " ") 
          AND (SUBSTRING(mgind.item.codigo-refer,wctd1,1) NE ".")
          THEN DO:
            ASSIGN wcodrefaux = wcodrefaux
                        + SUBSTRING(mgind.item.codigo-refer,wctd1,1).
            END.
          END.
        /*.............................fim da montagem cod referencia....*/


        /*.............................inicio montagem do controle.......*/
        ASSIGN wctr = "".
        DO wctd1 = 1 TO 8:
          IF  (SUBSTRING(mgdis.fam-comerc.fm-cod-com,wctd1,1) NE " ")
          THEN DO:
            ASSIGN wctr = wctr
                        + SUBSTRING(fam-comerc.fm-cod-com,wctd1,1).
            END.
          END.
        ASSIGN wctr = wctr + " ".
          
        IF (tt-param.wctrle = "Item" OR
            tt-param.wctrle = "C�digos") THEN DO:
          DO wctd1 = 1 TO 16:
            IF (SUBSTRING(mgind.item.it-codigo,wctd1,1) NE " ") 
            THEN DO:
              ASSIGN wctr = wctr
                          + SUBSTRING(mgind.item.it-codigo,wctd1,1).
              END.
            END.
          ASSIGN wctr = wctr + " ".
          END.

        IF  (tt-param.wctrle = "Pe�a" OR
             tt-param.wctrle = "C�digos")
        AND (mgind.item.codigo-refer NE "")      
        THEN DO:
          DO wctd1 = 1 TO 20:
            IF  (SUBSTRING(mgind.item.codigo-refer,wctd1,1) NE " ") 
            AND (SUBSTRING(mgind.item.codigo-refer,wctd1,1) NE ".")
            THEN DO:
              ASSIGN wctr = wctr
                          + SUBSTRING(mgind.item.codigo-refer,wctd1,1).
              END.
            END.
          ASSIGN wctr = wctr + " ".
          END.

        IF LENGTH(wctr) LT 18
        THEN DO:
           ASSIGN wctr = wctr + FILL(" ",(18 - LENGTH(wctr))).
          END.
          
        IF  (tt-param.wctrle = "Item" OR
             tt-param.wctrle = "Pe�a")
        THEN DO:
          ASSIGN wctr = wctr
                      + STRING(mgind.item.descricao-1).
          END.            
        /*.............................fim da montagem do controle.......*/



        FIND FIRST mgfas.lo-cliprd
             WHERE mgfas.lo-cliprd.it-codigo = mgind.item.it-codigo
             NO-LOCK NO-ERROR.
        ASSIGN wxx-skid = 1
               wxx-ipe  = 0.
        IF AVAILABLE mgfas.lo-cliprd THEN DO:
          wxx-skid    = mgfas.lo-cliprd.pr-skid.
          wxx-ipe     = 0.
          IF wope = "Inje��o" THEN wxx-ipe = mgfas.lo-cliprd.pr-injecao.
          IF wope = "Primer" THEN wxx-ipe = mgfas.lo-cliprd.pr-primer.
          IF wope = "Esmalte" THEN wxx-ipe = mgfas.lo-cliprd.pr-esmalte.
          IF witemant = mgfas.lo-cliprd.it-codigo
          AND tt-param.wprv = "Produ��o(GM-RND001)" THEN NEXT.
          witemant = mgfas.lo-cliprd.it-codigo.
          IF mgfas.lo-cliprd.pr-operacao = "" AND wope <> "Todas" THEN NEXT.
          IF mgfas.lo-cliprd.pr-operacao <> "" THEN DO:
  
            IF  (wope = "Inje��o") 
            AND (mgfas.lo-cliprd.pr-operacao <> "I")
            THEN NEXT.

            IF (wope = "Primer")
            AND (mgfas.lo-cliprd.pr-operacao <> "P"
             AND mgfas.lo-cliprd.pr-operacao <> "PE") 
            THEN NEXT.

            IF (wope = "Esmalte")
            AND (mgfas.lo-cliprd.pr-operacao <> "E"
             AND mgfas.lo-cliprd.pr-operacao <> "PE")
            THEN NEXT.
    
            END.
          END.

/*-------------------------------EXTRAINDO NECESSIDADES-----------------------*/

     


        /*.............................INICIO MONTAGEM TABELA DE CLINTES.....*/
        {lip/liwclix.i}.

        FIND FIRST showcli NO-ERROR.

        IF NOT AVAILABLE showcli THEN NEXT.
        /*.............................FIM DA MONTAGEM TABELA DE CLINTES.....*/



        /*........................INICIO MONTAGEM ESTOQUE DATASUL.......*/
        ASSIGN westoq = 0.
        FOR EACH saldo-estoq
           WHERE saldo-estoq.cod-depos = "EXP"
           AND   saldo-estoq.cod-estabel = "1"
           AND   saldo-estoq.it-codigo   = mgind.item.it-codigo
           NO-LOCK:           

          
        
          ASSIGN westoq = westoq + saldo-estoq.qtidade-atu.
          END.
        IF  tt-param.wprv = "Produ��o(GM-RND001)"
        AND (NOT westoqsn)
        THEN DO:
          ASSIGN westoq = 0.
          END.
        /*........................FIM DA MONTAGEM ESTOQUE DATASUL.......*/



        /*.............................INICIO ATUALIZA TABELA DE CLINTES.....*/
        {lip/liwcliy.i}
        /*.............................FIM DE ATUALIZA TABELA DE CLINTES.....*/

        /*.............................INICIO TRATAMENTO TABELA CLIENTES...*/
        
        
        FOR EACH showcli
        BREAK BY showcli.nome-abrev
              BY showcli.data-pgm
              BY showcli.cod-pgm
              BY showcli.tipo-pg:

        
          IF  showcli.atrsd LE 0 
          AND showcli.ne[1] = 0 
          AND showcli.ne[2] = 0 
          AND showcli.ne[3] = 0
          AND showcli.ne[4] = 0 THEN NEXT.
         

          ASSIGN wttfqn[1] = wttfqn[1] + showcli.atrsd
                 wttfqn[2] = wttfqn[2] + showcli.ne[1]
                 wttfqn[3] = wttfqn[3] + showcli.ne[2]
                 wttfqn[4] = wttfqn[4] + showcli.ne[3]
                 wttfqn[5] = wttfqn[5] + showcli.ne[4].

          /*...................................EXIBICAO DA LINHA PADRAO...*/
          IF tt-param.wdr <> "Fam�lia" 
          AND (wbdd = wbdd-integri)      
          THEN DO:

            IF whide   =  0 THEN PAGE.

            /*------------------------------------inicio exibe cabecalho----*/
            IF wchcab = 0 THEN DO:
              ASSIGN wdet = wcab1.
              DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
              ASSIGN wdet = wcab2.
              DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
              ASSIGN wdet = wcab3.
              DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
              ASSIGN wchcab = 1.
              END.
            /*------------------------------------fim de exibe cabecalho----*/


            ASSIGN wcod-pgmmsk = "*".
            IF showcli.codarq = "lo-clipgm" THEN DO:
              ASSIGN wcod-pgmmsk = "PGM="
                                 + STRING(showcli.cod-pgm,"x(06)").
              END.
            IF showcli.codarq = "lo-rn12" 
            OR showcli.codarq = "lo-rn01"
            THEN DO:
              ASSIGN wcod-pgmmsk = "GER="
                    + SUBSTRING(STRING(YEAR(showcli.data-pgm),"9999"),3,2)
                              + STRING(MONTH(showcli.data-pgm),"99")
                              + STRING(DAY(showcli.data-pgm),"99")
                              + STRING(showcli.cod-pgm,"x(06)").
              END.
            IF showcli.codarq = "lo-jitlog" THEN DO:
              ASSIGN wcod-pgmmsk = "JIT="
                                 + STRING(showcli.data-pgm,"99/99/9999").
              END.
            ASSIGN wdet = "".
            DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
            ASSIGN wlclipgm = STRING(showcli.nome-abrev)
                      + " " + STRING(wcod-pgmmsk)
                      + " ".
            
            ASSIGN wdet = STRING(wctr,"x(36)")
                        + FILL(" ",05).

            IF tt-param.wprv = "Entrega (GM-RND012)" 
            THEN DO:
              ASSIGN wdet = wdet + STRING(westoq).
              END.

            DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.

            /*................................INICIO EXIBE HORIZONTAL.......*/
            IF tt-param.wexi = "Horizontal" 
            THEN DO:
             ASSIGN wdet = FILL(" ",(30 - LENGTH(wlclipgm)))
                          + STRING(wlclipgm)
                          + FILL(" ",13)                     
                          + STRING(showcli.atrsd,">>>>>>>")
                          + STRING(showcli.ne[1],">>>>>>>")
                          + STRING(showcli.ne[2],">>>>>>>")
                          + STRING(showcli.ne[3],">>>>>>>")
                          + STRING(showcli.ne[4],">>>>>>>").

              DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
              ASSIGN wlclipgm = "".
              END.
            /*................................FIM DE EXIBE HORIZONTAL.......*/



            /*................................INICIO EXIBE VERTICAL.........*/
            IF tt-param.wexi = "Vertical"
            THEN
            DO wctper = 1 TO 5:
              IF wctper = 1 THEN DO:
                ASSIGN wqd-item = showcli.atrsd.
                END.
              IF wctper GT 1 THEN DO:
                ASSIGN wqd-item = showcli.ne[wctper - 1].
                END.

/***          IF wqd-item NE 0 THEN DO:****LINHA INIBIDA P/EXIBIR QDO ZERO ***/

                ASSIGN wdet = FILL(" ",(30 - LENGTH(wlclipgm)))
                            + STRING(wlclipgm)
                            + STRING(wdescper[wctper],"x(10)")
                            + STRING(wqd-item,">>>>>>>>9-").

                DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
                ASSIGN wlclipgm = "".
/***            END.                   ****LINHA INIBIDA P/EXIBIR QDO ZERO ***/


              END.  
            /*................................FIM DE EXIBE VERTICAL.........*/
            




            END.


          END.


        /*.............................FIM DE TRATAMENTO TABELA CLIENTES...*/


        END.
      whide = 0.
      /*.......................................FIM DE TRATAMENTO DOS ITEMS...*/


      /*............................EXIBICAO DAS QUANTIDADES POR FAMILIA...*/
      IF tt-param.wdr <> "Inibido"  /*
      AND (wbdd = wbdd-integri)       */
      THEN DO:
        IF wttfqn[1] = 0 AND wttfqn[2] = 0 AND wttfqn[3] = 0
                         AND wttfqn[4] = 0 AND wttfqn[5] = 0 THEN NEXT.
        IF tt-param.wdr <> "Fam�lia" THEN DO:
          wdet = FILL(" ",78).
          DISPLAY wdet WITH NO-LABEL STREAM-IO.
          DOWN 1.
          END.
            
        /*------------------------------------inicio exibe cabecalho----*/
        IF wchcab = 0 THEN DO:
          ASSIGN wdet = wcab1.
          DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
          ASSIGN wdet = wcab2.
          DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
          ASSIGN wdet = wcab3.
          DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
          ASSIGN wchcab = 1.
          END.
        /*------------------------------------fim de exibe cabecalho----*/

        ASSIGN wdet = "FAM "
                    + STRING(fam-comerc.fm-cod-com)
              + " " + "TOTAL".
        DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
            
        ASSIGN wlclipgm = FILL(" ",04)
                        + STRING(fam-comerc.descricao,"x(26)").

       
        /*................................INICIO EXIBE HORIZONTAL.......*/
        IF tt-param.wexi = "Horizontal" 
        THEN DO:
           
           ASSIGN wdet = FILL(" ",(30 - LENGTH(wlclipgm)))
                      + STRING(wlclipgm)
                      + FILL(" ",13)                     
                      + STRING(wttfqn[1],">>>>>>>")
                      + STRING(wttfqn[2],">>>>>>>")
                      + STRING(wttfqn[3],">>>>>>>")
                      + STRING(wttfqn[4],">>>>>>>")
                      + STRING(wttfqn[5],">>>>>>>").
          DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
          ASSIGN wlclipgm = "".
          END.
        /*................................FIM DE EXIBE HORIZONTAL.......*/


        /*................................INICIO EXIBE VERTICAL.........*/
        IF tt-param.wexi = "Vertical"
        THEN
        DO wctper = 1 TO 5:
          IF wttfqn[wctper] NE 0 THEN DO:
            ASSIGN wdet = FILL(" ",(30 - LENGTH(wlclipgm)))
                        + STRING(wlclipgm)
                        + STRING(wdescper[wctper],"x(10)")
                        + STRING(wttfqn[wctper],">>>>>>>>9-").
            DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
            ASSIGN wlclipgm = "".
            END.
          END.  
        /*................................FIM DE EXIBE VERTICAL.........*/


        END.

      IF tt-param.wdr NE "Fam�lia" THEN DO:
        ASSIGN wdet = FILL("_",78).
        DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
        ASSIGN wdet = "".
        DISPLAY wdet WITH NO-LABEL STREAM-IO. DOWN 1.
        ASSIGN wchcab = 0.
        END.

      END.
    /*.......................................FIM DO TRATAMENTO DA FAMILIA...*/

END.
