/**************************************************************************
**
** le0101.p  - Gera arquivo Importacao de Parcelas               
**                
**              09/01/2006 -  BENE   Programa novo.
**
**......................................................................
**
**
**       DEFINICAO:
**       A partir do plano de materiais [lo-matplano e
**       lo-matdatas] hoje (09/01/2006) mantido pelo usuario
**       (Rosa) do PCP, gerar arquivo para a inclusao e/ou
**       alteracao da Parcela de Compra no LAYOUT do arquivo
**       de entrada do programa CC0604-IMPORTACAO DE PARCELAS.
**    
**       DETALHAMENTO:
**
**       Selecionar os registros do lo-matplano com:
**       . numero do plano |
**       . item            > solicitados pelo usuario
**       . estabelecimento |
**
**       Ignorar os itens iniciados por 5100
**
**       Para cada data/Quantidade:
**
**       . Transformar a data que esta no formato altanumerico
**         para o formato de data.
**
**       . Verificar se existe parcela ainda nao recebida na
**         tabela prazo-compra.
**               
**         Se existir:
**                  Gerar registro para alteracao de parcela
**         Se nao existir:
**                  Gerar registro para inclusao de parcela.
**
***********************************************************************/

DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "le0101".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "  Gera arquivo Importacao de Parcelas ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".

DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

     

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".

DEFINE VARIABLE wdispfim   AS CHAR FORMAT "x(78)".


DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    
DEFINE VARIABLE wctditem    AS INTE.
DEFINE VARIABLE wtxt        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wlst        AS CHAR FORMAT "x(29)".

DEFINE VARIABLE wdet        AS CHAR FORMAT "x(247)".

DEFINE VARIABLE wreg247     AS CHAR FORMAT "x(247)".
DEFINE VARIABLE wreg179     AS CHAR FORMAT "x(179)".
DEFINE VARIABLE wreg412     AS CHAR FORMAT "x(412)".
DEFINE VARIABLE wreg96      AS CHAR FORMAT "x(96)".
DEFINE VARIABLE wreg159     AS CHAR FORMAT "x(159)".
DEFINE VARIABLE wreg111     AS CHAR FORMAT "x(111)".

define temp-table tt-param
    field destino            as integer
    field arquivo            as char format "x(50)":U
    field usuario            as char format "x(12)":U
    field data-exec          as date
    field hora-exec          as integer
    field classifica         as integer
    field desc-classifica    as char format "x(40)":U
    field wmmaaaax         AS CHAR FORMAT "x(7)"
    field wite             AS CHAR FORMAT "x(16)"
    FIELD wnomea           LIKE emitente.nome-abrev           
    field westab           LIKE lo-matplano.estab
    FIELD warqs            AS CHAR FORMAT "x(08)".

/* inibido para convers�o para gr�fico - valeria 20/06/06 
/*....................................................inicio flags...*/
DEFINE VARIABLE wmmaaaax      AS CHAR FORMAT "x(7)".

DEFINE VARIABLE wite          AS CHAR FORMAT "x(16)".
                               
DEFINE VARIABLE westab        LIKE lo-matplano.estab.

DEFINE VARIABLE wvia          AS CHAR FORMAT "!".

DEFINE VARIABLE warqs         AS CHAR FORMAT "X(08)".  

DEFINE VARIABLE ware          AS CHAR FORMAT "!".
*/
/*....................................................fim de flags...*/


DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".

DEFINE VARIABLE wlitfam     AS CHAR FORMAT "X(8)".

DEFINE VARIABLE wfami       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wfamf       LIKE mgdis.fam-comerc.fm-cod-com.



DEFINE VARIABLE wnomea      LIKE emitente.nome-abrev.

DEFINE VARIABLE wbate       AS   INTE.                    
 
DEFINE VARIABLE wctd        AS   INTE.                    
DEFINE VARIABLE wpos        AS   INTE.                    
DEFINE VARIABLE wtitx3      AS   CHAR FORMAT "x(3)".   
DEFINE VARIABLE wtitx6      AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitx9      AS   CHAR FORMAT "x(9)".   



DEFINE VARIABLE wtitlit     AS   CHAR FORMAT "x(100)".         

/*....................................................inicio titulos...*/
DEFINE VARIABLE wtitsem1    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem2    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem3    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem4    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem5    AS   CHAR FORMAT "x(6)".   

DEFINE VARIABLE wtitsem1-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem2-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem3-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem4-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem5-2  AS   CHAR FORMAT "x(6)".   

DEFINE VARIABLE wtitmes1    AS   CHAR FORMAT "x(3)".   
DEFINE VARIABLE wtitmes2    AS   CHAR FORMAT "x(3)".   
/*....................................................fim de titulos...*/


/*....................................................inicio datas.....*/
DEFINE VARIABLE wdtiniplano  AS   DATE FORMAT "99/99/9999".

DEFINE VARIABLE wdtsem1    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem2    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem3    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem4    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem5    AS   DATE FORMAT "99/99/9999".

DEFINE VARIABLE wdtsem1-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem2-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem3-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem4-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem5-2  AS   DATE FORMAT "99/99/9999".

DEFINE VARIABLE wdtmes1    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtmes2    AS   DATE FORMAT "99/99/9999".
/*....................................................fim de datas.....*/


/*....................................................inicio literais..*/
DEFINE VARIABLE wlitx10     AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wlitsem1    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem2    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem3    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem4    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem5    AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wlitsem1-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem2-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem3-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem4-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem5-2  AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wlitmes1    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitmes2    AS   CHAR FORMAT "x(10)".   
/*....................................................fim de literais..*/

DEFINE VARIABLE wsdofornec LIKE  prazo-compra.quantidade.

DEFINE VARIABLE wquant     LIKE  prazo-compra.quantidade.
DEFINE VARIABLE wxdata     LIKE prazo-compra.data-entrega.



DEFINE VARIABLE wdatacorte  AS   CHAR FORMAT "x(8)".   
DEFINE VARIABLE wnarrat6    AS   CHAR FORMAT "x(6)".   

DEFINE VARIABLE wmes       AS CHAR FORMAT "x(03)" EXTENT 12
                              INITIAL ["JAN","FEV","MAR","ABR","MAI","JUN",
                                       "JUL","AGO","SET","OUT","NOV","DEZ"].
DEFINE VARIABLE wctdmes    AS INTE.
DEFINE VARIABLE wmmm       AS CHAR FORMAT "x(3)".
DEFINE VARIABLE wmm        AS INTE FORMAT "99".
DEFINE VARIABLE wdd        AS INTE FORMAT "99".
DEFINE VARIABLE waaaa      AS INTE FORMAT "9999".
DEFINE VARIABLE wmmplano   AS INTE FORMAT "99". 


DEFINE VARIABLE wnatped     AS INTE FORMAT "99".          
DEFINE VARIABLE wcondpag    LIKE pedido-compr.cod-cond-pag.
DEFINE VARIABLE wprecoforn  LIKE ordem-compra.preco-fornec.

DEFINE VARIABLE wultped     LIKE pedido-compr.num-pedido.
DEFINE VARIABLE wultord     LIKE ordem-compra.numero-ordem.
DEFINE VARIABLE wultimaparc LIKE prazo-compra.parcela.
DEFINE VARIABLE wtotparc    AS INTE.
DEFINE VARIABLE wtotordem   AS INTE.
DEFINE VARIABLE wtotpedid   AS INTE.

DEFINE VARIABLE wnumaltdt   AS INTE FORMAT "99".
DEFINE VARIABLE wnumaltqt   AS INTE FORMAT "99".

DEFINE VARIABLE wqtalt      AS INTE.
DEFINE VARIABLE wqtele      AS INTE.

DEFINE VARIABLE wor03       AS INTE.

DEFINE TEMP-TABLE tt_parc
   FIELD parcela        AS INTE
   FIELD codreg         AS CHAR FORMAT "x(4)"
   FIELD data-entant    LIKE prazo-compra.data-entrega
   FIELD quantant       LIKE prazo-compra.quantidade 
   FIELD data-entrega   LIKE prazo-compra.data-entrega
   FIELD quantidade     LIKE prazo-compra.quantidade 
   FIELD obs            AS CHAR FORMAT "x(76)"
   FIELD utilizado      AS INTE
   INDEX idx-reg IS UNIQUE PRIMARY parcela
   INDEX idx-data                  data-entrega.


DEFINE VARIABLE wnumreg     AS INTE INITIAL 0.
DEFINE TEMP-TABLE tt_msg
   FIELD numreg AS INTE
   FIELD linha  AS CHAR FORMAT "x(132)"
   INDEX idx-reg IS UNIQUE PRIMARY numreg
   INDEX idx-linha                 linha.

/*..................................................inicio tabelas...*/
DEFINE VARIABLE wele        AS INTE.
DEFINE VARIABLE wtbdata     AS DATE FORMAT "99/99/9999" EXTENT 12.
DEFINE VARIABLE wtbqtde     AS INTE FORMAT ">>>>>>9"    EXTENT 12.
/*..................................................fim de tabelas...*/

/*.............................................inicio previsoes...*/
DEFINE VARIABLE wpresn  AS   LOGI FORMAT "Sim/Nao" INITIAL no.
FORM                      
SKIP "   Considera Previsoes:" wpresn
WITH NO-LABELS ROW 06 COLUMN 02 WIDTH 33 OVERLAY 
TITLE "PREVISOES"
FRAME f-prev.
/*.............................................fim de previsoes...*/


/*........................................inicio natureza do pedido..*/
DEFINE VARIABLE wnapedesc    AS CHAR FORMAT "x(14)"   EXTENT 3
       INITIAL ["Compra        ",
                "Servico       ",
                "Beneficiamento"].
DEFINE VARIABLE wnapenum     AS CHAR FORMAT "9"       EXTENT 3
       INITIAL ["1","2","3"].
DEFINE VARIABLE wnapesn      AS LOGI FORMAT "Sim/Nao" EXTENT 3       
       INITIAL [yes,yes,yes].
DEFINE VARIABLE wnapelista   AS CHAR.
DEFINE VARIABLE wnapepos     AS INTE.
      
FORM 
SKIP "1=Compra         " wnapesn[1]
SKIP "2=Servico        " wnapesn[2]
SKIP "3=Beneficiamento " wnapesn[3]
WITH NO-LABELS ROW 12 COLUMN 02 WIDTH 30 OVERLAY
TITLE "NATUREZA DO PEDIDO"
FRAME f-nape.
/*........................................fim de natureza do pedido..*/


/*........................................inicio situacao do pedido..*/
DEFINE VARIABLE wsipedesc    AS CHAR FORMAT "x(12)"   EXTENT 3
       INITIAL ["Impresso    ",
                "Nao Impresso",
                "Eliminado   "].
DEFINE VARIABLE wsipenum     AS CHAR FORMAT "9"       EXTENT 3
       INITIAL ["1","2","3"].
DEFINE VARIABLE wsipesn      AS LOGI FORMAT "Sim/Nao" EXTENT 3       
       INITIAL [yes,yes,yes].
DEFINE VARIABLE wsipelista   AS CHAR.
DEFINE VARIABLE wsipepos     AS INTE.
      
FORM 
SKIP "1=Impresso     " wsipesn[1]
SKIP "2=Nao Impresso " wsipesn[2]
SKIP "3=Eliminado    " wsipesn[3]
WITH NO-LABELS ROW 16 COLUMN 02 WIDTH 30 OVERLAY
TITLE "SITUACAO DO PEDIDO"
FRAME f-sipe.
/*........................................fim de situacao do pedido..*/


/*.......................................inicio situacao da ordem....*/
DEFINE VARIABLE wsitudesc    AS CHAR FORMAT "x(14)"   EXTENT 6
       INITIAL ["Nao Confirmada",
                "Confirmada    ",
                "Cotada        ",
                "Eliminada     ",
                "Em cotacao    ",
                "Terminada     "].
DEFINE VARIABLE wsitunum     AS CHAR FORMAT "9"       EXTENT 6
       INITIAL ["1","2","3","4","5","6"].
DEFINE VARIABLE wsitusn      AS LOGI FORMAT "Sim/Nao" EXTENT 6       
       INITIAL [yes,yes,yes,yes,yes,yes].
DEFINE VARIABLE wsitulista   AS CHAR.
DEFINE VARIABLE wsitupos     AS INTE.
      
FORM 
SKIP "1=Nao Confirmada     " wsitusn[1]
SKIP "2=Confirmada         " wsitusn[2]
SKIP "3=Cotada             " wsitusn[3]
SKIP "4=Eliminada          " wsitusn[4]
SKIP "5=Em Cotacao         " wsitusn[5]
SKIP "6=Terminada          " wsitusn[6]
WITH NO-LABELS ROW 12 COLUMN 22 WIDTH 30 OVERLAY
TITLE "SITUACAO DA ORDEM"
FRAME f-situ.
/*.......................................fim de situacao da ordem....*/


DEFINE IMAGE wlogo1 FILE "\\10.0.1.212\erp\esp\ems2\lep\LEP\2006.BMP"
  FROM X 5 Y 5
  IMAGE-SIZE-PIXELS 558 BY 62.

DEFINE BUTTON   bOK         LABEL "OK".                      
DEFINE BUTTON   bCANCEL     LABEL "CANCEL".

FORM SKIP(03) " " COLON 19 wstart SKIP(11) " "
     WITH NO-LABELS ROW 14 WIDTH 80 FRAME f-box4.

ASSIGN DEFAULT-WINDOW:VIRTUAL-WIDTH-CHARS = 120
       DEFAULT-WINDOW:VIRTUAL-HEIGHT-CHARS = 30
       DEFAULT-WINDOW:WIDTH-CHARS = 120
       DEFAULT-WINDOW:HEIGHT-CHARS = 24
       DEFAULT-WINDOW:TITLE = "TESTE".

/* inibido para convers�o para gr�fico - valeria -  20/06/06
FORM                                              
SKIP wlogo1
SKIP(0.10)                "Plano [MMAAAx]:" to 38 wmmaaaax
SKIP(0.10)                          "Item:" to 38 wite
SKIP(0.10)                    "Fornecedor:" to 38 wnomea
SKIP(0.10)               "Estabelecimento:" to 38 westab
SKIP(0.10)              "Video ou Arquivo:" to 38 wvia
SKIP(0.10)             "Arquivos de Saida:" to 38 warqs
SKIP(0.10) "Abandona, Restaura ou Executa:" to 38 ware
WITH NO-LABELS SCROLLABLE
SIZE 80 BY 22
FRAME f-video
KEEP-TAB-ORDER
.
  */


/*...................................................inicio entrada padrao..*/
ASSIGN 
wcalled  = wpgm

wusr     = STRING(USERID(ldbname(1)),"x(10)")

/* valeria 
wvia     = "A"
warqs    = wpgm + "a"
wimp     = "principal"
wmmaaaax  = STRING(MONTH(TODAY),"99") + STRING(YEAR(TODAY),"9999")
wite     = "*"        
westab   = "*"
wnomea   = "*" 
*/
wfami    = "        "
wfamf    = "ZZZZZZZZ" 
wpresn     = yes       /* considera previsoes                       */
wnapesn[1] = yes       /* natureza do pedido: 1 = compra            */
wnapesn[2] = no        /*                     2 = servico           */
wnapesn[3] = no        /*                     3 = beneficiamento    */
wsipesn[1] = yes       /* situacao do pedido: 1 = Impresso          */
wsipesn[2] = yes       /*                     2 = Nao impresso      */
wsipesn[3] = no        /*                     3 = Eliminado         */
wsitusn[1] = yes       /* Situacao da ordem   1 = Nao confirmada    */
wsitusn[2] = yes       /*                     2 = confirmada        */
wsitusn[3] = yes       /*                     3 = Cotada            */
wsitusn[4] = no        /*                     4 = Eliminada         */
wsitusn[5] = yes       /*                     5 = em cotacao        */
wsitusn[6] = yes       /*                     6 = terminada         */
/* valeria ware = "E" */
.

ASSIGN wnapelista = "".
DO wnapepos = 1 TO 3:
  IF wnapesn[wnapepos] THEN DO:
    IF LENGTH(wnapelista) GT 0 THEN ASSIGN wnapelista = wnapelista + ",".
    ASSIGN wnapelista = wnapelista + wnapenum[wnapepos].
    END.
  END.

ASSIGN wsipelista = "".
DO wsipepos = 1 TO 3:
  IF wsipesn[wsipepos] THEN DO:
    IF LENGTH(wsipelista) GT 0 THEN ASSIGN wsipelista = wsipelista + ",".
    ASSIGN wsipelista = wsipelista + wsipenum[wsipepos].
    END.
  END.

ASSIGN wsitulista = "".
DO wsitupos = 1 TO 6:
  IF wsitusn[wsitupos] THEN DO:
    IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
    ASSIGN wsitulista = wsitulista + wsitunum[wsitupos].
    END.
  END.
/*.................................................fim de entrada padrao..*/

DEFINE VARIABLE new-win AS WIDGET-HANDLE.
CREATE WINDOW new-win
      ASSIGN TITLE = wpgm + " - " + wtit.
    
CURRENT-WINDOW = new-win.


DEFINE VARIABLE wctdrepeat  AS INTE.
ASSIGN wctdrepeat = 0.

REPEAT:
  ASSIGN wctdrepeat = wctdrepeat + 1.
  IF wctdrepeat GT 1
  THEN DO:
    DELETE WIDGET new-win.
    RETURN.
    END.
  
 /* valeria
  UPDATE wmmaaaax
         wite
         wnomea
         westab
         wvia
         warqs
         ware
  WITH FRAME f-video.*/


  /* valeria 
  IF ware = "A" 
  THEN DO:
    DELETE WIDGET new-win.
    RETURN.
    END.
  IF ware = "R" THEN NEXT.

    
  IF wvia = "A"
  THEN DO:
    HIDE FRAME f-video.
    wtxt = "C:/spool/" + warqs + ".txt".
    ASSIGN wmsg = wmsg + " " + STRING(wtxt).
    OUTPUT TO VALUE(wtxt) PAGE-SIZE 0.
    END.


*/



  DO WITH FRAME f-lh:

    FORM 
/*
    HEADER wpgm
      SPACE(00) wvrs
      SPACE(01) wusr
      SPACE(00) wtit
      SPACE(01) wdtd
      SPACE(01) whra
    SKIP(1) wmsg
    SKIP(1)
"Plano   Item        Estab Emp Fornec."
*/
      WITH DOWN NO-LABEL ROW 01
/*
      TITLE (IF wvia = "V" THEN wbdd-area + wbdd   
                           ELSE wbdd-area + wbdd + " PG."
                                     + STRING(PAGE-NUMBER,">>>>9")) 
*/
      WIDTH 249
      FRAME f-lh.

    FOR EACH lo-matplano
       WHERE lo-matplano.nr-pl      MATCHES(tt-param.wmmaaaax)
       AND   lo-matplano.it-codigo  MATCHES(tt-param.wite) 
       AND   lo-matplano.estab      MATCHES(tt-param.westab)
       NO-LOCK WITH FRAME f-lh: 

      PUT SCREEN ROW 24 COLUMN 01 "pesquisando planos "
                       + STRING(lo-matplano.nr-pl,"x(6)")
                 + " " + STRING(lo-matplano.it-codigo,"x(16)")
                       + FILL(" ",28).

      /*...........................inicio ignorar itens 5100 da melida......*/

      IF lo-matplano.it-codigo BEGINS("5100") THEN NEXT.

      /*...........................inicio ignorar itens 5100 da melida......*/

      ASSIGN wdatacorte = "        ".
      IF lo-matplano.data-nota NE ? THEN DO:
        ASSIGN wdatacorte = STRING(lo-matplano.data-nota,"99999999").
        END.

      ASSIGN wlitfam = "".
      FIND item 
      WHERE item.it-codigo = lo-matplano.it-codigo
      NO-LOCK NO-ERROR.
      IF AVAILABLE item THEN  ASSIGN wlitfam = item.fm-cod-com.
      IF wlitfam LT wfami
      OR wlitfam GT wfamf
      THEN DO:
        NEXT.
        END.
      IF NOT AVAILABLE item THEN DO:
        ASSIGN wnumreg = wnumreg + 1.
        CREATE tt_msg.
        ASSIGN 
        tt_msg.numreg = wnumreg
        tt_msg.linha  = "*?* item nao cadastrado:"
                      + STRING(lo-matplano.it-codigo).
        NEXT.
        END.


      IF item.cod-obsoleto = 4 THEN DO:
        ASSIGN wnumreg = wnumreg + 1.
        CREATE tt_msg.
        ASSIGN 
        tt_msg.numreg = wnumreg
        tt_msg.linha = "*?* item obsoleto:"
                     + STRING(lo-matplano.it-codigo).
        NEXT.
        END.


      ASSIGN wbate = 0.   
      FIND emitente
      WHERE emitente.cod-emitente = lo-matplano.cod-emitente
      NO-LOCK NO-ERROR.
      IF emitente.nome-abrev MATCHES(wnomea)
      THEN DO:
        ASSIGN wbate = 1.                                       
        END.
      IF wbate = 0 THEN DO:
        NEXT.
        END.
        

      /*............................................inicio monta titulos...*/
      FIND lo-matdatas
      WHERE lo-matdatas.emp = lo-matplano.emp
      AND   lo-matdatas.estab = lo-matplano.estab
      AND   lo-matdatas.nr-pl = SUBSTRING(lo-matplano.nr-pl,1,6)
      NO-LOCK NO-ERROR.

      IF NOT AVAILABLE lo-matdatas THEN DO:
        ASSIGN wnumreg = wnumreg + 1.
        CREATE tt_msg.
        ASSIGN 
        tt_msg.numreg = wnumreg
        tt_msg.linha  = "*?* [lo-matdatas nao encontrado]"
                      + " Emp:"   + STRING(lo-matplano.emp)
                      + " Estab:" + STRING(lo-matplano.estab)
                      + " Plano:" + STRING(lo-matplano.nr-pl).
        NEXT.
        END.

      IF AVAILABLE lo-matdatas THEN DO:
        ASSIGN wtotparc = 0.
        
        /*.........................................inicio limpa tabelas...*/
        DO wele = 1 TO 12:
          ASSIGN wtbdata[wele] = ?
                 wtbqtde[wele] = 0.
          END.
        ASSIGN wele = 0.  
        /*.........................................inicio limpa tabelas...*/


        /*..............................................inicio monta datas..*/
        ASSIGN waaaa =
           INTEGER(SUBSTRING(STRING(lo-matplano.nr-pl,"x(7)"),3,4)).
        IF waaaa LT 1900 THEN DO:
          ASSIGN wnumreg = wnumreg + 1.
          CREATE tt_msg.
          ASSIGN 
          tt_msg.numreg = wnumreg
          tt_msg.linha  = "*?* [ano do plano invalido]"
                        + " Plano:" + STRING(lo-matplano.nr-pl).
          NEXT.
          END.

        ASSIGN wmmplano =
           INTEGER(SUBSTRING(STRING(wmmaaaax,"x(7)"),1,2)).

        ASSIGN wdtiniplano = DATE(wmmplano,01,waaaa).

        /*.................................inicio monta 1a.parte datas...*/
        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem1)
               wquant = lo-matplano.qt-sem1.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem2)
               wquant = lo-matplano.qt-sem2.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem3)
               wquant = lo-matplano.qt-sem3.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem4)
               wquant = lo-matplano.qt-sem4.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem5)
               wquant = lo-matplano.qt-sem5.
        RUN PI_Semanas.
        /*.................................fim de monta 1a.parte datas...*/


        /*..............................................inicio previsoes...*/
        IF wpresn THEN DO:

          /*.................................inicio monta 2a.parte datas...*/
          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem1-2)
                 wquant = lo-matplano.qt-sem1-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem2-2)
                 wquant = lo-matplano.qt-sem2-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem3-2)
                 wquant = lo-matplano.qt-sem3-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem4-2)
                 wquant = lo-matplano.qt-sem4-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem5-2)
                 wquant = lo-matplano.qt-sem5-2.
          RUN PI_Semanas.
          /*.................................fim de monta 2a.parte datas...*/


          /*.................................inicio monta 3a.parte datas...*/
          ASSIGN wtitx3 = "" wtitx9 = STRING(lo-matdatas.titmes1).
                 wquant = lo-matplano.mes1.
          RUN PI_Meses.


          ASSIGN wtitx3 = "" wtitx9 = STRING(lo-matdatas.titmes2).
                 wquant = lo-matplano.mes2.
          RUN PI_Meses.
          /*.................................fim de monta 3a.parte datas...*/

          END.
        /*..............................................fim de previsoes...*/





        /*..............................................fim de monta datas..*/
        END.
      /*............................................fim de monta titulos...*/


      RUN PI_Parcelas.


      /*........................................inicio monta arquivo.........*/
      IF  wtotpedid = 0 
      AND wtotparc NE 0
      THEN DO:
        ASSIGN wnumreg = wnumreg + 1.
        CREATE tt_msg.
        ASSIGN 
        tt_msg.numreg = wnumreg
        tt_msg.linha = "*?* nenhum pedido foi selecionado "
                     + STRING(lo-matplano.it-codigo)
               + " " + STRING(emitente.nome-abrev).
        NEXT.                      
        END.

      IF  wtotordem = 0 
      AND wtotparc NE 0
      THEN DO:
        ASSIGN wnumreg = wnumreg + 1.
        CREATE tt_msg.
        ASSIGN 
        tt_msg.numreg = wnumreg
        tt_msg.linha = "*?* nenhuma ordem foi selecionada "
                     + STRING(lo-matplano.it-codigo)
               + " " + STRING(emitente.nome-abrev).
        NEXT.                      
        END.


      FIND item-fornec
      WHERE item-fornec.cod-emitente = emitente.cod-emitente
      AND   item-fornec.it-codigo    = lo-matplano.it-codigo
      NO-LOCK NO-ERROR.

      IF NOT AVAILABLE item-fornec
      THEN DO:
        ASSIGN wnumreg = wnumreg + 1.
        CREATE tt_msg.
        ASSIGN 
        tt_msg.numreg = wnumreg
        tt_msg.linha = "*?* nenhum item-fornec selecionado "
                     + STRING(lo-matplano.it-codigo)
               + " " + STRING(emitente.nome-abrev).
        NEXT.                      
        END.


      RUN PI_Arquivo.
      /*........................................fim de monta arquivo.........*/



      ASSIGN wctditem = wctditem + 1.

      END.
/* valeria 
    IF wvia = "V" 
    THEN DO:
      RUN PI_Mensagens.
      END.


    END.


  IF wvia = "A" 
  THEN DO:
    OUTPUT CLOSE.
    wlst = "C:/spool/" + warqs + ".lst".
    ASSIGN wmsg = wmsg + " " + STRING(wlst).
    OUTPUT TO VALUE(wlst) PAGE-SIZE 60.    
    RUN PI_Mensagens.
    OUTPUT CLOSE.    
    END.

  IF wvia = "A" THEN DO:
    DISPLAY "OK - Vide Arquivo de Texto:" wtxt
    SKIP    "OK - Vide Arquivo Listagem:" wlst
    WITH NO-LABEL.
    PAUSE.
    DELETE WIDGET new-win.
    
    RETURN.    
    
    END.*/



  END.


/*................................................inicio procedure semanas...*/
PROCEDURE PI_Semanas.

  DO wpos = 1 TO 9:
    IF SUBSTRING(wtitx9,wpos,1) NE " " THEN DO:
      ASSIGN wtitx6 = wtitx6 + SUBSTRING(wtitx9,wpos,1).
      END.
    END.

  ASSIGN wele = wele + 1.
  ASSIGN wdd = INTEGER(SUBSTRING(STRING(wtitx6,"x(6)"),1,2))
         wmm = 0
         wmmm = SUBSTRING(STRING(wtitx6,"x(6)"),4,3).
  DO wctdmes = 1 TO 12:
    IF wmes[wctdmes] = wmmm THEN DO:
      ASSIGN wmm = wctdmes.
      IF  wele GT 1
      AND wmm LT wmmplano THEN DO:
        ASSIGN waaaa = waaaa + 1.
               wmmplano = wmm.
        END.
      LEAVE.
      END.
    END.
  ASSIGN wxdata  = ?
         wlitx10 = "*?* " + wtitx6.  
  IF wmm GT 0 THEN DO:
    ASSIGN wxdata = DATE(wmm,wdd,waaaa) 
           wlitx10 = STRING(wxdata,"99/99/9999").
    END.
  IF wmm = 0 AND wquant = 0 THEN wlitx10 = "".
  IF wquant NE 0 THEN ASSIGN wtotparc = wtotparc + 1.
  ASSIGN wtbdata[wele] = wxdata
         wtbqtde[wele] = wquant.

  RETURN.
  END PROCEDURE.
/*................................................fim de procedure semanas...*/


/*................................................inicio procedure meses.....*/
PROCEDURE PI_Meses.

  DO wpos = 1 TO 9:
    IF SUBSTRING(wtitx9,wpos,1) NE " " THEN DO:
      ASSIGN wtitx3 = wtitx3 + SUBSTRING(wtitx9,wpos,1).
      IF LENGTH(wtitx3) = 3 THEN LEAVE.
      END.
    END.

  ASSIGN wele = wele + 1.

  ASSIGN wdd = 01
         wmm = 0
         wmmm = wtitx3.                                  
  DO wctdmes = 1 TO 12:
    IF wmes[wctdmes] = wmmm THEN DO:
      ASSIGN wmm = wctdmes.
      IF  wele GT 1
      AND wmm LT wmmplano THEN DO:
        ASSIGN waaaa = waaaa + 1.
               wmmplano = wmm.
        END.
      LEAVE.
      END.
    END.
  ASSIGN wxdata  = ?
         wlitx10 = "*?* " + wtitx3.  
  IF wmm GT 0 THEN DO:
    ASSIGN wxdata = DATE(wmm,wdd,waaaa) 
           wlitx10 = STRING(wxdata,"99/99/9999").
    END.
  IF wmm = 0 AND wquant = 0 THEN wlitx10 = "".
  IF wquant NE 0 THEN ASSIGN wtotparc = wtotparc + 1.
  ASSIGN wtbdata[wele] = wxdata
         wtbqtde[wele] = wquant.

  RETURN.
  END PROCEDURE.
/*................................................fim de procedure meses.....*/



/*................................................inicio procedure arquivo...*/
PROCEDURE PI_Arquivo.

  ASSIGN wor03 = 0.

  FOR EACH tt_parc
  WHERE    tt_parc.codreg NE ""
  NO-LOCK WITH FRAME f-lh:

    FIND  pedido-compr
    WHERE pedido-compr.num-pedido = wultped
    NO-LOCK.


    /*......................................inicio inclusao e alteracao..*/
    IF tt_parc.codreg = "PA00" 
    OR tt_parc.codreg = "AP00"
    THEN DO:

      ASSIGN wor03 = 1.

      /*..................................inicio grava inclusao...........*/
      IF tt_parc.codreg = "PA00" 
      THEN DO:


        /*..................................inicio grava PA00 da inclusao...*/
        ASSIGN wsdofornec = tt_parc.quantidade.
        IF item-fornec.num-casa-dec = 1 THEN DO:
          ASSIGN wsdofornec = wsdofornec / 10.
          END.
        IF item-fornec.num-casa-dec = 2 THEN DO:
          ASSIGN wsdofornec = wsdofornec / 100.
          END.
        IF item-fornec.num-casa-dec = 3 THEN DO:
          ASSIGN wsdofornec = wsdofornec / 1000.
          END.



        ASSIGN wnumaltdt = 0
               wnumaltqt = 0.

        ASSIGN wdet 
        = STRING(tt_parc.codreg,"x(4)")                 

        + SUBSTRING(STRING(wultord,"999999,99"),1,6)
        + SUBSTRING(STRING(wultord,"999999,99"),8,2)

        + STRING(tt_parc.parcela,"99999")
        + STRING(lo-matplano.it-codigo,"x(16)")

        + STRING(INTE(tt_parc.quantidade),"99999999")
        + "0000"

        + STRING(item.un,"x(2)")
        + STRING(tt_parc.data-entrega,"99999999")
        + FILL(" ",12)                           
        + FILL(" ",12)                            /* num.ped.cli */
        + "02"                                    /* situacao da parcela */

        + STRING(INTE(wsdofornec),"9999999")           /* qt.fornecedor       */
        + "0000"

        + FILL("0",11)                                 /* qt.receb.fornec.    */
        + FILL("0",11)                                 /* qt.rej.fornec       */

        + STRING(INTE(wsdofornec),"9999999")           /* qt.saldo.fornec.    */
        + "0000"

        + FILL("0",11)                                 /* qt.receb.da emp.    */
        + FILL("0",11)                                 /* qt.rej.da emp.      */

        + STRING(INTE(tt_parc.quantidade),"9999999")   /* qt.saldo da emp     */
        + "0000"

        + FILL("0",11)                                 /* qt.orig.da emp.     */
        + " "
        + FILL(" ",16)                                 /* num.comtrato        */
        + FILL("0",8)                                  /* centro custo        */
        + FILL("0",5)                                  /* num.entrega         */
        + STRING(tt_parc.data-entrega,"99999999")      /* dt.original         */
        + STRING(TODAY,"99999999")                     /* dt.alt              */
        + STRING(wusr,"x(12)")                         /* usr ult.alt.        */
        + STRING(wnumaltdt,"99")                       /* num.alt.na data     */
        + STRING(wnumaltqt,"99")                       /* num.alt.na.qt.      */
        + STRING(wnatped,"9")
        + FILL(" ",8)                                  /* cod.ref             */
        + FILL("0",2)                                  /* hora entr.          */
        + FILL("0",5).                                 /* sequencia           */

        /*...................................inicio grava reg tam 247......*/
       /* ESTUDAR AQUI 
       
       IF wvia = "V" THEN DO:
          DISPLAY wdet. DOWN 1.
          END.

        IF wvia = "I"
        OR wvia = "A"
        THEN DO:
          ASSIGN wreg247 = SUBSTRING(wdet,1,247).

          PUT wreg247 SKIP.
          END.  */

        /*...................................fim de grava reg tam 247......*/
        /*..................................fim de grava PA00 da inclusao...*/


        /*..................................inicio grava AP00 da inclusao...*/
        ASSIGN wdet 
        = "AP00"                                        

        + STRING(wultped,"99999999")                     /* num.do pedido    */
     
        + SUBSTRING(STRING(wultord,"999999,99"),1,6)     /* num. da ordem    */
        + SUBSTRING(STRING(wultord,"999999,99"),8,2)

        + STRING(tt_parc.parcela,"99999")                /* num.da parcela   */
        + STRING(wcondpag,"999")                         /* cod.cond.pgto    */
        + STRING(TODAY,"99999999")                       /* data da alt.     */
        + STRING(tt_parc.data-entrega,"99999999")        /* data entrega     */

        + STRING(TIME,"HH:MM:SS")                        /* hora da alt.     */
        + STRING(emitente.nome-abrev,"x(12)")            /* nome abrev       */
        + STRING(tt_parc.obs,"x(76)")                    /* obs              */

                                                         /* preco do forn    */
        + SUBSTRING(STRING(wprecoforn,"99999999999.99999"),01,11)
        + SUBSTRING(STRING(wprecoforn,"99999999999.99999"),13,5)


        + STRING(INTE(tt_parc.quantidade),"9999999")     /* qtde             */
        + "0000"

        + STRING(wusr,"x(12)").                          /* usuario          */

        /*...................................inicio grava reg tam 179......*/
        IF wvia = "V" THEN DO:
          DISPLAY wdet. DOWN 1.
          END.

        IF wvia = "I"
        OR wvia = "A"
        THEN DO:
          ASSIGN wreg179 = SUBSTRING(wdet,1,179).

          PUT wreg179 SKIP.
          END.
        /*...................................fim de grava reg tam 179......*/
        /*..................................fim de grava AP00 da inclusao...*/

        END.
      /*..................................fim de grava inclusao...........*/



      /*..........................inicio grava alteracao..................*/
      IF tt_parc.codreg = "AP00"
      THEN DO:
      
        /*..................................inicio grava AP00 da alteracao..*/
        ASSIGN wdet 
        = "AP00"                                        

        + STRING(wultped,"99999999")                     /* num.do pedido    */
     
        + SUBSTRING(STRING(wultord,"999999,99"),1,6)     /* num. da ordem    */
        + SUBSTRING(STRING(wultord,"999999,99"),8,2)

        + STRING(tt_parc.parcela,"99999")                /* num.da parcela   */
        + STRING(wcondpag,"999")                         /* cod.cond.pgto    */
        + STRING(TODAY,"99999999")                       /* data da alt.     */
        + STRING(tt_parc.data-entant,"99999999")         /* data entrega     */

        + STRING(TIME,"HH:MM:SS")                        /* hora da alt.     */
        + STRING(emitente.nome-abrev,"x(12)")            /* nome abrev       */
        + STRING(tt_parc.obs,"x(76)")                    /* obs              */

                                                         /* preco do forn    */
        + SUBSTRING(STRING(wprecoforn,"99999999999.99999"),01,11)
        + SUBSTRING(STRING(wprecoforn,"99999999999.99999"),13,5)


        + STRING(INTE(tt_parc.quantant),"9999999")       /* qtde             */
        + "0000"

        + STRING(wusr,"x(12)").                          /* usuario          */

        /*...................................inicio grava reg tam 179......*/
        IF wvia = "V" THEN DO:
          DISPLAY wdet. DOWN 1.
          END.

        IF wvia = "I"
        OR wvia = "A"
        THEN DO:
          ASSIGN wreg179 = SUBSTRING(wdet,1,179).

          PUT wreg179 SKIP.
          END.
        /*...................................fim de grava reg tam 179......*/
        /*..................................fim de grava AP00 da alteracao..*/

        END.
      /*..........................fim de grava alteracao..................*/


      END.
    /*......................................fim de inclusao e alteracao..*/

    END.

  
  /*..................................inicio grava ORnn...............*/
  IF wor03 = 1 
  THEN 
  DO WITH FRAME f-lh:
  
    FIND  ordem-compra
    WHERE ordem-compra.numero-ordem = wultord
    NO-LOCK NO-ERROR.
    IF NOT AVAILABLE ordem-compra
    THEN DO:
      ASSIGN wnumreg = wnumreg + 1.
      CREATE tt_msg.
      ASSIGN 
      tt_msg.numreg = wnumreg
      tt_msg.linha = "*?* odem "
                   + STRING(wultord)
                   + " nao encontrada".
      NEXT.                      
      END.
    ASSIGN wnarrat6 = SUBSTRING(ordem-compra.narrativa,1,6).
    IF STRING(lo-matplano.nro-docto,"999999") = wnarrat6 THEN DO:
      NEXT.
      END.  
     
    ASSIGN wdet 
    = "OR"                                             /* codigo reg.      */
    + "03"                                             /* seq.03 a 07      */

    + SUBSTRING(STRING(wultord,"999999,99"),1,6)       /* num. da ordem    */
    + SUBSTRING(STRING(wultord,"999999,99"),8,2)

    + STRING(wultped,"99999999")                       /* num.do pedido    */


    + STRING(lo-matplano.nro-docto,"999999")          /* comentario c/NF */
    + FILL(" ",70).
  
    /*...................................inicio grava reg tam 96.......*/
    IF wvia = "V" THEN DO:
      DISPLAY wdet. DOWN 1.
      END.
  
    IF wvia = "I"
    OR wvia = "A"
    THEN DO:
      ASSIGN wreg96 = SUBSTRING(wdet,1,96).

      PUT wreg96 SKIP.
      END.
    /*...................................fim de grava reg tam 96.......*/
  
    END.
  /*..................................fim de grava ORnn...............*/
  

  RETURN.
  END PROCEDURE.
/*................................................fim de procedure arquivo...*/


/*................................................inicio procedure mensagem..*/
PROCEDURE PI_Mensagens.

  FOR EACH tt_msg WITH FRAME f-lh:
    ASSIGN wdet = tt_msg.linha.

    /*...........................................inicio grava .........*/
    IF wvia = "V" THEN DO:
      DISPLAY wdet. DOWN 1.
      END.

    IF wvia = "I"
    OR wvia = "A"
    THEN DO:
      PUT wdet SKIP.
      END.
    /*...........................................fim de grava .........*/

    END.

  RETURN.
  END PROCEDURE.
/*................................................fim de procedure mensagem..*/



/*................................................inicio procedure parcelas..*/
PROCEDURE PI_Parcelas.
  /*.................................inicio determina ultimo pedido e ordem..*/

  ASSIGN wtotpedid   = 0
         wtotordem   = 0
         wnatped     = 0
         wcondpag    = 0
         wprecoforn  = 0
         wultped     = 0
         wultord     = 0
         wultimaparc = 0
         wqtalt      = 0
         wqtele      = 0.



  FOR
  EACH  pedido-compr
  WHERE pedido-compr.cod-emitente = lo-matplano.cod-emitente
  AND   LOOKUP(STRING(pedido-compr.natureza),wnapelista) GT 0
  AND   LOOKUP(STRING(pedido-compr.situacao),wsipelista) GT 0
  NO-LOCK
  USE-INDEX emitente
  WITH FRAME f-lh:



    FIND 
    LAST  ordem-compra
    WHERE ordem-compra.it-codigo    = lo-matplano.it-codigo
    AND   ordem-compra.cod-emitente = emitente.cod-emitente
    AND   ordem-compra.num-pedido   = pedido-compr.num-pedido
    AND   LOOKUP(STRING(ordem-compra.situacao),wsitulista) GT 0
    NO-LOCK
    USE-INDEX item
    NO-ERROR.
    IF AVAILABLE ordem-compra THEN DO:
      ASSIGN
      wtotordem  = wtotordem + 1
      wtotpedid  = wtotpedid + 1
      wnatped    = pedido-compr.natureza
      wcondpag   = pedido-compr.cod-cond-pag
      wprecoforn = ordem-compra.preco-fornec
      wultped    = pedido-compr.num-pedido  
      wultord    = ordem-compra.numero-ordem.

      FIND
      LAST  prazo-compra
      WHERE prazo-compra.it-codigo    = ordem-compra.it-codigo
      AND   prazo-compra.numero-ordem = wultord
      NO-LOCK
      USE-INDEX item-ordem.
      IF AVAILABLE prazo-compra THEN DO:
        ASSIGN wultimaparc = prazo-compra.parcela.
        END.

      END.
    END.
  /*.................................fim de determina ultimo pedido e ordem..*/


  /*.........................................inicio limpa tt_parc...........*/
  FOR EACH tt_parc:
    DELETE tt_parc.
    END.
  /*.........................................inicio limpa tt_parc...........*/


  /*.............................inicio determina parcelas a alterar........*/
  IF wultord NE 0
  THEN
  FOR 
  EACH  prazo-compra
  WHERE prazo-compra.it-codigo = lo-matplano.it-codigo
  AND   prazo-compra.numero-ordem = wultord
  AND   prazo-compra.data-entrega GE wdtiniplano
  AND   prazo-compra.situacao    NE 4                     
  AND   prazo-compra.situacao    NE 6                     
  NO-LOCK
  USE-INDEX item-ordem
  WITH FRAME f-lh:

    CREATE tt_parc.
    ASSIGN tt_parc.parcela = prazo-compra.parcela
           tt_parc.codreg  = "AP00"
           tt_parc.data-entant  = prazo-compra.data-entrega
           tt_parc.quantant     = prazo-compra.quantidade
           tt_parc.data-entrega = prazo-compra.data-entrega
           tt_parc.quantidade   = prazo-compra.quantidade
           tt_parc.obs          = ""
           tt_parc.utilizado    = 0.
    ASSIGN wqtalt = wqtalt + 1.
    END.       
  /*.............................fim de determina parcelas a alterar........*/

  

  /*.......................................inicio monta todas as parcelas...*/
  DO wele = 1 TO 12 WITH FRAME f-lh:
    IF wtbdata[wele] = ? THEN NEXT.
    IF wtbqtde[wele] = 0 THEN NEXT.


    ASSIGN wqtele = wqtele + 1.
    
    ASSIGN wquant = wtbqtde[wele]
           wxdata = wtbdata[wele].
           
    IF wqtele GT wqtalt
    THEN DO:
      ASSIGN wultimaparc = wultimaparc + 1.
      CREATE tt_parc.
      ASSIGN tt_parc.parcela      = wultimaparc
             tt_parc.codreg       = "PA00"
             tt_parc.data-entrega = wxdata
             tt_parc.quantidade   = wquant
             tt_parc.obs          = "[I]"
                                  + "|"
                                  + STRING(wmmaaaax)
                                  + "|"
                                  + STRING(wprecoforn)
                                  + "|"
                                  + STRING(tt_parc.quantidade)
                                  + "|"
                                  + STRING(tt_parc.data-entrega,"99/99/9999")
                                  + "|"
                                  + STRING(lo-matplano.nro-docto,"999999")
                                  + "|"
                                  + STRING(wusr)
             tt_parc.utilizado    = 1.
      NEXT.
      END.

    IF wqtele = 1 THEN DO:
      FIND FIRST tt_parc.
      END.
    IF wqtele GT 1 THEN DO:
      FIND NEXT tt_parc.
      END.

    IF  tt_parc.data-entrega = wxdata
    AND tt_parc.quantidade   = wquant THEN DO:
      ASSIGN tt_parc.obs       = "mantida"
             tt_parc.codreg    = ""
             tt_parc.utilizado = 1.
      NEXT.
      END.

    /*.................................inicio alterar data ou quantidade...*/
    IF (tt_parc.data-entrega NE wxdata OR
        tt_parc.quantidade   NE wquant) 
    THEN DO:
      ASSIGN tt_parc.data-entrega = wxdata
             tt_parc.quantidade   = wquant
             tt_parc.obs          = "[A]"
                                  + "|"
                                  + STRING(wmmaaaax)
                                  + "|"
                                  + STRING(wprecoforn)
                                  + "|"
                                  + STRING(tt_parc.quantidade)
                                  + "|"
                                  + STRING(tt_parc.data-entrega,"99/99/9999")
                                  + "|"
                                  + STRING(lo-matplano.nro-docto,"999999")
                                  + "|"
                                  + STRING(wusr)
             tt_parc.utilizado    = 1. 
      NEXT.
      END.
    /*.................................fim de alterar data ou quantidade...*/


    END.
  /*.......................................fim de monta todas as parcelas...*/
  

  /*......................................inicio zerar nao utilizadas.......*/
  FOR 
  EACH  tt_parc
  WHERE tt_parc.utilizado = 0:
    ASSIGN tt_parc.quantidade = 0
           tt_parc.obs          = "[A]"
                                + "|"
                                + STRING(wmmaaaax)
                                + "|"
                                + STRING(wprecoforn)
                                + "|"
                                + STRING(tt_parc.quantidade)
                                + "|"
                                + STRING(tt_parc.data-entrega,"99/99/9999")
                                + "|"
                                + STRING(lo-matplano.nro-docto,"999999")
                                + "|"
                                + STRING(wusr)
           tt_parc.utilizado  = 1.
    END.
  /*......................................fim de zerar nao utilizadas.......*/
    

  RETURN.
  END PROCEDURE.
/*................................................fim de procedure parcelas..*/

