&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0215RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wanomes          AS CHAR FORMAT "x(06)"
    FIELD wfami            AS CHAR FORMAT "x(08)"
    FIELD wfamf            AS CHAR FORMAT "x(08)"
    FIELD wite             AS CHAR FORMAT "x(16)"
    FIELD wdr              AS CHAR FORMAT "x(01)"
    FIELD wnr-linha-ini    AS INTE FORMAT ">>9"
    FIELD wnr-linha-fim    AS INTE FORMAT ">>9".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 90 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 90 down stream-io frame f-relat.


/* DEFINI��ES DO BENE */ 

DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".


DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    
DEFINE VARIABLE wctditem    AS INTE.
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(110)".
DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(110)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".

DEFINE VARIABLE wlitfam     AS CHAR FORMAT "X(8)".

DEFINE VARIABLE wfami       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wfamf       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wite        LIKE mgind.item.it-codigo.
DEFINE VARIABLE wanomes     LIKE mgfas.lo-plames.anomes.

DEFINE VARIABLE wcab0       AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wcab1       AS CHAR FORMAT "x(78)".

DEFINE VARIABLE wa       AS INTEGER   FORMAT "9999"     INITIAL 0.         
DEFINE VARIABLE wm       AS INTEGER   FORMAT "99"       INITIAL 0.         
DEFINE VARIABLE wmex     AS CHARACTER FORMAT "x(7)"  EXTENT 4  INITIAL "".
DEFINE VARIABLE wmmm     AS CHARACTER FORMAT "x(3)"  EXTENT 12
    INITIAL ["jan","fev","mar","abr","mai","jun",
             "jul","ago","set","out","nov","dez"].


DEFINE VARIABLE wtotdet     LIKE mgfas.lo-plames.pl-przir.
DEFINE VARIABLE wtotfam     LIKE mgfas.lo-plames.pl-przir.
DEFINE VARIABLE wtotger     LIKE mgfas.lo-plames.pl-przir.

DEFINE VARIABLE wctd        AS INTEGER.
DEFINE VARIABLE wlit        AS CHARACTER FORMAT "x(110)".
DEFINE VARIABLE wlit0       AS CHARACTER FORMAT "x(110)".
DEFINE VARIABLE wlit1       AS CHARACTER FORMAT "x(110)".
DEFINE VARIABLE wdesc1      AS CHARACTER FORMAT "x(29)".
DEFINE VARIABLE wdesc2      AS CHARACTER FORMAT "x(36)".

DEFINE VARIABLE wdr         AS CHAR FORMAT "!".

DEFINE VARIABLE wlinha-fam  AS INTE FORMAT ">>9".

FORM SKIP(03) " " COLON 19 wstart SKIP(11) " "
     WITH NO-LABELS ROW 04 WIDTH 80 FRAME f-box4.


FORM HEADER 
    " " WITH FRAME f-cabec  WIDTH 90.


create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Plano Mensal do Item".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */


do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


PROCEDURE roda_prog.
 
 
DEFINE VARIABLE wctdrepeat  AS INTE.
ASSIGN wctdrepeat = 0.

  ASSIGN wctdrepeat = wctdrepeat + 1.

  ASSIGN wtotdet = 0
         wtotfam = 0
         wtotger = 0
         wlinha-fam = 0.
                 
wa = INTEGER(SUBSTRING(tt-param.wanomes,1,4)).                                      
  wm = INTEGER(SUBSTRING(tt-param.wanomes,5,2)).                                        
  wm = wm - 1.
  IF wm = 0 THEN DO:                                     
    wm = 12.                                                             
    wa = wa - 1.                                                   
    END.                                                                   
  DO wctd = 1 to 4:                                                   
    wmex[wctd] = wmmm[wm] + STRING(wa).                                
    wm = wm + 1.                                                    
    IF wm = 13 THEN DO:                                                  
      wm = 1.                                                           
      wa = wa + 1.                                                  
      END.                                                                 
    END.
  ASSIGN wmex[1] = wmex[2].

  ASSIGN wcab0 = FILL(" ",29)
               + "Faturar  "
               + " -----Produzir------".

  ASSIGN wcab1 = FILL(" ",29)
               + wmex[01]      
         + " " + wmex[02]
         + " " + wmex[03]
         + " " + wmex[04]
         + " " + string("  Linha Prod").

ASSIGN wmsg = " Plano: "  + tt-param.wanomes + " / Fam�lia: " + tt-param.wfami + " - " + tt-param.wfamf + " / Item: " + tt-param.wite + " / Nivel: " + tt-param.wdr
       + " / Linha Prod.: " + string(tt-param.wnr-linha-ini) + " a " +  string(tt-param.wnr-linha-fim).                                                                                
 
 FORM HEADER 
    SKIP(1) wmsg
    SKIP(1) wcab0
    SKIP    wcab1
    WITH FRAME f-cabec1 WIDTH 120 PAGE-TOP NO-LABELS.

   VIEW FRAME f-cabec1.
    /*.......................INICIO TRATAMENTO DE FAMILIA................*/
    FOR EACH mgdis.fam-comerc
       WHERE mgdis.fam-comerc.fm-cod-com GE tt-param.wfami
       AND   mgdis.fam-comerc.fm-cod-com LE tt-param.wfamf
       NO-LOCK:

       run pi-acompanhar in h-acomp (input "Familia: " +  fam-comerc.fm-cod-com).

    

    /*.....................INICIO TRATAMENTO DE ITEM..................*/
      FOR EACH mgind.item
         WHERE mgind.item.fm-cod-com = mgdis.fam-comerc.fm-cod-com
         AND   mgind.item.it-codigo  MATCHES(tt-param.wite)        
         AND   mgind.item.cod-obsoleto <> 4
         NO-LOCK:


        /* 07/08/18 - TRATAMENTO NO. LINHA - SOL. FABIO */ 
        FIND FIRST  item-uni-estab WHERE item-uni-estab.it-codigo =  ITEM.it-codigo
                           AND   item-uni-estab.cod-estabel = "1"
                           AND   (item-uni-estab.nr-linha GE  tt-param.wnr-linha-ini
                           AND item-uni-estab.nr-linha LE  tt-param.wnr-linha-fim)
                           NO-LOCK NO-ERROR.                                                          
    
         IF NOT AVAILABLE item-uni-estab  THEN NEXT.
 
      FOR EACH mgfas.lo-plames
           WHERE mgfas.lo-plames.it-codigo = mgind.item.it-codigo
           AND   mgfas.lo-plames.anomes    = tt-param.wanomes
           NO-LOCK:

          DO wctd = 1 TO 4:
            ASSIGN
            wtotdet[wctd] = wtotdet[wctd] + mgfas.lo-plames.pl-przir[wctd]
            wtotfam[wctd] = wtotfam[wctd] + mgfas.lo-plames.pl-przir[wctd]
            wtotger[wctd] = wtotger[wctd] + mgfas.lo-plames.pl-przir[wctd].
            END.
          END.

        IF tt-param.wdr = "D" THEN DO:
          ASSIGN wdesc1 = STRING(mgind.item.it-codigo,"x(13)")
                  + " " + STRING(mgind.item.descricao-1,"x(15)").
          ASSIGN wdesc2 = FILL(" ",36).
          ASSIGN wlit1 = wdesc1 + STRING(wtotdet[1],"zzzzzz9-").
          DO wctd = 2 TO 4:
            ASSIGN wlit1 = wlit1 + STRING(wtotdet[wctd],"zzzzzz9-") + " ".
            END.
          DO wctd = 1 TO 4:
            IF wtotdet[wctd] <> 0 THEN DO:
              ASSIGN wdet = wlit0.
             DISPLAY wdet NO-LABEL WITH STREAM-IO.
              DOWN.
              ASSIGN wdet = wlit1.
              /* 07/08/18 - no. linha - sol. fabio - 07/08/18 */
/*               ASSIGN wlinha-fam =  item-uni-estab.nr-linha. */
/*               ASSIGN wdet = wdet + string(wlinha-fam).      */
               IF AVAILABLE item-uni-estab THEN
               ASSIGN wdet = wdet + STRING(item-uni-estab.nr-linha).
              /* 07/08/18 fim */

              DISPLAY wdet NO-LABEL WITH STREAM-IO WIDTH 120.
              DOWN.
              ASSIGN wtotdet = 0.
              LEAVE.
              END.
            END.
          END. 
        END.
      IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN LEAVE.
      /*.....................FIM DE TRATAMENTO DE ITEM...................*/
  
      ASSIGN wdesc1 = "FAMILIA "
                    + STRING(mgdis.fam-comerc.fm-cod-com,"x(8)")
                    + " TOTAL======>".
      ASSIGN wdesc2 = STRING(mgdis.fam-comerc.descricao,"x(20)")
                    + FILL(" ",16).
      ASSIGN wlit1 = wdesc1 + STRING(wtotfam[1],"zzzzzz9-").
      DO wctd = 2 TO 4:
        ASSIGN wlit1 = wlit1 + STRING(wtotfam[wctd],"zzzzzz9-") + " ".
        END.
   
     DO wctd = 1 TO 4:
        IF wtotfam[wctd] <> 0 THEN DO:
          ASSIGN wdet = wlit1.
/*              /* 07/08/18 - no. linha - sol. fabio - 07/08/18 */ */
/*              ASSIGN wdet = wdet + STRING(wlinha-fam).           */
/*               /* 07/08/18 fim */                                */
          DISPLAY wdet WITH FRAME f-totf NO-LABEL  WITH STREAM-IO WIDTH 120.
          DOWN WITH FRAME f-totf .
          ASSIGN wdet = FILL("-",78).
          DISPLAY wdet WITH FRAME f-totf NO-LABEL  WITH STREAM-IO.
          ASSIGN wtotfam = 0.
          LEAVE.
          END.
        END.
      END.
    /*.......................FIM DE TRATAMENTO DE FAMILIA................*/
    


    /*................................inicio total geral...*/  
    ASSIGN wlit1 = " TOTAL DA CONSULTA==========>"
                 + STRING(wtotger[01],"zzzzzz9-").
    DO wctd = 2 TO 4:
      ASSIGN wlit1 = wlit1 + STRING(wtotger[wctd],"zzzzzz9-") + " ".
      END.
    
    DO wctd = 1 TO 4:
     IF wtotger[wctd] <> 0 THEN DO:
        ASSIGN wdet = wlit0.
        DISPLAY wdet NO-LABEL  WITH STREAM-IO.
        DOWN.
        ASSIGN wdet = wlit1.
        DISPLAY wdet NO-LABEL  WITH STREAM-IO WIDTH 120.
        DOWN.
        ASSIGN wtotger = 0.
        LEAVE.
        END.
      END.

    IF  wbdd = wbdd-integri
    THEN DO:
      ASSIGN wdet = "".
      DISPLAY wdet NO-LABEL  WITH STREAM-IO. DOWN 1.
      ASSIGN wdet = "       F I M   D A   C O N S U L T A         ".
      DISPLAY wdet NO-LABEL  WITH STREAM-IO WIDTH 120. DOWN 1.
      END.
   /*................................fim do total geral...*/  


    END.
/*
 END.*/




