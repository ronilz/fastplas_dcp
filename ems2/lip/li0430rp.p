                                                                                                                                                                           &ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0430RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define NEW SHARED temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wfami            AS CHAR FORMAT "x(08)"
    FIELD wfamf            AS CHAR FORMAT "x(08)"
    FIELD wite             AS CHAR FORMAT "x(16)"
    FIELD wcli             AS CHAR FORMAT "x(12)"
    FIELD wctrle           AS CHAR FORMAT "x(10)"
    FIELD wqualpgm         AS CHAR FORMAT "x(10)"
    FIELD wdtpgm           AS DATE FORMAT "99/99/9999".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* DEFINI��ES DO BENE */ 
DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li0430".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "       Exibe PGM�s por Familia        ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wvia       AS CHAR FORMAT "!".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)".                    

DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    


DEFINE VARIABLE wqualpgm    AS CHAR FORMAT "x(10)".
DEFINE VARIABLE wdtpgm      LIKE mgfas.lo-clipgm.data-pgm.
DEFINE VARIABLE wdata-pgm   LIKE mgfas.lo-clipgm.data-pgm.
DEFINE VARIABLE wcod-pgm    LIKE mgfas.lo-clipgm.cod-pgm.
DEFINE VARIABLE wtipo-pgm   LIKE mgfas.lo-clipgm.tipo-pgm.

DEFINE VARIABLE wcodrefaux  AS CHAR FORMAT "x(20)".
DEFINE VARIABLE wctd1       AS INTE.
DEFINE VARIABLE wposrefaux  AS INTE.

DEFINE VARIABLE wputscreen  AS CHARACTER FORMAT "x(22)".
DEFINE VARIABLE wdescfam    AS CHARACTER FORMAT "x(24)".
DEFINE VARIABLE wdescitem   AS CHARACTER FORMAT "x(13)".
DEFINE VARIABLE wfam        LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wfami       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wfamf       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wite        LIKE mgind.item.it-codigo.
DEFINE VARIABLE wcli        AS CHAR FORMAT "X(12)".        
DEFINE VARIABLE wdtdd       AS DATE.

DEFINE VARIABLE whide       AS INTE FORMAT "9".
DEFINE VARIABLE wctd        AS INTE FORMAT "99".
DEFINE VARIABLE wctq        AS INTE FORMAT "99".
DEFINE VARIABLE wctr        AS CHAR FORMAT "x(20)".
DEFINE VARIABLE wctrle      AS CHAR FORMAT "!".

DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(100)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(100)".

DEFINE VARIABLE wlinha1     AS CHAR FORMAT "X(78)".

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe PGM�s por Fam�lia".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
        ASSIGN wmsg = "Fam�lia: "  + STRING(tt-param.wfami) 
   + "-"
   + STRING(tt-param.wfamf)
   + " / Item: "      + tt-param.wite
   + " / Cliente: "   + tt-param.wcli
   + " / Controle: "  + tt-param.wctrle
   + " / Periodo: "   + tt-param.wqualpgm
   + " / Data: "      + STRING(tt-param.wdtpgm,"99/99/9999").
   
    view frame f-cabec.

    view frame f-rodape.    

    run utp/ut-acomp.p persistent set h-acomp.  

  {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}

  run pi-inicializar in h-acomp (input "Gerando":U). 

  /*:T --- Colocar aqui o c�digo de impress�o --- */

  RUN roda_prog.
 /*
      run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
   */

  run pi-finalizar in h-acomp.
  {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/************ PROCEDURE PARA RODAR RELAT�RIO DO BENE *********************/
    
PROCEDURE roda_prog.
 
   /*.................................................fim de entrada padrao..*/

   ASSIGN wdtdd   = TODAY
          wlinha1 = FILL("_",78).
   FORM HEADER
       SKIP(1) wmsg
       SKIP(1)             
       SKIP " "                                                 
       SKIP wdescfam "      CLIENTE ......PGM..... FQ  N.F.   DATA     QTDE"
       SKIP
       wlinha1
       WITH DOWN NO-LABEL FRAME F-CABEC.

       FOR EACH  mgdis.fam-comerc
           WHERE mgdis.fam-comerc.fm-cod-com GE tt-param.wfami
           AND   mgdis.fam-comerc.fm-cod-com LE tt-param.wfamf NO-LOCK:

        
         wdescfam = mgdis.fam-comerc.fm-cod-com + " "
                  + STRING(mgdis.fam-comerc.descricao,"x(20)").
         FOR EACH      mgind.item     
             WHERE     mgind.item.fm-cod-com = mgdis.fam-comerc.fm-cod-com
             AND       mgind.item.cod-obsoleto <> 4
             AND       mgind.item.it-codigo MATCHES(tt-param.wite) NO-LOCK:

       /*...............................inicio montagem COD REFERENCIA s/mascara....*/
           ASSIGN wcodrefaux = ""
                  wposrefaux = 0.
           DO wctd1 = 1 TO 20:
             IF  (SUBSTRING(item.codigo-refer,wctd1,1) NE " ")
             AND (SUBSTRING(item.codigo-refer,wctd1,1) NE ".") THEN DO:
               ASSIGN wcodrefaux = wcodrefaux
                                 + SUBSTRING(mgind.item.codigo-refer,wctd1,1).
               ASSIGN wposrefaux = wposrefaux + 1.
               END.
             END. 

         /*...............................fim da montagem COD REFERENCIA s/mascara....*/

          IF tt-param.wqualpgm = "�ltimo" THEN DO:    /*...ultimo programa...*/
             FIND LAST      mgfas.lo-clipgm 
                  WHERE     mgfas.lo-clipgm.codrefsbp = wcodrefaux              
                  AND       mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli) 
                  USE-INDEX ctrnec
                  NO-LOCK
                  NO-ERROR. 
              
           END.
           IF tt-param.wqualpgm = "Data" THEN DO:   /*...programa com data fornecida...*/
             FIND FIRST     mgfas.lo-clipgm
                  WHERE     mgfas.lo-clipgm.codrefsbp  = wcodrefaux            
                  AND       mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli) 
                  AND       mgfas.lo-clipgm.data-pgm  GE tt-param.wdtpgm
                  USE-INDEX ctrnec
                  NO-LOCK
                  NO-ERROR.
     
             IF AVAILABLE mgfas.lo-clipgm THEN DO:
               ASSIGN wdata-pgm = mgfas.lo-clipgm.data-pgm.
               FIND LAST      mgfas.lo-clipgm
                    WHERE     mgfas.lo-clipgm.codrefsbp  = wcodrefaux            
                    AND       mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli) 
                    AND       mgfas.lo-clipgm.data-pgm   = wdata-pgm
                    USE-INDEX ctrnec
                    NO-LOCK
                    NO-ERROR.
            
             END.
           END.

           IF AVAILABLE mgfas.lo-clipgm THEN DO:
             ASSIGN wdata-pgm = mgfas.lo-clipgm.data-pgm
                    wcod-pgm  = mgfas.lo-clipgm.cod-pgm
                    wtipo-pgm = mgfas.lo-clipgm.tipo-pgm.

             FOR EACH mgfas.lo-clipgm
                WHERE mgfas.lo-clipgm.codrefsbp = wcodrefaux             
                AND   mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli)
                AND   mgfas.lo-clipgm.data-pgm  = wdata-pgm
                AND   mgfas.lo-clipgm.cod-pgm   = wcod-pgm         
                AND   mgfas.lo-clipgm.tipo-pgm  = wtipo-pgm
                USE-INDEX ctrnec  
                NO-LOCK:

   /*----------------------------EXIBICAO DA LINHA PADRAO------------------------*/
  

               wdescitem = STRING(mgind.item.descricao-1,"x(13)").
               IF tt-param.wctrle = "Item" THEN wctr = mgind.item.it-codigo.
               IF tt-param.wctrle = "Descri��o" THEN DO:
                 wctr = STRING(mgind.item.descricao-1).
                 wdescitem = mgind.item.it-codigo.
                 END.
               IF tt-param.wctrle = "C�digo" THEN DO:
                 wctr      = mgind.item.codigo-refer.
                 wdescitem = mgind.item.it-codigo.
                 END.
               IF tt-param.wctrle = "Pe�a" THEN wctr = mgind.item.codigo-refer.
      
               ASSIGN wdet = STRING(wctr,"x(17)")
                           + STRING(wdescitem,"x(13)")           
                           + " "
                           + STRING(mgfas.lo-clipgm.nome-abrev,"x(07)") + " "
                           + STRING(mgfas.lo-clipgm.data-pgm,"999999")  + " "
                           + STRING(mgfas.lo-clipgm.cod-pgm,"x(06)")          
                           + STRING(mgfas.lo-clipgm.tipo-pgm,"!")
                           + STRING(mgfas.lo-clipgm.frequencia,"zz9") + " "  
                           + STRING(mgfas.lo-clipgm.nf,">>>>>>9")       + " "
                           + STRING(mgfas.lo-clipgm.data,"99/99/9999")  
                           + STRING(mgfas.lo-clipgm.qtde,">>>>9").
               DISPLAY wdet WITH STREAM-IO NO-LABEL WIDTH 150.
               DOWN 1.
             END.
             END.          
         END.
       END.                  
   /*----------------------------FINALIZA LOOP DOS PRODUTOS DA fam-comerc----*/
       
  END.
