&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI1340RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
     FIELD wordi            AS INTEGER FORMAT ">>>,>>>,>>9"
    FIELD wordf            AS INTEGER FORMAT ">>>,>>>,>>9"
    FIELD wdatai           AS DATE FORMAT "99/99/9999"
    FIELD wdataf           AS DATE FORMAT "99/99/9999".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 150 down stream-io frame f-relat.

/* VARIAVEIS BENE */
/***********************************************************************
**
** li1340.p  - Exibe Reservas                
**                
**              10/04/2003 -  BENE   Programa novo.
**
***********************************************************************/

DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li1340".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "            Exibe Reservas            ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wvia       AS CHAR FORMAT "!".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)".                    

DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    
DEFINE VARIABLE wctditem    AS INTE.
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(130)".
DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".

/* DEFINE VARIABLE wordi    LIKE reservas.nr-ord-produ. */
/* DEFINE VARIABLE wordf    LIKE reservas.nr-ord-produ. */
/*                                                      */
/* DEFINE VARIABLE wdatai  AS DATE FORMAT "99/99/9999". */
/* DEFINE VARIABLE wdataf  AS DATE FORMAT "99/99/9999". */

DEFINE VARIABLE wctd1         AS   INTE.
DEFINE VARIABLE wtotfat       AS   DECI FORMAT ">>>>>>>9.9999".
DEFINE VARIABLE wreceber      AS   DECI FORMAT "->>>>>>>9.9999".
DEFINE VARIABLE wenviar       AS   DECI FORMAT "->>>>>>>9.9999".
DEFINE VARIABLE wsaldo        AS   DECI FORMAT "->>>>>>>9.9999".
DEFINE VARIABLE wquant        AS   DECI FORMAT "->>>>>>>9.9999".

DEFINE VARIABLE wlit15        AS   CHAR FORMAT "x(15)".
DEFINE VARIABLE wxfaturado    AS   CHAR FORMAT "x(13)".
DEFINE VARIABLE wxretorno     AS   CHAR FORMAT "x(14)".


DEFINE VARIABLE wnr-ord-produ  LIKE reservas.nr-ord-produ.
DEFINE VARIABLE witem-pai      LIKE reservas.item-pai.      



/* FIM VARIAVEIS BENE */


create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Reservas".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */




PROCEDURE roda_prog.
FORM HEADER 
"Ordem: " tt-param.wordi "-" tt-param.wordf "Data: " wdatai "-" wdataf
SKIP(1)
"    Ordem  Item_pai           Qtde da       Qtde"
SKIP
"Data       Item_Reserva       Reserva       Atendida     Qtde       Documento"
  WITH FRAME f-cabec PAGE-TOP.
 
    FOR
EACH  reservas
WHERE reservas.nr-ord-produ GE tt-param.wordi
AND   reservas.nr-ord-produ LE tt-param.wordf
AND   reservas.dt-reserva GE tt-param.wdatai
AND   reservas.dt-reserva LE tt-param.wdataf
NO-LOCK:

run pi-acompanhar in h-acomp (input "Orden: " + string(reservas.nr-ord-produ)).


FIND  ord-prod
WHERE ord-prod.nr-ord-produ = reservas.nr-ord-produ
NO-LOCK NO-ERROR.
IF NOT AVAILABLE ord-prod THEN NEXT.

/* ALTERADO CONF.SOL. ROSA 02/05/07 - PARA NAO LISTAR
ORDENS TERMINADAS E FINALIZADAS */
IF ord-prod.estado =  7
OR  ord-prod.estado = 8 THEN NEXT.

IF ord-prod.tipo NE 2 THEN NEXT.

IF reservas.nr-ord-produ   NE  wnr-ord-produ
OR reservas.item-pai       NE  witem-pai
THEN DO:
ASSIGN wdet = FILL("-",130).
DISPLAY wdet WITH FRAME F-RELAT NO-LABELS.
DOWN WITH FRAME F-RELAT.

ASSIGN wdet = FILL(" ",4)
         + STRING(reservas.nr-ord-produ,">>>>>9")
   + " " + STRING(reservas.item-pai,"x(13)").

DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

ASSIGN wdet = STRING(ord-prod.qt-ordem,"->>>>>>>9.9999")
         + " <= Qtde da Ordem".
DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

ASSIGN wdet = STRING(ord-prod.qt-produzida,"->>>>>>>9.9999")
         + " <= Produzido".
DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

ASSIGN wsaldo = ord-prod.qt-ordem - ord-prod.qt-produzida.
ASSIGN wdet = STRING(wsaldo,"->>>>>>>9.9999")
         + " <= Saldo".
DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

END.

ASSIGN wdet = STRING(reservas.dt-reserva,"99/99/9999")
 + " " + STRING(reservas.it-codigo,"x(13)")
       + STRING(reservas.quant-orig,"->>>>>>>9.9999")
       + STRING(reservas.quant-atend,"->>>>>>>9.9999").

DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

ASSIGN wctd1    = 0
   wtotfat  = 0.

FOR
EACH  saldo-terc 
WHERE saldo-terc.nr-ord-prod   = reservas.nr-ord-produ
AND   saldo-terc.it-codigo     = reservas.it-codigo   
NO-LOCK:

FIND
FIRST it-nota-fisc 
WHERE it-nota-fisc.nr-nota-fis = saldo-terc.nro-docto
AND   it-nota-fisc.it-codigo   = saldo-terc.it-codigo
AND   it-nota-fisc.nr-seq-fat  = saldo-terc.sequencia
NO-LOCK NO-ERROR.

ASSIGN wxfaturado = FILL(" ",13).      

IF AVAILABLE it-nota-fisc THEN DO:
ASSIGN
wxfaturado = STRING(it-nota-fisc.qt-faturada[1],">>>>>>>9.9999")
wtotfat = wtotfat + it-nota-fisc.qt-faturada[1]
wctd1 = wctd1 + 1.
END.           

ASSIGN wdet = FILL(" ",52)
         + STRING(wxfaturado,"x(13)") + " "
         + "   "
         + STRING(INTE(saldo-terc.nro-docto),">>>>>>9").

DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

/*--------------------------------------inicio trata retorno-----*/
FOR
EACH  item-doc-est
WHERE item-doc-est.nro-comp     = saldo-terc.nro-docto
AND   item-doc-est.it-codigo    = saldo-terc.it-codigo
AND   item-doc-est.sequencia    = saldo-terc.sequencia
AND   item-doc-est.nat-operacao = "1999E3"
NO-LOCK :
ASSIGN wquant = item-doc-est.quantidade * -1.

ASSIGN wxretorno = STRING(wquant,">>>>>>>9.9999-")
       wtotfat = wtotfat + wquant.                        

ASSIGN wdet = FILL(" ",52)
           + STRING(wxretorno,"x(14)")
           + " (R"
           + STRING(INTE(item-doc-est.nro-docto),">>>>>>9")
           + ")".

DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.
END.
/*--------------------------------------fim de trata retorno-----*/


END.

ASSIGN wreceber = 0
   wenviar  = 0.
IF wctd1 NE 0 THEN DO:
ASSIGN wreceber = wtotfat - reservas.quant-atend
     wenviar  = reservas.quant-orig - wtotfat.

ASSIGN wlit15 = "  Falta Atender".
IF wreceber LT 0 THEN DO:
ASSIGN wlit15 = "Atendido a mais".
END.

ASSIGN wdet = FILL(" ",24)
         + STRING(wenviar,"->>>>>>>9.9999")
         + STRING(wreceber,"->>>>>>>9.9999")
         + STRING(wtotfat,">>>>>>>9.9999")
         + " <== Total".
DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT.

ASSIGN wdet = FILL(" ",31)
         + "Enviar        "
         + "Receber       ".
DISPLAY wdet WITH FRAME F-RELAT.
DOWN WITH FRAME F-RELAT. 

ASSIGN wdet = "".
DISPLAY wdet WITH FRAME F-RELAT. 
DOWN WITH FRAME F-RELAT.  

END.

ASSIGN wnr-ord-produ = reservas.nr-ord-produ
   witem-pai     = reservas.item-pai.

END.
END PROCEDURE.




