&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0230RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wmmaaaax         AS CHAR FORMAT "X(7)"
    FIELD wite             AS CHAR FORMAT "X(16)"
    FIELD wfami            AS CHAR FORMAT "X(08)"
    FIELD wfamf            AS CHAR FORMAT "X(08)"
    FIELD wnomea           AS CHAR FORMAT "X(12)"
    FIELD westab           AS CHAR FORMAT "X(3)"
    FIELD wpresn           AS LOGICAL FORMAT "Sim/Nao"
    FIELD wzerosn          AS LOGICAL FORMAT "Sim/Nao"
    FIELD wpedsn           AS LOGICAL FORMAT "Sim/Nao"
    FIELD wordsn           AS LOGICAL FORMAT "Sim/Nao"
    FIELD wparsn           AS LOGICAL FORMAT "Sim/Nao"
    FIELD wnapesn-1        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wnapesn-2        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wnapesn-3        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsipesn-1        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsipesn-2        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsipesn-3        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsitusn-1        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsitusn-2        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsitusn-3        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsitusn-4        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsitusn-5        AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsitusn-6        AS LOGICAL FORMAT "Sim/Nao"
    /* parcelas */
    FIELD wsiparsn-1       AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsiparsn-2       AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsiparsn-3       AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsiparsn-4       AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsiparsn-5       AS LOGICAL FORMAT "Sim/Nao"
    FIELD wsiparsn-6       AS LOGICAL FORMAT "Sim/Nao".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 300 down stream-io frame f-relat NO-LABEL.

/* VARIAVEIS BENE */

DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".
DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    
DEFINE VARIABLE wctditem    AS INTE.
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(140)".
DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".

DEFINE VARIABLE wlitfam     AS CHAR FORMAT "X(8)".

/* DEFINE VARIABLE wfami       LIKE fam-comerc.fm-cod-com. */
/* DEFINE VARIABLE wfamf       LIKE fam-comerc.fm-cod-com. */
/* DEFINE VARIABLE wite        LIKE item.it-codigo.        */
/* DEFINE VARIABLE westab      LIKE lo-matplano.estab.     */
/* DEFINE VARIABLE wmmaaaax    AS  CHAR FORMAT "X(7)".     */
/* DEFINE VARIABLE wnomea      LIKE emitente.nome-abrev.   */

DEFINE VARIABLE wbate       AS   INTE.                    
 
DEFINE VARIABLE wctd        AS   INTE.                    
DEFINE VARIABLE wpos        AS   INTE.                    
DEFINE VARIABLE wtitx3      AS   CHAR FORMAT "x(3)".   
DEFINE VARIABLE wtitx6      AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitx9      AS   CHAR FORMAT "x(9)".   



DEFINE VARIABLE wtitlit     AS   CHAR FORMAT "x(100)".         

/*....................................................inicio titulos...*/
DEFINE VARIABLE wtitsem1    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem2    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem3    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem4    AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem5    AS   CHAR FORMAT "x(6)".   

DEFINE VARIABLE wtitsem1-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem2-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem3-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem4-2  AS   CHAR FORMAT "x(6)".   
DEFINE VARIABLE wtitsem5-2  AS   CHAR FORMAT "x(6)".   

DEFINE VARIABLE wtitmes1    AS   CHAR FORMAT "x(3)".   
DEFINE VARIABLE wtitmes2    AS   CHAR FORMAT "x(3)".   
/*....................................................fim de titulos...*/


/*....................................................inicio datas.....*/
DEFINE VARIABLE wxdata     AS   DATE FORMAT "99/99/9999".

DEFINE VARIABLE wdtsem1    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem2    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem3    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem4    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem5    AS   DATE FORMAT "99/99/9999".

DEFINE VARIABLE wdtsem1-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem2-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem3-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem4-2  AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtsem5-2  AS   DATE FORMAT "99/99/9999".

DEFINE VARIABLE wdtmes1    AS   DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdtmes2    AS   DATE FORMAT "99/99/9999".
/*....................................................fim de datas.....*/


/*....................................................inicio literais..*/
DEFINE VARIABLE wlitx10     AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wlitsem1    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem2    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem3    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem4    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem5    AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wlitsem1-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem2-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem3-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem4-2  AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitsem5-2  AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wlitmes1    AS   CHAR FORMAT "x(10)".   
DEFINE VARIABLE wlitmes2    AS   CHAR FORMAT "x(10)".   
/*....................................................fim de literais..*/

DEFINE VARIABLE wquant     AS   INTE FORMAT ">>>>>>9".

DEFINE VARIABLE wdatacorte  AS   CHAR FORMAT "x(10)".   

DEFINE VARIABLE wmes       AS CHAR FORMAT "x(03)" EXTENT 12
                              INITIAL ["JAN","FEV","MAR","ABR","MAI","JUN",
                                       "JUL","AGO","SET","OUT","NOV","DEZ"].
DEFINE VARIABLE wctdmes    AS INTE.
DEFINE VARIABLE wmmm       AS CHAR FORMAT "x(3)".
DEFINE VARIABLE wmm        AS INTE FORMAT "99".
DEFINE VARIABLE wdd        AS INTE FORMAT "99".
DEFINE VARIABLE waaaa      AS INTE FORMAT "9999".
DEFINE VARIABLE wmmplano   AS INTE FORMAT "99". 

DEFINE VARIABLE wultimaparc LIKE prazo-compra.parcela.
DEFINE VARIABLE wtotparc    AS INTE.
DEFINE VARIABLE wtotordem   AS INTE.
DEFINE VARIABLE wtotpedid   AS INTE.

DEFINE VARIABLE wzeroprod   AS INTE.
DEFINE VARIABLE wzeroprev   AS INTE.


/*..................................................inicio tabelas...*/
DEFINE VARIABLE wele        AS INTE.
DEFINE VARIABLE wtbdata     AS DATE FORMAT "99/99/9999" EXTENT 12.
DEFINE VARIABLE wtbqtde     AS INTE FORMAT ">>>>>>9"    EXTENT 12.
/*..................................................fim de tabelas...*/


/* /*..............................................inicio considera..........*/ */
/* DEFINE VARIABLE wpresn  AS   LOGI FORMAT "Sim/Nao" INITIAL no.               */
/* DEFINE VARIABLE wzerosn AS   LOGI FORMAT "Sim/Nao" INITIAL no.               */
/*                                                                              */
/* DEFINE VARIABLE wpedsn  AS   LOGI FORMAT "Sim/Nao" INITIAL no.               */
/* DEFINE VARIABLE wordsn  AS   LOGI FORMAT "Sim/Nao" INITIAL no.               */
/* DEFINE VARIABLE wparsn  AS   LOGI FORMAT "Sim/Nao" INITIAL no.               */

 
/*..............................................inicio considera..........*/


/*........................................inicio natureza do pedido..*/
DEFINE VARIABLE wnapedesc    AS CHAR FORMAT "x(14)"   EXTENT 3
       INITIAL ["Compra        ",
                "Servico       ",
                "Beneficiamento"].
DEFINE VARIABLE wnapenum     AS CHAR FORMAT "9"       EXTENT 3
       INITIAL ["1","2","3"].
/*DEFINE VARIABLE wnapesn      AS LOGI FORMAT "Sim/Nao" EXTENT 3       
       INITIAL [yes,yes,yes].*/
DEFINE VARIABLE wnapelista   AS CHAR.
DEFINE VARIABLE wnapepos     AS INTE.

/*........................................fim de natureza do pedido..*/


/*........................................inicio situacao do pedido..*/
DEFINE VARIABLE wsipedesc    AS CHAR FORMAT "x(12)"   EXTENT 3
       INITIAL ["Impresso    ",
                "Nao Impresso",
                "Eliminado   "].
DEFINE VARIABLE wsipenum     AS CHAR FORMAT "9"       EXTENT 3
       INITIAL ["1","2","3"].
/*DEFINE VARIABLE wsipesn      AS LOGI FORMAT "Sim/Nao" EXTENT 3       
       INITIAL [yes,yes,yes]. */
DEFINE VARIABLE wsipelista   AS CHAR.
DEFINE VARIABLE wsipepos     AS INTE.
      
 
/*........................................fim de situacao do pedido..*/


/*.......................................inicio situacao da ordem....*/
DEFINE VARIABLE wsitudesc    AS CHAR FORMAT "x(14)"   EXTENT 6
       INITIAL ["Nao Confirmada",
                "Confirmada    ",
                "Cotada        ",
                "Eliminada     ",
                "Em cotacao    ",
                "Terminada     "].
DEFINE VARIABLE wsitunum     AS CHAR FORMAT "9"       EXTENT 6
       INITIAL ["1","2","3","4","5","6"].
/*DEFINE VARIABLE wsitusn      AS LOGI FORMAT "Sim/Nao" EXTENT 6       
       INITIAL [yes,yes,yes,yes,yes,yes].*/
DEFINE VARIABLE wsitulista   AS CHAR.
DEFINE VARIABLE wsitupos     AS INTE.
      
 
/*.......................................fim de situacao da ordem....*/


/*........................................inicio natureza da parcela.*/
DEFINE VARIABLE wnapardesc   AS CHAR FORMAT "x(14)"   EXTENT 3
       INITIAL ["Compra        ",
                "Servico       ",
                "Beneficiamento"].
DEFINE VARIABLE wnaparnum    AS CHAR FORMAT "9"       EXTENT 3
       INITIAL ["1","2","3"].
DEFINE VARIABLE wnaparsn     AS LOGI FORMAT "Sim/Nao" EXTENT 3       
       INITIAL [yes,yes,yes]. 
DEFINE VARIABLE wnaparlista  AS CHAR.
DEFINE VARIABLE wnaparpos    AS INTE.
 
/*........................................fim de natureza da parcela.*/

/*.......................................inicio situacao da parcela...*/
DEFINE VARIABLE wsipardesc    AS CHAR FORMAT "x(14)"   EXTENT 6
       INITIAL ["Nao Confirmada",
                "Confirmada    ",
                "Cotada        ",
                "Eliminada     ",
                "Em cotacao    ",
                "Recebida      "].
DEFINE VARIABLE wsiparnum     AS CHAR FORMAT "9"       EXTENT 6
       INITIAL ["1","2","3","4","5","6"].
/*DEFINE VARIABLE wsiparsn      AS LOGI FORMAT "Sim/Nao" EXTENT 6       
       INITIAL [yes,yes,yes,yes,yes,yes]. */
DEFINE VARIABLE wsiparlista   AS CHAR.
DEFINE VARIABLE wsiparpos     AS INTE.
 
/*.......................................fim de situacao da parcela...*/


 
/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Plano Mensal do Item".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
       ASSIGN wusr     = STRING(USERID(ldbname(1)),"x(10)").

 
/*.................................................fim de entrada padrao..*/
 

  ASSIGN wnapelista = "".
  DO wnapepos = 1 TO 3:
      /* 1*/
    IF wnapepos = 1  THEN DO:
      IF tt-param.wnapesn-1 THEN DO:
      IF LENGTH(wnapelista) GT 0 THEN ASSIGN wnapelista = wnapelista + ",".
      ASSIGN wnapelista = wnapelista + wnapenum[wnapepos].
      END.
    END.

      /* 2*/
    IF wnapepos = 2  THEN DO:
      IF tt-param.wnapesn-2 THEN DO:
      IF LENGTH(wnapelista) GT 0 THEN ASSIGN wnapelista = wnapelista + ",".
      ASSIGN wnapelista = wnapelista + wnapenum[wnapepos].
      END.
    END.
      /* 3*/
    IF wnapepos = 3  THEN DO:
      IF tt-param.wnapesn-3 THEN DO:
      IF LENGTH(wnapelista) GT 0 THEN ASSIGN wnapelista = wnapelista + ",".
      ASSIGN wnapelista = wnapelista + wnapenum[wnapepos].
      END.
    END.


    END.

 
  ASSIGN wsipelista = "".
  DO wsipepos = 1 TO 3:
   /*1*/
    IF wsipepos = 1  THEN DO:
     IF tt-param.wsipesn-1 THEN DO:
      IF LENGTH(wsipelista) GT 0 THEN ASSIGN wsipelista = wsipelista + ",".
      ASSIGN wsipelista = wsipelista + wsipenum[wsipepos].
      END.
    END.
   /*2*/
    IF wsipepos = 2  THEN DO:
     IF tt-param.wsipesn-2 THEN DO:
      IF LENGTH(wsipelista) GT 0 THEN ASSIGN wsipelista = wsipelista + ",".
      ASSIGN wsipelista = wsipelista + wsipenum[wsipepos].
      END.
    END.
   /*3*/
    IF wsipepos = 3  THEN DO:
     IF tt-param.wsipesn-3 THEN DO:
      IF LENGTH(wsipelista) GT 0 THEN ASSIGN wsipelista = wsipelista + ",".
      ASSIGN wsipelista = wsipelista + wsipenum[wsipepos].
      END.
    END.

  END.


 
  ASSIGN wsitulista = "".
  DO wsitupos = 1 TO 6:
     /*1*/
     IF wsitupos = 1  THEN DO:
       IF tt-param.wsitusn-1 THEN DO:
        IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
        ASSIGN wsitulista = wsitulista + wsitunum[wsipepos].
        END.
      END.
        /*2*/
     IF wsitupos = 2  THEN DO:
       IF tt-param.wsitusn-2 THEN DO:
        IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
        ASSIGN wsitulista = wsitulista + wsitunum[wsipepos].
        END.
      END.
        /*3*/
     IF wsitupos = 3  THEN DO:
       IF tt-param.wsitusn-3 THEN DO:
        IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
        ASSIGN wsitulista = wsitulista + wsitunum[wsipepos].
        END.
      END.
        /*4*/
     IF wsitupos = 4  THEN DO:
       IF tt-param.wsitusn-4 THEN DO:
        IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
        ASSIGN wsitulista = wsitulista + wsitunum[wsipepos].
        END.
      END.
        /*5*/
     IF wsitupos = 5  THEN DO:
       IF tt-param.wsitusn-5 THEN DO:
        IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
        ASSIGN wsitulista = wsitulista + wsitunum[wsipepos].
        END.
      END.
        /*6*/
     IF wsitupos = 6  THEN DO:
       IF tt-param.wsitusn-6 THEN DO:
        IF LENGTH(wsitulista) GT 0 THEN ASSIGN wsitulista = wsitulista + ",".
        ASSIGN wsitulista = wsitulista + wsitunum[wsipepos].
        END.
      END.
  END.
 
  ASSIGN wsiparlista = "".
  /*1*/
    DO wsiparpos = 1 TO 6:
    IF wsiparpos = 1  THEN DO:
      IF tt-param.wsiparsn-1 THEN DO:
        IF LENGTH(wsiparlista) GT 0 THEN ASSIGN wsiparlista = wsiparlista + ",".
        ASSIGN wsiparlista = wsiparlista + wsiparnum[wsipepos].
        END.
    END.

  /*2*/
      IF wsiparpos = 2  THEN DO:
      IF tt-param.wsiparsn-2 THEN DO:
        IF LENGTH(wsiparlista) GT 0 THEN ASSIGN wsiparlista = wsiparlista + ",".
        ASSIGN wsiparlista = wsiparlista + wsiparnum[wsipepos].
        END.
    END.
      /*3*/
      IF wsiparpos = 3  THEN DO:
      IF tt-param.wsiparsn-3 THEN DO:
        IF LENGTH(wsiparlista) GT 0 THEN ASSIGN wsiparlista = wsiparlista + ",".
        ASSIGN wsiparlista = wsiparlista + wsiparnum[wsipepos].
        END.
    END.
     /*4*/
      IF wsiparpos = 4  THEN DO:
      IF tt-param.wsiparsn-4 THEN DO:
        IF LENGTH(wsiparlista) GT 0 THEN ASSIGN wsiparlista = wsiparlista + ",".
        ASSIGN wsiparlista = wsiparlista + wsiparnum[wsipepos].
        END.
    END.
     /*5*/
      IF wsiparpos = 5  THEN DO:
      IF tt-param.wsiparsn-5 THEN DO:
        IF LENGTH(wsiparlista) GT 0 THEN ASSIGN wsiparlista = wsiparlista + ",".
        ASSIGN wsiparlista = wsiparlista + wsiparnum[wsipepos].
        END.
    END.
     /*1*/
      IF wsiparpos = 6  THEN DO:
      IF tt-param.wsiparsn-6 THEN DO:
        IF LENGTH(wsiparlista) GT 0 THEN ASSIGN wsiparlista = wsiparlista + ",".
        ASSIGN wsiparlista = wsiparlista + wsiparnum[wsipepos].
        END.
    END.
 
  END.
 
    FOR EACH lo-matplano
       WHERE lo-matplano.nr-pl      MATCHES(tt-param.wmmaaaax)
       AND   lo-matplano.it-codigo  MATCHES(tt-param.wite) 
       AND   lo-matplano.estab      MATCHES(tt-param.westab)
       NO-LOCK WITH FRAME f-lh: 


      run pi-acompanhar in h-acomp ("Item: " + STRING(lo-matplano.it-codigo)).

      ASSIGN wzeroprod = 1
             wzeroprev = 1.
      IF  lo-matplano.imediato = 0
      AND lo-matplano.qt-sem1 = 0
      AND lo-matplano.qt-sem2 = 0
      AND lo-matplano.qt-sem3 = 0
      AND lo-matplano.qt-sem4 = 0
      AND lo-matplano.qt-sem5 = 0 THEN DO:
        ASSIGN wzeroprod = 0.
        END.
      IF  lo-matplano.qt-sem1-2 = 0
      AND lo-matplano.qt-sem2-2 = 0
      AND lo-matplano.qt-sem3-2 = 0
      AND lo-matplano.qt-sem4-2 = 0
      AND lo-matplano.qt-sem5-2 = 0 
      AND lo-matplano.mes1      = 0
      AND lo-matplano.mes2      = 0 THEN DO:
        ASSIGN wzeroprev = 0.
        END.
      
      IF  (wzeroprod = 0)
      AND (wzeroprev = 0)
      AND NOT tt-param.wzerosn
      THEN DO:
        NEXT.
        END.

      IF NOT tt-param.wpresn THEN DO:
        IF wzeroprod = 0 THEN DO:
          IF NOT tt-param.wzerosn THEN DO:
            NEXT.
            END.
          END.
        END.

     

      ASSIGN wlitfam = "".
      FIND item 
      WHERE item.it-codigo = lo-matplano.it-codigo
      NO-LOCK NO-ERROR.
      IF AVAILABLE item THEN  ASSIGN wlitfam = item.fm-cod-com.
      IF wlitfam LT tt-param.wfami
      OR wlitfam GT tt-param.wfamf
      THEN DO:
        NEXT.
        END.
      IF NOT AVAILABLE item THEN DO:
        ASSIGN wdet = "*?* item nao cadastrado:"
                    + STRING(lo-matplano.it-codigo).
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        ASSIGN wdet = FILL("_",132).
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        NEXT.
        END.

      ASSIGN wbate = 0.   
      FIND emitente
      WHERE emitente.cod-emitente = lo-matplano.cod-emitente
      NO-LOCK NO-ERROR.
      IF emitente.nome-abrev MATCHES(tt-param.wnomea)
      THEN DO:
        ASSIGN wbate = 1.                                       
        END.
      IF wbate = 0 THEN DO:
        NEXT.
        END.
        

      /*............................................inicio monta titulos...*/
      FIND lo-matdatas
      WHERE lo-matdatas.emp = lo-matplano.emp
      AND   lo-matdatas.estab = lo-matplano.estab
      AND   lo-matdatas.nr-pl = SUBSTRING(lo-matplano.nr-pl,1,6)
      NO-LOCK NO-ERROR.

      IF NOT AVAILABLE lo-matdatas THEN DO:
        ASSIGN wdet = "*?* [lo-matdatas nao encontrado]"
                    + " Emp:"   + STRING(lo-matplano.emp)
                    + " Estab:" + STRING(lo-matplano.estab)
                    + " Plano:" + STRING(lo-matplano.nr-pl).
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        ASSIGN wdet = FILL("_",132).
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        NEXT.
        END.

      IF AVAILABLE lo-matdatas THEN DO:
        ASSIGN wtotparc = 0.
        
        /*.........................................inicio limpa tabelas...*/
        DO wele = 1 TO 12:
          ASSIGN wtbdata[wele] = ?
                 wtbqtde[wele] = 0.
          END.
        ASSIGN wele = 0.  
        /*.........................................inicio limpa tabelas...*/


        /*..............................................inicio monta datas..*/
        ASSIGN waaaa =
           INTEGER(SUBSTRING(STRING(lo-matplano.nr-pl,"x(7)"),3,4)).
        IF waaaa LT 1900 THEN DO:
          ASSIGN
          wdet = "*?* [ano do plano invalido]"
               + " Plano:" + STRING(lo-matplano.nr-pl).
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          ASSIGN wdet = FILL("_",132).
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          NEXT.
          END.

        ASSIGN wmmplano =
           INTEGER(SUBSTRING(STRING(tt-param.wmmaaaax,"x(7)"),1,2)).


        /*.................................inicio monta 1a.parte datas...*/
        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem1)
               wquant = lo-matplano.qt-sem1.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem2)
               wquant = lo-matplano.qt-sem2.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem3)
               wquant = lo-matplano.qt-sem3.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem4)
               wquant = lo-matplano.qt-sem4.
        RUN PI_Semanas.


        ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem5)
               wquant = lo-matplano.qt-sem5.
        RUN PI_Semanas.
        /*.................................fim de monta 1a.parte datas...*/


        /*..............................................inicio previsoes...*/
        IF tt-param.wpresn THEN DO:

          /*.................................inicio monta 2a.parte datas...*/
          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem1-2)
                 wquant = lo-matplano.qt-sem1-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem2-2)
                 wquant = lo-matplano.qt-sem2-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem3-2)
                 wquant = lo-matplano.qt-sem3-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem4-2)
                 wquant = lo-matplano.qt-sem4-2.
          RUN PI_Semanas.


          ASSIGN wtitx6 = "" wtitx9 = STRING(lo-matdatas.titsem5-2)
                 wquant = lo-matplano.qt-sem5-2.
          RUN PI_Semanas.
          /*.................................fim de monta 2a.parte datas...*/


          /*.................................inicio monta 3a.parte datas...*/
          ASSIGN wtitx3 = "" wtitx9 = STRING(lo-matdatas.titmes1).
                 wquant = lo-matplano.mes1.
          RUN PI_Meses.


          ASSIGN wtitx3 = "" wtitx9 = STRING(lo-matdatas.titmes2).
                 wquant = lo-matplano.mes2.
          RUN PI_Meses.
          /*.................................fim de monta 3a.parte datas...*/

          END.
        /*..............................................fim de previsoes...*/


        /*..............................................fim de monta datas..*/
        END.
      /*............................................fim de monta titulos...*/


      /*..................................inicio monta literais de data...*/
      ASSIGN
      wlitsem1   = STRING(wtbdata[01],"99/99/9999")
      wlitsem2   = STRING(wtbdata[02],"99/99/9999")
      wlitsem3   = STRING(wtbdata[03],"99/99/9999")
      wlitsem4   = STRING(wtbdata[04],"99/99/9999")
      wlitsem5   = STRING(wtbdata[05],"99/99/9999")
      wlitsem1-2 = STRING(wtbdata[06],"99/99/9999")
      wlitsem2-2 = STRING(wtbdata[07],"99/99/9999")
      wlitsem3-2 = STRING(wtbdata[08],"99/99/9999")
      wlitsem4-2 = STRING(wtbdata[09],"99/99/9999")
      wlitsem5-2 = STRING(wtbdata[10],"99/99/9999")
      wlitmes1   = STRING(wtbdata[11],"99/99/9999")
      wlitmes2   = STRING(wtbdata[12],"99/99/9999").
      /*..................................fim de monta literais de data...*/


      /*.......................................inicio exibe 1a. parte...*/
      ASSIGN wtitlit 
          = " " + STRING(wlitsem1,"x(10)")
          + " " + STRING(wlitsem2,"x(10)")
          + " " + STRING(wlitsem3,"x(10)")
          + " " + STRING(wlitsem4,"x(10)")
          + " " + STRING(wlitsem5,"x(10)").

      ASSIGN wdet = STRING(lo-matplano.nr-pl,"X(7)")      
            + " " + STRING(lo-matplano.it-codigo,"X(13)")               
            + " " + STRING(lo-matplano.estab,"x(3)")
            + " " + STRING(STRING(INTE(lo-matplano.emp)),"x(3)")
            + " " + STRING(STRING(INTE(emitente.cod-emitente)),"x(9)")
                  + FILL(" ",3)
                  + " Atraso    "
                  + wtitlit.
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.


      ASSIGN wdet = FILL(" ",8)
                  + STRING(item.descricao-1,"x(18)")
                  + FILL(" ",4)
                  + STRING(emitente.nome-abrev,"x(12)")
            + " " + STRING(STRING(INTE(lo-matplano.imediato)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem1)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem2)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem3)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem4)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem5)),"x(10)").

                      
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
      /*.......................................fim de exibe 1a. parte...*/


      /*.......................................inicio exibe 2a. parte...*/
      ASSIGN wtitlit = "".
      IF tt-param.wpresn THEN DO:
        ASSIGN wtitlit
            = " " + STRING(wlitsem1-2,"x(10)")
            + " " + STRING(wlitsem2-2,"x(10)")
            + " " + STRING(wlitsem3-2,"x(10)")
            + " " + STRING(wlitsem4-2,"x(10)")
            + " " + STRING(wlitsem5-2,"x(10)")
            + " " + STRING(wlitmes1,"x(10)")
            + " " + STRING(wlitmes2,"x(10)").
        END.


      ASSIGN wdatacorte = "*?*".
      IF  lo-matplano.data-nota = ?
      AND lo-matplano.nro-docto = 0 
      THEN DO:
        ASSIGN wdatacorte = "".
        END.
      IF lo-matplano.data-nota NE ? THEN DO:
        ASSIGN wdatacorte = STRING(lo-matplano.data-nota,"99/99/9999").
        END.



      ASSIGN wdet = FILL(" ",8)
                  + "NF.corte "
                  + STRING(wdatacorte,"x(10)")
                  + FILL(" ",26)
                  + wtitlit.
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.


      ASSIGN wdet = FILL(" ",8)
                  + STRING(STRING(INTE(lo-matplano.nro-docto)),"x(6)")
                  + FILL(" ",3)                                 

                  + "FAM="
                  + STRING(item.fm-cod-com,"x(8)")
                  + FILL(" ",24).                                 


      IF tt-param.wpresn THEN DO:
        ASSIGN wdet = wdet
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem1-2)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem2-2)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem3-2)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem4-2)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.qt-sem5-2)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.mes1)),"x(10)")
            + " " + STRING(STRING(INTE(lo-matplano.mes2)),"x(10)").
        END.
                      
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
      /*.......................................fim de exibe 2a. parte...*/

      /*.....................................inicio exibe pedido de compra...*/
      ASSIGN wtotpedid = 0
             wtotordem = 0.
      FOR
      EACH  pedido-compr
      WHERE pedido-compr.cod-emitente = lo-matplano.cod-emitente
      AND   LOOKUP(STRING(pedido-compr.natureza),wnapelista) GT 0
      AND   LOOKUP(STRING(pedido-compr.situacao),wsipelista) GT 0
      NO-LOCK
      USE-INDEX emitente
      WITH FRAME f-lh:
        ASSIGN wtotpedid = wtotpedid + 1.

        IF tt-param.wpedsn THEN DO:
          IF wtotpedid = 1 THEN DO:
            ASSIGN wdet = FILL(" ",8)
                        + FILL("_",18)
                        + FILL("_",10)
                        + FILL("_",10)
                        + "SITUACAO"
                        + FILL("_",9)
                        + "NATUREZA"
                        + FILL("_",12)
                        + "DATA"
                        + FILL("_",13)
                        + "QUANTIDADE"
                        + FILL("_",22)
                        .
            DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.          
            END.
          
          ASSIGN wdet = FILL(" ",8)
               + "Pedido:"
               + STRING(STRING(INTE(pedido-compr.num-pedido)),"x(8)")
               + FILL(" ",22)                          

               + STRING(pedido-compr.situacao,">9")
               + "="
               + STRING(wsipedesc[pedido-compr.situacao],"x(12)")

               + "  "
               + STRING(pedido-compr.natureza,">9")
               + "="
               + STRING(wnapedesc[pedido-compr.natureza],"x(14)")

               + " "
               + " pedido="
               + STRING(pedido-compr.data-pedido,"99/99/9999")
               + FILL(" ",12)
               + STRING(pedido-compr.cod-cond-pag,">>9")
               + "=Cond.Pgto"
               + " Estab="
               + STRING(pedido-compr.cod-estabel,"x(3)")
               .
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.

        /*.......................................inicio exibe ordem compra...*/
        FOR
        EACH  ordem-compra
        WHERE ordem-compra.it-codigo    = lo-matplano.it-codigo
        AND   ordem-compra.cod-emitente = emitente.cod-emitente
        AND   ordem-compra.num-pedido   = pedido-compr.num-pedido
        AND   LOOKUP(STRING(ordem-compra.situacao),wsitulista) GT 0
        NO-LOCK
        USE-INDEX item
        WITH FRAME f-lh:
          ASSIGN wtotordem = wtotordem + 1
                 wultimaparc = 0.
          
          IF tt-param.wordsn THEN DO:
            ASSIGN wdet = FILL(" ",19)
                 + "Ordem:" 
                 + STRING(ordem-compra.numero-ordem,"zzzzz9,99")
                 + FILL(" ",11)
                 + STRING(ordem-compra.situacao,">9")
                 + "="
                 + STRING(wsitudesc[ordem-compra.situacao],"x(14)")
                 + FILL(" ",17)
                 + " "
                 + " pedido="
                 + STRING(ordem-compra.data-pedido,"99/99/9999")
                 + FILL(" ",12)
                 .
            DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
            END.

          FOR 
          EACH  prazo-compra
          WHERE prazo-compra.it-codigo    = ordem-compra.it-codigo
          AND   prazo-compra.numero-ordem = ordem-compra.numero-ordem
          AND   LOOKUP(STRING(prazo-compra.situacao),wsiparlista) GT 0
          NO-LOCK
          USE-INDEX item-ordem:

            IF tt-param.wparsn THEN DO:
              ASSIGN wdet = FILL(" ",34)
                   + "Parc.:" 
                   + STRING(STRING(INTE(prazo-compra.parcela)),"x(5)")

                 + STRING(prazo-compra.situacao,">9")
                 + "="
                 + STRING(wsipardesc[prazo-compra.situacao],"x(12)")

                 + "  "
                 + STRING(prazo-compra.natureza,">9")
                 + "="
                 + STRING(wnapardesc[prazo-compra.natureza],"x(14)")

                 + " "
                 + "entrega="
                 + STRING(prazo-compra.data-entrega,"99/99/9999")
                 + STRING(prazo-compra.quantidade,">>>>>>9.9999")
                 + " "
                 + STRING(prazo-compra.pedido-clien,"x(12)")
                   .
              DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
              END.

            END.

          END.
        /*.......................................fim de exibe ordem compra...*/


        END.
      /*.....................................fim de exibe pedido de compra...*/
 
      IF item.cod-obsoleto = 4 THEN DO:
        ASSIGN wdet = "*?* item obsoleto:"
                    + STRING(lo-matplano.it-codigo).
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        END.

      IF tt-param.wpedsn THEN DO:
        IF wtotpedid = 0 THEN DO:
          ASSIGN wdet = "*?* nenhum pedido foi selecionado".
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.
        END.


      IF tt-param.wordsn THEN DO:
        IF wtotordem = 0 THEN DO:
          ASSIGN wdet = "*?* nenhuma ordem foi selecionada".
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.
        IF wtotordem GT 1 THEN DO:
          ASSIGN wdet = "*?* mais que uma ordem selecionada".
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.
        END.




 

      ASSIGN wdet = FILL("_", 132).
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.

      ASSIGN wctditem = wctditem + 1.

      END.


      DISPLAY wdet WITH FRAME f-relat WIDTH 300. DOWN 1.
      ASSIGN wdet = "       F I M   D A   C O N S U L T A         ".
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
/*................................fim do total geral...*/  


    PAGE.
      DISPLAY  
     "*********** PAR�METROS *********** "
      SKIP(2)
     "Mes: " tt-param.wmmaaaax   " - Item: " tt-param.wite 
      SKIP(2)
     "Fam�lia: " tt-param.wfami " a " tt-param.wfamf            
      SKIP 
      "Fornecedor: " tt-param.wnomea           
      SKIP 
      "Estabelecimento: " tt-param.westab   
      SKIP
      "Considera previs�es: " tt-param.wpresn 
      SKIP 
      "Exibe Zerados: " tt-param.wzerosn          
      skip
      "Exibe Pedidos de Compra: " tt-param.wpedsn 
      SKIP
      "Exibe Ordem de Compra: " tt-param.wordsn          
      SKIP
      "Exibe Parcela: " tt-param.wparsn
      SKIP(2)"--- Natureza do Pedido: " 
      SKIP "Compra: "  tt-param.wnapesn-1  
      SKIP "Servi�o: " tt-param.wnapesn-2 
      SKIP "Beneficiamento: " tt-param.wnapesn-3      
      SKIP(2) "--- Situa��o do Pedido: " 
      SKIP "Impresso: " tt-param.wsipesn-1 
      SKIP "N�o Impresso: " tt-param.wsipesn-2 
      SKIP "Eliminado: "  tt-param.wsipesn-3        
      SKIP(2)
      "--- Situa��o da Ordem:"
      SKIP "N�o confirmada: " tt-param.wsitusn-1 
      SKIP "Confirmada: "     tt-param.wsitusn-2 
      SKIP "Cotada: "         tt-param.wsitusn-3 
      SKIP "Eliminada: "      tt-param.wsitusn-4 
      SKIP "Em Cota��o: "     tt-param.wsitusn-5 
      SKIP "Terminada: "      tt-param.wsitusn-6     
      SKIP(2)
      "--- Situa��o das Parcelas:" 
      SKIP "N�o confirmada: " tt-param.wsiparsn-1 
      SKIP "Confirmada: "     tt-param.wsiparsn-2
      SKIP "Cotada: "         tt-param.wsiparsn-3   
      SKIP "Eliminada: "      tt-param.wsiparsn-4 
      SKIP "Em cota��o: "     tt-param.wsiparsn-5       
      SKIP "Terminada: "      tt-param.wsiparsn-6  
    WITH FRAME f-param WITH WIDTH 300 NO-LABELS.

    END PROCEDURE.



/*................................................inicio procedure semanas...*/
PROCEDURE PI_Semanas.

  DO wpos = 1 TO 9:
    IF SUBSTRING(wtitx9,wpos,1) NE " " THEN DO:
      ASSIGN wtitx6 = wtitx6 + SUBSTRING(wtitx9,wpos,1).
      END.
    END.

  ASSIGN wele = wele + 1.
  ASSIGN wdd = INTEGER(SUBSTRING(STRING(wtitx6,"x(6)"),1,2))
         wmm = 0
         wmmm = SUBSTRING(STRING(wtitx6,"x(6)"),4,3).
  DO wctdmes = 1 TO 12:
    IF wmes[wctdmes] = wmmm THEN DO:
      ASSIGN wmm = wctdmes.
      IF  wele GT 1
      AND wmm LT wmmplano THEN DO:
        ASSIGN waaaa = waaaa + 1.
               wmmplano = wmm.
        END.
      LEAVE.
      END.
    END.
  ASSIGN wxdata  = ?
         wlitx10 = "*?* " + wtitx6.  
  IF wmm GT 0 THEN DO:
    ASSIGN wxdata = DATE(wmm,wdd,waaaa) 
           wlitx10 = STRING(wxdata,"99/99/9999").
    END.
  IF wmm = 0 AND wquant = 0 THEN wlitx10 = "".
  IF wquant NE 0 THEN ASSIGN wtotparc = wtotparc + 1.
  ASSIGN wtbdata[wele] = wxdata
         wtbqtde[wele] = wquant.

  RETURN.
  END PROCEDURE.
/*................................................fim de procedure semanas...*/


/*................................................inicio procedure meses.....*/
PROCEDURE PI_Meses.

  DO wpos = 1 TO 9:
    IF SUBSTRING(wtitx9,wpos,1) NE " " THEN DO:
      ASSIGN wtitx3 = wtitx3 + SUBSTRING(wtitx9,wpos,1).
      IF LENGTH(wtitx3) = 3 THEN LEAVE.
      END.
    END.

  ASSIGN wele = wele + 1.

  ASSIGN wdd = 01
         wmm = 0
         wmmm = wtitx3.                                  
  DO wctdmes = 1 TO 12:
    IF wmes[wctdmes] = wmmm THEN DO:
      ASSIGN wmm = wctdmes.
      IF  wele GT 1
      AND wmm LT wmmplano THEN DO:
        ASSIGN waaaa = waaaa + 1.
               wmmplano = wmm.
        END.
      LEAVE.
      END.
    END.
  ASSIGN wxdata  = ?
         wlitx10 = "*?* " + wtitx3.  
  IF wmm GT 0 THEN DO:
    ASSIGN wxdata = DATE(wmm,wdd,waaaa) 
           wlitx10 = STRING(wxdata,"99/99/9999").
    END.
  IF wmm = 0 AND wquant = 0 THEN wlitx10 = "".
  IF wquant NE 0 THEN ASSIGN wtotparc = wtotparc + 1.
  ASSIGN wtbdata[wele] = wxdata
         wtbqtde[wele] = wquant.

  RETURN.



  END PROCEDURE.
  





