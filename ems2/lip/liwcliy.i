/***********************************************************************
**
** liwcliy.p - Atualiza WORKFILE de clientes: showcli
**
**              23/10/2000 - BENE   Programa novo.
**
***********************************************************************/


/*.............................INICIO TRATAMENTO TABELA CLIENTES...*/
FOR EACH showcli
BREAK BY showcli.nome-abrev
      BY showcli.data-pgm
      BY showcli.cod-pgm
      BY showcli.tipo-pgm:

  ASSIGN showcli.atrsd =0
         showcli.ne[1] =0
         showcli.ne[2] =0
         showcli.ne[3] =0
         showcli.ne[4] =0.

  /*.....................................INICIO ARQUIVO LO-CLIPGM.*/   
  IF LAST-OF(showcli.nome-abrev)
  AND showcli.codarq = "lo-clipgm"
  THEN          
  FOR EACH mgfas.lo-clipgm
     WHERE mgfas.lo-clipgm.codrefsbp  = wcodrefaux
     AND   mgfas.lo-clipgm.nome-abrev = showcli.nome-abrev
     AND   mgfas.lo-clipgm.data-pgm   = showcli.data-pgm
     AND   mgfas.lo-clipgm.cod-pgm    = showcli.cod-pgm
     AND   mgfas.lo-clipgm.tipo-pgm   = showcli.tipo-pgm
     USE-INDEX ctrpcl
     NO-LOCK:
     
    IF mgfas.lo-clipgm.data LT showcli.dtnf /*.ignora nec.anterior */
    THEN NEXT.                               /*.a dt da nf do pgm...*/

    /*..........................INICIO FREQUENCIA PARA EXPLOSAO.....*/
    ASSIGN wttreme = 1
           wpasso   = 1
           wdiainic = 0.
    FIND  mgadm.frequencia 
    WHERE mgadm.frequencia.cd-freq = mgfas.lo-clipgm.frequencia
    NO-LOCK 
    NO-ERROR.
    IF AVAILABLE mgadm.frequencia THEN DO:
      ASSIGN wttreme = 0.
      DO wctd = 1 TO 39:
        IF mgadm.frequencia.frequencia[wctd] = yes THEN DO:
          ASSIGN wttreme = wttreme + 1.
          IF mgadm.frequencia.dt-ref NE 0
          AND wdiainic = 0
          AND wctd GE 1
          AND wctd LE 7 THEN ASSIGN wdiainic = wctd.
          END.
        END.
      ASSIGN wpasso = mgadm.frequencia.intervalo.
      END.
    ASSIGN wdtaux = mgfas.lo-clipgm.data.
    IF wdiainic GT 0 THEN REPEAT:
      IF WEEKDAY(wdtaux) = wdiainic THEN LEAVE.
      ASSIGN wdtaux = wdtaux + 1.
      END.
    /*..........................FIM DE FREQUENCIA PARA EXPLOSAO.....*/


    /*........................INICIO EXPLOSAO.......................*/
    DO wctd = 1 TO wttreme:
      ASSIGN wdtne = wdtaux.

      /*...........................DEFINE A DATA DA PROXIMA ENTREGA.*/
      ASSIGN wdtaux = wdtaux + wpasso.                                
      REPEAT:
        IF (WEEKDAY(wdtaux) <> 1)
        AND (WEEKDAY(wdtaux) <> 7) THEN LEAVE.
        ASSIGN wdtaux = wdtaux + 1.
        END.

      /*...........................APLICACAO DO LEAD TIME...........*/
      IF tt-param.wprv = "Produ��o(GM-RND001)"
      OR tt-param.wprv = "Produ��o"  THEN DO:
        ASSIGN wxx-ltime = 0.
        FIND bff-prd
        WHERE bff-prd.it-codigo  = witemaux              
        AND   bff-prd.nome-abrev = mgfas.lo-clipgm.nome-abrev
        NO-LOCK NO-ERROR.

        IF AVAILABLE bff-prd
        AND bff-prd.pr-ltime GT 0
        THEN wxx-ltime = bff-prd.pr-ltime.

        ASSIGN wdtne  = wdtne - wxx-ltime.
        REPEAT:
          FIND  FIRST mgind.calen-prod
                WHERE mgind.calen-prod.data = wdtne
                AND   mgind.calen-prod.cod-estabel = "1"
                AND   mgind.calen-prod.tipo-dia = 3
                NO-LOCK NO-ERROR.
          IF NOT AVAILABLE mgind.calen-prod THEN DO:
            IF WEEKDAY(wdtne) <> 1 THEN LEAVE.
            END.
          ASSIGN wdtne = wdtne - 1.
          END.
        END.

      /*............................PROTELA ENTREGA EM FERIADO.....*/
      IF tt-param.wprv = "Entrega (GM-RND012)" 
      OR tt-param.wprv = "Entrega" THEN DO:
        REPEAT:
          FIND FIRST mgind.calen-prod
               WHERE mgind.calen-prod.data = wdtne
               AND   mgind.calen-prod.cod-estabel = "1" 
               AND   mgind.calen-prod.tipo-dia = 3
               NO-LOCK NO-ERROR.
          IF NOT AVAILABLE mgind.calen-prod THEN DO:
            IF (WEEKDAY(wdtne) <> 1)
            AND (WEEKDAY(wdtne) <> 7) THEN LEAVE.
            END.
          ASSIGN wdtne = wdtne + 1.
          END.
        END.

      /*...........................DISTRIBUI A QTDE DA EXPLOSAO....*/
      ASSIGN wqdne = TRUNCATE(mgfas.lo-clipgm.qtde / wttreme,0).
      IF wctd = 1 AND wttreme GT 1 THEN DO:
        wputscreen = "explodindo necessidade".
        wqdne 
        = wqdne + (mgfas.lo-clipgm.qtde -
        (TRUNCATE(mgfas.lo-clipgm.qtde / wttreme,0)
        * wttreme)).
        END.

     
      IF wdtne LT tt-param.wdtdd 
      THEN showcli.atrsd = showcli.atrsd + wqdne.
      DO wctq = 1 TO 4:
        IF  wdtne GE wdata[wctq] AND wdtne LT wdata[wctq + 1]
        THEN showcli.ne[wctq] = showcli.ne[wctq] + wqdne.
        END.
      END.
    /*........................FIM DA EXPLOSAO.......................*/

    END.
  /*.....................................FIM DE ARQUIVO LO-CLIPGM.*/   


  /*.....................................INICIO ARQUIVO LO-RN12...*/
  IF LAST-OF(showcli.nome-abrev)
  AND showcli.codarq = "lo-rn12"
  THEN
  FOR EACH lo-rn12
     WHERE lo-rn12.itemcli  = wcodrefaux
     AND   lo-rn12.geracao GE showcli.geri
     AND   lo-rn12.geracao LE showcli.gerf
     AND   LOOKUP(lo-rn12.locdestino,showcli.loclista) GT 0
     USE-INDEX gidh
     NO-LOCK:

   
    ASSIGN wdtne  = mgfas.lo-rn12.data.

    /*-------------PROTELA ENTREGA EM FERIADO-----------------------*/
    REPEAT:
      FIND FIRST calen-prod 
           WHERE calen-prod.data = wdtne
           AND   calen-prod.tipo-dia = 3
           NO-LOCK NO-ERROR.
      IF NOT AVAILABLE calen-prod THEN DO:
        IF (WEEKDAY(wdtne) <> 1) THEN LEAVE.
        END.
      ASSIGN wdtne = wdtne + 1.
      END.

    /*------------------------------------------------------------*/
    ASSIGN wqdne = mgfas.lo-rn12.qtde.                 
    IF wdtne LT tt-param.wdtdd 
    THEN ASSIGN showcli.atrsd = showcli.atrsd + wqdne.
    DO wctq = 1 TO 4:
      IF  wdtne GE wdata[wctq] 
      AND wdtne LT wdata[wctq + 1]
      THEN 
      ASSIGN showcli.ne[wctq] = showcli.ne[wctq] + wqdne.
      END.
    END.
  /*.....................................FIM DE ARQUIVO LO-RN12...*/


  /*.....................................INICIO ARQUIVO LO-RN01...*/
  IF LAST-OF(showcli.nome-abrev)
  AND showcli.codarq = "lo-rn01"
  THEN
  FOR EACH lo-rn01
     WHERE lo-rn01.itemcli   = wcodrefaux
     AND   lo-rn01.geracao  GE showcli.geri
     AND   lo-rn01.geracao  LE showcli.gerf
     AND   lo-rn01.progatual = showcli.progatual
     AND   LOOKUP(lo-rn01.locdestino,showcli.loclista) GT 0
     USE-INDEX gid
     NO-LOCK:

   
    ASSIGN wdtne  = mgfas.lo-rn01.data.

    /*-------------PROTELA ENTREGA EM FERIADO-----------------------*/
    REPEAT:
      FIND FIRST calen-prod 
           WHERE calen-prod.data = wdtne
           AND   calen-prod.tipo-dia = 3
           NO-LOCK NO-ERROR.
      IF NOT AVAILABLE calen-prod THEN DO:
        IF (WEEKDAY(wdtne) <> 1) THEN LEAVE.
        END.
      ASSIGN wdtne = wdtne + 1.
      END.

    /*------------------------------------------------------------*/
    ASSIGN wqdne = mgfas.lo-rn01.qtde.                 
    IF wdtne LT tt-param.wdtdd 
    THEN ASSIGN showcli.atrsd = showcli.atrsd + wqdne.
    DO wctq = 1 TO 4:
      IF  wdtne GE wdata[wctq] 
      AND wdtne LT wdata[wctq + 1]
      THEN 
      ASSIGN showcli.ne[wctq] = showcli.ne[wctq] + wqdne.
      END.
    END.
  /*.....................................FIM DE ARQUIVO LO-RN01...*/


  /*.....................................INICIO ARQUIVO lo-jitlog...*/
  IF LAST-OF(showcli.nome-abrev)
  AND showcli.codarq = "lo-jitlog"
  THEN
  FOR EACH lo-jitlog
     WHERE lo-jitlog.peca   = wcodrefaux
     AND   lo-jitlog.obs    = ""            
     AND   lo-jitlog.dt-ac NE ?             
     USE-INDEX ch-02
     NO-LOCK:

    
    IF wqualpgm = "U"
    AND lo-jitlog.dt-sd NE ?
    THEN NEXT.

    ASSIGN wdtne  = mgfas.lo-jitlog.dt-ac.
    IF tt-param.wprv = "Entrega (GM-RND012)" 
    OR tt-param.wprv = "Entrega" THEN ASSIGN wdtne = wdtne + 3.

    /*-------------PROTELA ENTREGA EM FERIADO-----------------------*/
    IF tt-param.wprv = "Entrega (GM-RND012)" 
    OR tt-param.wprv = "Entrega" THEN DO:
      REPEAT:
        FIND FIRST calen-prod 
             WHERE calen-prod.data = wdtne
             AND   calen-prod.tipo-dia = 3
             NO-LOCK NO-ERROR.
        IF NOT AVAILABLE calen-prod THEN DO:
          IF (WEEKDAY(wdtne) <> 1) THEN LEAVE.
          END.
        ASSIGN wdtne = wdtne + 1.
        END.
      END.

    /*----------------------------------ACUMULA NO PERIODO--------*/
    ASSIGN wqdne = mgfas.lo-jitlog.qtde.                 
    IF wdtne LT tt-param.wdtdd 
    THEN ASSIGN showcli.atrsd = showcli.atrsd + wqdne.
    DO wctq = 1 TO 4:
      IF  wdtne GE wdata[wctq] 
      AND wdtne LT wdata[wctq + 1]
      THEN 
      ASSIGN showcli.ne[wctq] = showcli.ne[wctq] + wqdne.
      END.
    END.
  /*.....................................FIM DE ARQUIVO lo-jitlog...*/


  /*........................INICIO EXTRAI ATRASO PELO FATURADO....*/


  FOR EACH movdis.it-nota-fisc
     WHERE movdis.it-nota-fisc.it-codigo    = witemaux             
     AND   movdis.it-nota-fisc.nome-ab-cli  =  showcli.nome-abrev
     AND   movdis.it-nota-fisc.dt-cancela   =  ?
     AND   movdis.it-nota-fisc.baixa-estoq  = yes
     AND   movdis.it-nota-fisc.dt-emis-nota LE tt-param.wdtdd
     AND (   (movdis.it-nota-fisc.dt-emis-nota GT showcli.dtnf)
          OR (movdis.it-nota-fisc.dt-emis-nota =  showcli.dtnf
                              AND
              INTE(movdis.it-nota-fisc.nr-nota-fis)  GT showcli.nf))
     USE-INDEX ch-item-nota
     NO-LOCK:

       ASSIGN  showcli.atrsd = showcli.atrsd 
                   - movdis.it-nota-fisc.qt-faturada[1].
    END.
  /*........................FIM DE EXTRAI ATRASO PELO FATURADO....*/


  /*........................INICIO EXTRAI ATRASO PELO DEVOLVIDO...*/
  
  FOR EACH movind.item-doc-est
     WHERE INTE(movind.item-doc-est.nro-comp)    GT 0                     
     AND   movind.item-doc-est.it-codigo    =  witemaux             
     AND   movind.item-doc-est.cod-emitente =  showcli.cod-emitente
     AND (   (movind.item-doc-est.data-comp GT showcli.dtnf)
          OR (movind.item-doc-est.data-comp =  showcli.dtnf
                            AND
            INTE(movind.item-doc-est.nro-comp)  GT showcli.nf))
     AND  (movind.item-doc-est.nat-operacao BEGINS("131") OR
           movind.item-doc-est.nat-operacao BEGINS("231") OR
           movind.item-doc-est.nat-operacao BEGINS("331"))
     USE-INDEX item
     NO-LOCK:

   
    ASSIGN showcli.atrsd = showcli.atrsd 
                  + movind.item-doc-est.quantidade.    
    END.
  /*........................FIM DE EXTRAI ATRASO PELO DEVOLVIDO...*/


  /*.......................................CALCULO DO ADIANTADO...*/
  DO wctq = 1 TO 4:
    IF   showcli.atrsd LT 0 
    AND (showcli.atrsd * -1) GE showcli.ne[wctq] 
    THEN DO:
      ASSIGN showcli.atrsd = showcli.atrsd 
                    + showcli.ne[wctq]
             showcli.ne[wctq] = 0.
      END.
    IF  showcli.atrsd LT 0 
    AND (showcli.atrsd * -1) LT showcli.ne[wctq]
    THEN DO:
      ASSIGN showcli.ne[wctq] = showcli.ne[wctq] 
                              + showcli.atrsd 
             showcli.atrsd = 0.
      END.
    END.

  /*.......................................ABATIMENTO DO ESTOQUE...*/
  IF (tt-param.wprv = "Produ��o(GM-RND001)" OR tt-param.wprv = "Produ��o")
  AND westoq GT 0 THEN DO:
    IF  westoq GT 0 
    AND westoq GE showcli.atrsd  
    THEN DO:
      ASSIGN westoq = westoq - showcli.atrsd
             showcli.atrsd = 0.
      END.
    IF  westoq GT 0 
    AND westoq LT showcli.atrsd
    THEN DO:
      ASSIGN showcli.atrsd = showcli.atrsd - westoq
             westoq = 0.
      END.

    DO wctq = 1 TO 4:
      IF  westoq GT 0 
      AND westoq GE showcli.ne[wctq] 
      THEN DO:
        ASSIGN westoq = westoq - showcli.ne[wctq]
               showcli.ne[wctq] = 0.
        END.
      IF  westoq GT 0 
      AND westoq LT showcli.ne[wctq] 
      THEN DO:
        ASSIGN showcli.ne[wctq] = showcli.ne[wctq] - westoq
               westoq = 0.
        END.
      END.
    END.

END.

        /*.............................FIM DE TRATAMENTO TABELA CLIENTES...*/







