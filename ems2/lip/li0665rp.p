&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0665RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.
DEF VAR c-arquivo-log                AS CHAR NO-UNDO.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wplanej          AS CHAR FORMAT "X(12)"
    FIELD warqse           AS CHAR FORMAT "X(40)".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.
/* valeria */
 DEFINE STREAM log-import.

/* VARIAVEIS BENE */

    DEFINE VARIABLE wdatai     AS DATE FORMAT "99/99/9999".
    DEFINE VARIABLE wdataf     AS DATE FORMAT "99/99/9999".
    DEFINE VARIABLE wpc        AS CHAR FORMAT "!".
    DEFINE VARIABLE wdr        AS CHAR FORMAT "!".


    DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

    DEFINE VARIABLE wdescarq   AS CHAR FORMAT "x(16)". 
    DEFINE VARIABLE wdescper   AS CHAR FORMAT "x(23)".

    DEFINE VARIABLE wseptable   AS CHAR FORMAT "x(25)".
    DEFINE VARIABLE wsepi       AS CHAR FORMAT "x(31)".
    DEFINE VARIABLE wsepf       AS CHAR FORMAT "x(10)".

    DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)".
   /* DEFINE VARIABLE wplanej     LIKE ord-prod.cd-planejado.
    DEFINE VARIABLE warqse      AS CHAR FORMAT "x(40)".      */
    DEFINE VARIABLE wdtdd       AS DATE FORMAT "99/99/9999".
    DEFINE VARIABLE wturno      AS CHAR FORMAT "x(12)".
    DEFINE VARIABLE wordem      AS INTE FORMAT ">>>>>9".
    DEFINE VARIABLE wqtdenew    AS INTE FORMAT ">>>>>>>>9".

    DEFINE VARIABLE wdet        AS CHAR FORMAT "x(78)".

    DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
    DEFINE VARIABLE wctd1       AS INTE.
    DEFINE VARIABLE wexiste     AS INTE.

    DEFINE VARIABLE wobsoleto   AS INTE.                       

    DEFINE VARIABLE wqtp        AS INTE.
    DEFINE VARIABLE wnumreg      AS INTE.
    DEFINE VARIABLE wxite       AS CHAR FORMAT "x(16)".
    DEFINE VARIABLE wxdata      AS CHAR FORMAT "x(10)".
    DEFINE VARIABLE wxquant     AS CHAR FORMAT "x(9)".
    DEFINE VARIABLE wctdvirg    AS INTE.
    DEFINE VARIABLE wlinha      AS CHAR FORMAT "x(80)" NO-UNDO.

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Importa Detalhes da Ordem".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:

    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}

    FILE-INFO:FILENAME = c-arquivo-log.

    IF FILE-INFO:PATHNAME <> ? THEN
        DOS SILENT notepad.exe value(FILE-INFO:PATHNAME).


end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* PROCEDURE roda_prog -----------------------------------------------------------------*/
PROCEDURE roda_prog.

	def var c-destino as char no-undo.

	RUN pi-retorna-diretorio 
		 (tt-param.usuario,
		 OUTPUT c-destino).

    c-arquivo-log = c-destino + "log-importacao.txt".

    /*FSW OUTPUT STREAM log-import TO VALUE("c:/spool/log-importacao.txt") PAGED.*/
	OUTPUT STREAM log-import TO VALUE(c-arquivo-log) 
        PAGED CONVERT TARGET SESSION:CHARSET.
    PUT STREAM log-import "*** LOG DE IMPORTA��O DE ORDENS ***" SKIP.
/*....................................inicio entrada de planejador...*/
      ASSIGN wmsg = "planejador ".
       
/*.......................................inicio importacao...........*/
      ASSIGN wrel = tt-param.warqse.

      IF SEARCH(wrel) = ?
      THEN DO:
        MESSAGE "Arquivo nao encontrado: " tt-param.warqse VIEW-AS ALERT-BOX.
        NEXT.
        END.

      ASSIGN wnumreg = 0.
      INPUT FROM VALUE(wrel) NO-ECHO.
      REPEAT:
        ASSIGN wlinha = "".
        IMPORT UNFORMATTED wlinha.

        ASSIGN wctdvirg = 0
               wxite    = ""
               wxdata   = ""
               wxquant  = "".

        ASSIGN  wnumreg = wnumreg + 1.

        DO wctd1 = 1 TO LENGTH(wlinha):
          IF SUBSTRING(wlinha,wctd1,1) = ";"
          THEN DO:
            ASSIGN wctdvirg = wctdvirg + 1.
            NEXT.
            END.

          IF wctdvirg = 0
          THEN ASSIGN wxite   = wxite   + SUBSTRING(wlinha,wctd1,1).

          IF wctdvirg = 1
          THEN ASSIGN wxdata  = wxdata  + SUBSTRING(wlinha,wctd1,1).

          IF wctdvirg = 2
          THEN ASSIGN wxquant = wxquant + SUBSTRING(wlinha,wctd1,1).

          END.
/*
                                         /* consiste mes */
        IF SUBSTRING(wxdata,4,2) LT "01"
        OR SUBSTRING(wxdata,4,2) GT "12"
                                         /* consiste dia */
        OR SUBSTRING(wxdata,1,2) LT "01"
        OR SUBSTRING(wxdata,1,2) GT "31"
                                         /* consiste ano */
        OR SUBSTRING(wxdata,7,2) LT "01"
        OR SUBSTRING(wxdata,7,2) GT "99"

        THEN DO:
          MESSAGE "*** reg" wnumreg
                  "Data Invalida ***" wxite wxdata wxquant VIEW-AS ALERT-BOX.
          NEXT.
          END.
  */

        ASSIGN wdtdd = 01/01/0001.

        ASSIGN wdtdd = DATE(INTE(SUBSTRING(wxdata,4,2)),
                            INTE(SUBSTRING(wxdata,1,2)),
                            INTE("20" + SUBSTRING(wxdata,7,2))).



        IF wdtdd = 01/01/0001 THEN DO:
          PUT STREAM log-import  "*** reg" wnumreg
                  "Data Invalida = 01/01/0001 ***" wxite wxdata wxquant 
                   SKIP.
          NEXT.
          END.

        IF wdtdd LT TODAY THEN DO:
          DISPLAY STREAM log-import  "*** reg" wnumreg
                  "Data Passada ***" TODAY wxite wdtdd wxquant SKIP WITH NO-LABELS.
          NEXT.
          END.

        ASSIGN wqtdenew = INTE(wxquant).
        IF wqtdenew = 0 THEN DO:
          PUT STREAM log-import  "*** reg" wnumreg
                  "Quantidade Invalida ***" wxite wdtdd wxquant SKIP.
          NEXT.
          END.

        ASSIGN wexiste = 0.
        ASSIGN wobsoleto = 0.
        FOR 
        EACH   item
        FIELDS(item.it-codigo
               item.cod-obsoleto)
        WHERE  item.it-codigo = wxite
        NO-LOCK:          
          ASSIGN wexiste = 1.
          ASSIGN wobsoleto = item.cod-obsoleto.
          END.

        IF wexiste = 0 THEN DO:
          PUT STREAM log-import  "*** reg" wnumreg
                  "item nao cadastrado ***" wxite wdtdd wqtdenew SKIP.
          NEXT.
          END.

        IF wexiste = 1
        AND wobsoleto = 4
        THEN DO:
          PUT STREAM log-import  "*** reg" wnumreg
                  "item obsoleto ***" wxite wdtdd wqtdenew SKIP.
          NEXT.
          END.

        ASSIGN wordem = 0.
        FIND
        LAST  ord-prod
        WHERE ord-prod.it-codigo   = wxite
        AND   ord-prod.dt-inicio  LE wdtdd
        AND   ord-prod.dt-termino GE wdtdd
        AND   ord-prod.cd-planejado MATCHES(tt-param.wplanej)
        AND   ord-prod.estado     NE 7
        AND   ord-prod.estado     NE 8
        NO-LOCK NO-ERROR. 

        IF AVAILABLE ord-prod THEN DO:
          ASSIGN wordem = ord-prod.nr-ord-produ.
          END.
        IF NOT AVAILABLE ord-prod
        THEN DO:
         PUT  STREAM log-import   "*** reg" wnumreg
                  "Nao ha ordem ativa p/esta data e planejador ***"
                  wxite wdtdd wqtdenew SKIP.
          NEXT.
          END.


        IF CAN-FIND(lo-detord
           WHERE    lo-detord.it-codigo    = wxite
           AND      lo-detord.data         = wdtdd
           AND      lo-detord.turno        = ""
           AND      lo-detord.nr-ord-produ = wordem 
           NO-LOCK)
        THEN DO:
          PUT STREAM log-import  "*** reg" wnumreg
                  "Detalhe ja existe ***"
                  wxite wdtdd wqtdenew
                  "na ordem" wordem SKIP.
          NEXT.
          END.

        CREATE lo-detord.
        ASSIGN lo-detord.it-codigo    = wxite
               lo-detord.data         = wdtdd
               lo-detord.turno        = ""
               lo-detord.nr-ord-produ = wordem
               lo-detord.qtde         = wqtdenew.
       
         PUT STREAM log-import 
                "Ok reg" wnumreg
                lo-detord.it-codigo
                lo-detord.data
                lo-detord.qtde
                "na ordem"
                lo-detord.nr-ord-produ SKIP.

      END.
      INPUT CLOSE.



      //NEXT.
      /*.......................................fim de importacao...........*/

      OUTPUT STREAM log-import CLOSE.


END PROCEDURE.


/* DOS SILENT  NOTEPAD.EXE "c:/spool/log-importacao.txt".  */
          /* temp-directory */

{utils/retorna_diretorio_usuario.i}

