/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 ASSIGN
               wep              = '1'
               westab           = "1"
               wgera-novo       = NO
               wseq             = 0
               wdata-geracao    = TODAY
               wnr-pl           = ""
               wrellog          = "V:\spool\CONSIS-IMPORT.TXT"
               warq-importado   = " "
               procname         = " "
               wpasta           = 1.
               
 DISPLAY  wep westab wdata-geracao wnr-pl wrellog warq-importado wpasta WITH FRAME {&FRAME-NAME}.
 ENABLE ALL except wdata-geracao warq-importado WITH FRAME     {&FRAME-NAME}.
 APPLY "ENTRY" TO wep IN FRAME    {&FRAME-NAME}.
END PROCEDURE.


----
    DO: 
 ASSIGN wep westab wnr-pl wpasta wrellog.
  run utp/ut-acomp.p persistent set h-acomp.  
           {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
           run pi-inicializar in h-acomp (input "Gerando":U). 
          
         
 RUN roda.
     run pi-finalizar in h-acomp.
 END.
--
    /* roda */
    /*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/* Pasta selecionada para importa��o */
/*assign i-pasta-select = 1.  qual pasta est� o conteudo  */
assign i-pasta-select = wpasta. /* qual pasta est� o conteudo */

{office/office.i excel v-chr-excel-application} 
v-chr-excel-application:visible = false. 

assign v-chr-work-book = v-chr-excel-application:workbooks:open(procname) 
v-chr-work-sheet = v-chr-excel-application:sheets:item(i-pasta-select) 
v-int-ultimalinha = v-chr-excel-application:activesheet:usedrange:rows:count. 

do v-int-line = 5 to v-int-ultimalinha: 
assign 
v-cod-emitente = v-chr-work-sheet:range('A':U + string(v-int-line)):VALUE
v-it-codigo = v-chr-work-sheet:range('B':U + string(v-int-line)):VALUE
v-descricao = v-chr-work-sheet:range('C':U + string(v-int-line)):VALUE 
v-chr-campo-4 = v-chr-work-sheet:range('D':U + string(v-int-line)):VALUE   
v-chr-campo-5 = v-chr-work-sheet:range('E':U + string(v-int-line)):VALUE 
v-chr-campo-6 = v-chr-work-sheet:range('F':U + string(v-int-line)):VALUE 
v-chr-campo-7 = v-chr-work-sheet:range('G':U + string(v-int-line)):VALUE 
v-chr-campo-8 = v-chr-work-sheet:range('H':U + string(v-int-line)):VALUE 
v-qt-sem1 = v-chr-work-sheet:range('I':U + string(v-int-line)):VALUE 
v-qt-sem2 = v-chr-work-sheet:range('J':U + string(v-int-line)):VALUE 
v-qt-sem3 = v-chr-work-sheet:range('K':U + string(v-int-line)):VALUE 
v-qt-sem4 = v-chr-work-sheet:range('L':U + string(v-int-line)):VALUE 
v-qt-sem5 = v-chr-work-sheet:range('M':U + string(v-int-line)):VALUE 
v-qt-sem1-2 = v-chr-work-sheet:range('N':U + string(v-int-line)):VALUE
v-qt-sem2-2 = v-chr-work-sheet:range('O':U + string(v-int-line)):VALUE 
v-qt-sem3-2 = v-chr-work-sheet:range('P':U + string(v-int-line)):VALUE 
v-qt-sem4-2 = v-chr-work-sheet:range('Q':U + string(v-int-line)):VALUE 
v-qt-sem5-2 = v-chr-work-sheet:range('R':U + string(v-int-line)):VALUE 
v-mes1 = v-chr-work-sheet:range('S':U + string(v-int-line)):VALUE 
v-mes2 = v-chr-work-sheet:range('T':U + string(v-int-line)):VALUE.

create tt-dados. 
assign 
tt-dados.c-cod-emitente = int(v-cod-emitente)    /* cod-emitente */
tt-dados.c-it-codigo    = (v-it-codigo)       /* cod-item */
tt-dados.c-descricao    = (v-descricao)       /* descricao */
tt-dados.campo-4        = INT(v-chr-campo-4)    
tt-dados.campo-5        = INT(v-chr-campo-5) 
tt-dados.campo-6        = INT(v-chr-campo-6) 
tt-dados.campo-7        = INT(v-chr-campo-7)
tt-dados.campo-8        = INT(v-chr-campo-8) 
tt-dados.c-qt-sem1      = INT(v-qt-sem1)   /* qt-sem1*/
tt-dados.c-qt-sem2      = INT(v-qt-sem2)  /* qt-sem]2 */
tt-dados.c-qt-sem3      = INT(v-qt-sem3)  /* qt-sem3 */
tt-dados.c-qt-sem4      = INT(v-qt-sem4)  /* qt-sem4 */
tt-dados.c-qt-sem5      = INT(v-qt-sem5)   /* qt-sem5 */
tt-dados.c-qt-sem1-2    = INT(v-qt-sem1-2)  /* qt-sem1-2 */
tt-dados.c-qt-sem2-2    = INT(v-qt-sem2-2)  /* qt-sem2-2 */
tt-dados.c-qt-sem3-2    = INT(v-qt-sem3-2)  /* qt-sem3-2 */
tt-dados.c-qt-sem4-2    = INT(v-qt-sem4-2)  /* qt-sem4-2 */
tt-dados.c-qt-sem5-2    = INT(v-qt-sem5-2)  /* qt-sem5-2 */
tt-dados.c-mes1         = INT(v-mes1)  /* mes1 */
tt-dados.c-mes2         = INT(v-mes2). /* mes2 */

/* Aqui est� limitado � 3 campos, por�m pode-se criar de acordo com a necessidade */ 


end. 

v-chr-excel-application:workbooks:application:quit(). 

DELETE object v-chr-excel-application no-error. 
DELETE object v-chr-work-sheet no-error. 
DELETE object v-chr-work-book no-error. 
RUN cria-tit.
RUN gera-plano.


/* Aqui pode-se fazer o que quiser com os dados importados para a temp-table */ 
OUTPUT TO "V:\spool\dados-plano.txt" paged.
for each tt-dados BREAK BY v-cod-emitente BY v-it-codigo:
 disp tt-dados WITH FRAME f-dados WIDTH 300 DOWN.
 DOWN WITH FRAME f-dados.
end. 

END PROCEDURE.


----/* gera plano */
    /*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    /* ROTINA PARA GERAR PLANO */

    FOR EACH tt-dados BREAK BY c-cod-emitente BY c-it-codigo: 

      run pi-acompanhar in h-acomp ("Emitente: " + STRING(c-cod-emitente) 
                                    + " Item: " 
                                    + STRING(c-it-codigo)).                 
      FIND FIRST item-fornec WHERE item-fornec.cod-emitente = tt-dados.c-cod-emitente
                             AND   item-fornec.it-codigo    = tt-dados.c-it-codigo
                             AND   item-fornec.ativo        = yes
                             NO-LOCK NO-ERROR.
         IF NOT AVAILABLE(item-fornec) THEN DO:
           MESSAGE "Relacionamento Item/Fornecedor nao encontrado: " tt-dados.c-it-codigo " / " tt-dados.c-cod-emitente
                   SKIP "ESTE ITEM N�O SER� IMPORTADO!"
           VIEW-AS ALERT-BOX.
           PAUSE. 
           NEXT.
           END.
      FIND FIRST ITEM WHERE item.it-codigo = tt-dados.c-it-codigo
                      AND   ITEM.cod-obsoleto <> 1 NO-LOCK no-error.
      IF AVAILABLE ITEM THEN DO:
        MESSAGE "Item:" tt-dados.c-it-codigo ", N�O EST� COM SITUACAO ATIVA NO CADASTRO." 
                 skip
                 "ITEM N�O SER� IMPORTADO!" VIEW-AS ALERT-BOX.
        NEXT.
        END.

      IF FIRST-OF(c-cod-emitente) THEN  ASSIGN wseq = 0.
   
      /* procura e se o plano nao existe */
      FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente =  tt-dados.c-cod-emitente
                            AND   lo-matplano.emp          = wep
                            AND   lo-matplano.estab        = westab
                            AND   lo-matplano.nr-pl        = wnr-pl 
                            AND   lo-matplano.it-codigo    = tt-dados.c-it-codigo
                            NO-ERROR.
   
    IF AVAILABLE lo-matplano THEN DO:
     MESSAGE "Plano: " wnr-pl "Para o Fornecedor:" v-cod-emitente 
             "Item:" c-it-codigo "j� cadastrado! Deseja sobrepor:?"
              VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wgera-novo.
     IF wgera-novo = NO THEN NEXT.
     END.
    
     IF NOT AVAILABLE lo-matplano THEN CREATE lo-matplano.
     ASSIGN wseq                      = wseq + 1.

     IF  tt-dados.c-qt-sem1 = ? THEN tt-dados.c-qt-sem1  = 0.
     IF  tt-dados.c-qt-sem2 = ? THEN tt-dados.c-qt-sem2  = 0.
     IF  tt-dados.c-qt-sem3 = ? THEN tt-dados.c-qt-sem3  = 0.
     IF  tt-dados.c-qt-sem4 = ? THEN  tt-dados.c-qt-sem4 = 0.
     IF  tt-dados.c-qt-sem5 = ?  THEN  tt-dados.c-qt-sem5 = 0.
     IF  tt-dados.c-qt-sem1-2 = ? THEN tt-dados.c-qt-sem1-2 = 0.
     IF  tt-dados.c-qt-sem2-2 = ? THEN tt-dados.c-qt-sem2-2 = 0.
     IF  tt-dados.c-qt-sem3-2 = ? THEN tt-dados.c-qt-sem3-2 = 0.
     IF  tt-dados.c-qt-sem4-2 = ? THEN tt-dados.c-qt-sem4-2 = 0.
     IF  tt-dados.c-qt-sem5-2 = ? THEN tt-dados.c-qt-sem5-2 = 0.
     IF  tt-dados.c-mes1      = ? THEN tt-dados.c-mes1 = 0.
     IF  tt-dados.c-mes2      = ? THEN tt-dados.c-mes2 = 0.
     
     ASSIGN   lo-matplano.cod-emitente  = tt-dados.c-cod-emitente
               lo-matplano.it-codigo     = tt-dados.c-it-codigo
               lo-matplano.emp           = wep
               lo-matplano.estab         = westab
               lo-matplano.nr-pl         = wnr-pl
               lo-matplano.seq-it        = wseq
               lo-matplano.data-geracao  = TODAY
               lo-matplano.qt-sem1       = tt-dados.c-qt-sem1
               lo-matplano.qt-sem2       = tt-dados.c-qt-sem2
               lo-matplano.qt-sem3       = tt-dados.c-qt-sem3
               lo-matplano.qt-sem4       = tt-dados.c-qt-sem4
               lo-matplano.qt-sem5       = tt-dados.c-qt-sem5
               lo-matplano.qt-sem1-2     = tt-dados.c-qt-sem1-2
               lo-matplano.qt-sem2-2     = tt-dados.c-qt-sem2-2
               lo-matplano.qt-sem3-2     = tt-dados.c-qt-sem3-2
               lo-matplano.qt-sem4-2     = tt-dados.c-qt-sem4-2
               lo-matplano.qt-sem5-2     = tt-dados.c-qt-sem5-2
               lo-matplano.mes1          = tt-dados.c-mes1
               lo-matplano.mes2          = tt-dados.c-mes2.
      run proc_nota.
     END.
   /* gera rel. para conferencia */
   OUTPUT TO VALUE(wrellog) PAGED.
     DISPLAY "**** Nome do arquivo importado:" wrellog "****"
     SKIP WITH NO-LABEL.
     FOR EACH lo-matplano 
       WHERE emp            = wep
       AND   estab          = westab
       AND   nr-pl          = wnr-pl 
       AND   data-geracao   = wdata-geracao NO-LOCK:
       DISPLAY
               lo-matplano.emp          
               lo-matplano.estab        
               lo-matplano.nr-pl  
               lo-matplano.data-geracao 
               lo-matplano.cod-emitente 
               lo-matplano.seq-it  
               lo-matplano.it-codigo 
               nro-docto
               data-nota
               lo-matplano.qt-sem1      
               lo-matplano.qt-sem2       
               lo-matplano.qt-sem3       
               lo-matplano.qt-sem4      
               lo-matplano.qt-sem5       
               lo-matplano.qt-sem1-2     
               lo-matplano.qt-sem2-2     
               lo-matplano.qt-sem3-2    
               lo-matplano.qt-sem4-2    
               lo-matplano.qt-sem5-2  
               lo-matplano.mes1         
               lo-matplano.mes2
           WITH WIDTH 300.
     
     END.
    OUTPUT CLOSE.
  
/*     MESSAGE "FIM DA IMPORTA��O." VIEW-AS ALERT-BOX. */
    ENABLE ALL WITH FRAME  {&FRAME-NAME}.
    
END PROCEDURE.


