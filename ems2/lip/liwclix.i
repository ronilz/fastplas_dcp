/***********************************************************************
**
** liwclix.p - Monta WORKFILE de clientes: showcli             
**
**              17/10/2000 - BENE   Programa novo.
**
***********************************************************************/
ASSIGN wgerpedida = SUBSTRING(STRING(YEAR(wdtpgm),"9999"),03,02)
                            + STRING(MONTH(wdtpgm),"99")
                            + STRING(DAY(wdtpgm),"99")
                            + "000000".


/*.............................inicio limpeza da TABELA DE CLIENTES..*/
FOR EACH showcli:
  DELETE showcli.
  END.
ASSIGN wregcli = 0.  
/*.............................fim de limpeza da TABELA DE CLIENTES..*/


/*...........................inicio LO-CLIPGM NA TABELA DE CLIENTES..*/
IF  (wfa-operacao NE "GMB")
AND (wfa-operacao NE "JIT")
THEN DO:

  IF wqualpgm = "U" THEN DO:
    FIND LAST mgfas.lo-clipgm 
        WHERE mgfas.lo-clipgm.codrefsbp     =    wcodrefaux             
        AND   mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli)
        USE-INDEX ctrpcl
        NO-LOCK
        NO-ERROR.
    END.
  IF wqualpgm = "D" THEN DO:
    FIND LAST mgfas.lo-clipgm 
        WHERE mgfas.lo-clipgm.codrefsbp     =    wcodrefaux             
        AND   mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli)
        AND   mgfas.lo-clipgm.data-pgm     LE    wdtpgm
        USE-INDEX ctrpcl
        NO-LOCK
        NO-ERROR.
    END.

  IF AVAILABLE mgfas.lo-clipgm THEN DO:
    REPEAT:
      ASSIGN wregcli = wregcli + 1.
      CREATE showcli.
      ASSIGN showcli.numreg     = wregcli
             showcli.codarq     = "lo-clipgm"
             showcli.nome-abrev = mgfas.lo-clipgm.nome-abrev         
             showcli.data-pgm   = mgfas.lo-clipgm.data-pgm   
             showcli.cod-pgm    = mgfas.lo-clipgm.cod-pgm
             showcli.tipo-pgm   = mgfas.lo-clipgm.tipo-pgm
             showcli.nf         = mgfas.lo-clipgm.nf
             showcli.dtnf       = mgfas.lo-clipgm.dtnf.
      FIND  mgadm.emitente
      WHERE mgadm.emitente.nome-abrev = showcli.nome-abrev
      NO-LOCK NO-ERROR.
      IF AVAILABLE mgadm.emitente THEN DO:
        ASSIGN showcli.cod-emitente = mgadm.emitente.cod-emitente.
        END.

      IF wqualpgm = "U" THEN DO:
        FIND LAST mgfas.lo-clipgm 
            WHERE mgfas.lo-clipgm.codrefsbp     =    wcodrefaux
            AND   mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli)
            AND   mgfas.lo-clipgm.nome-abrev   LT    showcli.nome-abrev
            USE-INDEX ctrpcl
            NO-LOCK
            NO-ERROR.
        END.

      IF wqualpgm = "D" THEN DO:
        FIND LAST mgfas.lo-clipgm 
            WHERE mgfas.lo-clipgm.codrefsbp     =    wcodrefaux
            AND   mgfas.lo-clipgm.nome-abrev MATCHES(tt-param.wcli)
            AND   mgfas.lo-clipgm.nome-abrev   LT    showcli.nome-abrev
            AND   mgfas.lo-clipgm.data-pgm     LE    wdtpgm
            USE-INDEX ctrpcl
            NO-LOCK
            NO-ERROR.
        END.

      IF NOT AVAILABLE mgfas.lo-clipgm THEN LEAVE.
      END.
    END.
  END.
/*...........................fim do LO-CLIPGM NA TABELA DE CLIENTES..*/


/*.............................inicio LO-RN12 NA TABELA DE CLIENTES..*/
IF tt-param.wprv = "Entrega (GM-RND012)"
AND wfa-operacao = "GMB" 
THEN DO:
 
  ASSIGN wgeri = ""
         wgerf = "".

  IF wqualpgm = "U" THEN DO:
    FIND LAST lo-rn12
        WHERE lo-rn12.itemcli = wcodrefaux
        USE-INDEX ig
        NO-LOCK NO-ERROR.
    END.

  IF wqualpgm = "D" THEN DO:
    FIND LAST lo-rn12
        WHERE lo-rn12.itemcli = wcodrefaux
        AND   lo-rn12.geracao GE wgerpedida
        USE-INDEX ig
        NO-LOCK NO-ERROR.
    END.

  IF AVAILABLE lo-rn12 THEN DO:
    ASSIGN wgeri = lo-rn12.geracao
           wgerf = wgeri.
    ASSIGN SUBSTRING(wgeri,07,06) = "000000"
           SUBSTRING(wgerf,07,06) = "999999".

    FOR EACH lo-rn12
       WHERE lo-rn12.itemcli  = wcodrefaux
       AND   lo-rn12.geracao GE wgeri
       AND   lo-rn12.geracao LE wgerf
       USE-INDEX gidh
       NO-LOCK:
               
      IF wcaller NE "S" THEN
      PUT SCREEN ROW 24 COLUMN 01 "tabela GMB " 
                                + STRING(lo-rn12.geracao)
                          + " " + STRING(wcodrefaux)
                                + FILL(" ",16).    

      ASSIGN wlocabrev = "".

      IF lo-rn12.locdestino = "72664"
      OR lo-rn12.locdestino = "72668"
      OR lo-rn12.locdestino = "72669"
      OR lo-rn12.locdestino = "72677"
      THEN ASSIGN wlocabrev = "GMB SJC".
              
      IF lo-rn12.locdestino = "72667"
      OR lo-rn12.locdestino = "72671"
      THEN ASSIGN wlocabrev = "GMB-SCS".
              
      IF lo-rn12.locdestino = "72681"
      THEN ASSIGN wlocabrev = "GMB.SCS".


      /*-----------------------------inicio proc.25-08-2003------*/
      FIND FIRST mgfas.lo-clidef
           WHERE mgfas.lo-clidef.codrefsbp = wcodrefaux
           NO-LOCK NO-ERROR.
      IF AVAILABLE mgfas.lo-clidef THEN DO:
        ASSIGN wlocabrev = mgfas.lo-clidef.nome-abrev.
        END.
      /*-----------------------------fim de proc.25-08-2003------*/



      IF wlocabrev MATCHES(tt-param.wcli) THEN DO:
                
        FIND FIRST showcli
             WHERE showcli.codarq     = "lo-rn12"
             AND   showcli.nome-abrev = wlocabrev
             NO-ERROR.
                     
        IF NOT AVAILABLE showcli THEN DO:
                    
          ASSIGN wseculo = "20".
          IF INTE(SUBSTRING(lo-rn12.geracao,01,02)) GT 50 THEN DO:
            ASSIGN wseculo = "19".
            END.
                    
          ASSIGN wdtx = DATE(INTE(SUBSTRING(lo-rn12.geracao,03,02)),
                             INTE(SUBSTRING(lo-rn12.geracao,05,02)),
                             INTE(STRING(wseculo)
                                + SUBSTRING(lo-rn12.geracao,01,02))).
          ASSIGN wnfx = 0.
          FIND LAST it-nota-fisc
              WHERE it-nota-fisc.it-codigo = witemaux
              AND   it-nota-fisc.nome-ab-cli = wlocabrev
              AND   it-nota-fisc.dt-cancela  = ?
              AND   it-nota-fisc.baixa-estoq = yes
              AND   it-nota-fisc.dt-emis-nota LT wdtx
              USE-INDEX ch-item-nota
              NO-LOCK
              NO-ERROR.
          IF AVAILABLE it-nota-fisc THEN DO:
            ASSIGN wnfx = INTE(it-nota-fisc.nr-nota-fis).
            END.
              

          ASSIGN wregcli = wregcli + 1.
          CREATE showcli.
          ASSIGN showcli.numreg     = wregcli
                 showcli.codarq     = "lo-rn12"
                 showcli.nome-abrev = wlocabrev
                 showcli.geri       = wgeri
                 showcli.gerf       = wgerf
                 showcli.data-pgm   = wdtx 
                 showcli.cod-pgm    = SUBSTRING(lo-rn12.geracao,07,06)
                 showcli.tipo-pgm   = " "
                 showcli.nf         = wnfx
                 showcli.dtnf       = showcli.data-pgm - 1.
          FIND  mgadm.emitente
          WHERE mgadm.emitente.nome-abrev = showcli.nome-abrev
          NO-LOCK NO-ERROR.
          IF AVAILABLE mgadm.emitente THEN DO:
            ASSIGN showcli.cod-emitente = mgadm.emitente.cod-emitente.
            END.

          END.   

        IF LOOKUP(lo-rn12.locdestino,showcli.loclista) = 0
        THEN DO:
          IF LENGTH(showcli.loclista) GT 0 THEN DO:
            ASSIGN showcli.loclista
                 = showcli.loclista + ",".
            END.
          ASSIGN showcli.loclista
               = showcli.loclista + lo-rn12.locdestino.
          END.
                   
        END.
      END.
    END.
  END.
/*.............................fim do LO-RN12 NA TABELA DE CLIENTES..*/


/*.............................inicio LO-RN01 NA TABELA DE CLIENTES..*/
IF tt-param.wprv = "Produ��o(GM-RND001)"
AND wfa-operacao = "GMB" 
THEN DO:
          
  ASSIGN wgeri = ""
         wgerf = ""
         wprogatual = "".
                 
  IF wqualpgm = "U" THEN DO:
    FIND FIRST lo-rn01
         WHERE lo-rn01.itemcli = wcodrefaux
         NO-LOCK
         NO-ERROR.
    IF NOT AVAILABLE mgfas.lo-rn01 THEN NEXT.       

    FIND LAST lo-rn01
        WHERE lo-rn01.itemcli = wcodrefaux
        USE-INDEX gid
        NO-LOCK NO-ERROR.
    END.

  IF wqualpgm = "D" THEN DO:
    FIND LAST lo-rn01
        WHERE lo-rn01.itemcli      = wcodrefaux
        AND   lo-rn01.dtprogatual GE wdtpgm
        USE-INDEX gid
        NO-LOCK NO-ERROR.
    END.


  IF AVAILABLE lo-rn01 THEN DO:
    ASSIGN wgeri  = lo-rn01.geracao
           wgerf = wgeri.
    ASSIGN SUBSTRING(wgeri,07,06) = "000000"
           SUBSTRING(wgerf,07,06) = "999999".

    ASSIGN wprogatual = lo-rn01.progatual.

    FOR EACH lo-rn01
       WHERE lo-rn01.itemcli  = wcodrefaux
       AND   lo-rn01.geracao GE wgeri
       AND   lo-rn01.geracao LE wgerf
       AND   lo-rn01.progatual = wprogatual
       USE-INDEX gid
       NO-LOCK:
               
      IF wcaller NE "S" THEN
      PUT SCREEN ROW 24 COLUMN 01 "tabela GMB " 
                                + STRING(lo-rn01.geracao)
                          + " " + STRING(wcodrefaux)
                                + FILL(" ",16).    

      ASSIGN wlocabrev = "".

      IF lo-rn01.locdestino = "72664"
      OR lo-rn01.locdestino = "72668"
      OR lo-rn01.locdestino = "72669"
      OR lo-rn01.locdestino = "72677"
      THEN ASSIGN wlocabrev = "GMB SJC".
              
      IF lo-rn01.locdestino = "72667"
      OR lo-rn01.locdestino = "72671"
      THEN ASSIGN wlocabrev = "GMB-SCS".
              
      IF lo-rn01.locdestino = "72681"
      THEN ASSIGN wlocabrev = "GMB.SCS".

      IF wlocabrev MATCHES(tt-param.wcli) THEN DO:
                
        FIND FIRST showcli
             WHERE showcli.codarq     = "lo-rn01"
             AND   showcli.nome-abrev = wlocabrev
             NO-ERROR.
                     
        IF NOT AVAILABLE showcli THEN DO:
                    
          ASSIGN wseculo = "20".
          IF INTE(SUBSTRING(lo-rn01.geracao,01,02)) GT 50 THEN DO:
            ASSIGN wseculo = "19".
            END.
                    
          ASSIGN wdtx = DATE(INTE(SUBSTRING(lo-rn01.geracao,03,02)),
                             INTE(SUBSTRING(lo-rn01.geracao,05,02)),
                             INTE(STRING(wseculo)
                                + SUBSTRING(lo-rn01.geracao,01,02))).
          ASSIGN wnfx = 0.
          FIND LAST it-nota-fisc
              WHERE it-nota-fisc.it-codigo = witemaux
              AND   it-nota-fisc.nome-ab-cli = wlocabrev
              AND   it-nota-fisc.dt-cancela  = ?
              AND   it-nota-fisc.baixa-estoq = yes
              AND   it-nota-fisc.dt-emis-nota LT wdtx
              USE-INDEX ch-item-nota
              NO-LOCK
              NO-ERROR.
          IF AVAILABLE it-nota-fisc THEN DO:
            ASSIGN wnfx = INTE(it-nota-fisc.nr-nota-fis).
            END.


          ASSIGN wregcli = wregcli + 1.
          CREATE showcli.
          ASSIGN showcli.numreg     = wregcli
                 showcli.codarq     = "lo-rn01"
                 showcli.nome-abrev = wlocabrev
                 showcli.geri       = wgeri
                 showcli.gerf       = wgerf
                 showcli.progatual  = lo-rn01.progatual
                 showcli.data-pgm   = wdtx
                 showcli.cod-pgm    = SUBSTRING(lo-rn01.geracao,07,06)
                 showcli.tipo-pgm   = " "
                 showcli.nf         = wnfx
                 showcli.dtnf       = showcli.data-pgm - 1.
          FIND  mgadm.emitente
          WHERE mgadm.emitente.nome-abrev = showcli.nome-abrev
          NO-LOCK NO-ERROR.
          IF AVAILABLE mgadm.emitente THEN DO:
            ASSIGN showcli.cod-emitente = mgadm.emitente.cod-emitente.
            END.


          END.   

        IF LOOKUP(lo-rn01.locdestino,showcli.loclista) = 0
        THEN DO:
          IF LENGTH(showcli.loclista) GT 0 THEN DO:
            ASSIGN showcli.loclista
                 = showcli.loclista + ",".
            END.
          ASSIGN showcli.loclista
               = showcli.loclista + lo-rn01.locdestino.
          END.
                    
        END.
      END.
    END.
  END.
/*.............................fim do LO-RN01 NA TABELA DE CLIENTES..*/


/*.............................inicio lo-jitlog NA TABELA DE CLIENTES..*/
IF wfa-operacao = "JIT" 
THEN DO:
 
          
  IF wqualpgm = "U" THEN DO:
    FIND FIRST lo-jitlog
         WHERE lo-jitlog.peca   = wcodrefaux
         AND   lo-jitlog.obs    = ""
         AND   lo-jitlog.dt-ac NE ?
         AND   lo-jitlog.dt-sd  = ?
         USE-INDEX ch-02
         NO-LOCK
         NO-ERROR.
    END.

  IF wqualpgm = "D" THEN DO:
    FIND FIRST lo-jitlog
         WHERE lo-jitlog.peca   = wcodrefaux
         AND   lo-jitlog.obs    = ""      
         AND   lo-jitlog.dt-ac GE wdtpgm
         USE-INDEX ch-02
         NO-LOCK NO-ERROR.
    END.


  IF AVAILABLE lo-jitlog THEN DO:

    IF wcaller NE "S" THEN
    PUT SCREEN ROW 24 COLUMN 01 "tabela JIT " 
                              + STRING(lo-jitlog.dt-ac)
                        + " " + STRING(wcodrefaux)
                              + FILL(" ",16).    

    ASSIGN wlocabrev = "VW SBC".

    IF wlocabrev MATCHES(tt-param.wcli) THEN DO:
                    
      ASSIGN wdtx = wdtdd.           

      ASSIGN wnfx = 0.
      FIND LAST it-nota-fisc
          WHERE it-nota-fisc.it-codigo = witemaux
          AND   it-nota-fisc.nome-ab-cli = wlocabrev
          AND   it-nota-fisc.dt-cancela  = ?
          AND   it-nota-fisc.baixa-estoq = yes
          AND   it-nota-fisc.dt-emis-nota LT wdtx            
          USE-INDEX ch-item-nota
          NO-LOCK
          NO-ERROR.
      IF AVAILABLE it-nota-fisc THEN DO:
        ASSIGN wnfx = INTE(it-nota-fisc.nr-nota-fis).
        END.

      ASSIGN wregcli = wregcli + 1.
      CREATE showcli.
      ASSIGN showcli.numreg     = wregcli
             showcli.codarq     = "lo-jitlog"
             showcli.nome-abrev = wlocabrev
             showcli.data-pgm   = wdtx             
             showcli.cod-pgm    = ""                                  
             showcli.tipo-pgm   = ""
             showcli.nf         = wnfx
             showcli.dtnf       = showcli.data-pgm - 1.
      FIND  mgadm.emitente
      WHERE mgadm.emitente.nome-abrev = showcli.nome-abrev
      NO-LOCK NO-ERROR.
      IF AVAILABLE mgadm.emitente THEN DO:
        ASSIGN showcli.cod-emitente = mgadm.emitente.cod-emitente.
        END.


      END.   

    END.
  END.
/*.............................fim do lo-jitlog NA TABELA DE CLIENTES..*/




