&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
** 19/07/11 - executa para todos os estabelecimentos
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI9002RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wemi-ini         AS INTEGER FORMAT ">>>>>>9"
    FIELD wemi-fim         AS INTEGER FORMAT ">>>>>>9"
    FIELD wep              AS INTE FORMAT ">>9"
    FIELD westab           AS CHAR FORMAT "X(03)"
    FIELD westab-1         AS CHAR FORMAT "X(03)"
    FIELD wnr-ini          AS CHAR FORMAT "999999X".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS valeria  */

/* Variaveis referente a Titulos */
DEFINE VARIABLE wpl-ant         LIKE lo-matplano.nr-pl.                    
DEFINE VARIABLE wmsg            AS CHAR FORMAT "X(40)".
DEFINE VARIABLE wmsg1           AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wlinha          AS CHAR FORMAT "X(138)".
DEFINE VARIABLE wlinha1         AS CHAR FORMAT "X(138)".
DEFINE VARIABLE wpag            AS INTE FORMAT "9999".

/* Workfile para guardar numero de planos solicitados */
DEFINE WORKFILE nroplano
  FIELD wcod-emitente           LIKE lo-matplano.cod-emitente 
  FIELD wnr-pl                  LIKE lo-matplano.nr-pl.

DEFINE VARIABLE wemp            LIKE estabelec.nome.
DEFINE VARIABLE wemit           LIKE emitente.nome-ab.
DEFINE VARIABLE wcod-emit       LIKE lo-matplano.cod-emitente.
DEFINE VARIABLE wconf-imp       AS LOGICAL FORMAT "Sim/Nao".

/* Buffer para pesquisa de mensagem de reprograma */
DEFINE BUFFER  b-matplano       FOR lo-matplano.
DEFINE VARIABLE wtipo           AS INTEGER NO-UNDO. /* tipo de impressora */

DEFINE VARIABLE wcont-estab     AS  INTEGER FORMAT "99".
/* FIM VARIAVEIS valeria*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PROCEDIMENTOS ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Relat�rio Plano de Materiais".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




 /*------------------------------------------------------------------------------
  Purpose:     roda_prog
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
PROCEDURE roda_prog.
   FORM 
  wlinha1
  SKIP
  "Cod.Ite            NfCorte     ATRASO"
  lo-matdatas.titsem1 AT 42 
  lo-matdatas.titsem2 
  lo-matdatas.titsem3       
  lo-matdatas.titsem4 
  lo-matdatas.titsem5 
  SKIP
  "_________" AT 42
  "_________" AT 56
  "_________" AT 70
  "_________" AT 84
  "_________" AT 98

  SKIP
  lo-matplano.it-codigo "" 
  lo-matplano.nro-docto  
  lo-matplano.imediato  
  lo-matplano.qt-sem1  AT 42
  lo-matplano.qt-sem2  AT 56
  lo-matplano.qt-sem3  AT 70
  lo-matplano.qt-sem4  AT 84 
  lo-matplano.qt-sem5  AT 98
  SKIP
  item.descricao-1
  lo-matdatas.titsem1-2 AT 42 
  lo-matdatas.titsem2-2 
  lo-matdatas.titsem3-2       
  lo-matdatas.titsem4-2 
  lo-matdatas.titsem5-2       
  lo-matdatas.titmes1 
  lo-matdatas.titmes2
  item.descricao-2      AT 01
  /*"__________________ _______ _________"*/
  "_________" AT 42
  "_________" AT 56
  "_________" AT 70
  "_________" AT 84
  "_________" AT 98
  "_________" AT 112
  "_________" AT 126
  SKIP
  lo-matplano.qt-sem1-2 AT 42 
  lo-matplano.qt-sem2-2 AT 56
  lo-matplano.qt-sem3-2 AT 70
  lo-matplano.qt-sem4-2 AT 84 
  lo-matplano.qt-sem5-2 AT 98
  lo-matplano.mes1      AT 112
  lo-matplano.mes2      AT 126
  wlinha
  SKIP(1)
HEADER
/*   wemp                                     */
/*   string(TODAY,"99/99/9999")               */
/*   string(TIME,"HH:MM:SS")                  */
/*   "Pag.:" AT 126 PAGE-NUMBER FORMAT ">>99" */
/*   SKIP                                     */
  "*** PLANO DE ENTREGA DE MATERIAIS NO.: "  AT 22 tt-param.wnr-ini FORMAT "99/9999X"
  "Empresa: " tt-param.wep "Estabelec.: "STRING(wcont-estab) "***"
  SKIP(1)
  "FORNECEDOR: "         AT 51 wcod-emit wemit
  WITH FRAME f-plano NO-LABELS DOWN WIDTH 140.

/* Form Rodape ------------------------------------------------------------*/
FORM HEADER
  "Este Plano substitui o Plano de Entrega de Materiais: "
   wpl-ant AT  70 FORMAT "99/9999X"
   "referente aos itens acima." AT 90
 WITH NO-LABELS WIDTH 150 NO-BOX FRAME f-rod PAGE-BOTTOM.
/* imprimir */
ASSIGN tt-param.wnr-ini = CAPS(tt-param.wnr-ini).
  IF LENGTH(tt-param.wnr-ini) > 6 THEN VIEW FRAME f-rod.
/* executa o procedimento at� terminar os estabelecimentos - sol. rosa - 190711*/
  DO wcont-estab = integer(tt-param.westab) TO  integer(tt-param.westab-1):
 
  /* estabelecimento 1 */
  FIND FIRST estabelec WHERE estabelec.ep-codigo   = string(tt-param.wep)
                       AND   estabelec.cod-estabel = STRING(wcont-estab)
                       NO-LOCK.
 /* MESSAGE STRING(wcont-estab) estabelec.cod-estabel  VIEW-AS ALERT-BOX.*/
  ASSIGN wemp  = estabelec.nome.
  FOR EACH lo-matplano  WHERE lo-matplano.cod-emitente  GE tt-param.wemi-ini      
                        AND   lo-matplano.cod-emitente  LE tt-param.wemi-fim
                            AND   lo-matplano.emp       = tt-param.wep
                        AND   lo-matplano.estab         = STRING(wcont-estab)
                   
                        AND   lo-matplano.nr-pl         = tt-param.wnr-ini
                        NO-LOCK BREAK BY lo-matplano.estab   BY lo-matplano.cod-emitente
                        BY lo-matplano.it-codigo:
    run pi-acompanhar in h-acomp ("Estab.: " + lo-matplano.estab + " Plano: " + STRING(lo-matplano.nr-pl) + " Item: " + STRING(lo-matplano.it-codigo)).
    IF FIRST-OF(lo-matplano.cod-emitente) THEN DO:
      PAGE.
      END.
    ASSIGN wpl-ant = " ".
    /* Verifica se e' reprograma - Ex. 031999A reprograma de 031999 */
    IF LENGTH(lo-matplano.nr-pl) > 6 THEN DO:
      FIND FIRST b-matplano WHERE b-matplano.cod-emitente  = 
                                  lo-matplano.cod-emitente
                            AND   b-matplano.emp           = 
                                  lo-matplano.emp
                            AND   b-matplano.estab         =
                                  lo-matplano.estab
                            AND   b-matplano.nr-pl         = 
                                  lo-matplano.nr-pl
                                  NO-LOCK NO-ERROR.
                        
      FIND PREV b-matplano  WHERE b-matplano.cod-emitente  = 
                                  lo-matplano.cod-emitente
                            AND   b-matplano.emp          = 
                                  lo-matplano.emp
                            AND   b-matplano.estab        =
                                  lo-matplano.estab
                            AND   b-matplano.nr-pl BEGINS
                                  (SUBSTRING(lo-matplano.nr-pl,1,6))
                            NO-LOCK NO-ERROR.
      IF AVAILABLE b-matplano THEN DO:
        ASSIGN wpl-ant = b-matplano.nr-pl.
        END.
      END.
        
        
    FIND FIRST emitente WHERE emitente.cod-emitente = lo-matplano.cod-emitente
                        NO-LOCK NO-ERROR.
    ASSIGN wemit     = emitente.nome-ab
           wcod-emit = lo-matplano.cod-emitente.
    FIND FIRST item    WHERE item.it-codigo = lo-matplano.it-codigo NO-LOCK
                       NO-ERROR.     
    FIND FIRST lo-matdatas WHERE lo-matdatas.emp   = lo-matplano.emp
                           AND   lo-matdatas.estab = lo-matplano.estab
                           AND   lo-matdatas.nr-pl =
                                 SUBSTRING(lo-matplano.nr-pl,1,6)
                                 NO-LOCK NO-ERROR.
    
  
    DISPLAY /* linha 1 */
            wlinha1
            lo-matdatas.titsem1 lo-matdatas.titsem2 
            lo-matdatas.titsem3 lo-matdatas.titsem4 
            lo-matdatas.titsem5
            lo-matplano.it-codigo lo-matplano.nro-docto 
            lo-matplano.imediato 
            lo-matplano.qt-sem1 lo-matplano.qt-sem2 lo-matplano.qt-sem3
            lo-matplano.qt-sem4 lo-matplano.qt-sem5 
            /* linha 2 */
            item.descricao-1 
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2 
            lo-matdatas.titsem5-2 lo-matdatas.titmes1 
            lo-matdatas.titmes2
            item.descricao-2
            lo-matplano.qt-sem1-2 lo-matplano.qt-sem2-2 lo-matplano.qt-sem3-2
            lo-matplano.qt-sem4-2 lo-matplano.qt-sem5-2 lo-matplano.mes1
            lo-matplano.mes2 
            wlinha    
            WITH FRAME f-plano WIDTH 140 OVERLAY NO-LABELS DOWN.
    DOWN WITH FRAME f-plano.
    IF LAST-OF(lo-matplano.cod-emitente) THEN DO:
    IF LINE-COUNTER > 57 THEN PAGE. 
    DISPLAY 
    SKIP(1)
    "Se em 02 dias n�o houver manifesta��o por parte do Fornecedor,"  AT 25
    SKIP
    "ser� considerado totalmente aceito o Plano de entrega de Materiais." AT 23
    SKIP(3)
       "___________________________"    AT 23
       SPACE(12) 
       "____________________________"
  SKIP "   Analisado e Aprovado    "    AT 23
       SPACE(12) 
       "            P.C.P. / Log�stica"
  SKIP " Departamento Comercial"       AT 24 WITH FRAME f-mens WIDTH 150.
    END.
    END.
  END.
END PROCEDURE.




