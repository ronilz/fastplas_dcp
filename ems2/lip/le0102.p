/***********************************************************************
**
** le0102.p  - Altera Parcelas [3a.Fase]             
**
**             07/12/2005 11:00  - BENE   programa novo.
**
**.....................................................................
**             DEFINICAO
**             arquivo de entrada: o mesmo do CC0604
**             Seleciona registros AP00 com:
**               . posicao 1-3 da observacao = "[A]"
**                                         e = "[I]"
**             Le tabela prazo-compra com:
**               . numero da ordem = numero da ordem do AP00
**               . parcela         = parcela do AP00
**             Ignora registro se:
**               .    data de entrega diferente da data entrega do AP00
**               . ou quantidade      diferente da quantidade   do AP00
**               Isto significa que a parcela ja esta alterada.
**
**             Para os registros selecionados
**             Le tabela alt-ped com:
**               . numero do pedido  |
**                 numero da ordem   |
**                 parcela           > iguais ao do AP00
**                 data de alteracao |
**                 hora de alteracao |
**                 observacao        |
**             Ignorar registro AP00 
**             se nao encontrar um registro alt-ped correspondente
**             Isto significa que a alteracao
**             nao passou na critica do CC0604
**
**             Para os registros que passaram nestas duas selecoes:
**             Se a obsevacao for igual a [I]
**               . Alterar impr-pedido da tabela pedido-compr p/yes
**               . Alterar a 1a. pos. da observacao da tabela alt-ped para *
**               . Alterar a narrativa da tabela ordem de compra para conter
**                 a nota fiscal de corte.
**             Se a obsevacao for igual a [A]
**               . Alterar data e quantidade da tabela prazo-compra
**               . Alterar char-1 da tabela alt-ped.
**               . Alterar impr-pedido da tabela pedido-compr p/yes
**               . Alterar a 1a. pos. da observacao da tabela alt-ped para *
**               . Alterar a narrativa da tabela ordem de compra para conter
**                 a nota fiscal de corte.
***********************************************************************/

DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "le0102".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "       Altera Parcelas [3a.Fase]      ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".
DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".

                  

DEFINE VARIABLE wdispfim   AS CHAR FORMAT "x(78)".

DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".


DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(230)".
DEFINE VARIABLE wcab        AS CHAR FORMAT "x(230)".
DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(132)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".

/*....................................................inicio flags...*/
DEFINE VARIABLE wdescarqe   AS CHAR FORMAT "x(30)".
DEFINE VARIABLE warqe       AS CHAR FORMAT "x(40)".

DEFINE VARIABLE wdescproc   AS CHAR FORMAT "x(30)".
DEFINE VARIABLE wproc       AS CHAR FORMAT "!".
                    
DEFINE VARIABLE wdescecit   AS CHAR FORMAT "x(30)".                    
DEFINE VARIABLE wecit       AS CHAR FORMAT "!".

DEFINE VARIABLE wdescvia    AS CHAR FORMAT "x(30)".   
DEFINE VARIABLE wvia        AS CHAR FORMAT "!".

DEFINE VARIABLE wdescarqs   AS CHAR FORMAT "x(30)".   
DEFINE VARIABLE warqs       AS CHAR FORMAT "X(08)".  

DEFINE VARIABLE wdescare    AS CHAR FORMAT "x(30)".   
DEFINE VARIABLE ware        AS CHAR FORMAT "!".

/*....................................................fim de flags...*/

DEFINE VARIABLE wseq       AS INTE INITIAL 0.

DEFINE VARIABLE wreg        AS CHAR FORMAT "x(179)".

DEFINE VARIABLE wnerro    AS INTE.
DEFINE VARIABLE wtberro   AS CHAR FORMAT "x(50)" EXTENT 20.
DEFINE VARIABLE wcte      AS INTE.


/*-------------------------------------------------------------inicio novo1--*/
/*-----------------------------------------------------------VARIAVEIS LOCAIS*/
DEFINE VARIABLE  v_chr_arquivo      AS CHAR FORMAT "x(40)" NO-UNDO.
DEFINE VARIABLE  v_chr_cod_emitente AS CHAR FORMAT "x(06)" NO-UNDO.
DEFINE VARIABLE  v_chr_linha        AS CHAR FORMAT "x(80)" NO-UNDO.
DEFINE VARIABLE  wtitrel            AS CHAR FORMAT "x(38)".         
DEFINE VARIABLE  v_dec_vl_preori    LIKE ped-item.vl-preori NO-UNDO.


DEFINE TEMP-TABLE tt_ap00
   FIELD codreg         AS CHAR FORMAT "x(4)"
   FIELD num-pedido     AS INTE FORMAT ">>>>>,>>9"
   FIELD numero-ordem   AS INTE FORMAT "zzzzz9,99"
   FIELD parcela        AS INTE FORMAT ">>>>9"
   FIELD cod-cond-pag   AS INTE FORMAT ">>9"
   FIELD datalt         AS DATE FORMAT "99/99/9999"      
   FIELD data-entrega   AS DATE FORMAT "99/99/9999"    
   FIELD horalt         AS CHAR FORMAT "x(8)"                
   FIELD nome-abrev     AS CHAR FORMAT "x(12)"         
   FIELD observacao     AS CHAR FORMAT "x(76)"
   FIELD preco-fornec   AS DECI FORMAT ">>>>>>>>>>9.99999"
   FIELD quantidade     AS DECI FORMAT ">>>>>>>9.9999"
   FIELD usuario        AS CHAR FORMAT "x(12)"
   FIELD mensa          AS CHAR FORMAT "x(25)"
   FIELD novadata       AS DATE FORMAT "99/99/9999"    
   FIELD novopreco      AS DECI FORMAT ">>>>>>>>>>9.99999"
   FIELD novaqtde       AS DECI FORMAT ">>>>>>>9.9999"
   FIELD nfcorte        AS CHAR FORMAT "x(6)"   
   INDEX idx-cod IS UNIQUE PRIMARY num-pedido
                                   numero-ordem
                                   parcela
                                   datalt
                                   horalt
   INDEX idx-data                  data-entrega.


DEFINE VARIABLE xcodreg         AS CHAR FORMAT "x(4)".
DEFINE VARIABLE xnum-pedido     AS CHAR FORMAT "x(8)".         
DEFINE VARIABLE xnumero-ordem   AS CHAR FORMAT "x(8)".         
DEFINE VARIABLE xparcela        AS CHAR FORMAT "x(5)".  
DEFINE VARIABLE xcod-cond-pag   AS CHAR FORMAT "x(3)".
DEFINE VARIABLE xdatalt         AS CHAR FORMAT "x(8)".          
DEFINE VARIABLE xdata-entrega   AS CHAR FORMAT "x(8)".         
DEFINE VARIABLE xhoralt         AS CHAR FORMAT "x(8)".               
DEFINE VARIABLE xnome-abrev     AS CHAR FORMAT "x(12)".        
DEFINE VARIABLE xobservacao     AS CHAR FORMAT "x(76)".
DEFINE VARIABLE xpreco-fornec   AS CHAR FORMAT "x(16)".             
DEFINE VARIABLE xquantidade     AS CHAR FORMAT "x(11)".                  
DEFINE VARIABLE xusuario        AS CHAR FORMAT "x(12)".
DEFINE VARIABLE xmensa          AS CHAR FORMAT "x(25)".

DEFINE VARIABLE wmsgini         AS CHAR FORMAT "x(25)".
DEFINE VARIABLE wmsgfff         AS CHAR FORMAT "x(25)".
DEFINE VARIABLE wchalt          AS CHAR FORMAT "x(2)".


DEFINE VARIABLE wobs            AS CHAR FORMAT "x(76)".
DEFINE VARIABLE xobsini         AS CHAR FORMAT "x(03)".
DEFINE VARIABLE xplano          AS CHAR FORMAT "x(07)".
DEFINE VARIABLE xnovopreco      AS CHAR FORMAT "x(16)".             
DEFINE VARIABLE xnovaqtde       AS CHAR FORMAT "x(11)".                  
DEFINE VARIABLE xnovadata       AS CHAR FORMAT "x(10)".
DEFINE VARIABLE xnfcorte         AS CHAR FORMAT "x(6)".             

DEFINE VARIABLE wchar1          AS CHAR FORMAT "x(100)".
DEFINE VARIABLE xchar1preco     AS CHAR FORMAT "x(16)".             
DEFINE VARIABLE xchar1qtde      AS CHAR FORMAT "x(11)".                  
DEFINE VARIABLE xchar1data      AS CHAR FORMAT "x(10)".         

DEFINE VARIABLE wctdalt         AS INTE.
DEFINE VARIABLE wctd1           AS INTE.
DEFINE VARIABLE wctdpipe        AS INTE.

DEFINE VARIABLE wsdofornec      LIKE prazo-compra.quantidade.


FORM SKIP(03) " " COLON 19 wstart SKIP(11) " "
     WITH NO-LABELS ROW 04 WIDTH 80 FRAME f-box4.

FORM                     
SKIP wdescarqe   warqe
SKIP wdescproc   wproc
SKIP wdescecit   wecit
SKIP wdescvia    wvia  
SKIP wdescarqs   warqs
WITH NO-LABELS ROW 05 COLUMN 02 WIDTH 76
TITLE "VIA" 
FRAME f-video.

CURRENT-WINDOW:VIRTUAL-HEIGHT-CHARS = 24.
CURRENT-WINDOW:HEIGHT-CHARS = 20.

/*...................................................inicio entrada padrao..*/
ASSIGN wcalled  = wpgm
       wcaller  = "C"
       wusr     = STRING(USERID(ldbname(1)),"x(10)")
       wvia     = "A"
       warqs    = wpgm + "a"
       wimp     = "altparc"
       warqe    = "C:/spool/le0101a.txt"
       wproc    = "C"
       wecit    = "T"
       ware     = "E"
       .
/*.................................................fim de entrada padrao..*/

DEFINE VARIABLE new-win AS WIDGET-HANDLE.
CREATE WINDOW new-win
      ASSIGN TITLE = wpgm + " - " + wtit.
    
CURRENT-WINDOW = new-win.


DEFINE VARIABLE wctdrepeat  AS INTE.
ASSIGN wctdrepeat = 0.
REPEAT:
  ASSIGN wctdrepeat = wctdrepeat + 1.
  IF wctdrepeat GT 1
  THEN DO:
    DELETE WIDGET new-win.
    RETURN.
    END.

  REPEAT WITH:
    STATUS INPUT "informe o arquivo de entrada".
    ASSIGN wdescarqe = "Arquivo de Entrada".
    DISPLAY wdescarqe WITH FRAME f-video.     
    UPDATE      warqe WITH FRAME f-video.     
    wmsg = wmsg + wdescarqe + ": " + warqe.
    LEAVE.
    END.
  IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN RETURN.

  /*........................................inicio numero de erros...*/
  ASSIGN wnerro = 0.

  ASSIGN wrel = warqe.
  IF SEARCH(wrel) = ?           
  THEN DO:
    ASSIGN wnerro = wnerro + 1.
    ASSIGN wtberro[wnerro] = "Arquivo nao encontrado: "
                           + warqe.
    END.
  /*........................................fim de numero de erros...*/


  REPEAT:
    STATUS INPUT "informe se procedimento: Consistencia ou Alteracao".
    ASSIGN  wdescproc = "Consistencia ou Alteracao".
    DISPLAY wdescproc WITH FRAME f-video.
    UPDATE      wproc WITH FRAME f-video.    
    IF wproc = "C"
    OR wproc = "A" THEN DO:
      IF wproc = "C" THEN ASSIGN wmsg = wmsg + " ,Consistencia".
      IF wproc = "A" THEN ASSIGN wmsg = wmsg + " ,Alteracao".
      LEAVE.
      END.
    MESSAGE "procedimento deve ser C ou A".
    IF wcaller = "S" THEN RETURN.
    NEXT.
    END.
  IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.

  REPEAT:
    STATUS INPUT "informe se exibe: Consistentes, Inconsistentes ou Todos".
    ASSIGN  wdescecit = "Exibe: C, I ou Todos".
    DISPLAY wdescecit WITH FRAME f-video.    
    UPDATE      wecit WITH FRAME f-video.
    IF wecit = "C"
    OR wecit = "I"
    OR wecit = "T" THEN DO:
      IF wecit = "C" THEN ASSIGN wmsg = wmsg + " ,Exibe Consistentes".
      IF wecit = "I" THEN ASSIGN wmsg = wmsg + " ,Exibe Inconsistentes".
      IF wecit = "T" THEN ASSIGN wmsg = wmsg + " ,Exibe Todos".
      LEAVE.
      END.
    MESSAGE "exibe deve ser C, I ou T".
    IF wcaller = "S" THEN RETURN.
    NEXT.
    END.
  IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.

  /*........................................inicio exibe erros.......*/
  DO wcte = 1 TO wnerro:
    MESSAGE "Erro " + STRING(wcte,">>9") + " " + STRING(wtberro[wcte]).
    IF wcaller NE "S" THEN PAUSE.
    END.
  IF wnerro NE 0 
  THEN DO:
    DELETE WIDGET new-win.
    RETURN.
    END.
  
  /*........................................fim de exibe erros.......*/



  REPEAT:
    STATUS INPUT "informe se VIDEO ou ARQUIVO".
    ASSIGN  wdescvia = "Video ou Arquivo".
    DISPLAY wdescvia WITH FRAME f-video.
    UPDATE      wvia WITH FRAME f-video.
    IF wvia = "V"
    OR wvia = "A" THEN DO:
      IF wvia = "V" THEN ASSIGN wmsg = wmsg + " ,Video".
      IF wvia = "A" THEN ASSIGN wmsg = wmsg + " ,Arquivo".
      LEAVE.
      END.
    MESSAGE "procedimento deve ser V ou A".
    NEXT.
    END.
  IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.


  IF wvia = "A"
  THEN DO:
    STATUS INPUT "informe o nome do Arquivo de Saida".  
    ASSIGN  wdescarqs = "Arquivo de Saida:".
    DISPLAY wdescarqs WITH FRAME f-video.
    UPDATE      warqs WITH FRAME f-video.
    IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.
    END.
 
 
   REPEAT:
    STATUS INPUT "informe se: Abandona, Restaura ou Executa".
    ASSIGN  wdescare = "Abandona, Restaura ou Executa".
    DISPLAY wdescare WITH FRAME f-video.    
    UPDATE      ware WITH FRAME f-video.
    IF ware = "A"
    OR ware = "R"
    OR ware = "E" THEN DO:
      LEAVE.
      END.
    MESSAGE "exibe deve ser A, R ou E".
    IF wcaller = "S" THEN RETURN.
    NEXT.
    END.
  IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.
  IF ware = "A" 
  THEN DO:
    DELETE WIDGET new-win.
    RETURN.
    END.
  
  IF ware = "R" THEN NEXT.

    
  IF wvia = "A"
  THEN DO:
    HIDE FRAME f-video.
    wrel = "C:/spool/" + warqs + ".lst".
    ASSIGN wmsg = wmsg + " " + STRING(wrel).
    OUTPUT TO VALUE(wrel) PAGE-SIZE 60.
    END.

  /*..............................................inicio monta cabecalho...*/
  ASSIGN wcab = "_____Mensagem____________"
        + " " + "___Pedido"
        + " " + "____Ordem"
        + " " + "_Parc"
        + " " + "I/A"        
        + " " + "_Data_Entr"
        + " " + "_Nova_Data"
        + " " + "______Quantidade"
        + " " + "_______Nova Qtde"
        + " " + "Fornecedor  "
/****************************************inibido para diminuir relatorio**        
        + "_" + "_____Preco_Fornec"
        + "_" + "Usuario     "
        + "_" + "Cod "
        + "_" + "_CP"
        + "_" + "__Data_Alt"
        + "_" + "Hora_Alt"
        + "_" + "Obs"
***********************************************************************/
        .
  /*..............................................fim de monta cabecalho...*/


  DO WITH FRAME f-lh:

    FORM HEADER wpgm
      SPACE(00) wvrs
      SPACE(01) wusr
      SPACE(00) wtit
      SPACE(01) wdtd
      SPACE(01) whra
    SKIP(1) wmsg
    SKIP(1) wcab
      WITH DOWN NO-LABEL ROW 01
      WIDTH 232
      TITLE (IF wvia = "V" THEN wbdd-area + wbdd   
                           ELSE wbdd-area + wbdd + " PG."
                                     + STRING(PAGE-NUMBER,">>>>9")) 
      FRAME f-lh.


    ASSIGN v_chr_arquivo = warqe.

    ASSIGN wctdalt = 0.

    /*------------------------inicio tratamento de input-----------------*/
    INPUT FROM VALUE(v_chr_arquivo) NO-ECHO.
    REPEAT WITH FRAME f-lh:

      IMPORT UNFORMATTED wreg.
      ASSIGN
      xcodreg       = SUBSTRING(wreg,001,004)
      xnum-pedido   = SUBSTRING(wreg,005,008)
      xnumero-ordem = SUBSTRING(wreg,013,008)
      xparcela      = SUBSTRING(wreg,021,005)
      xcod-cond-pag = SUBSTRING(wreg,026,003)
      xdatalt       = SUBSTRING(wreg,029,008)
      xdata-entrega = SUBSTRING(wreg,037,008)
      xhoralt       = SUBSTRING(wreg,045,008)
      xnome-abrev   = SUBSTRING(wreg,053,012)
      xobservacao   = SUBSTRING(wreg,065,076)
      xpreco-fornec = SUBSTRING(wreg,141,016)
      xquantidade   = SUBSTRING(wreg,157,011)
      xusuario      = SUBSTRING(wreg,168,012)
      xmensa        = "".

      /*.......................................inicio grava ap00..........*/
      IF SUBSTRING(xobservacao,1,3) = "[A]" 
      OR SUBSTRING(xobservacao,1,3) = "[I]" 
      THEN DO:

        CREATE tt_ap00.
        ASSIGN
        tt_ap00.codreg       = xcodreg
        tt_ap00.num-pedido   = INTEGER(xnum-pedido)
        tt_ap00.numero-ordem = INTEGER(xnumero-ordem)
        tt_ap00.parcela      = INTEGER(xparcela)
        tt_ap00.cod-cond-pag = INTEGER(xcod-cond-pag)
        tt_ap00.datalt       = DATE(INTEGER(SUBSTRING(xdatalt,3,2)),
                                    INTEGER(SUBSTRING(xdatalt,1,2)),
                                    INTEGER(SUBSTRING(xdatalt,5,4)))
        tt_ap00.data-entrega = DATE(INTEGER(SUBSTRING(xdata-entrega,3,2)),
                                    INTEGER(SUBSTRING(xdata-entrega,1,2)),
                                    INTEGER(SUBSTRING(xdata-entrega,5,4)))
        tt_ap00.horalt       = xhoralt
        tt_ap00.nome-abrev   = xnome-abrev
        tt_ap00.observacao   = xobservacao
        tt_ap00.preco-fornec = INTE(xpreco-fornec) / 100000
        tt_ap00.quantidade   = INTE(xquantidade)   / 10000
        tt_ap00.usuario      = xusuario.  

        ASSIGN wctdalt = wctdalt + 1.
        END.
      /*.......................................fim de grava ap00..........*/

      END.
    INPUT CLOSE.
    IF KEYFUNCTION(LASTKEY) = "END-ERROR" THEN NEXT.
    /*------------------------fim de tratamento de input-----------------*/

    IF wctdalt = 0 THEN DO:
      ASSIGN wdet = "nao ha atualizacoes no arquivo".
      DISPLAY wdet. DOWN 1.
      NEXT.
      END.
    IF wctdalt GT 0 THEN DO:
      ASSIGN wdet = "Ha " 
                  + STRING(wctdalt) 
                  + " atualizacoes".
      DISPLAY wdet. DOWN 1.
      END.

    /*......................................inicio primeira consistencia...*/
    FOR EACH tt_ap00
    WITH FRAME f-lh:
      ASSIGN wctdpipe   = 0
             wobs       = tt_ap00.observacao
             xobsini    = ""
             xplano     = ""
             xnovopreco = ""
             xnovaqtde  = ""
             xnovadata  = ""
             xnfcorte   = "".
      DO wctd1 = 1 TO LENGTH(wobs):
        IF SUBSTRING(wobs,wctd1,1) = "|" THEN DO:
          ASSIGN wctdpipe = wctdpipe + 1.
          NEXT.
          END.

        IF wctdpipe = 0 
        THEN ASSIGN xobsini    = xobsini + SUBSTRING(wobs,wctd1,1).

        IF wctdpipe = 1 
        THEN ASSIGN xplano     = xplano  + SUBSTRING(wobs,wctd1,1).
        
        IF wctdpipe = 2 
        THEN ASSIGN xnovopreco = xnovopreco  + SUBSTRING(wobs,wctd1,1).
        
        IF wctdpipe = 3 
        THEN ASSIGN xnovaqtde  = xnovaqtde   + SUBSTRING(wobs,wctd1,1).
        
        IF wctdpipe = 4 
        THEN ASSIGN xnovadata  = xnovadata   + SUBSTRING(wobs,wctd1,1).
        
        IF wctdpipe = 5 
        THEN ASSIGN xnfcorte   = xnfcorte    + SUBSTRING(wobs,wctd1,1).
                
        END.

      ASSIGN tt_ap00.novopreco = INTE(xnovopreco)
             tt_ap00.novaqtde  = INTE(xnovaqtde)
             tt_ap00.novadata  = DATE(INTEGER(SUBSTRING(xnovadata,4,2)),
                                      INTEGER(SUBSTRING(xnovadata,1,2)),
                                      INTEGER(SUBSTRING(xnovadata,7,4)))
             tt_ap00.nfcorte   = xnfcorte.

      FIND prazo-compra
      WHERE prazo-compra.numero-ordem = tt_ap00.numero-ordem
      AND   prazo-compra.parcela      = tt_ap00.parcela
      NO-LOCK NO-ERROR.
      
      IF NOT AVAILABLE prazo-compra
      THEN DO:
        ASSIGN tt_ap00.mensa = "  ***parc. nao encontrada".
        NEXT.
        END.
        
      IF AVAILABLE prazo-compra
      THEN DO:
        IF prazo-compra.data-entrega NE tt_ap00.data-entrega
        OR prazo-compra.quantidade   NE tt_ap00.quantidade
        THEN DO:
          ASSIGN tt_ap00.mensa = "  ###parcela ja alterada".
          NEXT.
          END.
          
        IF prazo-compra.situacao  NE 2   /* situcao dif de confirmada */
        THEN DO:
          ASSIGN tt_ap00.mensa = "#situacao dif confirmada".
          NEXT.
          END.
          
        END.

      FIND alt-ped
      WHERE alt-ped.num-pedido   = tt_ap00.num-pedido
      AND   alt-ped.numero-ordem = tt_ap00.numero-ordem
      AND   alt-ped.parcela      = tt_ap00.parcela
      AND   alt-ped.data         = tt_ap00.datalt
      AND   alt-ped.hora         = tt_ap00.horalt
      NO-LOCK NO-ERROR.

      IF NOT AVAILABLE alt-ped
      THEN DO:
        ASSIGN tt_ap00.mensa = "  ***nao tem alt-ped".
        NEXT.
        END.

      IF alt-ped.observacao NE tt_ap00.observacao
      THEN DO:
        ASSIGN tt_ap00.mensa = "  ###obs dif. do alt-ped".
        IF alt-ped.observacao BEGINS("*") THEN DO:
          ASSIGN tt_ap00.mensa = "!!alterado anteriormente".
          END.
        NEXT.
        END.

      /*.........................inicio verifica se ja esta alterado...*/
      IF alt-ped.char-1 NE "" THEN DO:

        ASSIGN wctdpipe   = 0
               wchar1     = alt-ped.char-1    
               xchar1preco = ""
               xchar1qtde  = ""
               xchar1data  = "".
        DO wctd1 = 1 TO LENGTH(wchar1):
          IF SUBSTRING(wchar1,wctd1,1) = "|" THEN DO:
            ASSIGN wctdpipe = wctdpipe + 1.
            NEXT.
            END.

          IF wctdpipe = 0 
          THEN ASSIGN xchar1preco = xchar1preco  + SUBSTRING(wchar1,wctd1,1).
        
          IF wctdpipe = 1 
          THEN ASSIGN xchar1qtde  = xchar1qtde   + SUBSTRING(wchar1,wctd1,1).
        
          IF wctdpipe = 2 
          THEN ASSIGN xchar1data  = xchar1data   + SUBSTRING(wchar1,wctd1,1).
        
          END.

        IF  xchar1preco = xnovopreco
        AND xchar1qtde  = xnovaqtde
        AND xchar1data  = xnovadata
        THEN DO:
          ASSIGN tt_ap00.mensa = "!! alterado anteriorment".
          NEXT.
          END.

        ASSIGN tt_ap00.mensa = "## char-1 desconhecido  ".
        NEXT.


        END.
      /*.........................fim de verifica se ja esta alterado...*/





      END.
    /*......................................fim da primeira consistencia...*/


    /*.................................inicio exibe ocorrencias........*/
    FOR EACH tt_ap00               
    WITH FRAME f-lh:
      ASSIGN wmsgini = tt_ap00.mensa
             wmsgfff = "".

      /*..............................................inicio alteracao...*/
      IF  tt_ap00.mensa  = ""      /*       reg.  consistente */
      AND wproc          = "A"     /* procedimento de alteracao */
      THEN DO:

        
        FIND  prazo-compra
        WHERE prazo-compra.numero-ordem = tt_ap00.numero-ordem
        AND   prazo-compra.parcela      = tt_ap00.parcela
        NO-ERROR.

        IF NOT AVAILABLE prazo-compra
        THEN ASSIGN wmsgini = "!!!parc.eliminada p/outro".
        /*!!!!!!!!!!!!!!!!!!...isto pode ocorrer se durante a execucao...!!!*/
        /*!!!!!!!!!!!!!!!!!!...outro usuario eliminou a parcela..........!!!*/
        
        FIND emitente
        WHERE emitente.nome-abrev = tt_ap00.nome-abrev
        NO-LOCK NO-ERROR.
        IF NOT AVAILABLE emitente
        THEN ASSIGN wmsgini = "* emitente inexistente".
        
        FIND item-fornec
        WHERE item-fornec.cod-emitente = emitente.cod-emitente
        AND   item-fornec.it-codigo    = prazo-compra.it-codigo
        NO-LOCK NO-ERROR.
        IF NOT AVAILABLE item-fornec
        THEN ASSIGN wmsgini = "* item-fornec inexistente".

        IF AVAILABLE item-fornec THEN DO:
          ASSIGN wsdofornec = tt_ap00.novaqtde.
          IF item-fornec.num-casa-dec = 1 THEN DO:
            ASSIGN wsdofornec = wsdofornec / 10.
            END.
          IF item-fornec.num-casa-dec = 2 THEN DO:
            ASSIGN wsdofornec = wsdofornec / 100.
            END.
          IF item-fornec.num-casa-dec = 3 THEN DO:
            ASSIGN wsdofornec = wsdofornec / 1000.
            END.
          END.  

        /*.....................................inicio altera prazo-compra...*/
        ASSIGN wchalt = "".
        IF  AVAILABLE prazo-compra
        AND AVAILABLE emitente
        AND AVAILABLE item-fornec
        THEN DO:


          IF SUBSTRING(tt_ap00.observacao,1,3) = "[A]" 
          THEN DO:
            IF  prazo-compra.data-entrega  = tt_ap00.data-entrega
            AND prazo-compra.data-entrega NE tt_ap00.novadata
            THEN DO:
              ASSIGN 
              prazo-compra.data-entrega = tt_ap00.novadata
              wchalt = wchalt + "D".
              END.

            IF  prazo-compra.quantidade  = tt_ap00.quantidade
            AND prazo-compra.quantidade NE tt_ap00.novaqtde
            THEN DO:
              ASSIGN 
              prazo-compra.quantidade   = tt_ap00.novaqtde
              prazo-compra.quant-saldo  = tt_ap00.novaqtde
              prazo-compra.qtd-do-forn  = wsdofornec
              prazo-compra.qtd-sal-forn = wsdofornec
              wchalt = wchalt + "Q".
              END.

            IF wchalt =   "" THEN wmsgini = "  ###nao ha o que alterar".
            IF wchalt = "DQ" THEN wmsgini = "ok...alterado DATA e QTDE".
            IF wchalt =  "D" THEN wmsgini = "ok...alterado DATA       ".
            IF wchalt =  "Q" THEN wmsgini = "ok...alterado QUANTIDADE ".
            END.
               
          /*..................................inicio altera alt-ped......*/
       
          IF wchalt NE ""
          OR SUBSTRING(tt_ap00.observacao,1,3) = "[I]" 
          THEN DO:
            FIND alt-ped
            WHERE alt-ped.num-pedido   = tt_ap00.num-pedido
            AND   alt-ped.numero-ordem = tt_ap00.numero-ordem
            AND   alt-ped.parcela      = tt_ap00.parcela
            AND   alt-ped.data         = tt_ap00.datalt
            AND   alt-ped.hora         = tt_ap00.horalt 
            NO-ERROR.

            IF NOT AVAILABLE alt-ped
            THEN DO:
              ASSIGN wmsgfff = "!! alt-ped elim.por outro".
              /*!!!!!!!!!!!!...isto pode ocorrer se durante a execucao...!!!*/
              /*!!!!!!!!!!!!...outro usuario eliminou a alteracao........!!!*/
              END.

            IF AVAILABLE alt-ped
            AND alt-ped.observacao NE tt_ap00.observacao
            THEN DO:
              ASSIGN wmsgfff = "!!! obs alt-ped alt.p/out".
              /*!!!!!!!!!!!!...isto pode ocorrer se durante a execucao...!!!*/
              /*!!!!!!!!!!!!...outro usuario alterou a obs do alt-ped....!!!*/
              END.


            IF AVAILABLE alt-ped
            AND alt-ped.observacao  = tt_ap00.observacao
            THEN DO:
  
              IF SUBSTRING(tt_ap00.observacao,1,3) = "[A]"   
              THEN DO:
                ASSIGN 
                alt-ped.char-1    = STRING(tt_ap00.novopreco)
                                  + "|"
                                  + STRING(tt_ap00.novaqtde)
                                  + "|"
                                  + STRING(tt_ap00.novadata,"99/99/9999").
                END.

              /*......................inicio altera pedido-compr.....*/
              FIND  pedido-compr 
              WHERE pedido-compr.num-pedido = tt_ap00.num-pedido              
              NO-ERROR.
              IF NOT AVAILABLE pedido-compr
              THEN DO:
                ASSIGN wmsgfff = "**  pedido nao encontrado".
                END.
              IF AVAILABLE pedido-compr
              THEN DO:
                ASSIGN pedido-compr.impr-pedido = yes.
                IF SUBSTRING(tt_ap00.observacao,1,3) = "[I]" 
                THEN DO:  
                  ASSIGN wmsgini = "ok...inclusao c/imprime ".
                  END.
                END.
              /*......................fim de altera pedido-compr.....*/



              /*......................inicio altera ordem-compra.....*/
              FIND  ordem-compra 
              WHERE ordem-compra.numero-ordem = tt_ap00.numero-ordem              
              NO-ERROR.
              IF NOT AVAILABLE ordem-compra
              THEN DO:
                ASSIGN wmsgfff = "**  ordem nao encontrada".
                END.
              IF AVAILABLE ordem-compra
              THEN DO:
                 /* altera situacao da ordem p/ confirmada, conf. Sol.
                  J.Luiz e Andre - 23/03/06 */
                  assign ordem-compra.situacao = 2.
                  /*-------------------------------------------- */
                IF STRING(ordem-compra.narrativa) NE tt_ap00.nfcorte
                THEN DO:
                  ASSIGN ordem-compra.narrativa = tt_ap00.nfcorte.
                  END.
                END.
              /*......................fim de altera ordem-compra.....*/

              END.
            END.
          /*..................................fim de altera alt-ped......*/

          END.
        /*.....................................fim de altera prazo-compra...*/




        END.
      /*..............................................fim de alteracao...*/


      IF  tt_ap00.mensa  = ""      /*       reg.  consistente */
      AND wecit          = "I"     /* exiber so inconsistentes */
      THEN NEXT.

      IF  tt_ap00.mensa NE ""      /*       reg.inconsistente  */
      AND wecit          = "C"     /* exiber so   consistentes */
      THEN NEXT.

      ASSIGN wdet =
              STRING(wmsgini,"x(25)")
      + " " + STRING(tt_ap00.num-pedido,"zzzzz,zz9")
      + " " + STRING(tt_ap00.numero-ordem,"zzzzz9,99")
      + " " + STRING(tt_ap00.parcela,"zzzz9")
      + " " + SUBSTRING(tt_ap00.observacao,1,3)      
      + " " + STRING(tt_ap00.data-entrega,"99/99/9999")    
      + " " + STRING(tt_ap00.novadata,"99/99/9999")    
      + " " + STRING(tt_ap00.quantidade,"zzzzzzzzzz9.9999")
      + " " + STRING(tt_ap00.novaqtde,"zzzzzzzzzz9.9999")
      + " " + STRING(tt_ap00.nome-abrev,"x(12)")         
/*****************************************inibido para diminuir relatorio      
      + " " + STRING(tt_ap00.preco-fornec,"zzzzzzzzzz9.99999")
      + " " + STRING(tt_ap00.usuario,"x(12)")
      + " " + STRING(tt_ap00.codreg,"x(4)")
      + " " + STRING(tt_ap00.cod-cond-pag,"zz9")
      + " " + STRING(tt_ap00.datalt,"99/99/9999")      
      + " " + STRING(tt_ap00.horalt,"x(8)")                
      + " " + STRING(tt_ap00.observacao,"x(76)")
****************************************************************/
      .
      DISPLAY wdet WITH FRAME f-lh.                  
      DOWN WITH FRAME f-lh.

      IF wmsgfff NE "" THEN DO:
        ASSIGN wdet = STRING(wmsgfff,"x(25)").
        DISPLAY wdet WITH FRAME f-lh.                  
        DOWN WITH FRAME f-lh.
        END.

      END.
    /*.................................fim de exibe ocorrencias........*/



    /*................................inicio total geral...*/  
    IF  wbdd = wbdd-integri
    THEN DO:
      ASSIGN wdet = "".
      DISPLAY wdet. DOWN 1.

      IF wproc = "C" 
      THEN ASSIGN wdet = "       F I M   D A   C O N S I S T E N C I A ".

      IF wproc = "A" 
      THEN ASSIGN wdet = "       F I M   D A   A L T E R A C A O       ".


      DISPLAY wdet. DOWN 1.
      END.
    /*................................fim do total geral...*/  


    END.


  IF wvia = "A" 
  THEN DO:
    OUTPUT CLOSE.
    END.

  IF wvia = "A" THEN DO:
    ASSIGN wdispfim = "OK - Vide Arquivo de Relatorio: " + STRING(wrel).
    DISPLAY wdispfim WITH NO-LABEL.
    
    PAUSE.
    DELETE WIDGET new-win.
    RETURN.
    
    
    END.
  
  END.





