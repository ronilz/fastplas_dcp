&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0220RP 0.00.04.002}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wfami            AS CHAR FORMAT "x(08)"
    FIELD wfamf            AS CHAR FORMAT "x(08)"
    FIELD wite             AS CHAR FORMAT "x(16)"
    FIELD wdr              AS CHAR FORMAT "X(10)"
    FIELD wdtdd            AS DATE FORMAT "99/99/9999"
    FIELD westab           LIKE movind.movto-estoq.cod-estabel
    FIELD wnr-linha-ini    AS INTE FORMAT ">>9"
    FIELD wnr-linha-fim    AS INTE FORMAT ">>9".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* DEFINI��ES DO BENE */ 

DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li0220".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "             Exibe Posicao            ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wvia       AS CHAR FORMAT "!".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)".                    

DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".    
DEFINE VARIABLE wctd        AS INTE.
DEFINE VARIABLE wctditem    AS INTE.


DEFINE VARIABLE wcodrefaux  AS CHAR FORMAT "x(20)".
DEFINE VARIABLE wctd1       AS INTE.
DEFINE VARIABLE wposrefaux  AS INTE.

DEFINE VARIABLE wexibecod   LIKE mgind.item.codigo-refer.

DEFINE VARIABLE wputscreen  AS CHARACTER FORMAT "x(22)".
DEFINE VARIABLE wfam        AS CHAR      FORMAT "x(3)".

DEFINE VARIABLE wfami       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wfamf       LIKE mgdis.fam-comerc.fm-cod-com.
DEFINE VARIABLE wite        LIKE mgind.item.it-codigo.   
DEFINE VARIABLE wdtdd       AS DATE FORMAT "99/99/9999".

DEFINE VARIABLE wctx        AS INTE FORMAT "99".
DEFINE VARIABLE widentx     AS CHAR FORMAT "x"             EXTENT 6.
DEFINE VARIABLE wident      AS CHAR FORMAT "x(6)".

DEFINE VARIABLE whide       AS INTE FORMAT "9".
DEFINE VARIABLE wctr        AS CHAR FORMAT "x(36)".
DEFINE VARIABLE wctrle      AS CHAR FORMAT "!".


DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(100)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(108)".
DEFINE VARIABLE wcab        AS CHAR FORMAT "x(1080)".

DEFINE VARIABLE wtabpre     LIKE mgdis.tb-preco.nr-tabpre.
DEFINE VARIABLE wdatunit    LIKE mgdis.tb-preco.dt-inival.
DEFINE VARIABLE wvalunit    LIKE mgdis.preco-item.preco-venda.


DEFINE VARIABLE wpgr2       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wpgr3       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wpgr4       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenvx       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenv2       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenv3       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenv4       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdx        AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdp2       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdp3       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdp4       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdn2       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdn3       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wsdn4       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE weda2       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE weda3       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE weda4       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wpzd2       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wpzd3       AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wpzd4       AS INTEGER FORMAT "zzzzzz9-".

DEFINE VARIABLE wresto      AS INTEGER.
DEFINE VARIABLE wlit0       AS CHARACTER FORMAT "x(79)".
DEFINE VARIABLE wlit1       AS CHARACTER FORMAT "x(79)".
DEFINE VARIABLE wlit2       AS CHARACTER FORMAT "x(79)".
DEFINE VARIABLE wdesc1      AS CHARACTER FORMAT "x(28)".
DEFINE VARIABLE wdesc2      AS CHARACTER FORMAT "x(36)".

DEFINE VARIABLE wdmatu      AS CHARACTER FORMAT "99999999".
DEFINE VARIABLE wddmmaa     AS CHARACTER FORMAT "x(8)".
DEFINE VARIABLE wanomes       AS CHARACTER FORMAT "999999".
DEFINE VARIABLE wday        AS INTEGER   FORMAT "99".
DEFINE VARIABLE wdtant      AS DATE      FORMAT "99/99/9999".
DEFINE VARIABLE wanomesant    AS CHARACTER FORMAT "999999".

DEFINE VARIABLE wdti        AS DATE    FORMAT "99/99/9999".
DEFINE VARIABLE wquant       LIKE movind.movto-estoq.quantidade.
DEFINE VARIABLE wqestorno    LIKE movind.movto-estoq.quantidade.

FORM SKIP(03) " " COLON 19 wstart SKIP(11) " "
     WITH NO-LABELS ROW 04 WIDTH 80 FRAME f-box4.
/* fim defini��es do bene */
FORM                     
SKIP "Video/Impressora/Arquivo:" wvia  
SKIP wdescarq warqtele
WITH NO-LABELS ROW 05 COLUMN 02 WIDTH 30 OVERLAY
TITLE "VIA" 
FRAME f-video.




create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Posi��o".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
    
    RUN roda_prog.

        
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME






PROCEDURE roda_prog.
/***********************************************************************
**
** li0220.p   - Exibe Posicao       
**
**              29/09/1990 -  BENE   Programa novo.
**
***********************************************************************/



/*.................................................fim de entrada padrao..*/

wdmatu = STRING(DAY(TODAY),"99") 
       + STRING(MONTH(TODAY),"99") 
       + SUBSTRING(STRING(INTEGER(YEAR(TODAY)),"9999"),1,4).
wddmmaa = wdmatu.

wlit0 = FILL(" ",78).
wlit1 = FILL(" ",78).
wlit2 = FILL(" ",78).


DEFINE VARIABLE wctdrepeat  AS INTE.
ASSIGN wctdrepeat = 0.

  ASSIGN wctdrepeat = wctdrepeat + 1.
  ASSIGN 
  wpgr2 = 0.
  wpgr3 = 0.
  wpgr4 = 0.
  wenv2 = 0.
  wenv3 = 0.
  wenv4 = 0.
  wsdp2 = 0.
  wsdp3 = 0.
  wsdp4 = 0.
  wsdn2 = 0.
  wsdn3 = 0.
  wsdn4 = 0.
  weda2 = 0.
  weda3 = 0.
  weda4 = 0.
  wpzd2 = 0.
  wpzd3 = 0.
  wpzd4 = 0.


 

IF tt-param.wdr  = "Detalhado"
    OR tt-param.wdr  = "Resumido" THEN DO:
      IF tt-param.wdr = "Detalhado" THEN wmsg = wmsg + " Detalhado ".
      IF tt-param.wdr = "Resumido" THEN wmsg = wmsg + " Resumido ".
      END.
   wmsg = "Estabelecimento: " + tt-param.westab + " " +  
          tt-param.wfami + "-" +  tt-param.wfamf + " " + wmsg + " Data: ".
  
    IF tt-param.wdtdd LT DATE(12,31,1990) THEN DO:
      MESSAGE "consulta permitida a partir de 31/12/1991".
      NEXT.
      END.
    wmsg = wmsg + " " + STRING(tt-param.wdtdd,"99/99/9999").

    ASSIGN wmsg = wmsg + " / Linha Prod.: " + string(tt-param.wnr-linha-ini) + " a " +  string(tt-param.wnr-linha-fim).    
  
  ASSIGN wdti = DATE(MONTH(tt-param.wdtdd),01,YEAR(tt-param.wdtdd)).
  ASSIGN wddmmaa = STRING(DAY(tt-param.wdtdd),"99")
                 + STRING(MONTH(tt-param.wdtdd),"99")
                 + STRING(YEAR(tt-param.wdtdd),"9999").

  wanomes = SUBSTRING(wddmmaa,5,4) + SUBSTRING(wddmmaa,3,2).
  wday  = INTEGER(SUBSTRING(wddmmaa,1,2)).
  wdtant = DATE(INTEGER(SUBSTRING(wddmmaa,3,2))
               ,INTEGER(SUBSTRING(wddmmaa,1,2))
               ,INTEGER(SUBSTRING(wddmmaa,5,4))) - 1.
  wanomesant = SUBSTRING(STRING(YEAR(wdtant),"9999"),1,4)
           + STRING(MONTH(wdtant),"99").




  
    FORM HEADER 
      SKIP(1) wmsg
              SKIP(1)
              "                                          PRODUZIR  ENV    SDO     ENV    PRZDO  NUM "
              SKIP
              "                                             MES    VDS   DATA    DT-1      ATE  LINHA"
              SKIP 
              "______________________________________________________________________________________"
              SKIP
      WITH FRAME f-cabec.


    /*.......................................INICIO TRATAMENTO DA FAMILIA...*/
    FOR EACH mgdis.fam-comerc
       WHERE mgdis.fam-comerc.fm-cod-com GE tt-param.wfami
       AND   mgdis.fam-comerc.fm-cod-com LE tt-param.wfamf
       NO-LOCK:

         
      /*.......................................INICIO TRATAMENTO DOS ITEMS...*/
      ASSIGN wctditem = 0.
      FOR EACH mgind.item 
         WHERE mgind.item.fm-cod-com = mgdis.fam-comerc.fm-cod-com
         AND   mgind.item.cod-obsoleto NE 4
         AND   mgind.item.it-codigo MATCHES(tt-param.wite)
         NO-LOCK:

        /* 07/08/18 - TRATAMENTO NO. LINHA - SOL. FABIO */ 
        FIND FIRST  item-uni-estab WHERE item-uni-estab.it-codigo =  ITEM.it-codigo
                           AND   item-uni-estab.cod-estabel = "1"
                           AND   (item-uni-estab.nr-linha GE  tt-param.wnr-linha-ini
                           AND item-uni-estab.nr-linha LE  tt-param.wnr-linha-fim)
                           NO-LOCK NO-ERROR.    
       
         IF NOT AVAILABLE item-uni-estab  THEN NEXT.
         /* 07/08/18*/
         run pi-acompanhar in h-acomp (input "Item: " + string(item.it-codigo)).

        ASSIGN wenvx = 0.
        FOR EACH mgfas.lo-plames
           WHERE mgfas.lo-plames.it-codigo = mgind.item.it-codigo
             AND mgfas.lo-plames.anomes      = wanomes
           NO-LOCK:
          wpgr2 = wpgr2 + mgfas.lo-plames.pl-przir[2].
          wpgr3 = wpgr3 + mgfas.lo-plames.pl-przir[2].
          wpgr4 = wpgr4 + mgfas.lo-plames.pl-przir[2].
          wenvx = 0.

          END.

        /*..........INICIO MANTAGEM DO ENVIADO E PRODUZIDO DE 01 ATE DIA.*/
        FOR EACH movind.movto-estoq
           WHERE movind.movto-estoq.cod-estabel = tt-param.westab
           AND   movind.movto-estoq.it-codigo   = mgind.item.it-codigo
           AND   movind.movto-estoq.dt-trans   GE wdti
           AND   movind.movto-estoq.dt-trans   LE tt-param.wdtdd
           AND ((movind.movto-estoq.esp-docto    =  1 OR
                 movind.movto-estoq.esp-docto    = 25 OR
                 movind.movto-estoq.esp-docto    =  8)
                                 OR
                (movind.movto-estoq.esp-docto    = 28 AND
                               movind.movto-estoq.serie          GE "0"      AND
                               movind.movto-estoq.serie          LE "999")
                                 OR
                (movind.movto-estoq.esp-docto    = 31 AND
                               movind.movto-estoq.serie          GE "0"      AND
                               movind.movto-estoq.serie          LE "999"))
           NO-LOCK:

          ASSIGN wquant = movind.movto-estoq.quantidade.
          IF movind.movto-estoq.tipo-trans = 2  THEN DO:
            ASSIGN wquant = wquant * -1.
            END.
          ASSIGN wenv2 = wenv2 + wquant                           
                 wenv3 = wenv3 + wquant                           
                 wenv4 = wenv4 + wquant                           
                 wenvx = wenvx + wquant.                          

          ASSIGN wpzd2 = wpzd2 + wquant                            
                 wpzd3 = wpzd3 + wquant                          
                 wpzd4 = wpzd4 + wquant.                           

          IF movind.movto-estoq.esp-docto = 25
          OR movind.movto-estoq.esp-docto = 28 
          OR movind.movto-estoq.esp-docto = 31
          THEN DO:
            ASSIGN wqestorno = movind.movto-estoq.quantidade.
            IF (movind.movto-estoq.esp-docto = 25 OR
                movind.movto-estoq.esp-docto = 31)
            AND movind.movto-estoq.tipo-trans = 1
            THEN DO:
              ASSIGN wqestorno = wqestorno * -1.
              END.

            ASSIGN wpzd2 = wpzd2 + wqestorno                        
                   wpzd3 = wpzd3 + wqestorno
                   wpzd4 = wpzd4 + wqestorno.                        
            END.

          END.
        /*..........FIM DA MONTAGEM DO ENVIADO E PRODUZIDO DE 01 ATE DIA*/

        ASSIGN wsdx = 0.
        FOR EACH mgfas.lo-plames
           WHERE mgfas.lo-plames.it-codigo = mgind.item.it-codigo
             AND mgfas.lo-plames.anomes      = wanomes
           NO-LOCK:
          ASSIGN wsdx = wsdx
               +  mgfas.lo-plames.pl-przir[2].         
          END.
        ASSIGN wsdx = wsdx - wenvx.


        IF wsdx LT 0 THEN DO:
          wsdn2 = wsdn2 + wsdx.
          wsdn3 = wsdn3 + wsdx.
          wsdn4 = wsdn4 + wsdx.
          END.
        IF wsdx GT 0 THEN DO:
          wsdp2 = wsdp2 + wsdx.
          wsdp3 = wsdp3 + wsdx.
          wsdp4 = wsdp4 + wsdx.
          END.

        /*.....................INICIO MANTAGEM DO ENVIADO DIA -1........*/
        FOR EACH movind.movto-estoq
           WHERE movind.movto-estoq.cod-estabel = "1"
           AND   movind.movto-estoq.it-codigo   = mgind.item.it-codigo
           AND   movind.movto-estoq.dt-trans   =  tt-param.wdtdd - 1
           AND ((movind.movto-estoq.esp-docto  =  1 OR
                 movind.movto-estoq.esp-docto  = 25 OR
                 movind.movto-estoq.esp-docto  =  8)
                                 OR
                (movind.movto-estoq.esp-docto  = 28 AND
                               movind.movto-estoq.serie          GE "0"      AND
                               movind.movto-estoq.serie          LE "999")
                                 OR
                (movind.movto-estoq.esp-docto  = 31 AND
                               movind.movto-estoq.serie          GE "0"      AND
                               movind.movto-estoq.serie          LE "999"))

           NO-LOCK:

          ASSIGN wquant = movind.movto-estoq.quantidade.
          IF movind.movto-estoq.tipo-trans = 2  THEN DO:
            ASSIGN wquant = wquant * -1.
            END.
          ASSIGN weda2 = weda2 + wquant                           
                 weda3 = weda3 + wquant                           
                 weda4 = weda4 + wquant.                          
          END.
        /*.....................FIM DA MONTAGEM DO ENVIADO DIA -1.......*/


        IF tt-param.wdr = "Detalhado"  THEN DO:

          IF wpgr2 <> 0
          OR wenv2 <> 0
          OR wsdp2 <> 0
          OR wsdn2 <> 0
          OR weda2 <> 0
          OR wpzd2 <> 0 THEN DO:
            ASSIGN wlit1 = STRING(mgind.item.it-codigo,"x(13)")
                         + " "
                         + STRING(mgind.item.descricao-1,"x(18)")
                         + FILL(" ",2)
                         + FILL(" ",6)                    
                         + STRING(wpgr2,"zzzzzz9-")
                         + STRING(wenv2,"zzzzzz9-")
                         + STRING(wsdx,"zzzzzz9-")
                         + STRING(weda2,"zzzzzz9-")
                         + STRING(wpzd2,"zzzzzz9-").
            ASSIGN wdet = wlit1.
           /*17/08/18*/
              IF AVAILABLE item-uni-estab THEN
               ASSIGN wdet = wdet + " " +  STRING(item-uni-estab.nr-linha).
              /* 17/08/18 fim */
            DISPLAY wdet WITH FRAME f-relat NO-LABEL WITH STREAM-IO WIDTH 110.
            DOWN WITH FRAME f-relat.
            ASSIGN wpgr2 = 0
                   wenv2 = 0
                   wsdp2 = 0
                   wsdn2 = 0
                   weda2 = 0
                   wpzd2 = 0.
            END.

          END. 
        END.
      /*.....................FIM DE TRATAMENTO DE ITEM...................*/
  
      IF wpgr3 <> 0
      OR wenv3 <> 0
      OR wsdp3 <> 0
      OR wsdn3 <> 0
      OR weda3 <> 0
      OR wpzd3 <> 0 THEN DO:
        ASSIGN wlit1 = "FAMILIA "
                     + STRING(mgdis.fam-comerc.fm-cod-com,"x(8)")
                     + " TOTAL ATRASADO =>"
                     + FILL(" ",6)                
                     + STRING(wpgr3,"zzzzzz9-")
                     + STRING(wenv3,"zzzzzz9-")
                     + STRING(wsdp3,"zzzzzz9-")
                     + STRING(weda3,"zzzzzz9-")
                     + STRING(wpzd3,"zzzzzz9-").
        ASSIGN wlit2 = STRING(mgdis.fam-comerc.descricao,"x(20)")
                     + "  ADIANTADO => " 
                     + FILL(" ",21)
                     + STRING(wsdn3,"zzzzzz9-") 
                     + FILL(" ",14).
        ASSIGN wdet = wlit0.
        PUT wdet SKIP.
        ASSIGN wdet = wlit1.
        PUT wdet SKIP.
        ASSIGN wdet = wlit2.
        PUT wdet SKIP(1).
        IF tt-param.wdr = "Detalhado" THEN DOWN 1.
        ASSIGN wpgr3 = 0
               wenv3 = 0
               wsdp3 = 0
               wsdn3 = 0
               weda3 = 0
               wpzd3 = 0.
        END.

      END.
    /*.......................FIM DE TRATAMENTO DE FAMILIA................*/

    IF wpgr4 <> 0
    OR wenv4 <> 0
    OR wsdp4 <> 0
    OR wsdn4 <> 0
    OR weda4 <> 0
    OR wpzd4 <> 0 THEN DO:
      ASSIGN wlit1 = "TOTAL DA CONSULTA      ATRASADO =>"
                   + FILL(" ",6)              
                   + STRING(wpgr4,"zzzzzz9-")
                   + STRING(wenv4,"zzzzzz9-")
                   + STRING(wsdp4,"zzzzzz9-")
                   + STRING(weda4,"zzzzzz9-")
                   + STRING(wpzd4,"zzzzzz9-").
      ASSIGN wlit2 = FILL(" ",23) + "ADIANTADO =>"
                   + FILL(" ",21)
                   + STRING(wsdn4,"zzzzzz9-")
                   + FILL(" ",14).
      ASSIGN wdet = wlit0.
      PUT wdet SKIP.
/*       DISPLAY wdet WITH FRAME f-atraso NO-LABEL WITH STREAM-IO. */
/*       DOWN WITH FRAME f-atraso .                                */
      ASSIGN wdet = wlit1.
      PUT wdet SKIP.
/*       DISPLAY wdet WITH FRAME f-atraso NO-LABEL WITH STREAM-IO. */
/*       DOWN WITH FRAME f-atraso .                                */
      ASSIGN wdet = wlit2.
      PUT wdet SKIP(1).
/*       DISPLAY wdet WITH FRAME f-atraso NO-LABEL WITH STREAM-IO. */
/*       DOWN WITH FRAME f-atraso .                                */
      ASSIGN wpgr4 = 0
             wenv4 = 0
             wsdp4 = 0
             wsdn4 = 0
             weda4 = 0
             wpzd4 = 0.
      END.
END.






