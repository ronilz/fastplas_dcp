&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0575RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD warqse           AS CHAR FORMAT "X(50)"
    FIELD warqbat          AS CHAR FORMAT "X(50)"
    FIELD warqs1           AS CHAR FORMAT "X(50)"
    FIELD warqs2           AS CHAR FORMAT "X(50)"
    FIELD waccli           AS CHAR FORMAT "X(1)".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */
/***********************************************************************
**
** li0575.p   - Converte Pedido Batch          
**
**              13/09/2004  - BENE   Programa novo.
**
**              LAYOUT
**              Numero do pedido.........  1 - 12   x(12)
**              Sequencia................ 13 - 17   9(05)
**                                 filler 18 - 18   x(01)
**              Codigo do Item........... 19 - 37   x(19)
**                                 filler 38 - 48   x(11)
**              Quantidade............... 49 - 57   x(09)
**                                 filler 58 - 58   x(01)
**              Codigo do Cliente........ 59 - 64   9(06)
**                                 filler 65 - 65   x(01)
**              Pedido da Revenda........ 66 - 78   x(13)
**             -----------------------------
**             Altera��o 
**             12/03/2008 - Conf.solicitacao J. Luiz
**             no registro 07, posicao 165, sera assinalado
**             conforme campo ind-uf-subs na tabela unid-feder,
**             conforme o estado e tambem se o item estiver na tabela
**             item-uf sera assinalado com 1 que equivale a sim.
**             Obs.: 0 equivale a nao
**
***********************************************************************/
/*
DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li0575".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "        Converte Pedido Batch         ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)". */

DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

/*DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".

DEFINE VARIABLE warqse     AS CHAR FORMAT "x(40)".                    
DEFINE VARIABLE warqs1     AS CHAR FORMAT "x(20)".                    
DEFINE VARIABLE warqs2     AS CHAR FORMAT "x(20)".                    
DEFINE VARIABLE warqbat    AS CHAR FORMAT "x(20)".                    
DEFINE VARIABLE waccli     AS CHAR FORMAT "x(01)".     */               

DEFINE VARIABLE wseq       AS INTE INITIAL 0.

DEFINE VARIABLE wrel        AS CHAR FORMAT "x(50)".
DEFINE VARIABLE wreg        AS CHAR FORMAT "x(471)".

DEFINE VARIABLE wcodcli    AS INTE FORMAT "999999".
DEFINE VARIABLE wpedido    AS CHAR FORMAT "x(12)".
DEFINE VARIABLE wseque     AS INTE FORMAT "99999".

DEFINE VARIABLE wanomes   LIKE    lo-plames.anomes.          
DEFINE VARIABLE wite      LIKE    lo-plames.it-codigo.
DEFINE VARIABLE wnomea    LIKE    lo-plames.nome-abrev.

DEFINE VARIABLE wnerro    AS INTE.
DEFINE VARIABLE wtberro   AS CHAR FORMAT "x(50)" EXTENT 20.
DEFINE VARIABLE wcte      AS INTE.

/*............................inicio solicitacao 12-04-2005 regiane.....*/
DEFINE VARIABLE wpedrev   AS CHAR FORMAT "x(13)".
/*............................fim de solicitacao 12-04-2005 regiane.....*/


/*-------------------------------------------------------------inicio novo1--*/
/*-----------------------------------------------------------VARIAVEIS LOCAIS*/
DEFINE VARIABLE  v_chr_arquivo      AS CHAR FORMAT "x(40)" NO-UNDO.
DEFINE VARIABLE  v_chr_cod_emitente AS CHAR FORMAT "x(06)" NO-UNDO.
DEFINE VARIABLE  v_chr_linha        AS CHAR FORMAT "x(80)" NO-UNDO.
DEFINE VARIABLE  wtitrel            AS CHAR FORMAT "x(38)".         
DEFINE VARIABLE  v_dec_vl_preori    LIKE ped-item.vl-preori NO-UNDO.

DEFINE VARIABLE wpos AS INTE FORMAT "99".
DEFINE VARIABLE wctd AS INTE FORMAT "99".
DEFINE VARIABLE wmes AS INTE FORMAT "99".
DEFINE VARIABLE wano AS INTE FORMAT "9999".
DEFINE VARIABLE wdtx AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wmmm
                AS CHAR FORMAT "x(3)" EXTENT 12
                INITIAL ["jan","fev","mar","abr","mai","jun",
                         "jul","ago","set","out","nov","dez"].
DEFINE VARIABLE wcodestab AS CHAR FORMAT "x(3)".
DEFINE VARIABLE wttped    AS INTE.


/*-----------------------------------------------------------------TEMP-TABLE*/
DEFINE TEMP-TABLE  tt_pedidos   NO-UNDO
        FIELD nr-pedcli         LIKE ped-venda.nr-pedcli
        FIELD nr-sequencia      LIKE ped-item.nr-sequencia
        FIELD it-codigo         LIKE item.it-codigo
        FIELD qt-pedida         AS   DECIMAL FORMAT ">>>>,>>9.99"
        FIELD cod-cli-vw        LIKE es-pedido-vw.cod-cli-vw
        FIELD cod-emitente      LIKE emitente.cod-emitente
        FIELD observacao        AS   CHAR FORMAT "x(60)"
        FIELD situacao          AS   INTEGER
        INDEX idx-codigo        IS UNIQUE PRIMARY cod-emitente
                                                  nr-pedcli
                                                  nr-sequencia
        INDEX idx-situacao                        situacao.


DEFINE TEMP-TABLE tt_pedfase
        FIELD rec-pedido    AS RECID
        FIELD observacao    AS CHARACTER    FORMAT "x(50)"
        FIELD situacao      AS INTEGER
        FIELD dt-criacao    AS DATE.


DEFINE TEMP-TABLE tt_pedven
           FIELD nome-abrev       LIKE ped-venda.nome-abrev           
           FIELD nr-pedcli        LIKE ped-venda.nr-pedcli              
           FIELD nr-pedido        LIKE ped-venda.nr-pedido            
           FIELD cod-estabel      LIKE ped-venda.cod-estabel          
           FIELD nr-pedrep        LIKE ped-venda.nr-pedrep
           FIELD dt-emissao       LIKE ped-venda.dt-emissao
           FIELD dt-implant       LIKE ped-venda.dt-implant        
           FIELD dt-entrega       LIKE ped-venda.dt-entrega         
           FIELD dt-entorig       LIKE ped-venda.dt-entorig        
           FIELD nat-operacao     LIKE ped-venda.nat-operacao           
           FIELD esp-ped          AS INTE FORMAT "99"
           FIELD cod-cond-pag     LIKE ped-venda.cod-cond-pag            
           FIELD nr-tabpre        LIKE ped-venda.nr-tabpre      
           FIELD nr-tab-finan     LIKE ped-venda.nr-tab-finan
           FIELD nr-ind-finan     LIKE ped-venda.nr-ind-finan
           FIELD cod-priori       LIKE ped-venda.cod-priori
           FIELD local-entreg     LIKE ped-venda.local-entreg         
           FIELD bairro           LIKE ped-venda.bairro              
           FIELD cidade           LIKE ped-venda.cidade              
           FIELD estado           LIKE ped-venda.estado               
           FIELD cep              LIKE ped-venda.cep                   
           FIELD pais             LIKE ped-venda.pais               
           FIELD cgc              LIKE ped-venda.cgc                  
           FIELD ins-estadual     LIKE ped-venda.ins-estadual           
           FIELD perc-desco1      LIKE ped-venda.perc-desco1
           FIELD perc-desco2      LIKE ped-venda.perc-desco2
           FIELD cond-redespa     LIKE ped-venda.cond-redespa
           FIELD cidade-cif       LIKE ped-venda.cidade-cif      
           FIELD cod-portador     LIKE ped-venda.cod-portador        
           FIELD modalidade       LIKE ped-venda.modalidade           
           FIELD cod-mensagem     LIKE ped-venda.cod-mensagem             
           FIELD user-impl        LIKE ped-venda.user-impl       
           FIELD dt-userimp       LIKE ped-venda.dt-userimp
           FIELD ind-aprov        LIKE ped-venda.ind-aprov
           FIELD quem-aprovou     LIKE ped-venda.quem-aprovou
           FIELD dt-apr-cred      LIKE ped-venda.dt-apr-cred     
           FIELD nome-transp      LIKE ped-venda.nome-transp           
           FIELD tp-preco         AS   INTE FORMAT "99"                
           FIELD ind-fat-par      LIKE ped-venda.ind-fat-par
           FIELD mo-codigo        LIKE ped-venda.mo-codigo                 
           FIELD ind-lib-nota     LIKE ped-venda.ind-lib-nota
           FIELD vl-tot-ped       LIKE ped-venda.vl-tot-ped
           FIELD vl-liq-ped       LIKE ped-venda.vl-liq-ped
           FIELD vl-liq-abe       LIKE ped-venda.vl-liq-abe
           FIELD per-max-canc     LIKE ped-venda.per-max-canc  
           FIELD no-ab-reppri     LIKE ped-venda.no-ab-reppri
           FIELD ind-antecip      LIKE ped-venda.ind-antecip
           FIELD distancia        LIKE ped-venda.distancia
           FIELD taxa-orig        LIKE ped-venda.taxa-orig
           FIELD taxa-real        LIKE ped-venda.taxa-real
           FIELD vl-mer-abe       LIKE ped-venda.vl-mer-abe
           FIELD user-aprov       LIKE ped-venda.user-aprov
           FIELD dsp-pre-fat      LIKE ped-venda.dsp-pre-fat
           FIELD cod-emitente     LIKE ped-venda.cod-emitente
           FIELD per-des-icms     LIKE ped-venda.per-des-icms
           FIELD vl-cred-lib      LIKE ped-venda.vl-cred-lib
           FIELD inc-desc-txt     LIKE ped-venda.inc-desc-txt
           FIELD tp-receita       LIKE ped-venda.tp-receita
           FIELD cod-gr-cli       LIKE ped-venda.cod-gr-cli
           FIELD atendido         LIKE ped-venda.atendido
           FIELD permissao        LIKE ped-venda.permissao
           FIELD observacoes      AS   CHAR FORMAT "x(80)"
        INDEX ch-pedseq         IS UNIQUE PRIMARY nr-pedido
        INDEX ch-pedido         IS UNIQUE         nome-abrev
                                                  nr-pedcli.



DEFINE TEMP-TABLE tt_pedent                        
           FIELD nome-abrev    LIKE ped-ent.nome-abrev
           FIELD nr-pedcli     LIKE ped-ent.nr-pedcli
           FIELD nr-sequencia  LIKE ped-ent.nr-sequencia
           FIELD it-codigo     LIKE ped-ent.it-codigo
           FIELD nr-entrega    LIKE ped-ent.nr-entrega
           FIELD dt-entorig    LIKE ped-ent.dt-entorig
           FIELD dt-entrega    LIKE ped-ent.dt-entrega
           FIELD user-alte     LIKE ped-ent.user-alte
           FIELD dt-useralt    LIKE ped-ent.dt-useralt
           FIELD qt-pedida     LIKE ped-ent.qt-pedida
           FIELD user-impl     LIKE ped-ent.user-impl 
           FIELD dt-userimp    LIKE ped-ent.dt-userimp
           FIELD vl-merc-abe   LIKE ped-ent.vl-merc-abe
           FIELD vl-liq-it     LIKE ped-ent.vl-liq-it
           FIELD vl-liq-abe    LIKE ped-ent.vl-liq-abe
           FIELD vl-tot-it     LIKE ped-ent.vl-tot-it
        INDEX codigo            IS UNIQUE PRIMARY nome-abrev
                                                  nr-pedcli
                                                  nr-sequencia
                                                  it-codigo
                                                  nr-entrega.

DEFINE TEMP-TABLE tt_peditem
           FIELD nome-abrev       LIKE ped-item.nome-abrev
           FIELD nr-pedcli        LIKE ped-item.nr-pedcli
           FIELD nr-sequencia     LIKE ped-item.nr-sequencia        
           FIELD it-codigo        LIKE ped-item.it-codigo
           FIELD dt-entorig       LIKE ped-item.dt-entorig
           FIELD dt-entrega       LIKE ped-item.dt-entrega
           FIELD qt-pedida        LIKE ped-item.qt-pedida
           FIELD aliquota-ipi     LIKE ped-item.aliquota-ipi
           FIELD vl-pretab        LIKE ped-item.vl-pretab
           FIELD vl-preori        LIKE ped-item.vl-preori
           FIELD vl-preuni        LIKE ped-item.vl-preuni
           FIELD vl-merc-abe      LIKE ped-item.vl-merc-abe 
           FIELD vl-liq-it        LIKE ped-item.vl-liq-it 
           FIELD vl-liq-abe       LIKE ped-item.vl-liq-abe 
           FIELD vl-tot-it        LIKE ped-item.vl-tot-it
           FIELD user-impl        LIKE ped-item.user-impl
           FIELD dt-userimp       LIKE ped-item.dt-userimp
           FIELD ind-icm-ret      LIKE ped-item.ind-icm-ret
           FIELD nr-tabpre        LIKE ped-item.nr-tabpre
           FIELD per-des-icms     LIKE ped-item.per-des-icms
           FIELD nat-operacao     LIKE ped-item.nat-operacao
           FIELD dt-min-fat       LIKE ped-item.dt-min-fat
           FIELD cod-entrega      LIKE ped-item.cod-entrega
           FIELD ind-fat-qtfam    LIKE ped-item.ind-fat-qtfam
           FIELD observacao       AS CHAR FORMAT "x(80)"
           FIELD wicmsretido      AS  INTEGER FORMAT "9"
        INDEX ch-item-ped       IS UNIQUE PRIMARY nome-abrev  
                                                  nr-pedcli
                                                  nr-sequencia
                                                  it-codigo.


DEFINE TEMP-TABLE tt_pedrep   LIKE ped-repre.   
DEFINE TEMP-TABLE tt_pedbatch LIKE es-pedido-vw.


/* variavel para assinalar o campo icms retido da fonte, no registro 07, campo posi��o 165*/
DEFINE VARIABLE wicmsretido       AS INTEGER FORMAT "9" INITIAL 0.

/*-------------------------------------------------------------BUFFERS LOCAIS*/
DEFINE BUFFER  buf_pedido FOR tt_pedven.

/*-------------------------------------------------------------fim de novo1--*/

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Converte Pedido Batch".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).*/
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
 /*........................................inicio numero de erros...*/
ASSIGN wnerro = 0.

IF tt-param.waccli = ""           
THEN DO:
  ASSIGN wnerro = wnerro + 1.
  ASSIGN wtberro[wnerro] = "ajusta codigo cliente esta em branco".
  END.

ASSIGN wrel = tt-param.warqbat.
IF SEARCH(wrel) = wrel           
THEN DO:
  ASSIGN wnerro = wnerro + 1.
  ASSIGN wtberro[wnerro] = "Ja existe arq batch com este nome: "
                         + tt-param.warqbat.
  END.


ASSIGN wrel = tt-param.warqs2.
IF SEARCH(wrel) = wrel           
THEN DO:
  ASSIGN wnerro = wnerro + 1.
  ASSIGN wtberro[wnerro] = "Ja existe rel.CRITICA CONVERSAO com este nome: "
                         + tt-param.warqs2.
  END.


ASSIGN wrel = tt-param.warqs1.
IF SEARCH(wrel) = wrel           
THEN DO:
  ASSIGN wnerro = wnerro + 1.
  ASSIGN wtberro[wnerro] = "Ja existe rel.CRITICA INICIAL com este nome: "
                         + tt-param.warqs1.
  END.

ASSIGN wrel = tt-param.warqse.
IF SEARCH(wrel) = ?           
THEN DO:
  ASSIGN wnerro = wnerro + 1.
  ASSIGN wtberro[wnerro] = "Arquivo nao encontrado: "
                         + tt-param.warqse.
  END.
END.
  /*........................................fim de numero de erros...*/


/*........................................inicio exibe erros.......*/
DO wcte = 1 TO wnerro:
  MESSAGE "Erro " + STRING(wcte,">>9") + " " + STRING(wtberro[wcte]).

  END.
IF wnerro NE 0 THEN QUIT.
/*........................................fim de exibe erros.......*/


/*-------------------------------------------------------------inicio novo2--*/
/*-----------------------------------------------------------Bloco Principal*/
RUN PI_ImportaDados.
DO TRANSACTION:
  Run PI_AtualizaTabelaEspecifica.
  END.
RUN PI_ExecutaImpressao.
 
ASSIGN wttped = 0.
FOR EACH tt_pedfase:
  DELETE tt_pedfase.
  END.
DO ON ENDKEY UNDO, LEAVE:
  RUN PI_AbreQueryPedidosPendentes.
  END.

IF wttped = 0 THEN DO:
  MESSAGE "Nao existem pedidos pendentes para conversao !".
  QUIT.
  END.

RUN PI_EfetivaGeracaoPedidos_de_Venda.
IF RETURN-VALUE="NOK" THEN RETURN NO-APPLY.
RUN PI_ImpressaoFinal.
RUN PI_ArquivoBatch.

 
/*-------------------------------------------------------Procedures Internas*/

PROCEDURE PI_ImportaDados.
  ASSIGN v_chr_arquivo = tt-param.warqse.
  DO ON ENDKEY UNDO, RETURN:
    IF SEARCH(v_chr_arquivo) = ?
    THEN DO:
      MESSAGE "Arquivo nao encontrado !!!" v_chr_arquivo.
      QUIT.
      END.
              
    FIND 
    FIRST tt_pedbatch 
    WHERE tt_pedbatch.nome-arq = v_chr_arquivo
    NO-LOCK
    NO-ERROR.

    IF AVAIL(tt_pedbatch) 
    THEN DO:
      MESSAGE "Ja existem PEDIDOS FORD importados de arq. com este nome.".
      QUIT.
      END.
              
    FOR EACH tt_pedidos EXCLUSIVE-LOCK:
      DELETE tt_pedidos.
      END.

    INPUT FROM VALUE(v_chr_arquivo) NO-ECHO.
    pi_importa:
    REPEAT:
      IMPORT UNFORMATTED v_chr_linha.
                 
      ASSIGN wseq = wseq + 10.

      IF SUBSTRING(v_chr_linha,60,01) = "T" THEN DO:
        ASSIGN
         SUBSTRING(v_chr_linha,60,01) = "0".        
        END.


      CREATE tt_pedidos.
      ASSIGN tt_pedidos.nr-pedcli    =     SUBSTR(v_chr_linha,01,12)
             tt_pedidos.nr-sequencia = wseq
             tt_pedidos.it-codigo    =     SUBSTR(v_chr_linha,19,19)
             tt_pedidos.qt-pedida    = DEC(SUBSTR(v_chr_linha,49,10)) /* / 100*/
             tt_pedidos.cod-cli-vw   =     SUBSTR(v_chr_linha,59,06)
             tt_pedidos.cod-emitente = INT(SUBSTR(v_chr_linha,59,06)).

      ASSIGN v_chr_cod_emitente 
           = tt-param.waccli
           + SUBSTRING(tt_pedidos.cod-cli-vw,2,5).
           
      ASSIGN tt_pedidos.cod-emitente = INT(v_chr_cod_emitente).     
      /*05/03/08 - alteracao icmsretido */     
   
      FIND  emitente
        WHERE emitente.cod-emitente = tt_pedidos.cod-emitente
        NO-LOCK NO-ERROR.
     
      ASSIGN wpedrev = SUBSTRING(v_chr_linha,66,13).

      IF NOT AVAIL(emitente) 
      THEN DO:
        ASSIGN tt_pedidos.situacao   = 9
               tt_pedidos.observacao  = STRING(wpedrev)
                                      + " Cliente nao cadastrado.".
        NEXT pi_importa.
        END.
                 
      FIND  item 
      WHERE item.it-codigo = tt_pedidos.it-codigo
      NO-LOCK
      NO-ERROR.
      
      IF NOT AVAIL(item) 
      THEN DO:
        ASSIGN tt_pedidos.situacao   = 9
               tt_pedidos.observacao = STRING(wpedrev)
                                     + " Item nao cadastrado.".
        NEXT pi_importa.
        END.
                 
      FIND  tt_pedbatch 
      WHERE tt_pedbatch.cod-cli-vw = tt_pedidos.cod-cli-vw   
      AND   tt_pedbatch.nr-pedcli  = tt_pedidos.nr-pedcli    
      AND   tt_pedbatch.linha      = tt_pedidos.nr-sequencia
      NO-LOCK
      NO-ERROR.

      IF AVAIL(tt_pedbatch) 
      THEN DO:
        ASSIGN tt_pedidos.situacao   = 9
               tt_pedidos.observacao = STRING(wpedrev)
                     + " Pedido ja importado p/tab temp(tt_pedbatch).".
        NEXT pi_importa.
        END.
                 
      FIND  ped-venda 
      WHERE ped-venda.nome-abrev = emitente.nome-abrev 
      AND   ped-venda.nr-pedcli  = tt_pedidos.nr-pedcli
      NO-LOCK
      NO-ERROR.

      IF AVAIL(ped-venda) 
      THEN DO:
        ASSIGN tt_pedidos.situacao   = 9
               tt_pedidos.observacao = STRING(wpedrev)
                                     + " Pedido ja cadastrado p/cliente".
        NEXT pi_importa.
        END.
                 
      IF tt_pedidos.qt-pedida = 0 
      THEN DO:
        ASSIGN tt_pedidos.situacao   = 9
               tt_pedidos.observacao = STRING(wpedrev)
                                     + " Qtd.deve ser diferente de 0.".
        NEXT pi_importa.
        END.

      ASSIGN tt_pedidos.situacao   = 1.

      ASSIGN tt_pedidos.observacao = SUBSTRING(v_chr_linha,66,13).
      ASSIGN SUBSTRING(tt_pedidos.observacao,14,13)
                    = SUBSTRING(v_chr_linha,01,13).
      ASSIGN SUBSTRING(tt_pedidos.observacao,27,25)
                    = "=PASSADO PARA CONVERSAO..".

      END.
    END.
  RETURN.
END.


PROCEDURE PI_AtualizaTabelaEspecifica.
  FOR 
  EACH  tt_pedidos 
  WHERE tt_pedidos.situacao = 1
  NO-LOCK:
    CREATE tt_pedbatch.
    ASSIGN tt_pedbatch.nr-pedcli = tt_pedidos.nr-pedcli
           tt_pedbatch.linha     = tt_pedidos.nr-sequencia
           tt_pedbatch.it-codigo    = tt_pedidos.it-codigo
           tt_pedbatch.qt-pedida    = tt_pedidos.qt-pedida
           tt_pedbatch.cod-cli-vw   = tt_pedidos.cod-cli-vw
           tt_pedbatch.cod-emitente = tt_pedidos.cod-emitente
           tt_pedbatch.nome-arq     = v_chr_arquivo
           tt_pedbatch.user-import  = wusr           
           tt_pedbatch.data-import  = TODAY
           tt_pedbatch.hora-import  = STRING(TIME,"HH:MM")
           tt_pedbatch.situacao  = 1.

    ASSIGN tt_pedbatch.observacao = tt_pedidos.observacao.

    END.
  RETURN.
END.
 
 
PROCEDURE PI_ExecutaImpressao.
  ASSIGN wrel = tt-param.warqs1.
  OUTPUT TO VALUE(wrel) PAGE-SIZE 60.

  IF CAN-FIND(FIRST tt_pedidos 
              WHERE tt_pedidos.situacao = 9) 
  THEN DO:
    ASSIGN wtitrel = "PEDIDO FORD - REJEITADOS".
    VIEW FRAME f-cabec.

    FOR
    EACH  tt_pedidos 
    WHERE tt_pedidos.situacao = 9
    NO-LOCK:
      DISPLAY tt_pedidos.nr-pedcli       COLUMN-LABEL "PedidoCli"
              tt_pedidos.nr-sequencia    COLUMN-LABEL "Linha"
              tt_pedidos.it-codigo
              tt_pedidos.qt-pedida       COLUMN-LABEL "Quantidade"
              tt_pedidos.cod-cli-vw      COLUMN-LABEL "Cliente   "
              tt_pedidos.cod-emitente    COLUMN-LABEL "Emitente"
              tt_pedidos.observacao      COLUMN-LABEL "Observacao"
      WITH WIDTH 132 FRAME f_impressao DOWN STREAM-IO.
      DOWN WITH FRAME f_impressao. 
      END.
    PAGE.
    END.
           
  IF CAN-FIND(FIRST tt_pedidos
              WHERE tt_pedidos.situacao = 1) 
  THEN DO:
    ASSIGN wtitrel = "PEDIDOS FORD - LIBERADOS P/ CONFIRMACAO".
    VIEW FRAME f-cabec.

    FOR
    EACH  tt_pedidos 
    WHERE tt_pedidos.situacao = 1
    NO-LOCK:
      DISPLAY tt_pedidos.nr-pedcli     COLUMN-LABEL "PedidoCli"
              tt_pedidos.nr-sequencia  COLUMN-LABEL "Linha"
              tt_pedidos.it-codigo
              tt_pedidos.qt-pedida     COLUMN-LABEL "Quantidade"
                                       FORMAT ">>>>,>>9.99"
              tt_pedidos.cod-cli-vw    COLUMN-LABEL "Cliente   "
              tt_pedidos.cod-emitente  COLUMN-LABEL "Emitente"
              tt_pedidos.observacao    COLUMN-LABEL "Observacao"
      WITH WIDTH 132 FRAME f_impressao_ok DOWN STREAM-IO .
      DOWN WITH FRAME f_impressao_ok.
      END.
    END.
  OUTPUT CLOSE.
  /*UNIX SILENT chown VALUE(wusr) VALUE(wrel).*/
  RETURN.  
END.
 

PROCEDURE PI_AbreQueryPedidosPendentes.
  FOR 
  EACH  tt_pedbatch 
  WHERE tt_pedbatch.situacao = 1
  NO-LOCK:
    CREATE tt_pedfase.
    ASSIGN tt_pedfase.rec-pedido = Recid(tt_pedbatch)
           tt_pedfase.dt-criacao = TODAY.
    ASSIGN wttped = wttped + 1.
    END.
  RETURN. 
END.


PROCEDURE PI_EfetivaGeracaoPedidos_de_Venda.
  FIND FIRST para-ped NO-LOCK NO-ERROR.

  FIND  tab-finan 
  WHERE tab-finan.nr-tab-finan = para-ped.tab-fin-pad
  NO-LOCK
  NO-ERROR.
           
  pi_pedidos:
  FOR EACH tt_pedfase EXCLUSIVE-LOCK:

    FIND  tt_pedbatch 
    WHERE RECID(tt_pedbatch) = tt_pedfase.rec-pedido
    EXCLUSIVE-LOCK NO-ERROR.

    FIND  emitente
    WHERE emitente.cod-emitente = tt_pedbatch.cod-emitente
    NO-LOCK NO-ERROR.

    IF emitente.cod-transp <> 0 
    THEN
    FIND  transporte 
    WHERE transporte.cod-transp = emitente.cod-transp
    NO-LOCK NO-ERROR.

    FIND  repres 
    WHERE repres.cod-rep = emitente.cod-rep
    NO-LOCK NO-ERROR.

    IF NOT AVAIL(repres)
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = 
             "Representante do Cliente nao cadastrado.".
      NEXT pi_pedidos.
      END.
                 
    FIND  cond-pagto
    WHERE cond-pagto.cod-cond-pag = emitente.cod-cond-pag
    NO-LOCK NO-ERROR.

    IF NOT AVAIL(cond-pagto) 
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = "Cond.Pagto do Cliente nao cadastrada.".
      NEXT pi_pedidos.
      END.
              
    FIND  natur-oper
    WHERE natur-oper.nat-operacao = emitente.nat-operacao
    NO-LOCK NO-ERROR.

    IF NOT AVAIL(natur-oper) 
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = "Nat.Operacao do Cliente nao cadastrada.".
      NEXT pi_pedidos.
      END.
              
    FIND  tb-preco 
    WHERE tb-preco.nr-tabpre = emitente.nr-tabpre 
    NO-LOCK NO-ERROR.

    IF NOT AVAIL(tb-preco) 
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = "Tab.Preco do Cliente nao cadastrada.".
      NEXT pi_pedidos.
      END.
              
    FIND
    FIRST  loc-entr 
    WHERE  loc-entr.nome-abrev = emitente.nome-abrev
    NO-LOCK NO-ERROR.
              
    FIND  tt_pedven 
    WHERE tt_pedven.nome-abrev = emitente.nome-abrev 
    AND   tt_pedven.nr-pedcli  = tt_pedbatch.nr-pedcli
    NO-ERROR.
              
    IF  AVAIL(tt_pedven)
    AND tt_pedfase.dt-criacao <> tt_pedven.dt-emissao 
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = "Pedido de Venda ja criado.".
      NEXT pi_pedidos.
      END.
              
    FIND  item 
    WHERE item.it-codigo = tt_pedbatch.it-codigo
    NO-LOCK NO-ERROR.

    IF NOT AVAIL(item) 
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = "Item nao cadastrado.".
      NEXT pi_pedidos.
      END.
              
    FIND
    FIRST preco-item 
    WHERE preco-item.nr-tabpre = emitente.nr-tabpre 
    AND   preco-item.it-codigo = item.it-codigo
    NO-LOCK NO-ERROR.

    IF NOT AVAIL(preco-item) 
    THEN DO:
      ASSIGN tt_pedfase.situacao   = 9
             tt_pedfase.observacao = "Item nao tem preco na tabela do Cliente.".
      NEXT pi_pedidos.
      END.
              
    ASSIGN tt_pedfase.situacao   = 1
           tt_pedfase.observacao = "Pedido convertido.......".
              
    IF NOT AVAIL(tt_pedven) 
    THEN
    RUN PI_CriaTabelatt_pedven.
              
    RUN PI_CriaItemDoPedido.
              
    ASSIGN tt_pedbatch.situacao   = 2      /* Atualizado Pedido no Magnus */
           tt_pedbatch.observacao = "Pedido Gerado com Sucesso.".
    END.
  RETURN.
END.
 
PROCEDURE PI_CriaItemDoPedido.
  DO ON ERROR UNDO, RETURN ERROR:
    ASSIGN v_dec_vl_preori = (preco-item.preco-venda
                           * tab-finan.tab-ind-fin[para-ped.nr-ind-fin]).
   
    CREATE tt_peditem.
    ASSIGN tt_peditem.nome-abrev           = tt_pedven.nome-abrev
           tt_peditem.nr-pedcli            = tt_pedven.nr-pedcli
           tt_peditem.nr-sequencia         = tt_pedbatch.linha
           tt_peditem.it-codigo            = item.it-codigo
           tt_peditem.dt-entorig           = tt_pedven.dt-entrega
           tt_peditem.dt-entrega           = tt_pedven.dt-entrega
           tt_peditem.qt-pedida            = tt_pedbatch.qt-pedida
           tt_peditem.aliquota-ipi         = item.aliquota-ipi
                      
           /*----------- Valores -------------------*/  
           tt_peditem.vl-pretab            = preco-item.preco-venda
           tt_peditem.vl-preori            = v_dec_vl_preori
           tt_peditem.vl-preuni            = v_dec_vl_preori
                      
                      
           tt_peditem.vl-merc-abe          = tt_peditem.qt-pedida 
                                         * tt_peditem.vl-preuni
           tt_peditem.vl-liq-it            = tt_peditem.qt-pedida 
                                         * tt_peditem.vl-preuni
           tt_peditem.vl-liq-abe           = tt_peditem.vl-liq-it 
                                         + ((tt_peditem.vl-liq-it  
                                         * tt_peditem.aliquota-ipi) / 100)
           tt_peditem.vl-tot-it            = tt_peditem.vl-liq-abe
                      
           tt_peditem.user-impl            = tt_pedven.user-impl
           tt_peditem.dt-userimp           = TODAY.
                      
    Assign tt_peditem.ind-icm-ret          = False     /* Retem ICMS Fonte */
           tt_peditem.nr-tabpre            = emitente.nr-tabpre
           tt_peditem.nat-operacao         = emitente.nat-operacao
           tt_peditem.dt-min-fat           = TODAY
           tt_peditem.ind-fat-qtfam        = False.     /* Fatura Familia */

    ASSIGN tt_peditem.observacao        
                    = SUBSTRING(tt_pedbatch.observacao,1,26).
    /*---------------------------------------------ICMS RETIDO ----------------*/
    /* 12-03-08 - SOL. JOSE LUIZ IMCSRETIDO -  */
    FIND FIRST MGCAD.unid-feder WHERE unid-feder.estado = emitente.estado 
                           AND   unid-feder.pais   = emitente.pais 
                           NO-LOCK NO-ERROR.
     IF AVAILABLE unid-feder AND unid-feder.ind-uf-subs = yes THEN DO:
        FIND FIRST item-uf WHERE item-uf.it-codigo = tt_peditem.it-codigo 
                           AND   item-uf.estado    = unid-feder.estado
                           AND   item-uf.pais      = unid-feder.pais
                           NO-LOCK NO-ERROR.
          IF AVAILABLE item-uf THEN ASSIGN wicmsretido = 1.
          IF NOT AVAILABLE item-uf THEN ASSIGN wicmsretido = 0.
          END.
     IF AVAILABLE unid-feder AND unid-feder.ind-uf-subs = NO THEN ASSIGN wicmsretido = 0.
     IF NOT AVAILABLE unid-feder THEN ASSIGN wicmsretido = 0.
    ASSIGN tt_peditem.wicmsretido          = wicmsretido.
    /*-----------------------------------------------ICMS RETIDO-----------------*/
                      
    CREATE tt_pedent.
    ASSIGN tt_pedent.nome-abrev    = tt_peditem.nome-abrev
           tt_pedent.nr-pedcli     = tt_peditem.nr-pedcli
           tt_pedent.nr-sequencia  = tt_peditem.nr-sequencia
           tt_pedent.it-codigo     = tt_peditem.it-codigo
           tt_pedent.nr-entrega    = 0
           tt_pedent.dt-entorig    = tt_peditem.dt-entorig
           tt_pedent.dt-entrega    = tt_peditem.dt-entrega
           tt_pedent.dt-useralt    = TODAY
           tt_pedent.qt-pedida     = tt_peditem.qt-pedida
           tt_pedent.user-impl     = tt_peditem.user-impl 
           tt_pedent.dt-userimp    = TODAY
           tt_pedent.vl-merc-abe   = tt_peditem.vl-merc-abe
           tt_pedent.vl-liq-it     = tt_peditem.vl-liq-it
           tt_pedent.vl-liq-abe    = tt_peditem.vl-liq-abe
           tt_pedent.vl-tot-it     = tt_peditem.vl-tot-it.
             
    ASSIGN tt_pedven.vl-tot-ped = tt_pedven.vl-tot-ped 
                                + tt_peditem.vl-tot-it
           tt_pedven.vl-liq-ped = tt_pedven.vl-liq-ped
                                + tt_peditem.vl-liq-it
           tt_pedven.vl-liq-abe = tt_pedven.vl-tot-ped
           tt_pedven.vl-mer-abe = tt_pedven.vl-mer-abe 
                                + tt_peditem.vl-merc-abe.
           
    END.
  RETURN.
END.
 

PROCEDURE PI_CriaTabelatt_pedven.
  DO ON ERROR UNDO, RETURN ERROR:
    FIND LAST buf_pedido NO-LOCK NO-ERROR.
              
    CREATE tt_pedven.
    ASSIGN tt_pedven.nome-abrev           = emitente.nome-abrev
           tt_pedven.nr-pedcli            = tt_pedbatch.nr-pedcli
           tt_pedven.nr-pedido            = IF avail buf_pedido 
                                            THEN 
                                                 buf_pedido.nr-pedido + 1
                                            ELSE 1   
           tt_pedven.cod-estabel          = para-ped.estab-pad
           tt_pedven.dt-emissao           = TODAY
           tt_pedven.dt-implant           = tt_pedven.dt-emissao
           tt_pedven.dt-entrega           = tt_pedven.dt-emissao
           tt_pedven.dt-entorig           = tt_pedven.dt-entrega
           tt_pedven.nat-operacao         = emitente.nat-operacao
           tt_pedven.cod-cond-pag         = emitente.cod-cond-pag
           tt_pedven.nr-tabpre            = emitente.nr-tabpre
           tt_pedven.nr-tab-finan         = IF cond-pagto.nr-tab-finan <> 0
                                            THEN cond-pagto.nr-tab-finan
                                            ELSE para-ped.tab-fin-pad
           tt_pedven.nr-ind-finan         = IF cond-pagto.nr-ind-finan <> 0 
                                            THEN cond-pagto.nr-ind-finan
                                            ELSE para-ped.nr-ind-finan
           tt_pedven.cod-priori           = 99
           tt_pedven.local-entreg         = IF AVAIL(loc-entr) 
                                            THEN loc-entr.endereco 
                                            ELSE emitente.endereco
           tt_pedven.bairro               = IF AVAIL(loc-entr)
                                            THEN loc-entr.bairro
                                            ELSE emitente.bairro
           tt_pedven.cidade               = IF AVAIL(loc-entr)
                                            THEN loc-entr.cidade
                                            ELSE emitente.cidade
           tt_pedven.estado               = IF AVAIL(loc-entr)
                                            THEN loc-entr.estado
                                            ELSE emitente.estado
           tt_pedven.cep                  = IF AVAIL(loc-entr)
                                            THEN loc-entr.cep
                                            ELSE emitente.cep
           tt_pedven.pais                 = IF AVAIL(loc-entr)
                                            THEN loc-entr.pais
                                            ELSE emitente.pais.


                     
    ASSIGN tt_pedven.cgc                  = IF AVAIL(loc-entr)
                                            THEN Loc-entr.cgc
                                            ELSE emitente.cgc 
           tt_pedven.ins-estadual         = IF AVAIL(loc-entr)
                                            THEN Loc-entr.ins-estadual
                                            ELSE emitente.ins-estadual
           tt_pedven.perc-desco2          = tb-preco.desconto        
           tt_pedven.cidade-cif           = "SBC"          
           tt_pedven.cod-portador         = emitente.portador
           tt_pedven.modalidade           = emitente.modalidade
           tt_pedven.cod-mensagem         = natur-oper.cod-mensagem

           tt_pedven.user-impl            = wusr           
           tt_pedven.dt-userimp           = TODAY
           tt_pedven.ind-aprov            = False       /* Padrao Dicionario */
           tt_pedven.quem-aprovou         = "Sistema"
           tt_pedven.dt-apr-cred          = TODAY          


           tt_pedven.nome-transp          = IF AVAIL(transporte)
                                            THEN transporte.nome-abrev
                                            ELSE "Desconhecido"         
           tt_pedven.tp-preco             = para-ped.tp-preco /* tab preco */

           tt_pedven.ind-fat-par          = True            /* Fat.Parcial ? */
           tt_pedven.mo-codigo            = tt_pedven.mo-codigo          
           tt_pedven.ind-lib-nota         = False            /* Libera NF */
                     
           tt_pedven.no-ab-reppri         = IF AVAIL(repres)
                                            THEN repres.nome-abrev
                                            ELSE ""
           tt_pedven.ind-antecip          = False            /* Antecipacao */

           tt_pedven.user-aprov           = "Sistema"

           tt_pedven.dsp-pre-fat          = True             /* Pre-fatur */
                     
           tt_pedven.cod-emitente         = emitente.cod-emitente
           tt_pedven.inc-desc-txt         = False
           tt_pedven.cod-gr-cli           = emitente.cod-gr-cli
           tt_pedven.esp-ped              = emitente.esp-pd-ven  /* Ab/Fechd*/
           tt_pedven.atendido             = False     /* Atendido Total ? */
           tt_pedven.permissao            = "*".
    ASSIGN tt_pedfase.dt-criacao          = TODAY.
              
    ASSIGN tt_pedven.observacoes           
                    = SUBSTRING(tt_pedbatch.observacao,1,26).



    /*------- tt_pedrep ---------------------------------------*/
    IF AVAIL(repres)
    THEN DO:
      CREATE tt_pedrep.
      ASSIGN tt_pedrep.nr-pedido   = tt_pedven.nr-pedido
             tt_pedrep.nome-ab-rep = repres.nome-abrev
             tt_pedrep.ind-repbase = True
             tt_pedrep.perc-comis  = repres.comis-direta
             tt_pedrep.comis-emis  = repres.comis-emis.
             
      /*...........................inicio macete para passar campos.......*/
      ASSIGN tt_pedrep.int-1  = repres.cod-rep
             tt_pedrep.char-1 = tt_pedbatch.nr-pedcli.
      /*...........................fim de macete para passar campos.......*/


      END.
    END.
  RETURN.
END.
 
PROCEDURE PI_ImpressaoFinal.
  ASSIGN wrel = tt-param.warqs2.
  OUTPUT TO VALUE(wrel) PAGE-SIZE 60.
  VIEW FRAME f-cabec.
           
  FOR
  EACH  tt_pedfase
  NO-LOCK
  BREAK BY tt_pedfase.situacao:
    FIND tt_pedbatch
    WHERE RECID(tt_pedbatch) = tt_pedfase.rec-pedido
    NO-LOCK NO-ERROR.
              
    DISPLAY 
    tt_pedbatch.nr-pedcli     COLUMN-LABEL "Pedido VW"  FORMAT "x(12)"
    tt_pedbatch.linha         COLUMN-LABEL "Linha"      FORMAT ">>>>9"
    tt_pedbatch.it-codigo                               FORMAT "x(16)"
    tt_pedbatch.qt-pedida     COLUMN-LABEL "Quantidade" FORMAT ">>>>,>>9.99"
    tt_pedbatch.cod-cli-vw    COLUMN-LABEL "Cliente VW"
    tt_pedbatch.cod-emitente  COLUMN-LABEL "Emitente"
    tt_pedfase.observacao      COLUMN-LABEL "Observacao"
    WITH WIDTH 132 FRAME f_impressao_final DOWN STREAM-IO.
    DOWN WITH FRAME f_impressao_final.
    END.
           
  OUTPUT CLOSE.
  /*UNIX SILENT chown VALUE(wusr) VALUE(wrel).*/
  RETURN.        
END.

 
PROCEDURE PI_ArquivoBatch.
  ASSIGN wrel = tt-param.warqbat.
  OUTPUT TO VALUE(wrel) PAGE-SIZE 0.


  DO WITH FRAME f-lh:
    FORM
      WITH DOWN NO-LABEL ROW 01
      WIDTH 473
      NO-BOX

      FRAME f-lh.

    /*.......................................inicio tt_pedven...*/
    FOR EACH tt_pedven
    USE-INDEX ch-pedido
    WITH FRAME f-lh:

      /*........................inicio reg pedido de venda........*/
      ASSIGN wreg = "01"
          +  STRING(tt_pedven.nome-abrev,"x(12)")
          +  STRING(tt_pedven.nr-pedcli,"x(12)")
          +  STRING(tt_pedven.cod-emitente,"999999999")
          +  STRING(tt_pedven.cgc,"x(19)")
          +  FILL(" ",12)  /* ped rep */
          
          +  STRING(DAY(tt_pedven.dt-emissao),"99")
          +  STRING(MONTH(tt_pedven.dt-emissao),"99")
          +  STRING(YEAR(tt_pedven.dt-emissao),"9999")


          +  FILL(" ",8)   /* dt minima */ 
          +  FILL(" ",8)   /* dt limite */
          +  FILL(" ",2)   /* tp pedido */

          +  STRING(tt_pedven.cod-cond-pag,"999")
          +  STRING(tt_pedven.nr-tabpre,"x(8)")

          +  STRING(tt_pedven.nr-tab-finan,"999")
          +  STRING(tt_pedven.nr-ind-finan,"99")
          +  STRING(tt_pedven.tp-preco,"99")
          +  STRING(tt_pedven.mo-codigo,"99")
          
          +  FILL(" ",5)     /* desconto 1 */
          +  FILL(" ",5)     /* desconto 2 */ 

          +  STRING(tt_pedven.cod-priori,"99")

          +  FILL(" ",1)     /* dest merc */
          +  FILL(" ",12)    /* rota      */
          
          +  FILL(" ",12)    /* nome-transp */

          +  STRING(tt_pedven.no-ab-reppri,"x(12)")
          +  STRING(tt_pedven.cidade-cif,"x(25)")
          +  STRING(tt_pedven.cod-mensagem,"999")
          
          +  FILL(" ",12)    /* cod entrega */
          +  FILL(" ",25)    /* contato   */
          +  FILL(" ",154)   /* brancos   */
          
          +  STRING(tt_pedven.nat-operacao,"x(6)")
          +  STRING(tt_pedven.cod-portador,"99999")
          +  STRING(tt_pedven.modalidade,"9")
          
          +  FILL(" ",12)    /* nome transp redespa */
          +  FILL(" ",50)    /* brancos   */
          +  FILL(" ",4)     /* brancos   */
          
          +  STRING(tt_pedven.cod-estabel,"x(3)")
          
          +  STRING(DAY(tt_pedven.dt-entrega),"99")
          +  STRING(MONTH(tt_pedven.dt-entrega),"99")
          +  STRING(YEAR(tt_pedven.dt-entrega),"9999")

          +  STRING(tt_pedven.esp-ped,"99")
          .
      PUT wreg SKIP.
      /*........................fim de reg pedido de venda........*/



      /*........................inicio reg obs do pedido de venda.*/
      ASSIGN wreg = "04"
          +  STRING(tt_pedven.observacoes,"x(80)")
          .
      PUT wreg SKIP.
      /*........................fim de reg obs do pedido de venda.*/



      /*.......................................inicio tt_peditem...*/
      FOR
      EACH  tt_peditem
      WHERE tt_peditem.nome-abrev = tt_pedven.nome-abrev
      AND   tt_peditem.nr-pedcli  = tt_pedven.nr-pedcli
      WITH FRAME f-lh:

        /*........................inicio reg itens do pedido........*/
        ASSIGN wreg = "07"
            +  STRING(tt_peditem.nome-abrev,"x(12)")
            +  STRING(tt_peditem.nr-pedcli,"x(12)")
            +  STRING(tt_peditem.nr-sequencia,"99999")
            +  STRING(tt_peditem.it-codigo,"x(16)")

            +  FILL(" ",8)  /* ordem compra */
            +  FILL(" ",2)  /* parcela      */
          
            +  STRING(DAY(tt_peditem.dt-entorig),"99")
            +  STRING(MONTH(tt_peditem.dt-entorig),"99")
            +  STRING(YEAR(tt_peditem.dt-entorig),"9999")

            + SUBSTRING(STRING(tt_peditem.qt-pedida,"9999999.9999"),01,7)
            + SUBSTRING(STRING(tt_peditem.qt-pedida,"9999999.9999"),09,4)

            + SUBSTRING(STRING(tt_peditem.vl-preuni,"999999999.99999"),01,9)
            + SUBSTRING(STRING(tt_peditem.vl-preuni,"999999999.99999"),11,5)

            +  FILL(" ",50)   /* brancos   */ 
            +  FILL(" ",4)    /* per mim fat parc*/
            +  FILL(" ",8)    /* referencia */


            +  STRING(tt_peditem.nat-operacao,"x(6)")

            + SUBSTRING(STRING(tt_peditem.per-des-icms,"999.999"),01,3)
            + SUBSTRING(STRING(tt_peditem.per-des-icms,"999.999"),05,3)
           /* alterado conf. J. Luiz, veja anota��o no cabe�alho - 05/03/08*/
            + STRING(tt_peditem.wicmsretido,"9") /* icms retido */
           /* antes da altera�ao do dia 05/03/08
            +  FILL(" ",1)    /* icms retido fonte */ */
            +  FILL(" ",1)    /* unid fat   */


            +  STRING(DAY(tt_peditem.dt-entrega),"99")
            +  STRING(MONTH(tt_peditem.dt-entrega),"99")
            +  STRING(YEAR(tt_peditem.dt-entrega),"9999")

            +  FILL(" ",2)    /* ind comp prod conf */
            +  FILL(" ",12)   /* cod entrega item  */

            +  STRING(tt_peditem.nr-tabpre,"x(8)")

            +  FILL(" ",7)   /* per des tab preco  */ 
            +  FILL(" ",50)  /* per des inf        */
            +  FILL(" ",11)  /* vl desc inf        */
            +  FILL(" ",1)   /* concede bonif qtd  */
            +  FILL(" ",5)   /* per bonif          */ 
            +  FILL(" ",5)   /* per desc per       */
            +  FILL(" ",5)   /* per desc prazo     */
            +  FILL(" ",11)  /* vl desc bonif      */
            +  FILL(" ",11)  /* qtd bonif          */
            +  FILL(" ",5)   /* seq bonif          */
            +  FILL(" ",70)  /* desco 1 a 5        */
            +  FILL(" ",2)   /* unid med fat       */
            +  FILL(" ",17)  /* conta aplic        */
            +  FILL(" ",12)  /* Vl custo contab    */
            +  FILL(" ",10)  /* branco             */

            +  STRING(tt_peditem.observacao,"x(80)")
            .
        PUT wreg SKIP.
        /*........................fim de reg itens do pedido........*/



        /*.......................................inicio tt_pedent...*/
        FOR 
        EACH  tt_pedent
        WHERE tt_pedent.nome-abrev   = tt_peditem.nome-abrev
        AND   tt_pedent.nr-pedcli    = tt_peditem.nr-pedcli
        AND   tt_pedent.nr-sequencia = tt_peditem.nr-sequencia
        AND   tt_pedent.it-codigo    = tt_peditem.it-codigo
        WITH FRAME f-lh:

          /*........................inicio reg entrega do item........*/
          ASSIGN wreg = "08"
              +  STRING(tt_pedent.nome-abrev,"x(12)")
              +  STRING(tt_pedent.nr-pedcli,"x(12)")
              +  STRING(tt_pedent.nr-sequencia,"99999")

              +  FILL(" ",5)  /* seq entrega  */

              +  STRING(tt_pedent.it-codigo,"x(16)")

              +  FILL(" ",8)  /* referencia   */

              +  FILL(" ",1)  /* tipo entrega */
          
              +  STRING(DAY(tt_pedent.dt-entrega),"99")
              +  STRING(MONTH(tt_pedent.dt-entrega),"99")
              +  STRING(YEAR(tt_pedent.dt-entrega),"9999")

              +  FILL(" ",6)  /* hora entrega */

              +  STRING(DAY(tt_pedent.dt-entorig),"99")
              +  STRING(MONTH(tt_pedent.dt-entorig),"99")
              +  STRING(YEAR(tt_pedent.dt-entorig),"9999")

              +  FILL(" ",6)  /* hora entrega orig */

              + SUBSTRING(STRING(tt_pedent.qt-pedida,"9999999.9999"),01,7)
              + SUBSTRING(STRING(tt_pedent.qt-pedida,"9999999.9999"),09,4)

              +  FILL(" ",1)  /* tipo atend        */
              .
          PUT wreg SKIP.
          /*........................fim de reg entrega do item........*/

          END.
        /*.......................................fim de tt_pedent...*/

        END.
      /*.......................................fim de tt_peditem...*/


      /*.......................................inicio tt_pedrep...*/
      FOR 
      EACH  tt_pedrep
      WHERE tt_pedrep.nr-pedido = tt_pedven.nr-pedido
      WITH FRAME f-lh:

        /*........................inicio reg representante..........*/
        ASSIGN wreg = "09"
            +  STRING(tt_pedrep.char-1,"x(12)")   /* macete passa ped-cli */
            +  STRING(tt_pedrep.int-1,"99999")

            + SUBSTRING(STRING(tt_pedrep.perc-comis,"999.999"),01,3)
            + SUBSTRING(STRING(tt_pedrep.perc-comis,"999.999"),05,2)

            +  STRING(tt_pedrep.comis-emis,"999")
            + "00"     /* decimais da comis-emis */
         
            + "0"      /* representante principal */
            .
        PUT wreg SKIP.
        /*........................fim de reg representante..........*/

        END.
      /*.......................................fim de tt_pedrep...*/


      END.
    /*.......................................fim de tt_pedven...*/


    END.
           
  /*...............................inicio grava ultimo reg em branco ...*/
  ASSIGN wreg = "99".
  PUT wreg SKIP.

  ASSIGN wreg = "".
  PUT wreg.
  /*...............................fim de grava ultimo reg em branco ...*/


  OUTPUT CLOSE.
  /*UNIX SILENT chown VALUE(wusr) VALUE(wrel).*/
  RETURN.  
END.


/*----------------------------------------------------FIM PROCEDURES INTERNAS*/

/*-------------------------------------------------------------fim de novo2--*/

MESSAGE "O ARQUIVO " tt-param.warqse SKIP 
        "FOI CONVERTIDO PARA " tt-param.warqbat SKIP(2)                   
        "VIDE CRITICA INICIAL NO ARQUIVO: " SKIP
        tt-param.warqs1 SKIP(2)
        "VIDE CRITICA DA CONVERSAO NO ARQUIVO: " SKIP
        tt-param.warqs2 SKIP(2)
        VIEW-AS ALERT-BOX.
QUIT.
/*---------------------------------------------------------------FIM PROGRAMA*/



 




