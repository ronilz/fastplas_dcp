&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
/* convertido por Val�ria */

{include/i-prgvrs.i LI1030RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wfami            AS CHAR FORMAT "X(08)"
    FIELD wfamf            AS CHAR FORMAT "X(08)"
    FIELD wite             AS CHAR FORMAT "X(16)"
    FIELD wdsn             AS CHAR FORMAT "X(10)"
    FIELD wdayi            AS INTE FORMAT "99"
    FIELD wdtf             AS DATE FORMAT "99/99/9999"
    FIELD wplanej          AS CHAR FORMAT "X(12)"
    FIELD wdopsn           AS CHAR FORMAT "X(03)"
    FIELD wturnoi          AS CHAR FORMAT "X(12)"
    FIELD wturnof          AS CHAR FORMAT "X(12)"
    FIELD westsn1          AS LOGICAL
    FIELD westsn2          AS LOGICAL
    FIELD westsn3          AS LOGICAL
    FIELD westsn4          AS LOGICAL
    FIELD westsn5          AS LOGICAL
    FIELD westsn6          AS LOGICAL
    FIELD westsn7          AS LOGICAL
    FIELD westsn8          AS LOGICAL
    FIELD westsn9          AS LOGICAL
    FIELD wnrlhi           AS INTE FORMAT ">>9"
    FIELD wnrlhf           AS INTE FORMAT ">>9"
    FIELD wnr-linha-ini    AS INTE FORMAT ">>9"
    FIELD wnr-linha-fim    AS INTE FORMAT ">>9"
    FIELD westab           AS CHAR FORMAT "x(03)".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat NO-LABELS.

/* VARIAVEIS BENE */
/***********************************************************************
**
** li1030.p   - Exibe Prevto/Envdo/Przdo Horizontal         
**
**              02/10/90  05:40  -  BENE  Programa novo.
**
**              20/04/1999 - Bene - Alteracao para operar no Magnus.
**
***********************************************************************/

DEFINE VARIABLE wdmatu      AS CHARACTER FORMAT "99999999".
DEFINE VARIABLE wddmmaa     AS CHARACTER FORMAT "x(8)".
DEFINE VARIABLE waamm       AS CHARACTER FORMAT "999999".
DEFINE VARIABLE wday        AS INTEGER   FORMAT "99".
DEFINE VARIABLE wdayi       AS INTEGER   FORMAT "99".
DEFINE VARIABLE wdayf       AS INTEGER   FORMAT "99".

DEFINE VARIABLE wdti        AS DATE    FORMAT "99/99/9999".
DEFINE VARIABLE wdtf        AS DATE    FORMAT "99/99/9999".
DEFINE VARIABLE wquant       LIKE movto-estoq.quantidade.
DEFINE VARIABLE wqestorno    LIKE movto-estoq.quantidade.    
DEFINE VARIABLE wqtprev      LIKE ord-prod.qt-ordem.

DEFINE VARIABLE wprvto2     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wprvto3     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wprvto4     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenvdo2     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenvdo3     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wenvdo4     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wprzdo2     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wprzdo3     AS INTEGER FORMAT "zzzzzz9-".
DEFINE VARIABLE wprzdo4     AS INTEGER FORMAT "zzzzzz9-".


DEFINE VARIABLE wjai        AS INTEGER   FORMAT "9" INITIAL 0.
DEFINE VARIABLE wresto      AS INTEGER.
DEFINE VARIABLE wctd        AS INTEGER.
DEFINE VARIABLE wdet        AS CHARACTER FORMAT "x(100)".
DEFINE VARIABLE wmsg        AS CHARACTER FORMAT "x(100)".
DEFINE VARIABLE wstart      AS CHARACTER FORMAT "x(01)".

DEFINE VARIABLE wdesc       AS CHARACTER FORMAT "x(44)".

DEFINE VARIABLE wrel        AS CHARACTER FORMAT "x(29)".


DEFINE VARIABLE westletra AS CHAR FORMAT "!"       EXTENT 9
       INITIAL ["N","L","A","E","S","R","I","F","T"].

/***--------------------------------------------------inicio-criado p/EMS ***/
DEFINE VARIABLE westnuems AS CHAR FORMAT "x(01)"   EXTENT 9
       INITIAL ["1","0","2","3","4","5","6","7","8"].
/***--------------------------------------------------fim de criado p/EMS ***/


DEFINE VARIABLE westsn    AS LOGI FORMAT "Sim/Nao" EXTENT 9       
       INITIAL [yes,yes,yes,yes,yes,yes,yes,yes,yes].
DEFINE VARIABLE westlista AS CHAR.
DEFINE VARIABLE westpos   AS INTE.

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find mgcad.empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PROCEDIMENTOS ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Previsto/Enviado/Produzido - Horizontal".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).*/
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
/*.................................................fim de entrada padrao..*/

wdmatu = STRING(DAY(TODAY),"99") +  STRING(MONTH(TODAY),"99") + SUBSTRING(STRING(INTEGER(YEAR(TODAY)),"9999"),1,4).
wddmmaa = wdmatu.

wdet = FILL(" ",100).

DEFINE VARIABLE wctdrepeat  AS INTE.
ASSIGN wctdrepeat = 0.
REPEAT:
  ASSIGN wctdrepeat = wctdrepeat + 1.
ASSIGN
    wjai    = 0
    wprvto2 = 0
    wprvto3 = 0
    wprvto4 = 0
    wenvdo2 = 0
    wenvdo3 = 0
    wenvdo4 = 0
    wprzdo2 = 0
    wprzdo3 = 0
    wprzdo4 = 0.

  wmsg = "fam".
 
/*     IF   (tt-param.wdtf GE 11/01/2000) */
/*     THEN DO:                           */
/*       ASSIGN tt-param.wdopsn = "Sim".  */
/*       END.                             */
/*                                        */
    ASSIGN wdti = DATE(MONTH(tt-param.wdtf),tt-param.wdayi,YEAR(tt-param.wdtf)).
    ASSIGN wddmmaa = STRING(DAY(tt-param.wdtf),"99")
                   + STRING(MONTH(tt-param.wdtf),"99")
                   + STRING(YEAR(tt-param.wdtf),"9999").

    IF SUBSTRING(wddmmaa,1,2) LT "01"
    OR SUBSTRING(wddmmaa,1,2) GT "31"
    OR SUBSTRING(wddmmaa,3,2) LT "01"
    OR SUBSTRING(wddmmaa,3,2) GT "12"
    OR SUBSTRING(wddmmaa,5,4) LT "1990" THEN DO:
      MESSAGE "data" wddmmaa "invalida".
      BELL.
      NEXT.
      END.
    wdayf = INTEGER(SUBSTRING(wddmmaa,1,2)).
    IF tt-param.wdayi GT wdayf THEN DO:
      MESSAGE "dia da data deve ser maior ou igual dia inicial".
      BELL.
      NEXT.
      END.
    waamm = SUBSTRING(wddmmaa,5,4) + SUBSTRING(wddmmaa,3,2).
    wmsg = wmsg + " " + STRING(wddmmaa,"99/99/9999").
    LEAVE.
    END.
 
ASSIGN westsn[1] = tt-param.westsn1
       westsn[2] = tt-param.westsn2
       westsn[3] = tt-param.westsn3
       westsn[4] = tt-param.westsn4
       westsn[5] = tt-param.westsn5
       westsn[6] = tt-param.westsn6
       westsn[7] = tt-param.westsn7
       westsn[8] = tt-param.westsn8
       westsn[9] = tt-param.westsn9.


  ASSIGN westlista = "".
  DO westpos = 1 TO 9:
    IF westsn[westpos] THEN DO:
      IF LENGTH(westlista) GT 0 THEN ASSIGN westlista = westlista + ",".

      /*** ---------------inicio inibido p EMS
      ASSIGN westlista = westlista + westletra[westpos].
                          ------------------------fim de inibido p EMS */

      /*** ---------------------------------------inicio criado p EMS */
      ASSIGN westlista = westlista + STRING(westnuems[westpos]).
      /*** ---------------------------------------fim de criado p EMS */

      END.
    END.


  DISPLAY "Estabel. " tt-param.westab "/Familia:"  tt-param.wfami "-" tt-param.wfamf "/Item:" tt-param.wite "/Total:" tt-param.wdsn
          "/Data:" tt-param.wday " a " tt-param.wdtf
          SKIP
          "Plajenador: " tt-param.wplanej " Detalhe ordem:  " tt-param.wdopsn " Turno:  " tt-param.wturnoi "-" tt-param.wturnof
          SKIP
          "Estado: " tt-param.westsn1  tt-param.westsn2  tt-param.westsn3  tt-param.westsn4  
                     tt-param.westsn5  tt-param.westsn6  tt-param.westsn7  tt-param.westsn8  
                     tt-param.westsn9 " L.Prod: " tt-param.wnrlhi "-" tt-param.wnrlhf
                     "L.Prod.Estab." tt-param.wnr-linha-ini "-"  tt-param.wnr-linha-fim
          SKIP 
          WITH FRAME f-param PAGE-TOP WIDTH 150 NO-LABELS.
  DISPLAY 
 "                                                    PREVISTO  ENVIADO REALIZADO LINHA PROD.ESTAB"
  WITH FRAME f-cabec1 NO-LABELS PAGE-TOP WIDTH 150.

      
    /*.......................INICIO TRATAMENTO DE FAMILIA................*/
    FOR EACH fam-comerc
       WHERE fam-comerc.fm-cod-com GE tt-param.wfami
       AND   fam-comerc.fm-cod-com LE tt-param.wfamf
       NO-LOCK:
       run pi-acompanhar in h-acomp ("Fam�lia: " + STRING(fam-comerc.fm-cod-com)).
      /*.....................INICIO TRATAMENTO DE ITEM..................*/
      FOR EACH item
         WHERE item.fm-cod-com = fam-comerc.fm-cod-com
         AND   item.it-codigo  MATCHES(tt-param.wite)        
         AND   item.cod-obsoleto NE 3
         NO-LOCK:
  /*020118 - LINHA PROD. SOL FABIO - PCP*/
  /* 22/02/21 - INSERIDO SELE��O POR ESTABELECIMENTO - CONF. SOL. FABIO */
      FIND FIRST  item-uni-estab WHERE item-uni-estab.it-codigo =  ITEM.it-codigo
                           AND   item-uni-estab.cod-estabel = tt-param.westab
                           AND   (item-uni-estab.nr-linha GE  wnr-linha-ini
                           AND item-uni-estab.nr-linha LE  wnr-linha-fim)
           NO-LOCK NO-ERROR.                                                          
    
      IF NOT AVAILABLE item-uni-estab  THEN NEXT.
     
       
        /*.....................................inicio PREVISTO NA ORDEM.*/
        IF  tt-param.wdopsn = "Nao"
        THEN
        FOR EACH ord-prod                         
           WHERE ord-prod.it-codigo    =  item.it-codigo
           AND   ord-prod.dt-termino   GE wdti
           AND   ord-prod.dt-termino   LE tt-param.wdtf
           AND   ord-prod.cd-planejado MATCHES(tt-param.wplanej)
           AND   ord-prod.nr-linha     GE tt-param.wnrlhi    
           AND   ord-prod.nr-linha     LE tt-param.wnrlhf     
           AND   LOOKUP(STRING(ord-prod.estado),westlista) GT 0         
           NO-LOCK:
          

          ASSIGN wqtprev = ord-prod.qt-ordem.
          IF ord-prod.estado = 8 THEN DO:
            ASSIGN wqtprev = ord-prod.qt-produzida.
            END.
          ASSIGN wprvto2 = wprvto2 + wqtprev
                 wprvto3 = wprvto3 + wqtprev
                 wprvto4 = wprvto4 + wqtprev.

          END.
        /*.....................................fim de PREVISTO NA ORDEM.*/


        /*.....................................inicio PREVISTO NO DETALHE*/
        IF tt-param.wdopsn = "Sim"
        THEN
        FOR EACH ord-prod                         
           WHERE ord-prod.it-codigo    =  item.it-codigo
           AND   ord-prod.dt-inicio    LE tt-param.wdtf
           AND   ord-prod.dt-termino   GE wdti
           AND   ord-prod.cd-planejado MATCHES(tt-param.wplanej)
           AND   ord-prod.nr-linha     GE tt-param.wnrlhi
           AND   ord-prod.nr-linha     LE tt-param.wnrlhf
           AND   LOOKUP(STRING(ord-prod.estado),westlista) GT 0         
           NO-LOCK:
          

          FOR EACH lo-detord
             WHERE lo-detord.it-codigo = item.it-codigo
             AND   lo-detord.data     GE wdti  
             AND   lo-detord.data     LE tt-param.wdtf 
             AND   lo-detord.turno    GE tt-param.wturnoi
             AND   lo-detord.turno    LE tt-param.wturnof
             AND   lo-detord.nr-ord-produ = ord-prod.nr-ord-produ
             NO-LOCK:

            ASSIGN wqtprev = lo-detord.qtde.    
            ASSIGN wprvto2 = wprvto2 + wqtprev 
                   wprvto3 = wprvto3 + wqtprev
                   wprvto4 = wprvto4 + wqtprev.
            END.

          END.
        /*.....................................fim de PREVISTO NO DETALHE*/

       /* 22/02/21 - INSERIDO SELE��O POR ESTABELECIMENTO - CONF. SOL. FABIO */
        /*...........INICIO MANTAGEM DO ENVIADO E PRODUZIDO NO PERIODO....*/
        FOR EACH movto-estoq
           WHERE movto-estoq.cod-estabel = tt-param.westab
           AND   movto-estoq.it-codigo   = item.it-codigo
           AND   movto-estoq.dt-trans   GE wdti
           AND   movto-estoq.dt-trans   LE tt-param.wdtf
           AND ((movto-estoq.esp-docto    =  1 OR
                 movto-estoq.esp-docto    = 25 OR
                 movto-estoq.esp-docto    =  8)
                                 OR
                (movto-estoq.esp-docto    = 28 AND
                               movto-estoq.serie          GE "0"      AND
                               movto-estoq.serie          LE "999")
                                 OR
                (movto-estoq.esp-docto    = 31 AND
                               movto-estoq.serie          GE "0"      AND
                               movto-estoq.serie          LE "999"))
           NO-LOCK:
          
        IF movto-estoq.nr-ord-produ GT 0
          THEN DO: 
            FIND ord-prod
            WHERE ord-prod.nr-ord-prod = movto-estoq.nr-ord-produ
            AND   ord-prod.nr-linha    GE tt-param.wnrlhi    
            AND   ord-prod.nr-linha    LE tt-param.wnrlhf     
            NO-LOCK
            NO-ERROR.
            IF NOT AVAILABLE ord-prod
            OR LOOKUP(STRING(ord-prod.estado),westlista) = 0 THEN NEXT.
            END.       
          IF  (movto-estoq.nr-ord-produ = 0)
          AND (movto-estoq.esp-docto    = 28)
          AND (tt-param.wnrlhi     NE 0   OR
               tt-param.wnrlhf     NE 999 OR
               tt-param.westsn1   = no  OR
               tt-param.westsn2   = no  OR
               tt-param.westsn3   = no  OR
               tt-param.westsn4   = no  OR
               tt-param.westsn5   = no  OR
               tt-param.westsn6   = no  OR
               tt-param.westsn7   = no  OR
               tt-param.westsn8   = no  OR
               tt-param.westsn9   = no)   
          THEN NEXT.

          ASSIGN wquant = movto-estoq.quantidade.
          IF movto-estoq.tipo-trans = 2  THEN DO:
            ASSIGN wquant = movto-estoq.quantidade * -1.
            END.

          ASSIGN wenvdo2 = wenvdo2 + wquant                           
                 wenvdo3 = wenvdo3 + wquant                           
                 wenvdo4 = wenvdo4 + wquant.                          
          ASSIGN wprzdo2 = wprzdo2 + wquant                           
                 wprzdo3 = wprzdo3 + wquant                          
                 wprzdo4 = wprzdo4 + wquant.                           

          IF movto-estoq.esp-docto = 25
          OR movto-estoq.esp-docto = 28
          OR movto-estoq.esp-docto = 31
          THEN DO:
            ASSIGN wqestorno = movto-estoq.quantidade.
            IF (movto-estoq.esp-docto  = 25 OR
                movto-estoq.esp-docto  = 31)
            AND movto-estoq.tipo-trans = 1 
            THEN DO:
              ASSIGN wqestorno = wqestorno * -1.
              END.

            ASSIGN wprzdo2 = wprzdo2 + wqestorno                        
                   wprzdo3 = wprzdo3 + wqestorno
                   wprzdo4 = wprzdo4 + wqestorno.                       
            END.

          END.
        /*..........FIM DA MONTAGEM DO ENVIADO E PRODUZIDO NO PERIODO...*/


        IF  tt-param.wdsn  = "Detalhado"
        THEN DO:
          IF wprvto2 <> 0
          OR wenvdo2 <> 0
          OR wprzdo2 <> 0 THEN DO:
            ASSIGN
            wdet = STRING(item.it-codigo,"x(13)") + "    "
                  + STRING(item.descricao-1,"x(26)")
                  + FILL(" ",9)
                  + " "
                  + STRING(wprvto2,"zzzzzz9-") 
                  + " "
                  + STRING(wenvdo2,"zzzzzz9-") 
                  + " "
                  + STRING(wprzdo2,"zzzzzz9-")
                 /*02/01/18 - SOL. FABIO - LINHA DE PRODUCAO ITEM-UNI-ESTAB.NR-LINHA */
               + FILL(" ",2).
            IF AVAILABLE item-uni-estab  THEN
                ASSIGN wdet = wdet + STRING(item-uni-estab.nr-linha,">>999").

            DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.

            ASSIGN wprvto2 = 0
                   wenvdo2 = 0
                   wprzdo2 = 0.
            END.
          END.

        END.

       /*.....................FIM DE TRATAMENTO DE ITEM...................*/
  

      IF wprvto3 <> 0
      OR wenvdo3 <> 0
      OR wprzdo3 <> 0 
       THEN DO:
        IF tt-param.wdsn = "Detalhado" THEN DO:
          ASSIGN wdet = "".
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.
        ASSIGN
        wdet = "TOTAL FAMILIA "
              + STRING(fam-comerc.fm-cod-com,"x(8)") + " "
              + STRING(fam-comerc.descricao,"x(20)")
              + FILL(" ",9)
              + " " 
              + STRING(wprvto3,"zzzzzz9-") 
              + " "
              + STRING(wenvdo3,"zzzzzz9-") 
              + " "
              + STRING(wprzdo3,"zzzzzz9-")
              /*02/01/18 - SOL. FABIO - LINHA DE PRODUCAO ITEM-UNI-ESTAB.NR-LINHA */
               + FILL(" ",2).
           IF AVAILABLE item-uni-estab  THEN
                ASSIGN wdet = wdet + STRING(item-uni-estab.nr-linha,">>999").
              

        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.

        IF tt-param.wdsn = "Detalhado" THEN DO:
          ASSIGN wdet = FILL("_",100).
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          ASSIGN wdet = "".
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.

        ASSIGN wprvto3 = 0 
               wenvdo3 = 0
               wprzdo3 = 0.
        END.

      END.
          
     /*.......................FIM DE TRATAMENTO DE FAMILIA................*/


    IF wprvto4 <> 0
    OR wenvdo4 <> 0
    OR wprzdo4 <> 0 
     THEN DO:
      IF tt-param.wdsn = "Resumido" THEN DO:
        ASSIGN wdet = " ".
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        END.

      ASSIGN
      wdet = "TOTAL DA CONSULTA" + FILL(".",26)
            + FILL(" ",9)
            + " "
            + STRING(wprvto4,"zzzzzz9-") 
            + " "
            + STRING(wenvdo4,"zzzzzz9-") 
            + " "
            + STRING(wprzdo4,"zzzzzz9-")
           /*02/01/18 - SOL. FABIO - LINHA DE PRODUCAO ITEM-UNI-ESTAB.NR-LINHA */
               + FILL(" ",2).
             /*  + STRING(item-uni-estab.nr-linha,">>999"). */
      wdesc = FILL(" ",44).

      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.

      ASSIGN wprvto4 = 0
             wenvdo4 = 0
             wprzdo4 = 0.
      END.


END PROCEDURE.




