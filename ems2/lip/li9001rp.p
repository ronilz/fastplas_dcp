&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI9001RP 0.00.04.002}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD warq-importado   AS CHAR FORMAT "X(100)"
    FIELD wpasta           AS INTE FORMAT "99"
    FIELD wanomes          AS CHAR FORMAT "x(06)".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS valeria */
/* Parameters Definitions ---                                           */
DEFINE VARIABLE procname AS CHAR NO-UNDO.
DEFINE VARIABLE okbt     AS LOGICAL INITIAL TRUE.

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE wseq            LIKE lo-matplano.seq-it.
DEFINE VARIABLE wcriaplano      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wdatapesq       LIKE recebimento.data-nota.
DEFINE VARIABLE wgera-novo       AS LOGICAL FORMAT "Sim/N�o".
/* outras variaveis */

/* Variaveis para importa��o */
def var v-chr-excel-application as office.iface.excel.ExcelWrapper no-undo. 
def var v-chr-work-book as office.iface.excel.Workbook no-undo. 
def var v-chr-work-sheet as office.iface.excel.WorkSheet no-undo. 
def var v-chr-range as character no-undo. 
def var v-int-line as integer no-undo initial 1. 
def var v-int-ultimalinha as integer FORMAT "9999999999" NO-UNDO.

def var i-pasta-select as dec. 

/* variaveis da planilha */
DEF VAR e-it-codigo  AS CHAR FORMAT "X(16)".
DEF VAR e-crit   AS INTEGER FORMAT  ">>,>>>,>>9".
DEF VAR e-nec1  AS INTEGER FORMAT  ">>,>>>,>>9".
DEF VAR e-nec2  AS INTEGER FORMAT  ">>,>>>,>>9".
DEF VAR e-nec3  AS INTEGER FORMAT  ">>,>>>,>>9".
DEF VAR e-obs   AS CHAR FORMAT "X(40)".

DEF TEMP-TABLE tt-dados-crit
  FIELD t-it-codigo     AS CHAR FORMAT "X(16)"
  FIELD t-crit           AS INTEGER FORMAT  ">>,>>>,>>9"
  FIELD t-nec1          AS INTEGER FORMAT  ">>,>>>,>>9"
  FIELD t-nec2          AS INTEGER FORMAT  ">>,>>>,>>9"
  FIELD t-nec3          AS INTEGER FORMAT  ">>,>>>,>>9"
  FIELD t-obs           AS CHAR FORMAT "X(40)"
  FIELD t-data-imp      AS DATE FORMAT "99/99/9999"
  FIELD t-hora-imp      AS CHAR FORMAT "X(10)"
  FIELD t-usuario       AS CHAR FORMAT "X(15)".

DEFINE STREAM   wconsis.

/* FIM VARIAVEIS valeria*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PROCEDIMENTOS ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "IMPORTA��O CRITICOS - EXIBE PERCENTUAL".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
   */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.

ASSIGN procname         = tt-param.warq-importado
       i-pasta-select   = tt-param.wpasta.
IF procname = " " THEN do:
 MESSAGE "Nome do arquivo n�o pode ser branco." VIEW-AS ALERT-BOX.
 NEXT.
 END.
/* Pasta selecionada para importa��o */
/*assign i-pasta-select = 1.  qual pasta est� o conteudo  */
assign i-pasta-select = tt-param.wpasta. /* qual pasta est� o conteudo */

{office/office.i excel v-chr-excel-application} 
v-chr-excel-application:visible = false. 

assign v-chr-work-book = v-chr-excel-application:workbooks:open(tt-param.warq-importado) 
v-chr-work-sheet = v-chr-excel-application:sheets:item(tt-param.wpasta) 
v-int-ultimalinha = v-chr-excel-application:activesheet:usedrange:rows:count. 

do v-int-line = 1 to v-int-ultimalinha: 
assign 
e-it-codigo = v-chr-work-sheet:range('A':U + string(v-int-line)):VALUE          
e-crit      = int(v-chr-work-sheet:range('B':U + string(v-int-line)):VALUE)          
e-nec1      = int(v-chr-work-sheet:range('C':U + string(v-int-line)):VALUE)          
e-nec2      = int(v-chr-work-sheet:range('D':U + string(v-int-line)):VALUE)          
e-nec3      = int(v-chr-work-sheet:range('E':U + string(v-int-line)):VALUE)          
e-obs       = v-chr-work-sheet:range('F':U + string(v-int-line)):VALUE.

create tt-dados-crit. 
run pi-acompanhar in h-acomp ( "Item: " + STRING(e-it-codigo)).
assign 
tt-dados-crit.t-it-codigo    =  (e-it-codigo)    /*item */
tt-dados-crit.t-crit         = INT(e-crit)       /* critico */
tt-dados-crit.t-nec1         = INT(e-nec1)       /* necessidade 1 */
tt-dados-crit.t-nec2         = INT(e-nec2)       /* necessidade 2 */
tt-dados-crit.t-nec3         = INT(e-nec3)        /* necessidade 3 */
tt-dados-crit.t-obs          = (e-obs)
tt-dados-crit.t-data-imp     = TODAY
tt-dados-crit.t-hora-imp     = STRING(TIME,"HH:MM:MM")
tt-dados-crit.t-usuario      = STRING(USERID(ldbname(1)),"x(15)").
/* Aqui est� limitado � 3 campos, por�m pode-se criar de acordo com a necessidade */ 
end. 

v-chr-excel-application:quit(). 

DELETE object v-chr-excel-application no-error. 
DELETE object v-chr-work-sheet no-error. 
DELETE object v-chr-work-book no-error. 
/*  OUTPUT STREAM  wconsis  TO VALUE(SESSION:TEMP-DIR + "CONSISTENCIA.LST") PAGE-SIZE 60.  */
/* Aqui pode-se fazer o que quiser com os dados importados para a temp-table */ 
 FOR EACH tt-dados-crit NO-LOCK:
    /* procura item no cadastro de item */
    FIND ITEM WHERE ITEM.it-codigo = tt-dados-crit.t-it-codigo NO-LOCK NO-ERROR.
   
    IF NOT AVAILABLE ITEM THEN MESSAGE "Item: " tt-dados-crit.t-it-codigo  "N�o existe no cadastro de item."
    VIEW-AS ALERT-BOX.
    /* SE ENCONTRAR ITEM CONTINUA */
    IF AVAILABLE ITEM THEN DO:
/*      FIND FIRST lo-plames WHERE lo-plames.it-codigo = tt-dados-crit.t-it-codigo        */
/*                         AND   lo-plames.anomes    = tt-param.wanomes NO-LOCK NO-ERROR. */
/*     /* verifica se tem plano */                                                                                           */
/*      IF NOT AVAILABLE lo-plames THEN DO:                                                                                  */
/*          /* gera relat. nao importados */                                                                                 */
/*         DISPLAY STREAM  wconsis tt-dados-crit.t-it-codigo LABEL "Item"                                                    */
/*                           "... SEM PLANO MENSAL "  LABEL "Mensagem" tt-param.wanomes "... ITEM NAO IMPORTADO"             */
/*                           WITH FRAME f-consis  DOWN NO-LABELS WIDTH 150 STREAM-IO TITLE "RELATORIO DE INCONSISTENCIAS" .  */
/*            DOWN WITH FRAME f-consis.                                                                                      */
/*          END.                                                                                                             */
/*      IF AVAILABLE lo-plames THEN DO:                                                 */
         FIND lo-perc-crit WHERE lo-perc-crit.it-codigo =  tt-dados-crit.t-it-codigo
         NO-ERROR.

        /* LIMPA OS CAMPOS S ESTIVER VAZIO NA PLANILHA EXCEL */     
        IF tt-dados-crit.t-crit = ? THEN  ASSIGN tt-dados-crit.t-crit = 0.      /* critico */
        IF tt-dados-crit.t-nec1 = ? THEN  ASSIGN tt-dados-crit.t-nec1 = 0.      /* necessidade 1 */
        IF tt-dados-crit.t-nec2 = ? THEN  ASSIGN tt-dados-crit.t-nec2 = 0.      /* necessidade 2 */
        IF tt-dados-crit.t-nec3 = ? THEN  ASSIGN tt-dados-crit.t-nec3 = 0.      /* necessidade 3 */
        IF tt-dados-crit.t-obs = ?  THEN  ASSIGN tt-dados-crit.t-obs = " ".     /* OBS*/    
        
        /* limpa dados antes de importar */
        IF AVAILABLE lo-perc-crit THEN
        ASSIGN   
              lo-perc-crit.it-codigo    = " " 
              lo-perc-crit.crit         = 0       /* critico */
              lo-perc-crit.nec1         = 0       /* necessidade 1 */
              lo-perc-crit.nec2         = 0       /* necessidade 2 */
              lo-perc-crit.nec3         = 0     /* necessidade 3 */
              lo-perc-crit.obs          = " "  
              lo-perc-crit.data-imp     = ?
              lo-perc-crit.hora-imp     = " " 
              lo-perc-crit.usuario      = " ".
        IF NOT AVAILABLE lo-perc-crit THEN CREATE lo-perc-crit.

        ASSIGN   lo-perc-crit.it-codigo    = tt-dados-crit.t-it-codigo  
              lo-perc-crit.crit         = tt-dados-crit.t-crit       /* critico */
              lo-perc-crit.nec1         = tt-dados-crit.t-nec1       /* necessidade 1 */
              lo-perc-crit.nec2         = tt-dados-crit.t-nec2       /* necessidade 2 */
              lo-perc-crit.nec3         = tt-dados-crit.t-nec3       /* necessidade 3 */
              lo-perc-crit.obs          = string(tt-dados-crit.t-obs,"x(40)")    
              lo-perc-crit.data-imp     = tt-dados-crit.t-data-imp 
              lo-perc-crit.hora-imp     = tt-dados-crit.t-hora-imp 
              lo-perc-crit.usuario       = tt-dados-crit.t-usuario.
       /* relat�rio */
       disp lo-perc-crit WITH FRAME f-relat WIDTH 300 DOWN STREAM-IO.
       DOWN WITH FRAME f-relat.
  END.
 END.
/*  OUTPUT STREAM wconsis CLOSE.  */
/*  DOS SILENT START notepad.exe VALUE(SESSION:TEMP-DIR + "CONSISTENCIA.LST").  */

END PROCEDURE.
 

