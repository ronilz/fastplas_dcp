&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI5105RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wpedi            AS INTE FORMAT ">>>,>>>,>>9"
     FIELD wpedf            AS INTE FORMAT ">>>,>>>,>>9"
     FIELD wcli             AS CHAR FORMAT "X(12)"
     FIELD wrepi            AS INTE FORMAT ">>>>9"
     FIELD wrepf            AS INTE FORMAT ">>>>9"
     FIELD wdatai           AS DATE FORMAT "99/99/9999"
     FIELD wdataf           AS DATE FORMAT "99/99/9999".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */
/***********************************************************************
**
** li5105.p   - Exibe Resumo de Pedidos                      
**
**              20/06/2001 - BENE   Programa novo.
**
**
***********************************************************************/

DEFINE NEW SHARED VARIABLE wpgm 
     AS CHAR FORMAT "x(06)" INITIAL "li5105".
DEFINE NEW SHARED VARIABLE wvrs 
     AS CHAR FORMAT "x(03)" INITIAL "I02".
DEFINE NEW SHARED VARIABLE wtit
     AS CHAR FORMAT "x(38)" INITIAL "       Exibe Resumo de Pedidos        ".
DEFINE NEW SHARED VARIABLE wdtd 
     AS DATE FORMAT "99/99/9999"
                            INITIAL TODAY. 
DEFINE NEW SHARED VARIABLE whra
     AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE NEW SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".
DEFINE NEW SHARED VARIABLE wusr         AS CHAR FORMAT "x(10)".

DEFINE NEW SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE VARIABLE wcalled    AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wvia       AS CHAR FORMAT "!".
DEFINE VARIABLE warqtele   AS CHAR FORMAT "X(08)".                    

DEFINE NEW SHARED VARIABLE wimp       AS CHAR FORMAT "X(10)".

/* DEFINE VARIABLE wdescarq    AS CHAR FORMAT "x(16)".  */
DEFINE VARIABLE wctd        AS INTE.
DEFINE VARIABLE wctd1       AS INTE.

DEFINE VARIABLE wctditem    AS INTE.
DEFINE VARIABLE wctdrepeat  AS INTE.

DEFINE VARIABLE wdescdt     AS CHARACTER FORMAT "x(25)".
DEFINE VARIABLE whide       AS INTE FORMAT "9".
DEFINE VARIABLE wctr        AS CHAR FORMAT "x(36)".
DEFINE VARIABLE wdr         AS CHAR FORMAT "!".

DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(78)".

DEFINE VARIABLE wcab0       AS CHAR FORMAT "x(78)".
/*                                                        */
/* DEFINE VARIABLE wpedi     LIKE ped-venda.nr-pedido.    */
/* DEFINE VARIABLE wpedf     LIKE ped-venda.nr-pedido.    */
/* DEFINE VARIABLE wcli      LIKE ped-venda.nome-abrev.   */
/*                                                        */
/* DEFINE VARIABLE wrepi     LIKE repres.cod-rep.         */
/* DEFINE VARIABLE wrepf     LIKE repres.cod-rep.         */
/* DEFINE VARIABLE wdatai    AS DATE FORMAT "99/99/9999". */
/* DEFINE VARIABLE wdataf    AS DATE FORMAT "99/99/9999". */

DEFINE VARIABLE wppp      AS INTE FORMAT "99".
DEFINE VARIABLE wttlh    AS INTE                INITIAL 999.
DEFINE VARIABLE wcor      AS CHAR.
DEFINE VARIABLE wdescompra AS CHAR FORMAT "x(10)".

DEFINE VARIABLE wmoeda        AS CHAR FORMAT "x(12)".              

DEFINE VARIABLE wsitped  AS CHAR FORMAT "x(12)" EXTENT 10
       INITIAL["0-Aberto","1-Atd Parc","2-Atd Tot","3-Pendente","4-Suspenso",
               "5","6","7","8","9-Cancelado"]. 

DEFINE BUFFER bff-venda FOR ped-venda.
DEFINE VARIABLE wlinha AS CHAR FORMAT "X(130)".

    /* FINAL VARIAVEIS BENE */


create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Resumo de Pedidos".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
ASSIGN wlinha = FILL("-",130).
DISPLAY "Pedido: " tt-param.wpedi "-" tt-param.wpedf " / Cliente: " tt-param.wcli 
        " / Representante: " tt-param.wrepi "-" tt-param.wrepf
        "/ Dt.emis.: " tt-param.wdatai "-" tt-param.wdataf WITH FRAME f-parametros WIDTH 132 NO-LABEL STREAM-IO PAGE-TOP.

        /*--------------------------------------------INICIO MOVIMENTO-------*/
        /*...............................................inicio pedidos...*/
        FOR
        EACH  ped-venda
        WHERE ped-venda.nr-pedido   GE tt-param.wpedi
        AND   ped-venda.nr-pedido   LE tt-param.wpedf
        AND   ped-venda.nome-abrev MATCHES(tt-param.wcli)
        AND   ped-venda.dt-emissao GE tt-param.wdatai
        AND   ped-venda.dt-emissao LE tt-param.wdataf
        NO-LOCK:
             
          FIND  emitente
          WHERE emitente.nome-abrev = ped-venda.nome-abrev
          NO-LOCK NO-ERROR.

          FIND  repres  
          WHERE repres.nome-abrev = ped-venda.no-ab-reppri
          NO-LOCK NO-ERROR.
          IF repres.cod-rep LT tt-param.wrepi
          OR repres.cod-rep GT tt-param.wrepf
          THEN NEXT.

          FIND  mensagem
          WHERE mensagem.cod-mensagem = ped-venda.cod-mensagem
          NO-LOCK NO-ERROR.

          ASSIGN wmoeda = "Padrao".

    /***************************************************--inicio inibicao--*****
          IF ped-venda.mo-codigo GT 0 THEN DO:
            FIND  moeda
            WHERE moeda.mo-codigo = ped-venda.mo-codigo
            NO-LOCK NO-ERROR.
            IF AVAILABLE moeda THEN ASSIGN wmoeda = moeda.descricao.
            END.
                                ************************--fim da inibicao--********/


          /*...............................................inicio exibe tudo...*/
        


            ASSIGN wdescompra = "          ".
            FIND  FIRST bff-venda
            WHERE bff-venda.nome-abrev = ped-venda.nome-abrev
            AND   bff-venda.nr-pedcli NE ped-venda.nr-pedcli
            NO-LOCK NO-ERROR.
            IF NOT AVAILABLE bff-venda THEN DO:
              ASSIGN wdescompra = "1a.COMPRA".
              END.

            DISPLAY STRING(ped-venda.nr-pedcli,"x(12)") AT 29 COLUMN-LABEL "PEDIDO"
                    emitente.cod-emitente "-" emitente.nome-abrev  COLUMN-LABEL "CLIENTE"
                                            emitente.cgc          COLUMN-LABEL "CGC"
                                            ped-venda.dt-emissao           COLUMN-LABEL "Emissao"
                                            ped-venda.vl-tot-ped  COLUMN-LABEL "Vr.Total" 
                                            wdescompra            NO-LABEL
                WITH FRAME f-ped WIDTH 132 STREAM-IO DOWN.
                DOWN WITH FRAME f-ped.
              
                                    /*...........................................inicio exibe item...*/
            FOR EACH ped-item
            OF   ped-venda
            NO-LOCK:

              FIND item WHERE item.it-codigo = ped-item.it-codigo.

              DISPLAY  ped-item.it-codigo   COLUMN-LABEL "Item" AT 41
                       item.descricao-1     COLUMN-LABEL "Descricao"
                       ped-item.dt-entrega  COLUMN-LABEL "Entrega"
                       STRING(INTE(ped-item.qt-pedida)) COLUMN-LABEL "Qtde"
                   WITH FRAME f-relat STREAM-IO WIDTH 132.
              DOWN WITH FRAME f-relat.
        
              END.
              DISPLAY wlinha WITH WIDTH 132 NO-LABEL.  
            END.
END PROCEDURE.




