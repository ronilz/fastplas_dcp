&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i LI0915RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wcod-estabeli    AS CHAR FORMAT "X(03)"
    FIELD wcod-estabelf    AS CHAR FORMAT "X(03)"
    FIELD wfami            AS CHAR FORMAT "X(8)"
    FIELD wfamf            AS CHAR FORMAT "X(8)"
    FIELD wite             AS CHAR FORMAT "X(16)"
    FIELD wdefi            AS INTE FORMAT ">>9"
    FIELD wdeff            AS INTE FORMAT ">>9"
    FIELD wdatai           AS DATE FORMAT "99/99/9999"
    FIELD wdataf           AS DATE FORMAT "99/99/9999"
    FIELD wctrle           AS CHAR FORMAT "X(15)"
    FIELD wdr              AS CHAR FORMAT "X(17)"
    FIELD wnrlhi           AS INTE FORMAT ">>9"
    FIELD wnrlhf           AS INTE FORMAT ">>9"
    FIELD westsn1          AS LOGICAL
    FIELD westsn2          AS LOGICAL
    FIELD westsn3          AS LOGICAL
    FIELD westsn4          AS LOGICAL
    FIELD westsn5          AS LOGICAL
    FIELD westsn6          AS LOGICAL
    FIELD westsn7          AS LOGICAL
    FIELD westsn8          AS LOGICAL
    FIELD westsn9          AS LOGICAL.

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat NO-LABELS.

/* VARIAVEIS BENE */
DEFINE VARIABLE wctd        AS INTE.
DEFINE VARIABLE wctd1       AS INTE.


DEFINE VARIABLE wdescdt     AS CHARACTER FORMAT "x(25)".
DEFINE VARIABLE wdescfam    AS CHARACTER FORMAT "x(24)".
DEFINE VARIABLE wcab        AS CHARACTER FORMAT "x(78)".
DEFINE VARIABLE wdescdef    AS CHARACTER FORMAT "x(34)".
DEFINE VARIABLE wdescitem   AS CHARACTER FORMAT "x(13)".
DEFINE VARIABLE wfami       LIKE fam-comerc.fm-cod-com.
DEFINE VARIABLE wfamf       LIKE fam-comerc.fm-cod-com.
DEFINE VARIABLE wite  LIKE item.it-codigo.    
DEFINE VARIABLE wdatai       AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wdataf       AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE widata       AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wfdata       AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wxdata       AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wiaamm       AS CHAR FORMAT "999999".
DEFINE VARIABLE wfaamm       AS CHAR FORMAT "999999".
DEFINE VARIABLE wxaamm       AS CHAR FORMAT "999999".
DEFINE VARIABLE wdayi        AS INTE FORMAT "99".
DEFINE VARIABLE wdayf        AS INTE FORMAT "99".
DEFINE VARIABLE wdayx        AS INTE FORMAT "99".

DEFINE VARIABLE witemant    LIKE item.it-codigo.

DEFINE VARIABLE wctr        AS CHAR FORMAT "x(20)".
DEFINE VARIABLE wctrle      AS CHAR FORMAT "!".
DEFINE VARIABLE wdefi       LIKE cod-rejeicao.codigo-rejei.
DEFINE VARIABLE wdeff       LIKE cod-rejeicao.codigo-rejei.  
DEFINE VARIABLE wdr         AS CHAR FORMAT "!".

DEFINE VARIABLE wmsg        AS CHAR FORMAT "x(78)".
DEFINE VARIABLE wstart      AS CHAR FORMAT "x(01)".
DEFINE VARIABLE wrel        AS CHAR FORMAT "x(29)".
DEFINE VARIABLE wdet        AS CHAR FORMAT "x(78)".

DEFINE VARIABLE wqestorno    LIKE movto-estoq.quantidade.    
DEFINE VARIABLE wquant    AS INTE      FORMAT "zzz,zzz,zzz,zz9-".
DEFINE VARIABLE wprztot   AS INTE      FORMAT "zzz,zzz,zzz,zz9-".
DEFINE VARIABLE wdefqde   AS INTE      FORMAT "zzz,zzz,zzz,zz9-".
DEFINE VARIABLE wdeftot   AS INTE      FORMAT "zzz,zzz,zzz,zz9-".
DEFINE VARIABLE wper      AS DECI      FORMAT ">>>>9.99-".

DEFINE VARIABLE westletra AS CHAR FORMAT "!"       EXTENT 9
       INITIAL ["N","L","A","E","S","R","I","F","T"].

/***--------------------------------------------------inicio-criado p/EMS ***/
DEFINE VARIABLE westnuems AS CHAR FORMAT "x(01)"   EXTENT 9
       INITIAL ["1","0","2","3","4","5","6","7","8"].
/***--------------------------------------------------fim de criado p/EMS ***/


DEFINE VARIABLE westsn    AS LOGI FORMAT "Sim/Nao" EXTENT 9       
       INITIAL [yes,yes,yes,yes,yes,yes,yes,yes,yes].
DEFINE VARIABLE westlista AS CHAR.
DEFINE VARIABLE westpos   AS INTE.
DEFINE VARIABLE wlinha1 AS CHAR FORMAT "X(130)".
 
/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Exibe Retrabalho/Sucata por Defeito".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
   */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
DO:
  ASSIGN wlinha1 = FILL("-",132).
  DISPLAY 
     "Cod.Estab.: " tt-param.wcod-estabeli "-" tt-param.wcod-estabelf
     "/Familia: "  tt-param.wfami
     "-" tt-param.wfamf            
     "/Item: "     tt-param.wite          
     "/Data: "     tt-param.wdatai "-" tt-param.wdataf           
     SKIP
     "Controle: " tt-param.wctrle   
     "/Total: "    tt-param.wdr       
     "/Linha Prod.: "    tt-param.wnrlhi "-" tt-param.wnrlhf           
     "/Estado da Ordem:" tt-param.westsn1     
     SKIP
     "Defeito: " tt-param.wdefi "-" tt-param.wdeff
     tt-param.westsn2           
     tt-param.westsn3          
     tt-param.westsn4          
     tt-param.westsn5          
     tt-param.westsn6           
     tt-param.westsn7          
     tt-param.westsn8           
     tt-param.westsn9      
     SKIP
     wlinha1  
     SKIP(1)
     WITH FRAME f-cabec1 WITH WIDTH 134 STREAM-IO PAGE-TOP NO-LABELS.


  ASSIGN westsn[1] = tt-param.westsn1
         westsn[2] = tt-param.westsn2
         westsn[3] = tt-param.westsn3
         westsn[4] = tt-param.westsn4
         westsn[5] = tt-param.westsn5
         westsn[6] = tt-param.westsn6
         westsn[7] = tt-param.westsn7
         westsn[8] = tt-param.westsn8
         westsn[9] = tt-param.westsn9.

/*.................................................fim de entrada padrao..*/
  ASSIGN wprztot = 0
         wdefqde = 0 wdeftot = 0.

  ASSIGN westlista = "".
  DO westpos = 1 TO 9:
    IF westsn[westpos] THEN DO:
      IF LENGTH(westlista) GT 0 THEN ASSIGN westlista = westlista + ",".

      /*** ---------------inicio inibido p EMS  
      ASSIGN westlista = westlista + westletra[westpos].
                          ------------------------fim de inibido p EMS */

      /*** ---------------------------------------inicio criado p EMS */
      ASSIGN westlista = westlista + STRING(westnuems[westpos]).
      /*** ---------------------------------------fim de criado p EMS */

      END.
    END.

  /*.........................................inicio montagem do cabecalho...*/
  IF tt-param.wdr = "Detalhado" THEN DO:
    IF tt-param.wctrle = "Item" THEN DO:
      ASSIGN wcab = "Defeito  Item        Descricao       ".
      END.
    IF tt-param.wctrle = "Peca" THEN DO:
      ASSIGN wcab = "Defeito  Peca        Descricao       ".
      END.
    IF tt-param.wctrle = "Codigos" THEN DO:
      ASSIGN wcab = "Defeito  Item        Peca            ".
      END.
    END.
  IF tt-param.wdr = "Resumido" THEN DO:
    ASSIGN   wcab = "Defeito                              ".
    END.

  IF tt-param.wdr = "Detalhado" THEN DO:
    ASSIGN wcab = wcab + "  Data     Fam  Przdo  Q.Defeitos   %   ".
    END.
  IF tt-param.wdr = "Resumido" THEN DO:
    ASSIGN wcab = wcab + "                Przdo  Q.Defeitos   %   ".
    END.
  /*.........................................fim da montagem do cabecalho...*/

DISPLAY wcab SKIP wlinha1 SKIP WITH FRAME f-cabec2 PAGE-TOP NO-LABELS WIDTH 132.
/*------------------------------EXTRAINDO PRODUZIDO--------------------------*/
   
    FOR
    EACH  item
    WHERE item.it-codigo MATCHES(tt-param.wite)
    AND   item.fm-cod-com   GE   tt-param.wfami
    AND   item.fm-cod-com   LE   tt-param.wfamf
    AND   item.cod-obsoleto <> 4
    NO-LOCK:
     run pi-acompanhar in h-acomp ("Fam�lia: " + STRING(item.fm-cod-com)).
      FOR
      EACH  movto-estoq
      WHERE movto-estoq.cod-estabel  = tt-param.wcod-estabeli
/*       AND   movto-estoq.cod-estabel  LE tt-param.wcod-estabelf  */
      AND   movto-estoq.it-codigo   =  item.it-codigo
      AND   movto-estoq.dt-trans    GE tt-param.wdatai
      AND   movto-estoq.dt-trans    LE tt-param.wdataf
      AND ((movto-estoq.esp-docto    =  1 OR
            movto-estoq.esp-docto    = 25 OR
            movto-estoq.esp-docto    =  8)
                            OR
           (movto-estoq.esp-docto    = 28 AND
                          movto-estoq.serie          GE "0"      AND
                          movto-estoq.serie          LE "999")
                            OR
           (movto-estoq.esp-docto    = 31 AND
                          movto-estoq.serie          GE "0"      AND
                          movto-estoq.serie          LE "999"))
      NO-LOCK:
        
        IF movto-estoq.quantidade = 0 THEN NEXT.

        IF movto-estoq.nr-ord-produ GT 0
        THEN DO: 
          FIND ord-prod
          WHERE ord-prod.nr-ord-prod = movto-estoq.nr-ord-produ
          AND   ord-prod.nr-linha    GE tt-param.wnrlhi
          AND   ord-prod.nr-linha    LE tt-param.wnrlhf
          NO-LOCK
          NO-ERROR.
          IF NOT AVAILABLE ord-prod
          OR LOOKUP(STRING(ord-prod.estado),westlista) = 0 THEN NEXT.
          END.       
        IF  (movto-estoq.nr-ord-produ = 0)
        AND (movto-estoq.esp-docto    = 28)
        AND (tt-param.wnrlhi NE 0   OR
             tt-param.wnrlhf NE 999 OR
             westsn[1]   = no  OR
             westsn[2]   = no  OR
             westsn[3]   = no  OR
             westsn[4]   = no  OR
             westsn[5]   = no  OR
             westsn[6]   = no  OR
             westsn[7]   = no  OR
             westsn[8]   = no  OR
             westsn[9]   = no)   
        THEN NEXT.

        ASSIGN wquant = movto-estoq.quantidade.
        IF movto-estoq.tipo-trans = 2 THEN DO:
          ASSIGN wquant = wquant * -1.
          END.

        ASSIGN
        wprztot  = wprztot  + wquant.                          

        IF movto-estoq.esp-docto = 25
        OR movto-estoq.esp-docto = 28
        OR movto-estoq.esp-docto = 31
        THEN DO:
          ASSIGN wqestorno = movto-estoq.quantidade.
          IF (movto-estoq.esp-docto  = 25 OR
              movto-estoq.esp-docto  = 31)
          AND movto-estoq.tipo-trans = 1 
          THEN DO:
            ASSIGN wqestorno = wqestorno * -1.
            END.

          ASSIGN
          wprztot  = wprztot  + wqestorno.                     
          END.            

        END.

      END.

    FOR EACH cod-rejeicao
       WHERE cod-rejeicao.codigo-rejei GE tt-param.wdefi
       AND   cod-rejeicao.codigo-rejei LE tt-param.wdeff
       NO-LOCK:

    
      IF tt-param.wdr = "Detalhado" THEN DO:
        wdescdef = STRING(cod-rejeicao.codigo-rejei,"zz9") + " "
                 + STRING(cod-rejeicao.descricao,"x(30)").
        END.                    


/*------------------------------EXTRAINDO REFUGADO---------------------------*/
     
      /*......................................................INICIO ITEM...*/
      FOR EACH item
         WHERE item.fm-cod-com     GE tt-param.wfami
         AND   item.fm-cod-com     LE tt-param.wfamf
         AND   item.cod-obsoleto   NE 4
         AND   item.it-codigo  MATCHES(tt-param.wite)
         NO-LOCK:

       

        FOR
        EACH  movto-estoq
        WHERE movto-estoq.cod-estabel  = tt-param.wcod-estabeli
/*         AND   movto-estoq.cod-estabel  LE tt-param.wcod-estabelf  */
        AND   movto-estoq.it-codigo   = item.it-codigo
        AND   movto-estoq.dt-trans    GE tt-param.wdatai
        AND   movto-estoq.dt-trans    LE tt-param.wdataf
        AND ((movto-estoq.esp-docto    = 25)   
                              OR
             (movto-estoq.esp-docto    = 28 AND
                            movto-estoq.serie          GE "0"      AND
                            movto-estoq.serie          LE "999")
                              OR
             (movto-estoq.esp-docto    = 31 AND
                            movto-estoq.serie          GE "0"      AND
                            movto-estoq.serie          LE "999"))
        NO-LOCK:

    

          IF movto-estoq.serie
          NE STRING(cod-rejeicao.codigo-rejei)
          THEN NEXT.

          IF movto-estoq.quantidade = 0 THEN NEXT.

          IF movto-estoq.nr-ord-produ GT 0
          THEN DO: 
            FIND ord-prod
            WHERE ord-prod.nr-ord-prod = movto-estoq.nr-ord-produ
            AND   ord-prod.nr-linha    GE tt-param.wnrlhi
            AND   ord-prod.nr-linha    LE tt-param.wnrlhf
            NO-LOCK
            NO-ERROR.
            IF NOT AVAILABLE ord-prod
            OR LOOKUP(STRING(ord-prod.estado),westlista) = 0 THEN NEXT.
            END.       
          IF  (movto-estoq.nr-ord-produ = 0)
          AND (movto-estoq.esp-docto    = 28)
          AND (tt-param.wnrlhi NE 0   OR
               tt-param.wnrlhf NE 999 OR
               westsn[1]   = no  OR
               westsn[2]   = no  OR
               westsn[3]   = no  OR
               westsn[4]   = no  OR
               westsn[5]   = no  OR
               westsn[6]   = no  OR
               westsn[7]   = no  OR
               westsn[8]   = no  OR
               westsn[9]   = no)   
          THEN NEXT.

          ASSIGN wquant = movto-estoq.quantidade.
          IF movto-estoq.tipo-trans = 2 THEN DO:
            ASSIGN wquant = wquant * -1.
            END.

          IF movto-estoq.esp-docto = 25
          OR movto-estoq.esp-docto = 28
          OR movto-estoq.esp-docto = 31
          THEN DO:
            ASSIGN wqestorno = movto-estoq.quantidade.
            IF (movto-estoq.esp-docto  = 25 OR
                movto-estoq.esp-docto  = 31)
            AND movto-estoq.tipo-trans = 1 
            THEN DO:
              ASSIGN wqestorno = wqestorno * -1.
              END.

            ASSIGN wdefqde  = wdefqde  + wqestorno                   
                   wdeftot  = wdeftot  + wqestorno.                      
            END.

          /*.............................inicio montagem do controle.......*/
          ASSIGN wctr = "".
          DO wctd1 = 1 TO 3:
            IF  
            (SUBSTRING(STRING(cod-rejeicao.codigo-rejei,"zz9"),wctd1,1)
             NE " ")
            THEN DO:
              ASSIGN 
              wctr = wctr +
              SUBSTRING(STRING(cod-rejeicao.codigo-rejei,"zz9"),wctd1,1).
              END.
            END.
          ASSIGN wctr = wctr + " ".
          
          IF (tt-param.wctrle = "Item" OR
              tt-param.wctrle = "Codigos") THEN DO:
            DO wctd1 = 1 TO 16:
              IF (SUBSTRING(item.it-codigo,wctd1,1) NE " ") 
              THEN DO:
                ASSIGN wctr = wctr
                            + SUBSTRING(item.it-codigo,wctd1,1).
                END.
              END.
            ASSIGN wctr = wctr + " ".
            END.

          IF  (tt-param.wctrle = "Peca" OR
               tt-param.wctrle = "Codigos")
          AND (item.codigo-refer NE "")      
          THEN DO:
            DO wctd1 = 1 TO 20:
              IF  (SUBSTRING(item.codigo-refer,wctd1,1) NE " ") 
              AND (SUBSTRING(item.codigo-refer,wctd1,1) NE ".")
              THEN DO:
                ASSIGN wctr = wctr
                            + SUBSTRING(item.codigo-refer,wctd1,1).
                END.
              END.
            ASSIGN wctr = wctr + " ".
            END.

          IF LENGTH(wctr) LT 18
          THEN DO:
            ASSIGN wctr = wctr + FILL(" ",(18 - LENGTH(wctr))).
            END.
          
          IF  (tt-param.wctrle = "Item" OR
               tt-param.wctrle = "Peca")
          THEN DO:
            ASSIGN wctr = wctr
                        + STRING(item.descricao-1).
            END.            
          /*.............................fim da montagem do controle.......*/


/*------------------------------EXIBINDO DETALHES---------------------------*/

          IF tt-param.wdr = "Detalhado" 
          THEN DO:
            ASSIGN wquant = wquant * -1.
            wper = 0.
            IF wprztot GT 0 THEN
            ASSIGN wper = (movto-estoq.quantidade / wprztot) * 100.
            wdet = STRING(wctr,"x(36)") + " "
                 + STRING(movto-estoq.dt-trans,"99/99/9999") + " "
                 + STRING(item.fm-cod-com,"x(08)") + "   "
                 + STRING(wquant,"zzzzzzzz9-")
                 + STRING(wper,">>>>9.99-").
            DISPLAY wdet WITH FRAME f-relat.
            DOWN 1 WITH FRAME f-relat.
            END.

          END.
 
        END.
       /*......................................................FIM DE ITEM...*/
      




/*--------------------------EXIBE TOTAL DO DEFEITO-------------------------*/

      IF wdefqde GT 0 
      THEN DO:
        IF tt-param.wdr = "Detalhado" THEN DO:
          wdet = FILL(" ",78).
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.
        wper = 0.
        IF wprztot GT 0 THEN
        ASSIGN wper = (wdefqde / wprztot ) * 100.
        wdet = STRING(cod-rejeicao.codigo-rejei,"zz9") + " "
             + STRING(cod-rejeicao.descricao,"x(30)")
             + FILL(".",25)
             + STRING(wdefqde,"zzzzzzzz9-")
             + STRING(wper,">>>>9.99-").
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        IF tt-param.wdr = "Detalhado" THEN DO:
          ASSIGN wdet = FILL("_",78).
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          ASSIGN wdet = FILL(" ",78).
          DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
          END.
        END.
      wdefqde = 0.

      END.
 
/*-----------------------EXIBE TOTAL GERAL-----------------------------------*/

    IF wdeftot GT 0
    OR wprztot GT 0 
    THEN DO:
      IF tt-param.wdr = "Resumido" THEN DO:
        wdet = FILL(" ",78).
        DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
        END.
      wper = 0.
      IF wprztot GT 0 THEN
      ASSIGN wper = (wdeftot / wprztot) * 100.
      wdet = FILL(" ",25)
           + "Total da consulta "
           + STRING(wprztot,"zzz,zzz,zzz,zz9-")
           + STRING(wdeftot,"zzzzzzzz9-")
           + STRING(wper,">>>>9.99-").
      DISPLAY wdet WITH FRAME f-relat. DOWN 1 WITH FRAME f-relat.
      END.
    END.


END PROCEDURE.




