/***********************************************************************
**
** liwcliv.p - Monta WORKFILE de clientes: showcli             
**
**              13/12/2002 - BENE   Programa novo.
**                           copia do subprograma liwcliv incluindo a 
**                           selecao por geracao a pedido de Bortoleto.
**
***********************************************************************/

DEFINE NEW SHARED VARIABLE wbdd-area    AS CHAR FORMAT "x(30)".
DEFINE SHARED VARIABLE wbdd         AS CHAR FORMAT "x(08)".
DEFINE SHARED VARIABLE wbdd-integri AS CHAR FORMAT "x(08)".

DEFINE SHARED VARIABLE wcaller      AS CHAR FORMAT "!".            

DEFINE SHARED VARIABLE wcodrefaux   AS CHAR FORMAT "x(20)".

DEFINE SHARED VARIABLE witemaux     LIKE item.it-codigo.

DEFINE SHARED VARIABLE wfa-operacao LIKE lo-clifam.fa-operacao.

DEFINE SHARED VARIABLE wcli         AS CHARACTER FORMAT "x(12)".
DEFINE SHARED VARIABLE wtipoforn    AS CHAR FORMAT "x(01)".
DEFINE SHARED VARIABLE wprv         AS CHAR FORMAT "!".
DEFINE SHARED VARIABLE wpst         AS CHAR FORMAT "!".

DEFINE SHARED VARIABLE wqualpgm     AS CHAR FORMAT "!".
DEFINE SHARED VARIABLE wdtpgm       LIKE lo-clipgm.data-pgm.
DEFINE SHARED VARIABLE a-dtpgm       LIKE lo-clipgm.data-pgm.
DEFINE SHARED VARIABLE wgersel      AS CHAR FORMAT "x(12)".    


DEFINE VARIABLE wloclista AS CHAR INITIAL "".
DEFINE VARIABLE wloclist1 AS CHAR INITIAL "72664,72668,72669,72677".
DEFINE VARIABLE wloclist2 AS CHAR INITIAL "72667,72671".
DEFINE VARIABLE wloclist3 AS CHAR INITIAL "72681".

DEFINE VARIABLE wlocabrev      AS CHAR   FORMAT "x(12)".
DEFINE VARIABLE wlocabre1      AS CHAR   FORMAT "x(12)" INITIAL "GMB SJC".
DEFINE VARIABLE wlocabre2      AS CHAR   FORMAT "x(12)" INITIAL "GMB-SCS".
DEFINE VARIABLE wlocabre3      AS CHAR   FORMAT "x(12)" INITIAL "GMB.SCS".

DEFINE VARIABLE wgeri         AS CHAR FORMAT "x(12)".
DEFINE VARIABLE wgerf         AS CHAR FORMAT "x(12)".
DEFINE VARIABLE wgerpedida    AS CHAR FORMAT "x(12)".
DEFINE VARIABLE a-gerpedida    AS CHAR FORMAT "x(12)".

DEFINE VARIABLE wprogatual    LIKE lo-rn01.progatual.
DEFINE VARIABLE wseculo       AS CHAR FORMAT "x(02)".

DEFINE VARIABLE wdtx          AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE wnfx          AS INTE FORMAT ">>>>>>9".

DEFINE VARIABLE wchavepa      AS INTE.
DEFINE VARIABLE wnomecli      AS CHAR FORMAT "x(12)".

DEFINE VARIABLE wregcli     AS INTE.             

DEFINE SHARED WORKFILE showcli
          FIELD  numreg       AS INTE
          FIELD  codarq       AS CHAR FORMAT "X(10)"                       
          FIELD  loclista     AS CHAR                                    

          FIELD  ageri         AS CHAR FORMAT "X(12)"                       
          FIELD  agerf         AS CHAR FORMAT "X(12)"                     
          FIELD  aprogatual    LIKE lo-rn01.progatual
          FIELD  acod-emitente AS INTE FORMAT ">>>>>9"                     
          FIELD  anome-abrev   AS CHAR FORMAT "X(12)"                      
          FIELD  adata-pgm     AS DATE      FORMAT "99/99/9999"            
          FIELD  acod-pgm      AS CHAR      FORMAT "x(6)"                  
          FIELD  atipo-pgm     AS CHAR      FORMAT "X(1)"                  
          FIELD  adtnf         AS DATE                                        
          FIELD  anf           AS INTE      FORMAT ">>>>>>9"              

          FIELD  geri         AS CHAR FORMAT "X(12)"                       
          FIELD  gerf         AS CHAR FORMAT "X(12)"                     
          FIELD  progatual    LIKE lo-rn01.progatual
          FIELD  cod-emitente AS INTE FORMAT ">>>>>9"                     
          FIELD  nome-abrev   AS CHAR FORMAT "X(12)"                      
          FIELD  data-pgm     AS DATE      FORMAT "99/99/9999"            
          FIELD  cod-pgm      AS CHAR      FORMAT "x(6)"                  
          FIELD  tipo-pgm     AS CHAR      FORMAT "X(1)"                  
          FIELD  dtnf         AS DATE                                        
          FIELD  nf           AS INTE      FORMAT ">>>>>>9"              

          FIELD  atrsd        AS INTE      FORMAT ">>>>>>>>9-"           
          FIELD  ne           AS INTE      FORMAT ">>>>>>>>9-" EXTENT 4.




ASSIGN wgerpedida = SUBSTRING(STRING(YEAR(wdtpgm),"9999"),03,02)
                            + STRING(MONTH(wdtpgm),"99")
                            + STRING(DAY(wdtpgm),"99")
                            + "999999".

ASSIGN a-gerpedida = SUBSTRING(STRING(YEAR(a-dtpgm),"9999"),03,02)
                             + STRING(MONTH(a-dtpgm),"99")
                             + STRING(DAY(a-dtpgm),"99")
                             + "999999".

/*.............................inicio limpeza da TABELA DE CLIENTES..*/
FOR EACH showcli:
  DELETE showcli.
  END.
ASSIGN wregcli = 0.  
/*.............................fim de limpeza da TABELA DE CLIENTES..*/


/*...........................inicio LO-CLIPGM NA TABELA DE CLIENTES..*/
IF  (wfa-operacao NE "GMB")
AND (wfa-operacao NE "JIT")
THEN DO:

  IF wqualpgm = "U" THEN DO:
    FIND LAST lo-clipgm 
        WHERE lo-clipgm.codrefsbp     =    wcodrefaux             
        AND   lo-clipgm.nome-abrev MATCHES(wcli)
        AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
        USE-INDEX ctrpcl
        NO-LOCK
        NO-ERROR.
    END.
  IF wqualpgm = "D" THEN DO:
    FIND LAST lo-clipgm 
        WHERE lo-clipgm.codrefsbp     =    wcodrefaux             
        AND   lo-clipgm.nome-abrev MATCHES(wcli)
        AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
        AND   lo-clipgm.data-pgm     LE    wdtpgm
        USE-INDEX ctrpcl
        NO-LOCK
        NO-ERROR.
    END.


  ASSIGN wnomecli = "ZZZZZZZZZZZZ".
  IF AVAILABLE lo-clipgm THEN DO:
    REPEAT:

      ASSIGN wchavepa = 0.
      IF lo-clipgm.cod-pgm MATCHES("*-*")
      THEN ASSIGN wchavepa = 1.
      
      IF (wpst = "T")
      OR (wpst = "P" AND wchavepa = 1)
      OR (wpst = "S" AND wchavepa = 0)
      THEN DO:
        ASSIGN wregcli = wregcli + 1.
        CREATE showcli.
        ASSIGN showcli.numreg     = wregcli
               showcli.codarq     = "lo-clipgm"
               showcli.nome-abrev = lo-clipgm.nome-abrev         
               showcli.data-pgm   = lo-clipgm.data-pgm   
               showcli.cod-pgm    = lo-clipgm.cod-pgm
               showcli.tipo-pgm   = lo-clipgm.tipo-pgm
               showcli.nf         = lo-clipgm.nf
               showcli.dtnf       = lo-clipgm.dtnf.
        ASSIGN wnomecli = showcli.nome-abrev.
        FIND  mgadm.emitente
        WHERE mgadm.emitente.nome-abrev = wnomecli                     
        NO-LOCK NO-ERROR.
        IF AVAILABLE mgadm.emitente THEN DO:
          ASSIGN showcli.cod-emitente = mgadm.emitente.cod-emitente.
          END.
        END.

      IF wqualpgm = "U" THEN DO:
        FIND PREV lo-clipgm 
            WHERE lo-clipgm.codrefsbp     =    wcodrefaux
            AND   lo-clipgm.nome-abrev MATCHES(wcli)
            AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
            AND   lo-clipgm.nome-abrev LT      wnomecli        
            USE-INDEX ctrpcl
            NO-LOCK
            NO-ERROR.
        END.

      IF wqualpgm = "D" THEN DO:
        FIND PREV lo-clipgm 
            WHERE lo-clipgm.codrefsbp     =    wcodrefaux
            AND   lo-clipgm.nome-abrev MATCHES(wcli)
            AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
            AND   lo-clipgm.nome-abrev LT      wnomecli            
            AND   lo-clipgm.data-pgm     LE    wdtpgm
            USE-INDEX ctrpcl
            NO-LOCK
            NO-ERROR.
        END.

      IF NOT AVAILABLE lo-clipgm THEN LEAVE.
      END.
    END.

  /*.....................................inicio LO-GLIPGM anterior...*/
  FOR EACH showcli:
    IF wqualpgm = "U" THEN DO:
      FIND LAST lo-clipgm 
          WHERE lo-clipgm.codrefsbp     = wcodrefaux             
          AND   lo-clipgm.nome-abrev    = showcli.nome-abrev
          AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
          AND  (lo-clipgm.data-pgm     LT showcli.data-pgm
             OR lo-clipgm.cod-pgm      LT showcli.cod-pgm
             OR lo-clipgm.tipo-pgm     LT showcli.tipo-pgm)
          USE-INDEX ctrpcl NO-LOCK NO-ERROR.
      END.
    IF wqualpgm = "D" THEN DO:
      FIND LAST lo-clipgm 
          WHERE lo-clipgm.codrefsbp     =    wcodrefaux             
          AND   lo-clipgm.nome-abrev    = showcli.nome-abrev
          AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
          AND  (lo-clipgm.data-pgm     LT showcli.data-pgm
             OR lo-clipgm.cod-pgm      LT showcli.cod-pgm
             OR lo-clipgm.tipo-pgm     LT showcli.tipo-pgm)
          AND   lo-clipgm.data-pgm     LE    a-dtpgm
          USE-INDEX ctrpcl NO-LOCK NO-ERROR.
      END.

    REPEAT:
      IF NOT AVAILABLE lo-clipgm THEN LEAVE.

      ASSIGN wchavepa = 0.
      IF lo-clipgm.cod-pgm MATCHES("*-*")
      THEN ASSIGN wchavepa = 1.
      
      IF (wpst = "T")
      OR (wpst = "P" AND wchavepa = 1)
      OR (wpst = "S" AND wchavepa = 0)
      THEN DO:

        ASSIGN showcli.anome-abrev = lo-clipgm.nome-abrev         
               showcli.adata-pgm   = lo-clipgm.data-pgm   
               showcli.acod-pgm    = lo-clipgm.cod-pgm
               showcli.atipo-pgm   = lo-clipgm.tipo-pgm
               showcli.anf         = lo-clipgm.nf
               showcli.adtnf       = lo-clipgm.dtnf.
        FIND  mgadm.emitente
        WHERE mgadm.emitente.nome-abrev = lo-clipgm.nome-abrev
        NO-LOCK NO-ERROR.
        IF AVAILABLE mgadm.emitente THEN DO:
          ASSIGN showcli.acod-emitente = mgadm.emitente.cod-emitente.
          END.
        LEAVE.
        END.

      IF wqualpgm = "U" THEN DO:
        FIND PREV lo-clipgm 
            WHERE lo-clipgm.codrefsbp     = wcodrefaux             
            AND   lo-clipgm.nome-abrev    = showcli.nome-abrev
            AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
            AND  (lo-clipgm.data-pgm     LT showcli.data-pgm
               OR lo-clipgm.cod-pgm      LT showcli.cod-pgm
               OR lo-clipgm.tipo-pgm     LT showcli.tipo-pgm)
            USE-INDEX ctrpcl NO-LOCK NO-ERROR.
        END.
      IF wqualpgm = "D" THEN DO:
        FIND PREV lo-clipgm 
            WHERE lo-clipgm.codrefsbp     =    wcodrefaux             
            AND   lo-clipgm.nome-abrev    = showcli.nome-abrev
            AND   lo-clipgm.tipoforn   MATCHES(wtipoforn)
            AND  (lo-clipgm.data-pgm     LT showcli.data-pgm
               OR lo-clipgm.cod-pgm      LT showcli.cod-pgm
               OR lo-clipgm.tipo-pgm     LT showcli.tipo-pgm)
            AND   lo-clipgm.data-pgm     LE    a-dtpgm
            USE-INDEX ctrpcl NO-LOCK NO-ERROR.
        END.

      END.
    END.
  /*.....................................fim do LO-GLIPGM anterior...*/

  END.
/*...........................fim do LO-CLIPGM NA TABELA DE CLIENTES..*/


/*.............................inicio LO-RN12 NA TABELA DE CLIENTES..*/
IF wprv = "E"
AND wfa-operacao = "GMB" 
THEN DO:
  IF wcaller NE "S" THEN
  PUT SCREEN ROW 24 COLUMN 01 "monta lo-rn12 na TAB CLI".
         
  ASSIGN wgeri = ""
         wgerf = "".

  IF wqualpgm = "U" THEN DO:
    FIND LAST lo-rn12
        WHERE lo-rn12.itemcli = wcodrefaux
        AND   lo-rn12.geracao MATCHES(wgersel)
        USE-INDEX ig
        NO-LOCK NO-ERROR.
    END.

  IF wqualpgm = "D" THEN DO:
    FIND LAST lo-rn12
        WHERE lo-rn12.itemcli = wcodrefaux
        AND   lo-rn12.geracao LE wgerpedida
        AND   lo-rn12.geracao MATCHES(wgersel)
        USE-INDEX ig
        NO-LOCK NO-ERROR.
    END.

  IF AVAILABLE lo-rn12 THEN DO:
    ASSIGN wgeri = lo-rn12.geracao
           wgerf = wgeri.
    ASSIGN SUBSTRING(wgeri,07,06) = "000000"
           SUBSTRING(wgerf,07,06) = "999999".

    FOR EACH lo-rn12
       WHERE lo-rn12.itemcli  = wcodrefaux
       AND   lo-rn12.geracao GE wgeri
       AND   lo-rn12.geracao LE wgerf
       AND   lo-rn12.geracao MATCHES(wgersel)
       USE-INDEX gidh
       NO-LOCK:
               
      IF wcaller NE "S" THEN
      PUT SCREEN ROW 24 COLUMN 01 "tabela GMB " 
                                + STRING(lo-rn12.geracao)
                          + " " + STRING(wcodrefaux)
                                + FILL(" ",16).    

      ASSIGN wloclista = ""
             wlocabrev = "".

      IF LOOKUP(lo-rn12.locdestino,wloclist1) GT 0 THEN DO:
        ASSIGN wloclista = wloclist1
               wlocabrev = wlocabre1.  
        END.              
      IF LOOKUP(lo-rn12.locdestino,wloclist2) GT 0 THEN DO:
        ASSIGN wloclista = wloclist2
               wlocabrev = wlocabre2.  
        END.              
      IF LOOKUP(lo-rn12.locdestino,wloclist3) GT 0 THEN DO:
        ASSIGN wloclista = wloclist3
               wlocabrev = wlocabre3.  
        END.              



      /*-----------------------------inicio proc.25-08-2003------*/
      FIND FIRST lo-clidef
           WHERE lo-clidef.codrefsbp = wcodrefaux
           NO-LOCK NO-ERROR.
      IF AVAILABLE lo-clidef THEN DO:
        ASSIGN wlocabrev = lo-clidef.nome-abrev.
        END.
      /*-----------------------------fim de proc.25-08-2003------*/



      IF wlocabrev MATCHES(wcli) THEN DO:
                
        FIND FIRST showcli
             WHERE showcli.codarq     = "lo-rn12"
             AND   showcli.nome-abrev = wlocabrev
             NO-ERROR.
                     
        IF NOT AVAILABLE showcli THEN DO:
                    
          ASSIGN wseculo = "20".
          IF INTE(SUBSTRING(lo-rn12.geracao,01,02)) GT 50 THEN DO:
            ASSIGN wseculo = "19".
            END.
                    
          ASSIGN wdtx = DATE(INTE(SUBSTRING(lo-rn12.geracao,03,02)),
                             INTE(SUBSTRING(lo-rn12.geracao,05,02)),
                             INTE(STRING(wseculo)
                                + SUBSTRING(lo-rn12.geracao,01,02))).
          ASSIGN wnfx = 0.
          FIND LAST it-nota-fisc
              WHERE it-nota-fisc.it-codigo = witemaux
              AND   it-nota-fisc.nome-ab-cli = wlocabrev
              AND   it-nota-fisc.dt-cancela  = ?
              AND   it-nota-fisc.baixa-estoq = yes
              AND   it-nota-fisc.dt-emis-nota LT wdtx
              USE-INDEX ch-item-nota
              NO-LOCK
              NO-ERROR.
          IF AVAILABLE it-nota-fisc THEN DO:
            ASSIGN wnfx = INTE(it-nota-fisc.nr-nota-fis).
            END.

          ASSIGN wregcli = wregcli + 1.
          CREATE showcli.
          ASSIGN showcli.numreg     = wregcli
                 showcli.codarq     = "lo-rn12"
                 showcli.loclista   = wloclista
                 showcli.nome-abrev = wlocabrev
                 showcli.geri       = wgeri
                 showcli.gerf       = wgerf
                 showcli.data-pgm   = wdtx
                 showcli.cod-pgm    = SUBSTRING(lo-rn12.geracao,07,06)
                 showcli.tipo-pgm   = " "
                 showcli.nf         = wnfx
                 showcli.dtnf       = showcli.data-pgm - 1.
          END.   

        END.
      END.
    END.

  /*.....................................inicio LO-RN12 anterior...*/
  FOR EACH showcli:

    IF wqualpgm = "U" THEN DO:
      FIND LAST lo-rn12
          WHERE lo-rn12.itemcli  = wcodrefaux
          AND   lo-rn12.geracao LT showcli.geri
          AND   LOOKUP(lo-rn12.locdestino,showcli.loclista) GT 0
          AND   lo-rn12.geracao MATCHES(wgersel)
          USE-INDEX gid NO-LOCK NO-ERROR.
      END.

    IF wqualpgm = "D" THEN DO:
      FIND LAST lo-rn12
          WHERE lo-rn12.itemcli  = wcodrefaux
          AND   lo-rn12.geracao LT showcli.geri
          AND   lo-rn12.geracao LE a-gerpedida
          AND   LOOKUP(lo-rn12.locdestino,showcli.loclista) GT 0
          AND   lo-rn12.geracao MATCHES(wgersel)
          USE-INDEX gid NO-LOCK NO-ERROR.
      END.

    IF AVAILABLE lo-rn12 THEN DO:
      ASSIGN wgeri = lo-rn12.geracao
             wgerf = wgeri.
      ASSIGN SUBSTRING(wgeri,07,06) = "000000"
             SUBSTRING(wgerf,07,06) = "999999".

      IF wcaller NE "S" THEN
      PUT SCREEN ROW 24 COLUMN 01 "ger GMB anterior" 
                                + STRING(lo-rn12.geracao)
                          + " " + STRING(wcodrefaux)
                                + FILL(" ",10).    

      ASSIGN wseculo = "20".
      IF INTE(SUBSTRING(lo-rn12.geracao,01,02)) GT 50 THEN DO:
        ASSIGN wseculo = "19".
        END.
                    
      ASSIGN showcli.anome-abrev = showcli.nome-abrev
             showcli.ageri       = wgeri
             showcli.agerf       = wgerf
             showcli.adata-pgm   = 
                    DATE(INTE(SUBSTRING(lo-rn12.geracao,03,02)),
                         INTE(SUBSTRING(lo-rn12.geracao,05,02)),
                         INTE(STRING(wseculo)
                            + SUBSTRING(lo-rn12.geracao,01,02)))
             showcli.acod-pgm    = SUBSTRING(lo-rn12.geracao,07,06)
             showcli.atipo-pgm   = " "
             showcli.anf         = 0
             showcli.adtnf       = showcli.adata-pgm - 1.
      END.   

    END.
  /*.....................................fim do LO-RN12 anterior...*/

  END.
/*.............................fim do LO-RN12 NA TABELA DE CLIENTES..*/


/*.............................inicio LO-RN01 NA TABELA DE CLIENTES..*/
IF wprv = "P"
AND wfa-operacao = "GMB" 
THEN DO:
  IF wcaller NE "S" THEN
  PUT SCREEN ROW 24 COLUMN 01 "monta lo-rn01 na TAB CLI".
          
  ASSIGN wgeri = ""
         wgerf = ""
         wprogatual = "".
                 
  IF wqualpgm = "U" THEN DO:
    FIND FIRST lo-rn01
         WHERE lo-rn01.itemcli = wcodrefaux
         AND   lo-rn01.geracao MATCHES(wgersel)
         NO-LOCK
         NO-ERROR.
    IF NOT AVAILABLE lo-rn01 THEN NEXT.       

    FIND LAST lo-rn01
        WHERE lo-rn01.itemcli = wcodrefaux
        AND   lo-rn01.geracao MATCHES(wgersel)
        USE-INDEX gid
        NO-LOCK NO-ERROR.
    END.

  IF wqualpgm = "D" THEN DO:
    FIND LAST lo-rn01
        WHERE lo-rn01.itemcli      = wcodrefaux
        AND   lo-rn01.dtprogatual LE wdtpgm
        AND   lo-rn01.geracao MATCHES(wgersel)
        USE-INDEX gid
        NO-LOCK NO-ERROR.
    END.


  IF AVAILABLE lo-rn01 THEN DO:
    ASSIGN wgeri  = lo-rn01.geracao
           wgerf = wgeri.
    ASSIGN SUBSTRING(wgeri,07,06) = "000000"
           SUBSTRING(wgerf,07,06) = "999999".

    ASSIGN wprogatual = lo-rn01.progatual.

    FOR EACH lo-rn01
       WHERE lo-rn01.itemcli  = wcodrefaux
       AND   lo-rn01.geracao GE wgeri
       AND   lo-rn01.geracao LE wgerf
       AND   lo-rn01.progatual = wprogatual
       AND   lo-rn01.geracao MATCHES(wgersel)
       USE-INDEX gid
       NO-LOCK:
               
      IF wcaller NE "S" THEN
      PUT SCREEN ROW 24 COLUMN 01 "tabela GMB " 
                                + STRING(lo-rn01.geracao)
                          + " " + STRING(wcodrefaux)
                                + FILL(" ",16).    

      ASSIGN wlocabrev = "".

      IF lo-rn01.locdestino = "72664"
      OR lo-rn01.locdestino = "72668"
      OR lo-rn01.locdestino = "72669"
      OR lo-rn01.locdestino = "72677"
      THEN ASSIGN wlocabrev = "GMB SJC".
              
      IF lo-rn01.locdestino = "72667"
      OR lo-rn01.locdestino = "72671"
      THEN ASSIGN wlocabrev = "GMB-SCS".
              
      IF lo-rn01.locdestino = "72681"
      THEN ASSIGN wlocabrev = "GMB.SCS".

      IF wlocabrev MATCHES(wcli) THEN DO:
                
        FIND FIRST showcli
             WHERE showcli.codarq     = "lo-rn01"
             AND   showcli.nome-abrev = wlocabrev
             NO-ERROR.
                     
        IF NOT AVAILABLE showcli THEN DO:
                    
          ASSIGN wseculo = "20".
          IF INTE(SUBSTRING(lo-rn01.geracao,01,02)) GT 50 THEN DO:
            ASSIGN wseculo = "19".
            END.
                    
          ASSIGN wdtx = DATE(INTE(SUBSTRING(lo-rn01.geracao,03,02)),
                             INTE(SUBSTRING(lo-rn01.geracao,05,02)),
                             INTE(STRING(wseculo)
                                + SUBSTRING(lo-rn01.geracao,01,02))).
          ASSIGN wnfx = 0.
          FIND LAST it-nota-fisc
              WHERE it-nota-fisc.it-codigo = witemaux
              AND   it-nota-fisc.nome-ab-cli = wlocabrev
              AND   it-nota-fisc.dt-cancela  = ?
              AND   it-nota-fisc.baixa-estoq = yes
              AND   it-nota-fisc.dt-emis-nota LT wdtx
              USE-INDEX ch-item-nota
              NO-LOCK
              NO-ERROR.
          IF AVAILABLE it-nota-fisc THEN DO:
            ASSIGN wnfx = INTE(it-nota-fisc.nr-nota-fis).
            END.
              
          ASSIGN wregcli = wregcli + 1.
          CREATE showcli.
          ASSIGN showcli.numreg     = wregcli
                 showcli.codarq     = "lo-rn01"
                 showcli.nome-abrev = wlocabrev
                 showcli.geri       = wgeri
                 showcli.gerf       = wgerf
                 showcli.progatual  = lo-rn01.progatual
                 showcli.data-pgm   = wdtx
                 showcli.cod-pgm    = SUBSTRING(lo-rn01.geracao,07,06)
                 showcli.tipo-pgm   = " "
                 showcli.nf         = wnfx
                 showcli.dtnf       = showcli.data-pgm - 1.
          END.   

        IF LOOKUP(lo-rn01.locdestino,showcli.loclista) = 0
        THEN DO:
          IF LENGTH(showcli.loclista) GT 0 THEN DO:
            ASSIGN showcli.loclista
                 = showcli.loclista + ",".
            END.
          ASSIGN showcli.loclista
               = showcli.loclista + lo-rn01.locdestino.
          END.
                    
        END.
      END.
    END.

  /*.....................................inicio LO-RN01 anterior...*/
  FOR EACH showcli:
                 
    IF wqualpgm = "U" THEN DO:
      FIND FIRST lo-rn01
           WHERE lo-rn01.itemcli = wcodrefaux
           AND   lo-rn01.geracao MATCHES(wgersel)
           NO-LOCK
           NO-ERROR.
      IF NOT AVAILABLE lo-rn01 THEN NEXT.       

      FIND LAST lo-rn01
          WHERE lo-rn01.itemcli = wcodrefaux
          AND   lo-rn01.geracao LT showcli.geri
          AND   LOOKUP(lo-rn01.locdestino,showcli.loclista) GT 0
          AND   lo-rn01.geracao MATCHES(wgersel)
          USE-INDEX gid NO-LOCK NO-ERROR.
      END.

    IF wqualpgm = "D" THEN DO:
      FIND LAST lo-rn01
          WHERE lo-rn01.itemcli  = wcodrefaux
          AND   lo-rn01.geracao LT showcli.geri
          AND   LOOKUP(lo-rn01.locdestino,showcli.loclista) GT 0
          AND   lo-rn01.dtprogatual LE a-dtpgm
          AND   lo-rn01.geracao MATCHES(wgersel)
          USE-INDEX gid                 
          NO-LOCK NO-ERROR.
      END.


    IF AVAILABLE lo-rn01 THEN DO:
      ASSIGN wgeri  = lo-rn01.geracao
             wgerf = wgeri.
      ASSIGN SUBSTRING(wgeri,07,06) = "000000"
             SUBSTRING(wgerf,07,06) = "999999".

      IF wcaller NE "S" THEN
      PUT SCREEN ROW 24 COLUMN 01 "ger GMB anterior" 
                                + STRING(lo-rn01.geracao)
                          + " " + STRING(wcodrefaux)
                                + FILL(" ",5).    

      ASSIGN wseculo = "20".
      IF INTE(SUBSTRING(lo-rn01.geracao,01,02)) GT 50 THEN DO:
        ASSIGN wseculo = "19".
        END.
                    
      ASSIGN showcli.anome-abrev = showcli.nome-abrev
             showcli.ageri       = wgeri
             showcli.agerf       = wgerf
             showcli.aprogatual  = lo-rn01.progatual
             showcli.adata-pgm   = 
                    DATE(INTE(SUBSTRING(lo-rn01.geracao,03,02)),
                         INTE(SUBSTRING(lo-rn01.geracao,05,02)),
                         INTE(STRING(wseculo)
                            + SUBSTRING(lo-rn01.geracao,01,02)))
             showcli.acod-pgm    = SUBSTRING(lo-rn01.geracao,07,06)
             showcli.atipo-pgm   = " "
             showcli.anf         = 0
             showcli.adtnf       = showcli.adata-pgm - 1.
      END.   
    END.
  /*.....................................fim do LO-RN01 anterior...*/

  END.
/*.............................fim do LO-RN01 NA TABELA DE CLIENTES..*/


/*.............................inicio lo-jitlog NA TABELA DE CLIENTES..*/
IF wfa-operacao = "JIT" 
THEN DO:
  IF wcaller NE "S" THEN
  PUT SCREEN ROW 24 COLUMN 01 "monta lo-jitlog na TAB CLI".
          
  IF wqualpgm = "U" THEN DO:
    FIND FIRST lo-jitlog
         WHERE lo-jitlog.peca   = wcodrefaux
         AND   lo-jitlog.obs    = ""
         AND   lo-jitlog.dt-ac NE ?
         AND   lo-jitlog.dt-sd  = ?
         USE-INDEX ch-02
         NO-LOCK
         NO-ERROR.
    END.

  IF wqualpgm = "D" THEN DO:
    FIND FIRST lo-jitlog
         WHERE lo-jitlog.peca   = wcodrefaux
         AND   lo-jitlog.obs    = ""      
         AND   lo-jitlog.dt-ac GE wdtpgm
         USE-INDEX ch-02
         NO-LOCK NO-ERROR.
    END.


  IF AVAILABLE lo-jitlog THEN DO:

    IF wcaller NE "S" THEN
    PUT SCREEN ROW 24 COLUMN 01 "tabela JIT " 
                              + STRING(lo-jitlog.dt-ac)
                        + " " + STRING(wcodrefaux)
                              + FILL(" ",16).    

    ASSIGN wlocabrev = "VW SBC".

    IF wlocabrev MATCHES(wcli) THEN DO:
                    
      ASSIGN wdtx = TODAY.           
      IF wqualpgm = "D" THEN DO:
        ASSIGN wdtx = wdtpgm.
        END.

      ASSIGN wnfx = 0.
      FIND LAST it-nota-fisc
          WHERE it-nota-fisc.it-codigo = witemaux
          AND   it-nota-fisc.nome-ab-cli = wlocabrev
          AND   it-nota-fisc.dt-cancela  = ?
          AND   it-nota-fisc.baixa-estoq = yes
          AND   it-nota-fisc.dt-emis-nota LT wdtx            
          USE-INDEX ch-item-nota
          NO-LOCK
          NO-ERROR.
      IF AVAILABLE it-nota-fisc THEN DO:
        ASSIGN wnfx = INTE(it-nota-fisc.nr-nota-fis).
        END.

      ASSIGN wregcli = wregcli + 1.
      CREATE showcli.
      ASSIGN showcli.numreg     = wregcli
             showcli.codarq     = "lo-jitlog"
             showcli.nome-abrev = wlocabrev
             showcli.data-pgm   = wdtx             
             showcli.cod-pgm    = ""                                  
             showcli.tipo-pgm   = ""
             showcli.nf         = wnfx
             showcli.dtnf       = showcli.data-pgm - 1.
      END.   

    END.
  END.
/*.............................fim do lo-jitlog NA TABELA DE CLIENTES..*/

