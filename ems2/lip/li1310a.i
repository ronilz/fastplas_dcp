/* 22/02/21 - AJUSTE PARA NOVO LAYOUT TOTVS EM 22/02/21 - VALERIA */
PUT STREAM f-arq  UNFORMATTED
                                                                                                                                                                    /* |------------------------------------------------------------------------------- */
                                                                                                                                                                    /* | Ordem |                         Descri��o                       | Tamanho |    */
                                                                                                                                                                    /* |-------+---------------------------------------------------------+---------+-   */
STRING(movto-estoq.it-codigo)                                                                                                                 AT 001 FORMAT "X(16)" /* |    1  | C�digo do Item                                          |    16   |    */
FILL(" ",03)                                                                                                                                  AT 017 FORMAT "X(03)" /* |    2  | Branco                                                  |     3   |    */ 
STRING(movto-estoq.cod-depos)                                                                                                                 AT 020 FORMAT "X(03)" /* |    3  | C�digo do Dep�sito                                      |     3   |    */  
STRING(movto-estoq.cod-localiz)                                                                                                               AT 023 FORMAT "X(10)" /* |    4  | Localiza��o at� 10 caracteres                           |    10   |    */ 
STRING(movto-estoq.lote)                                                                                                                      AT 033 FORMAT "X(10)" /* |    5  | Lote/Nr. de S�rie at� 10 caracteres                     |    10   |    */ 
STRING(MONTH(tt-param.wgdt),"99") + "/" + STRING(DAY(tt-param.wgdt),"99") + "/" + STRING(YEAR(tt-param.wgdt),"9999")                          AT 043 FORMAT "X(10)" /* |    6  | Data de Validade do Lote                                |    10   |    */ 
STRING(movto-estoq.referencia,"x(40)")                                                                                                        AT 053 FORMAT "X(40)" /* |    7  | Refer�ncia                                              |    40   |    */ 
STRING(movto-estoq.numero-ordem,">>>>>>>>>>9")                                                                                                AT 093 FORMAT "X(11)" /* |    8  | N�mero da Ordem                                         |    11   |    */  
STRING(wct)                                                                                                                                   AT 104 FORMAT "x(20)" /* |    9  | Conta                                                   |    20   |    */  
STRING(wsc)                                                                                                                                   AT 124 FORMAT "x(20)" /* |   10  | Centro de Custo                                         |    20   |    */  
STRING(tt-param.wgserie)                                                                                                                      AT 144 FORMAT "x(05)" /* |   11  | S�rie Documento                                         |     5   |    */ 
STRING(movto-estoq.nro-docto)                                                                                                                 AT 149 FORMAT "x(16)" /* |   12  | N�mero do Documento                                     |    16   |    */  
STRING(movto-estoq.tipo-trans,">9")                                                                                                           AT 165 FORMAT "x(02)" /* |   13  | Tipo da Transa��o                                       |     2   |    */                                                                                                                                                       
SUBSTRING(STRING(movto-estoq.quantidade,"->>>>>>>>9.9999"),01,10)     +  SUBSTRING(STRING(movto-estoq.quantidade,"->>>>>>>>9.9999"),12,04)    AT 167 FORMAT "x(14)" /* |   14  | Quantidade                                              |    14   |    */                                    
STRING(item.un)                                                                                                                               AT 181 FORMAT "x(02)" /* |   15  | Unidade de Medida                                       |     2   |    */  
SUBSTRING(STRING(movto-estoq.valor-mat-m[1],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-mat-m[1],">>>>>>>>>>>9.99"),14,02) AT 183 FORMAT "X(14)" /* |   16  | Valor Mat�ria Prima (moeda 1)                           |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-mat-m[2],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-mat-m[2],">>>>>>>>>>>9.99"),14,02) AT 197 FORMAT "X(14)" /* |   17  | Valor Mat�ria Prima (moeda 2)                           |    14   |    */ 
SUBSTRING(STRING(movto-estoq.valor-mat-m[3],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-mat-m[3],">>>>>>>>>>>9.99"),14,02) AT 211 FORMAT "X(14)" /* |   18  | Valor Mat�ria Prima (moeda 3)                           |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-mob-m[1],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-mob-m[1],">>>>>>>>>>>9.99"),14,02) AT 225 FORMAT "X(14)" /* |   19  | Valor M�o de Obra (moeda 1)                             |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-mob-m[2],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-mob-m[2],">>>>>>>>>>>9.99"),14,02) AT 239 FORMAT "X(14)" /* |   20  | Valor M�o de Obra (moeda 2)                             |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-mob-m[3],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-mob-m[3],">>>>>>>>>>>9.99"),14,02) AT 253 FORMAT "X(14)" /* |   21  | Valor M�o de Obra (moeda 3)                             |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-ggf-m[1],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-ggf-m[1],">>>>>>>>>>>9.99"),14,02) AT 267 FORMAT "X(14)" /* |   22  | Gastos Gerais de Fabrica��o (moeda 1)                   |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-ggf-m[2],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-ggf-m[2],">>>>>>>>>>>9.99"),14,02) AT 281 FORMAT "X(14)" /* |   23  | Gastos Gerais de Fabrica��o (moeda 2)                   |    14   |    */  
SUBSTRING(STRING(movto-estoq.valor-ggf-m[3],">>>>>>>>>>>9.99"),01,12) + SUBSTRING(STRING(movto-estoq.valor-ggf-m[3],">>>>>>>>>>>9.99"),14,02) AT 295 FORMAT "x(14)" /* |   24  | Gastos Gerais de Fabrica��o (moeda 3)                   |    14   |    */ 
STRING(movto-estoq.cod-refer)                                                                                                                 AT 309 FORMAT "x(08)" /* |   25  | C�digo de Refer�ncia                                    |     8   |    */  
FILL(" ",20)                                                                                                                                  AT 317 FORMAT "x(20)" /* |   26  | Conta de Aplica��o                                      |    20   |    */  
FILL(" ",20)                                                                                                                                  AT 337 FORMAT "x(20)" /* |   27  | Centro de Custo de Aplica��o                            |    20   |    */ 
STRING(tt-param.wgsn,"S/N")                                                                                                                   AT 357 FORMAT "x(01)" /* |   28  | Atualiza a Data e Pre�o da �ltima Entrada               |     1   |    */  
STRING(movto-estoq.cod-emitente,">>>>>>>>9")                                                                                                  AT 358 FORMAT "x(09)" /* |   29  | C�digo do Emitente                                      |     9   |    */ 
STRING(MONTH(tt-param.wgdt),"99")  + "/" + STRING(DAY(tt-param.wgdt),"99")    + "/" + STRING(YEAR(tt-param.wgdt),"9999")                      AT 367 FORMAT "x(10)" /* |   30  | Data da Transa��o                                       |    10   |    */  
FILL(" ",03)                                                                                                                                  AT 377 FORMAT "X(03)" /* |   31  | Unidade Neg�cio                                         |     3   |    */  
FILL(" ",03)                                                                                                                                  AT 380 FORMAT "X(03)" /* |   32  | Unidade Neg�cio Aplica��o                               |     3   |    */  
STRING(movto-estoq.cod-estabel)                                                                                                               AT 383 FORMAT "X(05)" /* |   33  | Estabelecimento                                         |     5   |    */  
FILL(" ",05)                                                                                                                                  AT 388 FORMAT "X(05)" /* |   34  | Opera��o da Ordem                                       |     5   |    */  
STRING(movto-estoq.cod-localiz,"x(20)")                                                                                                       AT 393 FORMAT "X(20)" /* |   35  | Localiza��o  at� 20 caracteres                          |    20   |    */ 
STRING(movto-estoq.cod-lote-fabrican,"x(40)")                                                                                                 AT 413 FORMAT "X(40)" /* |   36  | Lote/Nr. de S�rie at� 40 caracteres                     |    40   |    */ 
SKIP.                                                                                                                                                               /* +--------------------------------------------------------------------------------*/
                                                                                                                                                                 
/* NOVO LAYOUT ENVIADO PELA TOTVS - 23/02/21 - AJUSTE SOL. JOSE LUIZ, CAMPO REFERENCIA AUMENTO DE 10 PARA 40 */

/*|    1  | C�digo do Item                                          |    16   |     1  |    16   | Caracter |          |     Sim     |*/
/*|    2  | Branco                                                  |     3   |    17  |    19   | Caracter |          |     Sim     |*/ 
/*|    3  | C�digo do Dep�sito                                      |     3   |    20  |    22   | Caracter |          |     Sim     |*/
/*|    4  | Localiza��o at� 10 caracteres                           |    10   |    23  |    32   | Caracter |          |     Sim     |*/
/*|    5  | Lote/Nr. de S�rie at� 10 caracteres                     |    10   |    33  |    42   | Caracter |          |     Sim     |*/
/*|    6  | Data de Validade do Lote                                |    10   |    43  |    52   | Data     |          |     Sim     |*/
/*|       | Formato mm/dd/aaaa                                      |         |        |         |          |          |             |*/
/*|       | Onde    mm   = Mes                                      |         |        |         |          |          |             |*/
/*|       |         dd   = Dia                                      |         |        |         |          |          |             |*/
/*|       |         aaaa = Ano                                      |         |        |         |          |          |             |*/
/*|    7  | Refer�ncia                                              |    40   |    53  |    92   | Caracter |          |     Sim     |*/
/*|    8  | N�mero da Ordem                                         |    11   |    93  |   103   | Inteiro  |          |     Sim     |*/
/*|    9  | Conta                                                   |    20   |   104  |   123   | Caracter |          |     Sim     |*/
/*|   10  | Centro de Custo                                         |    20   |   124  |   143   | Caracter |          |     Sim     |*/
/*|   11  | S�rie Documento                                         |     5   |   144  |   148   | Caracter |          |     Sim     |*/
/*|   12  | N�mero do Documento                                     |    16   |   149  |   164   | Caracter |          |     Sim     |*/
/*|   13  | Tipo da Transa��o                                       |     2   |   165  |   166   | Inteiro  |          |     Sim     |*/
/*|       | Onde    1 - Entrada  e  2 - Saida                       |         |        |         |          |          |             |*/
/*|   14  | Quantidade                                              |    14   |   167  |   180   | Decimal  |     4    |     Sim     |*/
/*|   15  | Unidade de Medida                                       |     2   |   181  |   182   | Caracter |          |     Sim     |*/
/*|   16  | Valor Mat�ria Prima (moeda 1)                           |    14   |   183  |   196   | Decimal  |     2    |     Sim     |*/
/*|   17  | Valor Mat�ria Prima (moeda 2)                           |    14   |   197  |   210   | Decimal  |     2    |     Sim     |*/
/*|   18  | Valor Mat�ria Prima (moeda 3)                           |    14   |   211  |   224   | Decimal  |     2    |     Sim     |*/
/*|   19  | Valor M�o de Obra (moeda 1)                             |    14   |   225  |   238   | Decimal  |     2    |     Sim     |*/
/*|   20  | Valor M�o de Obra (moeda 2)                             |    14   |   239  |   252   | Decimal  |     2    |     Sim     |*/
/*|   21  | Valor M�o de Obra (moeda 3)                             |    14   |   253  |   266   | Decimal  |     2    |     Sim     |*/
/*|   22  | Gastos Gerais de Fabrica��o (moeda 1)                   |    14   |   267  |   280   | Decimal  |     2    |     Sim     |*/
/*|   23  | Gastos Gerais de Fabrica��o (moeda 2)                   |    14   |   281  |   294   | Decimal  |     2    |     Sim     |*/
/*|   24  | Gastos Gerais de Fabrica��o (moeda 3)                   |    14   |   295  |   308   | Decimal  |     2    |     Sim     |*/
/*|   25  | C�digo de Refer�ncia                                    |     8   |   309  |   316   | Caracter |          |     Sim     |*/
/*|   26  | Conta de Aplica��o                                      |    20   |   317  |   336   | Caracter |          |     Nao     |*/
/*|   27  | Centro de Custo de Aplica��o                            |    20   |   337  |   356   | Caracter |          |     Nao     |*/
/*|   28  | Atualiza a Data e Pre�o da �ltima Entrada               |     1   |   357  |   357   | Logico   |          |     Sim     |*/
/*|   29  | C�digo do Emitente                                      |     9   |   358  |   366   | Inteiro  |          |     Sim     |*/
/*|   30  | Data da Transa��o                                       |    10   |   367  |   376   | Data     |          |     Nao     |*/
/*|       | Formato mm/dd/aaaa                                      |         |        |         |          |          |             |*/
/*|       | Onde    mm   = Mes                                      |         |        |         |          |          |             |*/
/*|       |         dd   = Dia                                      |         |        |         |          |          |             |*/
/*|       |         aaaa = Ano                                      |         |        |         |          |          |             |*/
/*|   31  | Unidade Neg�cio                                         |     3   |   377  |   379   | Caracter |          |     Sim     |*/
/*|   32  | Unidade Neg�cio Aplica��o                               |     3   |   380  |   382   | Caracter |          |     Sim     |*/
/*|   33  | Estabelecimento                                         |     5   |   383  |   387   | Caracter |          |     Nao     |*/
/*|   34  | Opera��o da Ordem                                       |     5   |   388  |   392   | Inteiro  |          |     Nao     |*/
/*|   35  | Localiza��o  at� 20 caracteres                          |    20   |   393  |   412   | Caracter |          |     Sim     |*/
/*|   36  | Lote/Nr. de S�rie at� 40 caracteres                     |    40   |   413  |   462   | Caracter |          |     Sim     |*/                                                                                                                                                                
                                                                                                                                                                 
                                                                                                                                                                 
                                                                                                                                                                 
                                                                                                                                                                 
                                                                                                                                                                 



                                                                 
                                                                                                                                                                                                                                                                               
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                             
