/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/

def temp-table tt-ficha no-undo
    field mes         as integer
    field sequencia   as integer
    field mes-ano     as integer format ">>>9"
    field ano         as integer format "9" init 1
    field fator       as decimal format ">9.9999"
    field valor       as decimal format ">>>,>>>,>>9.99"
    field cod-estabel like mov-ciap.cod-estabel 
    index ch-sequencia sequencia
    index ch-mes       mes.
