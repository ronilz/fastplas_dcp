/*****************************************************************************
**
**    CD9500.I - Variaveis para impressao do cabecalho padrao
**
*****************************************************************************/

define var c-empresa      as character format "x(40)".
define var c-titulo-relat as character format "x(50)".
define var c-sistema      as character format "x(25)".
define var i-numper-x     as integer  format "ZZ".
define var da-iniper-x    as date format "99/99/9999".
define var da-fimper-x    as date format "99/99/9999".
define var c-rodape       as character.



/* CD9500.I */
