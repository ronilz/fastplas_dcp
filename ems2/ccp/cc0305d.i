/******************************************************************************
**
** Programa: cc0305d.i
**
** Data....: junho de 2000
**
** Autor...: DATASUL S.A.
**
** Objetivo: Cria��o das temp-tables para altera��o de pedidos de compras B2B
**
******************************************************************************/

def temp-table tt-pedido-compr no-undo
    field num-pedido        like pedido-compr.num-pedido        /*Order.OrderIdentifier*/
    field comentarios       like pedido-compr.comentarios       /*Order.Comment*/
    field l-tipo-ped        like pedido-compr.l-tipo-ped        /*Order.OrderTypeCode*/
    field forma-pagto       as char                             /*Order.PaymentMethod*/
    field cod-cond-pag      like pedido-compr.cod-cond-pag      /*PaymentPlan.PaymentCode*/
    field cod-estabel-cob   like estabelec.cod-estabel          /*BillTo.LegacyLocationIdentifier*/
    field endereco1-cob     like estabelec.endereco             /*BillTo.AddressLine1*/
    field endereco2-cob     like estabelec.endereco             /*BillTo.AddressLine2*/
    field endereco3-cob     like estabelec.endereco             /*BillTo.AddressLine3*/
    field cidade-cob        like estabelec.cidade               /*BillTo.CityName*/
    field bairro-cob        like estabelec.bairro               /*BillTo.DistrictName*/
    field estado-cob        like estabelec.estado               /*BillTo.StateCode*/
    field cep-cob           like estabelec.cep                  /*BillTo.NationalPostalCode*/
    field regiao-cob        like repres.nome-ab-reg             /*BillTo.RegionName*/
    field pais-cob          like estabelec.pais                 /*BillTo.LegacyCountryCode*/
    field contato-cob       like cotacao-item.contato           /*BillTo.ContactInformation.ContactName*/
    field e-mail-cob        like cont-emit.e-mail               /*BillTo.ContactInformation.EmailAddress*/
    field telefone-cob      like cont-emit.telefone             /*BillTo.ContactInformation.TelephoneNumber*/
    field cod-estabel-orig  like estabelec.cod-estabel          /*FromRole.LegacyLocationIdentifier*/
    field endereco1-orig    like estabelec.endereco             /*FromRole.AddressLine1*/
    field endereco2-orig    like estabelec.endereco             /*FromRole.AddressLine2*/
    field endereco3-orig    like estabelec.endereco             /*FromRole.AddressLine3*/
    field cidade-orig       like estabelec.cidade               /*FromRole.CityName*/
    field bairro-orig       like estabelec.bairro               /*FromRole.DistrictName*/
    field estado-orig       like estabelec.estado               /*FromRole.StateCode*/
    field cep-orig          like estabelec.cep                  /*FromRole.NationalPostalCode*/
    field regiao-orig       like repres.nome-ab-reg             /*FromRole.RegionName*/
    field pais-orig         like estabelec.pais                 /*FromRole.LegacyCountryCode*/
    field contato-orig      like cont-emit.nome                 /*FromRole.ContactInformation.ContactName*/
    field e-mail-orig       like cont-emit.e-mail               /*FromRole.ContactInformation.EmailAddress*/
    field telefone-orig     like cont-emit.telefone             /*FromRole.ContactInformation.TelephoneNumber*/
    field cod-emitente-dest like emitente.cod-emitente          /*ToRole.LegacyLocationIdentifier*/
    field endereco1-dest    like emitente.endereco              /*ToRole.AddressLine1*/
    field endereco2-dest    like emitente.endereco              /*ToRole.AddressLine2*/
    field endereco3-dest    like emitente.endereco              /*ToRole.AddressLine3*/
    field cidade-dest       like emitente.cidade                /*ToRole.CityName*/
    field bairro-dest       like emitente.bairro                /*ToRole.DistrictName*/
    field estado-dest       like emitente.estado                /*ToRole.StateCode*/
    field cep-dest          like emitente.cep                   /*ToRole.NationalPostalCode*/
    field regiao-dest       as character                        /*ToRole.RegionName*/
    field pais-dest         like emitente.pais                  /*ToRole.LegacyCountryCode*/
    field contato-dest      like cont-emit.nome                 /*ToRole.ContactInformation.ContactName*/
    field e-mail-dest       like cont-emit.e-mail               /*ToRole.ContactInformation.EmailAddress*/
    field telefone-dest     like cont-emit.telefone             /*ToRole.ContactInformation.TelephoneNumber*/
    field changecode        as character                        /*OrderUpdate.GlobalOrderChangeCode*/   
    index num-pedido is primary unique
    num-pedido ascending.

def temp-table tt-cond-especif no-undo like cond-especif.
/*    field num-pedido        like cond-especif.num-pedido
    field data-pagto        like cond-especif.data-pagto extent 0      /*Payment.DueDate*/
    field perc-pagto        like cond-especif.perc-pagto extent 0      /*Payment.Percent*/
    field valor-parcela     as decimal                                 /*Payment.MonetaryAmmount.Ammount*/
    index num-pedido is primary unique
    num-pedido ascending.*/

def temp-table tt-ordem-compra no-undo
    field num-pedido        like ordem-compra.num-pedido
    field numero-ordem      like ordem-compra.numero-ordem
    field seq-item          as integer                          /*ProductLineItem.POSequentialIdentification*/
    field it-codigo         like ordem-compra.it-codigo         /*ProductLineItem.LegacyProductIdentifier*/
    field descricao         like item.desc-item                 /*ProductLineItem.ProductDescription*/
    field referencia        as char                             /*ProductLineItem.LegacyProductReference*/
    field preco-unit        like ordem-compra.preco-unit        /*ProductLineItem.MonetaryAmmount.Ammount*/
    field pre-unit-for      like ordem-compra.pre-unit-for
    field mo-codigo         like ordem-compra.mo-codigo         /*ProductLineItem.MonetaryAmmount.LegacyCurrencyCode*/
    index numero-ordem is primary unique
    numero-ordem ascending
    index num-pedido
    num-pedido   ascending
    numero-ordem ascending.

def temp-table tt-despesas-ordem no-undo                           /* Despesas a nivel de item */
    field numero-ordem  like ordem-compra.numero-ordem
    field desc-despesa  as char                             /*OrderItemValue.ValuesIdentifier*/
    field perc-despesa  as decimal                          /*OrderItemValue.Percent*/
    field vl-despesa    as decimal                          /*OrderItemValue.MonetaryAmmount.Ammount*/
    field moeda-despesa like ordem-compra.mo-codigo         /*OrderItemValue.MonetaryAmmount.LegacyCurrencyCode*/
    index desp-ordem is primary unique
    numero-ordem ascending
    desc-despesa ascending.

def temp-table tt-prazo-compra no-undo
    field numero-ordem        like prazo-compra.numero-ordem
    field parcela             like prazo-compra.parcela         /*ProductShip.POLineNumber*/
    field cod-fornec-embarque like pedido-compr.cod-emitente    /*ProductShip.LegacyLocationIdentifier*/
    field data-entrega        like prazo-compra.data-entrega    /*ProductShip.RequestDeliveryDate*/
    field frete               as char format "x(3)"             /*ProductShip.LegacyShipmentTermsCode*/
    field via-transp          like pedido-compr.via-transp      /*ProductShip.LegacyShippingMethodCode*/
    field narrativa           like ordem-compra.narrativa       /*ProductShip.Comment*/
    field quantidade          like prazo-compra.quantidade      /*QuantityAmmount.Quantity*/
    field un                  like prazo-compra.un              /*QuantityAmmount.LegacyUnitOfMeasure*/
    field cod-estabel-ent     like estabelec.cod-estabel        /*DeliveryTo.LegacyLocationIdentifier*/
    field endereco1-ent       like estabelec.endereco           /*DeliveryTo.AddressLine1*/
    field endereco2-ent       like estabelec.endereco           /*DeliveryTo.AddressLine2*/
    field endereco3-ent       like estabelec.endereco           /*DeliveryTo.AddressLine3*/
    field cidade-ent          like estabelec.cidade             /*DeliveryTo.CityName*/
    field bairro-ent          like estabelec.bairro             /*DeliveryTo.DistrictName*/
    field estado-ent          like estabelec.estado             /*DeliveryTo.StateCode*/
    field cep-ent             like estabelec.cep                /*DeliveryTo.NationalPostalCode*/
    field regiao-ent          like repres.nome-ab-reg           /*DeliveryTo.RegionName*/
    field pais-ent            like estabelec.pais               /*DeliveryTo.LegacyCountryCode*/
    field nr-requisicao       like ordem-compra.nr-requisicao   /*Quote.QuoteIdentifier*/
    field cod-transp          like pedido-compr.cod-transp      /*CarrierInformation.LegacyLocationIdentifier*/
    field nome-transp         like transporte.nome-abrev        /*CarrierInformation.CarrierName*/
    field contato-transp      like cont-tran.nome               /*CarrierInformation.ContactInformation.ContactName*/
    field e-mail-transp       like cont-tran.e-mail             /*CarrierInformation.ContactInformation.EmailAddress*/
    field telefone-transp     like cont-tran.telefone           /*CarrierInformation.ContactInformation.TelephoneNumber*/
    index numero-ordem is primary unique
    numero-ordem ascending
    parcela      ascending.

/* cc0305d.i */
