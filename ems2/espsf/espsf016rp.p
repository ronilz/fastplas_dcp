/* exporta��p espsf011 */ 
/* valeria - 09/05/11*/
/* inclusao - 20/02/18 - chave de acesso inserida no registro ae8 posi��o 66 - conf.solicita��o jose luiz/alessandro */
/* include de controle de vers�o */
/*06/05/19 - log automatico - sol. jose luiz */
/*11/03/20 - conf. sol alessandro expedicao e fat - foram incluidos natureza 513MBB E 594c2 - fornd 2261 sbc*/
/* no caso   IF SUBSTRING(ITEM.codigo-refer,1,1) = "A" THEN ASSIGN witem = SUBSTRING(codigo-refer,1,11) + FILL(" ",6) + SUBSTRING(codigo-refer,14,4). 
inserido espaco de 6 digitos em branco para atender MBB conf. sol. jose luiz */
/* 20/08/21 - inserida nat. 5999D2 - CONF. SOL. FABIANO - ref. devolucao MBB*/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF016RP 1.00.00.000}
 

/* defini��o das temp-tables para recebimento de par�metros */
/* define temp-table tt-param         */
/*         field destino   as integer */
/*     field arq-destino   as char    */
/*     field arq-entrada   as char    */
/*     field usuario       as char    */
/*     field data-exec as date        */
/*     field hora-exec as integer     */


    define temp-table tt-param
    field destino          as integer
    field arq-destino      as char
    field arq-entrada        as char
    field usuario          as char
    field data-exec        as date
    field hora-exec        as INTEGER
    FIELD wcod-est         AS CHAR FORMAT "X(03)"      
    FIELD wcod-ini         AS INTEGER FORMAT ">>>>>>>>9"          
    FIELD wdt-ini          AS DATE FORMAT "99/99/9999"          
    FIELD wdt-fim          AS DATE FORMAT "99/99/9999"          
    FIELD wnf-ini          AS CHAR FORMAT "X(16)"         
    FIELD wnf-fim          AS CHAR FORMAT "X(16)"           
    FIELD wserie           AS CHAR FORMAT "X(5)"
    FIELD wrel-log         AS CHAR FORMAT "X(40)".

/* Local Variable Definitions --- defini��es valeria - exporta��o        */
DEFINE VARIABLE wemp        LIKE estabelec.nome.
DEFINE VARIABLE wqt-it     AS   INTEGER FORMAT "999".
DEFINE VARIABLE wtot-nf    AS CHAR FORMAT "X(17)".
DEFINE VARIABLE wdesenho   AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcgccli    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcgcfor    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcont      AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont1     AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont2     AS INTEGER FORMAT "99".
DEFINE VARIABLE wclass     AS CHAR FORMAT "X(10)".
DEFINE VARIABLE wdif       AS INTE FORMAT 99.
DEFINE VARIABLE wdif1      AS INTE FORMAT 99.
DEFINE VARIABLE wabrecon   AS CHAR FORMAT "X(20)".
DEFINE STREAM   w-log.
DEFINE VARIABLE wfab       AS INTEGER FORMAT "999".

/* codigo item e numero pedido */
DEFINE VARIABLE wconta     AS INTEGER FORMAT 99.
DEFINE VARIABLE wconta1    AS INTEGER FORMAT 99.
DEFINE VARIABLE wnarrativa LIKE item-cli.narrativa[1].
DEFINE VARIABLE wwitem     AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wwpedido   AS CHAR FORMAT "X(12)".
DEFINE VARIABLE wauxped    AS CHAR FORMAT "X(12)".
DEFINE VARIABLE wauxcont   AS INTE FORMAT "99".
DEFINE VARIABLE wvenc      AS CHAR FORMAT "X(06)" COLUMN-LABEL "Venc".

DEFINE VARIABLE wtot-itens            AS INTEGER.
DEFINE VARIABLE wun-cond              AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wqt-cond              AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wun-mov               AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wdent-embal           AS CHAR FORMAT "X(20)".
DEFINE VARIABLE wpc-embal             AS INTEGER FORMAT "99999999".
DEFINE VARIABLE wnf-embal             AS INTEGER FORMAT "999999".
DEFINE VARIABLE wserie-ori            AS CHAR FORMAT "X(04)".
DEFINE VARIABLE wdt-nf-for            AS INTEGER FORMAT "999999".
DEFINE VARIABLE wqt-embals            AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wae5-espaco           AS CHAR FORMAT "X(03)".

DEFINE VARIABLE wqt-registro          AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wtot-valores          LIKE it-nota-fisc.vl-preuni.
DEFINE VARIABLE wcont-item            AS INTEGER FORMAT "99".
DEFINE VARIABLE witem                 AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wseq-it               AS INTEGER FORMAT "999".
DEFINE VARIABLE wtam-item             AS INTEGER FORMAT "99".

DEFINE BUFFER   b-it-auxiliar        FOR it-nota-fisc.
DEFINE VARIABLE wespaco              AS CHAR FORMAT "x(93)".
DEFINE VARIABLE wreceptor            AS CHAR FORMAT "x(25)".

/* CAMPOS IPI  - 21/07/11 - LOGICA DEFINADA PELO IDELCIO */
DEFINE VARIABLE waliquota-ipi LIKE it-nota-fisc.aliquota-ipi.
DEFINE VARIABLE wvl-ipi-it    LIKE it-nota-fisc.vl-ipi-it.


/* campo remessa */
DEFINE VARIABLE wnf-remessa AS INTEGER FORMAT "999999"     COLUMN-LABEL "NF Rem".
DEFINE VARIABLE wdata-remessa     AS CHAR FORMAT "X(06)"   COLUMN-LABEL "DT Rem".
DEFINE VARIABLE wserie-remessa    AS CHAR FORMAT "X(04)"   COLUMN-LABEL "SerR".
/* 20/02/18 - sol. jose luiz nota remessa */
DEFINE VARIABLE wchave-remessa    LIKE  docum-est.cod-chave-aces-nf-eletro.



/* COD TIPO DE FORNECIMENTO - AE2 - POS 108) */
define variable wtipo-forn        AS CHAR FORMAT "X(01)".
DEFINE VARIABLE wcampos-ae4       AS CHAR FORMAT "X(19)".
DEFINE VARIABLE wnum-kamban       AS CHAR FORMAT "X(12)"     INITIAL " ".
DEFINE VARIABLE wdata-kamban      AS CHAR FORMAT "X(06)"     INITIAL "000000".
DEFINE VARIABLE wqt-embal-chamada AS CHAR FORMAT "999999999" INITIAL "000000000".
DEFINE VARIABLE wqt-it-chamada    AS CHAR FORMAT "999999999" INITIAL "000000000".
DEFINE VARIABLE wespaco-ae9       AS CHAR FORMAT "X(89)".
DEFINE VARIABLE wespaco-ae8       AS CHAR FORMAT "X(106)".

/* DEFINE BUFFER*/
DEFINE BUFFER wdoc-fiscal FOR doc-fiscal.

/* defini��es valeria - exporta��o */
DEFINE STREAM wlog-automa.


/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR FORMAT "x(40)" NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.




def temp-table tt-raw-digita
    	field raw-digita	as raw.
/* recebimento de par�metros */
def input parameter raw-param as raw no-undo. 
def input parameter TABLE for tt-raw-digita.   

create tt-param.
raw-transfer raw-param to tt-param.

/* include padr�o para vari�veis para o log  */
{include/i-rpvar.i}
/* defini��o de vari�veis e streams */
def stream s-exp.
def var h-acomp as handle no-undo.

/* defini��o de frames do log */
/* include padr�o para output de log */
{include/i-rpout.i &STREAM="stream str-rp" &TOFILE=tt-param.arq-destino}
/* include com a defini��o da frame de cabe�alho e rodap� */
{include/i-rpcab.i &STREAM="str-rp"}
/* bloco principal do programa */
assign	c-programa 	= "ESPSF016RP"
c-versao	= "1.00"
c-revisao	= ".00.000"
c-empresa	= /*"Empresa Teste"*/ wemp
c-sistema	= 'Sports'
c-titulo-relat = "Listagem Log Gera��o ASN MBB - LAYOUT 004.15".
view stream str-rp frame f-cabec.
view stream str-rp frame f-rodape.
run utp/ut-acomp.p persistent set h-acomp.
{utp/ut-liter.i Exportando *}

run pi-inicializar in h-acomp (input RETURN-VALUE).
/* define a sa�da para o arquivo de sa�da informando na p�gina de par�metros */

OUTPUT STREAM s-exp  TO value(tt-param.arq-entrada).

/*29/04/19 - novo rel.log */
OUTPUT STREAM wlog-automa TO VALUE(tt-param.wrel-log) PAGE-SIZE 64 CONVERT TARGET "ISO8859-1".

/* mostra acentos e cedilha
OUTPUT STREAM s-exp  TO value(tt-param.arq-entrada) CONVERT TARGET "ISO8859-1".
*/
/* bloco principal do programa */

RUN roda_exportacao.

output stream s-exp close.
OUTPUT STREAM wlog-automa CLOSE.

/*fechamento do output do log */
{include/i-rpclo.i &STREAM="stream str-rp"}
run pi-finalizar in h-acomp. 
return "Ok":U.

/* procedure roda exporta��o */
PROCEDURE roda_exportacao.

 /* AVISO */
ASSIGN  wdesenho   = FILL(" ",30)
        wtot-itens    = 0
        wqt-registro  = 0
        wtot-valores  = 0
        wcont-item    = 0
        witem         = ""
        wseq-it       = 0 
        wtam-item     = 0
        waliquota-ipi = 0 
        wvl-ipi-it    = 0
        wvenc         = " "
        wtot-valores  = 0.

       
 
  /* Relatorio de Consistencia */
FORM
    nota-fiscal.nr-nota-fis     COLUMN-LABEL "NF"        FORMAT "X(10)"
    wseq-it                     COLUMN-LABEL "Seq.It"    FORMAT "99999"
    nota-fiscal.cod-emitente    COLUMN-LABEL "Emitente"
    nota-fiscal.dt-emis-nota    COLUMN-LABEL "Dt.nf"
    it-nota-fisc.it-codigo      COLUMN-LABEL "Item"            FORMAT "X(16)"
    witem                       COLUMN-LABEL "Codigo Cliente"  FORMAT "X(20)"
    it-nota-fisc.vl-preuni      COLUMN-LABEL "Preco unit"
    nota-fiscal.vl-tot-nota     COLUMN-LABEL "Vr.Total NF"
    it-nota-fisc.qt-faturada[1] COLUMN-LABEL "Qt.Fat."
    wwpedido                    COLUMN-LABEL "Pedido"
    estabelec.cgc               COLUMN-LABEL "CGC FASTPLAS"
    emitente.cgc                COLUMN-LABEL "CGC MBB"
    it-nota-fisc.class-fiscal   COLUMN-LABEL "Classifica��o"
    nota-fiscal.nat-operacao    COLUMN-LABEL "Nat.Oper"
 
   WITH STREAM-IO WIDTH 450 FRAME f-log DOWN.
/* FORM LOG AUTOMATICO  - 29/04/19*/
FORM
    nota-fiscal.nr-nota-fis     COLUMN-LABEL "NF"        FORMAT "X(10)"
    wseq-it                     COLUMN-LABEL "Seq.It"    FORMAT "99999"
    nota-fiscal.cod-emitente    COLUMN-LABEL "Emitente"
    nota-fiscal.dt-emis-nota    COLUMN-LABEL "Dt.nf"
    it-nota-fisc.it-codigo      COLUMN-LABEL "Item"            FORMAT "X(16)"
    witem                       COLUMN-LABEL "Codigo Cliente"  FORMAT "X(20)"
    it-nota-fisc.vl-preuni      COLUMN-LABEL "Preco unit"
    nota-fiscal.vl-tot-nota     COLUMN-LABEL "Vr.Total NF"
    it-nota-fisc.qt-faturada[1] COLUMN-LABEL "Qt.Fat."
    wwpedido                    COLUMN-LABEL "Pedido"
    estabelec.cgc               COLUMN-LABEL "CGC FASTPLAS"
    emitente.cgc                COLUMN-LABEL "CGC MBB"
    it-nota-fisc.class-fiscal   COLUMN-LABEL "Classifica��o"
    nota-fiscal.nat-operacao    COLUMN-LABEL "Nat.Oper"
HEADER 
"------------------------------------------------------------------------------------------------------------------------------------"
SKIP
" LOG ASN MBB - BENEFICIAMENTO JUIZ DE FORA- RND 004.15     Pagina: " PAGE-NUMBER FORMAT "99" " - "  TODAY FORMAT "99/99/9999" " - "  STRING(TIME,"HH:MM:SS") "    / TOTVS - ESPSF016"
SKIP
"------------------------------------------------------------------------------------------------------------------------------------" 
SKIP
"*** ARQUIVO GERA��O ASN: " tt-param.arq-entrada FORMAT "X(40)"  " / ARQUIVO DE CONSIST�NCIA: "  tt-param.wrel-log
SKIP(1)  
   WITH STREAM-IO WIDTH 450 FRAME f-logautoma DOWN.
  
  /* Fabricas */ 
       IF tt-param.wcod-ini = 2261 /* mbb SBC*/
       THEN ASSIGN wfab = 540. 
       IF tt-param.wcod-ini = 2263 /* mbb CAMPINAS*/
       THEN ASSIGN wfab = 541.
       IF tt-param.wcod-ini = 506 /* mbb JUIS DE FORA */
       THEN ASSIGN wfab = 543. 
       
       
  
     /*  IF wcod-ini <> 3519 THEN  ASSIGN wreceptor = "VOLKSWAGEN DO BRASIL S/A".*/
    IF tt-param.wcod-ini = 2261 OR tt-param.wcod-ini = 2263
       OR tt-param.wcod-ini = 506  THEN  ASSIGN wreceptor = "MERCEDES BENZ DO BRASIL S".  

    IF wfab = 0 THEN DO:
      MESSAGE
      "CLIENTE:" tt-param.wcod-ini "NAO POSSUI CODIGO DE FABRICA DEFINIDO."
      "SE FOR NOVO C�DIGO ENTRE EM CONTATO COM VALERIA - R. 8527 / INFORM�TICA." 
          VIEW-AS ALERT-BOX.               
      quit.
      END.                                    

  /* PROCURA CGC */
  
  FIND FIRST estabelec WHERE estabelec.cod-estabel = tt-param.wcod-est 
                         NO-LOCK NO-ERROR.
  FIND FIRST emitente WHERE emitente.cod-emitente /*emitente.nome-abrev = "MBB" */ = tt-param.wcod-ini NO-LOCK NO-ERROR.
        /* CGC CLIENTE */
        DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(emitente.cgc,wcont,1)) = 45
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 46
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(emitente.cgc,wcont,1).
          END.
       
        /* CGC FORNECEDOR */
        DO wcont1 = 1 TO 19:
          IF ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  45
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  46
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgcfor = wcgcfor + SUBSTRING(estabelec.cgc,wcont1,1).
          END.
         /* MESSAGE wcgccli wcgcfor VIEW-AS ALERT-BOX. */
  ASSIGN wemp = estabelec.nome.
  /*ITP*/
  PUT stream s-exp UNFORMATTED
      "ITP" /* REGISTRO*/ 
      "004" /* TIPO RND 004*/ /* processo*/
      "15"  /* VERS�O RND */ /*vers�o transacao*/
      "00000" /* n.controle transmiss�o*/
      SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99" /* identificacao ger. movto at� hora*/
      MONTH(TODAY) FORMAT "99"
      DAY(TODAY) FORMAT "99"
      SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
      TRIM(wcgcfor) FORMAT "X(14)" /* identificacao transmissor*/
      TRIM(wcgccli) FORMAT "X(14)" /* identificacao receptor*/
      FILL(" ",16)/* cod. interno transmissor(8) e receptor(8)*/
      /*"SEEBER FASTPLAS LTDA." */
      wemp FORMAT "X(25)"  /* nome transmissor */
      wreceptor /* nome do receptor */ AT 95
      /*"VOLKSWAGEN DO BRASIL S/A" */ 
      FILL(" ",9)
      SKIP.
    /* CONTADOR REGISTRO TOTAL */
    ASSIGN wqt-registro  =  wqt-registro + 1.

    /* NATUREZAS DE OPERACAO DEFINIDAS POR JOSE LUIZ */
    FOR EACH nota-fiscal where nota-fiscal.cod-emitente =  tt-param.wcod-ini
                         AND   nota-fiscal.nr-nota-fis  GE tt-param.wnf-ini 
                         AND   nota-fiscal.nr-nota-fis  LE tt-param.wnf-fim
                         AND   nota-fiscal.dt-emis-nota GE tt-param.wdt-ini
                         AND   nota-fiscal.dt-emis-nota LE tt-param.wdt-fim
                         AND   nota-fiscal.dt-cancel    = ? 
                         AND   nota-fiscal.cod-estabel  = tt-param.wcod-est
                         AND   nota-fiscal.serie        = tt-param.wserie
                         /* 18/07/17 comentado conf. J.Luiz para estalec 5 encerrado - Juiz de Fora
                         AND   (nota-fiscal.nat-operacao = "513JF2"
                         OR     nota-fiscal.nat-operacao = "513JF3"
                         OR    nota-fiscal.nat-operacao = "594JF"
                         OR    nota-fiscal.nat-operacao = "5999JF" */ 
                        /* OR    nota-fiscal.nat-operacao = "6999DV")*/
                        /* alteracao 18/12/17 - jose luiz - para estabelecimento 1*/
                          
                        /*11/03/20 - conf. sol alessandro expedicao e fat - foram incluidos natureza 513MBB E 594c2 - fornd 2261 sbc
                         AND    (nota-fiscal.nat-operacao = "613MB"
                         OR     nota-fiscal.nat-operacao = "694C2"
                         OR     nota-fiscal.nat-operacao = "6999D2") */
                        /* 20/08/21 INSERIADA NAT 5999D2 - CONF. FABIANO */
                        AND    (nota-fiscal.nat-operacao = "613MB"
                         OR     nota-fiscal.nat-operacao = "694C2"
                         OR     nota-fiscal.nat-operacao = "6999D2"
                         OR     nota-fiscal.nat-operacao = "513MBB"
                         OR     nota-fiscal.nat-operacao = "594C2"
                         OR     nota-fiscal.nat-operacao = "5999D2")


                         /* inibido conf. sol. idelcio e jose luiz - 29/04/19
                            por se tratar de natureza de embalagem*/
                        /* OR     nota-fiscal.nat-operacao = "6999EB") */
                         NO-LOCK BREAK BY nota-fiscal.dt-emis-nota:
        
       ASSIGN wtot-itens  = 0
               wseq-it    = 0
               wtipo-forn = "".
       ASSIGN
             wnum-kamban    = ""
             wdata-kamban    = ""
             wqt-embal-chamada = ""
             wqt-it-chamada    = ""
             wespaco-ae9       = ""
             wespaco-ae8       = " " 
             wtipo-forn        = " " 
             wcampos-ae4       = " ".
      
       
       /* ASSINALA tipo de forneciemnto*/
       /* obs 
       Os 3 tipos de fornecimento poss�veis s�o:
		 - CFOP 5124 - INDUSTRIALIZACAO EFETUADA PARA OUTRA EMPRESA           - SEEBER 513JF2 -
		 O fornecedor do I-Park dever� enviar em n�vel de �tem a literal "M" no arquivo RND004.

		 - CFOP 5902 - RETORNO DE MERCADORIA UTILIZADA NA INDUSTRIALIZACAO   - SEEBER 594JF - 
		 O fornecedor do I-Park dever� enviar em n�vel de �tem a literal "F" no arquivo  RND004.

		 - CFOP 5903 - RETORNO DE MERCADORIA RECEBIDA P/INDUST. E NAO APLICADA NO REFERIDO PROCESSO - 5999JF -
		 O fornecedor do I-Park dever� enviar em n�vel de �tem a literal "o"  (letra o) no arquivo  RND004 */

       IF nota-fiscal.nat-operacao = "513JF2" THEN ASSIGN wtipo-forn = "M".
       IF nota-fiscal.nat-operacao = "513JF3" THEN ASSIGN wtipo-forn = "M".
       IF nota-fiscal.nat-operacao = "594JF"  THEN ASSIGN wtipo-forn = "F".
       IF nota-fiscal.nat-operacao = "5999JF" THEN ASSIGN wtipo-forn = "o".

       IF nota-fiscal.nat-operacao = "613MB" THEN ASSIGN wtipo-forn = "M".
       IF nota-fiscal.nat-operacao = "694C2" THEN ASSIGN wtipo-forn = "F".
       IF nota-fiscal.nat-operacao = "6999D2" THEN ASSIGN wtipo-forn = "o".
       IF nota-fiscal.nat-operacao = "6999EB" THEN ASSIGN wtipo-forn = "o".

       /* sbc forned 2261*/
       /*11/03/20 - INICIO - conf. sol alessandro expedicao e fat - foram incluidos natureza 513MBB E 594c2 - fornd 2261 sbc*/
        IF nota-fiscal.nat-operacao = "513MBB" THEN ASSIGN wtipo-forn = "M".
        IF nota-fiscal.nat-operacao = "594C2" THEN ASSIGN wtipo-forn = "F".
        /*11/03/20 - FIM - conf. sol alessandro expedicao e fat - foram incluidos natureza 513MBB E 594c2 - fornd 2261 sbc*/

       /* buffer para somar total de intes da nota */
       FOR EACH b-it-auxiliar OF nota-fiscal NO-LOCK:
          ASSIGN wtot-itens = wtot-itens + 1.
          END.   

       FIND FIRST natur-oper WHERE natur-oper.nat-operacao =
                                   nota-fiscal.nat-operacao
                                   NO-LOCK NO-ERROR.

  /*     IF natur-oper.emite-duplic = yes THEN DO: */
       
       FIND FIRST natur-oper WHERE natur-oper.nat-operacao = 
                                   nota-fiscal.nat-operacao NO-LOCK NO-ERROR.
       ASSIGN wqt-it  = 0
              wcont   = 0
              wcont1  = 0
              wcont2  = 0
              wcgccli = " "
              wcgcfor = " "
              wnarrativa = ""
              wwitem     = ""
              wvenc      = "".


       FOR EACH doc-fiscal WHERE doc-fiscal.cod-emitente 
                               = nota-fiscal.cod-emitente
                           AND doc-fiscal.dt-emis-doc  
                               = nota-fiscal.dt-emis-nota
                           AND doc-fiscal.nr-doc-fis   
                               = nota-fiscal.nr-nota-fis
                           NO-LOCK:
           FIND FIRST it-doc-fisc OF doc-fiscal NO-LOCK NO-ERROR.
            run pi-acompanhar in h-acomp ("Cliente: " + string(nota-fiscal.cod-emitente) + " - Nota: " + STRING(nota-fiscal.nr-nota-fis)). 


            /* ANTES 20/02/18 
            FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.nr-nota-fis:
             ASSIGN wnf-remessa     = 000000
                    wdata-remessa   = "000000"
                    wserie-remessa  = "    "
                    wwpedido        = "".
             */

           /****** 20/02/18 - NOVA L�GICA - CHAVE SOL. JOSE LUIZ *****************/
           FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.nr-nota-fis:
             /* nf remessa - origem  */
             ASSIGN wdata-remessa   = "000000"
                wserie-remessa  = "    "
                wchave-remessa = " "
                wnf-remessa = 0.
             
            
             /* SE A NOTA FOR MAIOR QUE 6 NAO ASSINALA O NUMERO - DEIXA COM ZEROS
             DECIDIDO C/ ALESSANDRO EM 12/03/20 / por nao caber no layout*/
             IF LENGTH(int(it-nota-fisc.nr-docum)) LE 6  THEN
                ASSIGN wnf-remessa = int(it-nota-fisc.nr-docum).

         
             FIND docum-est WHERE docum-est.nro-docto  = it-nota-fisc.nr-docum
                              AND   docum-est.cod-emitente = nota-fiscal.cod-emitente
                              AND   docum-est.cod-estabel  = nota-fiscal.cod-estabel
                              NO-LOCK NO-ERROR.
            /* MESSAGE docum-estdt-emis-doc docum-estserie VIEW-AS ALERT-BOX.*/
             IF AVAILABLE docum-est THEN 
             ASSIGN wdata-remessa   = SUBSTRING(STRING(YEAR(docum-est.dt-emissao)),3,4)
                                    + STRING(MONTH(docum-est.dt-emissao ),"99") 
                                    + STRING(DAY(docum-est.dt-emissao ),"99")
                                    wserie-remessa  = docum-est.serie
                                    wchave-remessa =  string(docum-est.cod-chave-aces-nf-eletro,"X(60)").
             IF NOT AVAILABLE docum-est  THEN DO:
                /* MESSAGE "CHAVE DE ACESSO DA NOTA DE ORIGEM N�O FOI LOCALIZADA!"  
                         SKIP "ARQUIVO GERADO COM ESPA�OS NO CAMPO CHAVE."
                         SKIP "VERIFIQUE O ARQUIVO ANTES DE ENVIAR."
                         VIEW-AS ALERT-BOX.*/

                 ASSIGN wdata-remessa   = "000000"
                        wserie-remessa  = "    "
                        wchave-remessa = " ".
                 END.
           /****** 20/02/18 **************************************************/



           /*  IPI - INIBIDO CONF. ZE E IDELCIO - L�GICA SOMENTE PARA MBB CAMPINAS E SBC
            21-07-11 - verifica a natureza e zera valores ipi, discutido com Idelcio e Jose Luiz 
                   
             FIND FIRST ITEM OF it-nota-fisc NO-LOCK.
             ASSIGN waliquota-ipi = it-nota-fisc.aliquota-ipi
                    wvl-ipi-it    = it-nota-fisc.vl-ipi-it.
            IF item.cd-trib-ipi = 4 /* reduzido */
              AND natur-oper.cd-trib-ipi = 3 /* outros */  THEN DO:
              ASSIGN waliquota-ipi = 0 
                     wvl-ipi-it    = 0.
              END.
              */

           /* 18/04/2012 - valor aliquota e ipi para MBB FORAM ZERADOS, POIS
           N�O SE APLICA PAA MBB - JUIZ DE FORA - CONF. IDELCIO E JOSE LUIZ */
           ASSIGN waliquota-ipi = 0 
                  wvl-ipi-it    = 0.

           ASSIGN wqt-it  = wqt-it + 1
                  wclass  = " "
                  wdif    = 0
                  wdif1   = 0
                  wseq-it = wseq-it + 1
                  wtam-item = 0.
      
         FIND item WHERE item.it-codigo = it-nota-fisc.it-codigo
                   NO-LOCK NO-ERROR.
         ASSIGN witem = "".

         /* tratamento de item - colocar 6 espa�os antes da cor, pois n�o cabe no campo ems 
            acertado com Thiago e Jos� Luiz*/

         /* 20/04/12 TRATAMENTO JUIZ DE FORA - CONF. JOSE LUIZ ------------------------------------------------------------
          1) Quando inicia com "N"999999999999 sempre � acompanhado de mais 12 caracteres.    = TOTAL DE 13 POSI��ES
          2) A outra regra antiga utilizada para Campinas e SBC, tamb�m se aplica para Juiz de Fora.  
          "A"9999999999bb9999  ( 2 espa�os no cadastro EMS pois n�o cabem 6)
          O PROGRAMA TRATA E COLOCA 6 ESPA�OS ANTES DA COR CONFORME ABAIXO.  
          O programa de aviso de embarque inclui mais quatrO brancos para atender o padr�o MBBras, com total de 6 espa�os
          antes da cor, por exemplo "A"9999999999bbbbbb9999 = TOTAL DE 21 POSI��ES
          3) Temos tamb�m os c�digos QSV99999999 (11 posi��es) = TOTAL DE 11 POSI��ES
          
          /* 18/09/12 - conf. jose luiz - alterado a ultima linha de "q" para "x" - para atender mbb*/
         ------------------------------------------------------------------------------------------------------------------*/
         IF SUBSTRING(ITEM.codigo-refer,1,1) = "N" THEN ASSIGN witem = SUBSTRING(item.codigo-refer,1,13).
         IF SUBSTRING(ITEM.codigo-refer,1,1) = "A" THEN ASSIGN witem = SUBSTRING(codigo-refer,1,11) + FILL(" ",6) + SUBSTRING(codigo-refer,14,4). 
         IF SUBSTRING(ITEM.codigo-refer,1,1) = "X" THEN ASSIGN witem = SUBSTRING(item.codigo-refer,1,11).
         
         /* ASSIGN witem = SUBSTRING(item.codigo-refer,1,20).*/
       
         /* Tratamento para extrar codigo do cliente e pedido */
         FIND FIRST item-cli 
              WHERE (item-cli.nome-abrev   = nota-fiscal.nome-ab-cli                         
              OR    item-cli.cod-emitente  = nota-fiscal.cod-emitente)
              AND   item-cli.it-codigo    = it-nota-fisc.it-codigo
                    NO-LOCK NO-ERROR.

         IF NOT AVAILABLE item-cli THEN DO:
           MESSAGE "ERROR NOTA FISCAL: " nota-fiscal.nr-nota-fis 
           SKIP(1)  "NAO EXISTE RELACIONAMENTO ITEM/CLIENTE PARA NF:" nota-fiscal.nr-nota-fis 
                
          /*     
            item-cli.cod-emitente item-cli.it-codigo 
                 nota-fiscal.nr-nota-fis
           SKIP "CLIENTE:" nota-fiscal.cod-emitente 
           "ITEM:" it-nota-fisc.it-codigo */
           SKIP "!!! GERA��O DE ARQUIVO PODE ESTAR COM PROBLEMA !!!"
           SKIP(1)  "PROCURE A REGIANE/COMERCIAL PARA RELACIONAMENTO ITENS DE VENDA."
           SKIP(1)  "PROCURE CARLOS/RECEBIMENTO PARA RELACIONAMENTO ITENS DE EMBALAGEM"
           VIEW-AS ALERT-BOX.
           /*
           DISPLAY "GERA��O COM PROBLEMA NA NOTA: " nota-fiscal.nr-nota-fis WITH FRAME f-log.
           */
           LEAVE.
           END.

         /* - = 45 / . = 46 / P = 80 / "/" = 47 / p = 112*/
         IF AVAILABLE item-cli THEN DO:

           ASSIGN wnarrativa = item-cli.narrativa.
             DO wconta = 1 TO 76:
              IF  ASC(SUBSTRING(wnarrativa,wconta,1))     = 80  /*P*/
              OR  ASC(SUBSTRING(wnarrativa,wconta,1))     = 112 /*p*/ 
              THEN DO:
              wconta = wconta + 1.
              IF (ASC(SUBSTRING(wnarrativa,wconta,1))    = 46  /* . */
              OR  ASC(SUBSTRING(wnarrativa,wconta,1))    = 45)  /* - */
              THEN
                 ASSIGN wauxped = SUBSTRING(wnarrativa,wconta - 1,14).
                  ASSIGN wwpedido = SUBSTRING(wauxped,3,12).
     
                 END.
                END.
    /* procura data de vencimento */
   FIND FIRST fat-duplic
             where fat-duplic.cod-estabel = nota-fiscal.cod-estabel
             and   fat-duplic.serie       = nota-fiscal.serie
             and   fat-duplic.nr-fatura   = nota-fiscal.nr-fatura
             and   fat-duplic.parcela     = "01" NO-LOCK NO-ERROR.
       IF AVAILABLE fat-duplic THEN ASSIGN 
           wvenc = SUBSTRING(STRING(YEAR(fat-duplic.dt-vencimen)),3,4)
                 + STRING(MONTH(fat-duplic.dt-vencimen),"99") 
                 + STRING(DAY(fat-duplic.dt-vencimen),"99").

       IF NOT AVAILABLE fat-duplic THEN ASSIGN wvenc = "000000".
     
     
     /* ---- logica at� 20/02/18 - antes inclusao chave - sol. jose luiz   
     /* NOTA REMESSA */
     ASSIGN wnf-remessa = int(it-nota-fisc.nr-docum).
     FIND wdoc-fiscal WHERE wdoc-fiscal.nr-doc-fis  = it-nota-fisc.nr-docum
                      AND   wdoc-fiscal.cod-emitente = nota-fiscal.cod-emitente
                      AND   wdoc-fiscal.cod-estabel  = nota-fiscal.cod-estabel
                      NO-LOCK NO-ERROR.
    /* MESSAGE wdoc-fiscal.dt-emis-doc wdoc-fiscal.serie VIEW-AS ALERT-BOX.*/
     IF AVAILABLE wdoc-fiscal THEN 
     ASSIGN wdata-remessa   = SUBSTRING(STRING(YEAR(wdoc-fiscal.dt-emis-doc )),3,4)
                            + STRING(MONTH(wdoc-fiscal.dt-emis-doc ),"99") 
                            + STRING(DAY(wdoc-fiscal.dt-emis-doc ),"99")
                            wserie-remessa  = wdoc-fiscal.serie.
     IF NOT AVAILABLE wdoc-fiscal  THEN
         ASSIGN wdata-remessa   = "000000"
                wserie-remessa  = "    ".
         
         ---- logica at� 20/02/18 - antes inclusao chave - sol. jose luiz   */
    
     
         /* LOG RELATORIO */
         DISPLAY STREAM str-rp
                 nota-fiscal.nr-nota-fis
                 wseq-it
                 nota-fiscal.cod-emitente 
                 nota-fiscal.dt-emis-nota 
                 it-nota-fisc.it-codigo            FORMAT "X(16)"
                 ITEM.un
                 witem          
                 it-nota-fisc.qt-faturada[1] 
                 wvenc                          
                 it-nota-fisc.vl-preuni
                 nota-fiscal.vl-tot-nota 
                 wnf-remessa   
                 wdata-remessa 
                 wserie-remessa 
                 wchave-remessa                     FORMAT "x(45)"
                 wwpedido
                 estabelec.cgc   
                 emitente.cgc    
                 it-nota-fisc.class-fiscal          COLUMN-LABEL "Class"
                 nota-fiscal.nat-operacao 
                 /*
                 it-nota-fisc.aliquota-icm
                 "Iem" it-nota-fisc.vl-bicms-it
                 "Item" it-nota-fisc.vl-icms-it    
                 */
                 it-doc-fis.vl-icms-it            FORMAT ">>>>9.99" COLUMN-LABEL "ICMS IT"
                 it-doc-fis.vl-bicms-it           FORMAT ">>>>9.99" COLUMN-LABEL "B ICMS IT"
                 it-doc-fis.aliquota-icm          COLUMN-LABEL "AliqICMS"
                 doc-fiscal.vl-icms               FORMAT ">>>>9.99" COLUMN-LABEL "ICMS Nota"
                 waliquota-ipi /* it-nota-fisc*/  FORMAT ">9.9"
                 wvl-ipi-it                       FORMAT ">>>>9.99"
                 WITH FRAME f-log WIDTH 450.
            /*19/06/20 - display nota original para conf. com MBB*/
            IF it-nota-fisc.nr-docum <> "" THEN DISPLAY  STREAM str-rp it-nota-fisc.nr-docum COLUMN-LABEL "*** NR.NF.ORI" WITH FRAME f-log.
             DISPLAY STREAM str-rp SKIP WITH FRAME f-log.
             DOWN STREAM str-rp WITH FRAME f-log.

         /* LOG AUT0MATICO - 29/04/19 */
            DISPLAY STREAM wlog-automa
                 nota-fiscal.nr-nota-fis
                 wseq-it
                 nota-fiscal.cod-emitente 
                 nota-fiscal.dt-emis-nota 
                 it-nota-fisc.it-codigo            FORMAT "X(16)"
                 ITEM.un
                 witem          
                 it-nota-fisc.qt-faturada[1] 
                 wvenc                          
                 it-nota-fisc.vl-preuni
                 nota-fiscal.vl-tot-nota 
                 wnf-remessa                    
                 wdata-remessa 
                 wserie-remessa 
                 wchave-remessa                     FORMAT "x(45)"
                 wwpedido
                 estabelec.cgc   
                 emitente.cgc    
                 it-nota-fisc.class-fiscal          COLUMN-LABEL "Class"
                 nota-fiscal.nat-operacao 
                 /*
                 it-nota-fisc.aliquota-icm
                 "Iem" it-nota-fisc.vl-bicms-it
                 "Item" it-nota-fisc.vl-icms-it    
                 */
                 it-doc-fis.vl-icms-it            FORMAT ">>>>9.99" COLUMN-LABEL "ICMS IT"
                 it-doc-fis.vl-bicms-it           FORMAT ">>>>9.99" COLUMN-LABEL "B ICMS IT"
                 it-doc-fis.aliquota-icm          COLUMN-LABEL "AliqICMS"
                 doc-fiscal.vl-icms               FORMAT ">>>>9.99" COLUMN-LABEL "ICMS Nota"
                 waliquota-ipi /* it-nota-fisc*/  FORMAT ">9.9"
                 wvl-ipi-it                       FORMAT ">>>>9.99"
                 SKIP WITH FRAME f-logautoma WIDTH 450.
             DOWN STREAM wlog-automa WITH FRAME f-logautoma.


        /* Classifica��o fiscal */
        DO wcont2 = 1 TO 19:
          IF ASC(SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1)) <> 46
          THEN DO:
            ASSIGN wclass = wclass
                          + SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1).
          END.                   
        END.
        ASSIGN wclass = TRIM(wclass).
        /*  
        IF LENGTH(wclass) < 10 THEN DO:
            ASSIGN wdif = 10 - LENGTH(wclass).
            DO wdif1 = 1 TO wdif:
              ASSIGN wclass = wclass + "0".
              END.
            END.
          */
         IF FIRST-of(it-nota-fisc.nr-nota-fis) THEN DO:
             RUN  DISP_ae1.
             RUN  DISP_nf2.
             END.
         RUN DISP_dados.
        END.
      END.
    END.
    END.
   /* END.*/
   RUN encerra.
 
END PROCEDURE.
/*---------------------------procedure disp_ae1-------------------------------------------------*/

PROCEDURE DISP_ae1.
  /* AE1 */
        PUT stream s-exp UNFORMATTED
      "AE1"
      INTEGER(nota-fiscal.nr-nota-fis)   FORMAT "999999"
            CAPS(nota-fiscal.serie)            FORMAT "X(4)"
      SUBSTRING(STRING(YEAR(dt-emis-nota),"9999"),3,4) FORMAT "99"
            MONTH(dt-emis-nota) FORMAT "99"
            DAY(dt-emis-nota) FORMAT "99"
      wtot-itens              FORMAT "999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),17,2) 
            FORMAT "99"
            /* anterior "2" */
            "0" /* no. casas decimais para quant. */
      SUBSTRING(nota-fiscal.nat-operacao,1,3) FORMAT "999"
      "00"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),1,15) FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),17,2) FORMAT "99"
      wvenc FORMAT "X(06)"
      "02" /*nota-fiscal.esp-docto */
      SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),1,15)
      FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),17,2)
      FORMAT "99" 
      wfab FORMAT "999"
      "991231" /* nota-fiscal.dt-entr-cli FORMAT "999999" */
      "    " FORMAT "X(04)"                  /* periodo de entrega */
      natur-oper.denominacao FORMAT "X(15)" 
      "            " FORMAT "X(13)"
           /* vw SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99"
            MONTH(TODAY) FORMAT "99"
            DAY(TODAY) FORMAT "99"
            SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
            "   "*/
            SKIP.
    /* CONTADOR REGISTRO TOTAL */
    ASSIGN wqt-registro  =  wqt-registro + 1.
    END PROCEDURE.

/* -----------------------------------disp_nf2 ---------------------------------*/
PROCEDURE DISP_nf2.
    PUT stream s-exp UNFORMATTED
      "NF2" 
      SUBSTRING(STRING(doc-fiscal.val-desp-outros,"9999999999.99"),1,10) FORMAT "9999999999"
      SUBSTRING(STRING(doc-fiscal.val-desp-outros,"9999999999.99"),12,2) FORMAT "99"  

      SUBSTRING(STRING(doc-fiscal.vl-frete,"9999999999.99"),1,10) FORMAT "9999999999"
      SUBSTRING(STRING(doc-fiscal.vl-frete,"9999999999.99"),12,2) FORMAT "99"   

      SUBSTRING(STRING(doc-fiscal.vl-seguro,"9999999999.99"),1,10) FORMAT "9999999999"
      SUBSTRING(STRING(doc-fiscal.vl-seguro,"9999999999.99"),12,2) FORMAT "99"   

      SUBSTRING(STRING(doc-fiscal.tot-desconto,"9999999999.99"),1,10) FORMAT "9999999999"
      SUBSTRING(STRING(doc-fiscal.tot-desconto,"9999999999.99"),12,2) FORMAT "99"   
      
      SUBSTRING(STRING(doc-fiscal.vl-bicms,"9999999999.99"),1,10) FORMAT "9999999999"
      SUBSTRING(STRING(doc-fiscal.vl-bicms,"9999999999.99"),12,2) FORMAT "99"

      SUBSTRING(STRING(doc-fiscal.vl-icms,"9999999999.99"),1,10)   FORMAT "9999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"9999999999.99"),12,2)  FORMAT "99"

      INTEGER(nota-fiscal.nr-nota-fis)   FORMAT "999999"            /* nr nf fornecedor */

      SUBSTRING(STRING(YEAR(nota-fiscal.dt-emis-nota),"9999"),3,4) FORMAT "99" /* dt nf fornecedor */
            MONTH(nota-fiscal.dt-emis-nota) FORMAT "99"
            DAY(nota-fiscal.dt-emis-nota) FORMAT "99" 

      CAPS(nota-fiscal.serie)            FORMAT "X(4)"            /* serie fornecedor */

      wfab FORMAT "999" /* codigo fabrica transmissora */

      doc-fiscal.nat-operacao FORMAT "99999" /* cfop*/

      FILL(" ",29) FORMAT "X(29)" /* espaco */ 

      SKIP.
END PROCEDURE.

/*------------------------------- PROCEDURE disp_dados ------------------------*/
PROCEDURE DISP_dados.

   
       /*ASSIGN KANBAN - ZERADO CONF. INFORMADO AO JOSE LUIZ PELA MBB EM 04/12*/
       ASSIGN    wnum-kamban        = "  " /*"XXVVVVVV"*/
                 wdata-kamban       =  SUBSTRING(STRING(YEAR(nota-fiscal.dt-emis-nota),"9999"),3,4) 
                                       + string(MONTH(nota-fiscal.dt-emis-nota),"99") 
                                       + string(DAY(nota-fiscal.dt-emis-nota),"99")  
                 wqt-embal-chamada  = "000000001"         
                 wqt-it-chamada     = "000000000".  
       
      /* AE2*/
       PUT stream s-exp UNFORMATTED 
                 "AE2"                                        /* tipo do registro */
                 wseq-it     FORMAT "999"    /* num. item na nota */
                 wwpedido                    FORMAT "X(12)"  /* pedido */
                 witem                       FORMAT "X(30)" /* cod-item */
            /*SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),1,7)
            FORMAT "9999999"
            SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),9,2)
            FORMAT "99"*/
                  SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"999999999.9999"),1,9)
                  FORMAT "999999999"
                  /* it-nota-fisc.un-fatur[1] FORMAT "X(02)" */
                  ITEM.un FORMAT "X(02)"
                  wclass FORMAT "X(10)"
                  SUBSTRING(STRING(waliquota-ipi,"999.99"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(waliquota-ipi,"999.99"),5,2) 
                  FORMAT "99"
                  /*
                  SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),5,2) 
                  FORMAT "99"
                  */
                  SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999.99999"),1,7)
                  FORMAT "9999999"
                  SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999.99999"),9,5)
                  /* corrigido o formato para 5 posi��es e nao 6, conf. sol. marcos gedas 
                  15/09/06" */
                  FORMAT "99999" 
                  SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"999999999.9999"),1,9)        FORMAT "999999999"         /* qt.item estoque       */
                  item.un                 FORMAT "X(02)"
                  SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"999999999.9999"),1,9)        FORMAT "999999999"         /* qt.unidade compra     */
                  item.un                 FORMAT "X(02)"   /* unidade medida compra */
                  /* aqui mbb juiz de fora - M - CONJUNTO MONTADO E L RETORNO DE PE�AS */
                  string(wtipo-forn,"X(01)")                     /* tipo de fornecimento  */
                  SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),2,2)     FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),5,2)     FORMAT "99"
                  "00000000000" FORMAT "X(11)"           /* Vr.tot.desc.item      */
                  "     " FORMAT "X(05)" /* alteracao tec e espaco */
                  SKIP
            /*AE4 */
                  "AE4"
                  SUBSTRING(STRING(it-doc-fis.aliquota-icm,"999.99"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-doc-fis.aliquota-icm,"999.99"),5,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-doc-fis.vl-bicms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
                  SUBSTRING(STRING(it-doc-fis.vl-bicms-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-doc-fis.vl-icms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
                  SUBSTRING(STRING(it-doc-fis.vl-icms-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
                  SUBSTRING(STRING(wvl-ipi-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
                  SUBSTRING(STRING(wvl-ipi-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
                  /* 18/04/12 INIBIDO 
                  SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
                  SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
                  */
                  /*vw "  "  */
                  "00"                      /* sit.tributaria            */
                  wdesenho  FORMAT "X(30)"  /* numero do desenho do item */
                  "000000"                  /* dt-val-desenho            */
                  wcampos-ae4 FORMAT "X(19)" /* PED VENDA / PESO LIQ IT / MULTPLICADOR PRE�O / / SIT TRIBU */
                  SUBSTRING(STRING(it-nota-fisc.vl-tot-item,"9999999999.99"),1,10) /* pre�o mercadoria */
                  FORMAT "9999999999"
                  SUBSTRING(STRING(it-nota-fisc.vl-tot-item,"9999999999.99"),12,2)
                  FORMAT "99"
                  " "
                 SKIP
             /* AE9 */
                "AE9"
                 wnum-kamban                FORMAT "X(12)"
                 wdata-kamban               FORMAT "999999"
                 wqt-embal-chamada          FORMAT "999999999"
                 wqt-it-chamada             FORMAT "999999999"
                 wespaco-ae9                FORMAT "X(89)"
                 SKIP

              /* AE8 */
                 "AE8"
                 wnf-remessa                FORMAT "999999" /*NO. NF REMESSA */
                 wserie-remessa             FORMAT "X(04)"  /*SERIE */
                 wdata-remessa              FORMAT "X(6)"   /*DATA */
                 "000"                      FORMAT "X(3)"  /*N. ITEM NA NF */
                 /* ate 20/02/18 
                 wespaco-ae8                FORMAT "X(106)" /* CORRIDA / CODIGO VIN / NO. AUTORIZACAO FATU */ */
                FILL(" ",43)               FORMAT "X(43)" /* espacos */
                wchave-remessa             FORMAT "x(60)" /* chave nota remessa*/
                FILL(" ",2) FORMAT "X(2)" /* CORRIDA / CODIGO VIN / NO. AUTORIZACAO FATU */
                SKIP.

       /* CONTADOR REGISTRO TOTAL - SOMANDO AQUI AE2 / AE4 / AE9 E AE8*/
       ASSIGN wqt-registro  =  wqt-registro + 4.      


             /*MESSAGE  int(it-nota-fisc.nr-docum) VIEW-AS ALERT-BOX.*/

           /* vw        
           "             "         /* PED REVENDA */
            SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),4,3)
                  FORMAT "999"
            SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),8,2)
                  FORMAT "99"
                  "0"
            SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),1,10)
                  FORMAT "9999999999"
            SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),12,2)
                  FORMAT "99"
                  " " /* situacao tributaria      */

                  SKIP.*/
            /* Referente embalagem  AE5*/

           /* Campos de embalagem assinalados conforme orienta��o
           do Sr. Marcos da Gedas em 15/08/2005, para evitar
           mensagens de alerta para o Sr. Nelson Yamaguti 
           Campos: numero da nota embalagem e data iguais 
           aos da nota fiscal de faturamento*/
           
    /*vw  ASSIGN wnf-embal = INTEGER(nota-fiscal.nr-nota-fis).*/
    
         
      /* TOTAL DE TRANSA��ES */
      ASSIGN  wtot-valores  = wtot-valores + it-nota-fisc.vl-preuni.

     /*  MESSAGE wtot-valores it-nota-fisc.vl-preuni vl-tot-nota VIEW-AS ALERT-BOX.*/

     

     END PROCEDURE.


/*----------------------- PROCEDURE disp_encerra ---------------------------*/
PROCEDURE encerra.
    ASSIGN wqt-registro  =  wqt-registro + 1.

    PUT stream s-exp UNFORMATTED 
    "FTP"
    "00000"  /*nO.CONTROLE TRANSMISS�O */
    wqt-registro  /* qt registros */ FORMAT "999999999"
    SUBSTRING(STRING(wtot-valores,"999999999999999.99"),1,15) 
    FORMAT "999999999999999"
    SUBSTRING(STRING(wtot-valores,"999999999999999.99"),17,2) 
          FORMAT "99"
    " "
    wespaco FORMAT "X(93)".
 /* MESSAGE wqt-registro wtot-valores VIEW-AS ALERT-BOX.  */
     MESSAGE "*** OBSERVA��ES IMPORTANTES ***"
             SKIP(2) 
             "1) OBSERVE SE APRESENTOU MENSAGEM DE RELACIONAMENTO ITEM X CLI, FALAR COM REGIANE/COMERCIAL PARA  ITEM DE VENDA OU CARLOS/RECEBIMENTO PARA ITEM EMBALAGEM."
             SKIP(2)
             "2) VERIFIQUE ARQUIVO DE LOG ANTES DO ENVIO DO ASN."
             SKIP(2)
              "3) OBSERVE CHAVE DE ACESSO  - SE ESTIVER EM BRANCO � PORQUE N�O LOCALIZOU NOTA/CHAVE DA NOTA DE REMESSA."
              VIEW-AS ALERT-BOX.
  END PROCEDURE.


