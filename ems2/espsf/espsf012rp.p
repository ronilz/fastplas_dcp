&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF012RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

 define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wcod-estabel     AS CHAR FORMAT "X(03)"
    FIELD wcod-emitente    AS INTEGER FORMAT ">>>>>>>>9"
    FIELD wnat-ini         AS CHAR FORMAT "X(06)"
    FIELD wnat-fim         AS CHAR FORMAT "X(06)"
    FIELD wdt-emis-notai   AS DATE FORMAT "99/99/9999"
    FIELD wdt-emis-notaf   AS DATE FORMAT "99/99/9999"
    FIELD wclass           AS INTEGER.

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS206 - PROC.ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Listagem de transfer�ncia de Materiais".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).*/
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
     DISPLAY "* Estabelecimento: " tt-param.wcod-estabel "/ Emitente: "  tt-param.wcod-emitente "/ Natureza: " tt-param.wnat-ini "a" 
        tt-param.wnat-fim "/ Per�odo: " tt-param.wdt-emis-notai "a" tt-param.wdt-emis-notaf "*"
        SKIP(1)
        WITH FRAME f-cabec1 PAGE-TOP WITH WIDTH 150 NO-LABELS.
     IF  tt-param.wclass = 1  THEN PUT  "*** Classifica��o: Item ***" AT 58  SKIP.
     IF  tt-param.wclass = 2  THEN PUT  "*** Classifica��o: Nota  ***" AT 58 SKIP.

     /* classifica��o item */
    IF tt-param.wclass = 1 THEN DO:
      FIND EMITENTE WHERE emitente.COD-EMITENTE = tt-param.wcod-emitente NO-LOCK NO-ERROR.
      FOR EACH it-nota-fisc WHERE it-nota-fisc.cod-estabel      = tt-param.wcod-estabel
                            AND   it-nota-fisc.nome-ab-cli      = emitente.nome-abrev
          /*    = tt-param.wcod-emitente*/
                            AND   it-nota-fisc.nat-oper         GE tt-param.wnat-ini
                            AND   it-nota-fisc.nat-oper         LE tt-param.wnat-fim
                            AND   it-nota-fisc.dt-emis-nota     GE tt-param.wdt-emis-notai
                            AND    it-nota-fisc.dt-emis-nota    LE tt-param.wdt-emis-notaf
                            NO-LOCK BREAK by it-nota-fisc.it-codigo:
    FOR EACH nota-fiscal OF it-nota-fisc  NO-LOCK:
      run pi-acompanhar in h-acomp ("Nota Fiscal: " + STRING(nota-fiscal.nr-nota-fis)).
     
          FIND ITEM OF it-nota-fisc NO-LOCK NO-ERROR.

          DISPLAY it-nota-fisc.it-codigo
                ITEM.descricao-1
                it-nota-fisc.qt-faturada[1]  
                it-nota-fisc.vl-preuni      COLUMN-LABEL "Pre�o Unit."
                it-nota-fisc.vl-tot-item 
                 nota-fiscal.cod-estabel
                nota-fiscal.cod-emitente   COLUMN-LABEL "Cli/For"
                nota-fiscal.nr-nota-fis 
                nota-fiscal.nat-operacao
                nota-fiscal.dt-emis-nota
                WITH WIDTH 150 STREAM-IO.
          END.
        END.
    END.
    /*----------------------------------------------------*/
     /* classifica��o nota */
    IF tt-param.wclass = 2 THEN DO:
    FOR EACH nota-fiscal WHERE nota-fiscal.cod-estabel      = tt-param.wcod-estabel
                         AND   nota-fiscal.cod-emitente     = tt-param.wcod-emitente
                         AND   nota-fiscal.nat-oper         GE tt-param.wnat-ini
                         AND   nota-fiscal.nat-oper         LE tt-param.wnat-fim
                         AND   nota-fiscal.dt-emis-nota     GE tt-param.wdt-emis-notai
                         AND    nota-fiscal.dt-emis-nota    LE tt-param.wdt-emis-notaf
                         NO-LOCK BREAK BY nota-fiscal.nr-nota-fis:
      run pi-acompanhar in h-acomp ("Nota Fiscal: " + STRING(nota-fiscal.nr-nota-fis)).
      FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK:
          FIND ITEM OF it-nota-fisc NO-LOCK NO-ERROR.

          DISPLAY 
                nota-fiscal.nr-nota-fis 
                nota-fiscal.nat-operacao
                nota-fiscal.dt-emis-nota
                it-nota-fisc.it-codigo
                ITEM.descricao-1
                it-nota-fisc.qt-faturada[1]  
                it-nota-fisc.vl-preuni      COLUMN-LABEL "Pre�o Unit."
                it-nota-fisc.vl-tot-item 
                 nota-fiscal.cod-estabel
                nota-fiscal.cod-emitente   COLUMN-LABEL "Cli/For"

                WITH WIDTH 150 STREAM-IO.
          END.
        END.
    END.
    /*----------------------------------------------------*/




 
/*     FOR EACH nota-fiscal WHERE nat-oper BEGINS "521a" AND cod-emitente = 530                                     */
/*          AND dt-emis-nota GE 03/01/2011 NO-LOCK:                                                                 */
/*     FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK:                                                                */
/*       DISPLAY nota-fiscal.cod-estabel nota-fiscal.nr-nota-fis nota-fiscal.dt-emis-nota                           */
/*           it-codigo nota-fiscal.nr-nota-fis it-nota-fisc.qt-faturada[1] nota-fiscal.nat-operacao WITH WIDTH 300. */
/*     END.                                                                                                         */
/* END.                                                                                                             */

END PROCEDURE.




