&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF018RP 0.00.06.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wponto-ini       AS CHAR FORMAT "X(05)"
    FIELD wponto-fim       AS CHAR FORMAT "X(05)"
    FIELD wperiodo-ini     AS DATE FORMAT "99/99/9999"
    FIELD wperiodo-fim     AS DATE FORMAT "99/99/9999"
    FIELD wdepos-ini       AS CHAR FORMAT "X(16)"
    FIELD wdepos-fim       AS CHAR FORMAT "X(16)"
    FIELD wapontador       AS CHAR FORMAT "X(30)"
    FIELD wsetor           AS CHAR FORMAT "X(20)"
    FIELD wdetalhe         AS INTEGER.


DEFINE VARIABLE wdispturno AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wtotal     AS INTEGER FORMAT ">>>>>>>>>9".
DEFINE VARIABLE wtot-it    AS INTEGER FORMAT ">>>>>>>>>9".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS206 - ESPEC�FICOS FASTPLAS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Relat�rio de Sucata Geral".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* PROCEDURE roda_prog */
PROCEDURE roda_prog.

  /* RESUMIDO - WDEDALHE = 2 */
  IF tt-param.wdetalhe = 2 THEN DO:
    /*
  IF tt-param.wturnos = 1 THEN ASSIGN wdispturno = "1o. Turno".
  IF tt-param.wturnos = 2 THEN ASSIGN wdispturno = "2o. Turno".
  IF tt-param.wturnos = 3 THEN ASSIGN wdispturno = "3o. Turno". */
  ASSIGN wtotal = 0.
  DISPLAY 
  FILL("=",173) FORMAT "X(173)"
  SKIP
  "PER�ODO: " tt-param.wperiodo-ini " a " tt-param.wperiodo-fim "PONTO CONTROLE: " tt-param.wponto-ini " a "  tt-param.wponto-fim 
  "DEP�SITO DESTINO: " tt-param.wdepos-ini " a " tt-param.wdepos-fim
  SKIP FILL("-",173) FORMAT "X(173)"
  SKIP "APONTADOR....: "  tt-param.wapontador "SETOR....: "AT 90 tt-param.wsetor 
  /*"TURNO....: " AT 109 wdispturno*/
  SKIP
  FILL("=",173) FORMAT "X(173)"
  SKIP
  WITH FRAME f-cabec1 /* PAGE-TOP*/ WITH WIDTH 200 NO-LABEL NO-BOX.


  FOR EACH dc-movto-pto-controle WHERE (cod-pto-controle            GE tt-param.wponto-ini
                              AND       cod-pto-controle            LE  tt-param.wponto-fim)
                              AND     (dc-movto-pto-controle.data   GE  tt-param.wperiodo-ini
                              AND      dc-movto-pto-controle.data   LE  tt-param.wperiodo-fim) 
                              AND     (cod-depos-dest               GE  wdepos-ini
                              AND      cod-depos-dest               LE  wdepos-fim)
                              NO-LOCK:
       FIND FIRST ITEM WHERE ITEM.it-codigo = dc-movto-pto-controle.it-codigo NO-LOCK NO-ERROR. 
       if not avail item then MESSAGE "ITEM N�O ENCONTRADO NO CONTRATO" VIEW-AS ALERT-BOX. 
     
      
     DISPLAY  rg-item  "  "
              dc-movto-pto-controle.it-codigo   ITEM.desc-item 
              cod-pto-controle                  COLUMN-LABEL "Ponto Controle"
              "  " cod-depos-dest               COLUMN-LABEL "Depos.Final"
              nr-rack                           COLUMN-LABEL "Rack" 
              cod-usuario                       COLUMN-LABEL "Usu�rio" 
              dc-movto-pto-controle.data        COLUMN-LABEL "Dt.Trans"
              dc-movto-pto-controle.hora
              WITH FRAME f-relat STREAM-IO DOWN WIDTH 200.
      ASSIGN wtotal = wtotal + 1.
      DOWN WITH FRAME f-relat.
      run pi-acompanhar in h-acomp ("Ponto Controle: " + STRING(cod-pto-controle) + " Item: " + STRING(dc-movto-pto-controle.it-codigo)).
      END.
      PUT SKIP(2)  "****** TOTAL DE REGISTROS: " AT 65 wtotal " ******".
  END.

  /* DETALHADO - WDETALHE = 1 */
   IF tt-param.wdetalhe = 1 THEN DO:
  ASSIGN wtotal = 0.
  DISPLAY 
  FILL("=",173) FORMAT "X(173)"
  SKIP
  "PER�ODO: " tt-param.wperiodo-ini " a " tt-param.wperiodo-fim "PONTO CONTROLE: " tt-param.wponto-ini " a "  tt-param.wponto-fim 
  "DEP�SITO DESTINO: " tt-param.wdepos-ini " a " tt-param.wdepos-fim
  SKIP FILL("-",173) FORMAT "X(173)"
  SKIP "APONTADOR....: "  tt-param.wapontador "SETOR....: "AT 90 tt-param.wsetor 
  /*"TURNO....: " AT 109 wdispturno*/
  SKIP
  FILL("=",173) FORMAT "X(173)"
  SKIP
  WITH FRAME f-cabec2 /* PAGE-TOP*/ WITH WIDTH 200 NO-LABEL NO-BOX.

  FOR EACH dc-movto-pto-controle WHERE   (cod-pto-controle             GE tt-param.wponto-ini
                                 AND      cod-pto-controle             LE  tt-param.wponto-fim)
                                 AND     (dc-movto-pto-controle.data   GE  tt-param.wperiodo-ini
                                 AND      dc-movto-pto-controle.data   LE  tt-param.wperiodo-fim) 
                                 AND     (cod-depos-dest               GE  wdepos-ini
                                 AND      cod-depos-dest               LE  wdepos-fim)
                                 NO-LOCK BREAK BY dc-movto-pto-controle.it-codigo:
       FIND FIRST ITEM WHERE ITEM.it-codigo = dc-movto-pto-controle.it-codigo NO-LOCK NO-ERROR. 
       if not avail item then MESSAGE "ITEM N�O ENCONTRADO NO CONTRATO" VIEW-AS ALERT-BOX. 
       IF FIRST-OF(dc-movto-pto-controle.it-codigo)  THEN DO:
          ASSIGN wtot-it = 0.
          PUT  FILL("-",173) FORMAT "X(173)".
          END.
       DISPLAY  rg-item  "  "   
              dc-movto-pto-controle.it-codigo ITEM.desc-item 
              cod-pto-controle            COLUMN-LABEL "Ponto Controle"
              "  " cod-depos-dest         COLUMN-LABEL "Depos.Final"
              nr-rack                     COLUMN-LABEL "RACK"                       
              cod-usuario                 COLUMN-LABEL "Usu�rio"  
              dc-movto-pto-controle.data  COLUMN-LABEL "Dt.Trans"
              dc-movto-pto-controle.hora  
              WITH FRAME f-relat STREAM-IO DOWN WIDTH 200.
      ASSIGN wtotal = wtotal + 1
             wtot-it = wtot-it + 1.
      DOWN WITH FRAME f-relat.
      IF LAST-OF(dc-movto-pto-controle.it-codigo) THEN DO:
          PUT SKIP(1) "*** ITEM: " AT 65 dc-movto-pto-controle.it-codigo " TOTAL DE OCORR�NCIAS: " wtot-it " ***" SKIP
          FILL("-",173) FORMAT "X(173)"
          SKIP(1).
          ASSIGN wtot-it = 0.
          END.
      
      run pi-acompanhar in h-acomp ("Ponto Controle: " + STRING(cod-pto-controle) + " Item: " + STRING(dc-movto-pto-controle.it-codigo)).
      END.
      PUT SKIP(2)  "****** TOTAL GERAL DE REGISTROS: " AT 65 wtotal " ******".
  END.

END PROCEDURE.




