&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME w-relat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-relat 
/*:T*******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF002 0.00.00.002}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/*:T Preprocessadores do Template de Relat�rio                            */
/*:T Obs: Retirar o valor do preprocessador para as p�ginas que n�o existirem  */

&GLOBAL-DEFINE PGSEL f-pg-sel
/*
&GLOBAL-DEFINE PGCLA f-pg-cla
&GLOBAL-DEFINE PGPAR f-pg-par
&GLOBAL-DEFINE PGDIG f-pg-dig*/
&GLOBAL-DEFINE PGIMP f-pg-imp

&GLOBAL-DEFINE RTF   YES
  
/* Parameters Definitions ---                                           */

/* Temporary Table Definitions ---                                      */

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wemp             AS INTEGER FORMAT ">>9"
    FIELD westab           AS CHAR FORMAT "x(03)"
    FIELD wcod-emitente    AS INTEGER FORMAT ">>>>>9"
    FIELD wcod-emitente-fim  AS INTEGER FORMAT ">>>>>9"
    FIELD wnr-pl             AS CHAR FORMAT "999999X"
    FIELD westab-2           AS CHAR FORMAT "x(03)"
    FIELD wnum-pedido-ini    AS INTEGER FORMAT ">>,>>>,>>9"
    FIELD wnum-pedido-fim    AS INTEGER FORMAT ">>,>>>,>>9".
/*
define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.
 
define buffer b-tt-digita for tt-digita.
*/
/* Transfer Definitions */

def var raw-param        as raw no-undo.

def temp-table tt-raw-digita
   field raw-digita      as raw.
                    
/* Local Variable Definitions ---                                       */

def var l-ok               as logical no-undo.
def var c-arq-digita       as char    no-undo.
def var c-terminal         as char    no-undo.
def var c-rtf              as char    no-undo.
def var c-modelo-default   as char    no-undo.

/*15/02/2005 - tech1007 - Variavel definida para tratar se o programa est� rodando no WebEnabler*/
DEFINE SHARED VARIABLE hWenController AS HANDLE NO-UNDO.
DEF VAR wobs               LIKE alt-ped.observacao NO-UNDO.
DEF VAR wzera               AS LOGICAL FORMAT "Sim/Nao" INITIAL "no".
DEF VAR wsituacao           AS LOGICAL FORMAT "Sim/Nao" INITIAL "no".
DEF VAR wlinha              AS CHAR FORMAT "X(150)".


/*24/10/13 - variaveis para datas - lo-matdas*/

DEFINE VARIABLE wcont AS INTE FORMA 9.
DEFINE VARIABLE wcont-titulos AS INTE FORMAT 99.
DEFINE VARIABLE wdata AS CHAR FORMAT "x(09)"  EXTENT 10.
DEFINE VARIABLE wdata-new AS CHAR FORMAT "x(06)" extent 10.
DEFINE VARIABLE wdata-new-ordem AS   DATE FORMAT "99/99/9999" EXTENT 10.
DEFINE VARIABLE wdia AS INTEGER FORMAT "99".
DEFINE VARIABLE wmes  AS INTEGER FORMAT "99".
DEFINE VARIABLE wano  AS INTEGER FORMAT "9999".

DEFINE TEMP-TABLE wdata-formatada-esp
    FIELD wdata-format-1 AS DATE
    FIELD wdata-format-2 AS DATE
    FIELD wdata-format-3 AS DATE
    FIELD wdata-format-4 AS DATE
    FIELD wdata-format-5 AS DATE
    FIELD wdata-format-6 AS DATE
    FIELD wdata-format-7 AS DATE
    FIELD wdata-format-8 AS DATE
    FIELD wdata-format-9 AS DATE
    FIELD wdata-format-10 AS DATE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE w-relat
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-pg-imp

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-7 RECT-9 rect-rtf rs-destino ~
bt-config-impr bt-arquivo c-arquivo l-habilitaRtf bt-modelo-rtf rs-execucao ~
text-rtf text-modelo-rtf 
&Scoped-Define DISPLAYED-OBJECTS rs-destino c-arquivo l-habilitaRtf ~
c-modelo-rtf rs-execucao text-rtf text-modelo-rtf 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-relat AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-arquivo 
     IMAGE-UP FILE "image\im-sea":U
     IMAGE-INSENSITIVE FILE "image\ii-sea":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE BUTTON bt-config-impr 
     IMAGE-UP FILE "image\im-cfprt":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE BUTTON bt-modelo-rtf 
     IMAGE-UP FILE "image\im-sea":U
     IMAGE-INSENSITIVE FILE "image\ii-sea":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE VARIABLE c-arquivo AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 256
     SIZE 40 BY .88
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE c-modelo-rtf AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 256
     SIZE 40 BY .88
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE text-destino AS CHARACTER FORMAT "X(256)":U INITIAL " Destino" 
      VIEW-AS TEXT 
     SIZE 8.57 BY .63 NO-UNDO.

DEFINE VARIABLE text-modelo-rtf AS CHARACTER FORMAT "X(256)":U INITIAL "Modelo:" 
      VIEW-AS TEXT 
     SIZE 10.86 BY .63 NO-UNDO.

DEFINE VARIABLE text-modo AS CHARACTER FORMAT "X(256)":U INITIAL "Execu��o" 
      VIEW-AS TEXT 
     SIZE 10.86 BY .63 NO-UNDO.

DEFINE VARIABLE text-rtf AS CHARACTER FORMAT "X(256)":U INITIAL "Rich Text Format(RTF)" 
      VIEW-AS TEXT 
     SIZE 20.86 BY .63 NO-UNDO.

DEFINE VARIABLE rs-destino AS INTEGER INITIAL 2 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Impressora", 1,
"Arquivo", 2,
"Terminal", 3
     SIZE 44 BY 1.08 NO-UNDO.

DEFINE VARIABLE rs-execucao AS INTEGER INITIAL 1 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "On-Line", 1,
"Batch", 2
     SIZE 27.72 BY .92 NO-UNDO.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.29 BY 2.79.

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.29 BY 1.71.

DEFINE RECTANGLE rect-rtf
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.29 BY 3.54.

DEFINE VARIABLE l-habilitaRtf AS LOGICAL INITIAL no 
     LABEL "RTF" 
     VIEW-AS TOGGLE-BOX
     SIZE 44 BY 1.08 NO-UNDO.

DEFINE VARIABLE wcod-emitente AS INTEGER FORMAT ">>>>>9" INITIAL 1 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE wcod-emitente-fim AS INTEGER FORMAT ">>>>>9" INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE wemp AS INTEGER FORMAT ">>9" INITIAL 1 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .88 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "x(3)" INITIAL "1" 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE westab-2 AS CHARACTER FORMAT "x(3)" INITIAL "1" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE wnr-pl AS CHARACTER FORMAT "999999X" 
     LABEL "Nr.Plano" 
     VIEW-AS FILL-IN 
     SIZE 11.14 BY .88 NO-UNDO.

DEFINE VARIABLE wnum-pedido-fim AS INTEGER FORMAT ">>,>>>,>>9":U INITIAL 9999999 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wnum-pedido-ini AS INTEGER FORMAT ">>,>>>,>>9":U INITIAL 0 
     LABEL "No. Pedido" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE IMAGE IMAGE-10
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-5
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-6
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-7
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-8
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-9
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Fechar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-executar 
     LABEL "Relat�rio Sit. 2" 
     SIZE 15 BY 1.

DEFINE BUTTON bt-situacao AUTO-END-KEY 
     LABEL "Altera Situa��o Parcelas p/recebida" 
     SIZE 27 BY 1.

DEFINE BUTTON bt-zerar AUTO-END-KEY 
     LABEL "Zera QTD" 
     SIZE 10 BY 1.

DEFINE IMAGE im-pg-imp
     FILENAME "image\im-fldup":U
     SIZE 15.72 BY 1.21.

DEFINE IMAGE im-pg-sel
     FILENAME "image\im-fldup":U
     SIZE 15.72 BY 1.21.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 0    
     SIZE 78.72 BY .13
     BGCOLOR 7 .

DEFINE RECTANGLE rt-folder
     EDGE-PIXELS 1 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 11.38
     FGCOLOR 0 .

DEFINE RECTANGLE rt-folder-left
     EDGE-PIXELS 0    
     SIZE .43 BY 11.21
     BGCOLOR 15 .

DEFINE RECTANGLE rt-folder-right
     EDGE-PIXELS 0    
     SIZE .43 BY 11.17
     BGCOLOR 7 .

DEFINE RECTANGLE rt-folder-top
     EDGE-PIXELS 0    
     SIZE 78.72 BY .13
     BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-relat
     bt-executar AT ROW 14.5 COL 3 HELP
          "Dispara a execu��o do relat�rio"
     bt-zerar AT ROW 14.5 COL 19 HELP
          "Zera quantidades"
     bt-situacao AT ROW 14.5 COL 30 HELP
          "Muda situa��o ordens e prazo de compra para eliminada"
     bt-cancelar AT ROW 14.5 COL 58 HELP
          "Fechar"
     bt-ajuda AT ROW 14.54 COL 70 HELP
          "Ajuda"
     rt-folder-left AT ROW 2.54 COL 2.14
     rt-folder-right AT ROW 2.67 COL 80.43
     rt-folder-top AT ROW 2.54 COL 2.14
     RECT-6 AT ROW 13.75 COL 2.14
     RECT-1 AT ROW 14.29 COL 2
     rt-folder AT ROW 2.5 COL 2
     im-pg-imp AT ROW 1.5 COL 18
     im-pg-sel AT ROW 1.5 COL 2.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81 BY 15
         DEFAULT-BUTTON bt-executar.

DEFINE FRAME f-pg-imp
     rs-destino AT ROW 1.63 COL 3.29 HELP
          "Destino de Impress�o do Relat�rio" NO-LABEL
     bt-config-impr AT ROW 2.71 COL 43.29 HELP
          "Configura��o da impressora"
     bt-arquivo AT ROW 2.71 COL 43.29 HELP
          "Escolha do nome do arquivo"
     c-arquivo AT ROW 2.75 COL 3.29 HELP
          "Nome do arquivo de destino do relat�rio" NO-LABEL
     l-habilitaRtf AT ROW 4.83 COL 3.29
     c-modelo-rtf AT ROW 6.63 COL 3 HELP
          "Nome do arquivo de modelo do relat�rio" NO-LABEL
     bt-modelo-rtf AT ROW 6.63 COL 43 HELP
          "Escolha do nome do arquivo"
     rs-execucao AT ROW 8.88 COL 2.86 HELP
          "Modo de Execu��o" NO-LABEL
     text-destino AT ROW 1.04 COL 3.86 NO-LABEL
     text-rtf AT ROW 4.17 COL 1.14 COLON-ALIGNED NO-LABEL
     text-modelo-rtf AT ROW 5.96 COL 1.14 COLON-ALIGNED NO-LABEL
     text-modo AT ROW 8.13 COL 1.14 COLON-ALIGNED NO-LABEL
     RECT-7 AT ROW 1.33 COL 2.14
     RECT-9 AT ROW 8.33 COL 2
     rect-rtf AT ROW 4.46 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3 ROW 3
         SIZE 73.72 BY 10.5.

DEFINE FRAME f-pg-sel
     wemp AT ROW 3.75 COL 33 COLON-ALIGNED
     westab AT ROW 4.75 COL 33 COLON-ALIGNED
     westab-2 AT ROW 4.75 COL 53 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     wcod-emitente AT ROW 5.75 COL 33 COLON-ALIGNED
     wcod-emitente-fim AT ROW 5.75 COL 53 COLON-ALIGNED NO-LABEL WIDGET-ID 2
     wnum-pedido-ini AT ROW 6.75 COL 33 COLON-ALIGNED WIDGET-ID 30
     wnum-pedido-fim AT ROW 6.75 COL 53 COLON-ALIGNED NO-LABEL WIDGET-ID 36
     wnr-pl AT ROW 7.75 COL 33 COLON-ALIGNED
     IMAGE-5 AT ROW 5.75 COL 49 WIDGET-ID 20
     IMAGE-6 AT ROW 5.75 COL 52 WIDGET-ID 22
     IMAGE-7 AT ROW 4.75 COL 49 WIDGET-ID 26
     IMAGE-8 AT ROW 4.75 COL 52 WIDGET-ID 28
     IMAGE-9 AT ROW 6.75 COL 49 WIDGET-ID 32
     IMAGE-10 AT ROW 6.75 COL 52 WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3 ROW 2.85
         SIZE 76.86 BY 10.62.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: w-relat
   Allow: Basic,Browse,DB-Fields,Window,Query
   Add Fields to: Neither
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-relat ASSIGN
         HIDDEN             = YES
         TITLE              = "<Title>"
         HEIGHT             = 15
         WIDTH              = 81.14
         MAX-HEIGHT         = 22.33
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.33
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-relat 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-relat.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-relat
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-pg-imp
   FRAME-NAME                                                           */
/* SETTINGS FOR EDITOR c-modelo-rtf IN FRAME f-pg-imp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN text-destino IN FRAME f-pg-imp
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
ASSIGN 
       text-destino:PRIVATE-DATA IN FRAME f-pg-imp     = 
                "Destino".

ASSIGN 
       text-modelo-rtf:PRIVATE-DATA IN FRAME f-pg-imp     = 
                "Modelo:".

/* SETTINGS FOR FILL-IN text-modo IN FRAME f-pg-imp
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       text-modo:PRIVATE-DATA IN FRAME f-pg-imp     = 
                "Execu��o".

ASSIGN 
       text-rtf:PRIVATE-DATA IN FRAME f-pg-imp     = 
                "Rich Text Format(RTF)".

/* SETTINGS FOR FRAME f-pg-sel
                                                                        */
/* SETTINGS FOR FRAME f-relat
                                                                        */
/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder-left IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder-right IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder-top IN FRAME f-relat
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-relat)
THEN w-relat:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-pg-imp
/* Query rebuild information for FRAME f-pg-imp
     _Query            is NOT OPENED
*/  /* FRAME f-pg-imp */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-pg-sel
/* Query rebuild information for FRAME f-pg-sel
     _Query            is NOT OPENED
*/  /* FRAME f-pg-sel */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-relat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-relat w-relat
ON END-ERROR OF w-relat /* <Title> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
   RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-relat w-relat
ON WINDOW-CLOSE OF w-relat /* <Title> */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda w-relat
ON CHOOSE OF bt-ajuda IN FRAME f-relat /* Ajuda */
DO:
 /*  {include/ajuda.i}*/
MESSAGE 
     "1) �CONE ALTERA SITUA��O PARCELA PARA RECEBIDA: "
SKIP "   ESTE BOT�O ALTERA A SITUA��O NAS PARCELAS (PRAZO-COMPRA)"
SKIP "   PARA RECEBIDA, CONFORME AS DASTAS DO PLANO SELECIONADO. "
SKIP(1) "OBS.: A PARTIR DE 31/10, N�O ALTERA MAIS A ORDEM PARA RECEBIDA"
SKIP    "CONF. SOL. COMPRAS."
VIEW-AS ALERT-BOX.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME bt-arquivo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arquivo w-relat
ON CHOOSE OF bt-arquivo IN FRAME f-pg-imp
DO:
    {include/i-rparq.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancelar w-relat
ON CHOOSE OF bt-cancelar IN FRAME f-relat /* Fechar */
DO:
   apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME bt-config-impr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-config-impr w-relat
ON CHOOSE OF bt-config-impr IN FRAME f-pg-imp
DO:
   {include/i-rpimp.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar w-relat
ON CHOOSE OF bt-executar IN FRAME f-relat /* Relat�rio Sit. 2 */
DO:
   do  on error undo, return no-apply:
       run pi-executar.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME bt-modelo-rtf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-modelo-rtf w-relat
ON CHOOSE OF bt-modelo-rtf IN FRAME f-pg-imp
DO:
    def var c-arq-conv  as char no-undo.
    def var l-ok as logical no-undo.

    assign c-modelo-rtf = replace(input frame {&frame-name} c-modelo-rtf, "/", "\").
    SYSTEM-DIALOG GET-FILE c-arq-conv
       FILTERS "*.rtf" "*.rtf",
               "*.*" "*.*"
       DEFAULT-EXTENSION "rtf"
       INITIAL-DIR "modelos" 
       MUST-EXIST
       USE-FILENAME
       UPDATE l-ok.
    if  l-ok = yes then
        assign c-modelo-rtf:screen-value in frame {&frame-name}  = replace(c-arq-conv, "\", "/"). 

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-situacao
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-situacao w-relat
ON CHOOSE OF bt-situacao IN FRAME f-relat /* Altera Situa��o Parcelas p/recebida */
DO:
 RUN proc_situacao.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-zerar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-zerar w-relat
ON CHOOSE OF bt-zerar IN FRAME f-relat /* Zera QTD */
DO:
   RUN proc_zera.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME im-pg-imp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL im-pg-imp w-relat
ON MOUSE-SELECT-CLICK OF im-pg-imp IN FRAME f-relat
DO:
    run pi-troca-pagina.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME im-pg-sel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL im-pg-sel w-relat
ON MOUSE-SELECT-CLICK OF im-pg-sel IN FRAME f-relat
DO:
    run pi-troca-pagina.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME l-habilitaRtf
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL l-habilitaRtf w-relat
ON VALUE-CHANGED OF l-habilitaRtf IN FRAME f-pg-imp /* RTF */
DO:
    &IF "{&RTF}":U = "YES":U &THEN
    RUN pi-habilitaRtf.
    &endif
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs-destino
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-destino w-relat
ON VALUE-CHANGED OF rs-destino IN FRAME f-pg-imp
DO:
/*Alterado 15/02/2005 - tech1007 - Evento alterado para correto funcionamento dos novos widgets
  utilizados para a funcionalidade de RTF*/
do  with frame f-pg-imp:
    case self:screen-value:
        when "1" then do:
            assign c-arquivo:sensitive    = no
                   bt-arquivo:visible     = no
                   bt-config-impr:visible = YES
                   /*Alterado 17/02/2005 - tech1007 - Realizado teste de preprocessador para
                     verificar se o RTF est� ativo*/
                   &IF "{&RTF}":U = "YES":U &THEN
                   l-habilitaRtf:sensitive  = NO
                   l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp = "No"
                   l-habilitaRtf = NO
                   &endif
                   /*Fim alteracao 17/02/2005*/
                   .
        end.
        when "2" then do:
            assign c-arquivo:sensitive     = yes
                   bt-arquivo:visible      = yes
                   bt-config-impr:visible  = NO
                   /*Alterado 17/02/2005 - tech1007 - Realizado teste de preprocessador para
                     verificar se o RTF est� ativo*/
                   &IF "{&RTF}":U = "YES":U &THEN
                   l-habilitaRtf:sensitive  = YES
                   &endif
                   /*Fim alteracao 17/02/2005*/
                   .
        end.
        when "3" then do:
            assign c-arquivo:sensitive     = no
                   bt-arquivo:visible      = no
                   bt-config-impr:visible  = no
                   /*Alterado 17/02/2005 - tech1007 - Realizado teste de preprocessador para
                     verificar se o RTF est� ativo*/
                   &IF "{&RTF}":U = "YES":U &THEN
                   l-habilitaRtf:sensitive  = YES
                   &endif
                   /*Fim alteracao 17/02/2005*/
                   .
            /*Alterado 15/02/2005 - tech1007 - Teste para funcionar corretamente no WebEnabler*/
            &IF "{&RTF}":U = "YES":U &THEN
            IF VALID-HANDLE(hWenController) THEN DO:
                ASSIGN l-habilitaRtf:sensitive  = NO
                       l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp = "No"
                       l-habilitaRtf = NO.
            END.
            &endif
            /*Fim alteracao 15/02/2005*/
        end.
    end case.
end.
&IF "{&RTF}":U = "YES":U &THEN
RUN pi-habilitaRtf.
&endif
/*Fim alteracao 15/02/2005*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs-execucao
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-execucao w-relat
ON VALUE-CHANGED OF rs-execucao IN FRAME f-pg-imp
DO:
   {include/i-rprse.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-relat 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{utp/ut9000.i "ESPSF002" "0.00.00.002"}

/*:T inicializa��es do template de relat�rio */
{include/i-rpini.i}

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

{include/i-rplbl.i}

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO  ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
    ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
    RUN inicia.
    RUN enable_UI.
    
    {include/i-rpmbl.i}
  
    IF  NOT THIS-PROCEDURE:PERSISTENT THEN
        WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-relat  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-relat  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cria_altped w-relat 
PROCEDURE cria_altped :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Cria registro de altera��o*/
CREATE alt-ped.
ASSIGN alt-ped.num-pedido       = ordem-compra.num-pedido
       alt-ped.numero-ordem     = ordem-compra.numero-ordem
       alt-ped.parcela          = prazo-compra.parcela
       alt-ped.data             =  TODAY
       alt-ped.hora             = string(TIME,"HH:MM:SS"). 
       alt-ped.observacao       = wobs.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-relat  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-relat)
  THEN DELETE WIDGET w-relat.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-relat  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE im-pg-imp im-pg-sel bt-executar bt-zerar bt-situacao bt-cancelar 
         bt-ajuda 
      WITH FRAME f-relat IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-relat}
  DISPLAY wemp westab westab-2 wcod-emitente wcod-emitente-fim wnum-pedido-ini 
          wnum-pedido-fim wnr-pl 
      WITH FRAME f-pg-sel IN WINDOW w-relat.
  ENABLE IMAGE-5 IMAGE-6 IMAGE-7 IMAGE-8 IMAGE-9 IMAGE-10 wemp westab westab-2 
         wcod-emitente wcod-emitente-fim wnum-pedido-ini wnum-pedido-fim wnr-pl 
      WITH FRAME f-pg-sel IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-pg-sel}
  DISPLAY rs-destino c-arquivo l-habilitaRtf c-modelo-rtf rs-execucao text-rtf 
          text-modelo-rtf 
      WITH FRAME f-pg-imp IN WINDOW w-relat.
  ENABLE RECT-7 RECT-9 rect-rtf rs-destino bt-config-impr bt-arquivo c-arquivo 
         l-habilitaRtf bt-modelo-rtf rs-execucao text-rtf text-modelo-rtf 
      WITH FRAME f-pg-imp IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-pg-imp}
  VIEW w-relat.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia w-relat 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN wnr-pl = string(MONTH(TODAY),"99") + string(YEAR(TODAY),"9999").
DISPLAY wnr-pl WITH FRAME f-pg-sel.
DO wcont-titulos = 1 TO 10:
   ASSIGN wdata[wcont-titulos] = " ".
   END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-relat 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-executar w-relat 
PROCEDURE pi-executar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define var r-tt-digita as rowid no-undo.

do on error undo, return error on stop  undo, return error:
    {include/i-rpexa.i}
    /*14/02/2005 - tech1007 - Alterada condicao para n�o considerar mai o RTF como destino*/
    if input frame f-pg-imp rs-destino = 2 and
       input frame f-pg-imp rs-execucao = 1 then do:
        run utp/ut-vlarq.p (input input frame f-pg-imp c-arquivo).
        
        if return-value = "NOK":U then do:
            run utp/ut-msgs.p (input "show":U, input 73, input "").
            
            apply "MOUSE-SELECT-CLICK":U to im-pg-imp in frame f-relat.
            apply "ENTRY":U to c-arquivo in frame f-pg-imp.
            return error.
        end.
    end.

    /*14/02/2005 - tech1007 - Teste efetuado para nao permitir modelo em branco*/
    &IF "{&RTF}":U = "YES":U &THEN
    IF ( INPUT FRAME f-pg-imp c-modelo-rtf = "" AND
         INPUT FRAME f-pg-imp l-habilitaRtf = "Yes" ) OR
       ( SEARCH(INPUT FRAME f-pg-imp c-modelo-rtf) = ? AND
         input frame f-pg-imp rs-execucao = 1 AND
         INPUT FRAME f-pg-imp l-habilitaRtf = "Yes" )
         THEN DO:
        run utp/ut-msgs.p (input "show":U, input 73, input "").        
        apply "MOUSE-SELECT-CLICK":U to im-pg-imp in frame f-relat.
        /*30/12/2004 - tech1007 - Evento removido pois causa problemas no WebEnabler*/
        /*apply "CHOOSE":U to bt-modelo-rtf in frame f-pg-imp.*/
        return error.
    END.
    &endif
    /*Fim teste Modelo*/
    
    /*:T Coloque aqui as valida��es da p�gina de Digita��o, lembrando que elas devem
       apresentar uma mensagem de erro cadastrada, posicionar nesta p�gina e colocar
       o focus no campo com problemas */
    /*browse br-digita:SET-REPOSITIONED-ROW (browse br-digita:DOWN, "ALWAYS":U).*/
    
    /*for each tt-digita no-lock:
        assign r-tt-digita = rowid(tt-digita).
        
        /*:T Valida��o de duplicidade de registro na temp-table tt-digita */
        find first b-tt-digita where b-tt-digita.ordem = tt-digita.ordem and 
                                     rowid(b-tt-digita) <> rowid(tt-digita) no-lock no-error.
        if avail b-tt-digita then do:
            apply "MOUSE-SELECT-CLICK":U to im-pg-dig in frame f-relat.
            reposition br-digita to rowid rowid(b-tt-digita).
            
            run utp/ut-msgs.p (input "show":U, input 108, input "").
            apply "ENTRY":U to tt-digita.ordem in browse br-digita.
            
            return error.
        end.
        
        /*:T As demais valida��es devem ser feitas aqui */
        if tt-digita.ordem <= 0 then do:
            assign browse br-digita:CURRENT-COLUMN = tt-digita.ordem:HANDLE in browse br-digita.
            
            apply "MOUSE-SELECT-CLICK":U to im-pg-dig in frame f-relat.
            reposition br-digita to rowid r-tt-digita.
            
            run utp/ut-msgs.p (input "show":U, input 99999, input "").
            apply "ENTRY":U to tt-digita.ordem in browse br-digita.
            
            return error.
        end.
        
    end.*/
    
    
    /*:T Coloque aqui as valida��es das outras p�ginas, lembrando que elas devem 
       apresentar uma mensagem de erro cadastrada, posicionar na p�gina com 
       problemas e colocar o focus no campo com problemas */
    
    
    
    /*:T Aqui s�o gravados os campos da temp-table que ser� passada como par�metro
       para o programa RP.P */
    
    create tt-param.
    assign tt-param.usuario         = c-seg-usuario
           tt-param.destino         = input frame f-pg-imp rs-destino
           tt-param.data-exec       = today
           tt-param.hora-exec       = time
          /* tt-param.classifica      = input frame f-pg-cla rs-classif
           tt-param.desc-classifica = entry((tt-param.classifica - 1) * 2 + 1, 
                                            rs-classif:radio-buttons in frame f-pg-cla)*/
           &IF "{&RTF}":U = "YES":U &THEN 
           tt-param.modelo-rtf      = INPUT FRAME f-pg-imp c-modelo-rtf
           /*Alterado 14/02/2005 - tech1007 - Armazena a informa��o se o RTF est� habilitado ou n�o*/
           tt-param.l-habilitaRtf     = INPUT FRAME f-pg-imp l-habilitaRtf
           /*Fim alteracao 14/02/2005*/ 
           &endif
           .
    
    /*Alterado 14/02/2005 - tech1007 - Alterado o teste para verificar se a op��o de RTF est� selecionada*/
    if tt-param.destino = 1 
    then assign tt-param.arquivo = "".
    else if  tt-param.destino = 2
         then assign tt-param.arquivo = input frame f-pg-imp c-arquivo.
         else assign tt-param.arquivo = session:temp-directory + c-programa-mg97 + ".tmp":U.
    /*Fim alteracao 14/02/2005*/

    /*:T Coloque aqui a/l�gica de grava��o dos demais campos que devem ser passados
       como par�metros para o programa RP.P, atrav�s da temp-table tt-param */
    
    ASSIGN tt-param.wemp               = INPUT FRAME f-pg-sel wemp
           tt-param.westab             = INPUT FRAME f-pg-sel westab
           tt-param.wcod-emitente      = INPUT FRAME f-pg-sel wcod-emitente
           tt-param.wcod-emitente-fim  = INPUT FRAME f-pg-sel wcod-emitente-fim
           tt-param.wnr-pl              = INPUT FRAME f-pg-sel wnr-pl
           tt-param.westab-2           = INPUT FRAME f-pg-sel westab-2
           tt-param.wnum-pedido-ini    = INPUT FRAME f-pg-sel wnum-pedido-ini  
           tt-param.wnum-pedido-fim    = INPUT FRAME f-pg-sel wnum-pedido-fim.
    /*:T Executar do programa RP.P que ir� criar o relat�rio */
    {include/i-rpexb.i}
    
    SESSION:SET-WAIT-STATE("general":U).
    
    {include/i-rprun.i espsf/espsf002rp.p}
    
    {include/i-rpexc.i}
    
    SESSION:SET-WAIT-STATE("":U).
    
    {include/i-rptrm.i}
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-troca-pagina w-relat 
PROCEDURE pi-troca-pagina :
/*:T------------------------------------------------------------------------------
  Purpose: Gerencia a Troca de P�gina (folder)   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{include/i-rptrp.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_datas w-relat 
PROCEDURE proc_datas :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /* transforma data lo-matdatas para formato de data */
    
    MESSAGE "AQUI PROC DATAS " VIEW-AS ALERT-BOX.
    /* limpara temp-table */
    FOR EACH  wdata-formatada-esp:
       DELETE wdata-formatada-esp.
    END.

    CREATE wdata-formatada-esp.

    FIND FIRST lo-matdatas WHERE lo-matdatas.emp    =    INPUT FRAME f-pg-sel wemp
                           AND   lo-matdatas.estab  GE   INPUT FRAME f-pg-sel westab
                           AND   lo-matdatas.estab  LE   INPUT FRAME f-pg-sel westab
                           AND   lo-matdatas.nr-pl   =   INPUT FRAME f-pg-sel wnr-pl
                           NO-LOCK NO-ERROR.
    DO wcont-titulos = 1 TO 10:
     ASSIGN wdata[wcont-titulos] = "" 
            wdata-new[wcont-titulos] = ""
            wdata-new-ordem[wcont-titulos] = ?.
     END.      
    ASSIGN  wdata[1] = lo-matdatas.titsem1
            wdata[2] = lo-matdatas.titsem2
            wdata[3] = lo-matdatas.titsem3
            wdata[4] = lo-matdatas.titsem4
            wdata[5] = lo-matdatas.titsem5
            wdata[6] = lo-matdatas.titsem1-2
            wdata[7] = lo-matdatas.titsem2-2
            wdata[8] = lo-matdatas.titsem3-2
            wdata[9] = lo-matdatas.titsem4-2
            wdata[10] = lo-matdatas.titsem5-2.

     DO wcont-titulos = 1 TO 10:
      /*   DISPLAY wdata[wcont-titulos].*/
        DO wcont = 1 TO 9.
          IF  SUBSTRING(wdata[wcont-titulos],wcont,1) NE "" THEN DO:
          ASSIGN wdata-new[wcont-titulos] = wdata-new[wcont-titulos] + SUBSTRING(wdata[wcont-titulos],wcont,1).
          END.
             ASSIGN wano = INTEGER((SUBSTRING(lo-matdatas.nr-pl,3,4))).
/*           DISPLAY wdata-new[wcont-titulos] wcont-titulos WITH FRAME xx DOWN. */
          
          ASSIGN  wdia =  INTEGER((SUBSTRING(wdata-new[wcont-titulos],1,2))).
       /*   DISPLAY wdia.*/
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "jan" THEN ASSIGN wmes = 01.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "fev" THEN ASSIGN wmes = 02.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "mar" THEN ASSIGN wmes = 03.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "abr" THEN ASSIGN wmes = 04.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "mai" THEN ASSIGN wmes = 05.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "jun" THEN ASSIGN wmes = 06.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "jul" THEN ASSIGN wmes = 07.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "ago" THEN ASSIGN wmes = 08.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "set" THEN ASSIGN wmes = 09.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "out" THEN ASSIGN wmes = 10.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "nov" THEN ASSIGN wmes = 11.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "dez" THEN ASSIGN wmes = 12.
  
          
          /* tratamento final de ano */
           IF SUBSTRING(lo-matdatas.nr-pl,1,2) = "12"
           AND SUBSTRING(wdata-new[wcont-titulos],4,3) = "jan" THEN ASSIGN wano = INTEGER((SUBSTRING(lo-matdatas.nr-pl,3,4))) + 1.   
          
       
         END.
         ASSIGN wdata-new-ordem[wcont-titulos] = date(wmes,wdia,wano).
         /*
         DISPLAY wdata-new-ordem[wcont-titulos] wdata-new[wcont-titulos] wcont-titulos WITH FRAME xx DOWN.
         */
         
         ASSIGN  
          wdata-formatada-esp.wdata-format-1= wdata-new-ordem[1]
          wdata-formatada-esp.wdata-format-2= wdata-new-ordem[2]
          wdata-formatada-esp.wdata-format-3= wdata-new-ordem[3]
          wdata-formatada-esp.wdata-format-4= wdata-new-ordem[4]
          wdata-formatada-esp.wdata-format-5= wdata-new-ordem[5]
          wdata-formatada-esp.wdata-format-6= wdata-new-ordem[6]
          wdata-formatada-esp.wdata-format-7= wdata-new-ordem[7]
          wdata-formatada-esp.wdata-format-8= wdata-new-ordem[8]
          wdata-formatada-esp.wdata-format-9= wdata-new-ordem[9]
          wdata-formatada-esp.wdata-format-10= wdata-new-ordem[10].
         
     /*    DISPLAY wdata-formatada-esp  WITH FRAME xx DOWN.*/
         
      DOWN.
    END. 
    /* fim - rotina seta datas */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_situacao w-relat 
PROCEDURE proc_situacao :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var h-acomp         as handle no-undo.    
run utp/ut-acomp.p persistent set h-acomp.  
    
{utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
run pi-inicializar in h-acomp (input "Inicializando":U). 
wlinha = FILL("*",150).
MESSAGE "CONFIRMA ALTERA��O DE SITUACAO PARA RECEBIDA, CONFORME SELE��O NOS PAR�METROS? " 
       VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "" UPDATE wsituacao.

   IF wsituacao = YES  THEN DO:
     DISABLE ALL WITH FRAME f-pg-sel.   
     OUTPUT TO VALUE(SESSION:TEMP-DIRECTORY + "situacao.log") PAGE-SIZE 60.
     /*RUN proc_datas. */
     

     FOR EACH lo-matplano WHERE lo-matplano.emp                =  INPUT FRAME f-pg-sel wemp
                        AND   lo-matplano.cod-emitente       GE   INPUT FRAME f-pg-sel wcod-emitente
                        AND   lo-matplano.cod-emitente       LE   INPUT FRAME f-pg-sel wcod-emitente-fim
                        AND   lo-matplano.nr-pl              =    INPUT FRAME f-pg-sel wnr-pl
                        AND   lo-matplano.estab              GE   INPUT FRAME f-pg-sel westab
                        AND   lo-matplano.estab              LE   INPUT FRAME f-pg-sel westab-2.
      
    /* fim - rotina seta datas */
    
       /* inicio alteracao */
       run pi-acompanhar in h-acomp (input "ITEM: " +  (lo-matplano.it-codigo)).
       FOR EACH ordem-compra WHERE ordem-compra.it-codigo = lo-matplano.it-codigo
                          AND   ordem-compra.situacao = 2  /* confirmada*/
                          AND   ordem-compra.cod-emitente = lo-matplano.cod-emitente
                          AND   (ordem-compra.num-pedido GE tt-param.wnum-pedido-ini
                          AND   ordem-compra.num-pedido LE tt-param.wnum-pedido-fim):
                       
         
       /* conf. sol. wallace 31/10  n�o altera mais a ordem para recebida, somente prazo compra - parcela
       ASSIGN ordem-compra.situacao = 6.*/
     
       DO wcont-titulos = 1 TO 10:
         ASSIGN wdata[wcont-titulos] = "" 
              wdata-new[wcont-titulos] = ""
              wdata-new-ordem[wcont-titulos] = ?
              wcont = 0.
         END.  
    /* MESSAGE "AQUI PROC DATAS " VIEW-AS ALERT-BOX. */
    /* limpar  temp-table */
    FOR EACH  wdata-formatada-esp:
       DELETE wdata-formatada-esp.
    END.
    /* limpar  temp-table */
    CREATE wdata-formatada-esp.

    FIND FIRST lo-matdatas WHERE lo-matdatas.emp    =    INPUT FRAME f-pg-sel wemp
                           AND   lo-matdatas.estab  GE   INPUT FRAME f-pg-sel westab
                           AND   lo-matdatas.estab  LE   INPUT FRAME f-pg-sel westab
                           AND   lo-matdatas.nr-pl   =   INPUT FRAME f-pg-sel wnr-pl
                           NO-LOCK NO-ERROR.
    
    ASSIGN  wdata[1] = lo-matdatas.titsem1
            wdata[2] = lo-matdatas.titsem2
            wdata[3] = lo-matdatas.titsem3
            wdata[4] = lo-matdatas.titsem4
            wdata[5] = lo-matdatas.titsem5
            wdata[6] = lo-matdatas.titsem1-2
            wdata[7] = lo-matdatas.titsem2-2
            wdata[8] = lo-matdatas.titsem3-2
            wdata[9] = lo-matdatas.titsem4-2
            wdata[10] = lo-matdatas.titsem5-2.

     DO wcont-titulos = 1 TO 10:
      /*   DISPLAY wdata[wcont-titulos].*/
        DO wcont = 1 TO 9.
          IF  SUBSTRING(wdata[wcont-titulos],wcont,1) NE "" THEN DO:
          ASSIGN wdata-new[wcont-titulos] = wdata-new[wcont-titulos] + SUBSTRING(wdata[wcont-titulos],wcont,1).
          END.
             ASSIGN wano = INTEGER((SUBSTRING(lo-matdatas.nr-pl,3,4))).
             /* DISPLAY wdata-new[wcont-titulos] wcont-titulos WITH FRAME xx DOWN. */
          
          ASSIGN  wdia =  INTEGER((SUBSTRING(wdata-new[wcont-titulos],1,2))).
          /*   DISPLAY wdia.*/
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "jan" THEN ASSIGN wmes = 01.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "fev" THEN ASSIGN wmes = 02.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "mar" THEN ASSIGN wmes = 03.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "abr" THEN ASSIGN wmes = 04.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "mai" THEN ASSIGN wmes = 05.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "jun" THEN ASSIGN wmes = 06.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "jul" THEN ASSIGN wmes = 07.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "ago" THEN ASSIGN wmes = 08.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "set" THEN ASSIGN wmes = 09.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "out" THEN ASSIGN wmes = 10.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "nov" THEN ASSIGN wmes = 11.
          IF  SUBSTRING(wdata-new[wcont-titulos],4,3) = "dez" THEN ASSIGN wmes = 12.
  
          
          /* tratamento final de ano */
           IF SUBSTRING(lo-matdatas.nr-pl,1,2) = "12"
           AND SUBSTRING(wdata-new[wcont-titulos],4,3) = "jan" THEN ASSIGN wano = INTEGER((SUBSTRING(lo-matdatas.nr-pl,3,4))) + 1.   
          
       
         END.
         ASSIGN wdata-new-ordem[wcont-titulos] = date(wmes,wdia,wano).
         /*DISPLAY wdata-new-ordem[wcont-titulos] wdata-new[wcont-titulos] wcont-titulos WITH FRAME xx DOWN.*/
         
         ASSIGN  
          wdata-formatada-esp.wdata-format-1= wdata-new-ordem[1]
          wdata-formatada-esp.wdata-format-2= wdata-new-ordem[2]
          wdata-formatada-esp.wdata-format-3= wdata-new-ordem[3]
          wdata-formatada-esp.wdata-format-4= wdata-new-ordem[4]
          wdata-formatada-esp.wdata-format-5= wdata-new-ordem[5]
          wdata-formatada-esp.wdata-format-6= wdata-new-ordem[6]
          wdata-formatada-esp.wdata-format-7= wdata-new-ordem[7]
          wdata-formatada-esp.wdata-format-8= wdata-new-ordem[8]
          wdata-formatada-esp.wdata-format-9= wdata-new-ordem[9]
          wdata-formatada-esp.wdata-format-10= wdata-new-ordem[10].
         
     /*    DISPLAY wdata-formatada-esp  WITH FRAME xx DOWN.*/
         
      DOWN.
  /*  END.*/ /* fim - rotina seta datas */
/*       /* header f-lista 1 logn */                                                                                                       */
/*       FORM HEADER                                                                                                                       */
/*          "DATAS DO PLANO: " lo-matdatas.nr-pl wdata-format-1 wdata-format-2 wdata-format-3 wdata-format-4 wdata-format-5 wdata-format-6 */
/*          wdata-format-7 wdata-format-8 wdata-format-9 wdata-format-10                                                                   */
/*          SKIP   WITH FRAME f-lista1 PAGE-TOP NO-LABELS.                                                                                 */
/*                                                                                                                                         */
/*       /* fim header */                                                                                                                  */

         FOR EACH prazo-compra OF ordem-compra  WHERE prazo-compra.situacao = 2 
                  AND prazo-compra.data-entrega = wdata-new-ordem[wcont-titulos] /*confirmada*/:
   
           /*  MESSAGE  prazo-compra.numero-ordem wdata-new-ordem[wcont-titulos] VIEW-AS ALERT-BOX.*/
            
             ASSIGN prazo-compra.situacao = 6. /*(teste)*/
            
             DISPLAY "ORDEM COMPRA: "  prazo-compra.data-entrega ordem-compra.numero-ordem lo-matplano.nr-pl ordem-compra.it-codigo 
            ordem-compra.num-pedido ordem-compra.situacao VIEW-AS TEXT
            " ---- PRAZO COMPRA: " prazo-compra.numero-ordem prazo-compra.parcela prazo-compra.situacao VIEW-AS TEXT
          
            WITH STREAM-IO WIDTH 150 TITLE "LOG ORDENS ALTERADAS PARA RECEBIDAS - Pg.: " + STRING(PAGE-NUMBER).
            PUT wlinha.
            DOWN WITH FRAME f-lista1.
            ASSIGN wobs = "SITUACAO ALTERADA PARA RECEBIDA POR: " + CAPS(STRING(USERID(ldbname(1)),"x(11)")) + "-" + STRING(TODAY,"99/99/9999").
            RUN cria_altped.  /*(teste) */
            END.
          END.  
        END.
      END. /* fim - rotina seta datas */
        OUTPUT CLOSE.
        DOS SILENT START notepad.exe VALUE(SESSION:TEMP-DIRECTORY + "situacao.log").
        ENABLE ALL WITH FRAME f-pg-sel. 
       
    END.
        run pi-finalizar in h-acomp.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_zera w-relat 
PROCEDURE proc_zera :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
def var h-acomp         as handle no-undo.    
run utp/ut-acomp.p persistent set h-acomp.  
{utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
run pi-inicializar in h-acomp (input "Inicializando":U). 

wlinha = FILL("*",150).
MESSAGE "CONFIRMA ZERAMENTO DOS CAMPOS DE QUANTIDADES, CONFORME PARAMETRO SELECIONADO?"
        SKIP
        "PROGRAMA ALTERADO EM 02/09/15 PARA ZERAR SOMENTE QTD-SAL-FORN"
       VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO TITLE "" UPDATE wzera.
   IF wzera = YES  THEN DO:
     DISABLE ALL WITH FRAME f-pg-sel.   
     OUTPUT TO VALUE(SESSION:TEMP-DIRECTORY + "zerados.log") PAGE-SIZE 60.
     FOR EACH lo-matplano WHERE lo-matplano.emp                =  INPUT FRAME f-pg-sel wemp
                        AND   lo-matplano.cod-emitente       GE  INPUT FRAME f-pg-sel wcod-emitente
                        AND   lo-matplano.cod-emitente       LE  INPUT FRAME f-pg-sel wcod-emitente-fim
                        AND   lo-matplano.nr-pl              =  INPUT FRAME f-pg-sel wnr-pl
                        AND   lo-matplano.estab              GE  INPUT FRAME f-pg-sel westab
                        AND   lo-matplano.estab              LE  INPUT FRAME f-pg-sel westab-2
                        NO-LOCK:
        
       run pi-acompanhar in h-acomp (input "ITEM: " +  (lo-matplano.it-codigo)).
       FOR EACH ordem-compra WHERE ordem-compra.it-codigo = lo-matplano.it-codigo
                          AND   ordem-compra.situacao = 2  /* confirmada*/
                          AND   ordem-compra.cod-emitente = lo-matplano.cod-emitente
                          AND   (ordem-compra.num-pedido GE tt-param.wnum-pedido-ini
                          AND   ordem-compra.num-pedido LE tt-param.wnum-pedido-fim):

           DISPLAY "PLANO" lo-matplano.nr-pl ordem-compra.it-codigo 
            ordem-compra.num-pedido ordem-compra.numero-ordem ordem-compra.situacao VIEW-AS TEXT 
                WITH STREAM-IO WIDTH 80 TITLE "LOG DE ITENS COM QUANTIDADES ZERADAS - Pg.: " + STRING(PAGE-NUMBER).
             
          
         FOR EACH prazo-compra OF ordem-compra  WHERE prazo-compra.situacao = 2 /*confirmada*/:
            DISPLAY 
            "ANTES DA ALTERACAO:" prazo-compra.numero-ordem prazo-compra.parcela prazo-compra.it-codigo prazo-compra.quantidade 
                   prazo-compra.situacao  VIEW-AS TEXT prazo-compra.quant-saldo prazo-compra.qtd-do-forn prazo-compra.qtd-sal-forn
              WITH FRAME f-lista DOWN STREAM-IO WIDTH 150.
            DOWN WITH FRAME f-lista.
            /* Zera qtd -*/
           
            ASSIGN prazo-compra.quantidade   = 0 /* quantidade cc0300*/
              prazo-compra.quant-saldo  = 0
              prazo-compra.qtd-do-forn  = 0
              prazo-compra.qtd-sal-forn = 0. /* quantidade cc0300*/
           
            /* conf. wallace e z� luiz em 02/09/15 - passou a zerar somente a quantidade do fornecedor */
            /* Pode consultar no cc0300 / cc0300b parcelas 
            ASSIGN  prazo-compra.qtd-sal-forn = 0.*/
             
            DISPLAY 
              "DEPOIS DA ALTERACAO:" prazo-compra.numero-ordem prazo-compra.parcela prazo-compra.it-codigo prazo-compra.quantidade 
                   prazo-compra.situacao  VIEW-AS TEXT prazo-compra.quant-saldo prazo-compra.qtd-do-forn prazo-compra.qtd-sal-forn
               WITH FRAME f-lista1 WIDTH 180 DOWN STREAM-IO.
            PUT wlinha.
            DOWN WITH FRAME f-lista1.
            ASSIGN wobs = "CAMPOS QUANTIDADES ZERADOS POR: " + CAPS(STRING(USERID(ldbname(1)),"x(11)")) + "-" + STRING(TODAY,"99/99/9999").
            RUN cria_altped.   
            END.
          END.
        END.
        OUTPUT CLOSE.
        DOS SILENT START notepad.exe VALUE(SESSION:TEMP-DIRECTORY + "zerados.log").
       ENABLE ALL WITH FRAME f-pg-sel. 
    END.
     run pi-finalizar in h-acomp.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-relat  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this w-relat, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-relat 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE zera_qtd w-relat 
PROCEDURE zera_qtd :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

