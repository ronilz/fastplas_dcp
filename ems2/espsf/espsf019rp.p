&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPF019RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wcod-estab-i     AS CHAR FORMAT "X(03)"
    FIELD wcod-estab-f     AS CHAR FORMAT "X(03)"
    FIELD wemit-i          AS INTE FORMAT ">>>>>>>>9"
    FIELD wemit-f          AS INTE FORMAT  ">>>>>>>>9"
    FIELD wnr-docto-i      AS CHAR FORMAT "X(16)"
    FIELD wnr-docto-f      AS CHAR FORMAT "X(16)"  
    FIELD wnat-i           AS CHAR FORMAT "X(06)"
    FIELD wnat-f           AS CHAR FORMAT "X(06)"
    FIELD wdt-trans-i      AS DATE FORMAT "99/99/9999"
    FIELD wdt-trans-f      AS DATE FORMAT "99/99/9999".
define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PROC.ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Relat�rio Doc.Recebimento p/confer�ncia".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U). */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
   FOR EACH docum-est WHERE docum-est.cod-estabel GE tt-param.wcod-estab-i   
                      AND  docum-est.cod-estabel  LE tt-param.wcod-estab-f     
                      AND  docum-est.cod-emitente GE tt-param.wemit-i 
                      AND  docum-est.cod-emitente LE tt-param.wemit-f
                      AND  docum-est.nro-docto    GE tt-param.wnr-docto-i   
                      AND  docum-est.nro-docto    LE tt-param.wnr-docto-f   
                      AND  docum-est.nat-operacao GE tt-param.wnat-i 
                      AND  docum-est.nat-operacao LE tt-param.wnat-f       
                      AND  docum-est.dt-trans     GE tt-param.wdt-trans-i 
                      AND  docum-est.dt-trans     LE tt-param.wdt-trans-f  
       NO-LOCK BREAK BY docum-est.nro-docto:
       FIND FIRST emitente OF docum-est NO-LOCK NO-ERROR.
/*        MESSAGE nro-docto  dt-trans nat-operacao cod-estabel docum-est.cod-emitente dt-trans VIEW-AS ALERT-BOX.  */
       FOR EACH item-doc-est OF docum-est NO-LOCK:

        FIND ITEM OF item-doc-est NO-LOCK.
        DISPLAY  docum-est.cod-estabel item.codigo-refer  docum-est.cod-emitente emitente.nome-abrev  docum-est.dt-trans item-doc-est.sequencia     item-doc-est.it-codigo item-doc-est.quantidade 
        item-doc-est.preco-total[1] docum-est.nat-operacao  docum-est.nro-docto  WITH WIDTH 200 DOWN WITH FRAME f-relat STREAM-IO.
        DOWN WITH FRAME f-relat.
        /*
        DISPLAY docum-est.cod-estabel  docum-est.cod-emitente "    " item.codigo-refer "    " item-doc-est.sequencia "   "  item-doc-est.it-codigo item-doc-est.quantidade 
                item-doc-est.preco-total[1] docum-est.nat-operacao "     " item-doc-est.nro-docto  WITH WIDTH 200 DOWN WITH FRAME f-relat STREAM-IO.
                DOWN WITH FRAME f-relat.
                */

        run pi-acompanhar in h-acomp ("Documento: " + STRING(item-doc-est.nro-docto) + " - Data: " + STRING(docum-est.dt-trans)).
        END.
        IF LAST-OF(docum-est.nro-docto) THEN PUT SKIP FILL("-",161) FORMAT "X(161)" SKIP.
     END.

END PROCEDURE.




