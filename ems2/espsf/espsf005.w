&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
 

DEFINE VARIABLE witem-prefixo AS CHAR FORMAT "X(7)".
DEFINE VARIABLE witem-basico  AS CHAR FORMAT "X(9)".
DEFINE VARIABLE witem-sufixo  AS CHAR FORMAT "X(6)".
DEFINE VARIABLE wvezes        AS INTE FORMAT "99".
DEFINE VARIABLE wpos          AS INTE FORMAT "99".
DEFINE VARIABLE wfirst        AS INTE FORMAT "99".
DEFINE VARIABLE wlast         AS INTE FORMAT "99".

DEFINE VARIABLE wcont AS INTE FORMAT "99".
DEFINE VARIABLE wcont2 AS INTE FORMAT "99".
DEFINE BUFFER b-nota FOR nota-fiscal.
DEFINE STREAM w-cons.
DEFINE STREAM w-comple.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 wemit wdt-ini wdt-fim wrel1 ~
BUTTON-1 wrel2 BUTTON-2 BtnOK BtnDone WLAY 
&Scoped-Define DISPLAYED-OBJECTS wemit wdt-ini wdt-fim wrel1 wrel2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON BtnDone DEFAULT 
     LABEL "Sair" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     LABEL "Executar" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "image/im-consulta.bmp":U
     LABEL "Button 1" 
     SIZE 8 BY 1.13.

DEFINE BUTTON BUTTON-2 
     IMAGE-UP FILE "image/im-consulta.bmp":U
     LABEL "Button 2" 
     SIZE 8 BY 1.13.

DEFINE BUTTON WLAY 
     LABEL "CONSULTA LAYOUT" 
     SIZE 20 BY 1.13.

DEFINE VARIABLE wdt-fim AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Per�odo" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wemit AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "C�digo emitente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wrel1 AS CHARACTER FORMAT "X(35)":U INITIAL "C:/SPOOL/CONSIS-Ford-Barueri.TXT" 
     LABEL "Relat�rio de consist�ncia" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE wrel2 AS CHARACTER FORMAT "X(35)":U INITIAL "C:/SPOOL/COMPL-Ford-Barueri.TXT" 
     LABEL "Arquivo layout" 
     VIEW-AS FILL-IN 
     SIZE 35 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 75 BY 12.25.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 75 BY 1.75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wemit AT ROW 4.5 COL 27 COLON-ALIGNED WIDGET-ID 8
     wdt-ini AT ROW 5.75 COL 27 COLON-ALIGNED WIDGET-ID 10
     wdt-fim AT ROW 5.75 COL 45 COLON-ALIGNED NO-LABEL WIDGET-ID 12
     wrel1 AT ROW 7.25 COL 27 COLON-ALIGNED WIDGET-ID 14
     BUTTON-1 AT ROW 7.25 COL 65 WIDGET-ID 18
     wrel2 AT ROW 8.5 COL 27 COLON-ALIGNED WIDGET-ID 16
     BUTTON-2 AT ROW 8.5 COL 65 WIDGET-ID 20
     BtnOK AT ROW 14.5 COL 10 WIDGET-ID 6
     BtnDone AT ROW 14.5 COL 26 WIDGET-ID 4
     WLAY AT ROW 14.5 COL 57 WIDGET-ID 26
     RECT-1 AT ROW 1.5 COL 4 WIDGET-ID 22
     RECT-2 AT ROW 14.25 COL 3 WIDGET-ID 24
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 79.29 BY 15.54
         DEFAULT-BUTTON BtnOK WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Gera��o arq. layout Ford - NF complementar - espsf005"
         HEIGHT             = 15.54
         WIDTH              = 79.29
         MAX-HEIGHT         = 29.71
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.71
         VIRTUAL-WIDTH      = 146.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Gera��o arq. layout Ford - NF complementar - espsf005 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Gera��o arq. layout Ford - NF complementar - espsf005 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnDone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnDone C-Win
ON CHOOSE OF BtnDone IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnOK C-Win
ON CHOOSE OF BtnOK IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN wemit wdt-ini wdt-fim.
  DISABLE ALL WITH FRAME  {&FRAME-NAME}.
  RUN relatorio.
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  
      DOS SILENT START notepad.exe VALUE(wrel1).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME DEFAULT-FRAME /* Button 2 */
DO:
  DOS SILENT START notepad.exe VALUE(wrel2).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME WLAY
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL WLAY C-Win
ON CHOOSE OF WLAY IN FRAME DEFAULT-FRAME /* CONSULTA LAYOUT */
DO:
OUTPUT TO VALUE(SESSION:TEMP-DIR + "LAYOUT-FORD.TXT") PAGED.

DISPLAY
"LAYOUT FORD - NOTAS FISCAIS COMPLEMENTARES"
SKIP
"  REGISTRO TIPO 1:"
SKIP 
"** INFORMACAO **        ** POSICAO **     ** OBSERVACAO **"   
SKIP          
" TIPO REGISTRO              1  A   1       VALOR FIXO = 1"                
SKIP   
" CNPJ FORD                  2  A  15       FORMATO =  99999999999999  -> EX:  (03470727000120)"                   
SKIP   
" CNPJ FORNECEDOR           16  A  29       FORMATO =  99999999999999  -> EX:  (04095571000108)"                      
SKIP   
" NR. NOTA FISCAL COMPL     30  A  35       FORMATO =          999999  -> EX:          (999999)"                      
SKIP   
" DATA DE EMISSAO           36  A  45       FORMATO =      DD.MM.AAAA  -> EX:      (01.01.2005)"         
SKIP   
" BASE DE ICMS              46  A  60       FORMATO = 999999999999999  -> EX: (000000000121200)"
SKIP   
" VALOR DE ICMS             61  A  75       FORMATO = 999999999999999  -> EX: (000000000021816)"  
SKIP   
" VALOR L�QUIDO             76  A  90       FORMATO = 999999999999999  -> EX: (000000000121200)" 
SKIP   
" VALOR BRUTO               91  A 105       FORMATO = 999999999999999  -> EX: (000000000121200)"   
SKIP   
" VALOR IPI                106  A 120       FORMATO = 999999999999999  -> EX: (000000000012120)" 
SKIP   
" PERCENTUAL DE IPI        121  A 125       FORMATO =           99999  -> EX:           (01000)"   
SKIP(2) 
"EXEMPLO DO REGISTRO TIPO 1 MONTADO:"   
SKIP(2) 
"1034707270001200409557100010899999901.01.200500000000012120000000000002181600000000012120000000000012120000000000012120001000"
SKIP(2)
"REGISTRO TIPO 2:"
SKIP
"** INFORMACAO **        ** POSICAO **     ** OBSERVACAO ** "  
SKIP           
" TIPO REGISTRO              1  A   1       VALOR FIXO = 2"         
SKIP " CNPJ FORD                  2  A  15       FORMATO    =  99999999999999  -> EX: (03470727000120)"                   
SKIP " CNPJ FORNECEDOR           16  A  29       FORMATO    =  99999999999999  -> EX: (04095571000108)"                      
SKIP " NR. NOTA FISCAL COMPL     30  A  35       FORMATO    =          999999  -> EX:         (999999)"
SKIP " PREFIXO DA PECA           36  A  42       FORMATO    =         XXXXXXX  -> EX:        (   2S65)"                     
SKIP " BASICO  DA PECA           43  A  51       FORMATO    =       XXXXXXXXX  -> EX:      (   A26087)"
SKIP " SUFIXO  DA PECA           52  A  57       FORMATO    =          XXXXXX  -> EX:         (AC    )"
SKIP " NOTA FISCAL ORIGINAL      58  A  63       FORMATO    =          999999  -> EX:         (019876)"
SKIP " DIFERENCA DE PRECO        64  A  78       FORMATO    = 999999999999999  -> EX:(000000000000020)"
SKIP " ESPACO LIVRE              79  A 125       FORMATO    = ESPACOS "
SKIP
"EXEMPLO DO REGISTRO TIPO 2 MONTADO:" 
SKIP(2)
"20347072700012003509521000167999999...2S65...A26087AC....019876000000000000020................................................................................."
SKIP     
"obs: 1.) Os valores n�mericos nao devem conter virgulas"
SKIP   
"     2.) Caracter ponto(.) no registro tipo 2 equivale a espacos, deixar em branco."
SKIP   
"         3.) A peCa(posiCAo 36) e� composta de 3 partes:"
SKIP   
"         1 - BASICO  com 7 posicoes alinhadas a direita. "
SKIP   
"    2 - PREFIXO com 9 posicoes alinhadas a direita."
SKIP   
"    3 - SUFIXO  com 6 posicoes alinhadas a esquerda"
SKIP   
"     4.) A NF Complementar e� composta no arquivo por 1 registro tipo 1 e "
SKIP   
" tantos registros tipo 2 quanto forem os itens da nf complementar ou seja, cada registro tipo 1, � uma nova nota complementar e os registros tipo 2 subsequentes sAo os itens pertencentes a ela."
SKIP   
"     5.) O CNPJ FORD, CNPJ FORN e NR. NOTA FISCAL COMPL, devem ser iguais no registro"
SKIP   
" tipo 1 e registro tipo 2." WITH FRAME F-LISTA-LAY  WIDTH 300 STREAM-IO.
OUTPUT CLOSE.
DOS SILENT START notepad.exe VALUE(SESSION:TEMP-DIR + "LAYOUT-FORD.TXT").

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wemit wdt-ini wdt-fim wrel1 wrel2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 wemit wdt-ini wdt-fim wrel1 BUTTON-1 wrel2 BUTTON-2 
         BtnOK BtnDone WLAY 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN wemit = 875
       wdt-ini = TODAY
       wdt-fim = TODAY.
DISPLAY wemit wdt-ini wdt-fim WITH FRAME     {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio C-Win 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* 30/06/09 - gera arquivo dif pre�o FORD, conf. solicitacao REgiane */
/* FORD BARUERI */
OUTPUT STREAM w-cons TO VALUE(wrel1) PAGED.
OUTPUT STREAM w-comple TO VALUE(wrel2) PAGED.


FOR EACH nota-fiscal  
         where  nota-fiscal.cod-emitente = wemit AND
               (nota-fiscal.dt-emis-nota GE wdt-ini AND  /* 05/22/09*/
               nota-fiscal.dt-emis-nota  LE wdt-fim) AND  /* 05/25/09*/
               nota-fiscal.ind-tip-nota  = 3 AND
               nota-fiscal.dt-cancela = ? NO-LOCK:
         
  FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.it-codigo:
     

  /* procura dt vencimento */
  FIND fat-duplic
    where fat-duplic.cod-estabel = nota-fiscal.cod-estabel
    and   fat-duplic.serie       = nota-fiscal.serie
    and   fat-duplic.nr-fatura   = nota-fiscal.nr-fatura   NO-LOCK NO-ERROR.

  FIND mgcad.estabel WHERE estabel.cod-estabel = nota-fiscal.cod-estabel NO-LOCK NO-ERROR.
  FIND ITEM WHERE ITEM.it-codigo = it-nota-fisc.it-codigo NO-LOCK NO-ERROR.
  ASSIGN witem-prefixo = ""
         witem-basico  = ""
         witem-sufixo =  "".
 
   FIND FIRST doc-fiscal WHERE doc-fiscal.cod-estabel = nota-fiscal.cod-estabel
                       AND  doc-fiscal.serie = nota-fiscal.serie
                       AND doc-fiscal.nr-doc-fis = nota-fiscal.nr-nota-fis
                       AND doc-fiscal.cod-emitente = nota-fiscal.cod-emitente
                       NO-LOCK.
 

   IF FIRST-OF(it-nota-fisc.it-codigo) THEN DO:
       FORM HEADER
       "SEEBER FASPTLA LTDA.   RELATORIO DE NFS COMPLEMENTARES - FORD"
       TODAY FORMAT "99/99/9999"
       string(time,"HH:MM:SS") "Hs"
       SKIP(2)
       WITH FRAME f-lista.
       /* ARQUIVO CONSISTENCIA */
       DISPLAY STREAM w-cons  
        "1"
        "03470727000120"                 COLUMN-LABEL "FORD"
         mgcad.estabel.cgc               COLUMN-LABEL "CGC"  
         nota-fiscal.nr-nota-fis         COLUMN-LABEL "NF complem."  
         nota-fiscal.dt-emis-nota        COLUMN-LABEL "Dt.NF.Com." 
         doc-fiscal.vl-bicms             COLUMN-LABEL "Base ICMS"   
         doc-fiscal.vl-icms              COLUMN-LABEL "ICMS"
/*          nota-fiscal.vl-mercad           COLUMN-LABEL "Vr.Merc."  */
         nota-fiscal.vl-tot-nota         COLUMN-LABEL "Vl.Liquido"
         nota-fiscal.vl-tot-nota         COLUMN-LABEL "Vl.Bruto" 
         doc-fiscal.vl-ipi               COLUMN-LABEL "Vl.IPI"
         it-nota-fisc.aliquota-ipi       COLUMN-LABEL "Al.IPI"
         SKIP
         WITH FRAME f-lista WIDTH 500 STREAM-IO DOWN.
           
       
       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */
       PUT STREAM w-comple  UNFORMATTED 
        "1"
         "03470727000120"           FORMAT "X(14)"             
         mgcad.estabel.cgc          FORMAT "X(14)"      
         SUBSTRING(nota-fiscal.nr-nota-fis,2,6)    FORMAT "999999"
         nota-fiscal.dt-emis-nota   FORMAT "99.99.9999"      
         SUBSTRING(STRING(doc-fiscal.vl-bicms,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(doc-fiscal.vl-bicms,"9999999999999.99"),15,2) FORMAT "99" 

         SUBSTRING(STRING(doc-fiscal.vl-icms,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(doc-fiscal.vl-icms,"9999999999999.99"),15,2) FORMAT "99" 

         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),15,2) FORMAT "99" 
         
         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),15,2) FORMAT "99"  /*VR BRUTO - REPETIR CONF.REGIANE */
         /*          nota-fiscal.vl-mercad      FORMAT "999999999999999"  */

         SUBSTRING(STRING(doc-fiscal.vl-ipi ,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(doc-fiscal.vl-ipi ,"9999999999999.99"),15,2) FORMAT "99" 

         it-nota-fisc.aliquota-ipi  FORMAT "99999"      
         SKIP.
       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */
     END.
 
      DISPLAY STREAM w-cons     
        "2"
        "03470727000120"                          COLUMN-LABEL "FORD"
         mgcad.estabel.cgc                        COLUMN-LABEL "CGC"  
         nota-fiscal.nr-nota-fis                  COLUMN-LABEL "NF complem."
         ITEM.codigo-refer                        COLUMN-LABEL "Item"
         it-nota-fisc.qt-faturada[1]              COLUMN-LABEL "Qt.Fat."
         it-nota-fisc.vl-preuni                   COLUMN-LABEL "Preco unit."
        /*it-nota-fisc.vl-merc-ori                COLUMN-LABEL "Vr."   /* sem impostos */ */
       /* it-nota-fisc.vl-tot-item                  COLUMN-LABEL "Tot.Item"*/
         SKIP
       WITH FRAME f-listad WIDTH 500 STREAM-IO DOWN.
       DOWN STREAM w-cons WITH FRAME f-listad.


       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */
       PUT STREAM w-comple UNFORMATTED 
        "2"
        "03470727000120"            FORMAT "X(14)"            
         mgcad.estabel.cgc          FORMAT "X(14)"     
         SUBSTRING(nota-fiscal.nr-nota-fis,2,6)    FORMAT "999999"
         ITEM.CODIGO-REFER FORMAT "X(22)"
         "      "   /* PRENCHIDO PELO REGIANE COM O NUMERO DA NOTA FISCAL DE ORIGEM* */

         SUBSTRING(STRING(it-nota-fisc.vl-preuni,"9999999999999.99"),1,13) FORMAT "9999999999999"
         SUBSTRING(STRING(it-nota-fisc.vl-preuni,"9999999999999.99"),15,2) FORMAT "99"



/*          SUBSTRING(STRING(it-nota-fisc.vl-merc-ori,"9999999999999.99"),1,13) FORMAT "9999999999999"  */
/*          SUBSTRING(STRING(it-nota-fisc.vl-merc-ori,"9999999999999.99"),15,2) FORMAT "99"             */
/*          SUBSTRING(STRING(it-nota-fisc.vl-tot-item,"9999999999999.99"),1,13) FORMAT "9999999999999" */
/*          SUBSTRING(STRING(it-nota-fisc.vl-tot-item,"9999999999999.99"),15,2) FORMAT "99"            */

        FILL(" ",47)
        SKIP.
       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */

     END.

   END.
   OUTPUT   STREAM w-cons  CLOSE.
   OUTPUT   STREAM w-comple  CLOSE.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

