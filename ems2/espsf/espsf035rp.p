&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF035RP 9.99.99.999}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> <m�dulo>}
&ENDIF

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD westab-ini       AS CHAR FORMAT "X(06)"
    FIELD westab-fim       AS CHAR FORMAT "X(06)"
    FIELD wemitente-ini    AS INTEGER FORMAT ">>>>>>>>9"
    FIELD wemitente-fim    AS INTEGER FORMAT ">>>>>>>>9"
    FIELD wserie-ini       AS CHAR FORMAT "X(05)"
    FIELD wserie-fim       AS CHAR FORMAT "X(05)"
    FIELD wnota-ini        AS CHAR FORMAT "X(16)"
    FIELD wnota-fim        AS CHAR FORMAT "X(16)"
    FIELD wdt-ini          AS DATE FORMAT "99/99/9999"
    FIELD wdt-fim          AS DATE FORMAT "99/99/9999"
    FIELD wnat-param-ini   AS CHAR FORMAT "X(06)"
    FIELD wnat-param-fim   AS CHAR FORMAT "X(06)".


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa
/*     where empresa.ep-codigo = v_cdn_empres_usuar */
    no-lock no-error.
find first param-global no-lock no-error.

{utp/ut-liter.i titulo_sistema * }
assign c-sistema = return-value.
 
{utp/ut-liter.i "RELAT�RIO DE DEVOLU��ES - PADR�O SCANIA"}
assign c-titulo-relat = return-value.
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
        /*:T --- Colocar aqui o c�digo de impress�o --- */
    /*     for each [TABELA] no-lock                                    */
    /*         where [WHERE].                                           */
    /*                                                                  */
    /*         run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U). */
    /*     end.                                                         */
        
    /* programa scania  - jose luiz */ 
     
    /*   FOR EACH nota-fiscal WHERE cod-emitente = 1880                              */
    /*                      /*AND   nr-nota-fis = "0287307" */                       */
    /*                      AND  dt-emis-nota GE 08/01/2015                          */
    /*                      AND  nat-operacao = "594c2"                              */
    /*                      AND ind-sit-nota <> 4                                    */
    /*                     NO-LOCK BREAK BY nota-fiscal.nr-nota-fis BY dt-emis-nota: */
  
   FOR EACH nota-fiscal WHERE  (nota-fiscal.cod-estabel        GE tt-param.westab-ini
                            AND nota-fiscal.cod-estabel        LE tt-param.westab-fim) 
                            AND (nota-fiscal.cod-emitente      GE tt-param.wemitente-ini
                            AND nota-fiscal.cod-emitente       LE tt-param.wemitente-fim)  
                            AND (nota-fiscal.serie             GE tt-param.wserie-ini
                            AND nota-fiscal.serie              LE tt-param.wserie-fim) 
                            AND (nota-fiscal.nr-nota-fis       GE tt-param.wnota-ini  
                            AND nota-fiscal.nr-nota-fis        LE tt-param.wnota-fim)
                            AND  (nota-fiscal.dt-emis-nota      GE tt-param.wdt-ini 
                            AND nota-fiscal.dt-emis-nota       LE tt-param.wdt-fim)
                            AND nota-fiscal.ind-sit-nota        <> 4 /* nao emite se for cancelada conf Idelcio*/
                            AND nota-fiscal.nat-operacao       GE tt-param.wnat-param-ini
                            AND nota-fiscal.nat-operacao       LE tt-param.wnat-param-fim
       /*"594c2" */
        NO-LOCK BREAK BY nota-fiscal.nr-nota-fis BY dt-emis-nota:   
        /*        MESSAGE tt-param.wnat-param   tt-param.westab-ini tt-param.westab-fim                              */
        /*                tt-param.wemitente-ini tt-param.wemitente-fim                                              */
        /*                tt-param.wserie-ini tt-param.wserie-fim                                                    */
        /*                tt-param.wnota-ini tt-param.wnota-fim tt-param.wdt-ini tt-param.wdt-fim VIEW-AS ALERT-BOX. */
         
   run pi-acompanhar in h-acomp ("Nota-fiscal: " + STRING(nota-fiscal.nr-nota-fis)).

   FIND natur-oper OF nota-fiscal NO-LOCK.
   FIND estabelec WHERE estabelec.cod-estabel = nota-fiscal.cod-estabe NO-LOCK.
 
   FIND FIRST emitente WHERE emitente.cod-emitente = nota-fiscal.cod-emitente NO-LOCK NO-ERROR.
        FORM HEADER
        "Nota Fiscal: "nota-fiscal.nr-nota-fis "     CFOP: " natur-oper.cod-cfop   "       Data Emiss�o: " nota-fiscal.dt-emis-nota 
        SKIP
        "Cliente: " nota-fiscal.cod-emitente  "-              "  emitente.nome-emit   FORMAT "X(37)" "Nat.Oper." nota-fiscal.nat-operacao
        SKIP(1)
        WITH FRAME f-cabec1 WIDTH 150 STREAM-IO.

    IF FIRST-OF(nota-fiscal.nr-nota-fis) THEN  VIEW frame f-cabec1.
       /* THEN view-frame nr-nota-fis nota-fisca.nat-operacao dt-emis-nota nota-fiscal.cod-emitente emitente.nome-emit 
        WITH FRAME f-cabec WIDTH 150. */
        
    FOR EACH it-nota-fisc OF nota-fiscal  NO-LOCK BREAK BY  it-nota-fisc.nr-docum: /*BREAK BY it-nota-fisc.it-codigo.*/
        FIND FIRST ITEM OF it-nota-fisc NO-LOCK.
        DISPLAY ITEM.codigo-refer COLUMN-LABEL "Item"
           /* it-nota-fisc.qt-faturada[1]*/  round(it-nota-fisc.qt-faturada[1],3) FORMAT "z,zz9.999" COLUMN-LABEL "Qt Mat"
            it-nota-fisc.nr-docum COLUMN-LABEL "Ref. Nota"
            WITH WIDTH 150 STREAM-IO.
            /* data emis-nota dev no fiscal */
            /*         FIND doc-fiscal WHERE doc-fiscal.nr-doc-fis  = it-nota-fisc.nr-docum                 */
            /*                         AND   doc-fiscal.cod-emitente = nota-fiscal.cod-emitente             */
            /*                         AND   doc-fiscal.cod-estabel  = nota-fiscal.cod-estabel              */
            /*                         NO-LOCK NO-ERROR.                                                    */
            /*     /* MESSAGE wdoc-fiscal.dt-emis-doc wdoc-fiscal.serie VIEW-AS ALERT-BOX.*/                */
            /*      IF AVAILABLE doc-fiscal THEN DISPLAY doc-fiscal.dt-emis-doc COLUMN-LABEL "Dt.Ref.Nota". */
     DISPLAY /*it-nota-fisc.vl-preuni */ round(it-nota-fisc.vl-preuni,2) COLUMN-LABEL "Pr Unit" 
             it-nota-fisc.vl-tot-item /*round(it-nota-fisc.vl-tot-item,2) */ COLUMN-LABEL "Total Item" it-nota-fisc.it-codigo COLUMN-LABEL "Item Fastplas" .
     
     END.
   END.


    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


