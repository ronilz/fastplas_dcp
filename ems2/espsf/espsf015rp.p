/* exporta��p espsf015*/ 
/* valeria - 09/05/11*/
/* EXPORTA NOTA DE COMPLEMENTO DE PRE�O FORD */
/* AUTOR: VSH - 290212 */

/* include de controle de vers�o */
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF015RP 1.00.00.000}
/* defini��o das temp-tables para recebimento de par�metros */
/* define temp-table tt-param         */
/*         field destino   as integer */
/*     field arq-destino   as char    */
/*     field arq-entrada   as char    */
/*     field usuario       as char    */
/*     field data-exec as date        */
/*     field hora-exec as integer     */


    define temp-table tt-param
    field destino          as integer
    field arq-destino      as char
    field arq-entrada        as char
    field usuario          as char
    field data-exec        as date
    field hora-exec        as INTEGER
    FIELD wemit             AS INTE FORMAT ">>>>>>>>9"
    FIELD wdt-ini          AS DATE FORMAT "99/99/9999"
    FIELD wdt-fim          AS DATE FORMAT "99/99/9999"
    FIELD wnf-ini          AS CHAR FORMAT "X(16)"
    FIELD wnf-fim          AS CHAR FORMAT "X(16)"
    FIELD wipi             AS INTEGER FORMAT "99999".

/* Local Variable Definitions --- defini��es valeria - exporta��o        */
DEFINE VARIABLE witem-prefixo AS CHAR FORMAT "X(7)".
DEFINE VARIABLE witem-basico  AS CHAR FORMAT "X(9)".
DEFINE VARIABLE witem-sufixo  AS CHAR FORMAT "X(6)".
DEFINE VARIABLE wvezes        AS INTE FORMAT "99".
DEFINE VARIABLE wpos          AS INTE FORMAT "99".
DEFINE VARIABLE wfirst        AS INTE FORMAT "99".
DEFINE VARIABLE wlast         AS INTE FORMAT "99".

DEFINE VARIABLE wcont AS INTE FORMAT "99".
DEFINE VARIABLE wcont2 AS INTE FORMAT "99".
DEFINE BUFFER b-nota FOR nota-fiscal.
DEFINE STREAM w-cons.
DEFINE STREAM w-comple.
DEFINE VARIABLE wnota-origem AS CHAR FORMAT "X(06)".
DEFINE VARIABLE wcont-nf-org AS INTE FORMAT "9999".
DEFINE VARIABLE wcont1       AS INTE FORMAT "99".
DEFINE VARIABLE wcont-disp   AS INTE FORMAT "99".
DEFINE VARIABLE wemp        LIKE estabelec.nome.

 
DEFINE VARIABLE wnf-origem AS CHAR FORMAT "x(06)" EXTENT 50.
DEFINE VARIABLE wcont-orig AS INTE FORMAT 99.
DEFINE VARIABLE worigem-disp AS INTEGER FORMAT "999999" EXTENT 50.

/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR FORMAT "x(40)" NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.

/* defini��es valeria - exporta��o */


def temp-table tt-raw-digita
    	field raw-digita	as raw.
/* recebimento de par�metros */
def input parameter raw-param as raw no-undo. 
def input parameter TABLE for tt-raw-digita.   

create tt-param.
raw-transfer raw-param to tt-param.
 

/* include padr�o para vari�veis para o log  */
{include/i-rpvar.i}
/* defini��o de vari�veis e streams */
def stream s-exp.
def var h-acomp as handle no-undo.

/* defini��o de frames do log */
/* include padr�o para output de log */
{include/i-rpout.i &STREAM="stream str-rp" &TOFILE=tt-param.arq-destino}
/* include com a defini��o da frame de cabe�alho e rodap� */
{include/i-rpcab.i &STREAM="str-rp"}
/* bloco principal do programa */
assign	c-programa 	= "ESPSF015RP"
c-versao	= "1.00"
c-revisao	= ".00.000"
c-empresa	= /*"Empresa Teste"*/ wemp
c-sistema	= 'Sports'
c-titulo-relat = "LISTAGEM LOG EXPORTA��O NF�s COMPLEMENTARES FORD".
view stream str-rp frame f-cabec.
view stream str-rp frame f-rodape.
run utp/ut-acomp.p persistent set h-acomp.
{utp/ut-liter.i Exportando *}

run pi-inicializar in h-acomp (input RETURN-VALUE).
/* define a sa�da para o arquivo de sa�da informando na p�gina de par�metros */

OUTPUT STREAM s-exp  TO value(tt-param.arq-entrada).
/* bloco principal do programa */

RUN roda_exportacao.

/* for EACH emitente WHERE cod-emitente = tt-param.wcod-ini on stop undo, leave:                      */
/*     MESSAGE emitente.cod-emitente tt-param.wcod-ini VIEW-AS ALERT-BOX.                             */
/*     run pi-acompanhar in h-acomp (input string(emitente.cod-emitente)).                            */
/*     put stream s-exp unformatted string(emitente.cod-emitente) string(emitente.nome-abrev,"x(40)") */
/* skip.                                                                                              */
/*     DISPLAY STREAM str-rp cod-emitente nome-abrev.                                                 */
/* end.     
                                                                                          */
output stream s-exp close.
/*fechamento do output do log */
{include/i-rpclo.i &STREAM="stream str-rp"}
run pi-finalizar in h-acomp. 
return "Ok":U.

/* procedure roda exporta��o */
PROCEDURE roda_exportacao.

/*ASSIGN wcont-nf-org = 1.*/
FOR EACH nota-fiscal  
         where  nota-fiscal.cod-emitente = tt-param.wemit AND
               (nota-fiscal.dt-emis-nota GE tt-param.wdt-ini AND  /* 05/22/09*/
                nota-fiscal.dt-emis-nota  LE tt-param.wdt-fim) AND  /* 05/25/09*/
                nota-fiscal.ind-tip-nota  = 3 AND
                nota-fiscal.dt-cancela = ? /*
                nota-fiscal.nr-nota-fis GE tt-param.wnf-ini and
                nota-fiscal.nr-nota-fis LE tt-param.wnf-fim*/
                NO-LOCK:
     /* --------------rotina para localizar as notas de origem no campo obs -------------------------------------------------------------------*/
    /* limpa o contador de notas */
    DO wcont2 = 1 TO 50:
      ASSIGN wnf-origem[wcont2] = "".
    END.
    
    ASSIGN wcont-orig = 0
          wcont-nf-org = 1.
    /*DISPLAY nr-nota-fis observ-nota obs-gerada.  */
    DO wcont-nf-org = 1 TO LENGTH(nota-fiscal.observ-nota):
     IF wcont-orig = 50 THEN NEXT. 
      /* primeira nota */
      IF SUBSTRING(nota-fiscal.observ-nota,wcont-nf-org,1) = "," THEN DO:
         ASSIGN wnota-origem = SUBSTRING(nota-fiscal.observ-nota,wcont-nf-org - 6,6)
                wcont-orig = wcont-orig + 1.
         ASSIGN wnf-origem[wcont-orig] = wnota-origem.
         END.
      /* ultima nota */
       IF SUBSTRING(nota-fiscal.observ-nota,wcont-nf-org,1) = "." 
          AND (SUBSTRING(nota-fiscal.observ-nota,wcont-nf-org - 7,1) = "," 
          OR  SUBSTRING(nota-fiscal.observ-nota,wcont-nf-org - 8,1) = "," )THEN DO:
          ASSIGN wnota-origem = SUBSTRING(nota-fiscal.observ-nota,wcont-nf-org - 6,6).
          ASSIGN wnf-origem[50] = wnota-origem.
                  wcont-orig = 50.  
          END.
  
   /* MESSAGE wcont-orig wnf-origem[wcont-orig] VIEW-AS ALERT-BOX.*/
    END.
    /* ----------------rotina para localizar as notas de origem no campo obs -------------------------------------------------------------------*/


  FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.it-codigo:
 /* procura dt vencimento */
  FIND fat-duplic
    where fat-duplic.cod-estabel = nota-fiscal.cod-estabel
    and   fat-duplic.serie       = nota-fiscal.serie
    and   fat-duplic.nr-fatura   = nota-fiscal.nr-fatura   NO-LOCK NO-ERROR.

  FIND mgcad.estabel WHERE estabel.cod-estabel = nota-fiscal.cod-estabel NO-LOCK NO-ERROR.
  FIND ITEM WHERE ITEM.it-codigo = it-nota-fisc.it-codigo NO-LOCK NO-ERROR.
 
  ASSIGN witem-prefixo = ""
         witem-basico  = ""
         witem-sufixo =  "".
 
   FIND FIRST doc-fiscal WHERE doc-fiscal.cod-estabel = nota-fiscal.cod-estabel
                       AND  doc-fiscal.serie = nota-fiscal.serie
                       AND doc-fiscal.nr-doc-fis = nota-fiscal.nr-nota-fis
                       AND doc-fiscal.cod-emitente = nota-fiscal.cod-emitente
                       NO-LOCK NO-ERROR.
    IF NOT AVAILABLE doc-fiscal THEN MESSAGE "DOCUMENTO FISCAL N�O ENCONTRADO"  nota-fiscal.nr-nota-fis
                                             "VERIFICAR COM O FISCAL" VIEW-AS ALERT-BOX.
    /* PROCURA ITEM DO CLIENTE */
    FIND FIRST item-cli OF ITEM NO-LOCK NO-ERROR.
    

   IF FIRST-OF(it-nota-fisc.it-codigo) THEN DO:
       /* ARQUIVO CONSISTENCIA */
       DISPLAY STREAM str-rp
         FILL("=",163) FORMAT "X(163)"
         "CGC FORD       CGC                   NF complem.      Dt.NF.Com.        Base ICMS             ICMS          Vl.Bruto           Vl.IPI Al.IPI IPI PAR�METRO TELA"
         SKIP
         "-------------------------------------------------------------------------------------------------------------------------------------------------------------------"
         SKIP
        "1"
        "03470727000120"                 COLUMN-LABEL "FORD"
         mgcad.estabel.cgc               COLUMN-LABEL "CGC"  
         nota-fiscal.nr-nota-fis         COLUMN-LABEL "NF complem."  
         nota-fiscal.dt-emis-nota        COLUMN-LABEL "Dt.NF.Com." 
         doc-fiscal.vl-bicms             COLUMN-LABEL "Base ICMS"   
         doc-fiscal.vl-icms              COLUMN-LABEL "ICMS"
/*       nota-fiscal.vl-mercad           COLUMN-LABEL "Vr.Merc."  */
         nota-fiscal.vl-tot-nota         COLUMN-LABEL "Vl.Liquido"
         nota-fiscal.vl-tot-nota         COLUMN-LABEL "Vl.Bruto" 
         doc-fiscal.vl-ipi               COLUMN-LABEL "Vl.IPI"
         it-nota-fisc.aliquota-ipi       COLUMN-LABEL "Al.IPI"
         tt-param.wipi                   COLUMN-LABEL "IPI PAR�METRO TELA"
         SKIP
         WITH FRAME f-log WIDTH 200 STREAM-IO DOWN NO-LABEL.
        
       
       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */
       PUT STREAM s-exp UNFORMATTED 
        "1"
         "03470727000120"           FORMAT "X(14)"             
         mgcad.estabel.cgc          FORMAT "X(14)"      
         SUBSTRING(nota-fiscal.nr-nota-fis,2,6)    FORMAT "999999"
         nota-fiscal.dt-emis-nota   FORMAT "99.99.9999"      
         SUBSTRING(STRING(doc-fiscal.vl-bicms,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(doc-fiscal.vl-bicms,"9999999999999.99"),15,2) FORMAT "99" 

         SUBSTRING(STRING(doc-fiscal.vl-icms,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(doc-fiscal.vl-icms,"9999999999999.99"),15,2) FORMAT "99" 

         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),15,2) FORMAT "99" 
         
         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"9999999999999.99"),15,2) FORMAT "99"  /*VR BRUTO - REPETIR CONF.REGIANE */
         /*          nota-fiscal.vl-mercad      FORMAT "999999999999999"  */

         SUBSTRING(STRING(doc-fiscal.vl-ipi ,"9999999999999.99"),1,13) FORMAT "9999999999999"      
         SUBSTRING(STRING(doc-fiscal.vl-ipi ,"9999999999999.99"),15,2) FORMAT "99" 
         /* it-nota-fisc.aliquota-ipi  FORMAT "99999" */ /* INIIBIDO POIS SAIA 5 MESMO QUANDO NAO TINHA */
         wipi FORMAT "99999"
         SKIP.
       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */
     END.
      
       DISPLAY STREAM str-rp 
        "**** DADOS DO ITEM: " AT 67 it-nota-fisc.it-codigo "****" NO-LABEL
        SKIP 
        "  CGC FORD          Item Ford                        Qt.Fat.       Preco unit."
        SKIP
        "-------------------------------------------------------------------------------------------------------------------------------------------------------------------"
        SKIP
        "2"
        "03470727000120"                          COLUMN-LABEL "FORD"
         mgcad.estabel.cgc                        COLUMN-LABEL "CGC"  
         nota-fiscal.nr-nota-fis                  COLUMN-LABEL "NF complem."
        /* alterado conf sol. REgiane para pegar do item x cli em 10/07/2012 */
      /*   "ford old" ITEM.codigo-refer                        COLUMN-LABEL "Item ford old"  */
         item-cli.item-do-cli                     COLUMN-LABEL "Item Ford"
         it-nota-fisc.qt-faturada[1]              COLUMN-LABEL "Qt.Fat."
         it-nota-fisc.vl-preuni                   COLUMN-LABEL "Preco unit."
         SKIP
         "**** OBSERVA��ES NOTA ****"  AT 67
         SKIP observ-nota NO-LABEL
         SKIP(1) 
        WITH FRAME f-log WIDTH 320 STREAM-IO DOWN NO-LABEL.
       DOWN   WITH FRAME f-log.


       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */
      
        /* rotina para imprimir 50 registros, refernete notas de origem localizadas na obs */
        
        
        DO wcont-disp = 1 TO 50:
         
          IF  wnf-origem[wcont-disp] <> "" THEN DO:
          IF substring(wnf-origem[wcont-disp],1,1) = "T" THEN NEXT. 
          IF SUBSTRING(wnf-origem[wcont-disp],1,1) = " " THEN ASSIGN wnf-origem[wcont-disp] = "0" + SUBSTRING(wnf-origem[wcont-disp],2,5).
         
/*           ASSIGN worigem-disp[wcont-disp] =  integer(STRING(wnf-origem[wcont-disp],"x(06)")). */
  
          PUT STREAM s-exp UNFORMATTED 
          "2"
          "03470727000120"            FORMAT "X(14)"            
           mgcad.estabel.cgc          FORMAT "X(14)"     
           SUBSTRING(nota-fiscal.nr-nota-fis,2,6)    FORMAT "999999"
          /* alterado conf sol. REgiane para pegar do item x cli em 10/07/2012 
           ITEM.CODIGO-REFER FORMAT "X(22)" */
           item-cli.item-do-cli                      FORMAT "X(22)" /* codigo do cliente*/
           wnf-origem[wcont-disp] FORMAT "999999"
           SUBSTRING(STRING(it-nota-fisc.vl-preuni,"9999999999999.99"),1,13) FORMAT "9999999999999"
           SUBSTRING(STRING(it-nota-fisc.vl-preuni,"9999999999999.99"),15,2) FORMAT "99"
           FILL(" ",47)
           SKIP.
 
          END.
        END.

       /* ---------------------------------------------- ARQUIVO COMPLEMENTO */

     END.                          
    END.
  
  END PROCEDURE.


 
