/******************************************************************************
*      Programa .....: espsf039rp                                             *
*      Data .........: 2016                                                   *
*      Empresa ......:                                                        *
*      Programador ..:                                                        *
*      Objetivo .....: Listagem Pedidos Scania por situa��o                   *
*      30/05/16        Ignora sequencias cancelas conf. sol. Regiane
*******************************************************************************/
 

/* include de controle de vers�o */
{include/i-prgvrs.i espsf038rp 2.08.00.000}

{utp/ut-glob.i}

/* pr�processador para ativar ou n�o a sa�da para RTF */
&GLOBAL-DEFINE RTF NO

/* pr�processador para setar o tamanho da p�gina */
&SCOPED-DEFINE pagesize 64
/* defini��o das temp-tables para recebimento de par�metros */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field c-nome-abrev-ini as char 
    field c-nome-abrev-fim as char 
    field c-dt-implant-fim as date
    field c-dt-implant-ini as date
    field c-dt-entrega-fim as date
    field c-dt-entrega-ini as date
    field c-nr-pedcli-fim  as char 
    field c-nr-pedcli-ini  as char 
    FIELD wdet-fat         AS INTE.


define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.

DEFINE TEMP-TABLE tt-raw-digita NO-UNDO
    FIELD raw-digita	   AS RAW.

/* recebimento de par�metros */
DEFINE INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEFINE INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* include padr�o para vari�veis de relat�rio  */
{include/i-rpvar.i}

/* defini��o de vari�veis  */

DEFINE VARIABLE h-acomp   AS HANDLE     NO-UNDO.

def var ch_excel                as office.iface.excel.ExcelWrapper   no-undo.
def var ch_workbook             as office.iface.excel.Workbook   no-undo.
def var ch_worksheet            as com-handle   no-undo.
def var ch_querytables          as com-handle   no-undo.
Def Var ch_sheet                as com-handle   no-undo.

def var c-pnumber as char no-undo.
def var c-cv as char no-undo.
def var c-cor as char no-undo.

DEF VAR c-arquivo AS CHAR NO-UNDO.

DEF VAR wnota LIKE nota-fiscal.nr-nota-fis.
DEF VAR wdata LIKE nota-fiscal.dt-emis-nota.
DEF VAR wdata-disp AS CHAR FORMAT "x(10)".

DEF STREAM str-excel.

/* include padr�o para output de relat�rios */
{include/i-rpout.i &STREAM="stream str-rp"}

/* include com a defini��o da frame de cabe�alho e rodap� */
{include/i-rpcab.i &STREAM="str-rp"}

VIEW STREAM str-rp FRAME f-cabec.
VIEW STREAM str-rp FRAME f-rodape.

/* executando de forma persistente o utilit�rio de acompanhamento */
RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}
RUN pi-inicializar IN h-acomp (INPUT RETURN-VALUE).

c-arquivo = SESSION:TEMP-DIRECTORY + "espsf039" + c-seg-usuario + ".csv".

RUN pi-acompanhar IN h-acomp (INPUT "Gerando...").

OUTPUT STREAM str-excel TO VALUE(c-arquivo) NO-CONVERT.

PUT STREAM str-excel UNFORMAT
    'Pedido;Nat.Oper.;Data Imp;Tab Pre�o;Cod.fast;P.number;COR;CV;CU;Quant;Qtd Part;Vl item;Valor CU;Ped.Logistico;POP ID;Seq.It.Ped;Ord.Prod;Data Entrega;NF.;DT.NF.;' SKIP.

FOR EACH ped-venda WHERE
    ped-venda.nome-abrev >= tt-param.c-nome-abrev-ini AND
    ped-venda.nome-abrev <= tt-param.c-nome-abrev-fim AND 
    ped-venda.nr-pedcli  >= tt-param.c-nr-pedcli-ini  AND 
    ped-venda.nr-pedcli  <= tt-param.c-nr-pedcli-fim  AND 
    ped-venda.dt-implant >= tt-param.c-dt-implant-ini AND 
    ped-venda.dt-implant <= tt-param.c-dt-implant-fim NO-LOCK,
    EACH ped-item OF ped-venda where 
    (ped-item.dt-entrega >= tt-param.c-dt-entrega-ini and
    ped-item.dt-entrega <= tt-param.c-dt-entrega-fim)  AND 
    ped-item.desc-cancela = " " NO-LOCK,
    EACH mgfas.ext-ped-item OF ped-item NO-LOCK.

    RUN pi-acompanhar IN h-acomp (INPUT "Pedido: " + STRING(ped-venda.nr-pedido)).

    
    /* procura nf e data - conf. sol. F�bio - 120416*/
    ASSIGN wnota = ""
           wdata = ?.
    FIND FIRST it-nota-fisc WHERE it-nota-fisc.nr-pedido  = ped-venda.nr-pedido
                           AND it-nota-fisc.nr-seq-ped = ped-item.nr-sequencia 
                           AND it-nota-fisc.it-codigo = ped-item.it-codigo
                           AND it-nota-fisc.dt-cancela = ? NO-LOCK NO-ERROR.
     IF AVAILABLE it-nota-fisc THEN DO:
           ASSIGN wnota = it-nota-fisc.nr-nota-fis
                  wdata = it-nota-fisc.dt-emis-nota.
     END.

/*      /* logica incluida sol. REgiane */                                  */
/*      FIND FIRST nota-fiscal OF it-nota-fisc NO-LOCK NO-ERROR.            */
/*       IF AVAILABLE nota-fiscal AND nota-fiscal.dt-cancela = ? THEN DO:   */
/*       /* notas validas */                                                */
/*                                                                          */
/*         END.                                                             */
/*        /* notas canceladas*/                                             */
/*        IF AVAILABLE nota-fiscal AND nota-fiscal.dt-cancela <> ? THEN DO: */
/*       /* notas validas */                                                */
/*        ASSIGN wnota = ""                                                 */
/*               wdata = ?.                                                 */
/*         END.                                                             */
/*       END.                                                               */

/*      MESSAGE  it-nota-fisc.nr-seq-ped ped-item.nr-sequencia  VIEW-AS ALERT-BOX.  */
    IF wdata = ?  THEN assign wdata-disp = " ".
    IF wdata <> ? THEN ASSIGN wdata-disp = STRING(wdata,"99/99/9999").
    
    FIND FIRST emitente use-index nome where 
         emitente.nome-abrev = ped-venda.nome-abrev no-lock no-error.
 
 
    FOR FIRST ord-prod FIELDS() USE-INDEX cliente-ped WHERE
        ord-prod.nome-abrev   = ext-ped-item.nome-abrev   AND
        ord-prod.nr-pedido    = ext-ped-item.nr-pedcli    AND
        ord-prod.nr-sequencia = ext-ped-item.nr-sequencia NO-LOCK. END.
    IF AVAIL ord-prod THEN DO:

       FOR EACH reservas OF ord-prod NO-LOCK,
           first item of reservas no-lock.
           
           FOR FIRST preco-item FIELDS(nr-tabpre preco-venda) WHERE
               preco-item.nr-tabpre  = emitente.nr-tabpre    AND
               preco-item.it-codigo = reservas.it-codigo AND 
               preco-item.situacao  = 1                  NO-LOCK. END.
               
              FIND es-etq-item WHERE es-etq-item.it-codigo = reservas.it-codigo NO-LOCK NO-ERROR.
              
              assign c-cv = IF AVAIL es-etq-item THEN STRING(es-etq-item.cod-local) ELSE ''.
              assign c-pnumber = entry(1,ITEM.codigo-refer,'') no-error.
              assign c-cor = entry(2,ITEM.codigo-refer,'') no-error.
   /* fat aberto */
   IF wdet-fat = 1 
    AND wdata = ?
    AND wnota = ""  THEN DO:
       PUT STREAM str-excel UNFORMAT
               ped-venda.nr-pedido                                        ';'
               ped-venda.nat-operacao                                     ';'
               ped-venda.dt-implant                                       ';'
               IF AVAIL preco-item THEN preco-item.nr-tabpre else ''      ';'
               reservas.it-codigo                                         ';'
               c-pnumber                                                  ';'
               c-cor                                                      ';'
               c-cv                                                       ';'
               ped-item.it-codigo                                         ';'
               ped-item.qt-pedida                                         ';'
               reserva.quant-orig                                         ';'
               IF AVAIL preco-item THEN (preco-item.preco-venda * reserva.quant-orig)  ELSE 0     ';'
               ped-item.vl-preuni                                         ';'
               ext-ped-item.pedido-cliente                                ';'
               ext-ped-item.cod-popid                                     ';'
               ped-item.nr-sequencia                                      ';'
               ord-prod.nr-ord-produ                                      ';'
               ped-item.dt-entrega                                        ';'
               wnota                                                      ';'
/*                wdata                                                      ';'  */
               wdata-disp                                                 ';'              
               SKIP.
          END.  /* fat aberto */
  /* fat fechad0 */
   IF wdet-fat = 2 
    AND wdata <> ?
    AND wnota <>  ""  THEN DO:
       PUT STREAM str-excel UNFORMAT
               ped-venda.nr-pedido                                        ';'
               ped-venda.nat-operacao                                     ';'
               ped-venda.dt-implant                                       ';'
               IF AVAIL preco-item THEN preco-item.nr-tabpre else ''      ';'
               reservas.it-codigo                                         ';'
               c-pnumber                                                  ';'
               c-cor                                                      ';'
               c-cv                                                       ';'
               ped-item.it-codigo                                         ';'
               ped-item.qt-pedida                                         ';'
               reserva.quant-orig                                         ';'
               IF AVAIL preco-item THEN (preco-item.preco-venda * reserva.quant-orig)  ELSE 0     ';'
               ped-item.vl-preuni                                         ';'
               ext-ped-item.pedido-cliente                                ';'
               ext-ped-item.cod-popid                                     ';'
               ped-item.nr-sequencia                                      ';'
               ord-prod.nr-ord-produ                                      ';'
               ped-item.dt-entrega                                        ';'
               wnota                                                      ';'
/*                wdata                                                      ';'  */
               wdata-disp                                                 ';'              
               SKIP.
          END.  /* fat fechado*/
   /* fat todos */
   IF wdet-fat = 3 THEN DO:
       PUT STREAM str-excel UNFORMAT
               ped-venda.nr-pedido                                        ';'
               ped-venda.nat-operacao                                     ';'
               ped-venda.dt-implant                                       ';'
               IF AVAIL preco-item THEN preco-item.nr-tabpre else ''      ';'
               reservas.it-codigo                                         ';'
               c-pnumber                                                  ';'
               c-cor                                                      ';'
               c-cv                                                       ';'
               ped-item.it-codigo                                         ';'
               ped-item.qt-pedida                                         ';'
               reserva.quant-orig                                         ';'
               IF AVAIL preco-item THEN (preco-item.preco-venda * reserva.quant-orig)  ELSE 0     ';'
               ped-item.vl-preuni                                         ';'
               ext-ped-item.pedido-cliente                                ';'
               ext-ped-item.cod-popid                                     ';'
               ped-item.nr-sequencia                                      ';'
               ord-prod.nr-ord-produ                                      ';'
               ped-item.dt-entrega                                        ';'
               wnota                                                      ';'
/*                wdata                                                      ';'  */
               wdata-disp                                                 ';'              
               SKIP.
          END.  /* fat todos*/



       END.
    END.
END.

OUTPUT STREAM str-excel CLOSE.

blk:
DO ON ERROR UNDO blk, LEAVE blk
   ON STOP UNDO blk, LEAVE blk:

    /* gera��o excel */
    {office/office.i Excel ch_excel}

    IF NOT VALID-OBJECT(ch_excel) THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "N�o foi possivel iniciar o Excel!~~N�o foi possivel iniciar o Excel!").

        LEAVE blk.

    END. /* IF NOT VALID-OBJECT(ch_excel) */

    ASSIGN ch_excel:VISIBLE = FALSE
           ch_workbook      = ch_excel:WorkBooks:OPEN(SEARCH("espsf\espsf039.xltm")).

    /**FSW
    ch_excel:RUN("carregarArquivo", c-arquivo).
    */

    ch_excel:VISIBLE = TRUE.

    OS-DELETE VALUE(c-arquivo).

END. /* DO ON ERROR UNDO blk, LEAVE blk */

DELETE OBJECT ch_workbook NO-ERROR.
DELETE OBJECT ch_excel NO-ERROR.


/*fechamento do output do relat�rio*/
 {include/i-rpclo.i &STREAM="stream str-rp"}  
RUN pi-finalizar IN h-acomp.
RETURN "OK":U.
