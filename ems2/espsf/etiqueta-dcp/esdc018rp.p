/*******************************************************************************
** Programa...: ESDC018rp
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..: Impress�o dos RG's
*******************************************************************************/
/* Include de Controle de Vers�o */
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESDC011RP.P 1.00.00.000}

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD c-rg-ini       AS CHAR
    FIELD c-rg-fim       AS CHAR.
    
DEFINE TEMP-TABLE tt-raw-digita
    FIELD raw-digita    AS RAW.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR i-padrao     AS INTE NO-UNDO.
DEF VAR i-quantidade AS INTE NO-UNDO.
DEF VAR c-valor      AS CHAR NO-UNDO.
DEF VAR h-acomp       AS HANDLE NO-UNDO.


/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ES011RP"
       c-versao       = "1.00"
       c-revisao      = "1.00.000"
       c-empresa      = empresa.nome
       c-sistema      = "ESP"
       c-titulo-relat = c-titulo-relat.

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN esp\esapi013i.w (INPUT 'esdc018rp').

IF  RETURN-VALUE = 'NOK' THEN
    RETURN.

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).

/**
*  Enviar etiquetas para impressora
*  API do Daniel Sartori
**/
    
    
    ASSIGN c-impressora = RETURN-VALUE
           i-padrao     = 001
           i-quantidade = 1.
    
    FOR EACH dc-rg-item
        WHERE dc-rg-item.rg-item >= tt-param.c-rg-ini
          AND dc-rg-item.rg-item <= tt-param.c-rg-fim.

        RUN pi-acompanhar IN h-acomp (INPUT dc-rg-item.rg-item).

        ASSIGN c-valor = STRING(INT(dc-rg-item.rg-item),"9999999999999").

        RUN esp\esapi013.p (INPUT c-impressora,
                            INPUT i-padrao,
                            INPUT i-quantidade,
                            INPUT c-valor).

        ASSIGN dc-rg-item.ind-impress = 2.
    END.

/*     FOR EACH dc-rg-item NO-LOCK                                           */
/*         WHERE dc-rg-item.nr-prod-produ = gl-cp0301-nr-ord-prod:           */
/*                                                                           */
/*         ASSIGN c-valor = STRING(INT(dc-rg-item.rg-item),"9999999999999"). */
/*                                                                           */
/*         RUN esp\esapi013.p (INPUT c-impressora,                           */
/*                             INPUT i-padrao,                               */
/*                             INPUT i-quantidade,                           */
/*                             INPUT c-valor).                               */
/*                                                                           */
/*     END.                                                                  */


RUN pi-finalizar IN h-acomp.

RETURN "OK":U.
