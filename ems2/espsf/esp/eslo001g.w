&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME d-vapara
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS d-vapara 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESLO001G 0.00.01.206}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
define output parameter     p-row-tabela    as rowid    no-undo.

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartVaPara
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME d-vapara

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rt-button c_it-codigo BUTTON-1 d_data ~
i_nr-ord-produ bt-ok bt-cancela bt-ajuda 
&Scoped-Define DISPLAYED-OBJECTS d_data i_nr-ord-produ 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-bt-ajuda 
       MENU-ITEM mi-sobre       LABEL "Sobre..."      .


/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ajuda 
     LABEL "&Ajuda" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-cancela AUTO-END-KEY 
     LABEL "&Cancelar" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-ok AUTO-GO 
     LABEL "&OK" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     LABEL "Procura ou Cria Ordem" 
     SIZE 28 BY .88.

DEFINE VARIABLE c_it-codigo AS CHARACTER FORMAT "x(16)":U 
     LABEL "Item" 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88 NO-UNDO.

DEFINE VARIABLE d_data AS DATE FORMAT "99/99/9999":U 
     LABEL "Data" 
     VIEW-AS FILL-IN 
     SIZE 10 BY .88 NO-UNDO.

DEFINE VARIABLE i_nr-ord-produ AS INTEGER FORMAT ">>>,>>9":U INITIAL 0 
     LABEL "Ordem" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-button
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 91 BY 1.42
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME d-vapara
     c_it-codigo AT ROW 2 COL 6 COLON-ALIGNED WIDGET-ID 2
     BUTTON-1 AT ROW 2 COL 26 WIDGET-ID 8
     d_data AT ROW 2 COL 60 COLON-ALIGNED WIDGET-ID 4
     i_nr-ord-produ AT ROW 2 COL 81 COLON-ALIGNED WIDGET-ID 6
     bt-ok AT ROW 4.5 COL 2.14
     bt-cancela AT ROW 4.5 COL 13
     bt-ajuda AT ROW 4.5 COL 81
     rt-button AT ROW 4.25 COL 1
     SPACE(0.13) SKIP(0.07)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "V� Para <insira complemento>"
         DEFAULT-BUTTON bt-ok CANCEL-BUTTON bt-cancela WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartVaPara
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB d-vapara 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-vapara.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX d-vapara
   FRAME-NAME L-To-R                                                    */
ASSIGN 
       FRAME d-vapara:SCROLLABLE       = FALSE
       FRAME d-vapara:HIDDEN           = TRUE.

ASSIGN 
       bt-ajuda:POPUP-MENU IN FRAME d-vapara       = MENU POPUP-MENU-bt-ajuda:HANDLE.

/* SETTINGS FOR FILL-IN c_it-codigo IN FRAME d-vapara
   NO-DISPLAY                                                           */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX d-vapara
/* Query rebuild information for DIALOG-BOX d-vapara
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX d-vapara */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "AutoField" d-vapara _INLINE
/* Actions: masters/d-vapara-af.p ? ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME d-vapara
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL d-vapara d-vapara
ON GO OF FRAME d-vapara /* V� Para <insira complemento> */
DO:
FIND lo-detord NO-LOCK
    WHERE lo-detord.it-codigo EQ INPUT FRAME {&FRAME-NAME} c_it-codigo
      AND lo-detord.data EQ INPUT FRAME {&FRAME-NAME} d_data
      AND lo-detord.nr-ord-produ EQ INPUT FRAME {&FRAME-NAME} i_nr-ord-produ NO-ERROR.

IF NOT AVAILABLE lo-detord THEN DO:
    {utp/ut-table.i mgfas lo-detord 1}
    RUN utp/ut-msgs.p(INPUT "show",
                      INPUT 2,
                      INPUT RETURN-VALUE).
    RETURN NO-APPLY.
END.
ASSIGN p-row-tabela = ROWID(lo-detord).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL d-vapara d-vapara
ON WINDOW-CLOSE OF FRAME d-vapara /* V� Para <insira complemento> */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda d-vapara
ON CHOOSE OF bt-ajuda IN FRAME d-vapara /* Ajuda */
OR HELP OF FRAME {&FRAME-NAME}
DO: /* Call Help Function (or a simple message). */
      MESSAGE       "!!!! CONSIDERACOES IMPORTANTES !!!"
          SKIP(1)  "1) O BOT�O PROCURA OU CRIA ORDEM ... IR� VERIFICAR SE EXITE DETALHE DA ORDEM PARA ESTE ITEM NO DIA CORRENTE."
                   "SE FOR ENCONTRADO OS CAMPOS DE PESQUISA SER�O COMPLETADOS COM OS DADOS."
          SKIP(2)  "2) SE N�O ENCONTRAR SER� TRAZIDO A �LTIMA ORDEM PARA O ITEM NO CADASTRO DE ORDENS."
          SKIP     "E SER� QUESTIONADO SE QUER CRIAR O DETALHE DA ORDEM PARA O DIA CORRENTE."
          SKIP(5)
          "Obs.: Vers�o nova do antigo programa da Intranet LI1050"
          VIEW-AS ALERT-BOX.
    {include/ajuda.i}
/* OBSERVA��ES 
- O CAMPO QUANTIDADE � ATUALIZADO NESTE PROGRAMA PARA APARECER NO RELAT�RIO LI1010 - EXIBE PERCENTUAL -
NO CAMPO COM T�TULO NO RELAT�RIO DE PRV NA DTA
*/

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 d-vapara
ON CHOOSE OF BUTTON-1 IN FRAME d-vapara /* Procura ou Cria Ordem */
DO:
   DEFINE VARIABLE wconfirma AS LOGICAL. 
   
   ASSIGN c_it-codigo = INPUT FRAME {&FRAME-NAME} c_it-codigo
          wconfirma = NO.
    /* PROCURA ORDEM NO EMS */
    FIND LAST ord-prod
                WHERE ord-prod.it-codigo   = c_it-codigo
              /*  AND   ord-prod.dt-inicio  LE d_data
                AND   ord-prod.dt-termino GE d_data
                /*AND   ord-prod.cd-planejado MATCHES(wplanej)*/*/
                AND   ord-prod.estado     NE 7
                AND   ord-prod.estado     NE 8
                NO-LOCK NO-ERROR.
   IF AVAILABLE ord-prod THEN DO:
    ASSIGN i_nr-ord-produ = ord-prod.nr-ord-produ.
                   d_data = ord-prod.dt-inicio.
    DISPLAY i_nr-ord-produ d_data WITH FRAME     {&FRAME-NAME}.

    FIND FIRST lo-detord WHERE lo-detord.it-codigo = ord-prod.it-codigo
                         AND   lo-detord.data = TODAY
                         AND   lo-detord.nr-ord-produ = ord-prod.nr-ord-produ
                         NO-LOCK NO-ERROR.
      
    /* se existe lo-detord */
    IF AVAILABLE lo-detord THEN do:
      ASSIGN i_nr-ord-produ = lo-detord.nr-ord-produ.
                      d_data = lo-detord.data.
                     
      DISPLAY i_nr-ord-produ d_data WITH FRAME     {&FRAME-NAME}.
      END.
    /* SE N�O EXISTE LO-DETORD */
    IF NOT AVAILABLE lo-detord THEN DO:
      MESSAGE "N�O ENCONTRADO O DETALHE DA ORDEM !!!"
          SKIP "Ordem: " ord-prod.nr-ord-produ "Para o item: " ord-prod.it-codigo
               "Data: " TODAY "N�O ENCONTRADA"
          SKIP
          "DESEJA CRIAR O DETALHE DA ORDEM?"
          VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL TITLE  "Confirma?"  
          UPDATE wconfirma.
      /* criando registro */    
      IF wconfirma = YES THEN DO:
        CREATE lo-detord.
        ASSIGN lo-detord.it-codigo    = ord-prod.it-codigo
                   lo-detord.data         = TODAY
                   lo-detord.nr-ord-produ = ord-prod.nr-ord-produ
                   lo-detord.qtde         = 0.
        MESSAGE lo-detord.it-codigo lo-detord.data lo-detord.nr-ord-produ lo-detord.qtde  
              lo-detord.turno
              "O DETALHE DA ORDEM FOI CRIADO, INSERIR A QUANTIDADE" VIEW-AS ALERT-BOX.
        ASSIGN d_data = lo-detord.data.
        DISPLAY d_data WITH FRAME     {&FRAME-NAME}.
      END.
      END.
  END. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sobre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sobre d-vapara
ON CHOOSE OF MENU-ITEM mi-sobre /* Sobre... */
DO:
  {include/sobre.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK d-vapara 


/* ***************************  Main Block  *************************** */

assign p-row-tabela = ?.

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects d-vapara  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available d-vapara  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI d-vapara  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME d-vapara.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI d-vapara  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY d_data i_nr-ord-produ 
      WITH FRAME d-vapara.
  ENABLE rt-button c_it-codigo BUTTON-1 d_data i_nr-ord-produ bt-ok bt-cancela 
         bt-ajuda 
      WITH FRAME d-vapara.
  VIEW FRAME d-vapara.
  {&OPEN-BROWSERS-IN-QUERY-d-vapara}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy d-vapara 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize d-vapara 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "ESLO001G" "0.00.06.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records d-vapara  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartVaPara, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed d-vapara 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

