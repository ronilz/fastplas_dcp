&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DAFTASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF004RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD westab           AS CHAR FORMAT "X(03)"
    FIELD wfm-ini         AS CHAR FORMAT "X(08)"
    FIELD wfm-fim         AS CHAR FORMAT "X(08)"
    FIELD wit-ini          AS CHAR FORMAT "X(16)"
    FIELD wit-fim          AS CHAR FORMAT "X(16)"
    FIELD wdt-termino      AS DATE FORMAT "99/99/9999".

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */
/*DEFINE VARIABLE wwcc      LIKE grup-maquina.cc-codigo EXTENT 20 
                          COLUMN-LABEL "C.C".*/
DEFINE VARIABLE wwcc      AS CHAR FORMAT "X(06)" EXTENT 20 COLUMN-LABEL "CC".
DEFINE VARIABLE wquant    AS DECIMAL FORMAT ">,>>9.9999" EXTENT 20
                          COLUMN-LABEL "Temp.Homem".
DEFINE VARIABLE wquantr   AS DECIMAL FORMAT "ZZZ,ZZ9.9999"
                          COLUMN-LABEL "Temp.Homem".
DEFINE VARIABLE wcont     AS INTEGER FORMAT "99".
DEFINE VARIABLE wlabel    AS INTEGER FORMAT "99".
DEFINE VARIABLE wtotcc    AS DECIMAL FORMAT "999,999.9999".
DEFINE VARIABLE wtotccr   AS DECIMAL FORMAT "999,999.9999".
DEFINE BUFFER b-estnivel2 FOR estrutura.                                      
DEFINE BUFFER b-estnivel3 FOR estrutura.
DEFINE BUFFER b-estnivel4 FOR estrutura.
DEFINE BUFFER b-estnivel5 FOR estrutura.
DEFINE BUFFER b-estnivel6 FOR estrutura.
DEFINE BUFFER b-estnivel7 FOR estrutura.
DEFINE BUFFER b-estnivel8 FOR estrutura.
DEFINE BUFFER b-estnivel9 FOR estrutura.
DEFINE BUFFER b-estnivel10 FOR estrutura.
DEFINE BUFFER b-estnivel11 FOR estrutura.
DEFINE BUFFER b-estnivel12 FOR estrutura.
DEFINE BUFFER b-estnivel13 FOR estrutura.
DEFINE BUFFER b-estnivel14 FOR estrutura.
DEFINE STREAM reldet.
DEFINE WORKFILE wcctempo
  FIELD witpai              LIKE item.it-codigo
  FIELD witfilho            LIKE item.it-codigo
  FIELD wcc                 LIKE grup-maquina.cc-codigo
  FIELD wtempo-homem        LIKE operacao.tempo-homem
  FIELD wproporcao          LIKE operacao.proporcao.
DEFINE VARIABLE wemp        AS CHAR FORMAT "X(40)".
DEFINE VARIABLE wlinha      AS CHAR FORMAT "X(315)".

DEFINE VARIABLE wconf-imp   AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE warq        AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wtipo       AS INTEGER NO-UNDO. /* tipo de impressora */
DEFINE VARIABLE warq1       AS CHAR FORMAT "X(30)".

DEFINE VARIABLE wseq        AS INTEGER FORMAT "99".
DEFINE VARIABLE wlimpa      AS INTEGER FORMAT "99".
DEFINE VARIABLE wlimpa1     AS INTEGER FORMAT "99".
/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.



/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PROC.ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "REL. TOTAIS TEMPO HOMEM P/ ITEM/C.C./MINUTO".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).*/
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.

  DISPLAY "Estab.:" tt-param.westab "/" 
          "Familia: " tt-param.wfm-ini "-"  tt-param.wfm-ini
          "Item: " tt-param.wit-ini "-" tt-param.wit-fim
          "Data termino base: " tt-param.wdt-termino
          WITH FRAME f-cabec1 WIDTH 132 NO-LABELS.

/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* procedure geracao */
/*FIND FIRST empresa NO-LOCK NO-ERROR.*/
  FOR EACH item WHERE item.fm-codigo GE tt-param.wfm-ini
                AND   item.fm-codigo LE tt-param.wfm-fim 
                AND   item.it-codigo GE tt-param.wit-ini
                AND   item.it-codigo LE tt-param.wit-fim
                AND   item.cod-obsoleto  = 1
                AND   ITEM.cod-estabel   = tt-param.westab
                NO-LOCK:
   run pi-acompanhar in h-acomp (input "Fam�lia: " + STRING(ITEM.fm-codigo)).
   FOR EACH operacao OF item 
                      WHERE data-termino  GE tt-param.wdt-termino
                      NO-LOCK BREAK 
                      BY operacao.it-codigo 
                      BY operacao.gm-codigo:
      /*
      FOR EACH grup-maquina OF operacao 
                            NO-LOCK 
                            BREAK BY grup-maquina.cc-codigo:
        */
      FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
        CREATE wcctempo.
        ASSIGN witpai                = item.it-codigo
               witfilho              = operacao.it-codigo
               wcctempo.wcc          = gm-estab.cc-codigo
               wcctempo.wtempo-homem = operacao.tempo-homem
               wproporcao            = operacao.proporcao.
        END.
      END.    
      /* controle de niveis */
      FOR EACH estrutura  WHERE estrutura.it-codigo = item.it-codigo 
                         AND   estrutura.data-termino GE TODAY 
                         NO-LOCK BREAK BY estrutura.it-codigo :
         IF FIRST-OF(estrutura.it-codigo)  THEN ASSIGN wseq = 0.
         ASSIGN wseq = wseq + 1.
         IF LASt-OF(estrutura.it-codigo) THEN  DO: 
           IF wseq > 14 THEN MESSAGE
           "Item: " estrutura.it-codigo "Estouro de niveis de estrutura tratado pelo relat�rio !"
           SKIP 
           "EXISTEM MAIS DE 14 N�VEIS  E O PROGRAMA TRATA AT� 14!!!"  
            SKIP
           "CASO NECESS�RIO ENTRE EM CONTATO COM O SETOR DE INFORM�TICA."
            VIEW-AS ALERT-BOX.
           END.
        END.
      /* NIVEL 1 */
      FOR EACH estrutura WHERE estrutura.it-codigo = item.it-codigo 
                         AND   estrutura.data-termino GE TODAY NO-LOCK:
        FOR EACH operacao WHERE operacao.it-codigo = estrutura.es-codigo
                          AND   operacao.data-termino GE TODAY
                          NO-LOCK BREAK BY operacao.it-codigo 
                                      BY operacao.gm-codigo:
          /*FOR EACH grup-maquina OF operacao 
                                NO-LOCK 
                                BREAK BY grup-maquina.cc-codigo:
            */
          FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
            CREATE wcctempo.
            ASSIGN witpai                = item.it-codigo
                   witfilho              = operacao.it-codigo
                   wcctempo.wcc          = gm-estab.cc-codigo
                   wcctempo.wtempo-homem = operacao.tempo-homem
                   wproporcao            = operacao.proporcao.  
            END.
          END.
        /* NIVEL 2 */
        FOR EACH b-estnivel2 WHERE b-estnivel2.it-codigo = 
                                   estrutura.es-codigo
                             AND   b-estnivel2.data-termino GE TODAY 
                             NO-LOCK:
          FOR EACH operacao WHERE operacao.it-codigo = b-estnivel2.es-codigo
                            AND   operacao.data-termino GE TODAY
                            NO-LOCK BREAK BY operacao.it-codigo 
                                        BY operacao.gm-codigo:
            FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
              CREATE wcctempo. 
              ASSIGN witpai                = item.it-codigo
                     witfilho              = operacao.it-codigo
                     wcctempo.wcc          = gm-estab.cc-codigo
                     wcctempo.wtempo-homem = operacao.tempo-homem
                     wproporcao            = operacao.proporcao.
              END.
            END.
          /* NIVEL 3 */
          FOR EACH b-estnivel3 WHERE b-estnivel3.it-codigo = 
                                     b-estnivel2.es-codigo 
                               AND   b-estnivel3.data-termino GE TODAY 
                               NO-LOCK:
            FOR EACH operacao WHERE operacao.it-codigo = b-estnivel3.es-codigo
                              AND   operacao.data-termino GE TODAY
                              NO-LOCK BREAK BY operacao.it-codigo 
                                            BY operacao.gm-codigo:
                FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                CREATE wcctempo.
                ASSIGN wcctempo.witpai       = item.it-codigo
                       witfilho              = operacao.it-codigo
                       wcctempo.wcc          = gm-estab.cc-codigo
                       wcctempo.wtempo-homem = operacao.tempo-homem
                       wproporcao            = operacao.proporcao.
                END.
              END.
            /* NIVEL 4 */
            FOR EACH b-estnivel4 WHERE b-estnivel4.it-codigo = 
                                       b-estnivel3.es-codigo 
                                 AND   b-estnivel4.data-termino GE TODAY 
                                 NO-LOCK:
              FOR EACH operacao WHERE operacao.it-codigo = b-estnivel4.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                  CREATE wcctempo.
                  ASSIGN wcctempo.witpai       = item.it-codigo
                         wcctempo.witfilho     = operacao.it-codigo
                         wcctempo.wcc          = gm-estab.cc-codigo
                         wcctempo.wtempo-homem = operacao.tempo-homem
                         wproporcao            = operacao.proporcao.
                  END.
                END.
              /* NIVEL 5 */
              FOR EACH b-estnivel5 WHERE b-estnivel5.it-codigo = 
                                         b-estnivel4.es-codigo 
                                   AND   b-estnivel5.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel5.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
         
              /* NIVEL  6*/
              FOR EACH b-estnivel6 WHERE b-estnivel6.it-codigo = 
                                         b-estnivel5.es-codigo 
                                   AND   b-estnivel6.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel6.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
            /* NIVEL  7*/
              FOR EACH b-estnivel7 WHERE b-estnivel7.it-codigo = 
                                         b-estnivel6.es-codigo 
                                   AND   b-estnivel7.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel7.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
           /* NIVEL  8*/
              FOR EACH b-estnivel8 WHERE b-estnivel8.it-codigo = 
                                         b-estnivel7.es-codigo 
                                   AND   b-estnivel8.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel8.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              /* NIVEL  9*/
              FOR EACH b-estnivel9 WHERE b-estnivel9.it-codigo = 
                                         b-estnivel8.es-codigo 
                                   AND   b-estnivel9.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel9.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              /* NIVEL  10*/
              FOR EACH b-estnivel10 WHERE b-estnivel10.it-codigo = 
                                         b-estnivel9.es-codigo 
                                   AND   b-estnivel10.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel10.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              /* NIVEL  11*/
              FOR EACH b-estnivel11 WHERE b-estnivel11.it-codigo = 
                                         b-estnivel10.es-codigo 
                                   AND   b-estnivel11.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel7.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
           /* NIVEL  12*/
              FOR EACH b-estnivel12 WHERE b-estnivel12.it-codigo = 
                                         b-estnivel11.es-codigo 
                                   AND   b-estnivel12.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel8.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              /* NIVEL  13*/
              FOR EACH b-estnivel13 WHERE b-estnivel13.it-codigo = 
                                         b-estnivel2.es-codigo 
                                   AND   b-estnivel3.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel9.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              /* NIVEL  14*/
              FOR EACH b-estnivel14 WHERE b-estnivel14.it-codigo = 
                                         b-estnivel13.es-codigo 
                                   AND   b-estnivel14.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel10.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                      END.
                    END.
                   END. 
                  END. 
                END.      
               END.
              END.
             END.     
            END.
          END.
         END.
        END.
       END.
     END.
    END.
   END.
  END.
RUN arquivo.     
  /*fim dos niveis */
END PROCEDURE.

/* procedure arquivo*/
PROCEDURE arquivo.
    FORM
    SKIP(1) "Familia inicial:" AT 22 tt-param.wfm-ini
    SKIP    "Familia final  :" AT 22 tt-param.wfm-fim
    SKIP    "Item inicial   :" AT 22 tt-param.wit-ini
    SKIP    "Item final     :" AT 22 tt-param.wit-fim
    SKIP    "Data de corte  :" AT 22 wdt-termino
    SKIP(1) "                             *** CONSIDERACOES ***"
    SKIP    " 1 - Este programa considera a data de termino maior or igual a data"
            "     digitada no parametro acima."
    SKIP    " 2 - Este programa considera ate 6 niveis da estrutura conforme pre'"
            "definido"
    SKIP    "     na confeccao do mesmo."
    SKIP    " 3 - Somente serao listados os itens ativos (codigo obsoleto = 0)."
    SKIP(1)
    WITH FRAME f-param row 04 NO-LABELS WIDTH 80.

  /* ARQUIVO */
  FORM 
    item.fm-codigo  
    wcctempo.witpai item.un  
    ITEM.descricao-1 
    wwcc[1]  AT 65   wquant[1] SPACE(3) 
    wwcc[2]      SPACE(3) wquant[2]   SPACE(3)
    wwcc[3]      SPACE(3) wquant[3]   SPACE(3)
    wwcc[4]      SPACE(3) wquant[4]   SPACE(3)   
    wwcc[5]      SPACE(3) wquant[5]   SPACE(3)     
    wwcc[6]      SPACE(3) wquant[6]   SPACE(3)  
    wwcc[7]      SPACE(3) wquant[7]   SPACE(3)   
    wwcc[8]      SPACE(3) wquant[8]   SPACE(3)
    wwcc[9]      SPACE(3) wquant[9]   SPACE(3)
    wwcc[10]     SPACE(3) wquant[10]  SPACE(3)
    skip 
    wwcc[11]  AT 65  wquant[11]   SPACE(3)  
    wwcc[12]     SPACE(3) wquant[12]        SPACE(3)
    wwcc[13]     SPACE(3) wquant[13]        SPACE(3)
    wwcc[14]     SPACE(3) wquant[14]        SPACE(3)
    wwcc[15]     SPACE(3) wquant[15]        SPACE(3)
    wwcc[16]     SPACE(3) wquant[16]        SPACE(3)
    wwcc[17]     SPACE(3) wquant[17]        SPACE(3)
    wwcc[18]     SPACE(3) wquant[18]        SPACE(3)
    wwcc[19]     SPACE(3) wquant[19]        SPACE(3)
    wwcc[20]     SPACE(3) wquant[20]        SPACE(3)
    SKIP 
    wlinha NO-LABEL 
    WITH FRAME f-arquivo DOWN WIDTH 600.
  /*
    HEADER
    wemp
    TODAY FORMAT "99/99/9999"
    STRING(TIME,"HH:MM:SS") SPACE(3) "HS"  
    "Pag.:" PAGE-NUMBER FORMAT "999"
    SKIP
    "RELATORIO DE TOTAIS DE TEMPO HOMEM POR ITEM E POR C.C. POR MINUTO - Estab.:" tt-param.westab            
    "- Familia:  " tt-param.wfm-ini " a " tt-param.wfm-fim " - Item: " tt-param.wit-ini " a " tt-param.wit-fim
    SKIP(1)
    "Familia"
    "Item"      AT 14
    "UN"        AT 32
    "Descricao" AT 38
    "CC."       AT 65
    "T. Hs."    AT 80
    "CC."       AT 89
    "T. Hs."    AT 104
    "CC."       AT 114
    "T. Hs."    AT 129
    "CC."       AT 139
    "T. Hs."    AT 154
    "CC."       AT 163
    "T. Hs."    AT 179
    "CC."       AT 188
    "T. Hs."    AT 203
    "CC."       AT 213
    "T. Hs."    AT 228
    "CC."       AT 238
    "T. Hs."    AT 253
    "CC."       AT 262
    "T. Hs."    AT 277
    "CC."      AT 287
    "T. Hs."   AT 302 
    WITH FRAME f-arquivo DOWN WIDTH 600 NO-LABEL.
   /* RELAT�RIO */
   FORM 
    item.fm-codigo  
    wcctempo.witpai item.un  
    ITEM.descricao-1 
    "CC."             AT 63
    "T. Hs."          AT 78
    "CC."             AT 87
    "T. Hs."          AT 102
    "CC."             AT 112
    "T. Hs."          AT 127
    "CC."             AT 137
    "T. Hs."          AT 152
    "CC."             AT 161
    "T. Hs."          AT 177
    wwcc[1]  AT 63    wquant[1] 
    wwcc[2]           wquant[2] 
    wwcc[3]           wquant[3] 
    wwcc[4]           wquant[4]      
    wwcc[5]           wquant[5]   
    SKIP(1)
    "CC."             AT 63
    "T. Hs."          AT 78
    "CC."             AT 87
    "T. Hs."          AT 102
    "CC."             AT 112
    "T. Hs."          AT 127
    "CC."             AT 137
    "T. Hs."          AT 152
    "CC."             AT 161
    "T. Hs."          AT 177
    wwcc[6]  AT 63    wquant[6]     
    wwcc[7]           wquant[7]      
    wwcc[8]           wquant[8]
    wwcc[9]           wquant[9]   
    wwcc[10]          wquant[10]
    skip(1)
    "CC."             AT 63
    "T. Hs."          AT 78
    "CC."             AT 87
    "T. Hs."          AT 102
    "CC."             AT 112
    "T. Hs."          AT 127
    "CC."             AT 137
    "T. Hs."          AT 152
    "CC."             AT 161
    "T. Hs."          AT 177
    wwcc[11]  AT 63   wquant[11]       
    wwcc[12]          wquant[12]     
    wwcc[13]          wquant[13]      
    wwcc[14]          wquant[14]       
    wwcc[15]          wquant[15]       
    SKIP(1)
    "CC."             AT 63
    "T. Hs."          AT 78
    "CC."             AT 87
    "T. Hs."          AT 102
    "CC."             AT 112
    "T. Hs."          AT 127
    "CC."             AT 137
    "T. Hs."          AT 152
    "CC."             AT 161
    "T. Hs."          AT 177
    wwcc[16]  AT 63   wquant[16]      
    wwcc[17]          wquant[17]      
    wwcc[18]          wquant[18]      
    wwcc[19]          wquant[19]      
    wwcc[20]          wquant[20]   
    SKIP 
    wlinha NO-LABEL 
    HEADER
    wemp
    TODAY FORMAT "99/99/9999"
    STRING(TIME,"HH:MM:SS") SPACE(3) "HS"  
    "Pag.:" PAGE-NUMBER FORMAT "999"
    SKIP
    "RELATORIO DE TOTAIS DE TEMPO HOMEM POR ITEM E POR C.C. - Estab.:" tt-param.westab            
    "- Familia:  " tt-param.wfm-ini " a " tt-param.wfm-fim " - Item:  " tt-param.wit-ini " a " tt-param.wit-fim
    SKIP(1)
    "Familia"
    "Item"        AT 14
    "UN"          AT 32
    "Descricao"   AT 38
   /*
    "CC."       AT 188
    "Qtd."      AT 203
    "CC."       AT 213
    "Qtd."      AT 228
    "CC."       AT 238
    "Qtd."      AT 253
    "CC."       AT 262
    "Qtd."      AT 277
    "CC."      AT 287
    "Qtd."     AT 302 
    */
    WITH FRAME f-impressora DOWN WIDTH 600 NO-LABEL.
*/

  /* GERACAO DO ARQUIVO  */
    /*OUTPUT TO VALUE(warq1).*/
   DO wlimpa1 =  1 TO 20:
     ASSIGN wwcc[wlimpa1] = ""
            wquant[wlimpa1] = 0.
     END.
    FOR EACH wcctempo NO-LOCK BREAK BY wcctempo.witpai BY wcctempo.wcc: 
     IF FIRST-OF(wcctempo.witpai) THEN DO: 

       /*
        IF wlabel > 08 THEN MESSAGE "ITEM POSSUI MAIS QUE 07 N�VEIS DE ESTRUTURA!"
                                    SKIP "*** GERA��O DO RELAT�RIO COM PROBLEMA. ***"
                                    wcctempo.witpai VIEW-AS ALERT-BOX.
        */
        ASSIGN wcont  = 0.
               wlabel = 0.

        END.

        IF FIRST-OF(wcctempo.wcc) THEN DO:
          ASSIGN wtotcc = 0.
          FIND FIRST item WHERE item.it-codigo = wcctempo.witpai NO-LOCK NO-ERROR.
          END.
        ASSIGN wtotcc = wtotcc + wcctempo.wtempo-homem.

        IF LAST-OF(wcctempo.wcc) THEN DO:
          wlabel = wlabel + 1.
          ASSIGN wwcc[wlabel]    = wcctempo.wcc.

          /* Estabelecimento 1 */
          IF tt-param.westab = "1"  THEN DO:
          IF wproporcao =  100 THEN DO:
             ASSIGN wquant[wlabel]  = wtotcc.
             END.
          ELSE DO: 
             ASSIGN wquant[wlabel]  = ((wtotcc * wproporcao) / 100).
             END.
          END.
          /* Estabelecimento 2 e 3 */
          IF tt-param.westab = "2" 
          OR tt-param.westab = "3" THEN DO:
            IF wproporcao =  100 THEN DO:
             ASSIGN wquant[wlabel]  = (wtotcc / 100) * 60.
             END.
           ELSE DO: 
             ASSIGN wquant[wlabel]  = (((wtotcc * wproporcao) / 100) / 100) * 60.
             END. 
            END.

          END.       
        IF LAST-OF(wcctempo.witpai) THEN DO:
          ASSIGN wwcc[1]   = string(INTEGER(wwcc[1]),"999999")
                 wwcc[2]   = string(INTEGER(wwcc[2]),"999999")
                 wwcc[3]   = string(INTEGER(wwcc[3]),"999999")
                 wwcc[4]   = string(INTEGER(wwcc[4]),"999999")
                 wwcc[5]   = string(INTEGER(wwcc[5]),"999999")
                 wwcc[6]   = string(INTEGER(wwcc[6]),"999999")
                 wwcc[7]   = string(INTEGER(wwcc[7]),"999999")
                 wwcc[8]   = string(INTEGER(wwcc[8]),"999999")
                 wwcc[9]   = string(INTEGER(wwcc[9]),"999999")
                 wwcc[10]  = string(INTEGER(wwcc[10]),"999999")
                 wwcc[11]  = string(INTEGER(wwcc[11]),"999999")
                 wwcc[12]  = string(INTEGER(wwcc[12]),"999999")
                 wwcc[13]  = string(INTEGER(wwcc[13]),"999999")
                 wwcc[14]  = string(INTEGER(wwcc[14]),"999999")
                 wwcc[15]  = string(INTEGER(wwcc[15]),"999999")
                 wwcc[16]  = string(INTEGER(wwcc[16]),"999999")
                 wwcc[17]  = string(INTEGER(wwcc[17]),"999999")
                 wwcc[18]  = string(INTEGER(wwcc[18]),"999999")
                 wwcc[19]  = string(INTEGER(wwcc[19]),"999999")
                 wwcc[20]  = string(INTEGER(wwcc[20]),"999999").
            DISPLAY item.fm-codigo   COLUMN-LABEL "Familia"
                  wcctempo.witpai  COLUMN-LABEL "Item"     
                  item.un          COLUMN-LABEL "UN"
                  item.descricao-1 COLUMN-LABEL "Descricao" 
                  wwcc             COLUMN-LABEL "C.C."
                  wquant           COLUMN-LABEL "Qtd."      
                  wlinha
                  WITH FRAME f-arquivo DOWN.
            DOWN WITH FRAME f-arquivo.
       
           END.
        END.
END PROCEDURE.




