&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPSF001RP 0.00.00.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param
    field destino            as integer
    field arquivo            as char format "x(50)":U
    field usuario            as char format "x(12)":U
    field data-exec          as date
    field hora-exec          as integer
    field classifica         as integer
    field desc-classifica    as char format "x(40)":U
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wcod-estabel       AS CHAR FORMAT "x(03)"
    FIELD wit-codigo-ini     LIKE ITEM.it-codigo
    FIELD wit-codigo-fin     LIKE ITEM.it-codigo
    FIELD wge-codigo-ini     LIKE ITEM.ge-codigo
    FIELD wge-codigo-fin     LIKE ITEM.ge-codigo
    FIELD wcod-depos-ini     LIKE saldo-estoq.cod-depos
    FIELD wcod-depos-fin     LIKE saldo-estoq.cod-depos.


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    
/* variaveis de totais */

DEF VAR WWSALDO       LIKE saldo-estoq.qtidade-atu.
DEF VAR went          LIKE saldo-estoq.qtidade-atu.
DEF VAR wsai          LIKE saldo-estoq.qtidade-atu.

DEF VAR f-WWSALDO      LIKE saldo-estoq.qtidade-atu.
DEF VAR f-went          LIKE saldo-estoq.qtidade-atu.
DEF VAR f-wsai          LIKE saldo-estoq.qtidade-atu.

/* defini��o buffers*/
DEFINE BUFFER b-saldo-estoq FOR saldo-esto.

/*--------------------------------------------*/
form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
 with no-box width 132 down stream-io frame f-relat.



create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}
/*
find empresa
    where empresa.ep-codigo = v_cdn_empres_usuar
    no-lock no-error.
*/
find first param-global no-lock no-error.

{utp/ut-liter.i "EMS204 - PROCEDIMENTOS ESPECIAIS - ESTOQUE * }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * }
assign c-titulo-relat = "REL.SALDO ATUAL ESTOQUE - ITENS NEGATIVOS".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
      run pi-inicializar in h-acomp (input "Processando ...":U). 
       DISPLAY  
       "Est Item             Desricao                             Gr Dep Localiz           Qtde Atual" SKIP
       "--- ---------------- ---------------------------------    -- --- ---------- -----------------"
       WITH STREAM-IO PAGE-TOP WIDTH 100.
    /*:T --- Colocar aqui o c�digo de impress�o --- */

       
    FOR EACH ITEM WHERE ITEM.IT-CODIGO GE tt-param.wit-codigo-ini
                  AND   ITEM.it-codigo LE tt-param.wit-codigo-fin
                  AND   ITEM.ge-codigo GE tt-param.wge-codigo-ini
                  AND   ITEM.ge-codigo LE tt-param.wge-codigo-fin
                  NO-LOCK BY item.ge-codigo:
      FOR EACH saldo-estoq WHERE  saldo-estoq.it-codigo   = ITEM.it-codigo
                         AND    saldo-estoq.cod-estabel = item.cod-estabel
                         NO-LOCK:
                     
      run pi-acompanhar in h-acomp (input "Item:  " + saldo-estoq.it-codigo).
      IF  saldo-estoq.cod-depos       GE tt-param.wcod-depos-ini  
          AND saldo-estoq.cod-depos   LE  tt-param.wcod-depos-fin   
      THEN
      IF qtidade-atu < 0 THEN RUN lista_dados.
      END.
    END.

   


/* PROCEDURE lista_dados--------------------------------------------------------------*/

procedure lista_dados.  
  
    FOR EACH b-saldo-estoq WHERE b-saldo-estoq.it-codigo   = saldo-estoq.it-codigo
                           AND   b-saldo-estoq.cod-estabel = saldo-estoq.cod-estabel
                           /*AND   b-saldo-estoq.cod-depos   = saldo-estoq.cod-depos */
                           NO-LOCK:
    IF b-saldo-estoq.qtidade-atu <> 0 THEN 
        DISPLAY  b-saldo-estoq.cod-estabel b-saldo-estoq.it-codigo item.descricao-1 SPACE(0) descricao-2 
             item.ge-codigo b-saldo-estoq.cod-depos 
             b-saldo-estoq.cod-localiz b-saldo-estoq.qtidade-atu WITH STREAM-IO NO-LABELS WIDTH 100.
    run pi-acompanhar in h-acomp (input "Item:  " + b-saldo-estoq.it-codigo).
    END.
  
   
END.
/*----------------------------------------------------------------------------------------*/
    
    
    
    
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


