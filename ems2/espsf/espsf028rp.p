 &ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPF028RP 0.00.04.001}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wcod-estab-i     AS CHAR FORMAT "X(03)"
    FIELD wcod-estab-f     AS CHAR FORMAT "X(03)"
    FIELD wemit-i          AS INTE FORMAT ">>>>>>>>9"
    FIELD wemit-f          AS INTE FORMAT  ">>>>>>>>9"
    FIELD wnr-docto-i      AS CHAR FORMAT "X(16)"
    FIELD wnr-docto-f      AS CHAR FORMAT "X(16)"  
    FIELD wnat-i           AS CHAR FORMAT "X(06)"
    FIELD wnat-f           AS CHAR FORMAT "X(06)"
    FIELD wdt-trans-i      AS DATE FORMAT "99/99/9999"
    FIELD wdt-trans-f      AS DATE FORMAT "99/99/9999".
define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    


/* stream valeria */
DEF STREAM wrel.
DEF VAR wdet AS CHAR FORMAT "X(36)" COLUMN-LABEL "     Vl.Tot.NF Cofins Ret    Pis Ret".
DEF VAR  wvl-dupl-simp LIKE  dupli-apagar.vl-a-pagar.
DEF VAR wimp-ret LIKE  dupli-apagar.vl-a-pagar.


form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 400 down stream-io frame f-relat.

form
    /*campos-do-relatorio*/
     with no-box width 400 down stream-io frame f-relat1.

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PROC.ESPECIAIS"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Relat�rio Confer�ncia Documentos Fiscais".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U). */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
  

  OUTPUT STREAM wrel TO VALUE("V:\spool\ESPSF028-IMP-EXCEL.TXT") PAGED.
  DISPLAY  STREAM wrel
  "Est Doc       Emitente Nat.OP Dt Impl  Item                Vl.Merc        Base ICMS    Vl.ICMS         Base IPI     Vl.IPI          Base PIS       Base COFINS     Vl.PIS  Vl.COFINS      B.ICMS Subs VlICMSSUBS     Vl Total NF   PIS Ret  Cofins Ret    Vl Duplicata"
  WITH FRAME f-cab-stream STREAM-IO WIDTH 400 PAGE-TOP.
  FOR EACH doc-fiscal WHERE (doc-fiscal.cod-estabel      GE tt-param.wcod-estab-i
                          AND doc-fiscal.cod-estabel      LE tt-param.wcod-estab-f)
                          AND  (doc-fiscal.cod-emitente  GE tt-param.wemit-i 
                          AND  doc-fiscal.cod-emitente  LE tt-param.wemit-f)
                          AND  (doc-fiscal.nr-doc-fis    GE tt-param.wnr-docto-i   
                          AND  doc-fiscal.nr-doc-fis    LE tt-param.wnr-docto-f)   
                          AND  (doc-fiscal.nat-operacao  GE tt-param.wnat-i 
                          AND  doc-fiscal.nat-operacao  LE tt-param.wnat-f)     
                          AND  (doc-fiscal.dt-impl   GE tt-param.wdt-trans-i 
                          AND  doc-fiscal.dt-impl   LE tt-param.wdt-trans-f) 
                          NO-LOCK BREAK BY cod-estabel BY dt-emis-doc BY nr-doc-fis: 
ASSIGN wdet = " "
       wvl-dupl-simp  = 0
      wimp-ret      = 0.
/* AND   nr-doc-fis = "0091100" NO-LOCK:            */
/*      DISPLAY  STREAM wrel "Est Doc       Emitente Nat.OP Dt Implant Item                Vl.Merc        Base ICMS    Vl.ICMS         Base IPI     Vl.IPI          Base PIS       Base COFINS     Vl.PIS  Vl.COFINS      B.ICMS Subs Vl.ICMS.SUBS     Vl Total NF   PIS Ret  Cofins Ret    Vl Duplicata" */
/*           WITH FRAME f-cabecst STREAM-IO WIDTH 400 NO-BOX PAGE-TOP.                                                                                                                                                                                                                                    */
/*                                                                                                                                                                                                                                                                                                        */
          run pi-acompanhar in h-acomp ("Documento: " + doc-fiscal.nr-doc-fis). 
          FOR EACH it-doc-fis OF doc-fiscal NO-LOCK:

          ACCUMULATE it-doc-fis.val-base-calc-pis (TOTAL).
          ACCUMULATE it-doc-fis.val-base-calc-cofins (TOTAL).
          ACCUMULATE it-doc-fis.vl-merc-liq (TOTAL).
          ACCUMULATE  it-doc-fis.val-pis  (TOTAL).              
          ACCUMULATE it-doc-fis.val-cofins  (TOTAL).       
         
          DISPLAY   doc-fiscal.cod-estabel      FORMAT "x(03)"
                    doc-fiscal.nr-doc-fis       FORMAT  "X(09)" COLUMN-LABEL "Doc"
                    doc-fiscal.cod-emitente     FORMAT 99999999 COLUMN-LABEL "Emitente"
                    doc-fiscal.nat-operacao     COLUMN-LABEL "Nat.OP"
                    doc-fiscal.dt-impl     FORMAT "99/99/99" 
                    it-doc-fis.it-codigo        
                    it-doc-fis.vl-merc-liq      FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.Merc"
                    it-doc-fis.vl-bicms-it       COLUMN-LABEL "Base ICMS"
                    it-doc-fis.vl-icms-it        FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.ICMS"
                    it-doc-fis.vl-bipi-it        COLUMN-LABEL "Base IPI"
                    it-doc-fis.vl-ipi-it         FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.IPI" 
                    it-doc-fis.val-base-calc-pis COLUMN-LABEL "Base PIS"
                    it-doc-fis.val-base-calc-cofins COLUMN-LABEL "Base COFINS"
                    it-doc-fis.val-pis           FORMAT ">>>,>>9.99"  COLUMN-LABEL "Vl.PIS"
                    it-doc-fis.val-cofins        FORMAT ">>>,>>9.99"  COLUMN-LABEL "Vl.COFINS"
                    it-doc-fis.vl-bsubs-it       COLUMN-LABEL "B.ICMS Subs"
                    it-doc-fis.vl-icmsub-it      FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.ICMS.SUBS"
                    " "                          COLUMN-LABEL      "   Vl Total NF   PIS Ret  Cofins Ret     Vl Duplicata"
                   /* wdet                          COLUMN-LABEL "    Vl Total NF   PIS Ret  Cofins Ret    Vl Duplicata"*/
                WITH FRAME f-relat WIDTH 400 DOWN STREAM-IO.
          ASSIGN wdet = "". 
          DISPLAY STREAM wrel
               doc-fiscal.cod-estabel      FORMAT "x(03)"
                    doc-fiscal.nr-doc-fis       FORMAT  "X(09)" COLUMN-LABEL "Doc"
                    doc-fiscal.cod-emitente     FORMAT 99999999 COLUMN-LABEL "Emitente"
                    doc-fiscal.nat-operacao     COLUMN-LABEL "Nat.OP"
                    doc-fiscal.dt-impl     FORMAT "99/99/99" 
                    it-doc-fis.it-codigo        
                    it-doc-fis.vl-merc-liq       FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.Merc"
                    it-doc-fis.vl-bicms-it       COLUMN-LABEL "Base ICMS"
                    it-doc-fis.vl-icms-it        FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.ICMS"
                    it-doc-fis.vl-bipi-it        COLUMN-LABEL "Base IPI"
                    it-doc-fis.vl-ipi-it         FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.IPI" 
                    it-doc-fis.val-base-calc-pis COLUMN-LABEL "Base PIS"
                    it-doc-fis.val-base-calc-cofins COLUMN-LABEL "Base COFINS"
                    it-doc-fis.val-pis           FORMAT ">>>,>>9.99"  COLUMN-LABEL "Vl.PIS"
                    it-doc-fis.val-cofins        FORMAT ">>>,>>9.99"  COLUMN-LABEL "Vl.COFINS"
                    it-doc-fis.vl-bsubs-it       COLUMN-LABEL "B.ICMS Subs"
                    it-doc-fis.vl-icmsub-it      FORMAT ">>>,>>9.99" COLUMN-LABEL "Vl.ICMS.SUBS"
                    wdet                          COLUMN-LABEL "    Vl Total NF   PIS Ret  Cofins Ret    Vl Duplicata"
                    WITH FRAME f-relat1 WIDTH 400 DOWN STREAM-IO NO-LABELS.
      
      IF FIRST-OF(doc-fiscal.nr-doc-fis) THEN do:
          DOWN WITH FRAME f-relat.
          DOWN WITH FRAME f-relat1.
      END.
      FIND FIRST docum-est WHERE docum-est.serie-docto   = doc-fiscal.serie
                           AND docum-est.nro-docto       = doc-fiscal.nr-doc-fis
                           AND docum-est.cod-emitente    = doc-fiscal.cod-emitente  
                           AND docum-est.nat-operacao    = doc-fiscal.nat-operacao
                           NO-LOCK NO-ERROR.

      IF AVAILABLE docum-est  THEN ASSIGN wdet =  string(docum-est.tot-valor, ">>,>>>,>>9.99").
      /* AQUI */
    
      FIND FIRST dupli-apagar WHERE dupli-apagar.serie-docto    = doc-fiscal.serie
                              AND dupli-apagar.nro-docto     = doc-fiscal.nr-doc-fis
                              AND dupli-apagar.cod-emitente  = doc-fiscal.cod-emitente
                              AND dupli-apagar.nat-operacao  = doc-fiscal.nat-operacao
                              /*AND parcela       = doc-fiscal.parcela*/
                              NO-LOCK NO-ERROR.
        
      /* imposto reten��o */ 
      FOR EACH dupli-imp OF dupli-apagar NO-LOCK BREAK BY dupli-imp.cod-retencao DESCENDING :
           ASSIGN WDET = WDET +  " " + STRING(dupli-imp.vl-imposto,">>>,>>9.99")
                  wimp-ret = wimp-ret + dupli-imp.vl-imposto.
          
/*             DISPLAY " DUPLI"  dupli-apagar.nro-docto   dupli-imp.tp-imposto dupli-imp.tp-codigo dupli-imp.vl-imposto dupli-imp.cod-retencao. */
/*             DOWN WITH FRAME f-relat.     */
                   
            END.
/*                                                            */
/*             DISPLAY  STREAM wrel wdet WITH FRAME f-relat1. */
/*                    DOWN STREAM wrel WITH FRAME f-relat1.   */
/*                    DISPLAY wdet WITH FRAME f-relat.        */
/*                    DOWN WITH FRAME f-relat.                */
           
     
     END.
 
 IF LAST-OF(doc-fiscal.nr-doc-fis) THEN DO:
/*    DISPLAY STREAM wrel wdet WITH FRAME f-relat1. */
/*    DOWN STREAM wrel WITH FRAME f-relat1.         */
   
   DISPLAY         SKIP
                   "TOTAIS: " AT 42
                   "  "
                   (ACCUM TOTAL it-doc-fis.vl-merc-liq)       FORMAT ">>>,>>9.99" AT 59         COLUMN-LABEL "Vl.Merc"
                   doc-fiscal.vl-bicms          FORMAT ">>>,>>9.99" TO 85         COLUMN-LABEL "Base ICMS"
                   doc-fiscal.vl-icms           FORMAT ">>>,>>9.99" TO 96         COLUMN-LABEL "Vl.ICMS"
                   doc-fiscal.vl-bipi           FORMAT ">>>,>>9.99" TO 113        COLUMN-LABEL "Base IPI"
                   doc-fiscal.vl-ipi            FORMAT ">>>,>>9.99" TO 124        COLUMN-LABEL "Vl.IPI" 
                   (ACCUM TOTAL it-doc-fis.val-base-calc-pis) TO 142              COLUMN-LABEL "Base PIS"
                   (ACCUM TOTAL  it-doc-fis.val-base-calc-cofins) TO 160          COLUMN-LABEL "Base COFINS"
                   (ACCUM TOTAL it-doc-fis.val-pis  )            FORMAT ">>>,>>9.99" TO 171        COLUMN-LABEL "Vl.PIS"
                   (ACCUM TOTAL it-doc-fis.val-cofins)           FORMAT ">>>,>>9.99" TO 182        COLUMN-LABEL "Vl.COFINS" 
/*                    doc-fiscal.vl-pis            FORMAT ">>>,>>9.99" TO 171        COLUMN-LABEL "Vl.PIS"    */
/*                    doc-fiscal.vl-finsocial      FORMAT ">>>,>>9.99" TO 182        COLUMN-LABEL "Vl.COFINS" */
                   doc-fiscal.vl-bsubs          FORMAT ">>>,>>9.99"  TO 199       COLUMN-LABEL "B.ICMS Subs"
                   doc-fiscal.vl-icmsub         FORMAT ">>>,>>9.99"  TO 212       COLUMN-LABEL "Vl.ICMS.SUBS"  
                   wdet COLUMN-LABEL "    Vl Total NF   PIS Ret  Cofins Ret    Vl Duplicata" /*dupli-apagar.vl-a-pagar    */
       WITH FRAME F-RELAT2  WIDTH 400 STREAM-IO NO-BOX.

   DISPLAY STREAM wrel  "TOTAIS: " AT 40
                   (ACCUM TOTAL it-doc-fis.vl-merc-liq)       FORMAT ">>>,>>9.99" AT 57         COLUMN-LABEL "Vl.Merc"
                   doc-fiscal.vl-bicms          FORMAT ">>>,>>9.99" TO 83         COLUMN-LABEL "Base ICMS"
                   doc-fiscal.vl-icms           FORMAT ">>>,>>9.99" TO 94         COLUMN-LABEL "Vl.ICMS"
                   doc-fiscal.vl-bipi           FORMAT ">>>,>>9.99" TO 111        COLUMN-LABEL "Base IPI"
                   doc-fiscal.vl-ipi            FORMAT ">>>,>>9.99" TO 122        COLUMN-LABEL "Vl.IPI" 
                   (ACCUM TOTAL it-doc-fis.val-base-calc-pis) TO 140              COLUMN-LABEL "Base PIS"
                   (ACCUM TOTAL  it-doc-fis.val-base-calc-cofins) TO 158          COLUMN-LABEL "Base COFINS"
                   (ACCUM TOTAL it-doc-fis.val-pis  )            FORMAT ">>>,>>9.99" TO 169        COLUMN-LABEL "Vl.PIS"
                   (ACCUM TOTAL it-doc-fis.val-cofins)           FORMAT ">>>,>>9.99" TO 180        COLUMN-LABEL "Vl.COFINS"   
                   doc-fiscal.vl-bsubs          FORMAT ">>>,>>9.99"  TO 197       COLUMN-LABEL "B.ICMS Subs"
                   doc-fiscal.vl-icmsub         FORMAT ">>>,>>9.99"  TO 208       COLUMN-LABEL "Vl.ICMS.SUBS"  
                   wdet /*dupli-apagar.vl-a-pagar    */
       WITH FRAME F-RELAT3 NO-LABELS WIDTH 400 STREAM-IO NO-BOX.


   IF AVAILABLE(dupli-apagar) THEN  DO: 
    ASSIGN wvl-dupl-simp = dupli-apagar.vl-a-pagar - wimp-ret.
/*     MESSAGE dupli-apagar.vl-a-pagar wimp-ret wvl-dupl-simp  VIEW-AS ALERT-BOX. */
/*     DISPLAY dupli-apagar.vl-a-pagar  SKIP(1) WITH FRAME F-RELAT2 NO-LABELS WIDTH 400 STREAM-IO NO-BOX.              */
/*     DISPLAY STREAM wrel  dupli-apagar.vl-a-pagar  SKIP(1) WITH FRAME F-RELAT3 NO-LABELS WIDTH 400 STREAM-IO NO-BOX. */

    DISPLAY wvl-dupl-simp   SKIP(1) WITH FRAME F-RELAT2 NO-LABELS WIDTH 400 STREAM-IO NO-BOX.
    DISPLAY STREAM wrel  wvl-dupl-simp    SKIP(1) WITH FRAME F-RELAT3 NO-LABELS WIDTH 400 STREAM-IO NO-BOX.
    END.
    DOWN WITH FRAME f-relat.
    DOWN STREAM wrel WITH FRAME f-relat1.
    END.

 END.
 
OUTPUT STREAM wrel CLOSE.
END PROCEDURE.


/* ACCUM          */
/*     accumulate */

