/*****************************************************************************************************
* Programa: UPCBODI317PR.P
* Cliente.: Fastplas
* Autor...: Datasul Metropolitana
* Data....: 07/06/2003
* Objetivo: GERACAO DE NOTA FISCAL DE RETORNO DE BENEFICIAMENTO
* Versao Inicial: CARLOS ALBERTO RIBEIRO
* Manuten��o: Mauro Jabur - 27/05/2004 
*             Corre��o de erros na gera��o da nota fiscal de retorno.  
* Altera��o.: Mauro Jabur - 06/07/2004
*               -   Ajustes para valida��o de estabelecimento na chave do SALDO-TERC para sele��o de notas 
*                   a retornar
*               -   Ajustes para gera��o do valor do item da nota fiscal, padronizando a origem do mesmo
*                   para o ITEM-DOC-EST
* Altera��o.: LL - TRIAH - 10/02/2010
*               -   Comentario da SHARED TEMP-TABLE tt-saldo-terc (erro 40 - too many index)
*******************************************************************************************************
* Alteracao: MSM - Triah - 08/05/2013
*            - Criacao de tabela de relacionamento entre nota de retorno e nota de venda
*******************************************************************************************************
* Alteracao: LL - Totvs - 08/09/2015
*            - Projeto Scania - buscar itens da reserva da OP do pedido que esta sendo faturado
*******************************************************************************************************
* Alteracao: FJG - Totvs - 02/10/2015
*            - Projeto Scania - buscar QUAMTIDADE DO itens da reserva da OP do pedido que esta sendo faturado
*******************************************************************************************************/

/***************************************************************
**
** I-EPC200.I1 - Padroniza a temp-table usada para os epcs
**
***************************************************************/ 

/* begin_temp_table_definition */

DEFINE TEMP-TABLE tt-epc NO-UNDO
   FIELD cod-event     AS CHAR FORMAT "x(12)"
   FIELD cod-parameter AS CHAR FORMAT "x(32)"
   FIELD val-parameter AS CHAR FORMAT "x(54)"
   INDEX  id IS PRIMARY cod-parameter cod-event ASCENDING.    
   
/*DEFINE TEMP-TABLE tt-arquivo NO-UNDO
    FIELD it-codigo-nf      AS CHAR
    FIELD it-codigo-comp    AS CHAR
    FIELD it-componente     AS CHAR
    FIELD quantidade        AS DECIMAL.

DEFINE NEW GLOBAL SHARED TEMP-TABLE tt-arquivo-imp-xml NO-UNDO
    FIELD it-codigo-nf      AS CHAR
    FIELD it-codigo-comp    AS CHAR
    FIELD it-componente     AS CHAR
    FIELD quantidade        AS DECIMAL
    FIELD usuario           AS CHAR
    FIELD impresso          AS LOG.*/

DEFINE BUFFER b-wt-it-docto FOR wt-it-docto.
DEFINE BUFFER bf-estrutura  FOR estrutura.
DEFINE BUFFER bfc-emitente  FOR emitente.
/* ***************************  Definitions  **************************** */

DEFINE TEMP-TABLE RowErrors NO-UNDO
    FIELD ErrorSequence     AS INTEGER
    FIELD ErrorNumber       AS INTEGER
    FIELD ErrorDescription  AS CHARACTER
    FIELD ErrorParameters   AS CHARACTER
    FIELD ErrorType         AS CHARACTER
    FIELD ErrorHelp         AS CHARACTER
    FIELD ErrorSubType      AS CHARACTER.

/* _UIB-CODE-BLOCK-END */
/* ********************  Preprocessor Definitions  ******************** */

/* _UIB-PREPROCESSOR-BLOCK-END */

/* *********************** Procedure Settings ************************ */

/* Settings for THIS-PROCEDURE

   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */

/* *************************  Create Window  ************************** */
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2.01
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
/* ***************************  Main Block  *************************** */
/* _UIB-CODE-BLOCK-END */


DEFINE INPUT        PARAMETER p-ind-event  AS CHARACTER  NO-UNDO.
DEFINE INPUT-OUTPUT PARAMETER TABLE FOR tt-epc.

DEFINE VARIABLE l-erro           AS LOGICAL  INIT NO  NO-UNDO.
DEFINE VARIABLE r-rowid-wt-docto AS ROWID             NO-UNDO.
DEFINE VARIABLE c-val-parameter  AS CHAR              NO-UNDO.
DEFINE VARIABLE c-notas-entrada  AS CHAR              NO-UNDO.

DEFINE VARIABLE d-qt-saldo   AS DECIMAL    NO-UNDO.
DEFINE VARIABLE d-qt-terc    AS DECIMAL    NO-UNDO.
DEFINE VARIABLE i-sequencia  AS INTEGER    NO-UNDO.
DEFINE VARIABLE d-qt-aux     AS DECIMAL    NO-UNDO.

DEFINE VARIABLE d-qt-disp    AS DECIMAL    NO-UNDO.

DEF VAR h-bodi317sd          AS HANDLE NO-UNDO.

/*DEF NEW SHARED TEMP-TABLE tt-item
    FIELD it-codigo             LIKE ITEM.it-codigo 
    FIELD nr-ord-ext            LIKE es-item-doc-eSt.nr-ord-ext
    FIELD qt-usada  	           AS DEC FORM "->>>,>>9.9999"
    FIELD preori                AS DEC 
    INDEX codigo IS PRIMARY UNIQUE it-codigo nr-ord-ext. */

/*LL - TRIAH - 10/02/2010
DEF NEW SHARED TEMP-TABLE tt-saldo-terc LIKE saldo-terc
    FIELD qt-usada  as dec form "->>>,>>9.9999"
    FIELD preori    AS DEC.*/

DEF TEMP-TABLE tt-es-it-ordem NO-UNDO
    FIELD char-1       LIKE es-it-ordem.char-1 
    FIELD char-2       LIKE es-it-ordem.char-2 
    FIELD data-1       LIKE es-it-ordem.data-1 
    FIELD data-2       LIKE es-it-ordem.data-2 
    FIELD dec-1        LIKE es-it-ordem.dec-1 
    FIELD dec-2        LIKE es-it-ordem.dec-2 
    FIELD int-1        LIKE es-it-ordem.int-1 
    FIELD int-2        LIKE es-it-ordem.int-2 
    FIELD it-codigo    LIKE es-it-ordem.it-codigo 
    FIELD log-1        LIKE es-it-ordem.log-1 
    FIELD log-2        LIKE es-it-ordem.log-2 
    FIELD nr-ord-ext   LIKE es-it-ordem.nr-ord-ext 
    FIELD preori       LIKE es-it-ordem.preori 
    FIELD qt-usada     LIKE es-it-ordem.qt-usada 
    FIELD usuario      LIKE es-it-ordem.usuario
    FIELD item-cj      AS CHAR
    FIELD seq-wt-it-docto LIKE wt-it-docto.seq-wt-it-docto.

DEFINE BUFFER bf-tw-docto    FOR wt-docto.
DEFINE BUFFER bf-tw-it-docto FOR wt-it-docto.
DEFINE BUFFER b-saldo-terc   FOR saldo-terc.
DEFINE VARIABLE wh-boin404te AS HANDLE NO-UNDO.

DEF NEW GLOBAL SHARED VAR c-seg-usuario AS CHAR NO-UNDO.

DEF BUFFER b-es-it-ordem FOR es-it-ordem.

DEF VAR c-nota-remessa AS CHAR FORMAT "x(160)" INIT "" NO-UNDO.

DEF VAR c-arquivo AS CHAR   NO-UNDO.
DEF VAR h-prog    AS HANDLE NO-UNDO.

/* Defini��o da tabela tempor�ria para itens de terceiros */
{dibo/bodi317sd.i1}

/*-----------------------------------------------------------------------------------*/

/* Definicao da tabela temporaria tt-notas-geradas, include {dibo/bodi317ef.i1} */
DEF TEMP-TABLE tt-notas-geradas NO-UNDO
    FIELD rw-nota-fiscal AS   ROWID
    FIELD nr-nota        LIKE nota-fiscal.nr-nota-fis
    FIELD seq-wt-docto   LIKE wt-docto.seq-wt-docto.

/* Defini��o de um buffer para tt-notas-geradas */
DEF BUFFER b-tt-notas-geradas FOR tt-notas-geradas.

DEF VAR c-nota-pri   AS CHAR NO-UNDO.
DEF VAR c-nota-ult   AS CHAR NO-UNDO.

DEF VAR c-aux AS CHAR NO-UNDO.

def new global shared var gb-upbodi317pr-it-venda as char no-undo.

/*-----------------------------------------------------------------------------------*/

ASSIGN l-erro = NO.

DEFINE STREAM s-saida.

def buffer b-tt-epc for tt-epc.

/* alteracao numero de linhas por nota Mercedes */
find first tt-epc where
     tt-epc.cod-event = "localizaWtDocto" and
     tt-epc.cod-parameter = "Rowid_wt-docto" no-lock no-error.
if avail tt-epc then do:
   find first wt-docto where
        ROWID(wt-docto) = TO-ROWID(tt-epc.val-parameter) no-lock no-error.
   if avail wt-docto then do:

      FIND es-itens-nota WHERE
           es-itens-nota.cod-estabel = wt-docto.cod-estabel AND
           es-itens-nota.cod-emitente = wt-docto.cod-emitente NO-LOCK NO-ERROR.
      IF AVAIL es-itens-nota THEN DO:
          find first b-tt-epc where
               b-tt-epc.cod-event = "localizaWtDocto" and
               b-tt-epc.cod-parameter = "i-nr-linhas-nota" no-lock no-error.
          if avail b-tt-epc then
             b-tt-epc.val-parameter = STRING(es-itens-nota.num-itens).
      END.
    end.
end.

IF p-ind-event = "beforeGeraDuplicatas" THEN 
DO:

   FOR EACH es-it-ordem WHERE
       es-it-ordem.usuario = c-seg-usuario:
       DELETE es-it-ordem. 
   END.

   FOR EACH tt-es-it-ordem:
       DELETE tt-es-it-ordem. 
   END.

   FOR EACH tt-epc
        WHERE tt-epc.cod-parameter = "object-handle"
        OR    tt-epc.cod-parameter = "table-rowid"
        BY    tt-epc.cod-parameter DESCENDING:
        
        IF tt-epc.cod-parameter = "table-rowid" THEN 
        DO:

           c-val-parameter = tt-epc.val-parameter.

           FOR FIRST wt-docto WHERE
               ROWID(wt-docto) = TO-ROWID(tt-epc.val-parameter) NO-LOCK.
           
           IF AVAIL wt-docto THEN 
           DO:
                            
              for each wt-it-docto of wt-docto
                  where wt-it-docto.selecionado = "*" no-lock:
                 if lookup(wt-it-docto.it-codigo,gb-upbodi317pr-it-venda) = 0 then
                    gb-upbodi317pr-it-venda = gb-upbodi317pr-it-venda + wt-it-docto.it-codigo + ",".              
              end.              

              FIND es-natur-oper NO-LOCK OF wt-docto NO-ERROR.

              IF AVAIL es-natur-oper        AND
                 es-natur-oper.gera-retorno THEN
              DO:
                 FIND natur-oper
                     WHERE natur-oper.nat-operacao = es-natur-oper.nat-oper-ret
                     NO-LOCK NO-ERROR.
                 IF  NOT AVAIL natur-oper THEN
                     UNDO, LEAVE.
                 ELSE DO:
                     IF  natur-oper.terceiros = NO THEN
                         UNDO, LEAVE.
                     IF  natur-oper.tp-oper-terc <> 2 THEN
                         UNDO, LEAVE.
                 END.
                 IF es-natur-oper.nat-oper-ret = "" THEN
                 DO:
                    MESSAGE "Natureza de servico nao possui natureza"   
                            "de retorno de beneficiamento cadastrada."  SKIP
                            "Favor abandonar implantacao !" 
                            VIEW-AS ALERT-BOX ERROR. 
                    ASSIGN l-erro = YES.
                    UNDO, LEAVE.
                 END.
                                  
                 item-nota-fiscal:
                 FOR EACH wt-it-docto NO-LOCK 
                    WHERE wt-it-docto.seq-wt-docto = wt-docto.seq-wt-docto:

                    
                    IF  wt-it-docto.nr-pedcli   <> ""  AND
                        wt-it-docto.selecionado <> "*" THEN
                        NEXT item-nota-fiscal.
                        
                    FIND item-cli WHERE 
                         item-cli.it-codigo  = wt-it-docto.it-codigo AND
                         item-cli.nome-abrev = wt-docto.nome-abrev
                         NO-LOCK NO-ERROR. 
                    IF  NOT AVAIL item-cli THEN
                        NEXT.
                                   
                    FIND es-item-cli NO-LOCK WHERE 
                         es-item-cli.it-codigo  = wt-it-docto.it-codigo AND
                         es-item-cli.nome-abrev = item-cli.nome-abrev 
                         NO-ERROR.          
          
                    IF NOT AVAIL es-item-cli OR 
                       es-item-cli.nr-ord-produ = 0 THEN 
                    DO:         
                       MESSAGE "Item....: " wt-it-docto.it-codigo                             SKIP
                               "Confira se h� ordem de producao cadastrada para item/cliente" SKIP
                               "Nota nao podera ser processada."                              SKIP
                               "Favor abandonar implantacao !"    
                               VIEW-AS ALERT-BOX ERROR. 
                          
                       ASSIGN l-erro = YES.               
                       UNDO, LEAVE.
                    END.

                    /* Projeto Scania - inicio */
                    find first mgfas.ext-ped-item where 
                        ext-ped-item.nome-abrev = wt-docto.nome-abrev   and 
                        ext-ped-item.nr-pedcli  = wt-it-docto.nr-pedcli no-lock no-error.
                    if avail mgfas.ext-ped-item then do:
                       FOR FIRST ord-prod USE-INDEX cliente-ped WHERE
                           ord-prod.nome-abrev   = wt-docto.nome-abrev    AND
                           ord-prod.nr-pedido    = wt-it-docto.nr-pedcli  AND
                           ord-prod.nr-sequencia = wt-it-docto.nr-seq-ped NO-LOCK. END.
                       IF AVAIL ord-prod THEN DO:
                          FOR EACH reservas OF ord-prod NO-LOCK.
                              FOR EACH estrutura WHERE 
                                  estrutura.it-codigo     = reserva.it-codigo AND 
                                  estrutura.data-inicio  <= TODAY             AND 
                                  estrutura.data-termino >= TODAY             NO-LOCK: 

                                  /* Inicio - Lauriano 
                                  FIND FIRST bfc-emitente
                                       WHERE bfc-emitente.nome-abrev = wt-docto.nome-abrev
                                       NO-LOCK NO-ERROR.
                                  IF AVAIL bfc-emitente THEN DO.
                                      IF bfc-emitente.cod-emitente = 506 OR bfc-emitente.cod-emitente = 2261 THEN DO.
                                          FIND FIRST bf-estrutura
                                               WHERE bf-estrutura.it-codigo = estrutura.es-codigo
                                               NO-LOCK NO-ERROR.


                                          IF AVAIL bf-estrutura THEN DO.
                                              IF SUBSTRING(estrutura.es-codigo,11,3) <> "000" THEN DO.
                                                  CREATE tt-arquivo.
                                                  ASSIGN tt-arquivo.it-codigo-nf      = wt-it-docto.it-codigo
                                                         tt-arquivo.it-codigo-comp    = estrutura.es-codigo
                                                         tt-arquivo.quantidade        = wt-it-docto.quantidade[1]
                                                         tt-arquivo.it-componente     = bf-estrutura.es-codigo.

                                                  CREATE tt-arquivo-imp-xml.
                                                  ASSIGN tt-arquivo-imp-xml.it-codigo-nf      = wt-it-docto.it-codigo
                                                         tt-arquivo-imp-xml.it-codigo-comp    = estrutura.es-codigo
                                                         tt-arquivo-imp-xml.quantidade        = wt-it-docto.quantidade[1]
                                                         tt-arquivo-imp-xml.it-componente     = bf-estrutura.es-codigo
                                                         tt-arquivo-imp-xml.usuario           = c-seg-usuario
                                                         tt-arquivo-imp-xml.impresso          = NO. 

                                              END.
                                              ELSE DO.

                                                  CREATE tt-arquivo.
                                                  ASSIGN tt-arquivo.it-codigo-nf      = wt-it-docto.it-codigo
                                                         tt-arquivo.it-codigo-comp    = estrutura.es-codigo
                                                         tt-arquivo.quantidade        = wt-it-docto.quantidade[1]
                                                         tt-arquivo.it-componente     = bf-estrutura.es-codigo.


                                                 CREATE tt-arquivo-imp-xml.
                                                 ASSIGN tt-arquivo-imp-xml.it-codigo-nf      = wt-it-docto.it-codigo
                                                        tt-arquivo-imp-xml.it-codigo-comp    = estrutura.es-codigo
                                                        tt-arquivo-imp-xml.quantidade        = wt-it-docto.quantidade[1]
                                                        tt-arquivo-imp-xml.it-componente     = bf-estrutura.es-codigo
                                                        tt-arquivo-imp-xml.usuario           = c-seg-usuario
                                                        tt-arquivo-imp-xml.impresso          = NO.
                                              END.



                                          END.
                                      END.
                                  END.
                                  /* fim - Lauriano */*/

                                  FOR EACH es-it-ordem WHERE
                                      es-it-ordem.usuario = c-seg-usuario:
                                      DELETE es-it-ordem. 
                                  END.

                                  /*FOR EACH tt-es-it-ordem WHERE 
                                      tt-es-it-ordem.seq-wt-it-docto = wt-it-docto.seq-wt-it-docto.
                                      DELETE tt-es-it-ordem.
                                  END.*/

                                  RUN esupc/upcft001.p (INPUT estrutura.es-codigo,
                                                        INPUT es-item-cli.nr-ord-produ,
                                                        INPUT estrutura.quant-usada,
                                                        INPUT reservas.quant-orig /*wt-it-docto.quantidade[1]*/).

                                  FOR EACH es-it-ordem WHERE
                                      es-it-ordem.usuario = c-seg-usuario:
                                      CREATE tt-es-it-ordem.
                                      BUFFER-COPY es-it-ordem TO tt-es-it-ordem.
                                      ASSIGN tt-es-it-ordem.item-cj         = wt-it-docto.it-codigo
                                             tt-es-it-ordem.seq-wt-it-docto = wt-it-docto.seq-wt-it-docto.
                                      DELETE es-it-ordem. 
                                  END.

                              END.
                          END.
                       END.
                    END. /* Projeto Scania - fim */
                    ELSE DO:
                       FOR EACH estrutura
                          WHERE estrutura.it-codigo     = wt-it-docto.it-codigo
                            AND estrutura.data-inicio  <= TODAY
                            AND estrutura.data-termino >= TODAY
                            NO-LOCK: 
        

                           /* Inicio - Lauriano 
                           FIND FIRST bfc-emitente
                                WHERE bfc-emitente.nome-abrev = wt-docto.nome-abrev
                                NO-LOCK NO-ERROR.
                           IF AVAIL bfc-emitente THEN DO.
                               IF bfc-emitente.cod-emitente = 506 OR bfc-emitente.cod-emitente = 2261 THEN DO.
                                   FIND FIRST bf-estrutura
                                        WHERE bf-estrutura.it-codigo = estrutura.es-codigo
                                        NO-LOCK NO-ERROR.
                                   IF AVAIL bf-estrutura THEN DO.
                                       IF SUBSTRING(estrutura.es-codigo,11,3) <> "000" THEN DO.
                                           CREATE tt-arquivo.
                                           ASSIGN tt-arquivo.it-codigo-nf      = wt-it-docto.it-codigo
                                                  tt-arquivo.it-codigo-comp    = estrutura.es-codigo
                                                  tt-arquivo.quantidade        = wt-it-docto.quantidade[1]
                                                  tt-arquivo.it-componente     = bf-estrutura.es-codigo.


                                          CREATE tt-arquivo-imp-xml.
                                          ASSIGN tt-arquivo-imp-xml.it-codigo-nf      = wt-it-docto.it-codigo
                                                 tt-arquivo-imp-xml.it-codigo-comp    = estrutura.es-codigo
                                                 tt-arquivo-imp-xml.quantidade        = wt-it-docto.quantidade[1]
                                                 tt-arquivo-imp-xml.it-componente     = bf-estrutura.es-codigo
                                                 tt-arquivo-imp-xml.usuario           = c-seg-usuario
                                                 tt-arquivo-imp-xml.impresso          = NO.

                                       END.
                                       ELSE DO.
                                           
                                           CREATE tt-arquivo.
                                           ASSIGN tt-arquivo.it-codigo-nf      = wt-it-docto.it-codigo
                                                  tt-arquivo.it-codigo-comp    = estrutura.es-codigo
                                                  tt-arquivo.quantidade        = wt-it-docto.quantidade[1]
                                                  tt-arquivo.it-componente     = bf-estrutura.es-codigo.


                                          CREATE tt-arquivo-imp-xml.
                                          ASSIGN tt-arquivo-imp-xml.it-codigo-nf      = wt-it-docto.it-codigo
                                                 tt-arquivo-imp-xml.it-codigo-comp    = estrutura.es-codigo
                                                 tt-arquivo-imp-xml.quantidade        = wt-it-docto.quantidade[1]
                                                 tt-arquivo-imp-xml.it-componente     = bf-estrutura.es-codigo
                                                 tt-arquivo-imp-xml.usuario           = c-seg-usuario
                                                 tt-arquivo-imp-xml.impresso          = NO.
                                       END.
                                   END.
                               END.
                           END.

                           /* fim - Lauriano */ */

                           FOR EACH es-it-ordem WHERE
                               es-it-ordem.usuario = c-seg-usuario:
                               DELETE es-it-ordem. 
                           END.

                           /*FOR EACH tt-es-it-ordem WHERE 
                               tt-es-it-ordem.seq-wt-it-docto = wt-it-docto.seq-wt-it-docto.
                               DELETE tt-es-it-ordem.
                           END.*/

                            RUN esupc/upcft001.p (INPUT estrutura.es-codigo,
                                                  INPUT es-item-cli.nr-ord-produ,
                                                  INPUT estrutura.quant-usada,
                                                  INPUT wt-it-docto.quantidade[1]).

                            FOR EACH es-it-ordem WHERE
                                es-it-ordem.usuario = c-seg-usuario:
                                CREATE tt-es-it-ordem.
                                BUFFER-COPY es-it-ordem TO tt-es-it-ordem.
                                ASSIGN tt-es-it-ordem.item-cj         = wt-it-docto.it-codigo
                                       tt-es-it-ordem.seq-wt-it-docto = wt-it-docto.seq-wt-it-docto.
                                DELETE es-it-ordem. 
                            END.

                       END.
                    END.
                 END.
              END.
           END.  /*IF AVAIL wt-docto THEN */
           END.
        END. /* IF tt-epc.cod-parameter = "table-rowid" then  */
  END.
  
  FIND FIRST tt-es-it-ordem WHERE 
       tt-es-it-ordem.usuario = c-seg-usuario NO-LOCK NO-ERROR.
  
  /*MESSAGE 'AVAIL tt-es-it-ordem' AVAIL tt-es-it-ordem SKIP 'l-erro' l-erro
      VIEW-AS ALERT-BOX INFO BUTTONS OK.*/

  IF AVAIL tt-es-it-ordem THEN
  DO:
  
     IF l-erro = NO THEN

        RUN pi-ver-saldo.
                                                         
     /*MESSAGE 'l-erro' l-erro
         VIEW-AS ALERT-BOX INFO BUTTONS OK.*/
     IF l-erro = NO THEN
     
        RUN pi-cria-nota-retorno.
        
  END.

END.
      

/*--------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/


PROCEDURE pi-ver-saldo:

 FOR EACH tt-epc
    WHERE tt-epc.cod-parameter = "object-handle"
       OR tt-epc.cod-parameter = "table-rowid"
       BY tt-epc.cod-parameter DESCENDING:
     
  IF tt-epc.cod-parameter = "table-rowid" THEN 
  DO:

     FOR EACH tt-es-it-ordem WHERE 
         tt-es-it-ordem.usuario = c-seg-usuario 
         BREAK BY tt-es-it-ordem.seq-wt-it-docto:
/* Comentado Lucio Zamarioli - 03/08/2021 
         IF FIRST-OF(tt-es-it-ordem.seq-wt-it-docto) THEN DO: //pra corrigir repeti��o
*/
            FIND FIRST para-fat NO-LOCK NO-ERROR.                    
            
            ASSIGN d-qt-terc = 0. 
    
            FOR EACH saldo-terc NO-LOCK WHERE 
                     saldo-terc.it-codigo           = tt-es-it-ordem.it-codigo 
                AND  saldo-terc.cod-emitente        = wt-docto.cod-emitente,
                EACH es-item-doc-est NO-LOCK WHERE
                     es-item-doc-est.serie-docto    = saldo-terc.serie-docto
                AND  es-item-doc-est.nro-docto      = saldo-terc.nro-docto
                AND  es-item-doc-est.cod-emitente   = saldo-terc.cod-emitente
                AND  es-item-doc-est.nat-operacao   = saldo-terc.nat-operacao
                AND  es-item-doc-est.sequencia      = saldo-terc.sequencia
                AND  es-item-doc-est.nr-ord-ext     = tt-es-it-ordem.nr-ord-ext:
                  
                IF saldo-terc.nat-operacao BEGINS "1" OR 
                   saldo-terc.nat-operacao BEGINS "2" OR 
    
                   saldo-terc.nat-operacao BEGINS "3" THEN 
                DO:
    
                   FIND  item-doc-est NO-LOCK WHERE
                         item-doc-est.serie-docto  = saldo-terc.serie        AND
                         item-doc-est.nro-docto    = saldo-terc.nro-docto    AND 
                         item-doc-est.cod-emitente = saldo-terc.cod-emitent  AND 
    
                         item-doc-est.nat-operacao = saldo-terc.nat-operacao AND 
                         item-doc-est.sequencia    = saldo-terc.sequencia     NO-ERROR. 
                   
                   IF NOT AVAIL item-doc-est  THEN NEXT.      
                   
                    ASSIGN d-qt-terc = d-qt-terc + (saldo-terc.quantidade - saldo-terc.dec-1)
                           tt-es-it-ordem.preori = item-doc-est.preco-unit[1].  
                
                END.
            END.
    
            IF tt-es-it-ordem.qt-usada > d-qt-terc AND 
               l-erro = NO                  THEN 
            DO:
    
    
               MESSAGE "Item: " tt-es-it-ordem.it-codigo                                     SKIP
                       "Possui saldo em aberto para retorno igual a " d-qt-terc       SKIP
                       "Quantidade necessaria a ser retornada:" tt-es-it-ordem.qt-usada      SKIP
                       "Confira se h� ordem de producao cadastrada para item/cliente" SKIP
                       "Nota nao podera ser processada."                              SKIP
                       "Favor abandonar implantacao !"    
                       VIEW-AS ALERT-BOX ERROR. 
               
               ASSIGN l-erro = YES.
               UNDO, LEAVE.
            END.
        /* Comentado Lucio Zamarioli - 03/08/2021    
         END.
         */
    END.          
    
    IF l-erro = YES THEN LEAVE.

    /*FOR EACH saldo-terc NO-LOCK WHERE 
             saldo-terc.it-codigo    = tt-es-it-ordem.it-codigo    
        AND  saldo-terc.cod-emitente = wt-docto.cod-emitente 
        AND  saldo-terc.quantidade - saldo-terc.dec-1 > 0,
        EACH es-item-doc-est NO-LOCK WHERE
             es-item-doc-est.serie-docto    = saldo-terc.serie-docto
        AND  es-item-doc-est.nro-docto      = saldo-terc.nro-docto
        AND  es-item-doc-est.cod-emitente   = saldo-terc.cod-emitente
        AND  es-item-doc-est.nat-operacao   = saldo-terc.nat-operacao
        AND  es-item-doc-est.sequencia      = saldo-terc.sequencia
        AND  es-item-doc-est.nr-ord-ext     = tt-es-it-ordem.nr-ord-ext:

        IF NOT saldo-terc.nat-operacao BEGINS "1" AND 
           NOT saldo-terc.nat-operacao BEGINS "2" AND 
           NOT saldo-terc.nat-operacao BEGINS "3" THEN NEXT.

        FIND es-item-cli NO-LOCK WHERE 
             es-item-cli.it-codigo  = tt-es-it-ordem.it-codigo   AND
             es-item-cli.nome-abrev = item-cli.nome-abrev 
             NO-ERROR.                   
        IF NOT AVAIL es-item-cli THEN NEXT.
        
        FIND  item-doc-est WHERE
              item-doc-est.serie-docto  = saldo-terc.serie        AND
              item-doc-est.nro-docto    = saldo-terc.nro-docto    AND 
              item-doc-est.cod-emitente = saldo-terc.cod-emitent  AND 
              item-doc-est.nat-operacao = saldo-terc.nat-operacao AND 
              item-doc-est.sequencia    = saldo-terc.sequencia NO-LOCK NO-ERROR. 

        IF NOT AVAIL item-doc-est  THEN NEXT.
        
        ASSIGN d-qt-saldo = saldo-terc.quantidade - saldo-terc.dec-1. 
        
        IF tt-es-it-ordem.qt-usada <> 0  THEN 
        DO:       
            ASSIGN i-sequencia = i-sequencia + 10.
            IF para-fat.nr-item-nota < i-sequencia / 10 THEN 
            DO:
                MESSAGE "Quantidade de itens na nota de retorno sera" SKIP
                        "Maior do que o permitido. Refazer a nota de faturamento."
                        "Favor abandonar implantacao da nota."
                        VIEW-AS ALERT-BOX ERROR.
                ASSIGN l-erro = YES.
                        
                UNDO, LEAVE.
            END.
        END.
    END.*/
  END.
 END.
END PROCEDURE.


/*-------------------------------------------------------------------------------------------------*/


PROCEDURE pi-cria-nota-retorno:
     
    /* Defini��o da vari�veis */
    DEF VAR wh-bodi317pr                  AS HANDLE NO-UNDO.
    DEF VAR wh-bodi317sd                  AS HANDLE NO-UNDO.
    DEF VAR wh-bodi317im1bra              AS HANDLE NO-UNDO.
    DEF VAR wh-bodi317va                  AS HANDLE NO-UNDO.
    DEF VAR wh-bodi317in                  AS HANDLE NO-UNDO.
    DEF VAR wh-bodi317ef                  AS HANDLE NO-UNDO.

    DEF VAR l-proc-ok-aux                 AS LOG    NO-UNDO.
    DEF VAR c-ultimo-metodo-exec          AS CHAR   NO-UNDO.
    DEF VAR c-cod-estabel                 AS CHAR   NO-UNDO.
    DEF VAR c-serie                       AS CHAR   NO-UNDO.
    DEF VAR da-dt-emis-nota               AS DATE   NO-UNDO.
    DEF VAR da-dt-base-dup                AS DATE   NO-UNDO.
    DEF VAR da-dt-prvenc                  AS DATE   NO-UNDO.
    DEF VAR c-seg-usuario                 AS CHAR   NO-UNDO.
    DEF VAR c-nome-abrev                  AS CHAR   NO-UNDO.   
    DEF VAR c-nr-pedcli                   AS CHAR   NO-UNDO.
    DEF VAR c-nat-operacao                AS CHAR   NO-UNDO.
    DEF VAR c-cod-canal-venda             AS CHAR   NO-UNDO.
    DEF VAR i-seq-wt-docto                AS INT    NO-UNDO.
    DEF VAR i-seq-wt-it-docto             AS INT    NO-UNDO.
    DEF VAR i-seq-wt-it-docto-fim         AS INT    NO-UNDO.
    DEF VAR i-cont-itens                  AS INT    NO-UNDO.
    //DEF VAR c-it-codigo                   AS CHAR   NO-UNDO.
    //DEF VAR c-cod-refer                   AS CHAR   NO-UNDO.
    //DEF VAR de-quantidade                 AS DEC    NO-UNDO.
    DEF VAR de-vl-preori-ped              AS DEC    NO-UNDO.
    DEF VAR de-val-pct-desconto-tab-preco AS DEC    NO-UNDO.
    DEF VAR de-per-des-item               AS DEC    NO-UNDO.
    DEF VAR de-perc-quantidade            AS DEC    NO-UNDO.
    
    FOR EACH tt-epc
       WHERE tt-epc.cod-parameter = "object-handle"
          OR tt-epc.cod-parameter = "table-rowid"
          BY tt-epc.cod-parameter DESCENDING:       
       
       FIND FIRST wt-docto WHERE
            ROWID(wt-docto) = TO-ROWID(tt-epc.val-parameter) NO-LOCK.
            
       IF AVAIL wt-docto THEN LEAVE.       
       
    END.    
    
    FIND es-natur-oper NO-LOCK WHERE
         es-natur-oper.nat-operacao = wt-docto.nat-operacao NO-ERROR.         

    /* Informa��es do embarque para c�lculo */

    ASSIGN c-seg-usuario     = USERID("mgadm") /*"super" /* Usu�rio                    */*/
           c-cod-estabel     = wt-docto.cod-estabel     /* Estabelecimento do pedido  */
           c-serie           = wt-docto.serie           /* S�rie das notas            */
           c-nome-abrev      = wt-docto.nome-abrev      /* Nome abreviado do cliente  */
           c-nr-pedcli       = wt-docto.nr-pedcli       /* Nr pedido do cliente       */
           da-dt-emis-nota   = wt-docto.dt-emis-nota    /* Data de emiss�o da nota    */
           c-nat-operacao    = IF AVAIL es-natur-oper THEN es-natur-oper.nat-oper-ret ELSE '' /* Quando � ? busca do pedido */

           c-cod-canal-venda = STRING(wt-docto.cod-canal-venda) .  /* Quando � ? busca do pedido */
                                                                 
                                                              

    /* Inicializa��o das BOS para C�lculo */
    RUN dibo/bodi317in.p PERSISTENT SET wh-bodi317in.
    RUN INBO/BOIN404TE.P PERSISTENT SET wh-boin404te.
    RUN inicializaBOS IN wh-bodi317in(OUTPUT wh-bodi317pr,
                                      OUTPUT wh-bodi317sd,     
                                      OUTPUT wh-bodi317im1bra,
                                      OUTPUT wh-bodi317va).


    /* In�cio da transa��o */
    REPEAT trans:
    
        /* Limpar a tabela de erros em todas as BOS */
        RUN emptyRowErrors IN wh-bodi317in.
        
        RUN criaWtDocto IN wh-bodi317sd
                (INPUT  c-seg-usuario,
                 INPUT  c-cod-estabel,

                 INPUT  c-serie,
                 INPUT  "1",
                 INPUT  c-nome-abrev,
                 INPUT  ?,
                 INPUT  4,
                 INPUT  9999,
                 INPUT  da-dt-emis-nota,
                 INPUT  0,
                 INPUT  c-nat-operacao,
                 INPUT  c-cod-canal-venda,
                 OUTPUT i-seq-wt-docto,
                 OUTPUT l-proc-ok-aux).

        FOR EACH tt-it-terc-nf exclusive:
            DELETE tt-it-terc-nf.
        END.
                     
        /* Busca poss�veis erros que ocorreram nas valida��es */
        RUN devolveErrosbodi317sd IN wh-bodi317sd(OUTPUT c-ultimo-metodo-exec,
                                                  OUTPUT table RowErrors).

        /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
        FIND FIRST RowErrors NO-LOCK NO-ERROR.

        /* Caso tenha achado algum erro ou advert�ncia, mostra em tela */
        IF  AVAIL RowErrors THEN
            FOR EACH RowErrors:
                IF errorsubtype = "ERROR" THEN
                DO:
                   MESSAGE "A - NOTA DE RETORNO" SKIP(1)
                           wt-docto.nat-operacao SKIP
                           c-nat-operacao SKIP
                           errordescription      SKIP
                           errorhelp             SKIP
                           "Favor abandonar implantacao !" 
                           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                   l-proc-ok-aux = NO.
                   UNDO, LEAVE.

                END.
                ELSE
                   MESSAGE "NOTA DE RETORNO" SKIP(1)
                           errordescription
                           VIEW-AS ALERT-BOX INFO BUTTONS OK.
                           
            END.

        /* Caso ocorreu problema nas valida��es, n�o continua o processo */
        IF  NOT l-proc-ok-aux THEN
            UNDO, LEAVE.               


        /* Limpar a tabela de erros em todas as BOS */
        RUN emptyRowErrors        IN wh-bodi317in.

        /* Disponibilizar o registro WT-DOCTO na bodi317sd */
        RUN localizaWtDocto IN wh-bodi317sd(INPUT  i-seq-wt-docto,
                                            OUTPUT l-proc-ok-aux). 

/*---------------------------------------------------------------------------------------------------------*/

        ASSIGN d-qt-terc = 0
               d-qt-aux  = 0
               d-qt-disp = 0.
               
        ASSIGN c-arquivo = SESSION:TEMP-DIR + c-seg-usuario + REPLACE(STRING(TODAY),'/','') + REPLACE(STRING(TIME,'HH:MM:SS'),':','') + '.txt'.
        OUTPUT TO VALUE(c-arquivo) NO-CONVERT.
      
        PUT UNFORMAT
            'Seq;Item;OP;Qtd;Valor' SKIP.

        FOR EACH tt-es-it-ordem WHERE 
            tt-es-it-ordem.usuario = c-seg-usuario
            BREAK BY tt-es-it-ordem.seq-wt-it-docto:

            IF FIRST-OF(tt-es-it-ordem.seq-wt-it-docto) THEN DO: //pra corrigir repeti��o
               PUT UNFORMAT
                   tt-es-it-ordem.seq-wt-it-docto ';'
                   tt-es-it-ordem.it-codigo       ';'
                   tt-es-it-ordem.nr-ord-ext      ';'
                   tt-es-it-ordem.qt-usada        ';'
                   tt-es-it-ordem.preori          ';'
                   SKIP.
            END.
        END.

        PUT UNFORMAT
            SKIP(1)
            'sequencia;it-codigo;cod-refer;desc-nar;quantidade;qt-disponivel-inf;qt-alocada;qt-disponivel;preco-total-inf;preco-total;selecionado;documento' SKIP.

        /*PUT UNFORMAT 
            'Seq'        AT 1
            'Item'       AT 20
            'Quantidade' AT 40 
            'Documento'  AT 60 SKIP.*/

        FOR EACH tt-es-it-ordem WHERE 
            tt-es-it-ordem.usuario = c-seg-usuario
            BREAK BY tt-es-it-ordem.seq-wt-it-docto:
        /* Comentado Lucio Zamarioli 03/08/2021 
            IF FIRST-OF(tt-es-it-ordem.seq-wt-it-docto) THEN DO: //pra corrigir repeti��o
        */        
                FOR EACH tt-it-terc-nf. DELETE tt-it-terc-nf. END.

                ASSIGN d-qt-terc = 0
                       d-qt-aux  = tt-es-it-ordem.qt-usada.
                        
                FOR EACH    saldo-terc NO-LOCK USE-INDEX item-emit 
                    WHERE   saldo-terc.cod-emitente = wt-docto.cod-emitente 
                    AND     saldo-terc.it-codigo    = tt-es-it-ordem.it-codigo
                    AND     saldo-terc.quantidade - saldo-terc.dec-1 > 0,
                    EACH    es-item-doc-est NO-LOCK WHERE
                            es-item-doc-est.serie-docto    = saldo-terc.serie-docto
                    AND     es-item-doc-est.nro-docto      = saldo-terc.nro-docto
                    AND     es-item-doc-est.cod-emitente   = saldo-terc.cod-emitente
                    AND     es-item-doc-est.nat-operacao   = saldo-terc.nat-operacao
                    AND     es-item-doc-est.sequencia      = saldo-terc.sequencia
                    AND     es-item-doc-est.nr-ord-ext     = tt-es-it-ordem.nr-ord-ext
                    BREAK BY saldo-terc.it-codigo:
                    /* Lucio Zamarioli 03/08/2021 
                    find first b-wt-it-docto 
                        where b-wt-it-docto.seq-wt-docto = i-seq-wt-docto
                        and   b-wt-it-docto.nat-comp     = saldo-terc.nat-operacao
                        and   b-wt-it-docto.nro-comp     = saldo-terc.nro-docto
                        and   b-wt-it-docto.seq-comp     = saldo-terc.sequencia
                        and   b-wt-it-docto.serie-comp   = saldo-terc.serie-docto NO-LOCK NO-ERROR.
                    IF AVAIL b-wt-it-docto THEN NEXT.
                    */

                    FIND ITEM
                        WHERE ITEM.it-codigo = saldo-terc.it-codigo
                        NO-LOCK NO-ERROR.
                    
                    ASSIGN //c-it-codigo                   = tt-es-it-ordem.it-codigo   /* C�digo do item     */
                           //c-cod-refer                   = ""                      /* Refer�ncia do item */
                           //de-quantidade                 = tt-es-it-ordem.qt-usada    /* Quantidade         */
                           //de-vl-preori-ped              = tt-es-it-ordem.preori      /* Pre�o unit�rio     */
                           de-val-pct-desconto-tab-preco = 0                       /* Desconto de tabela */
                           de-per-des-item               = 0                   /* Desconto do item   */
                           de-perc-quantidade            = 0.
                    
                    IF saldo-terc.nat-operacao BEGINS "1" OR 
                       saldo-terc.nat-operacao BEGINS "2" OR 
                       saldo-terc.nat-operacao BEGINS "3" THEN 
                    DO:              
                       
                        FIND item-cli WHERE 
                             item-cli.it-codigo  = saldo-terc.it-codigo AND
                             item-cli.nome-abrev = wt-docto.nome-abrev
                             NO-LOCK NO-ERROR. 
                                  
                        FIND es-item-cli NO-LOCK WHERE 
                            es-item-cli.it-codigo  = saldo-terc.it-codigo   AND
                            es-item-cli.nome-abrev = item-cli.nome-abrev 
                            NO-ERROR.                   
                       
                       FIND  item-doc-est NO-LOCK WHERE
                             item-doc-est.serie-docto  = saldo-terc.serie        AND
                             item-doc-est.nro-docto    = saldo-terc.nro-docto    AND 
                             item-doc-est.cod-emitente = saldo-terc.cod-emitent  AND 
                             item-doc-est.nat-operacao = saldo-terc.nat-operacao AND 
                             item-doc-est.sequencia    = saldo-terc.sequencia     NO-ERROR. 
                    
                    
    
                       IF  NOT AVAIL item-doc-est THEN NEXT. 
                       
                        ASSIGN d-qt-disp = saldo-terc.quantidade - saldo-terc.dec-1. 
                        
                       IF  d-qt-disp >= d-qt-aux THEN DO:
                       
                           RUN pi-cria-tt-it-terc-nf           (INPUT  i-seq-wt-docto, 
    
                                                                INPUT  d-qt-aux,
                                                                INPUT  tt-es-it-ordem.preori,
                                                                INPUT  0, 
                                                                INPUT  0, 
                                                                INPUT ROWID (saldo-terc),
                                                                OUTPUT TABLE tt-it-terc-nf,
                                                                OUTPUT l-proc-ok-aux).
                           
                           ASSIGN d-qt-aux = 0.
                           
                       END.
                       ELSE DO:
                           
                     
                           RUN pi-cria-tt-it-terc-nf           (INPUT  i-seq-wt-docto, 
                                                                INPUT  d-qt-disp,
                                                                INPUT  tt-es-it-ordem.preori,
                                                                INPUT  0, 
                                                                INPUT  0, 
                                                                INPUT ROWID (saldo-terc),
                                                                OUTPUT TABLE tt-it-terc-nf,
                                                                OUTPUT l-proc-ok-aux).
                                                                
                              ASSIGN d-qt-aux = d-qt-aux - d-qt-disp.
        
        
    
                       END.
                       
                     END.
                     
    
                    IF  d-qt-aux = 0 THEN LEAVE.
    
        
                    RUN devolveErrosbodi317sd IN wh-bodi317sd(OUTPUT c-ultimo-metodo-exec,
                                                              OUTPUT table RowErrors).
                                                  
                    FIND FIRST RowErrors NO-LOCK NO-ERROR.
                    /* Caso tenha achado algum erro ou advert�ncia, mostra em tela */
                    IF  AVAIL RowErrors THEN
                        FOR EACH RowErrors:
                            IF errorsubtype = "ERROR" THEN
                            DO:
                            
                               MESSAGE "B - NOTA DE RETORNO" SKIP(1)
                                       errordescription  SKIP                                           
                                       errorhelp         SKIP
                                       "Favor abandonar implantacao !" 
                                       VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                               l-proc-ok-aux = NO.
                               LEAVE.
                            END.
                            ELSE
                               MESSAGE "NOTA DE RETORNO" SKIP(1)
                                       errordescription
                                       VIEW-AS ALERT-BOX INFO BUTTONS OK.
                        END.
                     
    
                    /* Caso ocorreu problema nas valida��es, n�o continua o processo */
                    
                    IF  NOT l-proc-ok-aux THEN
                        UNDO, LEAVE.
    
                END.    /*** FOR EACH SALDO-TERC ***/                      

                IF d-qt-aux > 0 THEN
                    PUT UNFORMAT
                        SKIP(2)
                        '****Item: ' tt-es-it-ordem.it-codigo ' CJ: ' tt-es-it-ordem.item-cj ' Quantidade: ' STRING(d-qt-aux) ' nao relacionado na conta de retorno.' SKIP(2).
                       
                /* Limpar a tabela de erros em todas as BOS */
                RUN emptyRowErrors        IN wh-bodi317in.


               /* Disponibilizar o registro WT-DOCTO na bodi317sd */
               RUN localizaWtDocto IN wh-bodi317sd(INPUT  i-seq-wt-docto,
                                                   OUTPUT l-proc-ok-aux). 

               RUN setaHandleBoin404te IN wh-bodi317sd (INPUT wh-boin404te).


               FOR LAST wt-it-docto WHERE
                   wt-it-docto.seq-wt-docto = i-seq-wt-docto USE-INDEX seq-tabela NO-LOCK.
                   ASSIGN i-seq-wt-it-docto-fim = wt-it-docto.seq-wt-it-docto.
               END.

               RUN geraWtItDoctoPartindoDoTtItTercNf IN wh-bodi317sd(INPUT i-seq-wt-docto,
                                                                     INPUT  10,
                                                                     INPUT  10,
                                                                     INPUT  TABLE tt-it-terc-nf,
                                                                     OUTPUT l-proc-ok-aux).

               FOR EACH wt-it-docto USE-INDEX seq-tabela WHERE
                   wt-it-docto.seq-wt-docto    = i-seq-wt-docto AND
                   wt-it-docto.seq-wt-it-docto > i-seq-wt-it-docto-fim.
                   ASSIGN i-seq-wt-it-docto = wt-it-docto.seq-wt-it-docto.

                   FOR FIRST ITEM WHERE ITEM.it-codigo = tt-es-it-ordem.item-cj NO-LOCK.
                       ASSIGN c-aux = TRIM(ENTRY(1,ITEM.codigo-refer, ' ')) + '      ' + TRIM(ENTRY(3,ITEM.codigo-refer, '')) NO-ERROR. //26/05/2021
                       ASSIGN wt-it-docto.narrativa = 'CJ: ' + c-aux + '| ' + wt-it-docto.narrativa.
                   END.
                   //wt-it-docto.narrativa = 'Seq. Venda: ' + STRING(tt-es-it-ordem.seq-wt-it-docto) + '| ' + wt-it-docto.narrativa.
               END.

               FOR EACH tt-it-terc-nf WHERE
                   tt-it-terc-nf.selecionado = YES.

                   FIND b-saldo-terc WHERE 
                        ROWID(b-saldo-terc) = tt-it-terc-nf.rw-saldo-terc NO-LOCK NO-ERROR.

                   PUT UNFORMAT
                   tt-it-terc-nf.sequencia         ';'
                   tt-it-terc-nf.it-codigo         ';'
                   tt-it-terc-nf.cod-refer         ';'
                   tt-it-terc-nf.desc-nar          ';'
                   tt-it-terc-nf.quantidade        ';'
                   tt-it-terc-nf.qt-disponivel-inf ';'
                   tt-it-terc-nf.qt-alocada        ';'
                   tt-it-terc-nf.qt-disponivel     ';'
                   tt-it-terc-nf.preco-total-inf   ';'
                   tt-it-terc-nf.preco-total       ';'
                   tt-it-terc-nf.selecionado       ';'
                   (IF AVAIL b-saldo-terc THEN b-saldo-terc.nro-docto ELSE '') SKIP.
  
                   /*
                   
                   PUT UNFORMAT 
                       tt-it-terc-nf.sequencia  AT 1
                       tt-it-terc-nf.it-codigo  AT 20
                       tt-it-terc-nf.quantidade AT 40 
                       (IF AVAIL b-saldo-terc THEN b-saldo-terc.nro-docto ELSE '') AT 60 SKIP.*/
               END.
         
               FIND WT-DOCTO WHERE wt-docto.seq-wt-docto = i-seq-wt-docto NO-ERROR.

               RUN devolveErrosbodi317sd IN wh-bodi317sd(OUTPUT c-ultimo-metodo-exec,
                                                         OUTPUT table RowErrors).

               FIND FIRST RowErrors NO-LOCK NO-ERROR.
               /* Caso tenha achado algum erro ou advert�ncia, mostra em tela */

                IF  AVAIL RowErrors THEN
                    FOR EACH RowErrors:
                        IF errorsubtype = "ERROR" THEN
                        DO:

                           MESSAGE "C - NOTA DE RETORNO" SKIP(1)
                                   errordescription  SKIP
                                   errorhelp         SKIP
                                   "Favor abandonar implantacao !" 
                                   VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                           l-proc-ok-aux = NO.
                           UNDO, LEAVE.
                        END.
                        ELSE
                           MESSAGE "NOTA DE RETORNO" SKIP(1)
                                   errordescription
                                   VIEW-AS ALERT-BOX INFO BUTTONS OK.
                    END.

                /* Caso ocorreu problema nas valida��es, n�o continua o processo */
                IF  NOT l-proc-ok-aux THEN
                    UNDO, LEAVE.
/* Comentado Lucio Zamarioli 03/08/2021 
            END.
            */ 
        END.    /*** FOR EACH tt-es-it-ordem ***/        
            

        FOR FIRST WT-DOCTO WHERE 
            wt-docto.seq-wt-docto = i-seq-wt-docto NO-LOCK.

           PUT UNFORMAT 
               SKIP(2)
               'Item' AT 1
               'Quantidade' AT 20 SKIP.

           FOR EACH wt-it-docto OF wt-docto NO-LOCK.
               PUT UNFORMAT 
                   wt-it-docto.it-codigo     AT 1
                   wt-it-docto.quantidade[1] AT 20 SKIP.
           END.
       END.

       OUTPUT CLOSE.

       RUN utp/ut-utils.p PERSISTENT SET h-prog.
       RUN EXECUTE IN h-prog(INPUT "notepad",
                             INPUT c-arquivo).
       DELETE PROCEDURE h-prog.
        
        /* Finaliza��o das BOS utilizada no c�lculo */
        RUN finalizaBOS IN wh-bodi317in.

        /* Reinicializa��o das BOS para C�lculo */
        RUN dibo/bodi317in.p PERSISTENT SET wh-bodi317in.
        RUN inicializaBOS IN wh-bodi317in(OUTPUT wh-bodi317pr,
                                          OUTPUT wh-bodi317sd,     
                                          OUTPUT wh-bodi317im1bra,
                                          OUTPUT wh-bodi317va).

        /* Limpar a tabela de erros em todas as BOS */
        RUN emptyRowErrors        IN wh-bodi317in.

        /* Calcula o pedido, com acompanhamento */
        RUN inicializaAcompanhamento IN wh-bodi317pr.

        RUN confirmaCalculo          IN wh-bodi317pr(INPUT  i-seq-wt-docto,
                                                     OUTPUT l-proc-ok-aux).
        RUN finalizaAcompanhamento   IN wh-bodi317pr.          

        /* Busca poss�veis erros que ocorreram nas valida��es */
        RUN devolveErrosbodi317pr    IN wh-bodi317pr (OUTPUT c-ultimo-metodo-exec,
                                                      OUTPUT table RowErrors).

        /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
        FIND FIRST RowErrors NO-LOCK NO-ERROR.


        /* Caso tenha achado algum erro ou advert�ncia, mostra em tela */
        IF  AVAIL RowErrors THEN
            FOR EACH RowErrors:
                IF errorsubtype = "ERROR" THEN
                DO:
                   MESSAGE "E - NOTA DE RETORNO" SKIP(1)
                           errordescription  SKIP
                           errorhelp         SKIP
                           "Favor abandonar implantacao !" 

                           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                   l-proc-ok-aux = NO.
                   UNDO, LEAVE.
                END.
                ELSE
                   MESSAGE "NOTA DE RETORNO" SKIP(1)
                           errordescription
                           VIEW-AS ALERT-BOX INFO BUTTONS OK.
            END.
         
         
        /* Caso ocorreu problema nas valida��es, n�o continua o processo */
        IF  NOT l-proc-ok-aux THEN
            UNDO, LEAVE.
            
        /* Efetiva os pedidos e cria a nota */
        RUN dibo/bodi317ef.p PERSISTENT SET wh-bodi317ef.
        RUN emptyRowErrors           IN wh-bodi317in.
        RUN inicializaAcompanhamento IN wh-bodi317ef.
        RUN setaHandlesBOS           IN wh-bodi317ef(wh-bodi317pr,     
                                                     wh-bodi317sd, 
                                                     wh-bodi317im1bra, 
                                                     wh-bodi317va).
        RUN efetivaNota              IN wh-bodi317ef(INPUT  i-seq-wt-docto,
                                                     INPUT  YES,
                                                     OUTPUT l-proc-ok-aux).
        RUN finalizaAcompanhamento   IN wh-bodi317ef.

        /* Busca poss�veis erros que ocorreram nas valida��es */
        RUN devolveErrosbodi317ef    IN wh-bodi317ef(OUTPUT c-ultimo-metodo-exec,
                                                     OUTPUT table RowErrors).

        /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
        FIND FIRST RowErrors
             WHERE RowErrors.ErrorSubType = "ERROR":U NO-ERROR.

        /* Caso tenha achado algum erro ou advert�ncia, mostra em tela */
        IF  AVAIL RowErrors THEN
            FOR EACH RowErrors:
                IF errorsubtype = "ERROR" THEN
                DO:
                   MESSAGE "F - NOTA DE RETORNO" SKIP(1)
                           errordescription  SKIP
                           errorhelp         SKIP
                           "Favor abandonar implantacao !" 
                           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                   l-proc-ok-aux = NO.
                   UNDO, LEAVE.
                END.
                ELSE
                   MESSAGE "NOTA DE RETORNO" SKIP(1)
                           errordescription
                           VIEW-AS ALERT-BOX INFO BUTTONS OK.
            END.

        /* Caso ocorreu problema nas valida��es, n�o continua o processo */
        IF  NOT l-proc-ok-aux THEN DO:
            DELETE PROCEDURE wh-bodi317ef.
            UNDO, LEAVE.
        END.

        /* Busca as notas fiscais geradas */
        
        RUN buscaTTNotasGeradas IN wh-bodi317ef(OUTPUT l-proc-ok-aux,
                                                OUTPUT table tt-notas-geradas).
                                                
        /* Elimina o handle do programa bodi317ef */
        DELETE PROCEDURE wh-bodi317ef.
        LEAVE.
    END. /* REPEAT */

    /* Finaliza��o das BOS utilizada no c�lculo */
    RUN finalizaBOS IN wh-bodi317in.

    IF VALID-HANDLE(wh-bodi317in) THEN DO:
        DELETE PROCEDURE wh-bodi317in NO-ERROR.
        ASSIGN wh-bodi317in = ?.
    END.

    IF VALID-HANDLE(wh-bodi317ef) THEN DO:
        DELETE PROCEDURE wh-bodi317ef NO-ERROR.
        ASSIGN wh-bodi317ef = ?.
    END.



    ASSIGN c-nota-remessa = "".
    FOR EACH tt-it-terc-nf:
        
        FIND b-saldo-terc
            WHERE ROWID(b-saldo-terc) = tt-it-terc-nf.rw-saldo-terc
            NO-LOCK NO-ERROR.
        IF  AVAIL b-saldo-terc THEN DO:
            IF  INDEX(b-saldo-terc.nro-docto,c-nota-remessa) = 0 THEN DO:
                IF  c-nota-remessa = "" THEN
                    ASSIGN c-nota-remessa = c-nota-remessa + b-saldo-terc.nro-docto.
                ELSE
                    ASSIGN c-nota-remessa = c-nota-remessa + "," + b-saldo-terc.nro-docto.
            END.
        END.
        
    END.
    


    /* Mostrar as notas geradas */
 /* FOR FIRST tt-notas-geradas NO-LOCK :  /* triah 20042012 substituido */  
 
       /* FIND LAST WT-DOCTO WHERE wt-docto.seq-wt-docto = i-seq-wt-docto 
            NO-LOCK NO-ERROR.*/

        FIND LAST b-tt-notas-geradas NO-ERROR.   

        FOR FIRST nota-fiscal WHERE  
            ROWID(nota-fiscal) = tt-notas-geradas.rw-nota-fiscal EXCLUSIVE-LOCK:
    
            FIND FIRST param-estoq NO-LOCK NO-ERROR.
            IF  AVAIL param-estoq THEN DO:
                FOR EACH it-nota-fisc OF nota-fiscal:
                    ASSIGN it-nota-fisc.ct-cuscon = param-estoq.conta-consig.
                END.
            END.
            
            ASSIGN nota-fiscal.observ-nota =
                   " **  RETORNO DE INSUMOS REFERENTE A NOTA FISCAL DE VENDA NR. " +  
                   STRING(INT(b-tt-notas-geradas.nr-nota) + 1,"9999999") + "  ** " +
                   "   RETORNO REFERENTE SUA(s) NOTA(s) FISCAL(is) NR. " + c-nota-remessa. 




            BELL.

            IF  tt-notas-geradas.nr-nota = b-tt-notas-geradas.nr-nota THEN 
                run utp/ut-msgs.p(INPUT "show",
                                  INPUT 15263,
                                  INPUT STRING(tt-notas-geradas.nr-nota) + "~~" +
                                        STRING(nota-fiscal.cod-estabel)  + "~~" +
                                        STRING(nota-fiscal.serie)).
            ELSE 
                RUN utp/ut-msgs.p(INPUT "show",
                                  INPUT 15264,
                                  INPUT STRING(tt-notas-geradas.nr-nota)   + "~~" +
                                        STRING(b-tt-notas-geradas.nr-nota) + "~~" +
                                        STRING(nota-fiscal.cod-estabel)    + "~~" +
                                        STRING(nota-fiscal.serie)).
 
        END.
 
    END.
    */
 
 
    DEF BUFFER bf-nota-fiscal FOR nota-fiscal.

   /* Mostrar as notas geradas */
 /* FOR FIRST tt-notas-geradas NO-LOCK :  /* triah 20042012 substituido */ */
    FOR EACH tt-notas-geradas NO-LOCK :   /* triah 20042012 */ 

       /* FIND LAST WT-DOCTO WHERE wt-docto.seq-wt-docto = i-seq-wt-docto 
            NO-LOCK NO-ERROR.*/

        FIND LAST b-tt-notas-geradas NO-ERROR.   
  
        FOR FIRST nota-fiscal WHERE  
            ROWID(nota-fiscal) = tt-notas-geradas.rw-nota-fiscal EXCLUSIVE-LOCK:

            FIND FIRST param-estoq NO-LOCK NO-ERROR.
            IF  AVAIL param-estoq THEN DO:
                FOR EACH it-nota-fisc OF nota-fiscal:
                    ASSIGN it-nota-fisc.ct-cuscon = param-estoq.conta-consig.
                END.
            END.
            
            ASSIGN nota-fiscal.observ-nota = nota-fiscal.observ-nota +
                   " ** RETORNO DE INSUMOS REFERENTE A NOTA FISCAL DE VENDA NR. " +  
                   STRING(INT(b-tt-notas-geradas.nr-nota) + 1,"9999999") +  " ** "  . 

            /*            
            /* Criacao de tabela de relacionamento entre nota de retorno e nota de venda */
            FIND FIRST bf-nota-fiscal
                 WHERE ROWID(bf-nota-fiscal) = b-tt-notas-geradas.rw-nota-fiscal
                 NO-LOCK NO-ERROR.

            FIND FIRST es-nf-venda-retorno
                 WHERE es-nf-venda-retorno.cod-estabel     = bf-nota-fiscal.cod-estabel
                   AND es-nf-venda-retorno.serie           = bf-nota-fiscal.serie
                   AND es-nf-venda-retorno.nr-nota-fis     = STRING(INT(b-tt-notas-geradas.nr-nota) + 1,"9999999")
                   AND es-nf-venda-retorno.cod-estabel-ret = nota-fiscal.cod-estabel
                   AND es-nf-venda-retorno.serie-ret       = nota-fiscal.serie
                   AND es-nf-venda-retorno.nr-nota-fis-ret = nota-fiscal.nr-nota-fis
                NO-LOCK NO-ERROR.
            IF NOT AVAIL es-nf-venda-retorno THEN DO:
                CREATE es-nf-venda-retorno.
                ASSIGN es-nf-venda-retorno.cod-estabel     = bf-nota-fiscal.cod-estabel                        
                       es-nf-venda-retorno.serie           = bf-nota-fiscal.serie                              
                       es-nf-venda-retorno.nr-nota-fis     = STRING(INT(b-tt-notas-geradas.nr-nota) + 1,"9999999") 
                       es-nf-venda-retorno.cod-estabel-ret = nota-fiscal.cod-estabel                               
                       es-nf-venda-retorno.serie-ret       = nota-fiscal.serie                                     
                       es-nf-venda-retorno.nr-nota-fis-ret = nota-fiscal.nr-nota-fis
                       es-nf-venda-retorno.itens-venda     = gb-upbodi317pr-it-venda.                               
            END.
            */
             
            gb-upbodi317pr-it-venda = "".
    
            IF  tt-notas-geradas.nr-nota = b-tt-notas-geradas.nr-nota THEN DO:

                ASSIGN c-nota-ult = b-tt-notas-geradas.nr-nota.
                FIND FIRST  b-tt-notas-geradas NO-ERROR.   
                ASSIGN c-nota-pri = b-tt-notas-geradas.nr-nota.

                BELL.
 
                IF c-nota-pri = c-nota-ult THEN 
                    RUN utp/ut-msgs.p(INPUT "show",
                                      INPUT 15263,
                                      INPUT STRING(tt-notas-geradas.nr-nota) + "~~" +
                                            STRING(nota-fiscal.cod-estabel)  + "~~" +
                                            STRING(nota-fiscal.serie)).
                ELSE
                    RUN utp/ut-msgs.p(INPUT "show",
                                      INPUT 15264,
                                      INPUT STRING(c-nota-pri)   + "~~" +
                                            STRING(c-nota-ult)   + "~~" +
                                            STRING(nota-fiscal.cod-estabel)    + "~~" +
                                            STRING(nota-fiscal.serie)).
 
            END.
  
        END.
 
    END.

   /* Fim do programa que calcula uma nota complementar */
    
END PROCEDURE.


/********************************************************************************************************/
/********************************************************************************************************/

PROCEDURE pi-cria-tt-it-terc-nf:

    DEF INPUT  PARAM p-i-seq-wt-docto    LIKE wt-it-docto.seq-wt-docto NO-UNDO.
    DEF INPUT  PARAM p-de-qtde-inform    AS DEC                        NO-UNDO.
    DEF INPUT  PARAM p-de-preori         LIKE es-it-ordem.preori       NO-UNDO.
    DEF INPUT  PARAM p-de-proporcao      AS DEC                        NO-UNDO.
    DEF INPUT  PARAM p-de-reajuste       AS DEC                        NO-UNDO.
    DEF INPUT  PARAM p-r-rowid-saldo-terc AS ROWID                     NO-UNDO. 
    DEF OUTPUT PARAM table               FOR tt-it-terc-nf.
    DEF OUTPUT PARAM p-l-procedimento-ok AS LOGICAL INITIAL YES        NO-UNDO.
    
    DEFINE BUFFER bb-saldo-terc FOR saldo-terc.
    /*
    OUTPUT TO "\\10.0.1.212\erp-116\esp\ems2\arquivo.txt" APPEND.
    FOR EACH tt-arquivo.
        PUT tt-arquivo.it-codigo-nf     FORMAT "x(16)"  AT 01
            tt-arquivo.it-codigo-comp   FORMAT "x(16)"  AT 20
            tt-arquivo.it-componente    FORMAT "x(16)"  AT 40
            tt-arquivo.quantidade       FORMAT "->>>,>>>,>>>,>>9.9999" AT 80
            STRING(TIME,"hh:mm:ss") AT 100
            SKIP.
    END.
    OUTPUT CLOSE.


    OUTPUT TO "\\10.0.1.212\erp-116\esp\ems2\arquivo-imp-xml-antes.txt" APPEND.
    FOR EACH tt-arquivo-imp-xml.
        PUT tt-arquivo-imp-xml.it-codigo-nf     FORMAT "x(16)"  AT 01
            tt-arquivo-imp-xml.it-codigo-comp   FORMAT "x(16)"  AT 20
            tt-arquivo-imp-xml.it-componente    FORMAT "x(16)"  AT 40
            tt-arquivo-imp-xml.quantidade       FORMAT "->>>,>>>,>>>,>>9.9999" AT 80
            tt-arquivo-imp-xml.usuario          FORMAT "x(16)"  AT 100
            tt-arquivo-imp-xml.impresso
            STRING(TIME,"hh:mm:ss") AT 100
            SKIP.
    END.
    OUTPUT CLOSE.
    */


    
    FIND FIRST bb-saldo-terc 
           WHERE ROWID (bb-saldo-terc) = p-r-rowid-saldo-terc
        NO-LOCK NO-ERROR.
        
    IF AVAILABLE bb-saldo-terc THEN DO:
    
        FIND FIRST componente
            WHERE componente.serie-docto  = bb-saldo-terc.serie-docto
            AND   componente.nro-docto    = bb-saldo-terc.nro-docto
            AND   componente.cod-emitente = bb-saldo-terc.cod-emitente
            AND   componente.nat-operacao = bb-saldo-terc.nat-operacao
            AND   componente.it-codigo    = bb-saldo-terc.it-codigo
            AND   componente.cod-refer    = bb-saldo-terc.cod-refer
            AND   componente.sequencia    = bb-saldo-terc.sequencia 
            NO-LOCK NO-ERROR.
            
        IF AVAILABLE componente THEN DO:
        
            /* Inicio - Lauriano */
            IF componente.cod-emitente = 506 OR componente.cod-emitente = 2261 /* o codigo 2259 � novo*/ OR componente.cod-emitente = 2259 THEN DO.

                /*FIND FIRST tt-arquivo
                     WHERE tt-arquivo.it-componente = bb-saldo-terc.it-codigo.
                     NO-LOCK NO-ERROR.
                IF AVAIL tt-arquivo THEN DO.
                    FOR EACH tt-arquivo NO-LOCK
                        WHERE tt-arquivo.it-componente = bb-saldo-terc.it-codigo.
    
                        CREATE tt-it-terc-nf.
                        ASSIGN tt-it-terc-nf.rw-saldo-terc     = ROWID(bb-saldo-terc)
                               tt-it-terc-nf.sequencia         = bb-saldo-terc.sequencia
                               tt-it-terc-nf.it-codigo         = bb-saldo-terc.it-codigo
                               tt-it-terc-nf.cod-refer         = bb-saldo-terc.cod-refer
                               tt-it-terc-nf.desc-nar          = IF  item.tipo-contr = 4 /* D�bito Direto */
                                                                 THEN substr(item.narrativa,1,60)
                                                                 ELSE item.desc-item
                               tt-it-terc-nf.quantidade        = tt-arquivo.quantidade    /*bb-saldo-terc.quantidade*/
                               tt-it-terc-nf.qt-disponivel-inf = tt-arquivo.quantidade /* p-de-qtde-inform*/ * 
                                                                 IF p-de-proporcao = 0 OR 
                                                                    natur-oper.tp-oper-terc = 6 /* Reajuste de Pre�o */
                                                                 THEN 1
                                                                 ELSE (p-de-proporcao / 100)
                               tt-it-terc-nf.qt-alocada        = tt-arquivo.quantidade     /*bb-saldo-terc.dec-1*/
                               tt-it-terc-nf.qt-disponivel     = bb-saldo-terc.quantidade - tt-arquivo.quantidade    /*bb-saldo-terc.dec-1*/
                               tt-it-terc-nf.preco-total-inf   = tt-it-terc-nf.qt-disponivel-inf * p-de-preori
                               tt-it-terc-nf.preco-total       = tt-it-terc-nf.qt-disponivel-inf * p-de-preori
            /*                    tt-it-terc-nf.preco-total       = componente.preco-total[1] [Mauro Jabur 06/07/2004] */
                               tt-it-terc-nf.selecionado       = YES.
                    END.
                END.
                ELSE DO.
                    CREATE tt-it-terc-nf.

                    ASSIGN tt-it-terc-nf.rw-saldo-terc     = ROWID(bb-saldo-terc)
                           tt-it-terc-nf.sequencia         = bb-saldo-terc.sequencia
                           tt-it-terc-nf.it-codigo         = bb-saldo-terc.it-codigo
                           tt-it-terc-nf.cod-refer         = bb-saldo-terc.cod-refer
                           tt-it-terc-nf.desc-nar          = IF  item.tipo-contr = 4 /* D�bito Direto */
                                                             THEN substr(item.narrativa,1,60)
                                                             ELSE item.desc-item
                           tt-it-terc-nf.quantidade        = bb-saldo-terc.quantidade
                           tt-it-terc-nf.qt-disponivel-inf = p-de-qtde-inform * 
                                                             IF p-de-proporcao = 0 OR 
                                                                natur-oper.tp-oper-terc = 6 /* Reajuste de Pre�o */
                                                             THEN 1
                                                             ELSE (p-de-proporcao / 100)
                           tt-it-terc-nf.qt-alocada        = bb-saldo-terc.dec-1
                           tt-it-terc-nf.qt-disponivel     = bb-saldo-terc.quantidade - bb-saldo-terc.dec-1
                           tt-it-terc-nf.preco-total-inf   = tt-it-terc-nf.qt-disponivel-inf * p-de-preori
                           tt-it-terc-nf.preco-total       = tt-it-terc-nf.qt-disponivel-inf * p-de-preori
        /*                    tt-it-terc-nf.preco-total       = componente.preco-total[1] [Mauro Jabur 06/07/2004] */
                           tt-it-terc-nf.selecionado       = YES.

                END.

            END.
            ELSE DO.
            /* Fimm Lauriano */*/
                CREATE tt-it-terc-nf.
                
                ASSIGN tt-it-terc-nf.rw-saldo-terc     = ROWID(bb-saldo-terc)
                       tt-it-terc-nf.sequencia         = bb-saldo-terc.sequencia
                       tt-it-terc-nf.it-codigo         = bb-saldo-terc.it-codigo
                       tt-it-terc-nf.cod-refer         = bb-saldo-terc.cod-refer
                       tt-it-terc-nf.desc-nar          = IF  item.tipo-contr = 4 /* D�bito Direto */
                                                         THEN substr(item.narrativa,1,60)
                                                         ELSE item.desc-item
                       tt-it-terc-nf.quantidade        = bb-saldo-terc.quantidade
                       tt-it-terc-nf.qt-disponivel-inf = p-de-qtde-inform * 
                                                         IF p-de-proporcao = 0 OR 
                                                            natur-oper.tp-oper-terc = 6 /* Reajuste de Pre�o */
                                                         THEN 1
                                                         ELSE (p-de-proporcao / 100)
                       tt-it-terc-nf.qt-alocada        = bb-saldo-terc.dec-1
                       tt-it-terc-nf.qt-disponivel     = bb-saldo-terc.quantidade - bb-saldo-terc.dec-1
                       tt-it-terc-nf.preco-total-inf   = tt-it-terc-nf.qt-disponivel-inf * p-de-preori
                       tt-it-terc-nf.preco-total       = tt-it-terc-nf.qt-disponivel-inf * p-de-preori
    /*                    tt-it-terc-nf.preco-total       = componente.preco-total[1] [Mauro Jabur 06/07/2004] */
                       tt-it-terc-nf.selecionado       = YES.
            END.
                   
         END.
    
    END.
    
 END PROCEDURE.
  
/********************************************************************************************************/
/********************************************************************************************************/
