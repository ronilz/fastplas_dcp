
/*******************************************************************************
** Programa..: UPCCD1700.P
** Objetivo..: UPC - CD1700 - Cadastro de Usuarios de Materiais
** Autor.....: Jos� M�rio   -   A2Solutions 
** Data......: Julho - 19/07/2016
*******************************************************************************/



/* definicao de parametros */
DEF INPUT PARAM p-ind-event            AS CHAR             NO-UNDO. 
DEF INPUT PARAM p-ind-object           AS CHAR             NO-UNDO.
DEF INPUT PARAM p-wgh-object           AS HANDLE           NO-UNDO.
DEF INPUT PARAM p-wgh-frame            AS WIDGET-HANDLE    NO-UNDO.
DEF INPUT PARAM p-cod-table            AS CHAR             NO-UNDO.
DEF INPUT PARAM p-row-table            AS ROWID            NO-UNDO.

DEFINE VARIABLE c-handle-obj           AS CHARACTER        NO-UNDO.
DEFINE VARIABLE wh-usuar-comprador     AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-lim-solic           AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-lim-requis          AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-lim-requis-manut    AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-lim-solic-l         AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-lim-requis-l        AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-lim-requis-manut-l  AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-tp-preco            AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-cod-grp-compra      AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-sc-codigo           AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-c-lotacao           AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-cod-grp-compra-l    AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-sc-codigo-l         AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE wh-c-lotacao-l         AS WIDGET-HANDLE    NO-UNDO.

DEFINE VARIABLE wh-campo-altera-OC     AS WIDGET-HANDLE    NO-UNDO.
/* DEFINE VARIABLE wh-campo-pedido-OC     AS WIDGET-HANDLE    NO-UNDO.  */

{tools\fc-handle-obj.i}
{utp/ut-glob.i}
{tools\fc-falso.i}

/*
MESSAGE "p-ind-event  = " p-ind-event       SKIP
        "p-ind-object = " p-ind-object      SKIP
        "p-wgh-object = " p-wgh-object:NAME SKIP
        "p-wgh-frame  = " p-wgh-frame:NAME  SKIP
        "p-cod-table  = " string(p-cod-table)       SKIP
        "p-row-table  = " string(p-row-table)
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
*/


IF p-ind-object   =  "VIEWER"  THEN DO: 

   IF p-wgh-object:NAME =  "invwr/v01in627.w"  THEN DO:


      IF p-ind-event = "BEFORE-INITIALIZE" THEN DO:
     
          ASSIGN c-handle-obj = fc-handle-obj("usuar-comprador,lim-solic,lim-requis,lim-requis-manut,tp-preco,cod-grp-compra,sc-codigo,c-lotacao",p-wgh-frame)
                 wh-usuar-comprador  = WIDGET-HANDLE(ENTRY(1,c-handle-obj))
                 wh-lim-solic        = WIDGET-HANDLE(ENTRY(2,c-handle-obj))
                 wh-lim-requis       = WIDGET-HANDLE(ENTRY(3,c-handle-obj))
                 wh-lim-requis-manut = WIDGET-HANDLE(ENTRY(4,c-handle-obj))
                 wh-tp-preco         = WIDGET-HANDLE(ENTRY(5,c-handle-obj))
                 wh-cod-grp-compra   = WIDGET-HANDLE(ENTRY(6,c-handle-obj))
                 wh-sc-codigo        = WIDGET-HANDLE(ENTRY(7,c-handle-obj))
                 wh-c-lotacao        = WIDGET-HANDLE(ENTRY(8,c-handle-obj)).

          IF VALID-HANDLE(wh-usuar-comprador) THEN DO:
     
              ON 'value-changed':U OF wh-usuar-comprador PERSISTENT RUN ems2\upc\upccd1700.p (INPUT "value-changed",
                                                                                              INPUT "wh-usuar-comprador",
                                                                                              INPUT p-wgh-object ,
                                                                                              INPUT p-wgh-frame ,
                                                                                              INPUT ? /* p-cod-table  */ ,
                                                                                              INPUT ? /* p-row-table  */ ).
          END.

          /* Posi��o dos campos at� a vers�o TOTVS Datasul 12.1.13 */
          IF wh-tp-preco:COL < 30 AND       /* 29,29 */ 
             wh-tp-preco:ROW < 8  THEN DO:  /* 7,63 */

              ASSIGN wh-lim-solic:COL        = wh-lim-solic:COL        + 11.3
                     wh-lim-requis:COL       = wh-lim-requis:COL       + 11.3      
                     wh-lim-requis-manut:COL = wh-lim-requis-manut:COL + 11.3.

              ASSIGN wh-lim-solic-l        = wh-lim-solic:SIDE-LABEL-HANDLE
                     wh-lim-requis-l       = wh-lim-requis:SIDE-LABEL-HANDLE
                     wh-lim-requis-manut-l = wh-lim-requis-manut:SIDE-LABEL-HANDLE.

              ASSIGN wh-lim-solic-l:COL        = wh-lim-solic-l:COL        + 11.5
                     wh-lim-requis-l:COL       = wh-lim-requis-l:COL       + 11.5
                     wh-lim-requis-manut-l:COL = wh-lim-requis-manut-l:COL + 11.5.


              CREATE TOGGLE-BOX wh-campo-altera-OC
              ASSIGN COL       = wh-lim-solic:COL - 37.5
                     ROW       = wh-lim-solic:ROW
                     WIDTH     = wh-usuar-comprador:WIDTH + 9
                     HEIGHT    = wh-usuar-comprador:HEIGHT
                     LABEL     = "Altera OC (Ordem Compra)"
                     FRAME     = wh-usuar-comprador:FRAME
                     NAME      = "wh-campo-altera-OC"
                     VISIBLE   = TRUE
                     SENSITIVE = FALSE.

              IF wh-campo-altera-OC:MOVE-AFTER-TAB-ITEM(wh-usuar-comprador) THEN.

/*               CREATE TOGGLE-BOX wh-campo-pedido-OC                                */
/*               ASSIGN COL       = wh-campo-altera-OC:COL                           */
/*                      ROW       = wh-lim-requis:ROW                                */
/*                      WIDTH     = wh-campo-altera-OC:WIDTH                         */
/*                      HEIGHT    = wh-campo-altera-OC:HEIGHT                        */
/*                      LABEL     = "Pedido OC (outro Comprador)"                    */
/*                      FRAME     = wh-campo-altera-OC:FRAME                         */
/*                      NAME      = "wh-campo-pedido-OC"                             */
/*                      VISIBLE   = TRUE                                             */
/*                      SENSITIVE = FALSE.                                           */
/*               IF wh-campo-pedido-OC:MOVE-AFTER-TAB-ITEM(wh-campo-altera-OC) THEN. */

          END.
          /* Vers�o mais recente do TOTVS Datasul, a partir da 12.1.18 */
          ELSE DO:
          
              ASSIGN wh-cod-grp-compra:COL = wh-cod-grp-compra:COL + 16
                     wh-sc-codigo:COL      = wh-sc-codigo:COL      + 16
                     wh-c-lotacao:COL      = wh-c-lotacao:COL      + 16.

              ASSIGN wh-cod-grp-compra-l = wh-cod-grp-compra:SIDE-LABEL-HANDLE
                     wh-sc-codigo-l      = wh-sc-codigo:SIDE-LABEL-HANDLE
                     wh-c-lotacao-l      = wh-c-lotacao:SIDE-LABEL-HANDLE.

              ASSIGN wh-cod-grp-compra-l:COL = wh-cod-grp-compra-l:COL + 16
                     wh-sc-codigo-l:COL      = wh-sc-codigo-l:COL      + 16
                     wh-c-lotacao-l:COL      = wh-c-lotacao-l:COL      + 16.

              CREATE TOGGLE-BOX wh-campo-altera-OC
              ASSIGN COL       = wh-cod-grp-compra:COL - 35
                     ROW       = wh-cod-grp-compra:ROW
                     WIDTH     = wh-usuar-comprador:WIDTH + 9
                     HEIGHT    = wh-usuar-comprador:HEIGHT
                     LABEL     = "Altera OC (Ordem Compra)"
                     FRAME     = wh-usuar-comprador:FRAME
                     NAME      = "wh-campo-altera-OC"
                     VISIBLE   = TRUE
                     SENSITIVE = FALSE.

              IF wh-campo-altera-OC:MOVE-AFTER-TAB-ITEM(wh-usuar-comprador) THEN.

/*               CREATE TOGGLE-BOX wh-campo-pedido-OC                                */
/*               ASSIGN COL       = wh-campo-altera-OC:COL                           */
/*                      ROW       = wh-sc-codigo:ROW                                 */
/*                      WIDTH     = wh-campo-altera-OC:WIDTH                         */
/*                      HEIGHT    = wh-campo-altera-OC:HEIGHT                        */
/*                      LABEL     = "Pedido OC (outro Comprador)"                    */
/*                      FRAME     = wh-campo-altera-OC:FRAME                         */
/*                      NAME      = "wh-campo-pedido-OC"                             */
/*                      VISIBLE   = TRUE                                             */
/*                      SENSITIVE = FALSE.                                           */
/*               IF wh-campo-pedido-OC:MOVE-AFTER-TAB-ITEM(wh-campo-altera-OC) THEN. */

              
          END.
     
      END.   /* IF p-ind-event = "BEFORE-INITIALIZE" */

     
 

      IF p-ind-event = "AFTER-ENABLE" OR p-ind-event = "AFTER-DISABLE" THEN DO:
     
          ASSIGN c-handle-obj = fc-handle-obj("usuar-comprador,wh-campo-altera-OC,wh-campo-pedido-OC",p-wgh-frame) 
                 wh-usuar-comprador  = WIDGET-HANDLE(ENTRY(1,c-handle-obj))
                 wh-campo-altera-OC  = WIDGET-HANDLE(ENTRY(2,c-handle-obj))
                 /* wh-campo-pedido-OC  = WIDGET-HANDLE(ENTRY(3,c-handle-obj))*/ .
     
          IF p-ind-event = "AFTER-ENABLE" THEN
            ASSIGN wh-campo-altera-OC:SENSITIVE = TRUE.
          ELSE
              IF p-ind-event = "AFTER-DISABLE" THEN 
                  ASSIGN wh-campo-altera-OC:SENSITIVE = FALSE.

          FIND FIRST usuar-mater WHERE rowid(usuar-mater) = p-row-table NO-LOCK NO-ERROR.

          IF AVAIL usuar-mater THEN DO:
              
              
                  
                  assign wh-campo-altera-OC:CHECKED = IF SUBSTRING(usuar-mater.char-2,100,1) = 'y' THEN YES ELSE NO.              
             
     
          END.
          
     
      END.   /* IF p-ind-event = "AFTER-ENABLE" OR p-ind-event = "AFTER-DISABLE" */
     
     
     
      IF p-ind-event = "AFTER-CANCEL" THEN DO:
     
          ASSIGN c-handle-obj = fc-handle-obj("usuar-comprador,wh-campo-altera-OC,wh-campo-pedido-OC",p-wgh-frame) 
                 wh-usuar-comprador  = WIDGET-HANDLE(ENTRY(1,c-handle-obj))
                 wh-campo-altera-OC  = WIDGET-HANDLE(ENTRY(2,c-handle-obj)).
/*                  wh-campo-pedido-OC  = WIDGET-HANDLE(ENTRY(3,c-handle-obj)). */
     
          
            ASSIGN wh-campo-altera-OC:SENSITIVE = FALSE.

          FIND FIRST usuar-mater WHERE rowid(usuar-mater) = p-row-table EXCLUSIVE-LOCK NO-ERROR.

          IF AVAIL usuar-mater THEN DO:
              
              
                  
                  assign  substring(usuar-mater.char-2,100,1)   =  IF wh-campo-altera-OC:CHECKED THEN 'Y' ELSE 'N'.
                         
     
              END.
     
         
     
      END.    /* IF p-ind-event = "AFTER-CANCEL" */
     

      IF p-ind-event = "BEFORE-ASSIGN" OR p-ind-event = "ASSIGN"  THEN DO:
     
          ASSIGN c-handle-obj = fc-handle-obj("usuar-comprador,wh-campo-altera-OC,wh-campo-pedido-OC",p-wgh-frame) 
                 wh-usuar-comprador  = WIDGET-HANDLE(ENTRY(1,c-handle-obj))
                 wh-campo-altera-OC  = WIDGET-HANDLE(ENTRY(2,c-handle-obj))
              /*   wh-campo-pedido-OC  = WIDGET-HANDLE(ENTRY(3,c-handle-obj)) */.
           
     

          FIND FIRST usuar-mater WHERE rowid(usuar-mater) = p-row-table NO-LOCK NO-ERROR.

          IF AVAIL usuar-mater THEN DO:
              
             
              assign  SUBSTRING(usuar-mater.char-2,100,1) =  IF wh-campo-altera-OC:CHECKED THEN 'Y' ELSE 'N'.
            

          END.
     
      END.    /* IF p-ind-event = "BEFORE-ASSIGN" OR p-ind-event = "ASSIGN" */
      
      
      IF p-ind-event = "BEFORE-DISPLAY"  THEN DO:
     
          ASSIGN c-handle-obj = fc-handle-obj("usuar-comprador,wh-campo-altera-OC,wh-campo-pedido-OC",p-wgh-frame) 
                 wh-usuar-comprador  = WIDGET-HANDLE(ENTRY(1,c-handle-obj))
                 wh-campo-altera-OC  = WIDGET-HANDLE(ENTRY(2,c-handle-obj))
               /*  wh-campo-pedido-OC  = WIDGET-HANDLE(ENTRY(3,c-handle-obj))*/.
     
          FIND FIRST usuar-mater WHERE rowid(usuar-mater) = p-row-table NO-LOCK NO-ERROR.

          IF AVAIL usuar-mater THEN 
              DO:
              
                    assign wh-campo-altera-OC:CHECKED = IF SUBSTRING(usuar-mater.char-2,100,1) = 'Y' THEN YES ELSE NO.     
              END.
          ELSE 
              assign wh-campo-altera-OC:CHECKED = NO.

                          

          
     
      END.   /* IF p-ind-event = "BEFORE-DISPLAY" */
      

   END.   /* IF p-wgh-object:NAME =  "invwr/v01in627.w" */

   

END.   /* IF p-ind-object = "VIEWER" */



IF p-ind-event  = "value-changed" AND
   p-ind-object = "wh-usuar-comprador" THEN DO:

    ASSIGN c-handle-obj = fc-handle-obj("usuar-comprador,wh-campo-altera-OC",p-wgh-frame) 
           wh-usuar-comprador  = WIDGET-HANDLE(ENTRY(1,c-handle-obj))
           wh-campo-altera-OC  = WIDGET-HANDLE(ENTRY(2,c-handle-obj))
          /* wh-campo-pedido-OC  = WIDGET-HANDLE(ENTRY(3,c-handle-obj))*/.


    IF NOT wh-usuar-comprador:CHECKED THEN
        ASSIGN wh-campo-altera-OC:CHECKED   = NO.
/*                wh-campo-pedido-OC:CHECKED   = NO. */

/*     ASSIGN wh-campo-altera-OC:SENSITIVE = TRUE . */
    /* wh-usuar-comprador:CHECKED */
         /*  wh-campo-pedido-OC:SENSITIVE = wh-usuar-comprador:CHECKED */.

END.

 


/* Fim Programa */

