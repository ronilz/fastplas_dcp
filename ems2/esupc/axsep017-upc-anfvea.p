/*****************************************************************************
* Empresa  : DATASUL WA
* Cliente  : FASTPLAS
* Programa : UAXSEP017
* Descricao: UPC NO PROGRAMA DE GERA��O DE ARQUIVOS NOTA FISCAL ELETR�NICA
* Data     : 12/MAR�O/2009
* Altare��o: 31/01/2010 - wanderlei anfavea
* Versao   : 2.04.00.001 
**************************************************************************/

/************************************************************************* 

 UPC para o programa -> axsep006 (Envio NF-e)
 CUSTOMIZA��O ANFAVIA WA-THIAH    30/01/2010 
 
 Objetivo: Manipular informacoes impressas no XML.

*************************************************************************/

/*
** 2.06.00.000 - emiyahira - triah - 18/03/2011 - adaptacao para NF-e 2.0 (fonte original axsep006-upc-anfvea.p)
*/

/*******************************************************************************************************
* Alteracao: MSM - Triah - 08/05/2013
*            - Alteracao da TAG infAdProd para as notas de retorno do cliente Mercedes 
*              (informa��es da nota de remessa)
*******************************************************************************************************/
/*
2.06.00.002 - Fernando Gon�alves - Totvs Ibirapuera - 22/05/2014
	    - Altera��o Para incluir lista de clientes na logica de tipo de fornecimento	

*/


{cdp/cdcfgdis.i}
{include/i-epc200.i1}


DEF INPUT        PARAM p-ind-event AS CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAM TABLE FOR tt-epc.

DEFINE VARIABLE h-ttDet           AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE hQueryBuffer      AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE h-query           AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE h-campo           AS WIDGET-HANDLE NO-UNDO.

DEFINE VARIABLE h-cpxPed          AS WIDGET-HANDLE NO-UNDO. 
DEFINE VARIABLE h-cpnItem         AS WIDGET-HANDLE NO-UNDO. 

DEFINE VARIABLE h-doc             AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE h-ttInfAdic       AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE h-infcomp         AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE h-campo-desc      AS WIDGET-HANDLE NO-UNDO.

DEF VAR cInfAdProd     AS CHAR   NO-UNDO.
DEF VAR cInfCpl        AS CHAR   NO-UNDO.

DEF VAR h-cod-estabel  AS HANDLE NO-UNDO.
DEF VAR h-serie        AS HANDLE NO-UNDO.
DEF VAR h-nr-nota-fis  AS HANDLE NO-UNDO.
DEF VAR h-it-codigo    AS HANDLE NO-UNDO.
DEF VAR h-nr-seq-fat   AS HANDLE NO-UNDO.

DEF VAR i-cont         AS INTEGER NO-UNDO INIT 1.
DEF VAR l-entrou       AS LOG NO-UNDO.
DEF VAR cNrNotaFis     AS CHAR NO-UNDO.
DEF VAR cObsNotaEsp    AS CHAR NO-UNDO.
DEF VAR cCodEstabel    AS CHAR NO-UNDO.
DEF VAR cSerie         AS CHAR NO-UNDO.
DEF VAR cDescProd      AS CHAR NO-UNDO.
DEF VAR cItCodigo      AS CHAR NO-UNDO.    
/* criado variavel para pedido - valeria 22/09/09*/
DEF VAR cped           AS CHAR NO-UNDO.
DEF VAR cford          AS CHAR NO-UNDO.
DEF VAR cfabentrega    AS CHAR NO-UNDO.
DEF VAR vPosicao       AS INT  NO-UNDO.
DEF VAR cItemCli       AS CHAR NO-UNDO.
DEF VAR cTpFor         AS CHAR NO-UNDO.
DEF VAR cUnidade       AS CHAR NO-UNDO.
DEF VAR vcodigo-trans  AS CHAR NO-UNDO. 
DEF VAR vnome-trans    AS CHAR NO-UNDO. 
DEF VAR vcodigo-rec    AS CHAR NO-UNDO. 
DEF VAR vnome-rec      AS CHAR NO-UNDO. 


DEF VAR iInd           AS INT  NO-UNDO.


DEF VARIABLE wpedido             LIKE it-nota-fisc.nr-pedcli.
DEF VARIABLE w-tot-base-icms-obs AS DEC NO-UNDO.
DEF VARIABLE de-tot-icms-obs     AS DEC NO-UNDO.
DEF VARIABLE de-tot-icmssubs-obs AS DEC NO-UNDO.

DEF NEW GLOBAL SHARED VAR gc-infCpl           AS CHARACTER NO-UNDO.
DEF NEW GLOBAL SHARED VAR gc-infCplAnfavea    AS CHARACTER NO-UNDO.

DEF BUFFER bf-nota-fiscal  FOR nota-fiscal.
DEF BUFFER bf-it-nota-fisc FOR it-nota-fisc.
DEF BUFFER bemitente       FOR emitente.

DEF BUFFER bf-it-nota-venda FOR it-nota-fisc.
DEF BUFFER bf-nota-venda    FOR nota-fiscal.

DEF BUFFER bf-item FOR ITEM.


/* ANFAVEA - InfAdProd */
DEFINE TEMP-TABLE ttInfAdProd NO-UNDO
     FIELD item   AS CHARACTER  /* Codigo do item do pedido */    
     FIELD ped    AS CHARACTER  /* Pedido de compra do cliente */                                 
     FIELD sPed   AS CHARACTER  /* Sigla do tipo de pedido */                                  
     FIELD alt    AS CHARACTER  /* Altera��o t�cnica do Item ou Seppen */                                  
     FIELD tpF    AS CHARACTER  /* Tipo de fornecimento */       
     FIELD uM     AS CHARACTER  /* Unidade de medida do cliente (NF) */       
     FIELD dVD    AS CHARACTER  /* Data de validade do desenho. Formato AAAAMMDD */       
     FIELD pedR   AS CHARACTER  /* Pedido de revenda */   
     FIELD pE     AS CHARACTER  /* C�digo do pa�s exportador */   
     FIELD psB    AS CHARACTER  /* Peso bruto */   
     FIELD psL    AS CHARACTER  /* Peso liquido */   
     FIELD tCH    AS CHARACTER  /* Tipo de chamada */   
     FIELD ch     AS CHARACTER  /* N�mero da chamada ou Slip Number */ 
     FIELD hCH    AS CHARACTER  /* Data-hora da chamada. Formato AAAAMMDDHHMM */ 
     FIELD qtEm   AS CHARACTER  /* Qtde de embalagens da chamada */ 
     FIELD qtIt   AS CHARACTER  /* Qtde de itens da chamada */ 
     FIELD dca    AS CHARACTER  /* Local de entrega ou doca */ 
     FIELD ptU    AS CHARACTER  /* Ponto de uso */ 
     FIELD ctrans AS CHARACTER  /* Tipo de transporte */ 
     FIELD ltP    AS CHARACTER  /* Lote de produ��o/corrida */ 
     FIELD cPI    AS CHARACTER  /* CPI (Controle Inicial do Produ��o) */ 
     FIELD nFE    AS CHARACTER  /* N�mero da NF de embalagem associada */ 
     FIELD sNF    AS CHARACTER  /* S�rie da NF de embalagem associada */ 
     FIELD cdEm   AS CHARACTER  /* C�digo da embalagem */ 
     FIELD aF     AS CHARACTER  /* Autoriza��o de faturamento */ 

     FIELD nrNFeRemessa AS CHARACTER
     FIELD srNFeRemessa AS CHARACTER
     FIELD dtNFeRemessa AS CHARACTER
     FIELD Conjunto     AS CHARACTER
     
     FIELD NFeRemessa   AS CHARACTER
  .
  
DEF VAR c-nr-remessa       AS CHAR NO-UNDO.
DEF VAR c-serie-remessa    AS CHAR NO-UNDO.
DEF VAR c-dt-remessa       AS CHAR NO-UNDO.
DEF VAR l-retorno-mercedes AS LOG  NO-UNDO.
DEF VAR i                  AS INT  NO-UNDO.
DEF VAR c-conjunto AS CHAR NO-UNDO.
DEF VAR c-item-cli-506     AS CHAR NO-UNDO.
DEF VAR c-chave            AS CHAR NO-UNDO.


DEF TEMP-TABLE tt-item
FIELD it-venda        AS CHAR
FIELD it-codigo-pai   AS CHAR
FIELD it-codigo-filho AS CHAR.

DEF NEW GLOBAL SHARED VAR gb-upbodi317pr-it-venda AS CHAR NO-UNDO.
 

/* ANFAVEA - InfCpl */
DEFINE TEMP-TABLE ttInfCpl NO-UNDO
     FIELD versao       AS CHARACTER  /* Vers�o do documento*/                                       
     FIELD codigo-trans AS CHARACTER  /* C�digo interno do transmissor */                                     
     FIELD nome-trans   AS CHARACTER  /* Nome do transmissor */                                            
     FIELD codigo-rec   AS CHARACTER  /* C�digo interno do receptor*/                                                  
     FIELD nome-rec     AS CHARACTER  /* Nome do receptor */                                         
     FIELD especieNF    AS CHARACTER  /* Esp�cie da NF */                                            
     FIELD fabEntrega   AS CHARACTER  /* C�digo da f�brica de entrega */                              
     FIELD prevEntrega  AS CHARACTER  /* Data previs�o de entrega. Informar no formato AAAAMMDD  */  
     FIELD Invoice      AS CHARACTER  /* N�mero da Invoice*/                                         
  .

/* ANFAVEA - CDATA */
DEFINE NEW GLOBAL SHARED TEMP-TABLE ttInfAdProdAux NO-UNDO
    FIELD nItem   AS CHAR
    FIELD cText   AS CHAR
    FIELD cCDATA  AS CHAR

    FIELD cPedido AS CHAR
    FIELD cTxxPed   AS CHAR
    FIELD cTxnItem  AS CHAR /*isolado Vanilda tags nFCI 08/10/2013*/
     .
      
IF p-ind-event = "AtualizaDadosNFe":U THEN DO:

  EMPTY TEMP-TABLE ttInfAdProdAux.
  EMPTY TEMP-TABLE ttInfCpl.

  /* ANFAVEA: Informa��es Adicionais dos Itens da NF */
  FOR FIRST tt-epc NO-LOCK
      WHERE tt-epc.cod-event     = "AtualizaDadosNFe":U
        AND tt-epc.cod-parameter = "ttDet":U :
  
      ASSIGN h-ttDet      = WIDGET-HANDLE(tt-epc.val-parameter)
             hQueryBuffer = h-ttDet:DEFAULT-BUFFER-HANDLE.
  
      IF  VALID-HANDLE(hQueryBuffer) THEN DO:
  
          CREATE QUERY h-query.
                       h-query:SET-BUFFERS(hQueryBuffer).
                       h-query:QUERY-PREPARE('for each ' + h-ttDet:NAME + ' no-lock').
                       h-query:QUERY-OPEN().
      
          REPEAT ON ERROR UNDO, LEAVE:
      
              h-query:GET-NEXT() NO-ERROR.
              IF h-query:QUERY-OFF-END THEN LEAVE.
  
              RUN pi-ttDet.

          END.

      END.

  END.

    
  /* ANFAVEA: Informa��es Complementares da NF */
  FOR FIRST tt-epc NO-LOCK
      WHERE tt-epc.cod-event     = "AtualizaDadosNFe":U
      AND   tt-epc.cod-parameter = "ttInfAdic":U :

    ASSIGN h-ttInfAdic      = WIDGET-HANDLE(tt-epc.val-parameter)
           hQueryBuffer = h-ttInfAdic:DEFAULT-BUFFER-HANDLE.

    IF  VALID-HANDLE(hQueryBuffer) THEN DO:

         CREATE QUERY h-query.
         h-query:SET-BUFFERS(hQueryBuffer).
         h-query:QUERY-PREPARE('for each ' + h-ttInfAdic:NAME + ' no-lock').
         h-query:QUERY-OPEN().

        REPEAT ON ERROR UNDO, LEAVE:
    
            h-query:GET-NEXT() NO-ERROR.
            IF h-query:QUERY-OFF-END THEN LEAVE.

            /* busca nota fiscal */
            IF NOT AVAIL nota-fiscal THEN DO:
                ASSIGN w-tot-base-icms-obs  = 0
                       de-tot-icms-obs      = 0
                       de-tot-icms-obs      = 0
                       de-tot-icmssubs-obs  = 0.
   
                ASSIGN h-cod-estabel = hQueryBuffer:BUFFER-FIELD('CodEstabelNF') NO-ERROR.
                       h-serie       = hQueryBuffer:BUFFER-FIELD('SerieNF') NO-ERROR.
                       h-nr-nota-fis = hQueryBuffer:BUFFER-FIELD('NrNotaFisNF') NO-ERROR.

               FIND bf-nota-fiscal WHERE
                    bf-nota-fiscal.cod-estabel = h-cod-estabel:BUFFER-VALUE  AND
                    bf-nota-fiscal.serie       = h-serie:BUFFER-VALUE        AND
                    bf-nota-fiscal.nr-nota-fis = h-nr-nota-fis:BUFFER-VALUE  NO-LOCK NO-ERROR.
               IF AVAIL bf-nota-fiscal THEN DO:

                   FIND estabelec OF bf-nota-fiscal NO-LOCK NO-ERROR.
                   FIND emitente  OF bf-nota-fiscal NO-LOCK NO-ERROR.

                   FIND FIRST bemitente NO-LOCK 
                        WHERE bemitente.cod-emitente = estabelec.cod-emitente NO-ERROR.

                   ASSIGN l-entrou = NO.
                   ASSIGN vcodigo-trans  = estabelec.cgc
                          vnome-trans    = IF AVAIL bemitente THEN bemitente.nome-emit ELSE ""
                          vcodigo-rec    = emitente.cgc
                          vnome-rec      = emitente.nome-emit.

                   FOR EACH bf-it-nota-fisc OF bf-nota-fiscal NO-LOCK,
                       EACH ITEM WHERE item.it-codigo = bf-it-nota-fisc.it-codigo NO-LOCK:

                       /* fasplas - 12/05/2004 */
                       ASSIGN wpedido = bf-it-nota-fisc.nr-pedcli.
                       IF wpedido <> "" AND l-entrou = NO THEN DO:
                           ASSIGN l-entrou = YES.
                           cObsNotaEsp = cObsNotaEsp + IF cObsNotaEsp <> "" THEN " " ELSE "" + "| Pedido: " + TRIM(wpedido).
                       END.      
                      /* VALERIA aqui colocar c�digo de f�brica */
                      /* incluido codigo de fabrica ford CONF. INF. JEOVA E REGIANE*/
                       IF bf-nota-fiscal.cod-emitente = 875  THEN ASSIGN cfabentrega = "30".  /* barueri*/
                       IF bf-nota-fiscal.cod-emitente = 917  THEN ASSIGN cfabentrega = "36".  /* cama�ari */
                       IF bf-nota-fiscal.cod-emitente = 1160 THEN ASSIGN cfabentrega = "35".  /* piraja*/
                       IF bf-nota-fiscal.cod-emitente = 1414 THEN ASSIGN cfabentrega = "03".  /*sbc*/.

                       /*IF bf-nota-fiscal.cod-emitente = 1414 
                       AND (bf-it-nota-fisc.it-codigo = "1016.9701.749"
                         OR bf-it-nota-fisc.it-codigo = "1016.9701.759") THEN ASSIGN cfabentrega = "49".  /* sbc/venezuela*/*/
                         
                       if item.fm-codigo = '228' then 
                          ASSIGN cfabentrega = "49".  /* sbc/venezuela*/                         

                      /*02/04/08 fastplas */
                       ASSIGN w-tot-base-icms-obs  = w-tot-base-icms-obs     + bf-it-nota-fisc.vl-bicms-it
                              de-tot-icms-obs      = de-tot-icms-obs         + bf-it-nota-fisc.vl-icms-it
                              de-tot-icms-obs      = de-tot-icms-obs         + bf-it-nota-fisc.vl-icmsub-it
                              de-tot-icmssubs-obs  = de-tot-icmssubs-obs     + bf-it-nota-fisc.vl-icmsub-it.

                   END. /* FOR EACH bf-it-nota-fisc */

                   IF  AVAIL estabelec
                   AND AVAIL emitente  
                   AND (estabelec.estado = "SP"
                   AND emitente.estado   = "SP")
                     OR emitente.estado   = "RS" THEN 
                   /* 02/04/08 FASTPLAS */  
                   cObsNotaEsp = cObsNotaEsp + IF cObsNotaEsp <> "" THEN " " ELSE ""
                                             + "| Base ICMS PROPRIO: " 
                                             + STRING(w-tot-base-icms-obs,">>>,>>>,>>9.99")
                                             + " Valor ICMS PROPRIO: "
                                             + string(de-tot-icms-obs - de-tot-icmssubs-obs, ">>>,>>>,>>9.99").

                   ASSIGN 
                       h-infcomp       = hQueryBuffer:BUFFER-FIELD('infCpl')
                       hQueryBuffer:BUFFER-FIELD('infCpl'):BUFFER-VALUE   = h-infcomp:BUFFER-VALUE + " - " + cInfCpl + IF cInfCpl <> "" THEN "   " ELSE "" + STRING(cObsNotaEsp).

               END. /* IF AVAIL bf-nota-fiscal */
                       
               FOR FIRST nota-fiscal NO-LOCK 
                   WHERE nota-fiscal.cod-estabel = h-cod-estabel:BUFFER-VALUE 
                     AND nota-fiscal.serie       = h-serie:BUFFER-VALUE 
                     AND nota-fiscal.nr-nota-fis = h-nr-nota-fis:BUFFER-VALUE:
               END.
            END. /* IF NOT AVAIL nota-fiscal */

            /* Grava��o dos dados adicionais - tag InfCpl (ANFAVEA) */
            EMPTY TEMP-TABLE ttInfCpl.
            CREATE ttInfCpl.
            ASSIGN ttInfCpl.versao       = "00"                      /* Vers�o do documento */                     
                   ttInfCpl.codigo-trans = vcodigo-trans             /* C�digo interno do transmissor */                  
                   ttInfCpl.nome-trans   = vnome-trans               /* Nome do transmissor */                      
                   ttInfCpl.codigo-rec   = vcodigo-rec               /* C�digo interno do receptor*/          
                   ttInfCpl.nome-rec     = vnome-rec                 /* Nome do receptor */                         
                   ttInfCpl.especieNF    = "00"                      /* Esp�cie da NF */            
                   ttInfCpl.fabEntrega   = cfabentrega             /* C�digo da f�brica de entrega */                  
                   ttInfCpl.prevEntrega  = ""                      /* Data previs�o de entrega. Informar no formato AAAAMMDD  */                            
                   ttInfCpl.Invoice      = ""                      /* N�mero da Invoice*/                    
            .

            /* Formata tag InfCpl conforme layout Anfavea.EDI.Msg.Extens�oNF-e.01 */
            ASSIGN cInfCpl = '<versao>' + ttInfCpl.versao + '</versao>'. /* dados obrigatorios */

                   /* dados opcionais */
                   IF ttInfCpl.codigo-trans <> "" OR 
                      ttInfCpl.nome-trans   <> "" THEN 
                      ASSIGN cInfCpl = cInfCpl + '<transmissor'
                             cInfCpl = cInfCpl + ' codigo="' + ttInfCpl.codigo-trans  + '"' WHEN ttInfCpl.codigo-trans <> ""
                             cInfCpl = cInfCpl + ' nome="'   + ttInfCpl.nome-trans    + '"' WHEN ttInfCpl.nome-trans <> ""
                             cInfCpl = cInfCpl + '/>'.

                   IF ttInfCpl.codigo-rec <> "" OR 
                      ttInfCpl.nome-rec   <> "" THEN 
                      ASSIGN cInfCpl = cInfCpl + '<receptor'
                             cInfCpl = cInfCpl + ' codigo="' + ttInfCpl.codigo-rec  + '"' WHEN ttInfCpl.codigo-rec <> ""
                             cInfCpl = cInfCpl + ' nome="'   + ttInfCpl.nome-rec    + '"' WHEN ttInfCpl.nome-rec <> ""
                             cInfCpl = cInfCpl + '/>'.

                   IF ttInfCpl.especieNF <> "" THEN 
                      ASSIGN cInfCpl = cInfCpl + '<especieNF>' + ttInfCpl.especieNF + '</especieNF>'.

                   IF ttInfCpl.fabEntrega <> "" THEN 
                      ASSIGN cInfCpl = cInfCpl + '<fabEntrega>' + ttInfCpl.fabEntrega + '</fabEntrega>'.

                   IF ttInfCpl.prevEntrega <> "" THEN 
                      ASSIGN cInfCpl = cInfCpl + '<prevEntrega>' + ttInfCpl.prevEntrega + '</prevEntrega>'.

                   IF ttInfCpl.Invoice <> "" THEN 
                      ASSIGN cInfCpl = cInfCpl + '<Invoice>' + ttInfCpl.Invoice + '</Invoice>'.

            ASSIGN h-campo = hQueryBuffer:BUFFER-FIELD('infCpl')
                   gc-infCpl        = h-campo:BUFFER-VALUE /* informa��es complementares da NF */
                   gc-infCplAnfavea = cInfCpl .  /* complemento ANFAVEA - tag infCPL */
         END.

      END.

   END.

END.


/* ANFAVEA */
IF p-ind-event = 'AtualizaXML' THEN
FOR FIRST tt-epc NO-LOCK
    WHERE tt-epc.cod-event     = "AtualizaXML":U
      AND tt-epc.cod-parameter = "hBusinessContent":U:
    
    ASSIGN h-doc = WIDGET-HANDLE(tt-epc.val-parameter).

    RUN pi-atualiza-xml-anfavea (INPUT-OUTPUT h-doc,
                                 INPUT TABLE ttInfAdProdAux,
                                 INPUT gc-infcpl,
                                 INPUT gc-infcplAnfavea).

    ASSIGN tt-epc.cod-parameter = "hBusinessContentANFAVEA":U
           tt-epc.val-parameter = STRING(h-doc).

END.

RETURN "OK":U.


/*-----------------------------------------------------------------------------------------*/

PROCEDURE pi-ttDet:


    DEF VAR h-cod-estabel  AS WIDGET-HANDLE NO-UNDO.
    DEF VAR h-serie        AS WIDGET-HANDLE NO-UNDO.
    DEF VAR h-nr-nota-fis  AS WIDGET-HANDLE NO-UNDO.
    DEF VAR h-it-codigo    AS WIDGET-HANDLE NO-UNDO.
    DEF VAR h-nr-seq-fat   AS WIDGET-HANDLE NO-UNDO.

    def var l-fm-228 as log no-undo.
    
    ASSIGN h-cod-estabel = hQueryBuffer:BUFFER-FIELD('CodEstabelNF') NO-ERROR.
    ASSIGN h-serie       = hQueryBuffer:BUFFER-FIELD('SerieNF')      NO-ERROR.
    ASSIGN h-nr-nota-fis = hQueryBuffer:BUFFER-FIELD('NrNotaFisNF')  NO-ERROR.
    ASSIGN h-it-codigo   = hQueryBuffer:BUFFER-FIELD('ItCodigoNF')   NO-ERROR.
    ASSIGN h-nr-seq-fat  = hQueryBuffer:BUFFER-FIELD('NrSeqFatNF')   NO-ERROR.

    ASSIGN cDescProd   = ''
           cped        = ''
           cford       = ''
           cinfadprod  = ''
           cItemCli    = ''
           cTpFor      = ''
           cUnidade    = ''
           vPosicao    = 0. 

    FOR FIRST bf-nota-fiscal NO-LOCK 
        WHERE bf-nota-fiscal.cod-estabel = h-cod-estabel:BUFFER-VALUE 
          AND bf-nota-fiscal.serie       = h-serie:BUFFER-VALUE      
          AND bf-nota-fiscal.nr-nota-fis = h-nr-nota-fis:BUFFER-VALUE:
                   
          
        l-retorno-mercedes = bf-nota-fiscal.cod-emitente = 506 AND
                             CAN-FIND(FIRST natur-oper
                                      WHERE natur-oper.nat-operacao = bf-nota-fiscal.nat-operacao
                                        AND natur-oper.terceiros    = YES).            
                              
        assign l-fm-228 = no.
                       
        FOR FIRST bf-it-nota-fisc NO-LOCK
            WHERE bf-it-nota-fisc.cod-estabel = h-cod-estabel:BUFFER-VALUE
              AND bf-it-nota-fisc.serie       = h-serie:BUFFER-VALUE
              AND bf-it-nota-fisc.nr-nota-fis = h-nr-nota-fis:BUFFER-VALUE 
              AND bf-it-nota-fisc.nr-seq-fat  = h-nr-seq-fat:BUFFER-VALUE 
              AND bf-it-nota-fisc.it-codigo   = h-it-codigo:BUFFER-VALUE:
    
            FIND FIRST ITEM NO-LOCK 
                 WHERE ITEM.it-codigo = bf-it-nota-fisc.it-codigo NO-ERROR.
            IF NOT AVAIL ITEM THEN NEXT.

            if item.fm-codigo = '228' then
               assign l-fm-228 = yes.

            FIND FIRST emitente NO-LOCK 
                 WHERE emitente.cod-emitente = bf-nota-fiscal.cod-emitente NO-ERROR.
            IF NOT AVAIL emitente THEN NEXT.
    


            /*  rotinas substituidas pelas logo abaixo
            
            IF bf-nota-fiscal.cod-emitente = 875  /* FORD */ 
            OR bf-nota-fiscal.cod-emitente = 917
            OR bf-nota-fiscal.cod-emitente = 1160
            OR bf-nota-fiscal.cod-emitente = 1414
            OR bf-nota-fiscal.cod-emitente = 4115 THEN DO:
               /* assinala informacoes  infaprod */
     
               ASSIGN cped = SUBSTRING(ITEM.inform-compl,3,10)
                      cped = STRING(cped).
    
               ASSIGN cItemCli = STRING(SUBSTRING(ITEM.codigo-refer,1,22))
                      cUnidade = LC(ITEM.un).
            END.
    
            /****** ESPECIFICO MERCEDES   30/01/10 ******/
            IF bf-nota-fiscal.cod-emitente = 2261 OR 
               bf-nota-fiscal.cod-emitente = 2263 THEN DO:
    
                FIND FIRST item-cli NO-LOCK 
                     WHERE item-cli.nome-abrev = bf-nota-fiscal.nome-ab-cli
                       AND item-cli.it-codigo  = bf-it-nota-fisc.it-codigo NO-ERROR.
                IF AVAIL item-cli THEN DO:
    
                   ASSIGN cItemCli = item-cli.item-do-cli.   /**** CODIGO DO ITEM DO CLIENTE */
    
                   ASSIGN vPosicao = INDEX(item-cli.narrativa,"P.",1).
                   IF  vPosicao = 0 THEN
                       ASSIGN vPosicao = INDEX(item-cli.narrativa,"P-",1).
    
                   IF vPosicao > 0 THEN
                      ASSIGN cped = SUBSTRING(item-cli.narrativa,(vposicao + 2),12). /*NUMERO DO PEDIDO */
    
                   ASSIGN cTpFor = "P". /* TIPO DE FORNECIMENTO */
    
                END.
            END.
            */




            IF emitente.cod-gr-cli = 10 THEN DO:  /* Cliente OEM - Montadora */ 

                ASSIGN cPed = "".
                /*** BUSCA PEDIDO DO CLIENTE NA TABELA:   ITEM-CLI ****/
                FIND FIRST item-cli NO-LOCK 
                     WHERE item-cli.nome-abrev = bf-nota-fiscal.nome-ab-cli
                       AND item-cli.it-codigo  = bf-it-nota-fisc.it-codigo NO-ERROR.
                IF AVAIL item-cli THEN DO:
     
                    ASSIGN cItemCli = item-cli.item-do-cli.   /**** CODIGO DO ITEM DO CLIENTE */
    
                    ASSIGN vPosicao = INDEX(item-cli.narrativa,"P.",1).
                    IF  vPosicao = 0 THEN 
                        ASSIGN vPosicao = INDEX(item-cli.narrativa,"P-",1).
    
                    IF vPosicao > 0 THEN
                       ASSIGN cped = SUBSTRING(item-cli.narrativa,(vposicao + 2),12) no-error. /*NUMERO DO PEDIDO */
                        
                   ASSIGN cItemCli = item-cli.item-do-cli
                          cUnidade = LC(item-cli.unid-med-cli).

                   ASSIGN cTpFor = "P". /* TIPO DE FORNECIMENTO */
    
                END.
 
                /*** BUSCA PEDIDO DO CLIENTE NO CAMPO:   ITEM.INFORM-COMPL ****/
                IF cPed = "" THEN DO:

                    ASSIGN cped = SUBSTRING(ITEM.inform-compl,3,10)
                           cped = STRING(cped).
 
                    ASSIGN cItemCli = STRING(SUBSTRING(ITEM.codigo-refer,1,22))
                           cUnidade = LC(ITEM.un).
 
                    ASSIGN cTpFor   = "P". /* TIPO DE FORNECIMENTO */
  
                END.


                DO iInd = 1 TO 12 :
                    IF SUBSTRING(cPed,iInd,1) < "0" 
                    OR SUBSTRING(cPed,iInd,1) > "9" THEN DO:
                        ASSIGN cPed = SUBSTRING(cPed,1,(iInd - 1)) no-error.
                        LEAVE.
                    END.
                END.
   
            END.
 
    
    
           /* fim - 22/09/09 - valeria - assinala pedido ---------------------------------------------------------*/
            IF  ITEM.ind-imp-desc =  3 THEN DO:
    
                FIND item-cli NO-LOCK 
                     WHERE item-cli.nome-abrev = bf-nota-fiscal.nome-ab-cli
                       AND item-cli.it-codigo  = bf-it-nota-fisc.it-codigo NO-ERROR.
    
    /*                 ASSIGN cford = "item=" + SUBSTRING(ITEM-cli.narrativa,1,20) + " ped= " +  cped           */
    /*                        + " uM= " + ITEM.un.                                                              */
    /*                                                                                                          */
    /*                 ASSIGN hBufTTDet:BUFFER-FIELD('infadprod'):BUFFER-VALUE   =  cinfadprod + " | " + cford. */
    /*                                                                                                          */
    
               /* 01/04/09 FASTPLAS - ALTERADO POR DOUGLAS DATASUL*/
                ASSIGN cDescProd = TRIM(SUBSTRING(item.desc-item,1,18))
                                      + " "
                                      + IF  AVAIL item-cli THEN
                                        SUBSTRING(item-cli.narrativa,1,40)
                                      ELSE "".
               /* 01/04/09 FASTPLAS - FIM ALTERACAO -------------------------------*/
           END.

           IF l-retorno-mercedes THEN DO:
             
                c-conjunto = "".                                                            
                /* carregar itens da estrutura dos itens de venda */                 
                        
                DO i = 1 TO NUM-ENTRIES(gb-upbodi317pr-it-venda):
                     FOR EACH estrutura
                         WHERE estrutura.it-codigo     = entry(i,gb-upbodi317pr-it-venda) 
                           AND   estrutura.data-inicio  <= TODAY 
                           AND   estrutura.data-termino >= TODAY  
                          NO-LOCK:
                                
                          RUN pi-estrutura(INPUT estrutura.it-codigo,
                                           INPUT estrutura.it-codigo,
                                           INPUT estrutura.es-codigo).
                                
                     END.
                     
                     IF CAN-FIND(FIRST natur-oper 
                                WHERE natur-oper.nat-operacao = bf-nota-fiscal.nat-operacao
                                  AND natur-oper.cod-cfop = "5949") THEN DO:
                       CREATE tt-item.
                       ASSIGN tt-item.it-venda        = gb-upbodi317pr-it-venda
                              tt-item.it-codigo-pai   = gb-upbodi317pr-it-venda
                              tt-item.it-codigo-filho = gb-upbodi317pr-it-venda.
                    END.                        
                END.                
                
                FOR EACH tt-item
                    WHERE tt-item.it-codigo-filho = bf-it-nota-fisc.it-codigo
                    BREAK BY tt-item.it-venda:                                                 
                   
                    IF LAST-OF(tt-item.it-venda) THEN DO: 
                  
                        FIND FIRST item-cli NO-LOCK 
                             WHERE item-cli.nome-abrev = bf-nota-fiscal.nome-ab-cli
                               AND item-cli.it-codigo  = tt-item.it-venda NO-ERROR.                                                      
                        
                        IF AVAIL item-cli THEN DO:
                       
                            IF INDEX(item-cli.narrativa,"xvv") > 0 THEN DO:                            
                                IF INDEX(c-conjunto,SUBSTR(item-cli.narrativa,INDEX(item-cli.narrativa,"xvv"),11)) = 0 THEN DO:                            
                                    IF c-conjunto = "" THEN
                                       c-conjunto =  SUBSTR(item-cli.narrativa,INDEX(item-cli.narrativa,"xvv"),11) NO-ERROR.
                                    ELSE
                                       c-conjunto =  c-conjunto + " " + SUBSTR(item-cli.narrativa,INDEX(item-cli.narrativa,"xvv"),11) NO-ERROR.
                                END.
                            END.
                            ELSE DO: 
                                /* se nao tem XVV na narrativa DO ITEM DO cliente */ 
                                c-item-cli-506 = "".
                                FIND FIRST bf-item
                                     WHERE bf-item.it-codigo = item-cli.it-codigo
                                     NO-LOCK NO-ERROR.
                                IF substr(bf-item.codigo-refer,1,1) = "N" THEN
                                   c-item-cli-506 = substr(bf-item.codigo-refer,1,13).
                                ELSE IF substr(bf-item.codigo-refer,1,1) = "A" THEN
                                   c-item-cli-506 = substr(bf-item.codigo-refer,1,11) + fill(" ",6) + substr(bf-item.codigo-refer,14,4).
                                ELSE IF substr(bf-item.codigo-refer,1,1) = "X" THEN
                                   c-item-cli-506 = substr(bf-item.codigo-refer,1,11).                               
                                
                                c-item-cli-506 = TRIM(c-item-cli-506). 
                                IF INDEX(c-conjunto,c-item-cli-506) = 0 THEN DO:  
                                    IF c-conjunto = "" THEN
                                        c-conjunto = c-item-cli-506.
                                    ELSE
                                        c-conjunto = c-conjunto + " " + c-item-cli-506.
                                END.
                            END.
                        END.
                    END.
                END.                    
                
                ASSIGN c-nr-remessa    = ""
                       c-serie-remessa = ""
                       c-dt-remessa    = ""
                       c-chave         = ""
                       .
                              
                FIND FIRST docum-est
                     WHERE docum-est.serie-docto   = bf-it-nota-fisc.serie-docum
                       AND docum-est.nro-docto     = bf-it-nota-fisc.nr-docum
                       AND docum-est.nat-operacao  = bf-it-nota-fisc.nat-docum
                       AND docum-est.cod-emitente  = bf-nota-fiscal.cod-emitente
                     NO-LOCK NO-ERROR.
                IF AVAIL docum-est THEN DO:                
                   ASSIGN c-nr-remessa    = c-nr-remessa    + docum-est.nro-docto       + " "
                          c-serie-remessa = c-serie-remessa + docum-est.serie-docto     + " "
                          c-dt-remessa    = c-dt-remessa    + string(YEAR(docum-est.dt-emis),"9999") + string(MONTH(docum-est.dt-emis),"99") + string(DAY(docum-est.dt-emis),"99") + " "
/*                          c-chave         = c-chave         + STRING(SUBSTR(docum-est.char-1,93,60), "99.9999.99.999.999/9999-99-99-999.999999999-999999999-99":U) + " "*/
                          c-chave         = c-chave         + SUBSTR(docum-est.char-1,93,60) + " "
                          .
                END.

           END.
           
           c-item-cli-506 = "".
           IF bf-nota-fiscal.cod-emitente = 506 THEN DO:
              IF substr(item.codigo-refer,1,1) = "N" THEN
                 c-item-cli-506 = substr(item.codigo-refer,1,13).
              ELSE IF substr(item.codigo-refer,1,1) = "A" THEN
                 c-item-cli-506 = substr(item.codigo-refer,1,11) + fill(" ",6) + substr(item.codigo-refer,14,4).
              ELSE IF substr(item.codigo-refer,1,1) = "X" THEN
                 c-item-cli-506 = substr(item.codigo-refer,1,11).             
           END.
           
           c-item-cli-506 = TRIM(c-item-cli-506).                        
    
        END. /* bf-it-nota-fisc */
        
        IF c-item-cli-506 <> "" THEN
           cItemCli = c-item-cli-506.          

        ASSIGN h-campo-desc = hQueryBuffer:BUFFER-FIELD('xProd') NO-ERROR.                    
    
        IF cDescProd <> "" THEN
           ASSIGN h-campo-desc:BUFFER-VALUE = TRIM(SUBSTRING(cDescProd,1,120)).

        IF (bf-nota-fiscal.cod-emitente = 875  OR
            bf-nota-fiscal.cod-emitente = 1518 OR
            bf-nota-fiscal.cod-emitente = 3519 OR  	
            bf-nota-fiscal.cod-emitente = 2263 OR
            bf-nota-fiscal.cod-emitente = 1159 OR
	        bf-nota-fiscal.cod-emitente = 8333 ) THEN ASSIGN cTpFor = "R".


        /* Grava��o dos dados adicionais - tag InfAdProd (ANFAVEA) */
        EMPTY TEMP-TABLE ttInfAdProd.
        CREATE ttInfAdProd.
        ASSIGN ttInfAdProd.item   = cItemCli   /* Codigo do item do pedido */      
               ttInfAdProd.ped    = LC(cped)   /* Pedido de compra do cliente */   
               ttInfAdProd.ped    = LC(cped)   /* preencher o m�nimo de 12 caracteres com zeros � esquerda */
               ttInfAdProd.sPed   = ""         /* Sigla do tipo de pedido */                      
               ttInfAdProd.alt    = ""         /* Altera��o t�cnica do Item ou Seppen */          
               ttInfAdProd.tpF    = cTpFor     /* Tipo de fornecimento */                         
               ttInfAdProd.uM     = cUnidade   /* Unidade de medida do cliente (NF) */            
               ttInfAdProd.dVD    = ""   /* Data de validade do desenho. Informar no formato AAAAMMDD */                  
               ttInfAdProd.pedR   = ""   /* Pedido de revenda */                            
               ttInfAdProd.pE     = ""   /* C�digo do pa�s exportador */                    
               ttInfAdProd.psB    = ""   /* Peso bruto. Ponto como separador quando aplic�vel */                                   
               ttInfAdProd.psL    = ""   /* Peso liquido. Ponto como separador quando aplic�vel  */                                 
               ttInfAdProd.tCH    = ""   /* Tipo de chamada */                              
               ttInfAdProd.ch     = ""   /* N�mero da chamada ou Slip Number */             
               ttInfAdProd.hCH    = ""   /* Data-hora da chamada. Informar no formato AAAAMMDDHHMM */                         
               ttInfAdProd.qtEm   = ""   /* Qtde de embalagens da chamada */                
               ttInfAdProd.qtIt   = ""   /* Qtde de itens da chamada. Ponto como separador quando aplic�vel  */                     
               ttInfAdProd.dca    = ""   /* Local de entrega ou doca */                     
               ttInfAdProd.ptU    = ""   /* Ponto de uso */                                 
               ttInfAdProd.ctrans = ""   /* Tipo de transporte */                           
               ttInfAdProd.ltP    = ""   /* Lote de produ��o/corrida */                     
               ttInfAdProd.cPI    = ""   /* CPI (Controle Inicial do Produ��o) */           
               ttInfAdProd.nFE    = ""   /* N�mero da NF de embalagem associada. Informar com zeros � esquerda */          
               ttInfAdProd.sNF    = ""   /* S�rie da NF de embalagem associada */           
               ttInfAdProd.cdEm   = ""   /* C�digo da embalagem. Usar ";" para separar, quando mais de uma embalagem */                          
               ttInfAdProd.aF     = ""   /* Autoriza��o de faturamento */                   
           .

        IF l-retorno-mercedes THEN DO:        
            ASSIGN ttInfAdProd.nrNFeRemessa = STRING(INT(c-nr-remessa))
                   ttInfAdProd.srNFeRemessa = c-serie-remessa     
                   ttInfAdProd.dtNFeRemessa = c-dt-remessa
                   ttInfAdProd.Conjunto     = c-conjunto
/*                   ttInfAdProd.tpF          = "F"*/
                   ttInfAdProd.NFeRemessa   = c-chave.
                   
/*            FIND natur-oper                                              */
/*              WHERE natur-oper.nat-operacao = bf-nota-fiscal.nat-operacao*/
/*              NO-LOCK NO-ERROR.                                          */
/*            IF AVAIL natur-oper                                          */
/*               AND natur-oper.cod-cfop = "5903" THEN                     */
/*                ASSIGN ttInfAdProd.tpF          = "O".                   */
        END.
/*        ELSE IF bf-nota-fiscal.cod-emitente = 506 THEN DO:*/
/*            ASSIGN ttInfAdProd.tpF          = "M".        */
/*        END.                                              */
        
        /* verificar tipo de fornecimento em tabela especifica para a Mercedes*/
        IF bf-nota-fiscal.cod-emitente = 506 THEN 
        DO:
          FIND natur-oper
            WHERE natur-oper.nat-operacao = bf-nota-fiscal.nat-operacao
            NO-LOCK NO-ERROR.
          
          IF AVAIL natur-oper THEN DO:
            FIND es-cfop-natur NO-LOCK
              WHERE es-cfop-natur.cod-cfop = natur-oper.cod-cfop
              NO-ERROR.
            IF AVAIL es-cfop-natur THEN 
              ASSIGN ttInfAdProd.tpF          = es-cfop-natur.tpfor.
          END.
        END.

        /* solicitado z� luiz - 24/09/2013 - rzo */
        /*IF bf-nota-fiscal.cod-emitente = 2263 them*/
  
    /*2.06.00.002 - Fernando Gon�alves - Totvs Ibirapuera - 22/05/2014*/

        IF (bf-nota-fiscal.cod-emitente = 875  OR
            bf-nota-fiscal.cod-emitente = 1518 OR
            bf-nota-fiscal.cod-emitente = 3519 OR  	
            bf-nota-fiscal.cod-emitente = 2263 OR
            bf-nota-fiscal.cod-emitente = 1159 OR
            bf-nota-fiscal.cod-emitente = 8333 ) THEN ASSIGN ttInfAdProd.tpF = "R".

        if l-fm-228 then
           ASSIGN ttInfAdProd.tpF = "E".
        
        /* Formata tag InfAdProd conforme layout Anfavea.EDI.Msg.Extens�oNF-e.01 */
        ASSIGN cInfAdProd = '<id item="' + ttInfAdProd.item + '" ped="' + ttInfAdProd.ped   /* dados obrigatorios */

               /* dados opcionais */
               cInfAdProd = cInfAdProd + '" sPed="'         + ttInfAdProd.sPed          WHEN ttInfAdProd.sPed          <> ""
               cInfAdProd = cInfAdProd + '" alt="'          + ttInfAdProd.alt           WHEN ttInfAdProd.alt           <> ""
               cInfAdProd = cInfAdProd + '" tpF="'          + ttInfAdProd.tpF           WHEN ttInfAdProd.tpF           <> "".

        IF l-retorno-mercedes THEN 
        ASSIGN cInfAdProd = cInfAdProd + '" nrNFeRemessa="' + trim(ttInfAdProd.nrNFeRemessa)  WHEN ttInfAdProd.nrNFeRemessa  <> ""
               cInfAdProd = cInfAdProd + '" srNFeRemessa="' + trim(ttInfAdProd.srNFeRemessa)  WHEN ttInfAdProd.srNFeRemessa  <> ""
               cInfAdProd = cInfAdProd + '" dtNFeRemessa="' + trim(ttInfAdProd.dtNFeRemessa)  WHEN ttInfAdProd.dtNFeRemessa  <> ""
               cInfAdProd = cInfAdProd + '" Conjunto="'     + trim(ttInfAdProd.Conjunto)      WHEN ttInfAdProd.Conjunto      <> ""
               cInfAdProd = cInfAdProd + '" chNFeRemessa="' + trim(ttInfAdProd.NFeRemessa)    WHEN ttInfAdProd.NFeRemessa    <> "".      
              
        ASSIGN cInfAdProd = cInfAdProd + '"/>'.
       

               /* div */
               IF ttInfAdProd.uM   <> "" OR 
                  ttInfAdProd.dVD  <> "" OR
                  ttInfAdProd.pedR <> "" OR 
                  ttInfAdProd.pE   <> "" OR 
                  ttInfAdProd.psB  <> "" OR 
                  ttInfAdProd.psL  <> "" THEN 
                  ASSIGN cInfAdProd = cInfAdProd + '<div'
                         cInfAdProd = cInfAdProd + ' uM="'   + ttInfAdProd.uM   + '"' WHEN ttInfAdProd.uM   <> ""
                         cInfAdProd = cInfAdProd + ' dVD="'  + ttInfAdProd.dVD  + '"' WHEN ttInfAdProd.dVD  <> ""
                         cInfAdProd = cInfAdProd + ' pedR="' + ttInfAdProd.pedR + '"' WHEN ttInfAdProd.pedR <> ""
                         cInfAdProd = cInfAdProd + ' pE="'   + ttInfAdProd.pE   + '"' WHEN ttInfAdProd.pE   <> ""
                         cInfAdProd = cInfAdProd + ' psB="'  + ttInfAdProd.psB  + '"' WHEN ttInfAdProd.psB  <> ""
                         cInfAdProd = cInfAdProd + ' psL="'  + ttInfAdProd.psL  + '"' WHEN ttInfAdProd.psL  <> ""
                         cInfAdProd = cInfAdProd + '/>'.

               /* entg */
               IF ttInfAdProd.tCH  <> "" OR 
                  ttInfAdProd.ch   <> "" OR
                  ttInfAdProd.hCH  <> "" OR 
                  ttInfAdProd.qtEm <> "" OR 
                  ttInfAdProd.qtIt <> "" THEN 
                  ASSIGN cInfAdProd = cInfAdProd + '<entg'
                         cInfAdProd = cInfAdProd + ' tCH="'  + ttInfAdProd.tCH   + '"' WHEN ttInfAdProd.tCH  <> ""
                         cInfAdProd = cInfAdProd + ' ch="'   + ttInfAdProd.ch    + '"' WHEN ttInfAdProd.ch   <> ""
                         cInfAdProd = cInfAdProd + ' hCH="'  + ttInfAdProd.hCH   + '"' WHEN ttInfAdProd.hCH  <> ""
                         cInfAdProd = cInfAdProd + ' qtEm="' + ttInfAdProd.qtEm  + '"' WHEN ttInfAdProd.qtEm <> ""
                         cInfAdProd = cInfAdProd + ' qtIt="' + ttInfAdProd.qtIt  + '"' WHEN ttInfAdProd.qtIt <> ""
                         cInfAdProd = cInfAdProd + '/>'.

               /* dest */
               IF ttInfAdProd.dca    <> "" OR 
                  ttInfAdProd.ptU    <> "" OR
                  ttInfAdProd.ctrans <> "" THEN 
                  ASSIGN cInfAdProd = cInfAdProd + '<dest'
                         cInfAdProd = cInfAdProd + ' dca="'    + ttInfAdProd.dca     + '"' WHEN ttInfAdProd.dca    <> ""
                         cInfAdProd = cInfAdProd + ' ptU="'    + ttInfAdProd.ptU     + '"' WHEN ttInfAdProd.ptU    <> ""
                         cInfAdProd = cInfAdProd + ' trans="'  + ttInfAdProd.ctrans  + '"' WHEN ttInfAdProd.ctrans <> ""
                         cInfAdProd = cInfAdProd + '/>'.

                /* ctl */
                IF ttInfAdProd.ltP <> "" OR 
                   ttInfAdProd.cPI <> "" THEN 
                   ASSIGN cInfAdProd = cInfAdProd + '<ctl'
                          cInfAdProd = cInfAdProd + ' ltP="'  + ttInfAdProd.ltP   + '"' WHEN ttInfAdProd.ltP <> ""
                          cInfAdProd = cInfAdProd + ' cPI="'  + ttInfAdProd.cPI   + '"' WHEN ttInfAdProd.cPI <> ""
                          cInfAdProd = cInfAdProd + '/>'.

                /* ref */
                IF ttInfAdProd.nFE  <> "" OR 
                   ttInfAdProd.sNF  <> "" OR 
                   ttInfAdProd.cdEm <> "" OR 
                   ttInfAdProd.aF   <> "" THEN 
                   ASSIGN cInfAdProd = cInfAdProd + '<ref'
                          cInfAdProd = cInfAdProd + ' nFE="'   + ttInfAdProd.nFE   + '"' WHEN ttInfAdProd.nFE <> ""
                          cInfAdProd = cInfAdProd + ' sNF="'   + ttInfAdProd.sNF   + '"' WHEN ttInfAdProd.sNF <> ""
                          cInfAdProd = cInfAdProd + ' cdEm="'  + ttInfAdProd.cdEm  + '"' WHEN ttInfAdProd.cdEm <> ""
                          cInfAdProd = cInfAdProd + ' aF="'    + ttInfAdProd.aF    + '"' WHEN ttInfAdProd.aF <> ""
                          cInfAdProd = cInfAdProd + '/>'.

        ASSIGN h-campo   = hQueryBuffer:BUFFER-FIELD('infAdProd') no-error. 
        ASSIGN h-cpxPed  = hQueryBuffer:BUFFER-FIELD('xPed') no-error. 
        ASSIGN h-cpnItem = hQueryBuffer:BUFFER-FIELD('nItemPed') no-error.
		
		
        ASSIGN h-cpxPed:BUFFER-VALUE = trim(ttInfAdProd.ped) no-error.
		ASSIGN h-cpnItem:BUFFER-VALUE = 1 no-error.

        CREATE ttInfAdProdAux.
        ASSIGN ttInfAdProdAux.nItem     = STRING(i-cont)
               ttInfAdProdAux.cText     = h-campo:BUFFER-VALUE /* informa��es adicionais do item */
               ttInfAdProdAux.cCDATA    = cInfAdProd           /* complemento ANFAVEA - tag infAdProd */
               
               /*ttInfAdProdAux.cPedido   = ttInfAdProd.ped
               ttInfAdProdAux.cTxxPed   = h-cpxPed :BUFFER-VALUE   informa��es adicionais do item 
               ttInfAdProdAux.cTxnItem  = h-cpnItem:BUFFER-VALUE   = 1 informa��es adicionais do item  Isolado Vanilda tags nFCI 08/10/2013*/
			   

               i-cont = i-cont + 1.

    END. /* nota-fiscal */

END PROCEDURE.





PROCEDURE pi-atualiza-xml-anfavea:
    
    DEFINE INPUT-OUTPUT PARAMETER h-doc AS HANDLE    NO-UNDO.
    DEFINE INPUT PARAMETER TABLE FOR ttInfAdProdAux. 
    DEFINE INPUT PARAMETER cInfCplText  AS CHARACTER NO-UNDO.
    DEFINE INPUT PARAMETER cInfCplCDATA AS CHARACTER NO-UNDO.
     
    DEFINE VARIABLE h-root    AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-node1   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-node2   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-node3   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-node4   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-node5   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-node6   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-nodeval AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-ctext   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-cdata   AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-infAdProd AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-infCpl    AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-infAdic   AS HANDLE NO-UNDO.
 
    DEFINE VARIABLE h-xPed      AS HANDLE NO-UNDO.
    DEFINE VARIABLE h-nItemPed  AS HANDLE NO-UNDO.
 

    DEFINE VAR iAux    AS INT NO-UNDO.
    DEFINE VAR i-cont  AS INT NO-UNDO.
    DEFINE VAR i-cont1 AS INT NO-UNDO.
    DEFINE VAR i-cont2 AS INT NO-UNDO.
    DEFINE VAR i-cont3 AS INT NO-UNDO.
    DEFINE VAR i-cont4 AS INT NO-UNDO.
    DEFINE VAR i-cont5 AS INT NO-UNDO.
    DEFINE VAR i-cont6 AS INT NO-UNDO.
    DEFINE VAR i-cont7 AS INT NO-UNDO.
    DEFINE VAR i-cont8 AS INT NO-UNDO.
    DEFINE VAR cnItem  AS CHAR NO-UNDO.
    
    DEF VAR l-InfAdProd AS LOGICAL NO-UNDO.
    DEF VAR l-InfCpl    AS LOGICAL NO-UNDO.
    DEF VAR l-InfAdic   AS LOGICAL NO-UNDO.

    DEF VAR l-xPed      AS LOGICAL NO-UNDO.
    DEF VAR l-nItemPed  AS LOGICAL NO-UNDO.
      

    CREATE X-NODEREF h-root.
    CREATE X-NODEREF h-node1.
    CREATE X-NODEREF h-node2.
    CREATE X-NODEREF h-node3.
    CREATE X-NODEREF h-node4.
    CREATE X-NODEREF h-node5.
    CREATE X-NODEREF h-node6.
    CREATE X-NODEREF h-nodeval.
    CREATE X-NODEREF h-ctext.
    CREATE X-NODEREF h-cdata.
    
    h-doc:GET-DOCUMENT-ELEMENT(h-root).
    
    IF VALID-HANDLE(h-root) THEN DO:
                
        REPEAT i-cont1 = 1 TO h-root:NUM-CHILDREN:

            h-root:GET-CHILD(h-node1,i-cont1) NO-ERROR.
          
            IF h-node1:SUBTYPE = "ELEMENT" AND h-node1:NAME = "NFe" THEN DO:
                  
                REPEAT i-cont4 = 1 TO h-node1:NUM-CHILDREN:
                    
                    h-node1:GET-CHILD(h-node2, i-cont4) NO-ERROR.
                    
                    IF h-node2:SUBTYPE = "ELEMENT" AND h-node2:NAME = "infNFe" THEN DO:
                            
                        REPEAT i-cont5 = 1 TO h-node2:NUM-CHILDREN:
                                                              
                            h-node2:GET-CHILD(h-node3, i-cont5) NO-ERROR.
    
                            IF h-node3:SUBTYPE = "ELEMENT" AND h-node3:NAME = "InfAdic" THEN DO:
    
                                ASSIGN l-infAdic = YES.
    
                                REPEAT i-cont6 = 1 TO h-node3:NUM-CHILDREN:

                                    h-node3:GET-CHILD(h-node4, i-cont6) NO-ERROR.

                                    IF h-node4:SUBTYPE = "ELEMENT" AND h-node4:NAME = "infCpl" THEN DO:

                                        ASSIGN l-infCpl = YES.
                                        iAux = h-node4:NUM-CHILDREN.
                                        DO i-cont = 1 TO iAux:
                                            h-node4:GET-CHILD(h-nodeval,1).
                                            h-node4:REMOVE-CHILD(h-nodeval).
                                        END.
                                        h-node4:GET-CHILD(h-nodeval,1)  NO-ERROR.
                                        h-node4:REMOVE-CHILD(h-nodeval) NO-ERROR.
                                        CREATE X-NODEREF h-ctext.
                                        CREATE X-NODEREF h-cdata.
                                        h-doc:CREATE-NODE(h-ctext, ?, "Text"). 
                                        h-doc:CREATE-NODE(h-cdata, ?, "cdata-section"). 
                                        h-ctext:NODE-VALUE = cInfCplText.
                                        h-cdata:NODE-VALUE = cInfCplCDATA.
                                        h-node4:APPEND-CHILD(h-ctext).
                                        h-node4:APPEND-CHILD(h-cdata).
                                        DELETE OBJECT h-ctext.
                                        DELETE OBJECT h-cdata.

                                    END.

                                END.
    
                                IF l-infCpl = NO THEN DO:  /* cria tag infCpl */

                                    CREATE X-NODEREF h-infCpl.
                                    h-doc:CREATE-NODE(h-infCpl, "infCpl", "Element").
                                    h-node3:APPEND-CHILD(h-infCpl).
                               
                                    CREATE X-NODEREF h-ctext.
                                    CREATE X-NODEREF h-cdata.
                                    h-doc:CREATE-NODE(h-ctext, ?, "Text"). 
                                    h-doc:CREATE-NODE(h-cdata, ?, "cdata-section"). 
                                    h-ctext:NODE-VALUE = cInfCplText.
                                    h-cdata:NODE-VALUE = cInfCplCDATA.
                                    h-infCpl:APPEND-CHILD(h-ctext).
                                    h-infCpl:APPEND-CHILD(h-cdata).
                                    DELETE OBJECT h-ctext.
                                    DELETE OBJECT h-cdata.
                                    
                                END.
    
                            END.
                                
                            IF h-node3:SUBTYPE = "ELEMENT" AND h-node3:NAME = "det" THEN DO:

                                ASSIGN cnItem = h-node3:GET-ATTRIBUTE("nItem") NO-ERROR.
                                      
                                REPEAT i-cont6 = 1 TO h-node3:NUM-CHILDREN:
                                  
                                    h-node3:GET-CHILD(h-node4, i-cont6) NO-ERROR.

 
                                 /* ALTERA  prod -  Ramon 30/05/2012  - Somente quando existir pedido - Ramon 31/05/2012 */

                                    FIND FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem NO-LOCK NO-ERROR.

                                    /*IF AVAIL ttInfAdProdAux 
                                    AND ttInfAdProdAux.cPedido <> "" THEN DO:
     
                                         IF h-node4:SUBTYPE = "ELEMENT" AND h-node4:NAME = "prod" THEN DO:  /* altera tag prod */
     
                                            REPEAT i-cont7 = 1 TO h-node4:NUM-CHILDREN:
    
                                                h-node4:GET-CHILD(h-node5, i-cont7) NO-ERROR.
                                            
                                             /* ALTERA xPed */
                                                IF h-node5:SUBTYPE = "ELEMENT" AND h-node5:NAME = "xPed" THEN DO:  /* altera tag xPed */
                                                    
                                                    ASSIGN l-xPed = YES.
                                                    
                                                    FOR FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem:
                                                    
                                                        iAux = h-node5:NUM-CHILDREN.
                                                        DO i-cont = 1 TO iAux:
                                                            h-node5:GET-CHILD(h-nodeval,1).
                                                            h-node5:REMOVE-CHILD(h-nodeval).
                                                        END.
                                                        CREATE X-NODEREF h-ctext.
                                                        CREATE X-NODEREF h-cdata.
                                                        h-doc:CREATE-NODE(h-ctext, ?, "Text").
                                                        h-doc:CREATE-NODE(h-cdata, ?, "Text").   
                                                        h-ctext:NODE-VALUE = ttInfAdProdAux.cTxxPed.
                                                        h-cdata:NODE-VALUE = ttInfAdProdAux.cPedido.    
                                                        h-node5:APPEND-CHILD(h-ctext).
                                                        h-node5:APPEND-CHILD(h-cdata).     
                                                        DELETE OBJECT h-ctext.
                                                        DELETE OBJECT h-cdata.
             
                                                    END.
                                                    REPEAT i-cont8 = 1 TO h-node5:NUM-CHILDREN:
                                                        h-node5:GET-CHILD(h-node6, i-cont7) NO-ERROR.
                                                    END.
            
                                                END.
     
                                             /* ALTERA nItemPed */
                                                IF h-node5:SUBTYPE = "ELEMENT" AND h-node5:NAME = "nItemPed" THEN DO:  /* altera tag xPed */
                                                    
                                                    ASSIGN l-nItemPed = YES.
                                                    
                                                    FOR FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem:
                                                    
                                                        iAux = h-node5:NUM-CHILDREN.
                                                        DO i-cont = 1 TO iAux:
                                                            h-node5:GET-CHILD(h-nodeval,1).
                                                            h-node5:REMOVE-CHILD(h-nodeval).
                                                        END.
                                                        CREATE X-NODEREF h-ctext.
                                                        CREATE X-NODEREF h-cdata.
                                                        h-doc:CREATE-NODE(h-ctext, ?, "Text").
                                                        h-doc:CREATE-NODE(h-cdata, ?, "Text").   
                                                        h-ctext:NODE-VALUE = ttInfAdProdAux.cTxnItem.   
                                                        h-cdata:NODE-VALUE = "1".    
                                                        h-node5:APPEND-CHILD(h-ctext).
                                                        h-node5:APPEND-CHILD(h-cdata).     
                                                        DELETE OBJECT h-ctext.
                                                        DELETE OBJECT h-cdata.
            
                                                    END.
                                                    REPEAT i-cont8 = 1 TO h-node5:NUM-CHILDREN:
                                                        h-node5:GET-CHILD(h-node6, i-cont7) NO-ERROR.
                                                    END.
            
                                                END.
       
                                            END.
      
    
                                         /* CRIA TAG xPed */
                                            IF l-xPed = NO THEN DO:  
                                                 CREATE X-NODEREF h-xPed.
                                                 h-doc:CREATE-NODE(h-xPed, "xPed", "Element").
                                                 h-node4:APPEND-CHILD(h-xPed).
            
                                                 FOR FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem:
                                                     ASSIGN h-node5 = h-xPed.
                                                     iAux = h-node5:NUM-CHILDREN.
                                                     DO i-cont = 1 TO iAux:
                                                         h-node5:GET-CHILD(h-nodeval,1).
                                                         h-node5:REMOVE-CHILD(h-nodeval).
                                                     END.
                                                     CREATE X-NODEREF h-ctext.
                                                     CREATE X-NODEREF h-cdata.   
                                                     h-doc:CREATE-NODE(h-ctext, ?, "Text").
                                                     h-doc:CREATE-NODE(h-cdata, ?, "Text").     
                                                     h-ctext:NODE-VALUE = ttInfAdProdAux.cTxxPed.
                                                     h-cdata:NODE-VALUE = ttInfAdProdAux.cPedido.     
                                                     h-node5:APPEND-CHILD(h-ctext).
                                                     h-node5:APPEND-CHILD(h-cdata).                
                                                     DELETE OBJECT h-ctext.
                                                     DELETE OBJECT h-cdata.   
                                                 END.
                                            END.
              
     
                                         /* CRIA TAG nItemPed */
                                            IF l-nItemPed = NO THEN DO:  
                                                CREATE X-NODEREF h-nItemPed.
                                                h-doc:CREATE-NODE(h-nItemPed, "nItemPed", "Element").
                                                h-node4:APPEND-CHILD(h-nItemPed).
                                                        
                                                FOR FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem:
                                                    ASSIGN h-node5 = h-nItemPed.
                                                    iAux = h-node5:NUM-CHILDREN.
                                                    DO i-cont = 1 TO iAux:
                                                        h-node5:GET-CHILD(h-nodeval,1).
                                                        h-node5:REMOVE-CHILD(h-nodeval).
                                                    END.
                                                    CREATE X-NODEREF h-ctext.
                                                    CREATE X-NODEREF h-cdata.                        
                                                    h-doc:CREATE-NODE(h-ctext, ?, "Text").
                                                    h-doc:CREATE-NODE(h-cdata, ?, "Text").    
                                                    h-ctext:NODE-VALUE = ttInfAdProdAux.cTxnItem.   
                                                    h-cdata:NODE-VALUE = "1".           
                                                    h-node5:APPEND-CHILD(h-ctext).
                                                    h-node5:APPEND-CHILD(h-cdata).     
                                                    DELETE OBJECT h-ctext.
                                                    DELETE OBJECT h-cdata.     
                                                END.
                                            END.
      
                                        END.  /* prod */
                                            
                                    END.  /* IF ttInfAdProdAux.cPedido <> "" */ ISOLADO Vanilda tags nFCI 08/10/2013 */
 
 
                                 /* ALTERA infAdProd */
                                    IF h-node4:SUBTYPE = "ELEMENT" AND h-node4:NAME = "infAdProd" THEN DO:  /* altera tag InfAdProd */
                                        
                                        ASSIGN l-InfAdProd = YES.
                                        
                                        FOR FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem:
                                        
                                            iAux = h-node4:NUM-CHILDREN.
                                            DO i-cont = 1 TO iAux:
                                                h-node4:GET-CHILD(h-nodeval,1).
                                                h-node4:REMOVE-CHILD(h-nodeval).
                                            END.
                                            CREATE X-NODEREF h-ctext.
                                            CREATE X-NODEREF h-cdata.
                                            h-doc:CREATE-NODE(h-ctext, ?, "Text").
                                            h-doc:CREATE-NODE(h-cdata, ?, "cdata-section").
                                            h-ctext:NODE-VALUE = ttInfAdProdAux.cText.
                                            h-cdata:NODE-VALUE = ttInfAdProdAux.cCDATA.
                                            h-node4:APPEND-CHILD(h-ctext).
                                            h-node4:APPEND-CHILD(h-cdata).
                                            DELETE OBJECT h-ctext.
                                            DELETE OBJECT h-cdata.

                                        END.
                                        REPEAT i-cont7 = 1 TO h-node4:NUM-CHILDREN:
                                            h-node4:GET-CHILD(h-node5, i-cont6) NO-ERROR.
                                        END.

                                    END.
      
                                END.
  

                             /* CRIA TAG InfAdProd */
                                IF l-InfAdProd = NO THEN DO:  

                                    CREATE X-NODEREF h-infAdProd.
                                    h-doc:CREATE-NODE(h-infAdProd, "infAdProd", "Element").
                                    h-node3:APPEND-CHILD(h-infAdProd).
                                            
                                    FOR FIRST ttInfAdProdAux WHERE ttInfAdProdAux.nItem = cnItem:

                                        ASSIGN h-node4 = h-infAdProd.
                                        iAux = h-node4:NUM-CHILDREN.
                                        DO i-cont = 1 TO iAux:
                                            h-node4:GET-CHILD(h-nodeval,1).
                                            h-node4:REMOVE-CHILD(h-nodeval).
                                        END.
                                        CREATE X-NODEREF h-ctext.
                                        CREATE X-NODEREF h-cdata.
                                        h-doc:CREATE-NODE(h-ctext, ?, "Text").
                                        h-doc:CREATE-NODE(h-cdata, ?, "cdata-section").
                                        h-ctext:NODE-VALUE = ttInfAdProdAux.cText.
                                        h-cdata:NODE-VALUE = ttInfAdProdAux.cCDATA.
                                        h-node4:APPEND-CHILD(h-ctext).
                                        h-node4:APPEND-CHILD(h-cdata).
                                        DELETE OBJECT h-ctext.
                                        DELETE OBJECT h-cdata.
                                    END.

                                END.
    
                            END.  /* Det */

                        END.
    
                        IF l-infAdic = NO THEN DO:  /* cria tag infAdic */

                            CREATE X-NODEREF h-infAdic.
                            h-doc:CREATE-NODE(h-infAdic, "infAdic", "Element").
                            h-node2:APPEND-CHILD(h-infAdic).
    
                            CREATE X-NODEREF h-infCpl.   /* cria tag infCpl */
                            h-doc:CREATE-NODE(h-infCpl, "infCpl", "Element").
                            h-infAdic:APPEND-CHILD(h-infCpl).
    
                            CREATE X-NODEREF h-ctext.
                            CREATE X-NODEREF h-cdata.
                            h-doc:CREATE-NODE(h-ctext, ?, "Text"). 
                            h-doc:CREATE-NODE(h-cdata, ?, "cdata-section"). 
                            h-ctext:NODE-VALUE = cInfCplText.
                            h-cdata:NODE-VALUE = cInfCplCDATA.
                            h-infCpl:APPEND-CHILD(h-ctext).
                            h-infCpl:APPEND-CHILD(h-cdata).
                            DELETE OBJECT h-ctext.
                            DELETE OBJECT h-cdata.

                        END.
                    END.
                END.
            END.
        END.
    END.
    
    IF VALID-HANDLE(h-root) THEN
        DELETE OBJECT h-root.
    IF VALID-HANDLE(h-node1) THEN
        DELETE OBJECT h-node1.
    IF VALID-HANDLE(h-node2) THEN
        DELETE OBJECT h-node2.
    IF VALID-HANDLE(h-node1) THEN
        DELETE OBJECT h-node1.
    IF VALID-HANDLE(h-node2) THEN
        DELETE OBJECT h-node2.
    IF VALID-HANDLE(h-node3) THEN
        DELETE OBJECT h-node3.
    IF VALID-HANDLE(h-node4) THEN
        DELETE OBJECT h-node4.
    IF VALID-HANDLE(h-nodeval) THEN
        DELETE OBJECT h-nodeval.
    IF VALID-HANDLE(h-ctext) THEN
        DELETE OBJECT h-cdata.

END PROCEDURE.

PROCEDURE pi-estrutura.
     DEF INPUT PARAM pc-item-venda AS CHAR NO-UNDO.
     DEF INPUT PARAM pc-item-pai   AS CHAR NO-UNDO.
     DEF INPUT PARAM pc-item-filho AS CHAR NO-UNDO.

     IF NOT CAN-FIND(FIRST tt-item
                     WHERE tt-item.it-venda        = pc-item-venda
                       AND tt-item.it-codigo-pai   = pc-item-pai
                       AND tt-item.it-codigo-filho = pc-item-filho)
     THEN DO:
         CREATE tt-item.
         ASSIGN tt-item.it-venda        = pc-item-venda
                tt-item.it-codigo-pai   = pc-item-pai.
                tt-item.it-codigo-filho = pc-item-filho.
     END.
            
     FOR EACH estrutura
           WHERE estrutura.it-codigo     = pc-item-filho  /*
           and   estrutura.data-inicio  <= TODAY 
           and   estrutura.data-termino >= TODAY  */
           NO-LOCK:
           
           RUN pi-estrutura(INPUT pc-item-venda,
                            INPUT estrutura.it-codigo,
                            INPUT estrutura.es-codigo).
     END.

END.
    
/*----------------------------------------- FIM axsep017-upc-anfvea.p -----------------------------------------*/

