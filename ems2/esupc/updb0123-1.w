&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{esp\buffers.i}

{include/i-prgvrs.i updb0123-1 2.06.00.000}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR i-time-ini      AS INTEGER.
DEF VAR i-time-fin      AS INTEGER.
DEF VAR i-time          AS INTEGER.
DEF VAR c-nr-hrs        AS CHARACTER    FORMAT "x(08)".
DEF VAR i-nr-dias        AS INTEGER.
define new global shared var w-rowid-recur-sec-dbr   AS Rowid          No-undo.
DEF NEW GLOBAL  SHARED  VAR wh-updb0123-browse             AS WIDGET-HANDLE    NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rt-buttom RECT-2 dt-ini-periodo ~
hr-ini-periodo dt-fim-periodo hr-fim-periodo Qtde-disponivel bt-ok ~
bt-cancela bt-ajuda 
&Scoped-Define DISPLAYED-OBJECTS dt-ini-periodo hr-ini-periodo ~
dt-fim-periodo hr-fim-periodo Qtde-disponivel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ajuda 
     LABEL "&Ajuda" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-cancela AUTO-END-KEY 
     LABEL "&Cancelar" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-ok AUTO-GO 
     LABEL "&OK" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE dt-fim-periodo AS DATE FORMAT "99/99/9999":U 
     LABEL "Fim do Per�odo" 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY .88 NO-UNDO.

DEFINE VARIABLE dt-ini-periodo AS DATE FORMAT "99/99/9999":U 
     LABEL "In�cio do Per�odo" 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY .88 NO-UNDO.

DEFINE VARIABLE hr-fim-periodo AS CHARACTER FORMAT "99:99":U INITIAL "2400" 
     VIEW-AS FILL-IN 
     SIZE 5.57 BY .88 NO-UNDO.

DEFINE VARIABLE hr-ini-periodo AS CHARACTER FORMAT "99:99":U INITIAL "0000" 
     VIEW-AS FILL-IN 
     SIZE 5.57 BY .88 NO-UNDO.

DEFINE VARIABLE Qtde-disponivel AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Qtde Dispon�vel" 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 68 BY 5.

DEFINE RECTANGLE rt-buttom
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 68 BY 1.42
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     dt-ini-periodo AT ROW 2.46 COL 19 COLON-ALIGNED WIDGET-ID 2
     hr-ini-periodo AT ROW 2.46 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     dt-fim-periodo AT ROW 3.5 COL 19 COLON-ALIGNED WIDGET-ID 4
     hr-fim-periodo AT ROW 3.5 COL 32 COLON-ALIGNED NO-LABEL WIDGET-ID 12
     Qtde-disponivel AT ROW 4.5 COL 19 COLON-ALIGNED WIDGET-ID 6
     bt-ok AT ROW 6.75 COL 3
     bt-cancela AT ROW 6.75 COL 14
     bt-ajuda AT ROW 6.75 COL 59
     "Data" VIEW-AS TEXT
          SIZE 4 BY .54 AT ROW 1.71 COL 24 WIDGET-ID 16
     "Hora" VIEW-AS TEXT
          SIZE 4 BY .54 AT ROW 1.71 COL 35 WIDGET-ID 14
     rt-buttom AT ROW 6.5 COL 2
     RECT-2 AT ROW 1.25 COL 2 WIDGET-ID 18
     SPACE(0.85) SKIP(1.74)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "<insert SmartDialog title>"
         DEFAULT-BUTTON bt-ok WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME L-To-R                                                    */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* <insert SmartDialog title> */
DO:  
  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda D-Dialog
ON CHOOSE OF bt-ajuda IN FRAME D-Dialog /* Ajuda */
OR HELP OF FRAME {&FRAME-NAME}
DO: /* Call Help Function (or a simple message). */
  {include/ajuda.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok D-Dialog
ON CHOOSE OF bt-ok IN FRAME D-Dialog /* OK */
DO:
     ASSIGN INPUT FRAME D-Dialog dt-fim-periodo 
           INPUT FRAME D-Dialog dt-ini-periodo 
           INPUT FRAME D-Dialog hr-fim-periodo 
           INPUT FRAME D-Dialog hr-ini-periodo
           INPUT FRAME d-dialog Qtde-disponivel.
    IF dt-ini-periodo = ? THEN DO:
         run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Data inicial n�o informada!~~Favor informar uma data inicial para processar recurso secund�rio").
         APPLY 'ENTRY' TO dt-ini-periodo IN FRAME d-dialog.
         RETURN NO-APPLY.
    END.
    IF dt-fim-periodo= ? THEN DO:
        run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Data final n�o informada!~~Favor informar uma data final para processar recurso secund�rio").
         APPLY 'ENTRY' TO dt-fim-periodo IN FRAME d-dialog.
         RETURN NO-APPLY.
    END.
    IF hr-ini-periodo = ? THEN DO:
         run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Hora inicial n�o informada!~~Favor informar uma hora inicial para processar recurso secund�rio").
         APPLY 'ENTRY' TO hr-ini-periodo IN FRAME d-dialog.
         RETURN NO-APPLY.
    END.
    IF hr-fim-periodo = ? THEN DO:
         run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Hora final n�o informada!~~Favor informar uma hora final para processar recurso secund�rio").
         APPLY 'ENTRY' TO hr-fim-periodo IN FRAME d-dialog.
         RETURN NO-APPLY.
    END.
    IF dt-ini-periodo > dt-fim-periodo THEN
    DO:
        run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Data inicial inv�lida!~~Data inicial maior que a data final para processar recurso secund�rio").
         APPLY 'ENTRY' TO dt-ini-periodo IN FRAME d-dialog.
         RETURN NO-APPLY.
    END.
    IF int(hr-ini-periodo) > int(hr-fim-periodo) THEN DO:
         run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Hora inicial inv�lida!~~Hora inicial maior que a hora inicial para processar recurso secund�rio").
         APPLY 'ENTRY' TO hr-ini-periodo IN FRAME d-dialog.
         RETURN NO-APPLY.
    END.
    RUN pi-calcula-tempo.
    FIND FIRST recur-sec-dbr NO-LOCK
         WHERE ROWID(recur-sec-dbr) = w-rowid-recur-sec-dbr NO-ERROR.
    RUN utp/ut-msgs.p (INPUT "show":U, 
                       INPUT 27100, 
                       INPUT "Gera��o de manuten��o recurso secund�rio dispon�vel?~~" +
                             "Ser�o gerados " + string(i-nr-dias) + " dias com o tempo das " +
                             string(i-time-ini,"HH:MM") + " �s " + string(i-time-fin,"hh:mm") + " para o recurso " + STRING(recur-sec-dbr.cod-recurso) + CHR(10) +
                             "Confirma a gera��o de manuten��o de recurso secund�rio dispon�vel conforme a parametriza��o informada na tela?"  ).
    IF  RETURN-VALUE = "YES":U THEN DO:
        RUN pi-gera-dispon-recurso.
        IF RETURN-VALUE = "NOK" THEN
            RETURN NO-APPLY.
    END.
    
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY dt-ini-periodo hr-ini-periodo dt-fim-periodo hr-fim-periodo 
          Qtde-disponivel 
      WITH FRAME D-Dialog.
  ENABLE rt-buttom RECT-2 dt-ini-periodo hr-ini-periodo dt-fim-periodo 
         hr-fim-periodo Qtde-disponivel bt-ok bt-cancela bt-ajuda 
      WITH FRAME D-Dialog.
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "updb0123-1" "2.06.00.000"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-calcula-tempo D-Dialog 
PROCEDURE pi-calcula-tempo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   
           
    ASSIGN i-time-ini = (INT(SUBSTRING(hr-ini-periodo,1,2)) * 3600) + 
                        (INT(SUBSTRING(hr-ini-periodo,3,2)) * 60  ) + 
                         INT(SUBSTRING(hr-ini-periodo,5,2)).
    ASSIGN i-time-fin = (INT(SUBSTRING(hr-fim-periodo,1,2)) * 3600) + 
                        (INT(SUBSTRING(hr-fim-periodo,3,2)) * 60  ) + 
                         INT(SUBSTRING(hr-fim-periodo,5,2)).
    
    ASSIGN c-nr-hrs     = STRING(i-time-fin - i-time-ini,"hh:mm:ss")
           i-nr-dias    = (dt-fim-periodo - dt-ini-periodo) + 1.
    
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-gera-dispon-recurso D-Dialog 
PROCEDURE pi-gera-dispon-recurso :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEFINE VARIABLE i-dt-registro AS DATE     NO-UNDO.
    ASSIGN i-dt-registro = ?.
    FIND FIRST recur-sec-dbr NO-LOCK
         WHERE ROWID(recur-sec-dbr) = w-rowid-recur-sec-dbr NO-ERROR.
    IF AVAIL recur-sec-dbr THEN
    do:
        DO i-dt-registro = dt-ini-periodo TO dt-fim-periodo.
            IF CAN-FIND(FIRST recur-sec-dispon-dbr NO-LOCK
                        WHERE recur-sec-dispon-dbr.cod-recurso     = recur-sec-dbr.cod-recurso 
                        AND   recur-sec-dispon-dbr.dat-inic-period <= i-dt-registro
                        AND   recur-sec-dispon-dbr.dat-fim-period  >= i-dt-registro
                        /*AND   recur-sec-dispon-dbr.qtd-segs-inic   <= DEC(i-time-ini)
                        AND   recur-sec-dispon-dbr.qtd-segs-fim    >= DEC(i-time-fin)*/) OR
                CAN-FIND(FIRST recur-sec-dispon-dbr NO-LOCK
                        WHERE recur-sec-dispon-dbr.cod-recurso     = recur-sec-dbr.cod-recurso 
                        AND   recur-sec-dispon-dbr.dat-fim-period  = i-dt-registro
                        AND   recur-sec-dispon-dbr.qtd-segs-inic   <= DEC(i-time-ini)
                        AND   recur-sec-dispon-dbr.qtd-segs-fim    >= DEC(i-time-fin)) THEN
            DO:
                run utp/ut-msgs.p (input "show", 
                          input 17006, 
                          input "Os per�odos n�o podem se sobrepor!~~As datas ou as horas informadas est�o sobrepondo um per�odo j� cadastrado.").
                RETURN "nok".
            END.
        END.
        ASSIGN i-dt-registro = ?.
        DO i-dt-registro = dt-ini-periodo TO dt-fim-periodo.
            FIND FIRST recur-sec-dispon-dbr NO-LOCK
                 WHERE recur-sec-dispon-dbr.cod-recurso     = recur-sec-dbr.cod-recurso 
                 AND   recur-sec-dispon-dbr.dat-inic-period = i-dt-registro
                 AND   recur-sec-dispon-dbr.dat-fim-period  = i-dt-registro
                 AND   recur-sec-dispon-dbr.qtd-dispon      = Qtde-disponivel
                 AND   recur-sec-dispon-dbr.qtd-segs-inic   = DEC(i-time-ini)
                 AND   recur-sec-dispon-dbr.qtd-segs-fim    = DEC(i-time-fin) NO-ERROR.
            IF NOT AVAIL recur-sec-dispon-dbr THEN
            DO:
                CREATE recur-sec-dispon-dbr.
                ASSIGN recur-sec-dispon-dbr.cod-recurso     = recur-sec-dbr.cod-recurso  
                       recur-sec-dispon-dbr.dat-inic-period = i-dt-registro              
                       recur-sec-dispon-dbr.dat-fim-period  = i-dt-registro              
                       recur-sec-dispon-dbr.qtd-dispon      = Qtde-disponivel            
                       recur-sec-dispon-dbr.qtd-segs-inic   = DEC(i-time-ini)            
                       recur-sec-dispon-dbr.qtd-segs-fim    = DEC(i-time-fin) .
            END.
        END.
    END.
    
         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartDialog, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

