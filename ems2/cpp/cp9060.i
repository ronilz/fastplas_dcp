/******************************************************************************
 ** 
 **  INCLUDE  : CP09060.I 
 **
 **  OBJETIVO : Temp-table utilizada para informa��o das causas de
 **             Refugo/Aprovado Condicional
 **
 ******************************************************************************/
 def {1} temp-table tt-ref-ordem
         field op-seq        like ref-ordem.op-seq
         field nr-ord-prod   like ref-ordem.nr-ord-prod
         field qt-refugo     like ref-ordem.qt-refugo
         field tipo-rejeicao like cod-rejeicao.tipo-rejeicao 
         field dt-refugo     like ref-ordem.dt-refugo
         field codigo-rejei  like ref-ordem.codigo-rejei
         field observacao    like ref-ordem.observacao
         field descricao     like cod-rejeicao.descricao.          
        
 def {1} buffer b-tt-ref-ordem for tt-ref-ordem.
 
 def {1} var de-saldo like rep-prod.qt-reporte.

 /* FIM - CP9060G.I */
