/******************************************************************************
 ** 
 **  INCLUDE  : CPAPI009.I 
 **
 **  OBJETIVO : Definir as temp-tables da API de Rep. da Prod. Repetitiva 
 **
 ******************************************************************************/
 
 def temp-table tt-rep-prod1 no-undo like rep-prod use-index item-rep
     field l-emite-rel      as logical
     field rw-rep-prod      as rowid
     field prog-seg         as char
     field time-out         as integer init 30
     field tentativas       as integer init 10
     field cod-versao-integracao as integer format "999".
 
 def temp-table tt-serie no-undo
     field nr-serie  like ord-prod.lote-serie
     field sequencia   as integer
     field refugo      as logical init no
     index sequencia sequencia.  
     
 def temp-table tt-relatorio no-undo
     field es-codigo     like reservas.it-codigo
     field qt-item       like reservas.quant-orig
     field qt-estoq      like reservas.quant-orig
     field qt-saldo      like reservas.quant-orig
     field un            like item.un
     field descricao     like item.desc-item.     
           
 {cpp/cp9060.i}         /* Defini��o tt-ref-ordem (refugo/aprov cond) */
    

