    DEFINE VARIABLE wpis         AS DECIMAL.
    DEFINE VARIABLE wcofins      AS DECIMAL.
    DEFINE VARIABLE waliq-pis    AS CHAR.
    DEFINE VARIABLE waliq-cofins AS CHAR.
    DEFINE VARIABLE waliq-pis1    AS DEC FORMAT 99.99.
    DEFINE VARIABLE waliq-cofins1 AS DEC FORMAT 99.99.

    ASSIGN wpis         = 0
           wcofins      = 0.
          
    for each it-nota-fisc of nota-fiscal NO-LOCK:
        ASSIGN waliq-pis    = ""
               waliq-cofins = "".
        ASSIGN waliq-pis    = SUBSTRING(it-nota-fisc.char-2,76,5)
               waliq-cofins = SUBSTRING(it-nota-fisc.char-2,81,5)
               waliq-pis1   = DECIMAL(waliq-pis)
               waliq-cofins1 = DECIMAL(waliq-cofins).
      
        ASSIGN  wpis     = wpis    + ROUND(((it-nota-fisc.vl-merc-liq *  waliq-pis1) / 100),5)
                wcofins  = wcofins + ROUND(((it-nota-fisc.vl-merc-liq *  waliq-cofins1) / 100),5).
        ASSIGN wpis    = ROUND(wpis,2)
               wcofins = ROUND(wcofins,2).
     END.
