/* declaracao de variaveis e forms dos bloquetos */

define buffer b-emitente for emitente.

define var i-aux        as integer   no-undo.
define var i-aux1       as integer   no-undo.
define var i-x          as integer   no-undo.
define var i-lado       as integer   initial 1 no-undo.
def var i-moeda-urv      as integer no-undo.
def var i-mo-cod         as integer no-undo.

define var da-vencto    as date      no-undo.
define var da-vencto1   as date      no-undo.
define var da-emissao   as date      no-undo.
define var da-emissao1  as date      no-undo.
define var da-desconto  as date      no-undo.
define var da-desconto1 as date      no-undo.
define var da-emis-blo  as date      no-undo.
define var da-emis-blo1 as date      no-undo.
define var da-abatimen  as date      no-undo.
define var da-abatimen1 as date      no-undo.
define var c-nr-docto   as character no-undo format "x(10)".
define var c-nr-docto1  as character no-undo format "x(16)".
define var c-esp-moe    as character no-undo format "x(3)".
define var c-esp-moe1   as character no-undo format "x(3)".
define var c-instruc-1  as character no-undo.
define var c-instr      as character no-undo format "x(50)" extent 5.
define var c-instr1     as character no-undo format "x(50)" extent 5.
define var c-texto      as character no-undo format "x(50)" extent 3.
define var c-instruc-2  as character no-undo.
define var c-agencia    as character no-undo format "x(16)" initial "Qualquer Agencia".
define var c-bairro     as character no-undo format "x(20)".
define var c-bairro1    as character no-undo format "x(20)".
define var l-ultimo     as logical   no-undo initial no.
define var c-cod-esp    like titulo.cod-esp     no-undo.
define var c-cod-esp1   like titulo.cod-esp     no-undo.
define var de-quant     like titulo.vl-abatimen no-undo.
define var de-quant1    like titulo.vl-abatimen no-undo.
define var de-original  like titulo.vl-original no-undo.
define var de-original1 like titulo.vl-original no-undo.
define var de-desconto  like titulo.vl-desconto no-undo.
define var de-desconto1 like titulo.vl-desconto no-undo.
define var de-juros     like titulo.vl-original no-undo.
define var de-juros1    like titulo.vl-original no-undo.
define var de-abatimen  like titulo.vl-abatimen no-undo.
define var de-abatimen1 like titulo.vl-abatimen no-undo.
define var c-cod-emit   like emitente.cod-emit no-undo.
define var c-nome-emit  like emitente.nome-emit no-undo.
define var c-nome-emit1 like emitente.nome-emit no-undo.
define var c-cgc        like emitente.cgc       no-undo.
define var c-cgc1       like emitente.cgc       no-undo.
define var c-endereco   like emitente.endereco  no-undo.
define var c-endereco1  like emitente.endereco  no-undo.
define var i-cep        like emitente.cep       no-undo.
define var i-cep1       like emitente.cep       no-undo.
define var c-cidade     like emitente.cidade    no-undo.
define var c-cidade1    like emitente.cidade    no-undo.
define var c-estado     like emitente.estado    no-undo.
define var c-estado1    like emitente.estado    no-undo.
define var c-end-age    like agencia.endereco   no-undo.
define var c-end-age1   like agencia.endereco   no-undo.
define var i-cep-age    like emitente.cep       no-undo.
define var c-cod-age    like agencia.agencia    no-undo.
define var c-cod-age1   like agencia.agencia    no-undo.
define var i-cep-age1   like emitente.cep       no-undo.

define var c-instrucao  as character no-undo format "x(55)" extent 19 initial
    ["Assume Cadastro do Cedente",
     "Nao Receber Principal sem Encargos de Mora",
     "Devolver se nao pago ate 15 dias apos Vencto",
     "Devolver se nao pago ate 30 dias apos Vencto",
     "Acatar Instrucoes contidas no proprio Titulo",
     "Protestar apos 5 dias do Vencimento",
     "Nao protestar",
     "Nao cobrar Juros de Mora",
     "Protestar apos 15 dias de Vencimento",
     "Protestar apos 20 dias de Vencimento",
     "Protestar apos 30 dias de Vencimento",
     "Devolver se nao pago ate 10 dias apos Vencto",
     "Devolver se nao pago ate 60 dias apos Vencto",
     "Nao receber antes do Vencimento",
     "Dispensar Juros ate 7 dias apos Vencimento",
     "Protestar apos 10 dias do Vencimento",
     "Protestar apos  1 dia  do Vencimento",
     "Protestar apos  2 dias do Vencimento",
     "Protestar apos  3 dias do Vencimento"].

/* forms dos bloquetes bancarios */
form
   "." 
   skip(1)
   "QUALQUER AGENCIA" at 1
   da-vencto          at 050 format "99/99/99"
   "QUALQUER AGENCIA" at 068
   da-vencto1         at 120 format "99/99/99"       
   skip(2)
   da-emissao         at 001 format "99/99/99"
   c-nr-docto         at 011
   c-cod-esp          at 025
   da-emis-blo        at 036 format "99/99/99"
   da-emissao1        at 068 format "99/99/99"
   c-nr-docto1        at 078
   c-cod-esp1         at 093
   da-emis-blo1       at 103 format "99/99/99"
   skip
   c-esp-moe          at 017
   de-quant           at 022 format ">>>>>9.99"
   de-original        at 050 format ">>>>>>>>>>9.99"
   c-esp-moe1         at 084
   de-quant1          at 089 format ">>>>>9.99"
   de-original1       at 117 format ">>>>>>>>>>9.99" 
   skip(1)
   de-desconto        at 001 format ">>>>>>>>>9.99"
   da-desconto        at 019 format "99/99/99"
   de-juros           at 028 format ">>>>>>>>>9.99"
   de-abatimen        at 050 format ">>>>>>>>>9.99"
   de-desconto1       at 068 format ">>>>>>>>>9.99"
   da-desconto1       at 086 format "99/99/99"
   de-juros1          at 095 format ">>>>>>>>>9.99"
   de-abatimen1       at 117 format ">>>>>>>>>9.99"
   skip(2)
   c-instr[3]         at 001
   c-instr1[3]        at 068 
   skip
   c-instr[4]         at 001
   c-instr1[4]        at 068 
   skip
   c-instr[5]         at 001
   c-instr1[5]        at 068 
   skip(1)
   c-nome-emit        at 004
   c-cgc
   c-nome-emit1       at 073 format "x(20)"
   c-cgc1        
   skip
   c-endereco
   c-endereco1        at 068 
   skip
   i-cep
   c-cidade
   c-estado
   i-cep1             at 068
   c-cidade1
   c-estado1             
   skip(6)
   with no-labels no-box width 132 frame f-bradesco stream-io.

/*assign c-texto[1] = substring(tt-bloqueto.instrucoes-1,001,50,"CHARACTER")
       c-texto[2] = substring(tt-bloqueto.instrucoes-2,051,50,"CHARACTER")
       c-texto[3] = substring(tt-bloqueto.instrucoes-3,101,50,"CHARACTER"). */
form
   "." skip(1)
   "QUALQUER AGENCIA" at 1
   da-vencto     format "99/99/99"       colon 50 
   skip(2)
   da-emissao    format "99/99/99"       colon 1
   c-nr-docto                            colon 11
   c-cod-esp                             colon 25
   da-emis-blo   format "99/99/99"       colon 36 
   skip
   c-esp-moe                             colon 17
   de-quant      format ">>>>>9.99"      colon 25
   de-original   format ">>>>>>>>>>9.99" colon 50 
   skip(01)
   de-desconto   format ">>>>>>>>>9.99"  colon 1
   da-desconto   format "99/99/99"       colon 19
   de-juros      format ">>>>>>>>>9.99"  colon 28
   de-abatimen   format ">>>>>>>>>9.99"  colon 50 
   skip(2)   
   c-texto[1]                            colon 1 skip
   c-texto[2]                            colon 1 skip
   c-texto[3]                            colon 1 skip(1)
   c-nome-emit   format "x(40)"          colon 4 
   c-cgc                                         skip
   c-endereco                                    skip
   i-cep
   c-cidade
   c-estado       
   skip(6)
   with no-labels no-box width 80 stream-io frame f-bradesco-80 .

form
   "." at 1 skip
   da-vencto    format "99/99/99"       at 57
   da-vencto1   format "99/99/99"       at 123 skip(2)
   da-emissao        format "999999"         at 2
   c-nr-docto        at  17
   c-cod-esp         at  31
   da-emis-blo       format "999999"         at 41
   da-emissao1       format "999999"         at 70
   c-nr-docto1       at  84
   c-cod-esp1        at  98
   da-emis-blo1      format "999999"         at 108 skip(0)
   de-quant      format ">>>>>>>9.99" at 28
   de-original   format ">>>>>>>>>>9.99" at 52
   de-quant1     format ">>>>>>>9.99" at 93
   de-original1  format ">>>>>>>>>>9.99" at 118 skip(1)
   "APOS O VENCTO COBRAR JUROS DE"        at 07
   de-desconto    format ">>>>>>>>>9.99"  at 53
   "APOS O VENCTO COBRAR JUROS DE"        at 73
   de-desconto1   format ">>>>>>>>>9.99"  at 119 skip
   "R$ "                                  at 07
   de-juros       format "zzzzz9.99"      at 10
   " AO DIA"                              at 20
   de-abatimen    format ">>>>>>>>>9.99"  at 53
   "R$ "                                  at 73
   de-juros1      format "zzzzz9.99"      at 76
   " AO DIA"                              at 86
   de-abatimen1   format ">>>>>>>>>9.99"  at 119 skip(2)
   c-instr[3]                             at 2
   c-instr1[3]                            at 70
   c-instr[4]           at 2
   c-instr1[4]          at 69 skip
   c-instr[5]           at  2
   c-instr1[5]          at 70
   c-nome-emit          at 5
   c-cgc
   c-nome-emit1         at 73 format "x(40)"
   c-cgc1               skip
   c-endereco           at  2
   c-bairro             at 42
   c-endereco1          at 70 
   c-bairro1            at 112 skip
   i-cep                at  2
   c-cidade
   c-estado
   i-cep1       at 70
   c-cidade1
   c-estado1      skip(7)
   with no-labels no-box width 132 frame f-brasil stream-io.

form
   "." skip
   c-cidade         at 01
   da-vencto        at 49 skip(3)
   da-emissao       at 01 format "99/99/99"
   c-nr-docto       at 10
   c-cod-esp        at 30
   da-emis-blo      at 40 format "99/99/99" skip(1)
   c-esp-moe        at 21
   de-quant         format ">>>>>9.99" at 26 
   de-original      format ">>>>>>,>>>,>>9.99" at 45 skip
   de-abatimen      format ">>>,>>>,>>9.99"    at 48 skip
   c-instr[1]       at 01
   c-instr[2]       at 01
   c-instr[3]       at 01
   c-instr[4]       at 01
   c-instr[5]       at 01 skip(1)
   c-nome-emit      at 05 space(1)
   c-cgc
   c-endereco       at 05
   i-cep            at 05
   c-cidade
   c-estado         skip(6)
   with down no-labels no-box width 132 frame f-itau stream-io.

form
   "." skip
   c-agencia        at 01
   da-vencto        at 55
   c-end-age        at 01 format "x(35)" "-"
   i-cep-age        skip(2)
   da-emissao       at 01 format "99/99/99"
   c-nr-docto       at 11
   c-cod-esp        at 28
   da-emis-blo      at 40 skip(1)
   c-esp-moe     at 21
   de-quant      format ">>>>>>>9.99" at 30
   de-original  format ">>>,>>>,>>>,>>9.99" at 52 skip (1)
   c-instr[1]               at 01
   c-instr[2]               at 01
   c-instr[3]               at 01
   c-instr[4]               at 01
   c-instr[5]               at 01 skip(1)
   c-nome-emit      at 05   format "x(40)"
   "CGC."           at 47
   c-cgc            at 52
   c-endereco       at 05
   i-cep            at 05
   c-cidade
   c-estado
   skip(6)
   with down no-labels no-box width 80 frame f-bamerindus stream-io.

form
   "." skip
   c-cod-age     at 01
   da-vencto     at 55
   c-end-age     at 01 format "x(35)" "-"
   i-cep-age
   skip(2)
   da-emissao    at 01 format "99/99/99"
   c-nr-docto    at 18
   c-cod-esp     at 30
   da-emis-blo   format "99/99/99"  at 40 skip
   c-esp-moe     at 01
   de-quant      format ">>>>>9.99" at 05
   de-original   format ">>>,>>>,>>>,>>9.99" at 38 skip (1)
   c-instr[1]               at 01
   c-instr[2]               at 01
   c-instr[3]               at 01
   c-instr[4]               at 01
   c-instr[5]               at 01 skip(2)
   c-nome-emit              at 04 format "x(40)"
   space(1) c-cgc
   c-endereco       at 04
   i-cep            at 04
   c-cidade
   c-estado
   skip(6)
   with down no-labels no-box width 80 frame f-boavista stream-io.

form
   "." skip
   c-cod-age               at 01
   c-cod-age1              at 69
   da-vencto               at 49
   da-vencto1              at 115 skip(2)
   da-emissao              at 01 format "99/99/99"
   c-nr-docto              at 10
   c-cod-esp               at 27
   da-emis-blo             at 38 format "99/99/99"
   da-emissao1             at 68 format "99/99/99"
   c-nr-docto1             at 78
   c-cod-esp1              at 93
   da-emis-blo1            at 104 format "99/99/99" skip(1)
   c-esp-moe     at 12
   de-quant      format ">>>>>9.99" at 17
   de-original   format ">>>,>>>,>>>,>>9.99" at 45
   c-esp-moe1    at 79
   de-quant1     format ">>>>>9.99" at 84
   de-original1            format ">>>,>>>,>>>,>>9.99" at 112 skip(1)
   c-instr[3]              at 01
   c-instr1[3]             at 69
   c-instr[4]              at 01
   c-instr1[4]             at 69
   c-instr[5]              at 01
   de-juros                format ">>>,>>>,>>9.99" at 49
   c-instr1[5]             at 69
   de-juros1               format ">>>,>>>,>>9.99" at 116 skip(3)
   c-nome-emit             at 05
   c-nome-emit1            at 71 format "x(40)"
   c-endereco              at 05
   c-endereco1             at 71 skip(1)
   i-cep                   at 05
   c-cidade
   c-estado
   i-cep1                  at 71
   c-cidade1
   c-estado1
   c-cgc                   at 05
   c-cgc1                  at 71 skip(4)
   with down no-labels no-box width 132 frame f-frances stream-io.

form
   "." skip (1)
   c-cod-age                     at 1
   i-cep-age
   c-end-age    format "x(25)"
   da-vencto  format "99/99/99" at 53
   c-cod-age1                             at 68
   i-cep-age1
   c-end-age1           format "x(25)"
   da-vencto1           format "99/99/99" at 120 skip(2)
   da-emissao   format "99/99/99" at 1
   c-nr-docto                            at 17
   c-cod-esp                        at 29
   da-emis-blo          format "99/99/99" at 39
   da-emissao1       format "99/99/99" at 68
   c-nr-docto1                           at 78
   c-cod-esp1                            at 93
   da-emis-blo1         format "99/99/99" at 103 skip
   c-esp-moe     at 18
   de-quant      format ">>>>>9.99" at 29
   de-original  format ">>>,>>>,>>9.99" at 50
   c-esp-moe1    at 79
   de-quant1     format ">>>>>9.99" at 94
   de-original1 format ">>>,>>>,>>9.99" at 117 skip(1)
   de-abatimen  format ">,>>>,>>9.99"   at 50
   de-abatimen1 format ">,>>>,>>9.99"   at 117 skip(2)
   c-instr[3]          at 1
   c-instr1[3]         at 68 skip
   c-instr[4]          at 1
   c-instr1[4]         at 68 skip
   c-instr[5]          at 1
   c-instr1[5]         at 68 skip(1)
   c-nome-emit  at 4
   c-cgc
   c-nome-emit1         at 73 format "x(40)"
   c-cgc1               skip
   c-endereco
   c-endereco1          at 68 skip
   i-cep
   c-cidade
   c-estado
   i-cep1               at 68
   c-cidade1
   c-estado1            skip(6)
   with no-labels no-box width 132 frame f-real stream-io.

form
   "." skip (1)
   da-vencto    format "99/99/9999" at 56
   skip(3)
   da-emissao   format "99/99/9999" at 1
   c-nr-docto   format "x(11)"      at 14
   c-cod-esp                        at 25
   da-emis-blo  format "99/99/9999" at 34 
   " "                              at 1
   de-original  format ">>>,>>>,>>9.99" at 52 skip (1)
   c-instr[1]                       at 1
   c-instr[2]                       at 1
   c-instr[3]                       at 1
   c-instr[4]                       at 1
   c-instr[5]                       at 1 skip (2)
   c-cod-emit                       at 4 
   c-nome-emit format "x(37)"
   c-cgc      
   c-endereco                       at 1
   i-cep                            at 1
   c-cidade
   c-estado skip(5)
   with no-labels no-box width 80 frame f-safra stream-io.
