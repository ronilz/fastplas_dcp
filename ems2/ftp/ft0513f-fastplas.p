/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i FT0513F 2.00.00.033}  /*** 010033 ***/
/******************************************************************************
*
*   PROGRAMA :  FT0513F.P
*   OBJETIVO:  Mostrar Nota Fiscal - Modelo Padrao 1A SP
*
******************************************************************************/

def input parameter r-cidade-zf as rowid.
/**Pre-processadores**/
{cdp/cdcfgdis.i}

/****************************
*   definicao de work-file
****************************/

def new shared temp-table item-nota no-undo
    field registro     as rowid
    field it-codigo    like it-nota-fisc.it-codigo
    field aliquota-icm like it-nota-fisc.aliquota-icm
    field nr-seq-fat   like it-nota-fisc.nr-seq-fat
    field sit-tribut   as integer format ">>>".

def new shared var i-num9100     as integer.
def new shared var i-tam9100     as integer.
def new shared var de-val9100    as decimal.
def new shared var c-ext9100     as character extent 10.
def new shared var de-cotacao    as decimal format ">>>,>>9.99999999".
def new shared var i-cont        as integer.
def new shared var r-nota-fiscal as rowid.
def new shared var i-codigo      as integer.
def new shared var r-item        as rowid.

def new shared var de-tot-icms-obs      like it-nota-fisc.vl-icms-it.
def new shared var de-tot-icmssubs-obs  like it-nota-fisc.vl-icmsub-it.
def new shared var de-tot-bicmssubs-obs like it-nota-fisc.vl-bsubs-it.
def new shared var de-tot-ipi-dev-obs   like it-nota-fisc.vl-ipi-it.
def new shared var de-tot-ipi-calc      like it-nota-fisc.vl-ipi-it.
def new shared var de-tot-ipi-nota      like it-nota-fisc.vl-ipi-it.

/**********************************
*   definicao de tabelas globais
**********************************/

def new shared var i-parcela   as integer                           extent 6.
def new shared var i-fatura    as char format "x(16)"               extent 6.
def new shared var da-venc-dup as date    format "99/99/9999"       extent 6.
def new shared var de-vl-dup   as decimal format ">>>>>,>>>,>>9.99" extent 6.

/********************************************
*   definicao das variaveis compartilhadas
********************************************/

def  shared var r-nota       as rowid.
def  shared var r-natur-oper as rowid.
def  shared var r-ped-venda  as rowid.
def  shared var r-pre-fat    as rowid.
def  shared var r-emitente   as rowid.
def  shared var r-estabel    as rowid.
def  shared var l-dt         as logical format "Sim/Nao"  init no.
def  shared var dt-saida     as date    format "99/99/9999" init ?.
def  shared var hr-saida     as char    format "xx:xx:xx" init "".
def  shared var de-conv      as decimal format ">>>>9.99".

/************************************
*   definicao das variaveis locais
************************************/
def var c-observ          as character format "x(200)" init "  ".
def var i-cont3           as integer.
def var i-cont4           as integer.
def var i-espaco          as integer.
def var i-cont6           as integer.
def var i-cont1           as integer.
def var i-qt-volumes      as integer.
def var i-cont-item       as integer.
def var i-dup             as integer.
def var l-sub             as logical.
def var c-cod-suframa-est like estabelec.cod-suframa.
def var c-cod-suframa-cli like emitente.cod-suframa.
def var de-vl-serv        like it-nota-fisc.vl-iss-it.
def var de-aliquota-iss   like it-nota-fisc.aliquota-iss.
def var de-desc           like it-nota-fisc.vl-preori.
def var de-qt-fatur       as decimal format ">>>>,>>9.9999".
def var de-tot-icmssubs   like it-nota-fisc.vl-icmsub-it.
def var de-tot-bicmssubs  like it-nota-fisc.vl-bsubs-it.
def var de-tot-bas-icm    like it-nota-fisc.vl-icms-it.
def var de-tot-bas-iss    like it-nota-fisc.vl-iss-it.
def var de-tot-bas-ipi    like it-nota-fisc.vl-ipi-it.
def var de-tot-icm        like it-nota-fisc.vl-icms-it.
def var de-tot-iss        like it-nota-fisc.vl-iss-it.
def var de-tot-ipi        like it-nota-fisc.vl-ipi-it.
def var de-vl-bipi-it     like it-nota-fisc.vl-bipi-it.
def var de-vl-ipi-it      like it-nota-fisc.vl-ipi-it.
def var l-frete-bipi      as log.
def var c-pago            as character format "x" init " ".
def var c-class-fiscal    as character format {cdp/cd0603.i3}.
def var c-mensagem1       as character format "x(230)".
def var c-mensagem2       as character format "x(158)".
def var c-especie         as character.
def var c-desc-prod       as character format "x(63)".
def var c-un-fatur        as character format "x(2)".
def var r-it-nota         as rowid.
def var c-nome-tr         as character format "x(12)".
def var c-num-dup         as character.
def var c-num-nota        as character format "x(16)".
def var c-cgc             like estabelec.cgc.
def var c-formato-cfop    as char.

/* Integracao Modulo de Importacao */
def temp-table tt-desp
    field cod-desp  as integer format ">>>>9"
    field descricao as char    format "x(30)"
    field val-desp  as decimal format ">>>>>,>>>,>>9.99999"
    index codigo is unique primary
        cod-desp.
def var l-importacao as logical no-undo.

/**************/
/** Includes **/
/**************/

{include/tt-edit.i}
{include/pi-edit.i}

/**************************
*   definicao de tabelas
**************************/

def var c-mess as character format "x(68)" extent 18 init "  ".
find nota-fiscal where rowid(nota-fiscal) = r-nota       no-lock no-error.
find estabelec   where rowid(estabelec)   = r-estabel    no-lock no-error.
find ped-venda   where rowid(ped-venda)   = r-ped-venda  no-lock no-error.
find emitente    where rowid(emitente)    = r-emitente   no-lock no-error.
find pre-fatur   where rowid(pre-fatur)   = r-pre-fat    no-lock no-error.
find natur-oper  where rowid(natur-oper)  = r-natur-oper no-lock no-error.

  assign da-venc-dup          = ?
         i-cont               = 0
         i-cont6              = 0
         i-parcela            = 0
         i-fatura             = ""
         i-qt-volumes         = 0
         de-aliquota-iss      = 0
         i-cont3              = 0
         i-cont1              = 0
         de-vl-dup            = 0
         de-cotacao           = 0
         de-tot-ipi           = 0
         de-tot-ipi-nota      = 0
         de-tot-ipi-calc      = 0
         de-tot-icm           = 0
         de-tot-iss           = 0
         de-tot-icmssubs-obs  = 0
         de-tot-bicmssubs-obs = 0
         de-tot-icmssubs      = 0
         de-tot-bicmssubs     = 0
         de-tot-icms-obs      = 0
         de-tot-bas-ipi       = 0
         de-tot-ipi-dev-obs   = 0
         de-tot-bas-iss       = 0
         de-tot-bas-icm       = 0
         i-cont-item          = 0
         i-codigo             = 0
         de-vl-serv           = 0
         c-pago               = ""
         c-mess               = ""
         c-mensagem1          = ""
         c-mensagem2          = "" 
         c-especie            = ""
         c-nome-tr            = "".

  /*-------------------------------  Work-File  -----------------------------*/

  find estabelec of nota-fiscal no-lock.

 /*------------------ CODIGO SUFRAMA -----------------*/
 
   if   avail emitente
   and  avail estabelec
   and  r-cidade-zf <> ? then
       assign c-cod-suframa-est = estabelec.cod-suframa
              c-cod-suframa-cli = emitente.cod-suframa.
   else
       assign c-cod-suframa-est = ""
              c-cod-suframa-cli = "".                
       
 /*---------------------------------------------------*/

  find first param-global no-lock no-error.
  
  assign c-cgc = if param-global.formato-id-federal <> ""
                 then 
                   string(estabelec.cgc, param-global.formato-id-federal)
                 else 
                   estabelec.cgc.

  for each item-nota:
      delete item-nota.
  end.

  for each it-nota-fisc of nota-fiscal no-lock,
      each item
      where item.it-codigo = it-nota-fisc.it-codigo no-lock:

      

      assign r-it-nota     = rowid(it-nota-fisc)
             r-natur-oper  = rowid(natur-oper)
             r-nota-fiscal = rowid(nota-fiscal)
             r-item        = rowid(item).

      run ftp/ft0515a.p (input  r-it-nota,
                         output i-codigo,
                         output l-sub).

      run ftp/ft0513d.p (input r-it-nota,
                         input r-cidade-zf).
  end.

  /*--------------------  DUPLICATAS DA NOTA FISCAL FATURA  -----------------*/

  assign r-nota-fiscal = rowid(nota-fiscal).

  if  nota-fiscal.emite-duplic then
      run ftp/ft0513c.p.

  do  i-cont4 = 1 to 6:
      if  i-parcela[i-cont4] <> 0 then
          assign i-dup = i-dup + 1.
  end.

  /*-----------------------  DADOS DO ESTABELECIMENTO  ----------------------*/
  
  put skip (4).    /* salto de pagina inicial */
 
  
  /* 15/09/99 FASTPLAS - INIBIDO PARA ACERTAR PARA O LAYOUT DA NOTA FASTPLAS   
  put estabelec.endereco at 124.
  -- 15/09/99 FASTPLAS - FIM INIBICAO ---------------------------------------*/
  
  /* 15/09/99 FASTPLAS - ALTERACAO P/APRESENTAR BRANCO NO LUGAR DO ENDERECO--*/
  PUT FILL(" ",35)       AT 124.
  /* 15/09/99 FASTPLAS - FIM ALTERACAO --------------------------------------*/

if  natur-oper.tipo = 1 then      /* ENTRADA */
      put "X" at 196.
  else                                /* SAIDA   */
      put "X" at 179.
      
  assign c-num-nota = string(nota-fiscal.nr-nota-fis).
  /* 15/09/99 FASTPLAS - INIBIDO PARA ACERTAR LAYOUT DA NOTA FASTPLAS -------
  put c-num-nota                               at 220 skip
      estabelec.bairro                         at 124.

  put estabelec.cidade at 124
      estabelec.estado at 170 skip(1).

  put /* nao ha telefone                               at 139  */
      estabelec.cep                                    at 162.

  put /* nao ha telefax                                at 139  */
      c-cgc                                            at 178 skip.
  -- 15/09/99 FASTPLAS - FIM INIBICAO --------------------------------------*/

  /* 15/09/99 FASTPLAS - ALTERACAO -P/ACERTAR LAYOUT FASTPLAS --------------*/
  put c-num-nota at 220 skip.
  put   FILL(" ",15)                                 at 124 /* bairro */
        FILL(" ",25)                                 at 124 /* cidade */
        FILL(" ",2)                                  at 170 /* estado */
        skip(1)
        FILL(" ",8)                                  at 162 /* cep */
        FILL(" ",19)                                 at 178  /* estabelec.cgc*/
        SKIP.

  /* 15/09/99 FASTPLAS - FIM ALTERACAO ------------------------------------*/

 
  /*---------------------  OBSERVACOES DA NOTA FISCAL  ----------------------*/  

  /*------  OBSERVACOES  ------*/
  
  assign c-mensagem2 = nota-fiscal.observ-nota.
  
  /* Integracao Modulo Importacao */   
  
  find first param-global no-lock.
  if avail param-global and param-global.modulo-07 then do:
    if avail emitente  and
       avail estabelec and
      (emitente.natureza = 3 or emitente.natureza = 4) and
       emitente.pais <> estabelec.pais then do: 
        /* retorna temp-table com as despesas de importacao do documento e
        ** que estiverem com o parametro de "Imprime Nota Fiscal"(desp-imp.log-1) marcado no 
        ** programa Cadastro Despesa/Impostos Importacao (cd2565) */
        run imp/im9031.p (input rowid(nota-fiscal),
                          output table tt-desp).                                  
        for each tt-desp:
            assign c-mensagem2 = c-mensagem2 + " | "
                               + string(tt-desp.cod-desp) + "-"
                               + tt-desp.descricao + " / "
                               + trim(string(tt-desp.val-desp,">>>>>,>>>,>>9.99999")).
        end.
    end.
  end.    
  
  
  /* ******************************************************************************** */ 

  run pi-print-editor(input c-mensagem2, input 68).
  for each tt-editor:
      if  i-cont6 < 18 then 
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = tt-editor.conteudo.
      else
          leave.
  end.

  /*------  CODIGO DE MENSAGEM DA NOTA FISCAL  ------*/

  find mensagem
       where mensagem.cod-mensag = nota-fiscal.cod-mensag no-lock no-error.
  if  avail mensagem then do:
      run pi-print-editor(input mensagem.texto-mensag, input 68).
      for each tt-editor:
          c-mensagem1 = c-mensagem1 + tt-editor.conteudo.
      end.
      run pi-print-editor(input substr(c-mensagem1,1,381), input 68).
      for each tt-editor:
          if  i-cont6 < 18 then 
              assign i-cont6         = i-cont6 + 1 
                     c-mess[i-cont6] = tt-editor.conteudo.
          else
              leave.
      end.
  end.

  /*------  IMPRIME C�DIGO DA REPARTI��O FISCAL  ------*/

  if  trim(substr(estabelec.char-2,62,20)) <> "" 
      and i-cont6 < 18 then do:                  
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = "Cod.Repart.Fiscal: " + substr(estabelec.char-2,62,20).
  end.


  /*------  IMPRIME MENSAGEM DESCONTO ZONA FRANCA  ------*/

  if  r-cidade-zf <> ? 
      and de-conv  > 0
      and de-conv <> 1 
      and i-cont6 < 18 then do:                  
      if  natur-oper.nat-operacao begins "6" then do:
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = "Desconto de" 
                                 + string(((de-conv * 100 - 100) * -1),">>9.99")
                                 + "%, de ICMS Referente a "
                                 + "Vendas para Zona Franca..: "
                 i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = string(nota-fiscal.dec-1, ">>>,>>>,>>9.99").
      end.
      else do:
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] =
                "Desconto de " + string(((de-conv * 100 - 100) * -1), ">>9.99") +
                 "%, de ICMS Referente a Vendas para Area de livre "
                 i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = "Comercio..: " +
                                   string(nota-fiscal.dec-1, ">>>,>>>,>>9.99").
      end.

  end.

  /*---- IMPRESSAO DO CODIGO SUFRAMA DO ESTABELECIMENTO E CLIENTE ----*/

  if  c-cod-suframa-est <> ""
  and i-cont6 < 18 then
      assign i-cont6         = i-cont6 + 1 
             c-mess[i-cont6] = "Registrodo Estabelecimento na SUFRAMA: " +
                                 c-cod-suframa-est.

  if  c-cod-suframa-cli <> ""
  and i-cont6 < 18 then
      assign i-cont6         = i-cont6 + 1
             c-mess[i-cont6] = "Registro do Cliente na SUFRAMA: " +
                                c-cod-suframa-cli.

  /*------  SUBSTITUICAO TRIBUTARIA  ------*/

  if  de-tot-icmssubs-obs > 0 
  and i-cont6 < 18 then do:
      assign i-cont6         = i-cont6 + 1
             c-mess[i-cont6] =
                   "ICMS  Retido  na  fonte  por Substituicao Tributaria"
             i-cont6         = i-cont6 + 1
             c-mess[i-cont6] = "com base: "
                             + string(de-tot-bicmssubs-obs, ">>>,>>>,>>9.99")
                             + " e valor: "
                             + string(de-tot-icmssubs-obs, ">>>,>>>,>>9.99").
      if  avail estabelec
      and avail emitente  
      and estabelec.estado = "SP"
      and emitente.estado  = "SP" then 
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = "Valor do ICMS: "
                                 + string(de-tot-icms-obs, ">>>,>>>,>>9.99").
  end.

  /*------  IPI REFERENTE A DEVOLUCAO  ------*/

  if  de-tot-ipi-dev-obs > 0 
  and i-cont < 18 then do:
      assign i-cont6         = i-cont6 + 1
             c-mess[i-cont6] = "Valor do IPI Referente a Devolucao..: "
                             + string(de-tot-ipi-dev-obs,">>>>>>>>9.99").
  end.

  /*------  VALOR DO IPI SOBRE AS DESPESAS  ------*/

  if  de-tot-ipi-calc > 0
  and i-cont6 < 18 then do:
      assign i-cont6         = i-cont6 + 1
             c-mess[i-cont6] = "Valor do IPI sobre Despesas acessorias..: "
                             + string(de-tot-ipi-nota - de-tot-ipi-calc    
                             ,">>>>>>>>>9.99").
  end.

  /*------  CONDI��ES DE REDESPACHO ------*/

  assign i-espaco  = 12 - length(nota-fiscal.nome-tr-red)
         c-nome-tr = nota-fiscal.nome-tr-red + fill(" ",i-espaco).
 
  if  c-nome-tr <> " " then do:
      if  substring(nota-fiscal.cond-redespa,1,39) <> " " then do:
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = "Transp. Redespa:" + c-nome-tr + " "
                                 + substring(nota-fiscal.cond-redespa,1,39).
      end.
      if  substring(nota-fiscal.cond-redespa,40,37) +
          substring(nota-fiscal.cond-redespa,77,31) <> " " then do:
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = substring(nota-fiscal.cond-redespa,40,37)
                                 + substring(nota-fiscal.cond-redespa,77,31).
      end.
      if  substring(nota-fiscal.cond-redespa,108,45) <> " " then do:
          assign i-cont6         = i-cont6 + 1
                 c-mess[i-cont6] = substring(nota-fiscal.cond-redespa,108,45).
      end.
  end.

  /*------------- IMPRESSAO DAS LINHAS DA OBS E DADOS DO EMITENTE -----------*/

  /* Define o formato de impressao da natureza de operacao */
  assign c-formato-cfop = if  substr(natur-oper.char-2,78,10) <> " "
                          then trim(substr(natur-oper.char-2,78,10))
                          else "9.99".
                              
  do  i-cont1 = 1 to 17:
      put c-mess[i-cont1] at 03.

      if  i-cont1 = 2 then do:
          /**Definicoes referente CFOP**/
          if no then
              find first doc-fiscal no-lock no-error.
          {cdp/cd0620.i1 nota-fiscal.cod-estabel}
          {cdp/cd0620.i3 nat-operacao nota-fiscal.dt-emis-nota}.
          /* alterado o formato da cfop para 40 */
          put c-desc-cfop-nat  FORMAT "X(40)"  at  76
              {cdp/cd0620.i2 "nat-operacao" nota-fiscal.dt-emis-nota "' '" "c-formato-cfop"} at 137.
                                             
          /*------ INS. EST. DO SUBSTITUTO TRIB. ------*/

          /* ESTE ARQUIVO DEVERA SE CRIADO NO BANCO DE DADOS DO CLIENTE */

          if  l-sub = yes then do:
              find first estab-uf
                   where estab-uf.cod-estabel = nota-fiscal.cod-estabel
                   and   estab-uf.estado      = nota-fiscal.estado  no-lock no-error.
              if  avail estab-uf then
                  put estab-uf.ins-estadual at 146.
          end.
          /*-----------------------------------------------------*/
          /* 15/09/99 FASTPLAS - INIBIDO P/ ACERTAR FORMAT FASTPLAS 
          put estabelec.ins-estadual at 178.
          -- FIM DA INIBICAO ------------------------------------*/
          /* 15/09/99 FASTPLAS - ALTERACAO P/ ACERTAR LAYOUT FASTPLAS--*/
          put FILL(" ",19) at 178.
          /* -- FIM DA ALTERACAO --------------------------------------*/

      end.

      
      if  i-cont1 = 5 then do:
          if avail emitente then do:
              put emitente.nome-emit at  76.
          
              if emitente.natureza = 1 then 
                  assign c-cgc = if param-global.formato-id-pessoal <> ""
                                 then string(nota-fiscal.cgc, param-global.formato-id-pessoal)
                                 else nota-fiscal.cgc.
              else 
                  if emitente.natureza = 2 then 
                      assign c-cgc = if param-global.formato-id-federal <> ""
                                     then string(nota-fiscal.cgc, param-global.formato-id-federal)
                                     else nota-fiscal.cgc.
                  else
                      assign c-cgc = nota-fiscal.cgc.
          end.
          if  nota-fiscal.nome-ab-cli <> "brinde" then
              put c-cgc   at 178.
              put nota-fiscal.dt-emis-nota format "99/99/9999" at 215.
      end.

      if  i-cont1 = 7 then do:
          if  nota-fiscal.nome-ab-cli <> "brinde" then
              put nota-fiscal.endereco  at  76
                  nota-fiscal.bairro at 157
                  nota-fiscal.cep    at 195.

          if  nota-fiscal.dt-saida <> ? then do:
              put nota-fiscal.dt-saida  format "99/99/9999" at 215.
          end.
      end.

      if  i-cont1 = 9 then do:
          if  avail emitente 
          and avail nota-fiscal 
          and nota-fiscal.nome-ab-cli <> "brinde" then
              put nota-fiscal.cidade              at  76
                  emitente.telefone[1]  at 155
                  nota-fiscal.estado              at 174
                  nota-fiscal.ins-estadual at 182.

          if  l-dt = yes then
              put hr-saida at 215.  /* nao esta sendo gravada na nota */
      end.

      /*------  DUPLICATAS  ------*/

      if  i-cont1 = 13
      or  i-cont1 = 14
      or  i-cont1 = 15 then do:
          assign i-cont3 = i-cont3 + 1.
          if  i-parcela[i-cont3]  <> 0   then do:
              /*assign c-num-dup = substring(i-fatura[i-cont3],4,5).
               put c-num-dup format "99999"               at  79*/   
                 put i-fatura[i-cont3]     format "9999999"   at 77
                     "/"                                    at  84
                  i-parcela[i-cont3]    format "99"      at  85
                  da-venc-dup[i-cont3]                   at  97
                  de-vl-dup[i-cont3]                     at 127.

              if  i-dup < 4 then
                  put skip.
          end.
          else
              put skip.

          if  i-parcela[i-cont3 + 3] <> 0 then do:
/*              assign c-num-dup = substring(i-fatura[i-cont3],4,5).    
               put c-num-dup              format "99999"      at 157*/
               put i-fatura[i-cont3]     format "9999999"       at 155
                  "/"                                        at 162
                  i-parcela[i-cont3 + 3] format "99"         at 163
                  da-venc-dup[i-cont3 + 3]                   at 175
                  de-vl-dup[i-cont3 + 3]                     at 202.
          end.
      end.

      /*------  VALOR POR EXTENSO  ------*/

      if  i-cont1 = 17
      and nota-fiscal.emite-duplic then do:
          assign de-val9100 = nota-fiscal.vl-tot-nota
                 i-num9100  = 2
                 i-tam9100  = 80.
          run cdp/cd9100.p.
          put c-ext9100[1] format "x(80)" at 90.
      end.
      else 
        put skip.

  end.

  put c-ext9100[2] format "x(80)" at 90 skip(2).

  /*---------------------  ITENS DA NOTA FISCAL FATURA  ---------------------*/

  for each it-nota-fisc of nota-fiscal no-lock,
      each  item
      where item.it-codigo = it-nota-fisc.it-codigo no-lock,

      each  item-nota
      where item-nota.registro = rowid(it-nota-fisc) no-lock

      break by item-nota.sit-tribut
            by item-nota.aliquota-icm
            by item-nota.nr-seq-fat:

      /*------  PESQUISA A DESCRICAO DO PRODUTO  ------ */

      find narrativa of item no-lock no-error.
      if  item.ind-imp-desc = 1 then assign c-desc-prod = item.desc-item.
          
      if  item.ind-imp-desc =  8 then do:
          find item-cli where item-cli.nome-abrev = nota-fiscal.nome-ab-cli
                        and   item-cli.it-codigo  = it-nota-fisc.it-codigo no-lock no-error.
            
          /* 15/09/99 FASTPLAS - INIBIDO O PADRAO DATASUL P/ACERTAR LAYOUT FASTPLAS
          assign c-desc-prod = item.desc-item + if avail item-cli then
                                                  trim(substring(item-cli.narrativa,1,27))
                                                                        else "".
          -- 15/09/99 FASTPLAS - FIM INIBICAO --------------------------------*/
          /* 15/09/99 FASTPLAS - ALTERACAO - P/ACERTAR DESCRICAO PRODUTO FASTPLAS
             Motivo da alteracao: Devido ao fato de nao utilizarmos a descricao-2
             do item a mesma foi excluida e o tamanho do display do campo narrativa
             item x cliente foi aumentado para 40.-------------------------------*/
             assign c-desc-prod = item.descricao-1
                                 + " "
                                 + if  avail item-cli then
                                   substring(item-cli.narrativa,1,40)
                                 else "".
          /* 15/09/99 FASTPLAS - FIM ALTERACAO -------------------------------*/
          end.

      if  item.ind-imp-desc = 9 then do:
          find nar-it-nota where nar-it-nota.cod-estabel  = it-nota-fisc.cod-estabel
                           and   nar-it-nota.serie        = it-nota-fisc.serie
                           and   nar-it-nota.nr-nota-fis  = it-nota-fisc.nr-nota-fis
                           and   nar-it-nota.nr-sequencia = it-nota-fisc.nr-seq-fat
                           and   nar-it-nota.it-codigo    = it-nota-fisc.it-codigo no-lock no-error.
 
           assign c-desc-prod = item.desc-item 
                              + if  avail nar-it-nota then
                                    trim(substring(nar-it-nota.narrativa,1,27))
                                else "".
      end.

      if  item.ind-imp-desc = 10 then
          assign c-desc-prod = item.desc-item 
                             + if  avail narrativa then
                                   trim(substring(narrativa.descricao,1,27))
                               else "".

      if  (item.ind-imp-desc = 2 or item.ind-imp-desc = 5 or item.ind-imp-desc = 6)
          and avail narrativa then do:
              assign c-desc-prod = if  item.ind-imp-desc = 2 then
                                       item.desc-item + trim(substring(narrativa.descricao,1,38))
                                   else if item.ind-imp-desc = 5 then 
                                            trim(substring(narrativa.descricao,1,38))
                                        else if item.ind-imp-desc = 6 then
                                                    trim(entry(1,substring(narrativa.descricao,1,76),chr(10)))
                                             else "".
      end.
      
      if  item.ind-imp-desc = 3 then do:
          find item-cli where item-cli.nome-abrev = nota-fiscal.nome-ab-cli
               and   item-cli.it-codigo  = it-nota-fisc.it-codigo
               no-lock no-error.
          assign c-desc-prod = item.desc-item + if  avail item-cli then
                                   trim(substring(item-cli.narrativa,1,380))
                               else "".
      end.

      if  item.ind-imp-desc = 4
      or  item.ind-imp-desc = 7 then do:
          find nar-it-nota where nar-it-nota.cod-estabel  = it-nota-fisc.cod-estabel
                     and   nar-it-nota.serie        = it-nota-fisc.serie
               and   nar-it-nota.nr-nota-fis  = it-nota-fisc.nr-nota-fis
               and   nar-it-nota.nr-sequencia = it-nota-fisc.nr-seq-fat
               and   nar-it-nota.it-codigo    = it-nota-fisc.it-codigo
               no-lock no-error.

          if item.ind-imp-desc = 4 then 
             assign c-desc-prod = item.desc-item + if  avail nar-it-nota then
                                        trim(substring(nar-it-nota.narrativa,1,380))
                                    else "".

          if item.ind-imp-desc = 7 then
             assign c-desc-prod =  if  avail nar-it-nota then
                                       trim(substring
                                       (nar-it-nota.narrativa,1,380))
                                   else "".
             end.


      /*------  VERIFICA A CLASSIFICACAO FISCAL DO ITEM  ------*/

      if  it-nota-fisc.class-fisc <> "" then
          assign c-class-fiscal = it-nota-fisc.class-fisc.
      else
          assign c-class-fiscal = item.class-fisc.

      /*------  QUANTIDADE/UNIDADE FATURADA  ------*/
       assign c-un-fatur  = if   it-nota-fisc.ind-fat-qtfam = no 
                            then it-nota-fisc.un-fatur[1]
                            else it-nota-fisc.un-fatur[2]
              de-qt-fatur = if   it-nota-fisc.ind-fat-qtfam = no 
                            then it-nota-fisc.qt-faturada[1]
                            else it-nota-fisc.qt-faturada[2].
    
      /*-----------------------  IMPRESSAO DOS ITENS  -----------------------*/

      if  nota-fiscal.perc-desco1   <> 0
      or  nota-fiscal.perc-desco2   <> 0
      or  nota-fiscal.per-des-icms  <> 0
      or  it-nota-fisc.per-des-item <> 0 then
          assign de-desc = it-nota-fisc.vl-merc-ori - it-nota-fisc.vl-merc-liq.
      else
          assign de-desc = 0.
          
      run pi-print-editor(input replace(replace(c-desc-prod, chr(13), " "), chr(10), " "), input 63).
      
      find first tt-editor no-error.
          
      put item.it-codigo                                      at  03
          it-nota-fisc.peso-liq-fat   format ">>>>>>>9.99"    at  20
          (if avail tt-editor 
           then tt-editor.conteudo
           else " ")                  format "x(63)"          at  34
          c-class-fiscal              format {cdp/cd0603.i3}  at  98
          item-nota.sit-tribut        format "999"            at 116
          c-un-fatur                  format "x(2)"           at 121
          /* --- fastplas - 06/05/2003 - aumento p/ 4 casas decimais */
          de-qt-fatur                 format ">>>>>>9.9999"    at 126.
          
      /*--------------IMPRESSAO DA DESCRICAO DO ITEM ----------------------------*/

      find first tt-editor no-error.
      
      if avail tt-editor then  delete tt-editor.
      
      for each tt-editor:
         if i-cont-item < 13 then do:
            assign i-cont-item = i-cont-item + 1.
            put tt-editor.conteudo format "x(63)" at 34.
         end.
      end.

      /*-----------------  PARA ZONA FRANCA DE MANAUS 7% DE IPI  ------------*/

      if  r-cidade-zf <> ? and
          natur-oper.nat-operacao begins "6" then do:
              put it-nota-fisc.vl-preuni   / de-conv
                                    format ">>>,>>>,>>9.9999"   at 141
                  it-nota-fisc.vl-merc-liq / de-conv
                                    format ">>>>>,>>>,>>9.99" at 158
                  de-desc / de-conv format ">>>>>>>9.99"      at 180.
              if it-nota-fisc.cd-trib-icm <> 3 then
                  put it-nota-fisc.aliquota-icm               at 196.

              if  it-nota-fisc.aliquota-ipi <> 0
              or  it-nota-fisc.vl-ipi-it    <> 0
              or  nota-fiscal.esp-docto     <> 20 then do:
                  if  it-nota-fisc.vl-despes-it > 0
                  and it-nota-fisc.vl-ipi-it    > 0
                  and it-nota-fisc.cd-trib-ipi <> 3 then do:
                  
                      {ftp/ft0513.i1}  /* Determina valor do IPI */
                  
                      assign de-vl-ipi-it = de-vl-ipi-it / de-conv.
                  
                      put it-nota-fisc.aliquota-ipi                at 205
                          de-vl-ipi-it     format ">>>>>>>,>>9.99" at 213.
                  end.
                  else do:
                      if it-nota-fisc.cd-trib-ipi <> 3 then
                          put it-nota-fisc.aliquota-ipi            at 205
                              it-nota-fisc.vl-ipi-it / de-conv
                                           format ">>>>>>>,>>9.99" at 213.
                  end.
              end.
      end.

      /*------------------  NOTA FISCAL FATURA DE OUTRAS REGIOES ------------*/

      else do:
          put it-nota-fisc.vl-preuni   format ">>>,>>>,>>9.9999"   at 141
              it-nota-fisc.vl-merc-liq format ">>>>>,>>>,>>9.99" at 158
              de-desc                  format ">>>>>>>9.99"      at 180.
          if it-nota-fisc.cd-trib-icm <> 3 then
              put it-nota-fisc.aliquota-icm                      at 196.

          if  it-nota-fisc.aliquota-ipi <> 0
          or  it-nota-fisc.vl-ipi-it    <> 0
          or  nota-fiscal.esp-docto     <> 20 then do:
              if  it-nota-fisc.vl-despes-it > 0
              and it-nota-fisc.vl-ipi-it    > 0
              and it-nota-fisc.cd-trib-ipi <> 3 then do:
              
                  {ftp/ft0513.i1}  /* Determina valor do IPI */
              
                  put it-nota-fisc.aliquota-ipi                          at 205
                      de-vl-ipi-it               format ">>>>>>>,>>9.99" at 213.
              end.
              else do:
                  if it-nota-fisc.cd-trib-ipi <> 3 then
                      put it-nota-fisc.aliquota-ipi                      at 205
                          it-nota-fisc.vl-ipi-it format ">>>>>>>,>>9.99" at 213.
              end.
          end.
      end.

      /*----------------------------  ACUMULA TOTAIS  -----------------------*/

      assign de-tot-bas-icm   = de-tot-bas-icm   + it-nota-fisc.vl-bicms-it
             de-tot-icm       = de-tot-icm       + (if it-nota-fisc.cd-trib-icm <> 3
                                                    then it-nota-fisc.vl-icms-it
                                                    else 0)
             de-tot-bas-ipi   = de-tot-bas-ipi   + it-nota-fisc.vl-bipi-it
             de-tot-ipi       = de-tot-ipi       + (if it-nota-fisc.cd-trib-ipi <> 3
                                                    then it-nota-fisc.vl-ipi-it
                                                    else 0)
             de-tot-bas-iss   = de-tot-bas-iss   + it-nota-fisc.vl-biss-it
             de-tot-iss       = de-tot-iss       + it-nota-fisc.vl-iss-it
             de-vl-serv       = if  natur-oper.tipo   = 3
                                and item.aliquota-iss > 0 then
                                    de-vl-serv + it-nota-fisc.vl-merc-ori
                                else 0
             de-tot-icmssubs  = de-tot-icmssubs  + it-nota-fisc.vl-icmsub-it
             de-tot-bicmssubs = de-tot-bicmssubs + it-nota-fisc.vl-bsubs-it
             i-cont-item      = i-cont-item      + 1. /* controle de linhas */
  end.

  put skip (16 - i-cont-item).  /* nr de linhas a serem puladas */

  /*---------------------------  IMPRIME TOTAIS  ----------------------------*/

  if  de-tot-bas-iss <> 0 then
      assign de-aliquota-iss = de-tot-iss * 100 / de-tot-bas-iss.
  else
      assign de-aliquota-iss = 0.

  if  de-tot-icmssubs-obs <> 0 then
      if  avail estabelec
      and avail emitente  
      and estabelec.estado = "SP"
      and emitente.estado  = "SP" then 
          assign de-tot-icm = 0.
  /* 29/06/00 - SEEBER FASTPLAS ----------------------------------------------
  put de-tot-bas-iss  format ">>>,>>>,>>9.99" at 07
      de-aliquota-iss format ">>>,>>>,>>9.99" at 37
      de-tot-iss      format ">>>,>>>,>>9.99" at 69.
  ----------------------------------------------------------------------------*/
  /* fastplas 06/06/2003 */
  PUT FILL(" ",14)  AT 07
      FILL(" ",14)  AT 37
      FILL(" ",14)  AT 69.
  if  natur-oper.tipo   = 3
  and item.aliquota-iss > 0 then
      put de-vl-serv format ">>>,>>>,>>9.99" at 127 skip(2).
  else
      /* 29/06/00 - SEEBER FASTPLAS ----------------------------------------------
      put de-tot-iss format ">>>,>>>,>>9.99" at 127 skip(2).
      ----------------------------------------------------------------------------*/
      /* 29/06/00 - SEEBER FASTPLAS ----------------------------------------------*/
      put fill(" ",14) AT 127 skip(2).
     /*--------------------------------------------------------------------------*/
  if  de-tot-bas-icm > 0 then
      put de-tot-bas-icm   format ">>>,>>>,>>9.99" at 07.

  put de-tot-icm       format ">>>,>>>,>>9.99" at 37
      de-tot-bicmssubs format ">>>,>>>,>>9.99" at 69
      de-tot-icmssubs  format ">>>,>>>,>>9.99" at 95.

  if  natur-oper.tipo   = 3
  and item.aliquota-iss > 0 then
      put "0,00" at 137 skip (1).
  else
      put nota-fiscal.vl-mercad format ">>>,>>>,>>9.99" at 127 skip(1).
      put nota-fiscal.vl-frete     format ">>>,>>>,>>9.99" at  07
      nota-fiscal.vl-seguro    format ">>>,>>>,>>9.99" at  37
      nota-fiscal.vl-embalagem format ">>>,>>>,>>9.99" at  69
      de-tot-ipi               format ">>>,>>>,>>9.99" at  95
      nota-fiscal.vl-tot-nota  format ">>>,>>>,>>9.99" at 127 skip(2).

  /*------------------  TRANSPORTADORA E DESPESAS ACESSORIAS  ---------------*/

  find transporte
       where transporte.nome-abrev = nota-fiscal.nome-trans no-lock no-error.

  if  nota-fiscal.cidade-cif <> "" then
      assign c-pago = "1".
  else
      assign c-pago = "2".

  /*------  ESPECIE  ------*/

  for each  nota-embal use-index ch-nota-emb
      where nota-embal.cod-estabel = nota-fiscal.cod-estabel
      and   nota-embal.serie       = nota-fiscal.serie
      and   nota-embal.nr-nota-fis = nota-fiscal.nr-nota-fis no-lock
      break by nota-embal.sigla-emb:

      if  first (nota-embal.sigla-emb) then
          assign c-especie = nota-embal.sigla-emb.
      else
          assign c-especie = c-especie + "/" + nota-embal.sigla-emb.

      accumulate nota-embal.qt-volumes (total).

      if  last (nota-embal.sigla-emb) then
          assign i-qt-volumes = accum total nota-embal.qt-volumes.
  end.

  /*------  TRANSPORTADORA  ------*/

  if  avail transporte then do:
      put transporte.nome format "x(35)"   at  05
          c-pago                           at  83
          nota-fiscal.placa                at  89
          nota-fiscal.uf-placa             at 107
          transporte.cgc                   at 117 skip(1).

      put transporte.endereco format "x(35)" at  05
          transporte.cidade   format "x(15)" at  68
          transporte.estado                  at 107
          transporte.ins-estadual            at 117 skip(1).

      /*------  DADOS DOS VOLUMES  ------*/

      put i-qt-volumes             format ">>>>>9.99"       at  05
          c-especie                format "x(18)"           at  23
          nota-fiscal.marca-volume                          at  58
          nota-fiscal.nr-volumes                            at  83
          nota-fiscal.peso-bru-tot format ">>>,>>>,>>9.999" at 103

                          /* linhas a serem puladas no final da nota */

          nota-fiscal.peso-liq-tot format ">>>,>>>,>>9.999" at 128 skip(6).

      put c-num-nota at 205 skip(2).
  end.
  else do:
      put c-pago at 82 skip(3).

      /*------  DADOS DOS VOLUMES  ------*/

      put i-qt-volumes             format ">>>>>9.99"       at  05
          c-especie                format "x(18)"           at  23
          nota-fiscal.marca-volume format "x(05)"           at  58
          nota-fiscal.nr-volumes   format ">>>,>>>,>>>"     at  83
          nota-fiscal.peso-bru-tot format ">>>,>>>,>>9.999" at 103

                          /* linhas a serem puladas no final da nota */

      nota-fiscal.peso-liq-tot format ">>>,>>>,>>9.999" at 128 skip(6).
      /*
      put c-num-nota at 210 skip(2).
      */
      /* 06/06/2003 fastplas */
      put  "   " c-num-nota at 210 skip(2).
      /* ---------------------- */
  end.

