&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgesp            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i V01NR001 2.06.00.000}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i V01NR001 MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES es-nfr-peca
&Scoped-define FIRST-EXTERNAL-TABLE es-nfr-peca


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR es-nfr-peca.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS es-nfr-peca.cod-estabel es-nfr-peca.serie ~
es-nfr-peca.nr-nota-fis es-nfr-peca.nr-seq-fat es-nfr-peca.it-codigo ~
es-nfr-peca.dt-trans es-nfr-peca.quantidade 
&Scoped-define ENABLED-TABLES es-nfr-peca
&Scoped-define FIRST-ENABLED-TABLE es-nfr-peca
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold 
&Scoped-Define DISPLAYED-FIELDS es-nfr-peca.cod-estabel es-nfr-peca.serie ~
es-nfr-peca.nr-nota-fis es-nfr-peca.nr-seq-fat es-nfr-peca.it-codigo ~
es-nfr-peca.dt-trans es-nfr-peca.quantidade es-nfr-peca.ind-calculado 
&Scoped-define DISPLAYED-TABLES es-nfr-peca
&Scoped-define FIRST-DISPLAYED-TABLE es-nfr-peca
&Scoped-Define DISPLAYED-OBJECTS fi-desc-item fi-cod-emitente fi-nome-emit 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-CREATE-FIELDS es-nfr-peca.serie es-nfr-peca.nr-nota-fis ~
es-nfr-peca.nr-seq-fat es-nfr-peca.it-codigo 
&Scoped-define ADM-ASSIGN-FIELDS es-nfr-peca.dt-trans ~
es-nfr-peca.quantidade 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fi-cod-emitente AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 10.14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-item AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 44.86 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-emit AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 51.86 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 5.25.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 3.58.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     es-nfr-peca.cod-estabel AT ROW 1.17 COL 18 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 6 BY .88
     es-nfr-peca.serie AT ROW 2.17 COL 18 COLON-ALIGNED WIDGET-ID 14
          VIEW-AS FILL-IN 
          SIZE 6 BY .88
     es-nfr-peca.nr-nota-fis AT ROW 3.17 COL 18 COLON-ALIGNED WIDGET-ID 8
          LABEL "Nota Fiscal"
          VIEW-AS FILL-IN 
          SIZE 15 BY .88
     es-nfr-peca.nr-seq-fat AT ROW 4.17 COL 18 COLON-ALIGNED WIDGET-ID 10
          LABEL "Seq Fat"
          VIEW-AS FILL-IN 
          SIZE 6 BY .88
     es-nfr-peca.it-codigo AT ROW 5.17 COL 18 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 17 BY .88
     fi-desc-item AT ROW 5.17 COL 36.14 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     fi-cod-emitente AT ROW 6.71 COL 18 COLON-ALIGNED WIDGET-ID 18
     fi-nome-emit AT ROW 6.71 COL 29.14 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     es-nfr-peca.dt-trans AT ROW 7.71 COL 18 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 12.57 BY .88
     es-nfr-peca.quantidade AT ROW 8.71 COL 18 COLON-ALIGNED WIDGET-ID 12
          VIEW-AS FILL-IN 
          SIZE 20.57 BY .88
     es-nfr-peca.ind-calculado AT ROW 8.71 COL 58 WIDGET-ID 24
          VIEW-AS TOGGLE-BOX
          SIZE 15 BY .83
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 6.42 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE  WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgesp.es-nfr-peca
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 9.17
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN es-nfr-peca.dt-trans IN FRAME f-main
   2                                                                    */
/* SETTINGS FOR FILL-IN fi-cod-emitente IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-item IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-nome-emit IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX es-nfr-peca.ind-calculado IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN es-nfr-peca.it-codigo IN FRAME f-main
   1                                                                    */
/* SETTINGS FOR FILL-IN es-nfr-peca.nr-nota-fis IN FRAME f-main
   1 EXP-LABEL                                                          */
/* SETTINGS FOR FILL-IN es-nfr-peca.nr-seq-fat IN FRAME f-main
   1 EXP-LABEL                                                          */
/* SETTINGS FOR FILL-IN es-nfr-peca.quantidade IN FRAME f-main
   2                                                                    */
/* SETTINGS FOR FILL-IN es-nfr-peca.serie IN FRAME f-main
   1                                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME es-nfr-peca.cod-estabel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.cod-estabel V-table-Win
ON F5 OF es-nfr-peca.cod-estabel IN FRAME f-main /* Estabelecimento */
DO:

    {include/zoomvar.i &prog-zoom=dizoom/z04di088.w
                       &campo=es-nfr-peca.cod-estabel
                       &campozoom=cod-estabel
                       &campo2=es-nfr-peca.serie
                       &campozoom2=serie
                       &campo3=es-nfr-peca.nr-nota-fis
                       &campozoom3=nr-nota-fis
                       &campo4=es-nfr-peca.nr-seq-fat
                       &campozoom4=nr-seq-fat
                       &campo5=es-nfr-peca.it-codigo
                       &campozoom5=it-codigo}
                       
                       


END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.cod-estabel V-table-Win
ON LEAVE OF es-nfr-peca.cod-estabel IN FRAME f-main /* Estabelecimento */
DO:
  RUN pi-leave-campo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.cod-estabel V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-nfr-peca.cod-estabel IN FRAME f-main /* Estabelecimento */
DO:
  APPLY "F5":U  TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-nfr-peca.it-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.it-codigo V-table-Win
ON F5 OF es-nfr-peca.it-codigo IN FRAME f-main /* Item */
DO:
    {include/zoomvar.i &prog-zoom=dizoom/z04di088.w
                       &campo=es-nfr-peca.cod-estabel
                       &campozoom=cod-estabel
                       &campo2=es-nfr-peca.serie
                       &campozoom2=serie
                       &campo3=es-nfr-peca.nr-nota-fis
                       &campozoom3=nr-nota-fis
                       &campo4=es-nfr-peca.nr-seq-fat
                       &campozoom4=nr-seq-fat
                       &campo5=es-nfr-peca.it-codigo
                       &campozoom5=it-codigo}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.it-codigo V-table-Win
ON LEAVE OF es-nfr-peca.it-codigo IN FRAME f-main /* Item */
DO:

    RUN pi-leave-campo.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.it-codigo V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-nfr-peca.it-codigo IN FRAME f-main /* Item */
DO:
  APPLY "F5":U  TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-nfr-peca.nr-nota-fis
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.nr-nota-fis V-table-Win
ON F5 OF es-nfr-peca.nr-nota-fis IN FRAME f-main /* Nota Fiscal */
DO:
    {include/zoomvar.i &prog-zoom=dizoom/z04di088.w
                       &campo=es-nfr-peca.cod-estabel
                       &campozoom=cod-estabel
                       &campo2=es-nfr-peca.serie
                       &campozoom2=serie
                       &campo3=es-nfr-peca.nr-nota-fis
                       &campozoom3=nr-nota-fis
                       &campo4=es-nfr-peca.nr-seq-fat
                       &campozoom4=nr-seq-fat
                       &campo5=es-nfr-peca.it-codigo
                       &campozoom5=it-codigo}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.nr-nota-fis V-table-Win
ON LEAVE OF es-nfr-peca.nr-nota-fis IN FRAME f-main /* Nota Fiscal */
DO:
  RUN pi-leave-campo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.nr-nota-fis V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-nfr-peca.nr-nota-fis IN FRAME f-main /* Nota Fiscal */
DO:
  APPLY "F5":U  TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-nfr-peca.nr-seq-fat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.nr-seq-fat V-table-Win
ON F5 OF es-nfr-peca.nr-seq-fat IN FRAME f-main /* Seq Fat */
DO:
    {include/zoomvar.i &prog-zoom=dizoom/z04di088.w
                       &campo=es-nfr-peca.cod-estabel
                       &campozoom=cod-estabel
                       &campo2=es-nfr-peca.serie
                       &campozoom2=serie
                       &campo3=es-nfr-peca.nr-nota-fis
                       &campozoom3=nr-nota-fis
                       &campo4=es-nfr-peca.nr-seq-fat
                       &campozoom4=nr-seq-fat
                       &campo5=es-nfr-peca.it-codigo
                       &campozoom5=it-codigo}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.nr-seq-fat V-table-Win
ON LEAVE OF es-nfr-peca.nr-seq-fat IN FRAME f-main /* Seq Fat */
DO:
  RUN pi-leave-campo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.nr-seq-fat V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-nfr-peca.nr-seq-fat IN FRAME f-main /* Seq Fat */
DO:
  APPLY "F5":U  TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-nfr-peca.serie
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.serie V-table-Win
ON F5 OF es-nfr-peca.serie IN FRAME f-main /* S�rie */
DO:
    {include/zoomvar.i &prog-zoom=dizoom/z04di088.w
                       &campo=es-nfr-peca.cod-estabel
                       &campozoom=cod-estabel
                       &campo2=es-nfr-peca.serie
                       &campozoom2=serie
                       &campo3=es-nfr-peca.nr-nota-fis
                       &campozoom3=nr-nota-fis
                       &campo4=es-nfr-peca.nr-seq-fat
                       &campozoom4=nr-seq-fat
                       &campo5=es-nfr-peca.it-codigo
                       &campozoom5=it-codigo}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.serie V-table-Win
ON LEAVE OF es-nfr-peca.serie IN FRAME f-main /* S�rie */
DO:
  RUN pi-leave-campo.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-nfr-peca.serie V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-nfr-peca.serie IN FRAME f-main /* S�rie */
DO:
  APPLY "F5":U  TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

es-nfr-peca.cod-estabel:LOAD-MOUSE-POINTER ("image/lupa.cur")   IN FRAME {&FRAME-NAME}.
es-nfr-peca.serie:LOAD-MOUSE-POINTER ("image/lupa.cur")         IN FRAME {&FRAME-NAME}.
es-nfr-peca.nr-nota-fis:LOAD-MOUSE-POINTER ("image/lupa.cur")   IN FRAME {&FRAME-NAME}.
es-nfr-peca.nr-seq-fat:LOAD-MOUSE-POINTER ("image/lupa.cur")    IN FRAME {&FRAME-NAME}.
es-nfr-peca.it-codigo:LOAD-MOUSE-POINTER ("image/lupa.cur")     IN FRAME {&FRAME-NAME}.


  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "es-nfr-peca"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "es-nfr-peca"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
/*    {include/i-valid.i} */

    IF NOT FRAME {&FRAME-NAME}:VALIDATE() THEN
        RETURN 'ADM-ERROR':U.

    RUN pi-validate.
    IF RETURN-VALUE = "ADM-ERROR":U THEN UNDO, RETURN "ADM-ERROR":U. 
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    ASSIGN
        es-nfr-peca.cod-emitente = INPUT FRAME {&FRAME-NAME} fi-cod-emitente.
    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/


RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

RUN pi-leave-campo.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
/*     if adm-new-record = yes then */
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

    IF NOT AVAIL es-nfr-peca THEN
        DISP
            TODAY   @   es-nfr-peca.dt-trans
            WITH FRAME {&FRAME-NAME}.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available V-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF es-nfr-peca.ind-calculado:CHECKED IN FRAME {&FRAME-NAME} THEN
      RUN new-state("nao-habilitar":U).
  ELSE 
      RUN new-state("habilitar":U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-leave-campo V-table-Win 
PROCEDURE pi-leave-campo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

FOR FIRST ITEM
    FIELDS (it-codigo
            desc-item)
    WHERE ITEM.it-codigo = INPUT FRAME {&FRAME-NAME} es-nfr-peca.it-codigo
    NO-LOCK:

    fi-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.

END.

FOR FIRST nota-fiscal
    FIELDS (cod-emitente)
    WHERE nota-fiscal.cod-estabel   =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.cod-estabel
    AND   nota-fiscal.serie         =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.serie
    AND   nota-fiscal.nr-nota-fis   =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.nr-nota-fis
    NO-LOCK:

    FOR FIRST emitente
        FIELDS (cod-emitente
                nome-emit)
        WHERE emitente.cod-emitente = nota-fiscal.cod-emitente
        NO-LOCK:

        DISP
            emitente.cod-emitente   @ fi-cod-emitente
            emitente.nome-emit      @ fi-nome-emit
            WITH FRAME {&FRAME-NAME}.

    END.

END.

IF NOT AVAIL nota-fiscal THEN DO:
    DISP
        0    @ fi-cod-emitente
        ""   @ fi-nome-emit
        ""   @ fi-desc-item
        WITH FRAME {&FRAME-NAME}.


END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
DEFINE BUFFER bf-nfr-peca FOR es-nfr-peca.

    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
FIND FIRST it-nota-fisc
    WHERE it-nota-fisc.cod-estabel  =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.cod-estabel
    AND   it-nota-fisc.serie        =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.serie
    AND   it-nota-fisc.nr-nota-fis  =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.nr-nota-fis
    AND   it-nota-fisc.nr-seq-fat   =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.nr-seq-fat
    AND   it-nota-fisc.it-codigo    =   INPUT FRAME {&FRAME-NAME} es-nfr-peca.it-codigo
    NO-LOCK NO-ERROR.

IF NOT AVAIL it-nota-fisc THEN DO:

    {include/i-vldprg.i}
    RUN utp/ut-msgs.p (INPUT "show":U
                      ,INPUT 17006
                      ,INPUT "Nota-fiscal n�o encontrada.~~A nota-fiscal especificada n�o foi encontrada.").

    RETURN 'ADM-ERROR':U.

END.

IF  INPUT FRAME {&FRAME-NAME} es-nfr-peca.quantidade = 0
OR  INPUT FRAME {&FRAME-NAME} es-nfr-peca.quantidade > it-nota-fisc.qt-faturada[1]
THEN DO:

    {include/i-vldprg.i}
    RUN utp/ut-msgs.p (INPUT "show":U
                      ,INPUT 17006
                      ,INPUT "Quantidade inv�lida.~~A quantidade deve ser maior do que zero e menor ou igual a " + STRING(it-nota-fisc.qt-faturada[1]) + ".").

    RETURN 'ADM-ERROR':U.

END.

FIND FIRST nota-fiscal
    WHERE nota-fiscal.cod-estabel = INPUT FRAME {&FRAME-NAME} es-nfr-peca.cod-estabel
    AND   nota-fiscal.serie       = INPUT FRAME {&FRAME-NAME} es-nfr-peca.serie      
    AND   nota-fiscal.nr-nota-fis = INPUT FRAME {&FRAME-NAME} es-nfr-peca.nr-nota-fis
    NO-LOCK NO-ERROR.

IF  INPUT FRAME {&FRAME-NAME} es-nfr-peca.dt-trans = ?
OR  INPUT FRAME {&FRAME-NAME} es-nfr-peca.dt-trans < nota-fiscal.dt-emis-nota THEN DO:

    {include/i-vldprg.i}
    RUN utp/ut-msgs.p (INPUT "show":U
                      ,INPUT 17006
                      ,INPUT "Data inv�lida.~~A data de transa��o deve ser maior do que a data de emiss�o da nota-fiscal. (" + STRING(nota-fiscal.dt-emis-nota, "99/99/9999") + ").").

    RETURN 'ADM-ERROR':U.

END.

IF adm-new-record THEN DO:

    IF CAN-FIND(FIRST bf-nfr-peca
                WHERE bf-nfr-peca.cod-estabel = INPUT FRAME {&FRAME-NAME} es-nfr-peca.cod-estabel
                AND   bf-nfr-peca.serie       = INPUT FRAME {&FRAME-NAME} es-nfr-peca.serie      
                AND   bf-nfr-peca.nr-nota-fis = INPUT FRAME {&FRAME-NAME} es-nfr-peca.nr-nota-fis
                AND   bf-nfr-peca.nr-seq-fat  = INPUT FRAME {&FRAME-NAME} es-nfr-peca.nr-seq-fat 
                AND   bf-nfr-peca.it-codigo   = INPUT FRAME {&FRAME-NAME} es-nfr-peca.it-codigo  
                NO-LOCK) THEN DO:

        {include/i-vldprg.i}
        RUN utp/ut-msgs.p (INPUT "show":U
                          ,INPUT 17006
                          ,INPUT "Nota-fiscal j� cadastrada.~~A nota-fiscal especificada j� est� cadastrada. Selecione outra nota.").

        RETURN 'ADM-ERROR':U.

    END.

END.



/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "es-nfr-peca"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

