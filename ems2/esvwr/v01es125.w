&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i V99XX999 9.99.99.999}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

DEF VAR i-retrab AS INT.
DEF VAR i-tempo  AS INT.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES dc-param-depos
&Scoped-define FIRST-EXTERNAL-TABLE dc-param-depos


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR dc-param-depos.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS dc-param-depos.ind-fifo ~
dc-param-depos.ind-sac 
&Scoped-define ENABLED-TABLES dc-param-depos
&Scoped-define FIRST-ENABLED-TABLE dc-param-depos
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold 
&Scoped-Define DISPLAYED-FIELDS dc-param-depos.cod-depos ~
dc-param-depos.ind-fifo dc-param-depos.ind-retrabalho ~
dc-param-depos.ind-sac dc-param-depos.qtd-retrab dc-param-depos.tempo-sac 
&Scoped-define DISPLAYED-TABLES dc-param-depos
&Scoped-define FIRST-DISPLAYED-TABLE dc-param-depos
&Scoped-Define DISPLAYED-OBJECTS desc-depos 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-CREATE-FIELDS dc-param-depos.cod-depos 
&Scoped-define ADM-ASSIGN-FIELDS dc-param-depos.cod-depos 
&Scoped-define ADM-MODIFY-FIELDS dc-param-depos.cod-depos 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
cod-depos|y|y|mgfas.dc-param-depos.cod-depos
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "cod-depos",
     Keys-Supplied = "cod-depos"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE desc-depos AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 55.86 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88.57 BY 1.5.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88.57 BY 5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     dc-param-depos.cod-depos AT ROW 1.29 COL 16.86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     desc-depos AT ROW 1.29 COL 24.29 COLON-ALIGNED NO-LABEL
     dc-param-depos.ind-fifo AT ROW 2.96 COL 35
          VIEW-AS TOGGLE-BOX
          SIZE 16.14 BY .83
     dc-param-depos.ind-retrabalho AT ROW 3.79 COL 35
          VIEW-AS TOGGLE-BOX
          SIZE 26.57 BY .83
     dc-param-depos.ind-sac AT ROW 4.63 COL 35
          LABEL "Tempo de Cura"
          VIEW-AS TOGGLE-BOX
          SIZE 15.57 BY .83
     dc-param-depos.qtd-retrab AT ROW 5.58 COL 33 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12.57 BY .88
     dc-param-depos.tempo-sac AT ROW 6.58 COL 33 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12.57 BY .88
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 2.75 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.dc-param-depos
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 6.83
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN dc-param-depos.cod-depos IN FRAME f-main
   NO-ENABLE 1 2 3                                                      */
/* SETTINGS FOR FILL-IN desc-depos IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR TOGGLE-BOX dc-param-depos.ind-retrabalho IN FRAME f-main
   NO-ENABLE                                                            */
ASSIGN 
       dc-param-depos.ind-retrabalho:HIDDEN IN FRAME f-main           = TRUE.

/* SETTINGS FOR TOGGLE-BOX dc-param-depos.ind-sac IN FRAME f-main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN dc-param-depos.qtd-retrab IN FRAME f-main
   NO-ENABLE                                                            */
ASSIGN 
       dc-param-depos.qtd-retrab:HIDDEN IN FRAME f-main           = TRUE.

/* SETTINGS FOR FILL-IN dc-param-depos.tempo-sac IN FRAME f-main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dc-param-depos.cod-depos
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-param-depos.cod-depos V-table-Win
ON F5 OF dc-param-depos.cod-depos IN FRAME f-main /* Dep�sito */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z01in084.w
                        &campo=dc-param-depos.cod-depos
                        &campozoom=cod-depos
                        &campo2=desc-depos
                        &campozoom2=nome}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-param-depos.cod-depos V-table-Win
ON LEAVE OF dc-param-depos.cod-depos IN FRAME f-main /* Dep�sito */
DO:
  FIND deposito WHERE deposito.cod-depos = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos NO-LOCK NO-ERROR.
  IF AVAIL deposito THEN
     ASSIGN desc-depos:SCREEN-VALUE IN FRAME {&FRAME-NAME} = deposito.nome.
  ELSE
     ASSIGN desc-depos:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-param-depos.cod-depos V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-param-depos.cod-depos IN FRAME f-main /* Dep�sito */
DO:
  APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-param-depos.ind-retrabalho
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-param-depos.ind-retrabalho V-table-Win
ON VALUE-CHANGED OF dc-param-depos.ind-retrabalho IN FRAME f-main /* Retrabalhos no Dep�sito */
DO:
  IF dc-param-depos.ind-retrabalho:CHECKED IN FRAME {&FRAME-NAME} = TRUE THEN
     ASSIGN dc-param-depos.qtd-retrab:SENSITIVE IN FRAME {&FRAME-NAME} = YES.
  ELSE
     ASSIGN dc-param-depos.qtd-retrab:SENSITIVE IN FRAME {&FRAME-NAME} = NO
            dc-param-depos.qtd-retrab:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "0".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-param-depos.ind-sac
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-param-depos.ind-sac V-table-Win
ON VALUE-CHANGED OF dc-param-depos.ind-sac IN FRAME f-main /* Tempo de Cura */
DO:
  IF dc-param-depos.ind-sac:CHECKED IN FRAME {&FRAME-NAME} = TRUE THEN
     ASSIGN dc-param-depos.tempo-sac:SENSITIVE IN FRAME {&FRAME-NAME} = YES.
  ELSE
     ASSIGN dc-param-depos.tempo-sac:SENSITIVE IN FRAME {&FRAME-NAME} = NO
            dc-param-depos.tempo-sac:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "0".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'cod-depos':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = dc-param-depos
           &WHERE = "WHERE dc-param-depos.cod-depos eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "dc-param-depos"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "dc-param-depos"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  ASSIGN dc-param-depos.qtd-retrab:SENSITIVE IN FRAME {&FRAME-NAME} = NO
         dc-param-depos.tempo-sac:SENSITIVE IN FRAME {&FRAME-NAME} = NO.
  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */

    ASSIGN /*i-retrab = INPUT FRAME {&FRAME-NAME} dc-param-depos.qtd-retrab*/
           i-tempo  = INPUT FRAME {&FRAME-NAME} dc-param-depos.tempo-sac.

    RUN pi-validate.
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    
    ASSIGN /*dc-param-depos.qtd-retrab = i-retrab*/
           dc-param-depos.tempo-sac  = i-tempo.

    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record V-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  FIND FIRST dc-movto-pto-controle
       WHERE dc-movto-pto-controle.cod-depos-orig = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos
          OR dc-movto-pto-controle.cod-depos-dest = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos NO-LOCK NO-ERROR.

  IF AVAIL dc-movto-pto-controle THEN DO:
       run utp/ut-msgs.p (input "show":U, input 3, "Dep�sito"). 
       return 'ADM-ERROR':U.                                            
  END.
  ELSE DO:
        FIND FIRST dc-pto-controle
             WHERE dc-pto-controle.cod-depos-orig = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos
                OR dc-pto-controle.cod-depos-dest = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos NO-LOCK NO-ERROR.
        IF AVAIL dc-pto-controle THEN DO:
               run utp/ut-msgs.p (input "show":U, input 3, "Dep�sito"). 
               return 'ADM-ERROR':U. 
        END.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
    ASSIGN dc-param-depos.qtd-retrab:SENSITIVE IN FRAME {&FRAME-NAME} = NO
           dc-param-depos.tempo-sac:SENSITIVE IN FRAME {&FRAME-NAME} = NO.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

    APPLY "leave" TO dc-param-depos.cod-depos IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

    APPLY 'value-changed' TO dc-param-depos.ind-retrabalho IN FRAME {&FRAME-NAME}.
    APPLY 'value-changed' TO dc-param-depos.ind-sac IN FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  dc-param-depos.cod-depos:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    {include/i-vldprg.i}
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */


        IF adm-new-record THEN
        DO:

            FIND dc-param-depos NO-LOCK
                WHERE dc-param-depos.cod-depos = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos NO-ERROR.
            IF AVAIL dc-param-depos THEN
            DO:
                RUN utp/ut-msgs.p (input "show":U, input 7, "Dep�sito").
                APPLY "choose" TO dc-param-depos.cod-depos.
                RETURN 'ADM-ERROR':U.     

            END.

        END.

        
     FIND deposito
    WHERE deposito.cod-depos = INPUT FRAME {&FRAME-NAME} dc-param-depos.cod-depos NO-LOCK NO-ERROR.
    IF NOT AVAIL deposito THEN DO:
       run utp/ut-msgs.p (input "show":U, input 2, "Dep�sito").
       return 'ADM-ERROR':U.     
    END.
    
/*     IF INPUT FRAME {&FRAME-NAME} dc-param-depos.ind-retrabalho THEN                                                                      */
/*         IF INPUT FRAME {&FRAME-NAME} dc-param-depos.qtd-retrab <= 0 THEN DO:                                                             */
/*                run utp/ut-msgs.p (input "show":U, input 17006, "Valor retrabalho~~Campo 'M�ximo retrabalho' n�o pode ser igual a zero"). */
/*                return 'ADM-ERROR':U.                                                                                                     */
/*         END.                                                                                                                             */

    IF INPUT FRAME {&FRAME-NAME} dc-param-depos.ind-sac THEN
        IF INPUT FRAME {&FRAME-NAME} dc-param-depos.tempo-sac <= 0 THEN DO:
               run utp/ut-msgs.p (input "show":U, input 17006, "Valor tempo de cura~~Campo 'Tempo de cura' n�o pode ser igual a zero").
               return 'ADM-ERROR':U.    
        END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "cod-depos" "dc-param-depos" "cod-depos"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "dc-param-depos"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

