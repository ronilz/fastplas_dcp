&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          movind           PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wlinha AS CHAR FORMAT "X(145)".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wpedido wrel bt-executar bt-sair 
&Scoped-Define DISPLAYED-OBJECTS wpedido wrel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 15 BY 1.13.

DEFINE BUTTON bt-sair DEFAULT 
     LABEL "Sair" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wpedido LIKE pedido-compr.num-pedido
     VIEW-AS FILL-IN 
     SIZE 11.43 BY 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(40)":U INITIAL "c:~\spool~\pedido.lst" 
     LABEL "Nome do Relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wpedido AT ROW 2 COL 30 COLON-ALIGNED
     wrel AT ROW 3.5 COL 30 COLON-ALIGNED
     bt-executar AT ROW 6 COL 21
     bt-sair AT ROW 6 COL 42
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 73.72 BY 8.25
         DEFAULT-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Lista �ltima NF recebida do pedido - wpcc0002"
         HEIGHT             = 8.25
         WIDTH              = 73.72
         MAX-HEIGHT         = 29.79
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.79
         VIRTUAL-WIDTH      = 146.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* SETTINGS FOR FILL-IN wpedido IN FRAME DEFAULT-FRAME
   LIKE = movind.pedido-compr.num-pedido EXP-SIZE                       */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Lista �ltima NF recebida do pedido - wpcc0002 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Lista �ltima NF recebida do pedido - wpcc0002 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN wrel wpedido.
  RUN executar.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wpedido wrel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE wpedido wrel bt-executar bt-sair 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE executar C-Win 
PROCEDURE executar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

FORM HEADER
    "Listagem da ultima nota recebida para o pedido/parcela"  AT 50
    "Pagina:" AT 120 PAGE-NUMBER FORMAT "9999"
    wlinha
    SKIP(2)
    WITH FRAME f-lista.

ASSIGN wlinha = FILL("-",145).
DISABLE bt-executar bt-sair WITH FRAME    {&FRAME-NAME}.
OUTPUT TO value(wrel) PAGE-SIZE 60.
FOR EACH pedido-compr WHERE pedido-compr.num-pedido = wpedido
                       /* WHERE cod-emitente = 113 */NO-LOCK:
    DISPLAY pedido-compr.cod-emitente COLUMN-LABEL "Emitente"
            pedido-compr.num-pedido   COLUMN-LABEL "Pedido" 
            WITH FRAME f-lista.
    FOR EACH ordem-compra WHERE ordem-compra.num-pedido = pedido-compr.num-pedido NO-LOCK
                          BREAK BY ordem-compra.numero-ordem:
      IF LAST-OF(ordem-compra.numero-ordem) THEN PUT SKIP  wlinha. 
      DISPLAY ordem-compra.numero-ordem COLUMN-LABEL "Ordem"
              WITH FRAME f-lista.
      FOR EACH prazo-compra OF ordem-compra NO-LOCK:
        DISPLAY prazo-compra.parcela    COLUMN-LABEL "Parcela"
                ordem-compra.it-codigo  COLUMN-LABEL "Item"
                WITH FRAME f-lista.
         FOR EACH  recebimento WHERE   recebimento.cod-movto    = 1
                       AND recebimento.num-pedido   = pedido-compr.num-pedido
                       AND recebimento.numero-ordem = ordem-compra.numero-ordem
                       AND recebimento.parcela      = prazo-compra.parcela
                       AND recebimento.it-codigo    = ordem-compra.it-codigo
                       AND recebimento.cod-emitente = pedido-compr.cod-emitente
                       NO-LOCK BREAK BY recebimento.it-codigo BY recebimento.data-nota
                       BY recebimento.numero-nota:
            IF  last-of(recebimento.it-codigo) AND LAST-OF(recebimento.data-nota)  
            AND LAST-OF(recebimento.numero-nota)
            THEN DO:
         
          DISPLAY /*recebimento.num-pedido recebimento.numero-ordem recebimento.num-pedido 
                      recebimento.it-codigo RECEBIMENTO.PARCELA*/
                      substring(ordem-compra.narrativa,1,6) COLUMN-LABEL "Nota!Corte!Narrativa!Pedido"
                      recebimento.data-nota                 COLUMN-LABEL "Dt.Nota"
                      substring(recebimento.numero-nota,2,6) COLUMN-LABEL "Nr.Nota"
                      WITH FRAME F-LISTA WIDTH 200 DOWN STREAM-IO.
           IF (substring(ordem-compra.narrativa,1,6)) =  (SUBSTRING(recebimento.numero-nota,2,6)) 
           THEN DISPLAY "*** Nota de corte da narrativa IGUAL a ultima nota recebida" COLUMN-LABEL "Mensagem" WITH FRAME f-lista.
           DOWN WITH FRAME f-lista .
                END.
              END.
            END.
          END.
       END.
MESSAGE "Gera��o Conclu�da para o pedido:" wpedido VIEW-AS ALERT-BOX. 
ENABLE bt-executar bt-sair WITH FRAME  {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE executar-1 C-Win 
PROCEDURE executar-1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

