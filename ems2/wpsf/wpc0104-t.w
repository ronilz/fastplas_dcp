&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          movind           PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE  ww-item        AS CHAR FORMAT "X(16)" NO-UNDO.
DEFINE VARIABLE  ww-data-ini    AS DATE FORMAT "99/99/9999".
DEFINE VARIABLE  ww-data-fim    AS DATE FORMAT "99/99/9999".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-2

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES doc-fisico it-doc-fisico

/* Definitions for BROWSE BROWSE-2                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-2 doc-fisico.cod-estabel ~
it-doc-fisico.num-pedido it-doc-fisico.numero-ordem doc-fisico.cod-emitente ~
it-doc-fisico.it-codigo it-doc-fisico.parcela it-doc-fisico.serie-docto ~
doc-fisico.nro-docto doc-fisico.dt-trans doc-fisico.dt-emissao ~
it-doc-fisico.preco-unit[1] it-doc-fisico.quantidade doc-fisico.situacao 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-2 
&Scoped-define OPEN-QUERY-BROWSE-2 OPEN QUERY BROWSE-2 FOR EACH doc-fisico ~
      WHERE doc-fisico.situacao = 2 ~
 AND doc-fisico.dt-trans = TODAY ~
 NO-LOCK, ~
      EACH it-doc-fisico OF doc-fisico NO-LOCK ~
    BY it-doc-fisico.it-codigo INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-2 doc-fisico it-doc-fisico
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-2 doc-fisico
&Scoped-define SECOND-TABLE-IN-QUERY-BROWSE-2 it-doc-fisico


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-2}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wdata-ini wdata-fim BtnFirst BtnLast ~
BUTTON-6 BUTTON-4 BUTTON-1 BUTTON-5 BROWSE-2 RECT-12 RECT-3 
&Scoped-Define DISPLAYED-OBJECTS wdata-ini wdata-fim 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON BtnFirst 
     IMAGE-UP FILE "image/iu-pp.bmp":U
     LABEL "&First" 
     SIZE 5 BY 1.25 TOOLTIP "Primeiro registro da consulta"
     BGCOLOR 8 .

DEFINE BUTTON BtnLast 
     IMAGE-UP FILE "image/iu-ff.bmp":U
     LABEL "&Last" 
     SIZE 5 BY 1.25 TOOLTIP "�ltimo registro da consulta"
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Button 1" 
     SIZE 5 BY 1.25 TOOLTIP "Procura determinado item".

DEFINE BUTTON BUTTON-4 
     IMAGE-UP FILE "image/im-zoom.bmp":U
     LABEL "Todos" 
     SIZE 5 BY 1.25 TOOLTIP "Consulta Recebimento F�sico conforme sele��o".

DEFINE BUTTON BUTTON-5 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Button 5" 
     SIZE 5 BY 1.25.

DEFINE BUTTON BUTTON-6 
     IMAGE-UP FILE "image/im-param.bmp":U
     LABEL "Par�metros" 
     SIZE 5 BY 1.25 TOOLTIP "Altera data de transa��o para consulta".

DEFINE VARIABLE wdata-fim AS DATE FORMAT "99/99/9999":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE wdata-ini AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 16 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 101 BY 1.75.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 101 BY 1.5
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-2 FOR 
      doc-fisico, 
      it-doc-fisico SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-2 C-Win _STRUCTURED
  QUERY BROWSE-2 NO-LOCK DISPLAY
      doc-fisico.cod-estabel FORMAT "x(3)":U
      it-doc-fisico.num-pedido FORMAT ">>>>>,>>9":U WIDTH 7.72
      it-doc-fisico.numero-ordem FORMAT "zzzzz9,99":U WIDTH 7.43
      doc-fisico.cod-emitente FORMAT ">>>>>>>>9":U
      it-doc-fisico.it-codigo FORMAT "X(16)":U
      it-doc-fisico.parcela FORMAT ">>>>9":U WIDTH 3.57
      it-doc-fisico.serie-docto FORMAT "x(5)":U WIDTH 4.43
      doc-fisico.nro-docto FORMAT "x(16)":U WIDTH 8.43
      doc-fisico.dt-trans FORMAT "99/99/9999":U
      doc-fisico.dt-emissao FORMAT "99/99/9999":U
      it-doc-fisico.preco-unit[1] FORMAT ">>>,>>>,>>9.99999":U
            WIDTH 11.72
      it-doc-fisico.quantidade COLUMN-LABEL "Qtde" FORMAT ">>>>>,>>9.9999":U
            WIDTH 10
      doc-fisico.situacao COLUMN-LABEL "S" FORMAT ">9":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 103 BY 15.5
         FONT 1 ROW-HEIGHT-CHARS .5 EXPANDABLE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wdata-ini AT ROW 1.75 COL 33 COLON-ALIGNED NO-LABEL
     wdata-fim AT ROW 1.75 COL 51 COLON-ALIGNED
     BtnFirst AT ROW 3.25 COL 26
     BtnLast AT ROW 3.25 COL 31
     BUTTON-6 AT ROW 3.25 COL 45
     BUTTON-4 AT ROW 3.25 COL 50
     BUTTON-1 AT ROW 3.25 COL 55
     BUTTON-5 AT ROW 3.25 COL 68
     BROWSE-2 AT ROW 5 COL 2
     RECT-12 AT ROW 3 COL 2
     RECT-3 AT ROW 1.25 COL 2
     "Data de Transa��o" VIEW-AS TEXT
          SIZE 14 BY .54 AT ROW 1 COL 45
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 104 BY 19.63
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Consulta Recebimento F�sico - wpc0104"
         HEIGHT             = 19.63
         WIDTH              = 104
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* BROWSE-TAB BROWSE-2 BUTTON-5 DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-2
/* Query rebuild information for BROWSE BROWSE-2
     _TblList          = "movind.doc-fisico,movind.it-doc-fisico OF movind.doc-fisico"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "movind.it-doc-fisico.it-codigo|yes"
     _Where[1]         = "movind.doc-fisico.situacao = 2
 AND movind.doc-fisico.dt-trans = TODAY
"
     _FldNameList[1]   = movind.doc-fisico.cod-estabel
     _FldNameList[2]   > movind.it-doc-fisico.num-pedido
"it-doc-fisico.num-pedido" ? ? "integer" ? ? ? ? ? ? no ? no no "7.72" yes no no "U" "" ""
     _FldNameList[3]   > movind.it-doc-fisico.numero-ordem
"it-doc-fisico.numero-ordem" ? ? "integer" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" ""
     _FldNameList[4]   = movind.doc-fisico.cod-emitente
     _FldNameList[5]   = movind.it-doc-fisico.it-codigo
     _FldNameList[6]   > movind.it-doc-fisico.parcela
"it-doc-fisico.parcela" ? ? "integer" ? ? ? ? ? ? no ? no no "3.57" yes no no "U" "" ""
     _FldNameList[7]   > movind.it-doc-fisico.serie-docto
"it-doc-fisico.serie-docto" ? ? "character" ? ? ? ? ? ? no ? no no "4.43" yes no no "U" "" ""
     _FldNameList[8]   > movind.doc-fisico.nro-docto
"doc-fisico.nro-docto" ? ? "character" ? ? ? ? ? ? no ? no no "8.43" yes no no "U" "" ""
     _FldNameList[9]   = movind.doc-fisico.dt-trans
     _FldNameList[10]   = movind.doc-fisico.dt-emissao
     _FldNameList[11]   > movind.it-doc-fisico.preco-unit[1]
"it-doc-fisico.preco-unit[1]" ? ? "decimal" ? ? ? ? ? ? no ? no no "11.72" yes no no "U" "" ""
     _FldNameList[12]   > movind.it-doc-fisico.quantidade
"it-doc-fisico.quantidade" "Qtde" ? "decimal" ? ? ? ? ? ? no ? no no "10" yes no no "U" "" ""
     _FldNameList[13]   > movind.doc-fisico.situacao
"doc-fisico.situacao" "S" ? "integer" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is OPENED
*/  /* BROWSE BROWSE-2 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Consulta Recebimento F�sico - wpc0104 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Consulta Recebimento F�sico - wpc0104 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-2
&Scoped-define SELF-NAME BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-2 C-Win
ON VALUE-CHANGED OF BROWSE-2 IN FRAME DEFAULT-FRAME
DO: /*
  ASSIGN ww-item = it-doc-fisico.it-codigo.
  ASSIGN wdata-ini wdata-fim.
  OPEN QUERY BROWSE-2 FOR EACH doc-fisico WHERE doc-fisico.dt-trans GE wdata-ini
                                            AND  doc-fisico.dt-trans LE wdata-fim 
                                            AND  doc-fisico.situacao = 2 NO-LOCK , 
        EACH it-doc-fisico OF doc-fisico WHERE it-doc-fisico.it-codigo = ww-item NO-LOCK
        BY doc-fisico.dt-emissao DESCENDING 
        BY it-doc-fisico.it-codigo INDEXED-REPOSITION.*/

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnFirst
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnFirst C-Win
ON CHOOSE OF BtnFirst IN FRAME DEFAULT-FRAME /* First */
DO: /*
  GET FIRST browse-2.
  REPOSITION browse-2 TO ROWID ROWID(doc-fisico).*/
  REPOSITION browse-2 TO ROW 1.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnLast
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnLast C-Win
ON CHOOSE OF BtnLast IN FRAME DEFAULT-FRAME /* Last */
DO:
 GET LAST browse-2.
 REPOSITION browse-2 TO ROWID ROWID(doc-fisico).
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  RUN wpsf/wpc0104a (OUTPUT ww-item).
  ASSIGN wdata-ini wdata-fim.
 OPEN QUERY BROWSE-2 FOR EACH doc-fisico WHERE doc-fisico.dt-trans GE wdata-ini
                                          AND  doc-fisico.dt-trans LE wdata-fim 
                                          AND  doc-fisico.situacao = 2 NO-LOCK , 
      EACH it-doc-fisico OF doc-fisico WHERE it-doc-fisico.it-codigo = ww-item NO-LOCK
      BY doc-fisico.dt-emissao DESCENDING 
      BY it-doc-fisico.it-codigo INDEXED-REPOSITION.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-4 C-Win
ON CHOOSE OF BUTTON-4 IN FRAME DEFAULT-FRAME /* Todos */
DO:
    ASSIGN wdata-ini wdata-fim.
    OPEN QUERY BROWSE-2 FOR EACH doc-fisico WHERE doc-fisico.dt-trans GE wdata-ini
                                            AND   doc-fisico.dt-trans LE wdata-fim 
                                            AND   doc-fisico.situacao = 2 NO-LOCK, 
      EACH it-doc-fisico OF doc-fisico NO-LOCK
      BY doc-fisico.dt-emissao DESCENDING 
      BY it-doc-fisico.it-codigo INDEXED-REPOSITION.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-5 C-Win
ON CHOOSE OF BUTTON-5 IN FRAME DEFAULT-FRAME /* Button 5 */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-6 C-Win
ON CHOOSE OF BUTTON-6 IN FRAME DEFAULT-FRAME /* Par�metros */
DO:
   /*RUN wpsf/wpc0104b (OUTPUT ww-data-ini, OUTPUT ww-data-fim).*/
 ENABLE wdata-ini wdata-fim WITH FRAME   {&FRAME-NAME}.
 APPLY "ENTRY" TO wdata-ini.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wdata-ini wdata-fim 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE wdata-ini wdata-fim BtnFirst BtnLast BUTTON-6 BUTTON-4 BUTTON-1 
         BUTTON-5 BROWSE-2 RECT-12 RECT-3 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*DISABLE wdata-fim wdata-ini WITH FRAME   {&FRAME-NAME}.*/
ASSIGN   wdata-ini = TODAY
         wdata-fim = TODAY. 
DISPLAY wdata-ini wdata-fim WITH FRAME   {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE procura C-Win 
PROCEDURE procura :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

