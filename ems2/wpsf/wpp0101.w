&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wrel            AS CHAR FORMAT "X(20)"
                                INITIAL "./spool/CADPLANO.LST".
DEFINE VARIABLE r-registro      AS RECID INITIAL ?.
DEFINE VARIABLE l-resposta      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wrecidant       AS RECID INITIAL ?.
DEFINE VARIABLE wconf           AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wcur            AS INTE FORMAT "99".
DEFINE VARIABLE wfor-ini        AS INTE FORMAT ">>>>>9".
DEFINE VARIABLE wfor-fim        AS INTE FORMAT ">>>>>9".
DEFINE VARIABLE wpl-ini         AS CHAR FORMAT "999999X".
DEFINE VARIABLE wnr-plfim       AS CHAR FORMAT "999999X".
DEFINE VARIABLE wnr-plini       AS CHAR FORMAT "999999X". 
DEFINE VARIABLE wobs            AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wresp           AS LOGICAL FORMAT "Todo/Individual".
DEFINE VARIABLE wresp1          AS LOGICAL FORMAT "Sim/Nao".
/* Variaveis dos arquivos --------------------------------------------------*/
DEFINE VARIABLE wseq            LIKE lo-matplano.seq-it.
DEFINE VARIABLE wcriaplano      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wdatapesq       LIKE recebimento.data-nota.

/* Variaveis do IT-PL ------------------------------------------------------*/
DEFINE VARIABLE witini  LIKE lo-matplano.it-codigo INITIAL 0.
DEFINE VARIABLE witfim  LIKE lo-matplano.it-codigo INITIAL 0.
DEFINE VARIABLE wpl     LIKE lo-matplano.nr-pl INITIAL 0.
DEFINE VARIABLE wit     LIKE lo-matplano.it-codigo INITIAL " ".
DEFINE VARIABLE witant  LIKE lo-matplano.it-codigo INITIAL " ".
DEFINE VARIABLE wcontit AS   INTEGER FORMAT "999999".
DEFINE VARIABLE wdatapl LIKE recebimento.data-nota INITIAL ?.
/* BUFFER PARA lo-mataplano */
DEFINE BUFFER b-plano FOR lo-matplano.

DEFINE WORKFILE fit-plano
  FIELD fit-codigo     LIKE lo-matplano.it-codigo      INITIAL 0
  FIELD fcod-emitente  LIKE lo-matplano.cod-emitente   INITIAL 0
  FIELD fnr-pl         LIKE lo-matplano.nr-pl          INITIAL " "
  FIELD femp           LIKE lo-matplano.emp            INITIAL 1
  FIELD festab         LIKE lo-matplano.estab          INITIAL "1".
/* Variaveis referente a criacao dos titulos -------------------------------*/
DEFINE VARIABLE wpl-ant         LIKE lo-matplano.nr-pl.                    
DEFINE VARIABLE wcont           AS INTE FORMAT 9.
DEFINE VARIABLE wcont-2         AS INTE FORMAT 9.
DEFINE VARIABLE dia             AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE dia-2           AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE data            AS DATE FORMAT "99/99/9999" INITIAL ?.
DEFINE VARIABLE wmes            AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wmes1-ab        AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wano1-ab        AS INTE FORMAT 9999 INITIAL 0.
DEFINE VARIABLE wmes2-ab        AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wano2-ab        AS INTE FORMAT 9999 INITIAL 0.
DEFINE VARIABLE wwmes1           AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wwmes2           AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wmestit         AS CHAR FORMAT "X(15)".
DEFINE VARIABLE meslist         AS CHAR FORMAT "X(15)" INITIAL
       " Jan, Fev, Mar, Abr, Mai, Jun, Jul, Ago, Set, Out, Nov, Dez".
DEFINE VARIABLE wmes-aux1       AS INTE FORMAT "99".
DEFINE VARIABLE wano-aux1       AS INTE FORMAT "9999".
DEFINE VARIABLE wmes-aux2       AS INTE FORMAT "99".
DEFINE VARIABLE wano-aux2       AS INTE FORMAT "9999".
DEFINE VARIABLE c-rodape        AS CHAR FORMAT "X(110)".
DEFINE VARIABLE wultdia1-ab     AS DATE.
DEFINE VARIABLE wcont1-ab       AS INTEGER FORMAT "99".
DEFINE VARIABLE wultdia2-ab     AS DATE.
DEFINE VARIABLE wcont2-ab       AS INTEGER FORMAT "99".
DEFINE VARIABLE wtitsem         AS CHAR FORMAT "X(09)" EXTENT 4 INITIAL " ".
DEFINE VARIABLE wtitsem1        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem2        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem3        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem4        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem5        AS CHAR FORMAT "X(09)" INITIAL " ".

DEFINE VARIABLE wtitsem1-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem2-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem3-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem4-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem5-2      AS CHAR FORMAT "X(09)" INITIAL " ".

DEFINE VARIABLE wtitmes1        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitmes2        AS CHAR FORMAT "X(09)" INITIAL " ".

DEFINE VARIABLE cmode           AS CHAR NO-UNDO.

DEFINE VARIABLE wnrult LIKE recebimento.numero-nota.
DEFINE VARIABLE wdtult LIKE recebimento.data-nota.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bt-sair-2 wep wcod-emitente bt-codigo westab ~
wnome-abrev wseq-it wnr-pl bt-consulta bt-first bt-prev bt-next bt-last ~
bt-plano bt-consultap bt-sair wit-codigo bt-it bt-inc bt-modif bt-confirma ~
bt-cancela bt-exclui-ind bt-exclui-todos wnro-docto wimediato wdatatela ~
wqt-sem1 wqt-sem1-2 wqt-sem2 wqt-sem2-2 wqt-sem3 wqt-sem3-2 wqt-sem4 ~
wqt-sem4-2 wqt-sem5 wqt-sem5-2 wmes1 wmes2 RECT-1 RECT-10 RECT-12 RECT-13 ~
RECT-14 RECT-15 RECT-16 RECT-2 RECT-8 RECT-9 
&Scoped-Define DISPLAYED-OBJECTS wep wcod-emitente westab wnome-abrev ~
wseq-it wnr-pl wit-codigo wnro-docto wimediato wdatatela wqt-sem1 ~
wqt-sem1-2 wqt-sem2 wqt-sem2-2 wqt-sem3 wqt-sem3-2 wqt-sem4 wqt-sem4-2 ~
wqt-sem5 wqt-sem5-2 wmes1 wmes2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancela AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-cancel.bmp":U
     LABEL "Cancela" 
     SIZE 4 BY 1.13 TOOLTIP "Cancela Opera��o"
     BGCOLOR 8 .

DEFINE BUTTON bt-codigo 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Plano/Item" 
     SIZE 5 BY .75 TOOLTIP "Inserir C�digo Empresa / Estab. / Fornecedor"
     FONT 1.

DEFINE BUTTON bt-confirma 
     IMAGE-UP FILE "image/im-ok.bmp":U
     LABEL "Confirma" 
     SIZE 4 BY 1.13 TOOLTIP "Confirma opera��o".

DEFINE BUTTON bt-consulta 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Procura Plano" 
     SIZE 5 BY .75 TOOLTIP "Inserir c�digo do plano"
     BGCOLOR 8 .

DEFINE BUTTON bt-consultap 
     LABEL "Consulta Plano" 
     SIZE 15 BY 1
     FONT 1.

DEFINE BUTTON bt-exclui-ind 
     IMAGE-UP FILE "image/gr-eli.bmp":U
     LABEL "Exclui Item" 
     SIZE 5 BY 1.13 TOOLTIP "Exclui item".

DEFINE BUTTON bt-exclui-todos 
     IMAGE-UP FILE "image/gr-eli.bmp":U
     LABEL "Exclui Todos" 
     SIZE 5 BY 1.13 TOOLTIP "Exclui o plano completo"
     BGCOLOR 8 .

DEFINE BUTTON bt-first 
     IMAGE-UP FILE "image/im-pre3.bmp":U
     LABEL "Primeiro" 
     SIZE 4 BY 1 TOOLTIP "Primeiro registro"
     BGCOLOR 8 .

DEFINE BUTTON bt-inc 
     IMAGE-UP FILE "image/im-new.bmp":U
     LABEL "Inclui Item" 
     SIZE 5 BY 1.13 TOOLTIP "Inclui Item".

DEFINE BUTTON bt-it 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Procura Item" 
     SIZE 5 BY 1.13 TOOLTIP "Procura Plano/Item".

DEFINE BUTTON bt-last 
     IMAGE-UP FILE "image/im-las.bmp":U
     LABEL "�ltimo" 
     SIZE 4 BY 1 TOOLTIP "�ltimo registro".

DEFINE BUTTON bt-modif 
     IMAGE-UP FILE "image/gr-mod.bmp":U
     LABEL "Modifica" 
     SIZE 5 BY 1.13 TOOLTIP "Modifica registro".

DEFINE BUTTON bt-next 
     IMAGE-UP FILE "image/im-nex.bmp":U
     LABEL "Pr�ximo" 
     SIZE 4 BY 1 TOOLTIP "Pr�ximo registro".

DEFINE BUTTON bt-plano 
     LABEL "Gera Plano" 
     SIZE 12 BY 1 TOOLTIP "Gera Plano"
     BGCOLOR 4 FGCOLOR 4 FONT 1.

DEFINE BUTTON bt-prev 
     IMAGE-UP FILE "image/im-pre.bmp":U
     LABEL "Anterior" 
     SIZE 4 BY 1 TOOLTIP "Registro anterior".

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE wcod-emitente AS INTEGER FORMAT ">>>>>9":U INITIAL 0 
     LABEL "Cod. do Fornecedor" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .75 NO-UNDO.

DEFINE VARIABLE wdatatela AS DATE FORMAT "99/99/9999":U 
     LABEL "Dt.Emiss�o" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .75 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .75 NO-UNDO.

DEFINE VARIABLE wimediato AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. em Atraso" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wit-codigo AS CHARACTER FORMAT "X(16)":U 
     LABEL "C�digo do Item" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wmes1 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wmes2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnome-abrev AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 23 BY .75 NO-UNDO.

DEFINE VARIABLE wnr-pl AS CHARACTER FORMAT "999999X":U 
     LABEL "N�mero do Plano" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .75 NO-UNDO.

DEFINE VARIABLE wnro-docto AS INTEGER FORMAT ">>>>>9":U INITIAL 0 
     LABEL "No. NF (corte)" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem1 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem1-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem2-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem3 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem3-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem4 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem4-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem5 AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-sem5-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd." 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wseq-it AS INTEGER FORMAT ">>>>9":U INITIAL 0 
     LABEL "Sequ�ncia" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .75 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 89 BY 5.25.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 89 BY 6.5.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 89 BY 1.5
     BGCOLOR 8 .

DEFINE RECTANGLE RECT-13
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 89 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-14
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 89 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-15
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 33 BY 1.5.

DEFINE RECTANGLE RECT-16
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 28 BY 1.5
     BGCOLOR 15 FGCOLOR 9 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 89 BY 1.5.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 89 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 89 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bt-sair-2 AT ROW 2 COL 85
     wep AT ROW 2.75 COL 13 COLON-ALIGNED
     wcod-emitente AT ROW 2.75 COL 37 COLON-ALIGNED
     bt-codigo AT ROW 2.75 COL 53
     westab AT ROW 3.5 COL 13 COLON-ALIGNED
     wnome-abrev AT ROW 3.5 COL 37 COLON-ALIGNED NO-LABEL
     wseq-it AT ROW 4.25 COL 13 COLON-ALIGNED
     wnr-pl AT ROW 4.25 COL 37 COLON-ALIGNED
     bt-consulta AT ROW 4.25 COL 54
     bt-first AT ROW 6 COL 20
     bt-prev AT ROW 6 COL 24
     bt-next AT ROW 6 COL 28 HELP
          "sssfsf"
     bt-last AT ROW 6 COL 32
     bt-plano AT ROW 6 COL 38
     bt-consultap AT ROW 6 COL 51
     bt-sair AT ROW 6 COL 67
     wit-codigo AT ROW 8.25 COL 34 COLON-ALIGNED
     bt-it AT ROW 8.25 COL 53
     bt-inc AT ROW 9.75 COL 18
     bt-modif AT ROW 9.75 COL 24
     bt-confirma AT ROW 9.75 COL 32
     bt-cancela AT ROW 9.75 COL 36
     bt-exclui-ind AT ROW 9.75 COL 49 HELP
          "Elimina Item"
     bt-exclui-todos AT ROW 9.75 COL 65
     wnro-docto AT ROW 12.25 COL 15 COLON-ALIGNED
     wimediato AT ROW 12.25 COL 45 COLON-ALIGNED
     wdatatela AT ROW 12.25 COL 71 COLON-ALIGNED
     wqt-sem1 AT ROW 13.75 COL 22 COLON-ALIGNED
     wqt-sem1-2 AT ROW 13.75 COL 57 COLON-ALIGNED
     wqt-sem2 AT ROW 14.75 COL 22 COLON-ALIGNED
     wqt-sem2-2 AT ROW 14.75 COL 57 COLON-ALIGNED
     wqt-sem3 AT ROW 15.75 COL 22 COLON-ALIGNED
     wqt-sem3-2 AT ROW 15.75 COL 57 COLON-ALIGNED
     wqt-sem4 AT ROW 16.75 COL 22 COLON-ALIGNED
     wqt-sem4-2 AT ROW 16.75 COL 57 COLON-ALIGNED
     wqt-sem5 AT ROW 17.75 COL 22 COLON-ALIGNED
     wqt-sem5-2 AT ROW 17.75 COL 57 COLON-ALIGNED
     wmes1 AT ROW 18.75 COL 22 COLON-ALIGNED
     wmes2 AT ROW 18.75 COL 57 COLON-ALIGNED
     RECT-1 AT ROW 2 COL 2
     RECT-10 AT ROW 13.5 COL 2
     RECT-12 AT ROW 5.75 COL 2
     RECT-13 AT ROW 11.25 COL 2
     RECT-14 AT ROW 7.25 COL 2
     RECT-15 AT ROW 9.5 COL 47
     RECT-16 AT ROW 9.5 COL 16
     RECT-2 AT ROW 12 COL 2
     RECT-8 AT ROW 1.25 COL 2
     RECT-9 AT ROW 20 COL 2
     "Exclui  Item" VIEW-AS TEXT
          SIZE 8 BY .67 AT ROW 10 COL 54
          FONT 1
     "Exclui  Plano" VIEW-AS TEXT
          SIZE 9 BY .67 AT ROW 10 COL 70
          FONT 1
     "Clique para alterar Empresa/" VIEW-AS TEXT
          SIZE 28 BY .67 AT ROW 2.75 COL 58
          FONT 2
     "Estab/Fornecedor" VIEW-AS TEXT
          SIZE 16 BY .54 AT ROW 3.5 COL 70
          FONT 2
     "Clique p/ inserir c�digo" VIEW-AS TEXT
          SIZE 25 BY .54 AT ROW 4.25 COL 60
          FONT 2
     "do plano" VIEW-AS TEXT
          SIZE 8 BY .54 AT ROW 4.75 COL 76
          FONT 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 91.43 BY 20.38
         FONT 1
         CANCEL-BUTTON bt-cancela.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Plano de Entrega de Materiais - wpp0101"
         HEIGHT             = 19.88
         WIDTH              = 90.72
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
ASSIGN 
       bt-next:PRIVATE-DATA IN FRAME DEFAULT-FRAME     = 
                "sfssf".

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Plano de Entrega de Materiais - wpp0101 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Plano de Entrega de Materiais - wpp0101 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancela
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancela C-Win
ON CHOOSE OF bt-cancela IN FRAME DEFAULT-FRAME /* Cancela */
DO:
  DISABLE ALL WITH FRAME {&FRAME-NAME}.
  ENABLE bt-plano bt-first
         bt-next bt-last bt-modif bt-prev bt-confirma bt-inc
         bt-consulta bt-cancela bt-it bt-codigo bt-sair bt-sair-2
         bt-exclui-todos bt-exclui-ind bt-consultap WITH FRAME    {&FRAME-NAME}.
  RUN disp_dados.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-codigo C-Win
ON CHOOSE OF bt-codigo IN FRAME DEFAULT-FRAME /* Plano/Item */
DO:
  ENABLE wep westab wcod-emitente wnr-pl wit-codigo WITH FRAME {&FRAME-NAME}.
  ASSIGN wep           = 1
         westab        = "1"
         wit-codigo    = ""
         wcod-emitente = 0
         wnr-pl        = STRING(MONTH(TODAY),"99") + STRING(YEAR(TODAY)).
  DISPLAY wep westab wit-codigo wcod-emitente wnr-pl WITH FRAME  {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-confirma
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-confirma C-Win
ON CHOOSE OF bt-confirma IN FRAME DEFAULT-FRAME /* Confirma */
DO:
  IF cmode = "Modifica" THEN DO:
    ASSIGN wcod-emitente wep westab wnr-pl wit-codigo wseq-it wnro-docto wdatatela.
    ASSIGN  wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
          wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
          wqt-sem5-2 wmes1 wmes2. 
    RUN modifica.
    DISABLE ALL WITH FRAME  {&FRAME-NAME}.
    ENABLE bt-plano bt-first
       bt-next bt-last bt-modif bt-prev bt-confirma bt-inc
       bt-consulta bt-cancela bt-it bt-codigo bt-sair 
       bt-exclui-todos bt-exclui-ind bt-consultap bt-sair-2 WITH FRAME    {&FRAME-NAME}.
    END.
  IF cmode = "Inclui" THEN DO:
    ASSIGN wcod-emitente wep westab wnr-pl wit-codigo wseq-it wnro-docto wdatatela.
    ASSIGN  wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
          wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
          wqt-sem5-2 wmes1 wmes2. 
    RUN inc.
    DISABLE wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
          wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
          wqt-sem5-2 wmes1 wmes2. 
    RUN inicia.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-consulta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-consulta C-Win
ON CHOOSE OF bt-consulta IN FRAME DEFAULT-FRAME /* Procura Plano */
DO:
   ASSIGN wcod-emitente wep westab wnr-pl.
   RUN DISP_emit.
   FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                            AND   lo-matplano.emp      = wep
                            AND   lo-matplano.estab    = westab
                            AND   lo-matplano.nr-pl    = wnr-pl 
                            NO-LOCK USE-INDEX ch-primaria NO-ERROR.
   IF NOT AVAILABLE lo-matplano THEN  MESSAGE "PLANO NAO ENCONTRADO" 
                                VIEW-AS ALERT-BOX.
   IF AVAILABLE lo-matplano THEN RUN disp_dados.
   FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl = SUBSTRING(lo-matplano.nr-pl,1,6) 
                  NO-LOCK NO-ERROR.
   IF AVAILABLE lo-matdatas THEN RUN DISP_tit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-consultap
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-consultap C-Win
ON CHOOSE OF bt-consultap IN FRAME DEFAULT-FRAME /* Consulta Plano */
DO:
  RUN ../especificos/wpsf/wpp0101a.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-exclui-ind
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-exclui-ind C-Win
ON CHOOSE OF bt-exclui-ind IN FRAME DEFAULT-FRAME /* Exclui Item */
DO:
  ASSIGN wcod-emitente wep westab wnr-pl wit-codigo.

  IF wep <> 0 AND westab <> "" AND wnr-pl <> ""  AND wit-codigo <> "" THEN DO:
    RUN exc-ind.
    DISABLE ALL WITH FRAME {&FRAME-NAME}.
    ENABLE bt-plano bt-first
       bt-next bt-last bt-modif bt-prev bt-confirma bt-inc
       bt-consulta bt-cancela bt-it bt-codigo bt-sair 
       bt-exclui-todos bt-exclui-ind bt-consultap bt-sair-2 WITH FRAME    {&FRAME-NAME}.
  END.
  ELSE DO:
    MESSAGE "Empresa / Estabelecimento / Emitente / Plano devem ser diferente de 0 ou branco"
            VIEW-AS ALERT-BOX.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-exclui-todos
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-exclui-todos C-Win
ON CHOOSE OF bt-exclui-todos IN FRAME DEFAULT-FRAME /* Exclui Todos */
DO:
  ASSIGN wep westab wcod-emitente wnr-pl.
  IF wep <> 0 AND westab <> ""  AND wnr-pl <> ""  THEN DO:
    RUN disp_emit.
    RUN exclui.
  END.
  ELSE DO:
    MESSAGE "Empresa / Estabelecimento / Emitente / Plano devem ser diferente de 0 ou branco"
            VIEW-AS ALERT-BOX.
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-first
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-first C-Win
ON CHOOSE OF bt-first IN FRAME DEFAULT-FRAME /* Primeiro */
DO:
   ASSIGN wcod-emitente wep westab wnr-pl.
   RUN DISP_emit.
   FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                            AND   lo-matplano.emp      = wep
                            AND   lo-matplano.estab    = westab
                            AND   lo-matplano.nr-pl    = wnr-pl 
                            NO-LOCK USE-INDEX ch-primaria NO-ERROR.
   IF NOT AVAILABLE lo-matplano THEN  MESSAGE "INICIO DE ARQUIVO" 
                                VIEW-AS ALERT-BOX.
   RUN disp_dados.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-inc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-inc C-Win
ON CHOOSE OF bt-inc IN FRAME DEFAULT-FRAME /* Inclui Item */
DO:
  ASSIGN cmode = SELF:PRIVATE-DATA.
  ASSIGN wcod-emitente wnr-pl wep westab.
  ASSIGN     wit-codigo  = " "          
               wnro-docto       = 0 
               wseq-it          = 0
               wimediato        = 0
               wqt-sem1         = 0
               wqt-sem2         = 0
               wqt-sem3         = 0
               wqt-sem4         = 0
               wqt-sem5         = 0
               wqt-sem1-2       = 0
               wqt-sem2-2       = 0
               wqt-sem3-2       = 0
               wqt-sem4-2       = 0
               wqt-sem5-2       = 0
               wmes1            = 0
               wmes2            = 0.
    DISPLAY wit-codigo  wnro-docto wseq-it wimediato wqt-sem1 wqt-sem2 
               wqt-sem3 wqt-sem4 wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 
               wqt-sem4-2 wqt-sem5-2 wmes1 wmes2 WITH FRAME   {&FRAME-NAME}.
    IF wep <> 0 AND westab <> "" AND wnr-pl <> ""  THEN DO:
    ENABLE ALL WITH FRAME  {&FRAME-NAME}.
    ASSIGN wcod-emitente wnr-pl wep westab wit-codigo
         wdatatela wseq-it wnro-docto wimediato wqt-sem1 wqt-sem2
         wqt-sem3 wqt-sem4 wqt-sem5 wqt-sem1-2 wqt-sem2-2
         wqt-sem3-2 wqt-sem4-2 wqt-sem5-2 wqt-sem5-2 wmes1 wmes2.
    RUN  proc_emit.
    run cria_tit.
    run disp_tit.
    END.
  ELSE DO:
    MESSAGE "Empresa / Estabelecimento / Emitente / Plano devem ser diferente de 0 ou branco"
            VIEW-AS ALERT-BOX.
    END.
  
  DISABLE bt-first bt-prev bt-next bt-last bt-modif bt-exclui-ind 
          bt-exclui-todos bt-plano bt-consultap bt-it WITH FRAME   {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-it
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-it C-Win
ON CHOOSE OF bt-it IN FRAME DEFAULT-FRAME /* Procura Item */
DO:
   ENABLE wep westab wcod-emitente wnr-pl wit-codigo WITH FRAME {&FRAME-NAME}.
   ASSIGN wcod-emitente wep westab wnr-pl wit-codigo.
   RUN DISP_emit.
   FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente    = wcod-emitente
                            AND   lo-matplano.emp           = wep
                            AND   lo-matplano.estab         = westab
                            AND   lo-matplano.nr-pl         = wnr-pl 
                            AND   lo-matplano.it-codigo     = wit-codigo
                            NO-LOCK USE-INDEX ch-primaria NO-ERROR.
   IF NOT AVAILABLE lo-matplano THEN  MESSAGE "PLANO/ITEM NAO ENCONTRADO" 
                                VIEW-AS ALERT-BOX.
   RUN disp_dados.
   FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl = lo-matplano.nr-pl NO-LOCK NO-ERROR.
   IF AVAILABLE lo-matdatas THEN RUN DISP_tit.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-last
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-last C-Win
ON CHOOSE OF bt-last IN FRAME DEFAULT-FRAME /* �ltimo */
DO: 
  ASSIGN wcod-emitente wep westab wnr-pl.
  RUN disp_emit.
  FIND LAST lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                        AND   lo-matplano.emp      = wep
                        AND   lo-matplano.estab    = westab
                        AND   lo-matplano.nr-pl    = wnr-pl NO-LOCK 
                       USE-INDEX ch-primaria NO-ERROR.
  IF NOT AVAILABLE lo-matplano THEN  MESSAGE "FIM DE ARQUIVO" 
                                VIEW-AS ALERT-BOX.
  RUN disp_dados.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-modif
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-modif C-Win
ON CHOOSE OF bt-modif IN FRAME DEFAULT-FRAME /* Modifica */
DO:
  ASSIGN cmode = SELF:PRIVATE-DATA.
  IF wep <> 0 AND westab <> ""  AND wnr-pl <> ""  THEN DO:
    ENABLE ALL  WITH FRAME  {&FRAME-NAME}.
    ASSIGN wcod-emitente wep westab wnr-pl wit-codigo.
    
    ASSIGN   wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
             wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
             wqt-sem5-2 wmes1 wmes2. 
    END.
  ELSE DO:
    MESSAGE "Empresa / Estabelecimento / Emitente / Plano devem ser diferente de 0 ou branco"
            VIEW-AS ALERT-BOX.
  END.

  DISABLE bt-inc bt-first bt-prev bt-next bt-last bt-exclui-ind 
          bt-exclui-todos bt-plano bt-consultap bt-it WITH FRAME   {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-next
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-next C-Win
ON CHOOSE OF bt-next IN FRAME DEFAULT-FRAME /* Pr�ximo */
DO:
   ASSIGN wcod-emitente wep westab wnr-pl.
   RUN disp_emit.
   FIND NEXT lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                            AND   lo-matplano.emp      = wep
                            AND   lo-matplano.estab    = westab
                            AND   lo-matplano.nr-pl    = wnr-pl NO-LOCK 
                            USE-INDEX ch-primaria NO-ERROR.
   
   IF NOT AVAILABLE lo-matplano THEN  MESSAGE "FIM DE ARQUIVO" 
                                VIEW-AS ALERT-BOX.
   RUN disp_dados.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-plano
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-plano C-Win
ON CHOOSE OF bt-plano IN FRAME DEFAULT-FRAME /* Gera Plano */
DO:
    ASSIGN wcod-emitente wep westab wnr-pl.
    /*IF wep <> 0 AND westab <> "" AND wnr-pl <> ""  THEN DO:*/
    FIND FIRST item-fornec WHERE item-fornec.cod-emitente = wcod-emitente 
                            AND   item-fornec.ativo        = yes
                            NO-LOCK NO-ERROR.
     IF NOT AVAILABLE(item-fornec) THEN DO:
       MESSAGE "Relacionamento Item/Fornecedor nao encontrado".
       PAUSE. 
       UNDO,RETRY.
       END.
    ASSIGN wnr-pl = CAPS(wnr-pl).
    RUN cria_tit.
    RUN disp_emit. 
    RUN disp_tit.
    RUN procura.
    /*END.
  ELSE MESSAGE "N�o pode criar plano para empresa 0 / estabelecimento branco"
               SKIP "e emitente diferente de branco" view-as alert-box */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-prev
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-prev C-Win
ON CHOOSE OF bt-prev IN FRAME DEFAULT-FRAME /* Anterior */
DO:
   ASSIGN wcod-emitente wep westab wnr-pl.
   RUN disp_emit.
   FIND PREV lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                            AND   lo-matplano.emp      = wep
                            AND   lo-matplano.estab    = westab
                            AND   lo-matplano.nr-pl    = wnr-pl NO-LOCK 
                            USE-INDEX ch-primaria NO-ERROR.
   
   IF NOT AVAILABLE lo-matplano THEN  MESSAGE "N�O EXISTE PLANO ANTERIOR" 
                                VIEW-AS ALERT-BOX.
   RUN disp_dados.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cria_tit C-Win 
PROCEDURE cria_tit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN wcont           = 0
         wcont-2         = 0
         wcont1-ab       = 0
         wcont2-ab       = 0
         wtitsem1        = " "  
         wtitsem2        = " "
         wtitsem3        = " "
         wtitsem4        = " " 
         wtitsem5        = " " 
         wtitsem1-2      = " " 
         wtitsem2-2      = " " 
         wtitsem3-2      = " " 
         wtitsem4-2      = " " 
         wtitsem5-2      = " " 
         wtitmes1        = " " 
         wtitmes2        = " "
         data            = ?. 
   FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl = SUBSTRING(wnr-pl,1,6)
                          AND   lo-matdatas.emp   = wep
                          AND   lo-matdatas.estab = westab
                           NO-LOCK NO-ERROR.
  IF NOT AVAILABLE lo-matdatas THEN DO:
    CREATE lo-matdatas.
    ASSIGN  lo-matdatas.emp   = wep
            lo-matdatas.estab = westab
            lo-matdatas.nr-pl = wnr-pl.

    /* Assinala mes e ano em aberto */
    ASSIGN wmes     = INTEGER(SUBSTRING(lo-matdatas.nr-pl,1,2)).
           wano1-ab = INTEGER(SUBSTRING(lo-matdatas.nr-pl,3,4)).
    
    IF wmes = 10 THEN     
      ASSIGN wmes1-ab = 10
             wmes2-ab = 11
             wwmes1    = 12
             wwmes2    = 1
             wano1-ab = wano1-ab
             wano2-ab = wano1-ab.

    IF wmes = 11 THEN     
      ASSIGN wmes1-ab = 11
             wmes2-ab = 12
             wwmes1    = 1
             wwmes2    = 2
             wano1-ab = wano1-ab
             wano2-ab = wano1-ab.
  
    IF wmes = 12 THEN
      ASSIGN wmes1-ab = 12
             wmes2-ab = 1
             wwmes1    = 2
             wwmes2    = 3
             wano1-ab = wano1-ab
             wano2-ab = wano1-ab + 1.
       
    IF wmes  <> 10  
    AND wmes <> 11
    AND wmes <> 12 THEN ASSIGN 
        wmes1-ab = wmes
        wmes2-ab = wmes + 1
        wwmes1    = wmes2-ab + 1
        wwmes2    = wwmes1 + 1
        wano1-ab = wano1-ab
        wano2-ab = wano1-ab.

    ASSIGN lo-matdatas.titmes1 =  "     " + ENTRY(wwmes1,meslist)
           lo-matdatas.titmes2 =  "     " + ENTRY(wwmes2,meslist).
    /***********  TITULOS DAS SEMANAS PARA O 1O.MES EM ABERTO **********/
    /* Calcula ultimo dia do mes */

    ASSIGN wmes-aux1 = wmes1-ab + 1.
    IF wmes1-ab = 12 THEN ASSIGN wmes-aux1 = 1
                                 wano-aux1 = wano1-ab + 1.  
    ASSIGN wultdia1-ab  = DATE(wmes-aux1,01,wano1-ab) - 1
           wcont1-ab    = 
           INTEGER(SUBSTRING(STRING(wultdia1-ab,"99/99/9999"),1,2)).
    /* Assinala Titulo das Semanas ------------------------------------*/
    DO dia  = 1 TO wcont1-ab:
       data = DATE(wmes1-ab,dia,wano1-ab).
    
      IF WEEKDAY(data) = 2 THEN DO:
        wcont = wcont + 1.
        END.
      IF WEEKDAY(data) = 2 AND wcont = 1 THEN 
      ASSIGN wtitsem1  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem1 = wtitsem1.
   
      IF WEEKDAY(data) = 2 AND wcont = 2 THEN
      ASSIGN wtitsem2  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem2 = wtitsem2.
     
      IF WEEKDAY(data) = 2 AND wcont = 3 THEN 
      ASSIGN wtitsem3  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem3 = wtitsem3.
   
      IF WEEKDAY(data) = 2 AND wcont = 4 THEN 
      ASSIGN wtitsem4  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem4 = wtitsem4.
       
      IF WEEKDAY(data) = 2 AND wcont = 5 THEN 
      ASSIGN wtitsem5  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem5 = wtitsem5.
      END.
                            
      /***********  TITULOS DAS SEMANAS PARA O 2o.MES EM ABERTO **********/
      /* Calcula ultimo dia do mes */        
      IF wmes2-ab <> 12 THEN
      ASSIGN wmes-aux2  = wmes2-ab + 1
             wano-aux2  = wano2-ab.
      IF wmes2-ab = 12 THEN ASSIGN wmes-aux2 = 1
                                   wano-aux2 = wano2-ab + 1.
      ASSIGN wultdia2-ab  = DATE(wmes-aux2,01,wano-aux2) - 1.       
             wcont2-ab     = 
             INTEGER(SUBSTRING(STRING(wultdia2-ab,"99/99/9999"),1,2)).
       
      /* Assinala Titulo das Semanas ------------------------------------*/
      DO dia-2 = 1 TO wcont2-ab:
         data  = DATE(wmes2-ab,dia-2,wano2-ab).
        IF WEEKDAY(data) = 2 THEN DO:
          wcont-2 = wcont-2 + 1.
        END.

      IF WEEKDAY(data)  = 2 AND wcont-2 = 1 THEN 
      ASSIGN wtitsem1-2 = " " + STRING(DAY(data),"99") + " / " 
                        + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem1-2 = wtitsem1-2.
        
      IF WEEKDAY(data)  = 2 AND wcont-2 = 2 THEN 
      ASSIGN wtitsem2-2 = " " + STRING(DAY(data),"99") + " / " 
                        + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem2-2 = wtitsem2-2.
    
      IF WEEKDAY(data)  = 2 AND wcont-2 = 3 THEN 
      ASSIGN wtitsem3-2 = " " + STRING(DAY(data),"99") + " / " 
                        + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem3-2 = wtitsem3-2.
    
      IF WEEKDAY(data) = 2 AND wcont-2 = 4 THEN 
      ASSIGN wtitsem4-2  = " " + STRING(DAY(data),"99") + " / " 
                         + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem4-2 = wtitsem4-2.    
      
      IF WEEKDAY(data) = 2 AND wcont-2 = 5 THEN 
      ASSIGN wtitsem5-2  = " " + STRING(DAY(data),"99") + " / " 
                               + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem5-2 = wtitsem5-2.
      END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_dados C-Win 
PROCEDURE disp_dados :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 IF AVAILABLE lo-matplano THEN
 DISPLAY 
          lo-matplano.cod-emitente  @  wcod-emitente
          lo-matplano.estab         @  westab
          lo-matplano.emp           @  wep
          lo-matplano.nr-pl         @  wnr-pl 
          lo-matplano.seq-it        @  wseq-it
          lo-matplano.it-codigo     @  wit-codigo          
          lo-matplano.nro-docto     @  wnro-docto     
          lo-matplano.data-nota     @  wdatatela
          lo-matplano.imediato      @  wimediato          
          lo-matplano.qt-sem1       @  wqt-sem1
          lo-matplano.qt-sem2       @  wqt-sem2 
          lo-matplano.qt-sem3       @  wqt-sem3
          lo-matplano.qt-sem4       @  wqt-sem4
          lo-matplano.qt-sem5       @  wqt-sem5   
          lo-matplano.qt-sem1-2     @  wqt-sem1-2
          lo-matplano.qt-sem2-2     @  wqt-sem2-2
          lo-matplano.qt-sem3-2     @  wqt-sem3-2
          lo-matplano.qt-sem4-2     @  wqt-sem4-2
          lo-matplano.qt-sem5-2     @  wqt-sem5-2 
          lo-matplano.mes1          @  wmes1
          lo-matplano.mes2          @  wmes2
          WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_data C-Win 
PROCEDURE disp_data :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN wdatatela = ?.
  FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =   wcod-emitente
                           AND recebimento.num-nota
                           =   wnro-docto
                           NO-LOCK NO-ERROR.
  IF AVAILABLE recebimento THEN ASSIGN wdatatela = recebimento.data-nota.
  IF NOT AVAILABLE recebimento THEN ASSIGN wdatatela = ?.
  DISPLAY wdatatela WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_emit C-Win 
PROCEDURE disp_emit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  FIND FIRST emitente WHERE cod-emitente = wcod-emitente NO-LOCK NO-ERROR.
  IF AVAILABLE emitente THEN DISPLAY emitente.nome-abrev @ wnome-abrev WITH FRAME 
  {&FRAME-NAME}.
  IF NOT AVAILABLE emitente THEN DO:
    MESSAGE "Fornecedor nao encontrado".
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_nf C-Win 
PROCEDURE disp_nf :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  CLEAR FRAME f-nf ALL NO-PAUSE.
  DEFINE VARIABLE wnrult LIKE recebimento.numero-nota.
  DEFINE VARIABLE wdtult LIKE recebimento.data-nota.
  MESSAGE "AGUARDE ...".
  ASSIGN wdatapesq = ?.
    FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     lo-matplano.cod-emitente
                           AND   recebimento.num-nota
                           =     lo-matplano.nro-docto
                           AND   recebimento.it-codigo
                           =     lo-matplano.it-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               lo-matplano.cod-emitente
                         AND   recebimento.it-codigo    =
                               lo-matplano.it-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   recebimento.num-nota  GT
                               lo-matplano.nro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
  
       DISPLAY recebimento.it-codigo recebimento.numero-nota           
             recebimento.data-nota recebimento.quant-receb
             WITH FRAME f-nf
             DOWN OVERLAY CENTERED TITLE "Fornecedor: " 
             + STRING(recebimento.cod-emitente)
             + " NF's apos a nota de corte " 
             + STRING(lo-matplano.nro-docto).
             DOWN WITH FRAME f-nf OVERLAY.
     END.
   PAUSE.
   HIDE FRAME f-nf.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_tela C-Win 
PROCEDURE disp_tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* PROCEDURES --------------------------------------------------------------*/
  ASSIGN wdatatela = ?.
  FIND FIRST recebimento  WHERE recebimento.cod-emitente
                           =     lo-matplano.cod-emitente
                           AND   recebimento.num-nota
                           =     lo-matplano.nro-docto
                           AND   recebimento.it-codigo
                           =     lo-matplano.it-codigo
                           NO-LOCK NO-ERROR.

  IF AVAILABLE recebimento THEN ASSIGN wdatatela = recebimento.data-nota.
  IF NOT AVAILABLE recebimento THEN ASSIGN wdatatela = ?.
  run proc_tit.
  run disp_tit.
  DISPLAY 
          lo-matplano.seq-it        @  wseq-it
          lo-matplano.it-codigo     @  wit-codigo          
          lo-matplano.nro-docto     @  wnro-docto     
          lo-matplano.data-nota     @  wdatatela
          lo-matplano.imediato      @  wimediato          
          lo-matplano.qt-sem1       @  wqt-sem1
          lo-matplano.qt-sem2       @  wqt-sem2 
          lo-matplano.qt-sem3       @  wqt-sem3
          lo-matplano.qt-sem4       @  wqt-sem4
          lo-matplano.qt-sem5       @  wqt-sem5   
          lo-matplano.qt-sem1-2     @  wqt-sem1-2
          lo-matplano.qt-sem2-2     @  wqt-sem2-2
          lo-matplano.qt-sem3-2     @  wqt-sem3-2
          lo-matplano.qt-sem4-2     @  wqt-sem4-2
          lo-matplano.qt-sem5-2     @  wqt-sem5-2 
          lo-matplano.mes1          @  wmes1
          lo-matplano.mes2          @  wmes2
          WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_tit C-Win 
PROCEDURE disp_tit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN wqt-sem1:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem1
       wqt-sem1-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem1-2
       wqt-sem2:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem2
       wqt-sem2-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem2-2
       wqt-sem3:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem3
       wqt-sem3-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem3-2
       wqt-sem4:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem4
       wqt-sem4-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem4-2
       wqt-sem5:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem5
       wqt-sem5-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem5-2
       wmes1:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titmes1
       wmes2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titmes2.

/*DISPLAY   lo-matdatas.titsem1   lo-matdatas.titsem2 
          lo-matdatas.titsem3   lo-matdatas.titsem4 
          lo-matdatas.titsem5 
          lo-matdatas.titsem1-2 lo-matdatas.titsem2-2
          lo-matdatas.titsem3-2 lo-matdatas.titsem4-2
          lo-matdatas.titsem5-2 
          lo-matdatas.titmes1 lo-matdatas.titmes2
          WITH FRAME {&FRAME-NAME}. 
  */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wep wcod-emitente westab wnome-abrev wseq-it wnr-pl wit-codigo 
          wnro-docto wimediato wdatatela wqt-sem1 wqt-sem1-2 wqt-sem2 wqt-sem2-2 
          wqt-sem3 wqt-sem3-2 wqt-sem4 wqt-sem4-2 wqt-sem5 wqt-sem5-2 wmes1 
          wmes2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bt-sair-2 wep wcod-emitente bt-codigo westab wnome-abrev wseq-it 
         wnr-pl bt-consulta bt-first bt-prev bt-next bt-last bt-plano 
         bt-consultap bt-sair wit-codigo bt-it bt-inc bt-modif bt-confirma 
         bt-cancela bt-exclui-ind bt-exclui-todos wnro-docto wimediato 
         wdatatela wqt-sem1 wqt-sem1-2 wqt-sem2 wqt-sem2-2 wqt-sem3 wqt-sem3-2 
         wqt-sem4 wqt-sem4-2 wqt-sem5 wqt-sem5-2 wmes1 wmes2 RECT-1 RECT-10 
         RECT-12 RECT-13 RECT-14 RECT-15 RECT-16 RECT-2 RECT-8 RECT-9 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exc-ind C-Win 
PROCEDURE exc-ind :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 FIND FIRST lo-matplano exclusive-lock 
                           WHERE lo-matplano.cod-emitente  = wcod-emitente 
                           AND   lo-matplano.emp           = wep
                           AND   lo-matplano.estab         = westab
                           AND   lo-matplano.nr-pl         = wnr-pl 
                           AND   lo-matplano.it-codigo     = wit-codigo
                           NO-ERROR.            
 IF NOT AVAILABLE lo-matplano THEN DO:
    BELL.
    MESSAGE "Plano nao cadastrado" VIEW-AS ALERT-BOX.
    NEXT.
    END.
 run disp_tela.
 MESSAGE "Confirma a exclusao?" VIEW-AS ALERT-BOX BUTTON YES-NO UPDATE l-resposta
          AS LOGICAL FORMAT "Sim/Nao".
 IF l-resposta THEN DO:
   DELETE lo-matplano.
   CLEAR FRAME  {&FRAME-NAME}.
   FIND NEXT lo-matplano WHERE lo-matplano.cod-emitente  
                       =     wcod-emitente
                       AND   lo-matplano.emp    = wep
                       AND   lo-matplano.estab  = westab
                       AND   lo-matplano.nr-pl  = wnr-pl
                       USE-INDEX item1
                       NO-ERROR.
   IF AVAILABLE lo-matplano THEN RUN disp_dados.
   IF NOT AVAILABLE lo-matplano THEN DO: 
     FIND PREV lo-matplano WHERE lo-matplano.cod-emitente  
                       =     wcod-emitente
                       AND   lo-matplano.emp    = wep
                       AND   lo-matplano.estab  = westab
                       AND   lo-matplano.nr-pl  = wnr-pl
                       USE-INDEX item1
                       NO-ERROR.
     IF AVAILABLE lo-matplano THEN RUN disp_dados.
     END.
   END.
 
   END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exclui C-Win 
PROCEDURE exclui :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
        MESSAGE "Deseja exclui o plano Todo ?" VIEW-AS ALERT-BOX BUTTON YES-NO
        UPDATE wresp AS LOGICAL FORMAT "Sim/Nao".   
        /* exclusao completa */ 
        IF wresp = yes THEN DO:
           DISABLE ALL WITH FRAME   {&FRAME-NAME}.
           FOR EACH lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                                AND   lo-matplano.emp          = wep
                                AND   lo-matplano.estab        = westab
                                AND   lo-matplano.nr-pl        = wnr-pl
                                EXCLUSIVE-LOCK:
             PUT SCREEN ROW 22 "AGUARDE ... Excluindo Fornecedor: "
                               + STRING(wcod-emitente)
                               + " Plano: " 
                               + STRING(wnr-pl).
             DELETE lo-matplano.
             END.
           MESSAGE "EXCLUS�O CONCLUIDA COM SUCESSO !" VIEW-AS ALERT-BOX.
           CLEAR FRAME {&FRAME-NAME} ALL NO-PAUSE.
           DISABLE ALL WITH FRAME {&FRAME-NAME}.
           RUN inicia.
         END.
       END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inc C-Win 
PROCEDURE inc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* inclui */
   FIND FIRST item-fornec WHERE item-fornec.cod-emitente = wcod-emitente 
                           AND   item-fornec.it-codigo    = wit-codigo
                           AND   item-fornec.ativo        = yes
                           NO-LOCK NO-ERROR.
    IF NOT AVAILABLE(item-fornec) THEN DO:
      MESSAGE "Relacionamento Item/Fornecedor nao encontrado" VIEW-AS ALERT-BOX.
      NEXT.
      END.
    FIND  lo-matplano WHERE lo-matplano.cod-emitente  = wcod-emitente
                      AND   lo-matplano.emp           = wep
                      AND   lo-matplano.estab         = westab
                      AND   lo-matplano.nr-pl         = wnr-pl
                      AND   lo-matplano.it-codigo     = wit-codigo NO-ERROR.
    IF AVAILABLE lo-matplano THEN DO:
      MESSAGE "Item:" wit-codigo "ja consta no plano." VIEW-AS ALERT-BOX.
      DISABLE ALL WITH FRAME   {&FRAME-NAME}.
      RUN inicia.
      NEXT.
      END.
    FIND LAST lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                          AND   lo-matplano.emp          = wep
                          AND   lo-matplano.estab        = westab
                          AND   lo-matplano.nr-pl        = wnr-pl
                          NO-LOCK NO-ERROR.
    IF AVAILABLE lo-matplano THEN DO:
      ASSIGN wseq = lo-matplano.seq + 1.
      END.
    IF NOT AVAILABLE lo-matplano THEN DO:
      ASSIGN wseq = 1.
      END.
    ASSIGN wseq-it = wseq.
    DISPLAY wseq-it WITH FRAME {&FRAME-NAME}.  
    run disp_data.
    ASSIGN wseq-it wnro-docto wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
           wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
           wqt-sem5-2 wmes1 wmes2.
    MESSAGE "Confirma a inclus�o" VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wconfinc
             AS LOGICAL FORMAT "Sim/Nao".
    IF wconfinc = YES THEN DO:
      CREATE lo-matplano.
      ASSIGN  lo-matplano.cod-emitente  = wcod-emitente
              lo-matplano.nr-pl         = wnr-pl
              lo-matplano.emp           = wep
              lo-matplano.estab         = westab
              lo-matplano.it-codigo     = wit-codigo 
              lo-matplano.seq           = wseq
              lo-matplano.nro-docto     = wnro-docto
              lo-matplano.data-nota     = wdatatela
              lo-matplano.imediato      = wimediato
              lo-matplano.qt-sem1       = wqt-sem1
              lo-matplano.qt-sem2       = wqt-sem2
              lo-matplano.qt-sem3       = wqt-sem3
              lo-matplano.qt-sem4       = wqt-sem4
              lo-matplano.qt-sem5       = wqt-sem5   
              lo-matplano.qt-sem1-2     = wqt-sem1-2
              lo-matplano.qt-sem2-2     = wqt-sem2-2
              lo-matplano.qt-sem3-2     = wqt-sem3-2
              lo-matplano.qt-sem4-2     = wqt-sem4-2
              lo-matplano.qt-sem5-2     = wqt-sem5-2 
              lo-matplano.mes1          = wmes1
              lo-matplano.mes2          = wmes2.
    END.
    RUN disp_dados.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 ASSIGN        wit-codigo       = " "          
               wep              = 0
               wseq-it          = 0
               westab           = ""
               wcod-emitente    = 0              
               wnro-docto       = 0 
               wseq-it          = 0
               wimediato        = 0
               wqt-sem1         = 0
               wqt-sem2         = 0
               wqt-sem3         = 0
               wqt-sem4         = 0
               wqt-sem5         = 0
               wqt-sem1-2       = 0
               wqt-sem2-2       = 0
               wqt-sem3-2       = 0
               wqt-sem4-2       = 0
               wqt-sem5-2       = 0
               wmes1            = 0
               wmes2            = 0
               wnr-pl           = "".
DISPLAY wep westab wcod-emitente wnr-pl wit-codigo wseq-it wimediato
        wnro-docto wdatatela wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4 wqt-sem5 wqt-sem1-2
        wqt-sem2-2 wqt-sem3-2 wqt-sem4-2 wqt-sem5-2 wmes1 wmes2 wnr-pl
        WITH FRAME  {&FRAME-NAME}.
DISABLE ALL WITH FRAME {&FRAME-NAME}.
ENABLE bt-plano bt-first
       bt-next bt-last bt-modif bt-prev bt-confirma bt-inc
       bt-consulta bt-cancela bt-it bt-codigo bt-sair bt-sair-2
       bt-exclui-todos bt-exclui-ind bt-consultap WITH FRAME    {&FRAME-NAME}.
ASSIGN bt-inc:PRIVATE-DATA IN FRAME {&FRAME-NAME}     = "Inclui":U.
ASSIGN bt-modif:PRIVATE-DATA IN FRAME   {&FRAME-NAME} = "Modifica":U.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE item C-Win 
PROCEDURE item :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
RUN disp_emit.
FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                          AND   lo-matplano.emp          = wep
                          AND   lo-matplano.estab        = westab
                          AND   lo-matplano.nr-pl        = wnr-pl 
                          AND   lo-matplano.it-codigo    = wit-codigo
                          NO-LOCK USE-INDEX ch-primaria NO-ERROR.
RUN disp_dados.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modif C-Win 
PROCEDURE modif :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*
/* modifica */
  REPEAT ON ERROR UNDO,RETRY:
    FIND  lo-matplano WHERE lo-matplano.cod-emitente  = wcod-emitente
                      AND   lo-matplano.emp           = wep
                      AND   lo-matplano.estab         = westab
                      AND   lo-matplano.nr-pl         = wnr-pl
                      AND   lo-matplano.it-codigo     = wit-codigo NO-ERROR.
    IF NOT AVAILABLE lo-matplano THEN DO:
      MESSAGE "Item:" wit-codigo "nao consta no plano." VIEW-AS ALERT-BOX.
      UNDO,RETRY.
      END.                             
    IF AVAILABLE lo-matplano THEN DO:
      run disp_tela.
    
    /*
    SET wnro-docto HELP "Tecle F5 para ver ultimas NF's apos a nf de corte"
      WITH FRAME f-dados editing:
      READKEY.
      IF KEYFUNCTION(LASTKEY) = "get" THEN DO:
        run disp_nf.
        HIDE FRAME f-nf.
        END.
      ELSE APPLY LASTKEY.
      END.
    */  
      /* new */
      run disp_data.
      ASSIGN  wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
             wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
             wqt-sem5-2 wmes1 wmes2. 
      ASSIGN      lo-matplano.nro-docto     = wnro-docto
                  lo-matplano.imediato      = wimediato
                  lo-matplano.qt-sem1       = wqt-sem1
                  lo-matplano.qt-sem2       = wqt-sem2
                  lo-matplano.qt-sem3       = wqt-sem3
                  lo-matplano.qt-sem4       = wqt-sem4
                  lo-matplano.qt-sem5       = wqt-sem5   
                  lo-matplano.qt-sem1-2     = wqt-sem1-2
                  lo-matplano.qt-sem2-2     = wqt-sem2-2
                  lo-matplano.qt-sem3-2     = wqt-sem3-2
                  lo-matplano.qt-sem4-2     = wqt-sem4-2
                  lo-matplano.qt-sem5-2     = wqt-sem5-2 
                  lo-matplano.mes1          = wmes1
                  lo-matplano.mes2          = wmes2.      
      END.
    END.
  */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifica C-Win 
PROCEDURE modifica :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  MESSAGE "Confirma altera��o" VIEW-AS ALERT-BOX BUTTONS YES-NO 
           UPDATE wconf AS LOGICAL FORMAT "Sim/N�o".
  IF wconf  = YES  THEN DO:
    
    FIND  lo-matplano WHERE lo-matplano.cod-emitente  = wcod-emitente
                      AND   lo-matplano.emp           = wep
                      AND   lo-matplano.estab         = westab
                      AND   lo-matplano.nr-pl         = wnr-pl
                      AND   lo-matplano.it-codigo     = wit-codigo NO-ERROR.

    ASSIGN  lo-matplano.nro-docto     =  wnro-docto     
            lo-matplano.data-nota     = wdatatela    
            lo-matplano.imediato      = wimediato
            lo-matplano.qt-sem1       = wqt-sem1
            lo-matplano.qt-sem2       = wqt-sem2
            lo-matplano.qt-sem3       = wqt-sem3
            lo-matplano.qt-sem4       = wqt-sem4
            lo-matplano.qt-sem5       = wqt-sem5   
            lo-matplano.qt-sem1-2     = wqt-sem1-2
            lo-matplano.qt-sem2-2     = wqt-sem2-2
            lo-matplano.qt-sem3-2     = wqt-sem3-2
            lo-matplano.qt-sem4-2     = wqt-sem4-2
            lo-matplano.qt-sem5-2     = wqt-sem5-2 
            lo-matplano.mes1          = wmes1
            lo-matplano.mes2          = wmes2.      
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE modifica1 C-Win 
PROCEDURE modifica1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ENABLE ALL  WITH FRAME  {&FRAME-NAME}.
  ASSIGN wcod-emitente wep westab wnr-pl wit-codigo.
  ASSIGN     wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
             wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
             wqt-sem5-2 wmes1 wmes2. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE procura C-Win 
PROCEDURE procura :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                          AND   lo-matplano.emp          = wep
                          AND   lo-matplano.estab        = westab
                          AND   lo-matplano.nr-pl        = wnr-pl 
                          NO-ERROR.
   IF AVAILABLE lo-matplano THEN DO:
     MESSAGE "Plano ja cadastrado" VIEW-AS ALERT-BOX.
     DISABLE ALL WITH FRAME {&FRAME-NAME}.
     RUN inicia.
     END.
   IF NOT AVAILABLE lo-matplano THEN DO:  
      ASSIGN wseq = 0.
      FOR EACH item-fornec WHERE item-fornec.cod-emitente = wcod-emitente 
                           AND   item-fornec.ativo        = yes
                           NO-LOCK BREAK BY item-fornec.it-codigo:
        FIND FIRST item WHERE item.it-codigo = item-fornec.it-codigo
                        NO-LOCK NO-ERROR.
        IF item.cod-obsoleto <> 1 THEN NEXT.

        IF LENGTH(wnr-pl) > 6 THEN DO:
          PUT SCREEN ROW 22 FILL(" ",80).
          MESSAGE "Deseja Criar plano para o item? " + string(item-fornec.it-codigo)
          VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wcriaplano.
          IF wcriaplano = no THEN NEXT.
          IF wcriaplano = YES THEN DO:
            CREATE lo-matplano.
            ASSIGN wseq                      = wseq + 1
               lo-matplano.cod-emitente  = item-fornec.cod-emitente
               lo-matplano.it-codigo     = item-fornec.it-codigo
               lo-matplano.emp           = wep
               lo-matplano.estab         = westab
               lo-matplano.nr-pl         = wnr-pl
               lo-matplano.seq-it        = wseq.
            run proc_nota.
            ASSIGN lo-matplano.nro-docto = integer(wnrult)
                   lo-matplano.data-nota = wdtult.
            RUN disp_dados.
            END.
          END.
        IF LENGTH(wnr-pl) < 7 THEN 
        CREATE lo-matplano.
        ASSIGN wseq                      = wseq + 1
               lo-matplano.cod-emitente  = item-fornec.cod-emitente
               lo-matplano.it-codigo     = item-fornec.it-codigo
               lo-matplano.emp           = wep
               lo-matplano.estab         = westab
               lo-matplano.nr-pl         = wnr-pl
               lo-matplano.seq-it        = wseq.
        run proc_nota.
        ASSIGN lo-matplano.nro-docto = integer(wnrult)
               lo-matplano.data-nota = wdtult.
        RUN disp_dados.
        END.  
        /*RUN recebe.*/
        MESSAGE "GERA��O CONCLUIDA COM SUCESSO." VIEW-AS ALERT-BOX.
      END. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_emit C-Win 
PROCEDURE proc_emit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  FIND FIRST emitente WHERE cod-emitente = wcod-emitente NO-LOCK NO-ERROR.
  IF AVAILABLE emitente THEN DISPLAY emitente.nome-abrev @ wnome-abrev 
                        WITH FRAME {&FRAME-NAME}.
  IF NOT AVAILABLE emitente THEN DO:
    MESSAGE "EMITENTE INEXISTENTE." VIEW-AS ALERT-BOX.
    NEXT.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_nota C-Win 
PROCEDURE proc_nota :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN wnrult = ""
         wdtult = ?.
  FOR EACH recebimento WHERE recebimento.cod-emitente = 
                             lo-matplano.cod-emitente
                       AND   recebimento.it-codigo    = 
                             lo-matplano.it-codigo
                       AND   recebimento.cod-movto    = 1
                       NO-LOCK BREAK BY recebimento.data-nota
                       BY recebimento.numero-nota:
    IF  LAST-OF(recebimento.data-nota)  
    AND LAST-OF(recebimento.numero-nota) THEN DO:
      IF LAST(recebimento.data-nota) THEN 
        ASSIGN wdtult = recebimento.data-nota
               wnrult = recebimento.numero-nota.
    END.
  END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_tit C-Win 
PROCEDURE proc_tit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl = SUBSTRING(lo-matplano.nr-pl,1,6)
                       AND   lo-matdatas.emp     = lo-matplano.emp
                       AND   lo-matdatas.estab   = lo-matplano.estab
                       NO-LOCK NO-ERROR.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_tit_temp C-Win 
PROCEDURE proc_tit_temp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl   =  wnr-pl 
                       AND   lo-matdatas.emp     =  wep
                       AND   lo-matdatas.estab   =  westab 
                       NO-LOCK NO-ERROR.

 ASSIGN wqt-sem1:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem1
       wqt-sem1-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem1-2
       wqt-sem2:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem2
       wqt-sem2-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem2-2
       wqt-sem3:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem3
       wqt-sem3-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem3-2
       wqt-sem4:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem4
       wqt-sem4-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem4-2
       wqt-sem5:LABEL IN FRAME  {&FRAME-NAME}   = lo-matdatas.titsem5
       wqt-sem5-2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titsem5-2
       wmes1:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titmes1
       wmes2:LABEL IN FRAME  {&FRAME-NAME} = lo-matdatas.titmes2.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE recebe C-Win 
PROCEDURE recebe :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  FOR EACH lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                       AND   lo-matplano.emp          = wep
                       AND   lo-matplano.estab        = westab
                       AND   lo-matplano.nr-pl        = wnr-pl
                       USE-INDEX item:
    ASSIGN r-registro = RECID(lo-matplano).
    RUN disp_dados. 
    END.
  /*FOR EACH lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                       AND   lo-matplano.emp          = wep
                       AND   lo-matplano.estab        = westab
                       AND   lo-matplano.nr-pl        = wnr-pl
                       USE-INDEX item:
    ASSIGN r-registro = RECID(lo-matplano).
    run disp_tela.
    DISPLAY wnro-docto WITH FRAME {&FRAME-NAME}.
    ASSIGN wnro-docto.
    run disp_data.
    DISPLAY  wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
        wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
        wqt-sem5-2 wmes1 wmes2 WITH FRAME {&FRAME-NAME}.
    
    ASSIGN wit-codigo wnro-docto wdatatela wimediato wqt-sem1 wqt-sem2 wqt-sem3 wqt-sem4        
        wqt-sem5 wqt-sem1-2 wqt-sem2-2 wqt-sem3-2 wqt-sem4-2
        wqt-sem5-2 wmes1 wmes2.
    ASSIGN 
              lo-matplano.nro-docto     = wnro-docto
              lo-matplano.data-nota     = wdatatela
              lo-matplano.imediato      = wimediato
              lo-matplano.qt-sem1       = wqt-sem1
              lo-matplano.qt-sem2       = wqt-sem2
              lo-matplano.qt-sem3       = wqt-sem3
              lo-matplano.qt-sem4       = wqt-sem4
              lo-matplano.qt-sem5       = wqt-sem5   
              lo-matplano.qt-sem1       = wqt-sem1
              lo-matplano.qt-sem2       = wqt-sem2
              lo-matplano.qt-sem3       = wqt-sem3
              lo-matplano.qt-sem4       = wqt-sem4
              lo-matplano.qt-sem5       = wqt-sem5   
              lo-matplano.qt-sem1-2     = wqt-sem1-2
              lo-matplano.qt-sem2-2     = wqt-sem2-2
              lo-matplano.qt-sem3-2     = wqt-sem3-2
              lo-matplano.qt-sem4-2     = wqt-sem4-2
              lo-matplano.qt-sem5-2     = wqt-sem5-2 
              lo-matplano.mes1          = wmes1
              lo-matplano.mes2          = wmes2.
    END.
    RUN disp_dados.    */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

