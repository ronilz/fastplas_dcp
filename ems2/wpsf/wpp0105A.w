&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win

/* Temp-Table and Buffer definitions                                    */
DEFINE TEMP-TABLE tt-lo-matdatas NO-UNDO LIKE lo-matdatas.


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wmeses AS CHAR FORMAT "X(03)" INITIAL
   "Jan, Fev, Mar, Abr, Mai, Jun, Jul, Ago, Set, Out, Nov, Dez".
DEFINE VARIABLE wcont  AS INTE FORMAT "99" INITIAL 0.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-1

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES lo-matdatas

/* Definitions for BROWSE BROWSE-1                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-1 lo-matdatas.emp lo-matdatas.estab ~
lo-matdatas.nr-pl lo-matdatas.titsem1 lo-matdatas.titsem2 ~
lo-matdatas.titsem3 lo-matdatas.titsem4 lo-matdatas.titsem5 ~
lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 lo-matdatas.titsem3-2 ~
lo-matdatas.titsem4-2 lo-matdatas.titsem5-2 lo-matdatas.titmes1 ~
lo-matdatas.titmes2 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-1 
&Scoped-define QUERY-STRING-BROWSE-1 FOR EACH lo-matdatas ~
      WHERE lo-matdatas.emp = wwemp and  ~
mgfas.lo-matdatas.estab = wwestab-ini and ~
mgfas.lo-matdatas.nr-pl = wwnr-plini NO-LOCK ~
    BY lo-matdatas.emp ~
       BY lo-matdatas.estab ~
        BY lo-matdatas.nr-pl INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-1 OPEN QUERY BROWSE-1 FOR EACH lo-matdatas ~
      WHERE lo-matdatas.emp = wwemp and  ~
mgfas.lo-matdatas.estab = wwestab-ini and ~
mgfas.lo-matdatas.nr-pl = wwnr-plini NO-LOCK ~
    BY lo-matdatas.emp ~
       BY lo-matdatas.estab ~
        BY lo-matdatas.nr-pl INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-1 lo-matdatas
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-1 lo-matdatas


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-1}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wwemp wwestab-ini wwnr-plini BUTTON-1 ~
BUTTON-3 bt-sair-3 BROWSE-1 wtit-sem bt-modif bt-confirma bt-cancela ~
bt-sair-2 wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 ~
wtit-mes bt-modif-2 bt-confirma-2 bt-cancela-2 bt-sair-4 wmes1 wmes2 ~
RECT-20 RECT-21 RECT-22 RECT-23 RECT-24 
&Scoped-Define DISPLAYED-OBJECTS wwemp wwestab-ini wwnr-plini wtit-sem ~
wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wtit-mes wmes1 ~
wmes2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancela AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-cancel.bmp":U
     LABEL "Cancela" 
     SIZE 4 BY 1.13 TOOLTIP "Cancela Opera��o"
     BGCOLOR 8 .

DEFINE BUTTON bt-cancela-2 AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-cancel.bmp":U
     LABEL "Cancela" 
     SIZE 4 BY 1.13 TOOLTIP "Cancela Opera��o"
     BGCOLOR 8 .

DEFINE BUTTON bt-confirma 
     IMAGE-UP FILE "image/im-ok.bmp":U
     LABEL "Confirma" 
     SIZE 4 BY 1.13 TOOLTIP "Confirma opera��o".

DEFINE BUTTON bt-confirma-2 
     IMAGE-UP FILE "image/im-ok.bmp":U
     LABEL "Confirma" 
     SIZE 4 BY 1.13 TOOLTIP "Confirma opera��o".

DEFINE BUTTON bt-modif 
     IMAGE-UP FILE "image/gr-mod.bmp":U
     LABEL "Modifica" 
     SIZE 5 BY 1.13 TOOLTIP "Modifica registro".

DEFINE BUTTON bt-modif-2 
     IMAGE-UP FILE "image/gr-mod.bmp":U
     LABEL "Modifica" 
     SIZE 5 BY 1.13 TOOLTIP "Modifica registro".

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON bt-sair-3 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-sair-4 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "image/im-ok.bmp":U
     LABEL "Button 1" 
     SIZE 5 BY 1.13.

DEFINE BUTTON BUTTON-3 
     LABEL "Ver todos os planos" 
     SIZE 15 BY 1
     FONT 1.

DEFINE VARIABLE wmes1 AS CHARACTER FORMAT "Xxx" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wmes2 AS CHARACTER FORMAT "Xxx" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem1 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem10 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem2 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem3 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem4 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem5 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem6 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem7 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem8 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wsem9 AS CHARACTER FORMAT "99Xxx" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wtit-mes AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "M�s a ser alterado" 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wtit-sem AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "Semana a ser alterada" 
     VIEW-AS FILL-IN 
     SIZE 4 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wwemp AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE wwestab-ini AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estab. Inicial" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 NO-UNDO.

DEFINE VARIABLE wwnr-plini AS CHARACTER FORMAT "999999X":U 
     LABEL "Nr.Plano Inicial" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-20
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 104 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-21
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 104 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-22
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 104 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-23
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 104 BY 7.

DEFINE RECTANGLE RECT-24
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 104 BY .75
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-1 FOR 
      lo-matdatas SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-1 C-Win _STRUCTURED
  QUERY BROWSE-1 NO-LOCK DISPLAY
      lo-matdatas.emp FORMAT ">>9":U
      lo-matdatas.estab FORMAT "x(3)":U
      lo-matdatas.nr-pl FORMAT "999999X":U WIDTH 7.14
      lo-matdatas.titsem1 COLUMN-LABEL "Sem [1]" FORMAT "X(09)":U
      lo-matdatas.titsem2 COLUMN-LABEL "Sem [2]" FORMAT "X(09)":U
      lo-matdatas.titsem3 COLUMN-LABEL "Sem [3]" FORMAT "X(09)":U
      lo-matdatas.titsem4 COLUMN-LABEL "Sem [4]" FORMAT "X(09)":U
            WIDTH 6.29
      lo-matdatas.titsem5 COLUMN-LABEL "Sem [5]" FORMAT "X(09)":U
      lo-matdatas.titsem1-2 COLUMN-LABEL "Sem [6]" FORMAT "X(09)":U
            WIDTH 7.43
      lo-matdatas.titsem2-2 COLUMN-LABEL "Sem [7]" FORMAT "X(09)":U
            WIDTH 7.29
      lo-matdatas.titsem3-2 COLUMN-LABEL "Sem [8]" FORMAT "X(09)":U
            WIDTH 7.43
      lo-matdatas.titsem4-2 COLUMN-LABEL "Sem [9]" FORMAT "X(09)":U
            WIDTH 7.29
      lo-matdatas.titsem5-2 COLUMN-LABEL "Sem [10]" FORMAT "X(09)":U
            WIDTH 7.43
      lo-matdatas.titmes1 COLUMN-LABEL "Mes 1" FORMAT "X(09)":U
            WIDTH 5.43
      lo-matdatas.titmes2 COLUMN-LABEL "Mes 2" FORMAT "X(09)":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104 BY 5
         FONT 1 ROW-HEIGHT-CHARS .67 EXPANDABLE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wwemp AT ROW 2.25 COL 11 COLON-ALIGNED
     wwestab-ini AT ROW 2.25 COL 33 COLON-ALIGNED
     wwnr-plini AT ROW 2.25 COL 58 COLON-ALIGNED
     BUTTON-1 AT ROW 2.25 COL 71
     BUTTON-3 AT ROW 2.25 COL 82
     bt-sair-3 AT ROW 2.25 COL 99
     BROWSE-1 AT ROW 4.25 COL 2
     wtit-sem AT ROW 11.25 COL 41 COLON-ALIGNED
     bt-modif AT ROW 11.25 COL 47
     bt-confirma AT ROW 11.25 COL 52
     bt-cancela AT ROW 11.25 COL 56
     bt-sair-2 AT ROW 11.25 COL 60
     wsem1 AT ROW 14.25 COL 13 COLON-ALIGNED NO-LABEL
     wsem2 AT ROW 14.25 COL 31 COLON-ALIGNED NO-LABEL
     wsem3 AT ROW 14.25 COL 48 COLON-ALIGNED NO-LABEL
     wsem4 AT ROW 14.25 COL 67 COLON-ALIGNED NO-LABEL
     wsem5 AT ROW 14.25 COL 86 COLON-ALIGNED NO-LABEL
     wsem6 AT ROW 15.5 COL 13 COLON-ALIGNED NO-LABEL
     wsem7 AT ROW 15.5 COL 31 COLON-ALIGNED NO-LABEL
     wsem8 AT ROW 15.5 COL 48 COLON-ALIGNED NO-LABEL
     wsem9 AT ROW 15.5 COL 67 COLON-ALIGNED NO-LABEL
     wsem10 AT ROW 15.5 COL 86 COLON-ALIGNED NO-LABEL
     wtit-mes AT ROW 19 COL 42 COLON-ALIGNED
     bt-modif-2 AT ROW 19 COL 48
     bt-confirma-2 AT ROW 19 COL 53
     bt-cancela-2 AT ROW 19 COL 57
     bt-sair-4 AT ROW 19 COL 61
     wmes1 AT ROW 20.5 COL 47 COLON-ALIGNED NO-LABEL
     wmes2 AT ROW 21.75 COL 47 COLON-ALIGNED NO-LABEL
     RECT-20 AT ROW 17 COL 2
     RECT-21 AT ROW 1.25 COL 2
     RECT-22 AT ROW 3.5 COL 2
     RECT-23 AT ROW 10 COL 2
     RECT-24 AT ROW 9.25 COL 2
     "Sem 10:" VIEW-AS TEXT
          SIZE 6 BY .67 AT ROW 15.75 COL 82
          FONT 1
     "Formato do Campo Dia e M�s- Ex. 02Jan" VIEW-AS TEXT
          SIZE 44 BY .67 AT ROW 13 COL 34
          FGCOLOR 12 FONT 0
     "Sem 3:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 14.5 COL 45
          FONT 1
     "Sem 5:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 14.5 COL 83
          FONT 1
     "Sem 1:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 14.5 COL 10
          FONT 1
     "Sem 6:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 15.75 COL 10
          FONT 1
     "Sem 7:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 15.75 COL 28
          FONT 1
     "Altera��o de Datas das Semanas" VIEW-AS TEXT
          SIZE 23 BY .67 AT ROW 10.25 COL 43
     "Sem 8:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 15.75 COL 45
          FONT 1
     "Sem 9:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 15.75 COL 64
          FONT 1
     "Altera��o de Datas dos Meses" VIEW-AS TEXT
          SIZE 22 BY .67 AT ROW 18 COL 44
     "1o. M�s:" VIEW-AS TEXT
          SIZE 7 BY .67 AT ROW 20.75 COL 42
          FONT 1
     "2o. M�s:" VIEW-AS TEXT
          SIZE 7 BY .67 AT ROW 22 COL 42
          FONT 1
     "Sem 2:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 14.5 COL 28
          FONT 1
     "Sem 4:" VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 14.5 COL 64
          FONT 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 105.57 BY 22.17
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
   Temp-Tables and Buffers:
      TABLE: tt-lo-matdatas T "?" NO-UNDO mgfas lo-matdatas
   END-TABLES.
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Altera��o de Datas do Plano de Recebimento de Materiais"
         HEIGHT             = 22.17
         WIDTH              = 105.57
         MAX-HEIGHT         = 29.79
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.79
         VIRTUAL-WIDTH      = 146.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* BROWSE-TAB BROWSE-1 bt-sair-3 DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-1
/* Query rebuild information for BROWSE BROWSE-1
     _TblList          = "mgfas.lo-matdatas"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "mgfas.lo-matdatas.emp|yes,mgfas.lo-matdatas.estab|yes,mgfas.lo-matdatas.nr-pl|yes"
     _Where[1]         = "mgfas.lo-matdatas.emp = wwemp and 
mgfas.lo-matdatas.estab = wwestab-ini and
mgfas.lo-matdatas.nr-pl = wwnr-plini"
     _FldNameList[1]   = mgfas.lo-matdatas.emp
     _FldNameList[2]   = mgfas.lo-matdatas.estab
     _FldNameList[3]   > mgfas.lo-matdatas.nr-pl
"lo-matdatas.nr-pl" ? ? "character" ? ? ? ? ? ? no ? no no "7.14" yes no no "U" "" ""
     _FldNameList[4]   > mgfas.lo-matdatas.titsem1
"lo-matdatas.titsem1" "Sem [1]" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[5]   > mgfas.lo-matdatas.titsem2
"lo-matdatas.titsem2" "Sem [2]" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[6]   > mgfas.lo-matdatas.titsem3
"lo-matdatas.titsem3" "Sem [3]" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[7]   > mgfas.lo-matdatas.titsem4
"lo-matdatas.titsem4" "Sem [4]" ? "character" ? ? ? ? ? ? no ? no no "6.29" yes no no "U" "" ""
     _FldNameList[8]   > mgfas.lo-matdatas.titsem5
"lo-matdatas.titsem5" "Sem [5]" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _FldNameList[9]   > mgfas.lo-matdatas.titsem1-2
"lo-matdatas.titsem1-2" "Sem [6]" ? "character" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" ""
     _FldNameList[10]   > mgfas.lo-matdatas.titsem2-2
"lo-matdatas.titsem2-2" "Sem [7]" ? "character" ? ? ? ? ? ? no ? no no "7.29" yes no no "U" "" ""
     _FldNameList[11]   > mgfas.lo-matdatas.titsem3-2
"lo-matdatas.titsem3-2" "Sem [8]" ? "character" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" ""
     _FldNameList[12]   > mgfas.lo-matdatas.titsem4-2
"lo-matdatas.titsem4-2" "Sem [9]" ? "character" ? ? ? ? ? ? no ? no no "7.29" yes no no "U" "" ""
     _FldNameList[13]   > mgfas.lo-matdatas.titsem5-2
"lo-matdatas.titsem5-2" "Sem [10]" ? "character" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" ""
     _FldNameList[14]   > mgfas.lo-matdatas.titmes1
"lo-matdatas.titmes1" "Mes 1" ? "character" ? ? ? ? ? ? no ? no no "5.43" yes no no "U" "" ""
     _FldNameList[15]   > mgfas.lo-matdatas.titmes2
"lo-matdatas.titmes2" "Mes 2" ? "character" ? ? ? ? ? ? no ? no no ? yes no no "U" "" ""
     _Query            is OPENED
*/  /* BROWSE BROWSE-1 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Altera��o de Datas do Plano de Recebimento de Materiais */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Altera��o de Datas do Plano de Recebimento de Materiais */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-1
&Scoped-define SELF-NAME BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-1 C-Win
ON VALUE-CHANGED OF BROWSE-1 IN FRAME DEFAULT-FRAME
DO: 
  ASSIGN wwemp        = lo-matdatas.emp
         wwestab-ini  = lo-matdatas.estab
         wwnr-plini   = lo-matdatas.nr-pl.
  DISPLAY wwemp wwestab-ini wwnr-plini WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancela
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancela C-Win
ON CHOOSE OF bt-cancela IN FRAME DEFAULT-FRAME /* Cancela */
DO:
  RUN cancela.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancela-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancela-2 C-Win
ON CHOOSE OF bt-cancela-2 IN FRAME DEFAULT-FRAME /* Cancela */
DO:
  RUN cancela.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-confirma
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-confirma C-Win
ON CHOOSE OF bt-confirma IN FRAME DEFAULT-FRAME /* Confirma */
DO:
    IF wtit-sem = 1 THEN ASSIGN wsem1.
    IF wtit-sem = 2 THEN ASSIGN wsem2.
    IF wtit-sem = 3 THEN ASSIGN wsem3. 
    IF wtit-sem = 4 THEN ASSIGN wsem4.
    IF wtit-sem = 5 THEN ASSIGN wsem5.
    IF wtit-sem = 6 THEN ASSIGN wsem6.
    IF wtit-sem = 7 THEN ASSIGN wsem7.
    IF wtit-sem = 8 THEN ASSIGN wsem8.
    IF wtit-sem = 9 THEN ASSIGN wsem9.
    IF wtit-sem = 10 THEN ASSIGN wsem10.
   /* aqui 
   IF SUBSTRING(wtit-sem,3,3) <> ENTRY(wtit-sem,wmeses)
       THEN MESSAGE "teste teste " VIEW-AS ALERT-BOX.
           */

     FOR EACH lo-matdatas WHERE lo-matdatas.emp    = wwemp
                                           AND   lo-matdatas.estab  = wwestab-ini
                                           AND   lo-matdatas.nr-pl  = wwnr-plini:
  
    IF wsem1 <> "" THEN  
    ASSIGN lo-matdatas.titsem1   = " " 
                                 + SUBSTRING(wsem1,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem1,3,3). 
    IF wsem2 <> "" THEN  
    ASSIGN lo-matdatas.titsem2   = " " 
                                 + SUBSTRING(wsem2,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem2,3,3). 
    IF wsem3 <> "" THEN  
     ASSIGN lo-matdatas.titsem3   = " " 
                                  + SUBSTRING(wsem3,1,2) 
                                  + " " 
                                  + "/ " 
                                  + SUBSTRING(wsem3,3,3). 

    IF wsem4 <> "" THEN  
    ASSIGN lo-matdatas.titsem4  = " " 
                                 + SUBSTRING(wsem4,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem4,3,3). 
    IF wsem5 <> "" THEN  
    ASSIGN lo-matdatas.titsem5  = " " 
                                 + SUBSTRING(wsem5,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem5,3,3). 

    IF wsem6 <> "" THEN  
    ASSIGN lo-matdatas.titsem1-2  = " " 
                                 + SUBSTRING(wsem6,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem6,3,3).
    IF wsem7 <> "" THEN  
    ASSIGN lo-matdatas.titsem2-2  = " " 
                                 + SUBSTRING(wsem7,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem7,3,3).
    IF wsem8 <> "" THEN  
    ASSIGN lo-matdatas.titsem3-2  = " " 
                                 + SUBSTRING(wsem8,1,2) 
                                 + " " 
                                 + "/ " 
                                 + SUBSTRING(wsem8,3,3).
    IF wsem9 <> "" THEN  
    ASSIGN lo-matdatas.titsem4-2  = " " 
                              + SUBSTRING(wsem9,1,2) 
                              + " " 
                              + "/ " 
                              + SUBSTRING(wsem9,3,3).
    IF wsem10 <> "" THEN  
    ASSIGN lo-matdatas.titsem5-2  = " " 
                              + SUBSTRING(wsem10,1,2) 
                              + " " 
                              + "/ " 
                              + SUBSTRING(wsem10,3,3).



  END.
  DISABLE wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wmes1 wmes2
         WITH FRAME  {&FRAME-NAME}.
  OPEN QUERY browse-1 FOR EACH lo-matdatas WHERE lo-matdatas.emp    = wwemp
                                           AND   lo-matdatas.estab  = wwestab-ini
                                           AND   lo-matdatas.nr-pl  = wwnr-plini
                                           NO-LOCK.
  GET FIRST browse-1.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-confirma-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-confirma-2 C-Win
ON CHOOSE OF bt-confirma-2 IN FRAME DEFAULT-FRAME /* Confirma */
DO:
    IF wtit-mes = 1 THEN ASSIGN wmes1.
    IF wtit-mes = 2 THEN ASSIGN wmes2.
  


  FOR EACH lo-matdatas WHERE lo-matdatas.emp    = wwemp
                                           AND   lo-matdatas.estab  = wwestab-ini
                                           AND   lo-matdatas.nr-pl  = wwnr-plini:
  
    IF wmes1 <> "" THEN  
    ASSIGN lo-matdatas.titmes1  = FILL(" ",5)
                                 + SUBSTRING(wmes1,1,3). 
    IF wmes2 <> "" THEN  
    ASSIGN lo-matdatas.titmes2   = FILL(" ",5)
                                 + SUBSTRING(wmes2,1,3).
   
  END.
  DISABLE wmes1 wmes2
         WITH FRAME  {&FRAME-NAME}.

  OPEN QUERY browse-1 FOR EACH lo-matdatas WHERE lo-matdatas.emp    = wwemp
                                           AND   lo-matdatas.estab  = wwestab-ini
                                           AND   lo-matdatas.nr-pl  = wwnr-plini
                                           NO-LOCK.
  GET FIRST browse-1.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-modif
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-modif C-Win
ON CHOOSE OF bt-modif IN FRAME DEFAULT-FRAME /* Modifica */
DO:
    /* habilita campos */
    ASSIGN wtit-sem.
    IF wtit-sem = 1 THEN ENABLE wsem1  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 2 THEN ENABLE wsem2  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 3 THEN ENABLE wsem3  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 4 THEN ENABLE wsem4  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 5 THEN ENABLE wsem5  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 6 THEN ENABLE wsem6  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 7 THEN ENABLE wsem7  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 8 THEN ENABLE wsem8  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 9 THEN ENABLE wsem9  WITH FRAME  {&FRAME-NAME}.
    IF wtit-sem = 10 THEN ENABLE wsem10 WITH FRAME  {&FRAME-NAME}.
    
   /* recebe dados */ 
   IF wtit-sem = 1 THEN ASSIGN wsem1.
   IF wtit-sem = 2 THEN ASSIGN wsem2.
   IF wtit-sem = 3 THEN ASSIGN wsem3. 
   IF wtit-sem = 4 THEN ASSIGN wsem4.
   IF wtit-sem = 5 THEN ASSIGN wsem5.
   IF wtit-sem = 6 THEN ASSIGN wsem6.
   IF wtit-sem = 7 THEN ASSIGN wsem7.
   IF wtit-sem = 8 THEN ASSIGN wsem8.
   IF wtit-sem = 9 THEN ASSIGN wsem9.
   IF wtit-sem = 10 THEN ASSIGN wsem10.

   

  ENABLE bt-confirma bt-cancela WITH FRAME  {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-modif-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-modif-2 C-Win
ON CHOOSE OF bt-modif-2 IN FRAME DEFAULT-FRAME /* Modifica */
DO:
    /* habilita campos */
    ASSIGN wtit-mes.
    IF wtit-mes = 1 THEN ENABLE wmes1 WITH FRAME  {&FRAME-NAME}.
    IF wtit-mes = 2 THEN ENABLE wmes2  WITH FRAME  {&FRAME-NAME}.

   /* recebe dados */ 
   IF wtit-mes = 1 THEN ASSIGN wmes1.
   IF wtit-mes = 2 THEN ASSIGN wmes2.

   ENABLE bt-confirma-2 bt-cancela-2 WITH FRAME  {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-3 C-Win
ON CHOOSE OF bt-sair-3 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-4 C-Win
ON CHOOSE OF bt-sair-4 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  ASSIGN wwemp wwestab-ini  wwnr-plini.
  OPEN QUERY browse-1 FOR EACH lo-matdatas WHERE lo-matdatas.emp    = wwemp
                                           AND   lo-matdatas.estab  = wwestab-ini
                                           AND   lo-matdatas.nr-pl  = wwnr-plini
                                           NO-LOCK.
  GET FIRST browse-1.
 /* RUN carrega.*/
  ENABLE wtit-mes wtit-sem bt-modif bt-modif-2 BROWSE-1 WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-3 C-Win
ON CHOOSE OF BUTTON-3 IN FRAME DEFAULT-FRAME /* Ver todos os planos */
DO:
  OPEN QUERY browse-1 FOR EACH lo-matdatas  NO-LOCK .
  GET FIRST browse-1.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cancela C-Win 
PROCEDURE cancela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 DISPLAY wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wmes1 wmes2
         WITH FRAME  {&FRAME-NAME}.
 DISABLE wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wmes1 wmes2
         BROWSE-1 WITH FRAME  {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE carrega C-Win 
PROCEDURE carrega :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wsem1  = lo-matdatas.titsem1
       wsem2  = lo-matdatas.titsem2
       wsem3  = lo-matdatas.titsem3
       wsem4  = lo-matdatas.titsem4
       wsem5  = lo-matdatas.titsem5
       wsem6  = lo-matdatas.titsem1-2
       wsem7  = lo-matdatas.titsem2-2
       wsem8  = lo-matdatas.titsem3-2
       wsem9  = lo-matdatas.titsem4-2
       wsem10 = lo-matdatas.titsem5-2
       wmes1  = lo-matdatas.titmes1 
       wmes2  = lo-matdatas.titmes2.
 DISPLAY wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wmes1 wmes2
         WITH FRAME  {&FRAME-NAME}.
 DISABLE wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wmes1 wmes2
         WITH FRAME  {&FRAME-NAME}.
 END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wwemp wwestab-ini wwnr-plini wtit-sem wsem1 wsem2 wsem3 wsem4 wsem5 
          wsem6 wsem7 wsem8 wsem9 wsem10 wtit-mes wmes1 wmes2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE wwemp wwestab-ini wwnr-plini BUTTON-1 BUTTON-3 bt-sair-3 BROWSE-1 
         wtit-sem bt-modif bt-confirma bt-cancela bt-sair-2 wsem1 wsem2 wsem3 
         wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wtit-mes bt-modif-2 
         bt-confirma-2 bt-cancela-2 bt-sair-4 wmes1 wmes2 RECT-20 RECT-21 
         RECT-22 RECT-23 RECT-24 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ASSIGN wwemp       = 1
         wwestab-ini = "1"
         wwnr-plini = STRING(MONTH(TODAY),"99") + STRING(YEAR(TODAY),"9999")
         wsem1 = ""
         wsem2 = ""
         wsem3 = ""
         wsem4 = ""
         wsem5 = ""
         wsem6 = ""
         wsem7 = ""
         wsem8 = ""
         wsem9 = ""
         wsem10 = "".
  DISPLAY wwemp wwestab-ini wwnr-plini WITH FRAME  {&FRAME-NAME}.
  DISABLE wsem1 wsem2 wsem3 wsem4 wsem5 wsem6 wsem7 wsem8 wsem9 wsem10 wmes1 wmes2
          wtit-sem wtit-mes
         WITH FRAME  {&FRAME-NAME}.
  DISABLE bt-modif bt-confirma bt-cancela bt-modif-2 bt-confirma-2 bt-cancela-2
          WITH FRAME  {&FRAME-NAME}.
  /* mostra os plano do ano corrente */
  OPEN QUERY browse-1 FOR EACH lo-matdatas WHERE substring(nr-pl,3,4) = string(year(today)) NO-LOCK BY nr-pl DESCENDING.
  GET FIRST browse-1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

