&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
{utils/retorna_diretorio_usuario.i}
{utp/ut-glob.i}
DEFINE VARIABLE procname AS CHAR NO-UNDO.
DEFINE VARIABLE okbt     AS LOGICAL INITIAL TRUE.

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE wseq            LIKE lo-matplano.seq-it.
DEFINE VARIABLE wcriaplano      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wdatapesq       LIKE recebimento.data-nota.
DEFINE VARIABLE wgera-novo       AS LOGICAL FORMAT "Sim/N�o".
/* outras variaveis */

/* Variaveis referente a criacao dos titulos -------------------------------*/
DEFINE VARIABLE wpl-ant         LIKE lo-matplano.nr-pl.                    
DEFINE VARIABLE wcont           AS INTE FORMAT 9.
DEFINE VARIABLE wcont-2         AS INTE FORMAT 9.
DEFINE VARIABLE dia             AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE dia-2           AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE data            AS DATE FORMAT "99/99/9999" INITIAL ?.
DEFINE VARIABLE wmes            AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wmes1-ab        AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wano1-ab        AS INTE FORMAT 9999 INITIAL 0.
DEFINE VARIABLE wmes2-ab        AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wano2-ab        AS INTE FORMAT 9999 INITIAL 0.
DEFINE VARIABLE wwmes1          AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wwmes2          AS INTE FORMAT 99 INITIAL 0.
DEFINE VARIABLE wmestit         AS CHAR FORMAT "X(15)".
DEFINE VARIABLE meslist         AS CHAR FORMAT "X(15)" INITIAL
       " Jan, Fev, Mar, Abr, Mai, Jun, Jul, Ago, Set, Out, Nov, Dez".
DEFINE VARIABLE wmes-aux1       AS INTE FORMAT "99".
DEFINE VARIABLE wano-aux1       AS INTE FORMAT "9999".
DEFINE VARIABLE wmes-aux2       AS INTE FORMAT "99".
DEFINE VARIABLE wano-aux2       AS INTE FORMAT "9999".
DEFINE VARIABLE c-rodape        AS CHAR FORMAT "X(110)".
DEFINE VARIABLE wultdia1-ab     AS DATE.
DEFINE VARIABLE wcont1-ab       AS INTEGER FORMAT "99".
DEFINE VARIABLE wultdia2-ab     AS DATE.
DEFINE VARIABLE wcont2-ab       AS INTEGER FORMAT "99".
DEFINE VARIABLE wtitsem         AS CHAR FORMAT "X(09)" EXTENT 4 INITIAL " ".
DEFINE VARIABLE wtitsem1        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem2        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem3        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem4        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem5        AS CHAR FORMAT "X(09)" INITIAL " ".

DEFINE VARIABLE wtitsem1-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem2-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem3-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem4-2      AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitsem5-2      AS CHAR FORMAT "X(09)" INITIAL " ".

DEFINE VARIABLE wtitmes1        AS CHAR FORMAT "X(09)" INITIAL " ".
DEFINE VARIABLE wtitmes2        AS CHAR FORMAT "X(09)" INITIAL " ".

DEFINE VARIABLE cmode           AS CHAR NO-UNDO.

DEFINE VARIABLE wnrult LIKE recebimento.numero-nota.
DEFINE VARIABLE wdtult LIKE recebimento.data-nota.

 
DEF VAR c-arquivo-dados-plano AS CHAR NO-UNDO FORMAT "X(70)".


/* Variaveis para importa��o */

def var v-chr-excel-application as office.iface.excel.ExcelWrapper no-undo. 
def var v-chr-work-book as office.iface.excel.Workbook no-undo. 
def var v-chr-work-sheet as office.iface.excel.WorkSheet no-undo. 
def var v-chr-range as character no-undo. 
def var v-int-line as integer no-undo initial 1. 
def var v-int-ultimalinha as integer FORMAT "9999999999" NO-UNDO.

def var i-pasta-select as dec. 

def var v-cod-emitente      LIKE emitente.cod-emitente.
def var v-it-codigo         LIKE ITEM.it-codigo.
def var v-descricao         LIKE ITEM.descricao-1.
def var v-chr-campo-4       as INTEGER FORMAT "->,>>>,>>9".
def var v-chr-campo-5       as INTEGER FORMAT "->,>>>,>>9".
def var v-chr-campo-6       as INTEGER FORMAT "->,>>>,>>9".
def var v-chr-campo-7       as INTEGER FORMAT "->,>>>,>>9".
def var v-chr-campo-8       as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem1           as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem2           as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem3           as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem4           as INTEGER FORMAT "->,>>>,>>9".
def VAR v-qt-sem5           as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem1-2         as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem2-2         as INTEGER FORMAT "->,>>>,>>9".
def VAR v-qt-sem3-2         as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem4-2         as INTEGER FORMAT "->,>>>,>>9".
def var v-qt-sem5-2         as INTEGER FORMAT "->,>>>,>>9".
def var v-mes1              as INTEGER FORMAT "->,>>>,>>9".
def var v-mes2              as INTEGER FORMAT "->,>>>,>>9".
DEF VAR v-estab             LIKE estabelec.cod-emitente.

def temp-table tt-dados 
FIELD c-cod-emitente    LIKE emitente.cod-emitente         /* cod-fornecedor */
field c-it-codigo       LIKE ITEM.it-codigo                /* item */
field c-descricao       LIKE ITEM.descricao-1              /* descri��o */
field campo-4           as INTEGER FORMAT "->,>>>,>>9"     /* estoque */
field campo-5           as INTEGER FORMAT "->,>>>,>>9"     /* cq */
field campo-6           as INTEGER FORMAT "->,>>>,>>9"     /**/
field campo-7           as INTEGER FORMAT "->,>>>,>>9"
field campo-8           as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem1         as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem2         as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem3         as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem4         as INTEGER FORMAT "->,>>>,>>9" 
field c-qt-sem5         as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem1-2       as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem2-2       as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem3-2       as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem4-2       as INTEGER FORMAT "->,>>>,>>9"
field c-qt-sem5-2       as INTEGER FORMAT "->,>>>,>>9"
field c-mes1            as INTEGER FORMAT "->,>>>,>>9"
field c-mes2            as INTEGER FORMAT "->,>>>,>>9"
FIELD c-estab           AS CHAR FORMAT "x(03)".

/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR FORMAT "x(40)" NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.
/* acompanhamento */
def var h-acomp         as handle no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-20 RECT-21 RECT-3 ~
bt-elimina bt-consulta bt-importa BUTTON-3 BUTTON-1 bt-sair BUTTON-2 ~
warq-importado wep westab wnr-pl wdata-geracao wrellog wpasta 
&Scoped-Define DISPLAYED-OBJECTS warq-importado wep westab wnr-pl ~
wdata-geracao wrellog wpasta 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-consulta 
     LABEL "Consulta Plano" 
     SIZE 13 BY 1.13.

DEFINE BUTTON bt-elimina 
     IMAGE-UP FILE "image/im-era.bmp":U
     LABEL "Button 4" 
     SIZE 5 BY 1.13 TOOLTIP "Elimina plano".

DEFINE BUTTON bt-importa 
     IMAGE-UP FILE "image/im-fold.bmp":U
     LABEL "Importa Planos" 
     SIZE 5 BY 1.13 TOOLTIP "Procura arquivo para importar".

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 4 BY 1.13 TOOLTIP "Sair"
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "image/im-consulta.bmp":U
     LABEL "Visualiza Arquivo importado" 
     SIZE 5 BY 1.13 TOOLTIP "Verifica relat�rio de consist�ncia de importa��o".

DEFINE BUTTON BUTTON-2 
     IMAGE-UP FILE "image/im-hel.bmp":U
     LABEL "Instru��es" 
     SIZE 5 BY 1.13 TOOLTIP "Ajuda".

DEFINE BUTTON BUTTON-3 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 3" 
     SIZE 5 BY 1.13 TOOLTIP "Confirma importa��o do arquivo selecionado".

DEFINE VARIABLE warq-importado AS CHARACTER FORMAT "X(40)":U 
     LABEL "Arquivo a ser importado" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1.04 NO-UNDO.

DEFINE VARIABLE wdata-geracao AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Gera��o" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 4 BY .92 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 4 BY .92 NO-UNDO.

DEFINE VARIABLE wnr-pl AS CHARACTER FORMAT "999999x":U 
     LABEL "N�mero do Plano" 
     VIEW-AS FILL-IN 
     SIZE 8 BY 1 TOOLTIP "M�s (2 d�gitos) e Ano (4 d�gitos)" NO-UNDO.

DEFINE VARIABLE wpasta AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "No. da Pasta da Planilha do Excel" 
     VIEW-AS FILL-IN 
     SIZE 5 BY 1 NO-UNDO.

DEFINE VARIABLE wrellog AS CHARACTER FORMAT "X(40)":U 
     LABEL "Nome rel. de consist�ncia" 
     VIEW-AS FILL-IN 
     SIZE 37 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 85 BY 1.25
     BGCOLOR 8 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 86 BY .75
     BGCOLOR 8 .

DEFINE RECTANGLE RECT-20
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86 BY 2.75.

DEFINE RECTANGLE RECT-21
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86 BY 12.5.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 86 BY 1
     BGCOLOR 8 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bt-elimina AT ROW 2.75 COL 7
     bt-consulta AT ROW 2.75 COL 21 WIDGET-ID 2
     bt-importa AT ROW 2.75 COL 50
     BUTTON-3 AT ROW 2.75 COL 55
     BUTTON-1 AT ROW 2.75 COL 60
     bt-sair AT ROW 2.75 COL 79
     BUTTON-2 AT ROW 2.75 COL 83
     warq-importado AT ROW 9.5 COL 35 COLON-ALIGNED
     wep AT ROW 10.75 COL 35 COLON-ALIGNED
     westab AT ROW 11.75 COL 35 COLON-ALIGNED
     wnr-pl AT ROW 12.75 COL 35 COLON-ALIGNED
     wdata-geracao AT ROW 13.75 COL 35 COLON-ALIGNED
     wrellog AT ROW 14.75 COL 35 COLON-ALIGNED
     wpasta AT ROW 15.75 COL 35 COLON-ALIGNED
     "  Elimina Plano" VIEW-AS TEXT
          SIZE 11 BY .54 AT ROW 2.25 COL 4
     "Par�metros para importa��o do Plano de Entrega de Materiais" VIEW-AS TEXT
          SIZE 71 BY .67 AT ROW 7 COL 9
          FONT 0
     "      Importa   Plano" VIEW-AS TEXT
          SIZE 15 BY .54 AT ROW 2.25 COL 50
     RECT-1 AT ROW 19 COL 3
     RECT-2 AT ROW 1.25 COL 3
     RECT-20 AT ROW 2 COL 3
     RECT-21 AT ROW 6.25 COL 3
     RECT-3 AT ROW 5.25 COL 3
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 90.43 BY 19.5
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Importa��o Plano de Entrega de Materiais - wpp0110 - 10/2011"
         HEIGHT             = 19.5
         WIDTH              = 90.43
         MAX-HEIGHT         = 29.88
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.88
         VIRTUAL-WIDTH      = 146.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
ASSIGN 
       wpasta:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Importa��o Plano de Entrega de Materiais - wpp0110 - 10/2011 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Importa��o Plano de Entrega de Materiais - wpp0110 - 10/2011 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-consulta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-consulta C-Win
ON CHOOSE OF bt-consulta IN FRAME DEFAULT-FRAME /* Consulta Plano */
DO:
  RUN wpsf/wpp0111.r.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-elimina
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-elimina C-Win
ON CHOOSE OF bt-elimina IN FRAME DEFAULT-FRAME /* Button 4 */
DO:
  RUN wpsf/wpp0110a.r.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-importa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-importa C-Win
ON CHOOSE OF bt-importa IN FRAME DEFAULT-FRAME /* Importa Planos */
DO:

    DEF VAR c-dir-inicial AS CHAR NO-UNDO.

    RUN pi-retorna-diretorio 
         (c-seg-usuario,
         OUTPUT c-dir-inicial).


    RUN inicia.  
    SYSTEM-DIALOG GET-FILE procname
       TITLE   "Escolha o nome de arquivo para importa��o:"
       FILTERS  "xlsx" "*.xlsx",
                "xls" "*.xls"
        INITIAL-DIR c-dir-inicial 
        MUST-EXIST
        USE-FILENAME.
    ASSIGN warq-importado = procname.
    DISPLAY warq-importado WITH FRAME  {&FRAME-NAME}.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
 &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Visualiza Arquivo importado */
DO:
  RUN gera-tela.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME DEFAULT-FRAME /* Instru��es */
DO:
  MESSAGE "Esta rotina importa plano de entrega de materiais a partir de um arquivo excel (extens�o .xls ou xlsx)."
          SKIP(2) "1) ***** FORMATO DO ARQUIVO A SER IMPORTADO ***"
          SKIP(2) "a) IMPORTANTE **** OS DADOS DEVER�O ESTAR NA PRIMEIRA PASTA DA PLANILHA."
          SKIP    "b) As primeiras 4 linhas est�o reservadas para o cabe�alho da planilha, e estas linhas n�o ser�o importadas."
          SKIP    "c) No arquivo Excel a partir da 5a. linha dever�o existir 20 colunas com dados a serem importados,com as seguintes informa��es (exatamente nesta ordem):"
          SKIP    "d)Fornec/item/descricao/Estoque/qtd/qtd/qtd/diferen�a/1a.semana/2a.semana/3a.semana/4a.semana/5a.semana/1a.semana/2a.semana/3a.semana/4a.semana/5a.semana/3o.M�s/4o.M�s."
          SKIP    "e) Podendo existir quantas linhas forem necess�rias, sendo cada linha para um item."
          SKIP(2) "2) O programa apresenta mensagem quando o plano/fornecedor/item existe e questiona se quer sobrepor ou n�o."
          SKIP(2) "3) Tamb�m � poss�vel eliminar todo o plano do m�s para todos os fornecedores."
          SKIP(2) "Observa��o: Na nota de corte para o emitente 6423 e 530 - Sorocaba � utilizada a �ltima nota de trasnfer�ncia,de Fastplas - Sorocaba para Fastplas - Diadema, com a natureza de opera��o 121A2."
                                                                                                                                                

VIEW-AS ALERT-BOX.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-3 C-Win
ON CHOOSE OF BUTTON-3 IN FRAME DEFAULT-FRAME /* Button 3 */
DO: 
 ASSIGN wep westab wnr-pl wpasta wrellog warq-importado wdata-geracao.
 .MESSAGE wep westab  VIEW-AS ALERT-BOX.
  run utp/ut-acomp.p persistent set h-acomp.  
           {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
           run pi-inicializar in h-acomp (input "Gerando":U). 
          
 RUN roda.

 IF SEARCH(wrellog) <> ? THEN DOS SILENT START notepad.exe VALUE(wrellog).

 run pi-finalizar in h-acomp.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.


RUN pi-retorna-diretorio 
     (c-seg-usuario,
     OUTPUT c-arquivo-dados-plano).

c-arquivo-dados-plano = c-arquivo-dados-plano + "\dados-plano.txt".

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN iniciO-prog.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE cria-tit C-Win 
PROCEDURE cria-tit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wcont           = 0
         wcont-2         = 0
         wcont1-ab       = 0
         wcont2-ab       = 0
         wtitsem1        = " "  
         wtitsem2        = " "
         wtitsem3        = " "
         wtitsem4        = " " 
         wtitsem5        = " " 
         wtitsem1-2      = " " 
         wtitsem2-2      = " " 
         wtitsem3-2      = " " 
         wtitsem4-2      = " " 
         wtitsem5-2      = " " 
         wtitmes1        = " " 
         wtitmes2        = " "
         data            = ?. 

   FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl = SUBSTRING(wnr-pl,1,6)
                          AND   lo-matdatas.emp   = wep
                          AND   lo-matdatas.estab = westab
                           NO-LOCK NO-ERROR.
  IF NOT AVAILABLE lo-matdatas THEN DO:
    CREATE lo-matdatas.
    ASSIGN  lo-matdatas.emp   = wep
            lo-matdatas.estab = westab
            lo-matdatas.nr-pl = wnr-pl.

    /* Assinala mes e ano em aberto */
    ASSIGN wmes     = INTEGER(SUBSTRING(lo-matdatas.nr-pl,1,2)).
           wano1-ab = INTEGER(SUBSTRING(lo-matdatas.nr-pl,3,4)).
    
    IF wmes = 10 THEN     
      ASSIGN wmes1-ab = 10
             wmes2-ab = 11
             wwmes1    = 12
             wwmes2    = 1
             wano1-ab = wano1-ab
             wano2-ab = wano1-ab.

    IF wmes = 11 THEN     
      ASSIGN wmes1-ab = 11
             wmes2-ab = 12
             wwmes1    = 1
             wwmes2    = 2
             wano1-ab = wano1-ab
             wano2-ab = wano1-ab.
  
    IF wmes = 12 THEN
      ASSIGN wmes1-ab = 12
             wmes2-ab = 1
             wwmes1    = 2
             wwmes2    = 3
             wano1-ab = wano1-ab
             wano2-ab = wano1-ab + 1.
       
    IF wmes  <> 10  
    AND wmes <> 11
    AND wmes <> 12 THEN ASSIGN 
        wmes1-ab = wmes
        wmes2-ab = wmes + 1
        wwmes1    = wmes2-ab + 1
        wwmes2    = wwmes1 + 1
        wano1-ab = wano1-ab
        wano2-ab = wano1-ab.

    ASSIGN lo-matdatas.titmes1 =  "     " + ENTRY(wwmes1,meslist)
           lo-matdatas.titmes2 =  "     " + ENTRY(wwmes2,meslist).
    /***********  TITULOS DAS SEMANAS PARA O 1O.MES EM ABERTO **********/
    /* Calcula ultimo dia do mes */

    ASSIGN wmes-aux1 = wmes1-ab + 1.
    IF wmes1-ab = 12 THEN ASSIGN wmes-aux1 = 1
                                 wano-aux1 = wano1-ab + 1.  
    ASSIGN wultdia1-ab  = DATE(wmes-aux1,01,wano1-ab) - 1
           wcont1-ab    = 
           INTEGER(SUBSTRING(STRING(wultdia1-ab,"99/99/9999"),1,2)).
    /* Assinala Titulo das Semanas ------------------------------------*/
    DO dia  = 1 TO wcont1-ab:
       data = DATE(wmes1-ab,dia,wano1-ab).
    
      IF WEEKDAY(data) = 2 THEN DO:
        wcont = wcont + 1.
        END.
      IF WEEKDAY(data) = 2 AND wcont = 1 THEN 
      ASSIGN wtitsem1  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem1 = wtitsem1.
   
      IF WEEKDAY(data) = 2 AND wcont = 2 THEN
      ASSIGN wtitsem2  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem2 = wtitsem2.
     
      IF WEEKDAY(data) = 2 AND wcont = 3 THEN 
      ASSIGN wtitsem3  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem3 = wtitsem3.
   
      IF WEEKDAY(data) = 2 AND wcont = 4 THEN 
      ASSIGN wtitsem4  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem4 = wtitsem4.
       
      IF WEEKDAY(data) = 2 AND wcont = 5 THEN 
      ASSIGN wtitsem5  = " " + STRING(DAY(data),"99") + " / " 
                       + SUBSTRING(ENTRY(wmes1-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem5 = wtitsem5.
      END.
                            
      /***********  TITULOS DAS SEMANAS PARA O 2o.MES EM ABERTO **********/
      /* Calcula ultimo dia do mes */        
      IF wmes2-ab <> 12 THEN
      ASSIGN wmes-aux2  = wmes2-ab + 1
             wano-aux2  = wano2-ab.
      IF wmes2-ab = 12 THEN ASSIGN wmes-aux2 = 1
                                   wano-aux2 = wano2-ab + 1.
      ASSIGN wultdia2-ab  = DATE(wmes-aux2,01,wano-aux2) - 1.       
             wcont2-ab     = 
             INTEGER(SUBSTRING(STRING(wultdia2-ab,"99/99/9999"),1,2)).
       
      /* Assinala Titulo das Semanas ------------------------------------*/
      DO dia-2 = 1 TO wcont2-ab:
         data  = DATE(wmes2-ab,dia-2,wano2-ab).
        IF WEEKDAY(data) = 2 THEN DO:
          wcont-2 = wcont-2 + 1.
        END.

      IF WEEKDAY(data)  = 2 AND wcont-2 = 1 THEN 
      ASSIGN wtitsem1-2 = " " + STRING(DAY(data),"99") + " / " 
                        + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem1-2 = wtitsem1-2.
        
      IF WEEKDAY(data)  = 2 AND wcont-2 = 2 THEN 
      ASSIGN wtitsem2-2 = " " + STRING(DAY(data),"99") + " / " 
                        + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem2-2 = wtitsem2-2.
    
      IF WEEKDAY(data)  = 2 AND wcont-2 = 3 THEN 
      ASSIGN wtitsem3-2 = " " + STRING(DAY(data),"99") + " / " 
                        + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem3-2 = wtitsem3-2.
    
      IF WEEKDAY(data) = 2 AND wcont-2 = 4 THEN 
      ASSIGN wtitsem4-2  = " " + STRING(DAY(data),"99") + " / " 
                         + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem4-2 = wtitsem4-2.    
      
      IF WEEKDAY(data) = 2 AND wcont-2 = 5 THEN 
      ASSIGN wtitsem5-2  = " " + STRING(DAY(data),"99") + " / " 
                               + SUBSTRING(ENTRY(wmes2-ab,meslist),2,3).
      ASSIGN lo-matdatas.titsem5-2 = wtitsem5-2.
      END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY warq-importado wep westab wnr-pl wdata-geracao wrellog wpasta 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-20 RECT-21 RECT-3 bt-elimina bt-consulta bt-importa 
         BUTTON-3 BUTTON-1 bt-sair BUTTON-2 warq-importado wep westab wnr-pl 
         wdata-geracao wrellog wpasta 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-plano C-Win 
PROCEDURE gera-plano :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEF BUFFER blo-matplano FOR lo-matplano.

    /* ROTINA PARA GERAR PLANO */
        /* VERIFICA COD. ESTAB SE ESTA CORRETO */
      IF  tt-dados.c-estab     = ?  THEN  DO:
          MESSAGE "ESTABELECIMENTO(CAMPO U) N�O PODE SER BRANCO" 
          SKIP(1) "ANALISE O ARQUIVO xls" "ESTAB.: " tt-dados.c-estab "VERIFIQUE LINHA: " v-int-line VIEW-AS ALERT-BOX.
          RETURN.
          END.
      IF tt-dados.c-estab       <> westab THEN DO: 
          MESSAGE "ESTABELECIMENTO PAR�METROS DA TELA: "  westab  SKIP(1) 
         "DIFERENTE DO ESTABELECIMENTO DO ARQUIVO: " tt-dados.c-estab.
          RETURN.
      END.
      FIND FIRST estabelec WHERE estabelec.cod-estabel = tt-dados.c-estab  NO-LOCK NO-ERROR.
      IF NOT AVAILABLE estabelec  THEN DO:
         MESSAGE "ESTABELECIMENTO: " tt-dados.c-estab  "N�O ENCONTRADO." VIEW-AS ALERT-BOX.
         RETURN.
         END.
        
    FOR EACH tt-dados BREAK BY c-cod-emitente BY c-it-codigo: 
   
      run pi-acompanhar in h-acomp ("Emitente: " + STRING(c-cod-emitente) 
                                    + " Item: " 
                                    + STRING(c-it-codigo)).                 
      FIND FIRST item-fornec WHERE item-fornec.cod-emitente = tt-dados.c-cod-emitente
                             AND   item-fornec.it-codigo    = tt-dados.c-it-codigo
                             AND   item-fornec.ativo        = yes
                             NO-LOCK NO-ERROR.
         IF NOT AVAILABLE(item-fornec) THEN DO:
           MESSAGE "Relacionamento Item/Fornecedor nao encontrado: " tt-dados.c-it-codigo " / " tt-dados.c-cod-emitente
                   SKIP "ESTE ITEM N�O SER� IMPORTADO!"
           VIEW-AS ALERT-BOX.
           PAUSE. 
           NEXT.
           END.
      FIND FIRST ITEM WHERE item.it-codigo = tt-dados.c-it-codigo
                      AND   ITEM.cod-obsoleto <> 1 NO-LOCK no-error.
      IF AVAILABLE ITEM THEN DO:
        MESSAGE "Item:" tt-dados.c-it-codigo ", N�O EST� COM SITUACAO ATIVA NO CADASTRO." 
                 skip
                 "ITEM N�O SER� IMPORTADO!" VIEW-AS ALERT-BOX.
        NEXT.
        END.

      IF FIRST-OF(c-cod-emitente) THEN  ASSIGN wseq = 0.
   
      /* procura e se o plano nao existe */
      FIND FIRST lo-matplano WHERE lo-matplano.cod-emitente =  tt-dados.c-cod-emitente
                            AND   lo-matplano.emp          = wep
                            AND   lo-matplano.estab        = westab
                            AND   lo-matplano.nr-pl        = wnr-pl 
                            AND   lo-matplano.it-codigo    = tt-dados.c-it-codigo
                            EXCLUSIVE-LOCK NO-ERROR.
   
    IF AVAILABLE lo-matplano THEN DO:
     MESSAGE "Emp: " emp " Estab.: " estab tt-dados.c-estab "Plano: " wnr-pl 
              SKIP "Fornecedor:" lo-matplano.cod-emitente "Item:" lo-matplano.it-codigo "j� cadastrado! "
              SKIP "DESEJA SOBREPOR?"
              VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wgera-novo.
     IF wgera-novo = NO THEN NEXT.
     END.
    
     IF NOT AVAILABLE lo-matplano THEN do:
         CREATE lo-matplano.

         FIND LAST blo-matplano NO-LOCK
             WHERE blo-matplano.cod-emitente = tt-dados.c-cod-emitente
               AND blo-matplano.emp          = wep
               AND blo-matplano.nr-pl        = wnr-pl
               AND blo-matplano.estab        = westab NO-ERROR.

         IF AVAIL blo-matplano THEN wseq = blo-matplano.seq-it + 1.

         ASSIGN
             lo-matplano.cod-emitente = tt-dados.c-cod-emitente
             lo-matplano.emp          = wep                    
             lo-matplano.nr-pl        = wnr-pl                 
             lo-matplano.estab        = westab 
             lo-matplano.seq-it       = wseq.

     END.

     IF  tt-dados.c-qt-sem1 = ? THEN tt-dados.c-qt-sem1  = 0.
     IF  tt-dados.c-qt-sem2 = ? THEN tt-dados.c-qt-sem2  = 0.
     IF  tt-dados.c-qt-sem3 = ? THEN tt-dados.c-qt-sem3  = 0.
     IF  tt-dados.c-qt-sem4 = ? THEN  tt-dados.c-qt-sem4 = 0.
     IF  tt-dados.c-qt-sem5 = ?  THEN  tt-dados.c-qt-sem5 = 0.
     IF  tt-dados.c-qt-sem1-2 = ? THEN tt-dados.c-qt-sem1-2 = 0.
     IF  tt-dados.c-qt-sem2-2 = ? THEN tt-dados.c-qt-sem2-2 = 0.
     IF  tt-dados.c-qt-sem3-2 = ? THEN tt-dados.c-qt-sem3-2 = 0.
     IF  tt-dados.c-qt-sem4-2 = ? THEN tt-dados.c-qt-sem4-2 = 0.
     IF  tt-dados.c-qt-sem5-2 = ? THEN tt-dados.c-qt-sem5-2 = 0.
     IF  tt-dados.c-mes1      = ? THEN tt-dados.c-mes1 = 0.
     IF  tt-dados.c-mes2      = ? THEN tt-dados.c-mes2 = 0.
     IF  tt-dados.c-estab     = ? THEN MESSAGE "ESTABELECIMENTO(CAMPO U) N�O PODE SER BRANCO" SKIP "ANALISE O ARQUIVO XMLS" VIEW-AS ALERT-BOX.
    
     ASSIGN    lo-matplano.it-codigo     = tt-dados.c-it-codigo
               lo-matplano.data-geracao  = TODAY
               lo-matplano.qt-sem1       = tt-dados.c-qt-sem1
               lo-matplano.qt-sem2       = tt-dados.c-qt-sem2
               lo-matplano.qt-sem3       = tt-dados.c-qt-sem3
               lo-matplano.qt-sem4       = tt-dados.c-qt-sem4
               lo-matplano.qt-sem5       = tt-dados.c-qt-sem5
               lo-matplano.qt-sem1-2     = tt-dados.c-qt-sem1-2
               lo-matplano.qt-sem2-2     = tt-dados.c-qt-sem2-2
               lo-matplano.qt-sem3-2     = tt-dados.c-qt-sem3-2
               lo-matplano.qt-sem4-2     = tt-dados.c-qt-sem4-2
               lo-matplano.qt-sem5-2     = tt-dados.c-qt-sem5-2
               lo-matplano.mes1          = tt-dados.c-mes1
               lo-matplano.mes2          = tt-dados.c-mes2.
        
      run proc_nota.
     END.

   /* gera rel. para conferencia */
   OUTPUT TO VALUE(wrellog) PAGED.
     DISPLAY "** Nome do arquivo de importado              : "  warq-importado AT 53
             SKIP 
             "** Nome do arquivo de consistencia           : "  wrellog AT 53
             SKIP
             "** Nome do arquivo espelho da planilha excel : " c-arquivo-dados-plano AT 53
         WITH NO-LABEL WIDTH 150.
     FOR EACH lo-matplano 
       WHERE emp            = wep
       AND   estab          = westab
       AND   nr-pl          = wnr-pl 
       AND   data-geracao   = wdata-geracao NO-LOCK:
       DISPLAY
               lo-matplano.emp          
               lo-matplano.estab        
               lo-matplano.nr-pl  
               lo-matplano.data-geracao 
               lo-matplano.cod-emitente 
               lo-matplano.seq-it  
               lo-matplano.it-codigo 
               nro-docto
               data-nota
               lo-matplano.qt-sem1      
               lo-matplano.qt-sem2       
               lo-matplano.qt-sem3       
               lo-matplano.qt-sem4      
               lo-matplano.qt-sem5       
               lo-matplano.qt-sem1-2     
               lo-matplano.qt-sem2-2     
               lo-matplano.qt-sem3-2    
               lo-matplano.qt-sem4-2    
               lo-matplano.qt-sem5-2  
               lo-matplano.mes1         
               lo-matplano.mes2
           WITH WIDTH 300 TITLE "SITUACAO DO PLANO ATUAL " + "EMP: " + STRING(wep) + "/Estab.: " + STRING(westab) + "/Plano: " +  STRING(wnr-pl) + "/Data: " + STRING(wdata-geracao).
     
     END.
    OUTPUT CLOSE.
  
/*     MESSAGE "FIM DA IMPORTA��O." VIEW-AS ALERT-BOX. */
    ENABLE ALL WITH FRAME  {&FRAME-NAME}.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN warquivo = wrellog.
  get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
  if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
  end.
  run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE importa-plano C-Win 
PROCEDURE importa-plano :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR c-arquivo-log AS CHAR NO-UNDO.
/* Pasta selecionada para importa��o */
/*assign i-pasta-select = 1.  qual pasta est� o conteudo  */
FOR EACH tt-dados:
  DELETE tt-dados.
  END.
assign i-pasta-select = wpasta. /* qual pasta est� o conteudo */

{office/office.i excel v-chr-excel-application} 
v-chr-excel-application:visible = false. 

IF SEARCH(procname) = ? THEN RETURN.

assign v-chr-work-book = v-chr-excel-application:workbooks:open(procname) 
v-chr-work-sheet = v-chr-excel-application:sheets:item(int(i-pasta-select)) .
// N�o funciona no libreOffice
//v-int-ultimalinha = v-chr-excel-application:activesheet:usedrange:rows:count. 

&SCOPED-DEFINE LIBRE-CALC-MAX_ROWS 1048576 
// Fonte: https://ask.libreoffice.org/t/limites-de-linhas-e-colunas-no-calc/25712

do v-int-line = 5 to {&LIBRE-CALC-MAX_ROWS}: 

 /* exibem linha da planilha */
 run pi-acompanhar in h-acomp ("Linha da planilha: " + STRING(v-int-line)). 

v-cod-emitente = (v-chr-work-sheet:range('A':U + string(v-int-line)):integerVALUE).

IF v-cod-emitente = 0 OR v-cod-emitente = ? THEN LEAVE.

assign 

v-it-codigo = v-chr-work-sheet:range('B':U + string(v-int-line)):VALUE
v-descricao = v-chr-work-sheet:range('C':U + string(v-int-line)):VALUE 
v-chr-campo-4 = (v-chr-work-sheet:range('D':U + string(v-int-line)):integerVALUE)   
v-chr-campo-5 = (v-chr-work-sheet:range('E':U + string(v-int-line)):integerVALUE) 
v-chr-campo-6 = (v-chr-work-sheet:range('F':U + string(v-int-line)):integerVALUE) 
v-chr-campo-7 = (v-chr-work-sheet:range('G':U + string(v-int-line)):integerVALUE) 
v-chr-campo-8 = (v-chr-work-sheet:range('H':U + string(v-int-line)):integerVALUE) 
v-qt-sem1 = (v-chr-work-sheet:range('I':U + string(v-int-line)):integerVALUE) 
v-qt-sem2 = (v-chr-work-sheet:range('J':U + string(v-int-line)):integerVALUE) 
v-qt-sem3 = (v-chr-work-sheet:range('K':U + string(v-int-line)):integerVALUE) 
v-qt-sem4 = (v-chr-work-sheet:range('L':U + string(v-int-line)):integerVALUE) 
v-qt-sem5 = (v-chr-work-sheet:range('M':U + string(v-int-line)):integerVALUE) 
v-qt-sem1-2 = (v-chr-work-sheet:range('N':U + string(v-int-line)):integerVALUE)
v-qt-sem2-2 = (v-chr-work-sheet:range('O':U + string(v-int-line)):integerVALUE) 
v-qt-sem3-2 = (v-chr-work-sheet:range('P':U + string(v-int-line)):integerVALUE) 
v-qt-sem4-2 = (v-chr-work-sheet:range('Q':U + string(v-int-line)):integerVALUE) 
v-qt-sem5-2 = (v-chr-work-sheet:range('R':U + string(v-int-line)):integerVALUE) 
v-mes1 = (v-chr-work-sheet:range('S':U + string(v-int-line)):integerVALUE) 
v-mes2 = (v-chr-work-sheet:range('T':U + string(v-int-line)):integerVALUE)
v-estab = (v-chr-work-sheet:range('U':U + string(v-int-line)):integerVALUE).

create tt-dados. 
assign 
tt-dados.c-cod-emitente = int(v-cod-emitente)    /* cod-emitente */
tt-dados.c-it-codigo    = (v-it-codigo)       /* cod-item */
tt-dados.c-descricao    = (v-descricao)       /* descricao */
tt-dados.campo-4        = INT(v-chr-campo-4)    
tt-dados.campo-5        = INT(v-chr-campo-5) 
tt-dados.campo-6        = INT(v-chr-campo-6) 
tt-dados.campo-7        = INT(v-chr-campo-7)
tt-dados.campo-8        = INT(v-chr-campo-8) 
tt-dados.c-qt-sem1      = INT(v-qt-sem1)   /* qt-sem1*/
tt-dados.c-qt-sem2      = INT(v-qt-sem2)  /* qt-sem]2 */
tt-dados.c-qt-sem3      = INT(v-qt-sem3)  /* qt-sem3 */
tt-dados.c-qt-sem4      = INT(v-qt-sem4)  /* qt-sem4 */
tt-dados.c-qt-sem5      = INT(v-qt-sem5)   /* qt-sem5 */
tt-dados.c-qt-sem1-2    = INT(v-qt-sem1-2)  /* qt-sem1-2 */
tt-dados.c-qt-sem2-2    = INT(v-qt-sem2-2)  /* qt-sem2-2 */
tt-dados.c-qt-sem3-2    = INT(v-qt-sem3-2)  /* qt-sem3-2 */
tt-dados.c-qt-sem4-2    = INT(v-qt-sem4-2)  /* qt-sem4-2 */
tt-dados.c-qt-sem5-2    = INT(v-qt-sem5-2)  /* qt-sem5-2 */
tt-dados.c-mes1         = INT(v-mes1)  /* mes1 */
tt-dados.c-mes2         = INT(v-mes2) /* mes2 */
tt-dados.c-estab        = STRING(v-estab).  /* estab*/

/* Aqui est� limitado � 3 campos, por�m pode-se criar de acordo com a necessidade */ 


end. 

v-chr-work-book:CLOSE().

v-chr-excel-application:quit(). 

DELETE object v-chr-excel-application no-error. 
DELETE object v-chr-work-sheet no-error. 
DELETE object v-chr-work-book no-error. 

IF CAN-FIND(FIRST tt-dados) THEN DO:
    RUN cria-tit.
    RUN gera-plano.
END.

/* Aqui pode-se fazer o que quiser com os dados importados para a temp-table */ 


OUTPUT TO value(c-arquivo-dados-plano) paged. 
for each tt-dados BREAK BY v-cod-emitente BY v-it-codigo:
 disp tt-dados WITH FRAME f-dados  TITLE "DADOS DA PLANILHA EXCEL "  WIDTH 300 DOWN.
 DOWN WITH FRAME f-dados.
end. 
OUTPUT CLOSE.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEF VAR c-arquivo-log AS CHAR NO-UNDO.

RUN pi-retorna-diretorio 
     (c-seg-usuario,
     OUTPUT c-arquivo-log).


 ASSIGN
               wep              = 1
               westab           = ""
               wgera-novo       = NO
               wseq             = 0
               wdata-geracao    = TODAY
               wnr-pl           = ""
               wrellog          = c-arquivo-log + "CONSIS-IMPORT" + STRING(TIME,"999999")+ ".TXT"
               warq-importado   = " "
               procname         = " "
               wpasta           = 1
               warq-importado   = "".
               
 DISPLAY  wep westab wdata-geracao wnr-pl wrellog warq-importado wpasta WITH FRAME {&FRAME-NAME}.
 ENABLE ALL except wdata-geracao warq-importado WITH FRAME     {&FRAME-NAME}.
 APPLY "ENTRY" TO wep IN FRAME    {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicio-prog C-Win 
PROCEDURE inicio-prog :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DISABLE wep westab wdata-geracao wnr-pl wrellog warq-importado wpasta WITH FRAME  {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc_nota C-Win 
PROCEDURE proc_nota :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN wnrult = ""
         wdtult = ?.
  FOR EACH recebimento WHERE recebimento.cod-emitente = 
                             lo-matplano.cod-emitente
                       AND   recebimento.it-codigo    = 
                             lo-matplano.it-codigo
                       AND   recebimento.cod-movto    = 1
                       NO-LOCK BREAK BY recebimento.data-nota
                       BY recebimento.numero-nota:
    IF  LAST-OF(recebimento.data-nota)  
    AND LAST-OF(recebimento.numero-nota) THEN DO:
      IF LAST(recebimento.data-nota) THEN 
        ASSIGN wdtult = recebimento.data-nota
               wnrult = recebimento.numero-nota.
        ASSIGN lo-matplano.data-nota = recebimento.data-nota 
             lo-matplano.nro-docto = integer(recebimento.numero-nota).
       END.
    END.
/* rotina especial - definida pela rosa para pegar nota de transferencia do material
   de sorocaba para a seeber - como nota de corte */
 
/* Inserido nat-operacao -  122A2 - conf. Fabio 24/01/17 - para evitar erros faturamento*/
IF lo-matplano.cod-emitente = 6423 OR lo-matplano.cod-emitente = 530 THEN DO:
  FIND LAST item-doc-est WHERE item-doc-est.cod-emitente =  lo-matplano.cod-emitente 
                         AND   item-doc-est.it-codigo = lo-matplano.it-codigo
                         AND   (item-doc-est.nat-operacao = "121A2"
                          OR   item-doc-est.nat-operacao = "122A2")
                         NO-LOCK NO-ERROR.
 
    FIND FIRST docum-est OF item-doc-est NO-LOCK NO-ERROR.
    IF AVAILABLE item-doc-est THEN  IF AVAILABLE docum-est  THEN
    ASSIGN wdtult = docum-est.dt-emissao
           wnrult = item-doc-est.nro-docto
           lo-matplano.nro-docto = INTEGER(item-doc-est.nro-docto)
           lo-matplano.data-nota = docum-est.dt-emissao.
    END.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE roda C-Win 
PROCEDURE roda :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  IF wnr-pl <> "" THEN DO:
      DISABLE ALL  WITH FRAME  {&FRAME-NAME}.
        RUN importa-plano.
      
        ENABLE ALL WITH FRAME  {&FRAME-NAME}.
     
      RUN inicio-prog.
    END.
   IF wnr-pl = "" THEN DO:
      MESSAGE "Plano n�o pode ser branco" VIEW-AS ALERT-BOX.
      RUN inicio-prog.
   END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

