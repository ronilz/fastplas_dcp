&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE c-rodape        AS CHAR FORMAT "X(110)".
/* Variaveis referente a Titulos */
DEFINE VARIABLE wpl-ant         LIKE lo-matplano.nr-pl.                    
DEFINE VARIABLE wmsg            AS CHAR FORMAT "X(40)".
DEFINE VARIABLE wmsg1           AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wlinha          AS CHAR FORMAT "X(138)".
DEFINE VARIABLE wlinha1         AS CHAR FORMAT "X(138)".
DEFINE VARIABLE wpag            AS INTE FORMAT "9999".

/* Workfile para guardar numero de planos solicitados */
DEFINE WORKFILE nroplano
  FIELD wcod-emitente           LIKE lo-matplano.cod-emitente 
  FIELD wnr-pl                  LIKE lo-matplano.nr-pl.

DEFINE VARIABLE wemp            LIKE estabelec.nome.
DEFINE VARIABLE wemit           LIKE emitente.nome-ab.
DEFINE VARIABLE wcod-emit       LIKE lo-matplano.cod-emitente.
DEFINE VARIABLE wconf-imp       AS LOGICAL FORMAT "Sim/Nao".

/* Buffer para pesquisa de mensagem de reprograma */
DEFINE BUFFER  b-matplano       FOR lo-matplano.
DEFINE VARIABLE wtipo           AS INTEGER NO-UNDO. /* tipo de impressora */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 RECT-5 wemi-ini wemi-fim wep westab ~
wnr-ini wrel w-imp bt-executar bt-sair-2 
&Scoped-Define DISPLAYED-OBJECTS wemi-ini wemi-fim wep westab wnr-ini wrel ~
w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 11 BY 1.13
     FONT 1.

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wemi-fim AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "Cod.Fornecedor final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE wemi-ini AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "Cod.Fornecedor inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(3)":U INITIAL "0" 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnr-ini AS CHARACTER FORMAT "999999X":U INITIAL "0" 
     LABEL "Numero Plano inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(23)":U 
     LABEL "Nome do Relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 1,
"Impressora", 2,
"Arquivo com compress�o", 3
     SIZE 55.29 BY 1.13 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 12.5
     FGCOLOR 8 .

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY 1.42
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wemi-ini AT ROW 3.75 COL 35 COLON-ALIGNED
     wemi-fim AT ROW 4.75 COL 35 COLON-ALIGNED
     wep AT ROW 5.75 COL 35 COLON-ALIGNED
     westab AT ROW 6.75 COL 35 COLON-ALIGNED
     wnr-ini AT ROW 7.75 COL 35 COLON-ALIGNED
     wrel AT ROW 8.75 COL 35 COLON-ALIGNED
     w-imp AT ROW 11 COL 69.29 RIGHT-ALIGNED NO-LABEL
     bt-executar AT ROW 14.5 COL 33
     bt-sair-2 AT ROW 14.5 COL 44
     RECT-3 AT ROW 1.5 COL 3
     RECT-5 AT ROW 14.29 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81 BY 15.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Plano de Entrega de Material"
         HEIGHT             = 15.17
         WIDTH              = 81.57
         MAX-HEIGHT         = 16
         MAX-WIDTH          = 81.57
         VIRTUAL-HEIGHT     = 16
         VIRTUAL-WIDTH      = 81.57
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = 15
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Plano de Entrega de Material */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Plano de Entrega de Material */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN w-imp.
  DISABLE ALL WITH FRAME  {&FRAME-NAME}.
  ASSIGN wemi-ini wemi-fim wep westab wnr-ini wrel.
  CASE w-imp:
   WHEN 1 THEN DO:
     OUTPUT TO VALUE(wrel) PAGE-SIZE 60.
     RUN imprimir. 
     END. 
     
     WHEN 2 THEN DO:
       /* impressora */
       SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
       IF wconf-imp = YES THEN DO:
         OUTPUT TO PRINTER PAGE-SIZE 44.
         {wpsf/imp.p}
         PUT CONTROL wcomprime wpaisagem a4. 
         RUN imprimir. 
         END.
       END.
      WHEN 3 THEN DO:
       RUN comprime-arquivo.
       END.
     END CASE. 
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo C-Win 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
 DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
 DEFINE VARIABLE a4 AS CHAR.
RUN  wpsf/wp9901.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    END.
/* HP */
IF wtipo = 2 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
OUTPUT TO value(wrel) page-size 44.
PUT CONTROL wcomprime wpaisagem a4. 
RUN imprimir.                     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wemi-ini wemi-fim wep westab wnr-ini wrel w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-3 RECT-5 wemi-ini wemi-fim wep westab wnr-ini wrel w-imp 
         bt-executar bt-sair-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IMPRIMIR C-Win 
PROCEDURE IMPRIMIR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FORM 
  wlinha1
  SKIP
  "Cod.Ite            NfCorte     ATRASO"
  lo-matdatas.titsem1 AT 42 
  lo-matdatas.titsem2 
  lo-matdatas.titsem3       
  lo-matdatas.titsem4 
  lo-matdatas.titsem5 
  SKIP
  "_________" AT 42
  "_________" AT 56
  "_________" AT 70
  "_________" AT 84
  "_________" AT 98

  SKIP
  lo-matplano.it-codigo "" 
  lo-matplano.nro-docto  
  lo-matplano.imediato  
  lo-matplano.qt-sem1  AT 42
  lo-matplano.qt-sem2  AT 56
  lo-matplano.qt-sem3  AT 70
  lo-matplano.qt-sem4  AT 84 
  lo-matplano.qt-sem5  AT 98
  SKIP
  item.descricao-1
  lo-matdatas.titsem1-2 AT 42 
  lo-matdatas.titsem2-2 
  lo-matdatas.titsem3-2       
  lo-matdatas.titsem4-2 
  lo-matdatas.titsem5-2       
  lo-matdatas.titmes1 
  lo-matdatas.titmes2
  item.descricao-2      AT 01
  /*"__________________ _______ _________"*/
  "_________" AT 42
  "_________" AT 56
  "_________" AT 70
  "_________" AT 84
  "_________" AT 98
  "_________" AT 112
  "_________" AT 126
  SKIP
  lo-matplano.qt-sem1-2 AT 42 
  lo-matplano.qt-sem2-2 AT 56
  lo-matplano.qt-sem3-2 AT 70
  lo-matplano.qt-sem4-2 AT 84 
  lo-matplano.qt-sem5-2 AT 98
  lo-matplano.mes1      AT 112
  lo-matplano.mes2      AT 126
  wlinha
  SKIP(1)
HEADER
  wemp 
  string(TODAY,"99/99/9999") 
  string(TIME,"HH:MM:SS")   
  "Pag.:" AT 126 PAGE-NUMBER FORMAT ">>99"
  SKIP
  "*** PLANO DE ENTREGA DE MATERIAIS NO."  
  "Empresa:" wep "Estabelec.:" westab wnr-ini FORMAT "99/9999X" "***"
  SKIP
  "Fornecedor:"         AT 20 wcod-emit wemit
  WITH FRAME f-plano NO-LABELS DOWN WIDTH 140.

/* Form Rodape ------------------------------------------------------------*/
FORM HEADER
  "Este Plano substitui o Plano de Entrega de Materiais"
  wpl-ant FORMAT "99/9999X"
  "referente aos itens acima."
  WITH NO-LABELS WIDTH 112 NO-BOX FRAME f-rod PAGE-BOTTOM.
/* imprimir */
ASSIGN wnr-ini = CAPS(wnr-ini).
  IF LENGTH(wnr-ini) > 6 THEN VIEW FRAME f-rod.
  FIND FIRST estabelec WHERE estabelec.ep-codigo   = string(wep)
                       AND   estabelec.cod-estabel = westab
                       NO-LOCK NO-ERROR.   
  ASSIGN wemp  = estabelec.nome.
  FOR EACH lo-matplano  WHERE lo-matplano.cod-emitente  GE wemi-ini      
                        AND   lo-matplano.cod-emitente  LE wemi-fim
                        AND   lo-matplano.emp           = wep
                        AND   lo-matplano.estab         = westab
                        AND   lo-matplano.nr-pl         = wnr-ini
                        NO-LOCK BREAK BY lo-matplano.cod-emitente
                        BY lo-matplano.it-codigo:
    IF FIRST-OF(lo-matplano.cod-emitente) THEN DO:
      PAGE.
      END.
    ASSIGN wpl-ant = " ".
    /* Verifica se e' reprograma - Ex. 031999A reprograma de 031999 */
    IF LENGTH(lo-matplano.nr-pl) > 6 THEN DO:
      FIND FIRST b-matplano WHERE b-matplano.cod-emitente  = 
                                  lo-matplano.cod-emitente
                            AND   b-matplano.emp           = 
                                  lo-matplano.emp
                            AND   b-matplano.estab         =
                                  lo-matplano.estab
                            AND   b-matplano.nr-pl         = 
                                  lo-matplano.nr-pl
                                  NO-LOCK NO-ERROR.
                        
      FIND PREV b-matplano  WHERE b-matplano.cod-emitente  = 
                                  lo-matplano.cod-emitente
                            AND   b-matplano.emp          = 
                                  lo-matplano.emp
                            AND   b-matplano.estab        =
                                  lo-matplano.estab
                            AND   b-matplano.nr-pl BEGINS
                                  (SUBSTRING(lo-matplano.nr-pl,1,6))
                            NO-LOCK NO-ERROR.
      IF AVAILABLE b-matplano THEN DO:
        ASSIGN wpl-ant = b-matplano.nr-pl.
        END.
      END.
        
        
    FIND FIRST emitente WHERE emitente.cod-emitente = lo-matplano.cod-emitente
                        NO-LOCK NO-ERROR.
    ASSIGN wemit     = emitente.nome-ab
           wcod-emit = lo-matplano.cod-emitente.
    FIND FIRST item    WHERE item.it-codigo = lo-matplano.it-codigo NO-LOCK
                       NO-ERROR.     
    FIND FIRST lo-matdatas WHERE lo-matdatas.emp   = lo-matplano.emp
                           AND   lo-matdatas.estab = lo-matplano.estab
                           AND   lo-matdatas.nr-pl =
                                 SUBSTRING(lo-matplano.nr-pl,1,6)
                                 NO-LOCK NO-ERROR.
    
    PUT SCREEN ROW 23 "Emitente: "  + STRING(lo-matplano.cod-emitente)
                    +  " Cod.Item.: " + STRING(lo-matplano.it-codigo).
    DISPLAY /* linha 1 */
            wlinha1
            lo-matdatas.titsem1 lo-matdatas.titsem2 
            lo-matdatas.titsem3 lo-matdatas.titsem4 
            lo-matdatas.titsem5
            lo-matplano.it-codigo lo-matplano.nro-docto 
            lo-matplano.imediato 
            lo-matplano.qt-sem1 lo-matplano.qt-sem2 lo-matplano.qt-sem3
            lo-matplano.qt-sem4 lo-matplano.qt-sem5 
            /* linha 2 */
            item.descricao-1 
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2 
            lo-matdatas.titsem5-2 lo-matdatas.titmes1 
            lo-matdatas.titmes2
            item.descricao-2
            lo-matplano.qt-sem1-2 lo-matplano.qt-sem2-2 lo-matplano.qt-sem3-2
            lo-matplano.qt-sem4-2 lo-matplano.qt-sem5-2 lo-matplano.mes1
            lo-matplano.mes2 
            wlinha    
            WITH FRAME f-plano OVERLAY NO-LABELS DOWN.
    DOWN WITH FRAME f-plano.
    IF LAST-OF(lo-matplano.cod-emitente) THEN DO:
    IF LINE-COUNTER > 57 THEN PAGE. 
    DISPLAY 
    SKIP(1)
    "Se em 02 dias nao houver manifestacao por parte do Fornecedor,"  AT 25
    SKIP
    "sera considerado totalmente aceito o Plano de entrega de Materiais." AT 23
    SKIP(3)
       "___________________________"    AT 23
       SPACE(12) 
       "____________________________"
  SKIP "   Analisado e Aprovado    "    AT 23
       SPACE(12) 
       "      P.C.P. / Logistica"
  SKIP " Departamento Comercial"       AT 24 WITH FRAME f-mens WIDTH 110.
    END.
    END.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  ASSIGN wlinha       = FILL("_",138)
         wlinha1      = FILL("_",138).
  ASSIGN wemi-ini     = 0
         wemi-fim     = 9999999
         wep          = 1
         westab       = "1"
         wnr-ini      = ""
         wrel         = "V:\spool\PLANO.TXT".
  DISPLAY wemi-ini wemi-fim wep westab wnr-ini wrel 
          WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

