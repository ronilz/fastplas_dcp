&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE wrelatorio AS CHAR FORMAT "x(30)".
DEFINE VARIABLE wconf      AS LOGICAL FORMAT "Sim/N�o" INITIAL NO.
/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-10 wnr-ini wnr-fim wcodigo-orig ~
bt-lista bt-val bt-sair BUTTON-1 
&Scoped-Define DISPLAYED-OBJECTS wnr-ini wnr-fim wcodigo-orig 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-lista 
     LABEL "Lista" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-sair DEFAULT 
     LABEL "Sair" 
     SIZE 10 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-val 
     LABEL "Altera origem" 
     SIZE 10 BY 1.

DEFINE BUTTON BUTTON-1 
     LABEL "Ajuda" 
     SIZE 8 BY 1.

DEFINE VARIABLE wcodigo-orig AS INTEGER FORMAT ">9":U INITIAL 0 
     LABEL "NOVO C�DIGO DE ORIGEM" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .79
     BGCOLOR 14 FONT 0 NO-UNDO.

DEFINE VARIABLE wnr-fim AS CHARACTER FORMAT "X(16)":U INITIAL "0" 
     LABEL "Item final" 
     VIEW-AS FILL-IN 
     SIZE 28 BY .88 NO-UNDO.

DEFINE VARIABLE wnr-ini AS CHARACTER FORMAT "X(16)":U INITIAL "0" 
     LABEL "Item inicial" 
     VIEW-AS FILL-IN 
     SIZE 28 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 12.25.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 77 BY 1.42
     BGCOLOR 7 FGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wnr-ini AT ROW 3.5 COL 30 COLON-ALIGNED
     wnr-fim AT ROW 5 COL 30 COLON-ALIGNED
     wcodigo-orig AT ROW 7 COL 41 COLON-ALIGNED WIDGET-ID 6
     bt-lista AT ROW 14 COL 9
     bt-val AT ROW 14 COL 22
     bt-sair AT ROW 14 COL 57
     BUTTON-1 AT ROW 14 COL 68 WIDGET-ID 10
     "o campo origem conforme acima ser� alterado." VIEW-AS TEXT
          SIZE 53 BY .54 AT ROW 10.75 COL 19 WIDGET-ID 8
          FONT 0
     "Para a faixa de itens acima selecionada" VIEW-AS TEXT
          SIZE 46 BY .54 AT ROW 9.5 COL 20 WIDGET-ID 4
          FONT 0
     RECT-1 AT ROW 1 COL 2
     RECT-10 AT ROW 13.75 COL 2 WIDGET-ID 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2 ROW 1.25
         SIZE 78 BY 14.71
         FONT 1
         DEFAULT-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Altera o campo origem Item p/ Faturamento- wpft017"
         HEIGHT             = 15.08
         WIDTH              = 80
         MAX-HEIGHT         = 42.42
         MAX-WIDTH          = 274.29
         VIRTUAL-HEIGHT     = 42.42
         VIRTUAL-WIDTH      = 274.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Altera o campo origem Item p/ Faturamento- wpft017 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Altera o campo origem Item p/ Faturamento- wpft017 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-lista
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-lista C-Win
ON CHOOSE OF bt-lista IN FRAME DEFAULT-FRAME /* Lista */
DO:
  ASSIGN wnr-ini wnr-fim.
  DISABLE bt-val bt-sair  bt-lista WITH FRAME {&FRAME-NAME}.
  ASSIGN wrelatorio = "V:\spool\LISTA-ITEM-ORIGEM.TXT".
  OUTPUT TO VALUE(wrelatorio) PAGED convert target "iso8859-1".
  FOR EACH  ITEM WHERE it-codigo GE  wnr-ini 
                 AND   it-codigo LE  wnr-fim    NO-LOCK:
     DISPLAY it-codigo  desc-item  codigo-orig VIEW-AS TEXT  WITH WIDTH 120
            TITLE "LISTAGEM ITEM/ C�DIGO ORIGEM -      P�GINA: " + STRING(PAGE-NUMBER) STREAM-IO.
     END.

  OUTPUT CLOSE.
  RUN gera-tela.
  APPLY "ENTRY" TO wnr-ini.
  ENABLE bt-val bt-lista bt-sair WITH FRAME     {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-val
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-val C-Win
ON CHOOSE OF bt-val IN FRAME DEFAULT-FRAME /* Altera origem */
DO:
  ASSIGN wnr-ini wnr-fim wcodigo-orig.
  IF wnr-ini = " "
  AND  wnr-fim = " " THEN do:
      retry.
      MESSAGE "C�DIGO N�O PODE SER BRANCO" VIEW-AS ALERT-BOX.
      END.
  DISABLE bt-val bt-sair  bt-lista WITH FRAME {&FRAME-NAME}.
  ASSIGN wrelatorio = "V:\spool\REL-ORIGEM-ALTERADO.TXT".
  OUTPUT TO VALUE(wrelatorio) PAGED convert target "iso8859-1".
  MESSAGE "CONFIRMA ALTERA��O DA FAIXA DE ITENS: " wnr-ini " a " wnr-fim "PARA CODIGO DE ORIGEM:  "  wcodigo-orig "?"
           VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wconf.

  IF wconf = YES THEN  DO:
    FOR EACH  ITEM WHERE it-codigo GE  wnr-ini 
                   AND   it-codigo LE  wnr-fim:
      
      /* ALTERA  */
      ASSIGN codigo-orig = wcodigo-orig.
     
      DISPLAY it-codigo  desc-item  codigo-orig VIEW-AS TEXT  WITH WIDTH 120
            TITLE "*** ALTERA��O C�DIGO ORIGEM NO CADASTRO DE ITEM ***        P�GINA: " + STRING(PAGE-NUMBER) STREAM-IO.
      END.
    END.
 IF wconf = NO then MESSAGE "ALTERA��O CANCELADA" VIEW-AS ALERT-BOX.
 ENABLE  bt-val bt-sair  bt-lista WITH FRAME {&FRAME-NAME}.
 APPLY "ENTRY" TO wnr-ini.
 OUTPUT CLOSE.
 MESSAGE "RELAT�RIO GERADO COM O NOME: "   wrelatorio VIEW-AS ALERT-BOX.
 RUN gera-tela.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Ajuda */
DO:
  MESSAGE
      "ESTE PROGRAMA ALTERA O CAMPO ORIGEM (codigo-orig) NA TABELA item."
      SKIP
      "PODE SER CONSULTADO NA TELA DE ITENS PARA FATURAMENTO, NA ABA INFORM. COMPLEMENTAR (CD0903)."
      VIEW-AS ALERT-BOX.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
    RUN INICIA.
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wnr-ini wnr-fim wcodigo-orig 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-10 wnr-ini wnr-fim wcodigo-orig bt-lista bt-val bt-sair 
         BUTTON-1 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN warquivo = wrelatorio.

get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
end.
    
run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN 
    wnr-ini = " "
    wnr-fim = " " 
    wcodigo-orig  = 0.
DISPLAY wnr-ini wnr-fim wcodigo-orig WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

