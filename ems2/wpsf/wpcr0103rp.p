&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i WPCR0103RP 0.00.04.001}
/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    field c-cod_empresa-ini like movfin.tit_acr.cod_empresa
    field c-cod_empresa-fim like movfin.tit_acr.cod_empresa
    field c-cod_estab-ini like movfin.tit_acr.cod_estab
    field c-cod_estab-fim like movfin.tit_acr.cod_estab
    field i-cdn_cliente-ini like movfin.tit_acr.cdn_cliente
    field i-cdn_cliente-fim like movfin.tit_acr.cdn_cliente
    field da-dat_emis_docto-ini like movfin.tit_acr.dat_emis_docto
    field da-dat_emis_docto-fim like movfin.tit_acr.dat_emis_docto
    field da-dat_emis_docto-ini1 like movfin.tit_acr.dat_emis_docto
    field da-dat_emis_docto-fim1 like movfin.tit_acr.dat_emis_docto.

/* defini��o valeria */
DEFINE VARIABLE whistorico AS CHAR FORMAT "X(150)" COLUMN-LABEL "Hist�rico".
DEFINE VARIABLE wnat       LIKE nota-fiscal.nat-oper.


define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.

form
    /*campos-do-relatorio*/
     with no-box width 132 down stream-io frame f-relat.

/* VARIAVEIS BENE */

/* FIM VARIAVEIS BENE*/

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

find empresa /*
    where empresa.ep-codigo = v_cdn_empres_usuar*/
    no-lock no-error.
find first param-global no-lock no-error.

/*{utp/ut-liter.i titulo_sistema * }*/
{utp/ut-liter.i "EMS204 - PLANEJAMENTO"  }
assign c-sistema = return-value.
{utp/ut-liter.i titulo_relatorio * } 

ASSIGN c-titulo-relat = "Relat�rio T�tulos Abertos Contas a Receber".
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "Gerando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
      
    RUN roda_prog.
   /*
        run pi-acompanhar in h-acomp (input "xxxxxxxxxxxxxx":U).
     */
    
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME





/* PROCEDURE roda_prog */
PROCEDURE roda_prog.
for each movfin.tit_acr NO-LOCK
         where movfin.tit_acr.cdn_cliente >= tt-param.i-cdn_cliente-ini and 
               movfin.tit_acr.cdn_cliente <= tt-param.i-cdn_cliente-fim and
               movfin.tit_acr.cod_empresa >= tt-param.c-cod_empresa-ini and 
               movfin.tit_acr.cod_empresa <= tt-param.c-cod_empresa-fim and
               movfin.tit_acr.cod_estab >= tt-param.c-cod_estab-ini and 
               movfin.tit_acr.cod_estab <= tt-param.c-cod_estab-fim and
               movfin.tit_acr.dat_vencto_tit_acr >= tt-param.da-dat_emis_docto-ini and 
               movfin.tit_acr.dat_vencto_tit_acr <= tt-param.da-dat_emis_docto-fim AND
               movfin.tit_acr.dat_emis_docto >= tt-param.da-dat_emis_docto-ini1 and 
               movfin.tit_acr.dat_emis_docto <= tt-param.da-dat_emis_docto-fim1 AND
               movfin.tit_acr.val_sdo_tit_acr <> 0 AND 
               movfin.tit_acr.LOG_tit_acr_estordo = NO
    BREAK by cdn_cliente BY  dat_vencto_tit_acr:

            /*   movfin.tit_acr.dat_emis_docto >= da-dat_emis_docto-ini and 
               movfin.tit_acr.dat_emis_docto <= da-dat_emis_docto-fim,*/
    FOR each movfin.histor_movto_tit_acr no-lock
         where movfin.histor_movto_tit_acr.cod_estab = movfin.tit_acr.cod_estab and
               movfin.histor_movto_tit_acr.num_id_tit_acr = movfin.tit_acr.num_id_tit_acr:
       IF  ind_orig_histor_acr = "Usu�rio" THEN ASSIGN whistorico = substring(string(des_text_histor,"x(150)"),1,150). 
       END.

    /* natureza de operacao */
    FIND FIRST nota-fiscal WHERE nr-nota-fis = movfin.tit_acr.cod_tit_acr 
                           AND dt-emis-nota =   movfin.tit_acr.dat_emis_docto 
                           AND nota-fiscal.cod-emitente = movfin.tit_acr.cdn_cliente NO-LOCK NO-ERROR.
 
    IF AVAILABLE nota-fiscal THEN ASSIGN wnat = nota-fiscal.nat-operacao.
        
    /*  MESSAGE whistorico VIEW-AS ALERT-BOX. */
   
    run pi-acompanhar in h-acomp("Vencimento:" + STRING(tit_acr.dat_vencto_tit_ac)).

    /***  C�DIGO PARA SA�DA EM 132 COLUNAS ***/
 

        view   frame f-cabec.
        view   frame f-rodape.
      
        display movfin.tit_acr.cod_empresa
            movfin.tit_acr.cod_estab
            movfin.tit_acr.cod_tit_acr
            wnat
            movfin.tit_acr.cod_portador
            movfin.tit_acr.cdn_cliente
            movfin.tit_acr.nom_abrev
            movfin.tit_acr.cod_espec_docto
            movfin.tit_acr.cod_ser_docto
            movfin.tit_acr.dat_emis_docto
            movfin.tit_acr.dat_vencto_tit_acr 
            movfin.tit_acr.val_origin_tit_acr
            movfin.tit_acr.dat_transacao
            movfin.tit_acr.val_sdo_tit_acr 
/* movfin.histor_movto_tit_acr.num_seq_histor_movto_acr */
            whistorico    FORMAT "x(100)"
            with  frame f-relat DOWN WITH WIDTH 300.
        ASSIGN whistorico = ""
               wnat = " " .            
        
       
      /*  for each tt-editor:
                delete tt-editor.
            end.
            run pi-print-editor (input movfin.histor_movto_tit_acr.des_text_histor,
                                 input 100).
            for each tt-editor:
                if  tt-editor.linha = 1 then do:
                        disp stream str-rp tt-editor.conteudo @ movfin.histor_movto_tit_acr.des_text_histor with stream-io frame f-relat-09-132.
                    down stream str-rp with frame f-relat-09-132.
                end.
                else
                    put stream str-rp unformatted tt-editor.conteudo at 001.
            end.                                         */ 
            down with frame f-relat.
 
       end.
 
END PROCEDURE.
