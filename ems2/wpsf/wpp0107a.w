&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wlinha              AS CHARACTER FORMAT "X(65)".
DEFINE VARIABLE wcont               AS INTEGER FORMAT "999".
DEFINE VARIABLE wfornecedor         AS CHAR FORMAT "X(38)".
DEFINE VARIABLE wendereco           AS CHAR FORMAT "X(36)".
DEFINE VARIABLE wcgc                AS CHAR FORMAT "X(19)".
DEFINE VARIABLE wcontato            AS CHAR FORMAT "X(33)".
DEFINE VARIABLE wpreco              LIKE ordem-compra.pre�o-fornec.
DEFINE VARIABLE waliq-ipi           LIKE ordem-compra.aliquota-ipi.
DEFINE VARIABLE waliq-icm           LIKE ordem-compra.aliquota-icm.
DEFINE VARIABLE woutro-imposto      LIKE ordem-compra.aliquota-icm.
DEFINE VARIABLE wpag1               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag2               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag3               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag4               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag5               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag6               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag7               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag8               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag9               AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag10              AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag11              AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpag12              AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wpedfonte           AS CHAR FORMAT "x(30)".
DEFINE VARIABLE wextenso            AS CHAR FORMAT "X(50)".
DEFINE VARIABLE wrel                AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wimposto            AS INTE FORMAT "99".
DEFINE VARIABLE wdesc-imposto       AS CHAR FORMAT "x(30)".


/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wpedido bt-codigo BT-REL bt-zoom bt-sair-2 ~
wrelatorio RECT-10 RECT-11 
&Scoped-Define DISPLAYED-OBJECTS wpedido wrelatorio 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-codigo 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Pedido" 
     SIZE 4 BY .79 TOOLTIP "V� para o pedido"
     FONT 1.

DEFINE BUTTON bt-inc 
     IMAGE-UP FILE "image/im-new.bmp":U
     LABEL "Inclui Item" 
     SIZE 3.86 BY 1.13 TOOLTIP "Inclui Item".

DEFINE BUTTON BT-REL 
     IMAGE-UP FILE "image/im-cq.bmp":U
     LABEL "Gera��o de Arquivo" 
     SIZE 4.14 BY 1.13 TOOLTIP "Gera arquivo com pedido".

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 4 BY 1.13 TOOLTIP "Sair"
     BGCOLOR 8 .

DEFINE BUTTON bt-zoom 
     IMAGE-UP FILE "image/im-zoom.bmp":U
     LABEL "Button 1" 
     SIZE 5 BY 1.13 TOOLTIP "Verifica arquivo na tela".

DEFINE VARIABLE wpedido AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N�mero do Pedido" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79
     FONT 1 NO-UNDO.

DEFINE VARIABLE wrelatorio AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome do Relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 25 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 62 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-11
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 62 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bt-inc AT ROW 2.25 COL 56
     wpedido AT ROW 3.25 COL 19 COLON-ALIGNED
     bt-codigo AT ROW 3.25 COL 30
     BT-REL AT ROW 4 COL 46
     bt-zoom AT ROW 4 COL 50
     bt-sair-2 AT ROW 4 COL 55
     wrelatorio AT ROW 4.25 COL 19 COLON-ALIGNED
     RECT-10 AT ROW 1.25 COL 2
     RECT-11 AT ROW 6 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 64.43 BY 6.25
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Arquivo - Pedido Ferramental - wpp0107a"
         HEIGHT             = 6.25
         WIDTH              = 64.43
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* SETTINGS FOR BUTTON bt-inc IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bt-inc:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Arquivo - Pedido Ferramental - wpp0107a */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Arquivo - Pedido Ferramental - wpp0107a */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-codigo C-Win
ON CHOOSE OF bt-codigo IN FRAME DEFAULT-FRAME /* Pedido */
DO:
  ASSIGN wpedido.
         wrelatorio = "V:\spool\ped" + STRING(wpedido) + ".TXT".
         DISPLAY wrelatorio WITH FRAME  {&FRAME-NAME}.
         ENABLE wrelatorio WITH FRAME   {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-inc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-inc C-Win
ON CHOOSE OF bt-inc IN FRAME DEFAULT-FRAME /* Inclui Item */
DO:
  RUN limpa.
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BT-REL
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BT-REL C-Win
ON CHOOSE OF BT-REL IN FRAME DEFAULT-FRAME /* Gera��o de Arquivo */
DO:
    ASSIGN  wrelatorio.
    /* procura pedido NO EMS */
    FOR EACH pedido-compr WHERE num-pedido = wpedido NO-LOCK:
        FIND first mgadm.emitente OF pedido-compr NO-LOCK.
        /* Dados do emitente */
        ASSIGN wfornecedor = emitente.nome-emit
                   wendereco   = emitente.endereco + ", " + emitente.estado 
                   wcgc        = emitente.cgc
                   wcontato    = contato[1].

        FOR EACH ordem-compra WHERE  ordem-compra.cod-emitente 
                                   = pedido-compr.cod-emitente
                              AND    ordem-compra.num-pedido = 
                                     pedido-compr.num-pedido NO-LOCK:

            ASSIGN wpreco    = ordem-compra.preco-fornec
                   waliq-ipi = aliquota-ipi 
                   waliq-icm = aliquota-icm
                   wpedido   = pedido-compr.num-pedido. 
           FIND FIRST mgfas.lo-pedferra WHERE mgfas.lo-ped.b-pedido
                                      = wpedido NO-LOCK NO-ERROR. 
           IF NOT AVAILABLE mgfas.lo-pedferra   THEN DO:
             MESSAGE "N�O EXISTEM DETALHES DO PEDIDO FERRAMENTAL!"
                     VIEW-AS ALERT-BOX.
             NEXT.
             END.
           ASSIGN wcontato = lo-pedferra.b-contato
                  wpag1 = b-pag1
                  wpag2 = b-pag2
                  wpag3 = b-pag3
                  wpag4 = b-pag4
                  wpag5 = b-pag5
                  wpag6 = b-pag6
                  wpag7 = b-pag7
                  wpag8 = b-pag8
                  wpag9 = b-pag9
                  wpag10 = b-pag10
                  wpag11 = b-pag11
                  wpag12 = b-pag12
                  wextenso = b-extenso
                  wimposto = b-imposto. /*
            IF wimposto = 1 THEN ASSIGN wdesc-imposto = "est�o inclu�do impostos.".
            IF wimposto = 2 THEN ASSIGN wdesc-imposto = "n�o est�o inclu�do impostos.".*/
           END.
       
    END.
/* Gera��o do pedido de compra e Venda de Ferramental */
OUTPUT TO VALUE(wrelatorio). 
ASSIGN wrel = " wpsf/pedfonte.ans".

INPUT FROM VALUE(wrel) NO-ECHO.

REPEAT WITH FRAME f-wreg WIDTH 67:
  IMPORT UNFORMATTED wlinha.
  ASSIGN wcont = wcont + 1.
  /* LINHA 7 - N�MERO DO PEDIDO */
  IF wcont = 7 
  THEN ASSIGN wlinha = "                      No. " + STRING(wpedido,">>>>>,>>9").
  /* LINHA 47 - representante fornecedor*/
  IF wcont = 87 
  THEN ASSIGN wlinha = "   PEDIDO DE COMPRA E VENDA DE FERRAMENTAL No."
                     + STRING(wpedido,">>>>>,>>9").
  /* INSER��O DOS CAMPOS DO PEDIDO */

  /* LINHA 96 - NOME DO FORNECEDOR */
  IF wcont = 96
  THEN ASSIGN wlinha = wfornecedor                                      
                     + "                 com      sede       na".
  /* LINHA 97 - endere�o fornecedor */
  IF wcont = 97 
  THEN ASSIGN wlinha = wendereco
                     + " "
                     + SUBSTRING(wlinha,37,61).
  /* LINHA 98 - cgc do fornecedor */
  IF wcont = 98
  THEN ASSIGN wlinha = "o  n. "
                     + string(wcgc,"99.999.999/9999-99")
                     + " representada na  forma  de  seu".
  
  /* LINHA 99  representante */
  IF wcont = 99 
  THEN ASSIGN wlinha = "Contrato Social, por "
                     + STRING(wcontato,"X(33)")
                     + ", dora-".

  /* linha 69 - valor do ferramental */
  IF wcont = 121
  THEN ASSIGN wlinha = "R$ "
                     + STRING(wpreco,">>>>>,>>>,>>9.99").
  IF wcont = 122 
  THEN ASSIGN wlinha = "( " + wextenso + " )".
  
  /* linha 124 */

  IF wcont = 124 
  THEN DO:
    IF wimposto = 1 THEN ASSIGN wdesc-imposto = SUBSTRING(wlinha,34,57).
    IF wimposto = 2 THEN ASSIGN wdesc-imposto = SUBSTRING(wlinha,30,3) 
                                              + " " 
                                              + SUBSTRING(wlinha,34,57).
    ASSIGN wlinha = SUBSTRING(wlinha,1,29) 
                     + " "
                     + wdesc-imposto.
  END.
  
  IF wimposto = 1 THEN DO:
    /* linhas 72 e 73 - tributos */
    IF wcont = 126
    THEN ASSIGN wlinha = "Aliquota IPI: " 
                       + STRING(waliq-ipi,">>9.99").
    IF wcont = 127
    THEN ASSIGN wlinha = "Aliquota ICMS: "
                     + STRING(waliq-icm,">>9.99").
    END.

  /* LINHAS 132 a 137 forma de pagamento */
  IF wcont = 132
  THEN ASSIGN wlinha = wpag1.
  IF wcont = 133
  THEN ASSIGN wlinha = wpag2.
  IF wcont = 134 
  THEN ASSIGN wlinha = wpag3.
  IF wcont = 135
  THEN ASSIGN wlinha = wpag4.
   IF wcont = 136 
  THEN ASSIGN wlinha = wpag5.
  IF wcont = 137
  THEN ASSIGN wlinha = wpag6.

  /* LINHAS 138 a 144 forma de pagamento */
  IF wcont = 138
  THEN ASSIGN wlinha =  wpag7.
  IF wcont = 139
  THEN ASSIGN wlinha =  wpag8.
  IF wcont = 140 
  THEN ASSIGN wlinha =  wpag9.
  IF wcont = 141
  THEN ASSIGN wlinha =  wpag10.
   IF wcont = 142
  THEN ASSIGN wlinha =  wpag11.
  IF wcont = 143
  THEN ASSIGN wlinha =  wpag12.

  IF wcont = 701 
  THEN ASSIGN wlinha = wfornecedor.

  IF wcont = 702
  THEN ASSIGN wlinha = wendereco.

  IF wcont = 703
  THEN ASSIGN wlinha = "At.." 
                     + wcontato.

  DISPLAY wlinha AT 18 WITH CENTERED NO-LABEL NO-BOX WIDTH 120.
  
END.
OUTPUT CLOSE.
MESSAGE "GERA��O CONCLU�DA!" SKIP 
        "Nome do arquivo: " wrelatorio VIEW-AS ALERT-BOX.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-zoom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-zoom C-Win
ON CHOOSE OF bt-zoom IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
 RUN gera-tela.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE aceita C-Win 
PROCEDURE aceita :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wpedido wrelatorio 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE wpedido bt-codigo BT-REL bt-zoom bt-sair-2 wrelatorio RECT-10 RECT-11 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN warquivo = wrelatorio.

get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
end.
    
run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE INICIA C-Win 
PROCEDURE INICIA :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
RUN limpa. /*
DISABLE ALL EXCEPT bt-inc bt-sair-2 WITH FRAME  {&FRAME-NAME}.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE limpa C-Win 
PROCEDURE limpa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wfornecedor      = ""
       wendereco        = ""
       wcgc             = ""
       wcontato         = ""
       wpreco           = 0
       waliq-ipi        = 0
       waliq-icm        = 0
       wpedido          = 0
       wextenso         = ""
       wpag1            = ""
       wpag2            = ""
       wpag3            = ""
       wpag4            = ""
       wpag5            = ""
       wpag6            = ""
       wpag7            = ""
       wpag8            = ""
       wpag9            = ""
       wpag10           = ""
       wpag11           = ""
       wpag12           = ""
       wimposto         = 2.
  
ASSIGN wpedido = 0.
DISPLAY wpedido WITH FRAME {&FRAME-NAME}.
DISABLE wrelatorio WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

