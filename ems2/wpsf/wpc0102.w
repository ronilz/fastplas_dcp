&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          eai              PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE BUFFER xtb-pr-cc     FOR tb-pr-cc.
DEFINE BUFFER xitem-tab     FOR item-tab.
DEFINE VARIABLE wexiste     AS  INTE FORMAT "9".
DEFINE VARIABLE wvlprat     AS  DECI FORMAT ">>>,>>>,>>>,>>9.9999".

DEFINE VARIABLE wprogr      AS CHAR    FORMAT "X(08)".
DEFINE VARIABLE wemp        AS CHAR FORMAT "X(40)".
DEFINE VARIABLE wltmes      AS CHAR FORMAT "X(09)" EXTENT 12 INITIAL ["Janeiro",
                                                                 "Fevereiro",
                                                                 "Marco",
                                                                 "Abril",
                                                                 "Maio",
                                                                 "Junho",
                                                                 "Julho",
                                                                 "Agosto",
                                                                 "Setembro",
                                                                 "Outubro",
                                                                 "Novembro",
                                                                 "Dezembro"].

/* VARIAVEIS PARA O RELATORIO */
DEFINE VARIABLE wdata       AS DATE    FORMAT "99/99/9999".
DEFINE VARIABLE wusr        AS CHAR    FORMAT "X(10)".
DEFINE VARIABLE whora       AS CHAR    FORMAT "X(8)".
DEFINE VARIABLE wstart      AS CHAR    FORMAT "X".
DEFINE VARIABLE wdtd        AS CHAR    FORMAT "X(10)".
DEFINE VARIABLE wctf        AS INTE.
DEFINE VARIABLE www         AS INTE.
DEFINE VARIABLE wtab        LIKE item-tab.nr-tab.
DEFINE VARIABLE wtipo          AS INTEGER NO-UNDO. /* tipo de impressora */

DEFINE VARIABLE wconf-imp      AS LOGICAL FORMAT "Sim/Nao".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-3

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tb-pr-cc

/* Definitions for BROWSE BROWSE-3                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-3 tb-pr-cc.cod-emitente ~
tb-pr-cc.nr-tab tb-pr-cc.dt-inicio tb-pr-cc.dt-termino 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-3 
&Scoped-define QUERY-STRING-BROWSE-3 FOR EACH tb-pr-cc NO-LOCK ~
    BY tb-pr-cc.dt-inicio INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-3 OPEN QUERY BROWSE-3 FOR EACH tb-pr-cc NO-LOCK ~
    BY tb-pr-cc.dt-inicio INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-3 tb-pr-cc
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-3 tb-pr-cc


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-3}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-14 RECT-18 RECT-19 RECT-2 RECT-3 ~
RECT-4 RECT-5 wemit-con bt-consulta-2 wforn apdat wemit wnome-emit BUTTON-2 ~
bt-sair-4 BROWSE-3 apref wobs wrel w-imp bt-executar bt-sair-3 
&Scoped-Define DISPLAYED-OBJECTS wemit-con wforn apdat wemit wnome-emit ~
apref wobs wrel w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-consulta 
     IMAGE-UP FILE "image/im-getfl.bmp":U
     LABEL "Consulta Tabela" 
     SIZE 5 BY 1 TOOLTIP "Consulta Tabela de Pre�os"
     BGCOLOR 8 .

DEFINE BUTTON bt-consulta-2 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Procura Plano" 
     SIZE 5 BY 1 TOOLTIP "Procura Tabela"
     BGCOLOR 8 .

DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 11 BY 1 TOOLTIP "Gerar em arquivo ou impressora ou arquivo c/compress�o"
     FONT 1.

DEFINE BUTTON bt-sair-3 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-sair-4 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1 TOOLTIP "Sair"
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-2 
     LABEL "Confirma ordem" 
     SIZE 15 BY 1.13.

DEFINE VARIABLE wobs AS CHARACTER 
     VIEW-AS EDITOR NO-WORD-WRAP MAX-CHARS 160 SCROLLBAR-HORIZONTAL SCROLLBAR-VERTICAL
     SIZE 38 BY 3.25 NO-UNDO.

DEFINE VARIABLE apdat AS DATE FORMAT "99/99/9999":U 
     LABEL "Vigor a partir de" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE apref AS CHARACTER FORMAT "X(40)":U 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wemit AS CHARACTER FORMAT "X(40)":U 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 41 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wemit-con AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "C�digo Emitente" 
     VIEW-AS FILL-IN 
     SIZE 6 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wforn AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Fornecedor" 
     VIEW-AS FILL-IN 
     SIZE 8.72 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wnome-emit AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 36 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(29)":U 
     LABEL "Nome do relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 1,
"Impressora", 2,
"Arquivo com compress�o", 3
     SIZE 56 BY 1.13 TOOLTIP "Escolha o par�metro para impress�o" NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 54 BY 7.75.

DEFINE RECTANGLE RECT-14
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 47 BY 14.

DEFINE RECTANGLE RECT-18
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 56 BY 14.

DEFINE RECTANGLE RECT-19
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 104 BY 1.5.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 54 BY 4.25.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 104 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 104 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 54 BY .75
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-3 FOR 
      tb-pr-cc SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-3 C-Win _STRUCTURED
  QUERY BROWSE-3 NO-LOCK DISPLAY
      tb-pr-cc.cod-emitente WIDTH 8.43
      tb-pr-cc.nr-tab WIDTH 12.43
      tb-pr-cc.dt-inicio WIDTH 10.43
      tb-pr-cc.dt-termino
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 45 BY 8
         FONT 1 ROW-HEIGHT-CHARS .67 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wemit-con AT ROW 2.5 COL 21 COLON-ALIGNED
     bt-consulta-2 AT ROW 2.5 COL 33
     wforn AT ROW 2.5 COL 61 COLON-ALIGNED
     apdat AT ROW 2.5 COL 87.72 COLON-ALIGNED
     wemit AT ROW 3.75 COL 61 COLON-ALIGNED
     wnome-emit AT ROW 4.25 COL 6 COLON-ALIGNED NO-LABEL
     BUTTON-2 AT ROW 5 COL 63 WIDGET-ID 2
     bt-sair-4 AT ROW 5 COL 80
     bt-consulta AT ROW 5 COL 87
     BROWSE-3 AT ROW 7.25 COL 3
     apref AT ROW 8 COL 85 COLON-ALIGNED NO-LABEL
     wobs AT ROW 9.75 COL 66 NO-LABEL
     wrel AT ROW 13.25 COL 68 COLON-ALIGNED
     w-imp AT ROW 17 COL 70 RIGHT-ALIGNED NO-LABEL
     bt-executar AT ROW 17 COL 79
     bt-sair-3 AT ROW 17 COL 92
     "Impress�o" VIEW-AS TEXT
          SIZE 11 BY .92 AT ROW 17 COL 3
     "Observa��o:" VIEW-AS TEXT
          SIZE 12 BY .67 TOOLTIP "Observa��o" AT ROW 10.25 COL 52
     "Esta altera��o de pedido refere-se a:" VIEW-AS TEXT
          SIZE 35 BY .67 AT ROW 8 COL 52
     RECT-1 AT ROW 7.75 COL 51
     RECT-14 AT ROW 2 COL 2
     RECT-18 AT ROW 2 COL 50
     RECT-19 AT ROW 16.75 COL 2
     RECT-2 AT ROW 2.25 COL 51
     RECT-3 AT ROW 1.25 COL 2
     RECT-4 AT ROW 16 COL 2
     RECT-5 AT ROW 6.5 COL 51
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 105.57 BY 17.54.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Valor Praticado x Valor Negociado - wpc0102"
         HEIGHT             = 17.54
         WIDTH              = 105.57
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB BROWSE-3 bt-consulta DEFAULT-FRAME */
/* SETTINGS FOR BUTTON bt-consulta IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       bt-consulta:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-3
/* Query rebuild information for BROWSE BROWSE-3
     _TblList          = "eai.tb-pr-cc"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "mgind.tb-pr-cc.dt-inicio|yes"
     _FldNameList[1]   > eai.tb-pr-cc.cod-emitente
"tb-pr-cc.cod-emitente" ? ? "integer" ? ? ? ? ? ? no ? no no "8.43" yes no no "U" "" ""
     _FldNameList[2]   > eai.tb-pr-cc.nr-tab
"tb-pr-cc.nr-tab" ? ? "character" ? ? ? ? ? ? no ? no no "12.43" yes no no "U" "" ""
     _FldNameList[3]   > eai.tb-pr-cc.dt-inicio
"tb-pr-cc.dt-inicio" ? ? "date" ? ? ? ? ? ? no ? no no "10.43" yes no no "U" "" ""
     _FldNameList[4]   = eai.tb-pr-cc.dt-termino
     _Query            is OPENED
*/  /* BROWSE BROWSE-3 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Valor Praticado x Valor Negociado - wpc0102 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Valor Praticado x Valor Negociado - wpc0102 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-3
&Scoped-define SELF-NAME BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 C-Win
ON VALUE-CHANGED OF BROWSE-3 IN FRAME DEFAULT-FRAME
DO:
   ASSIGN wforn = tb-pr-cc.cod-emitente
          apdat = tb-pr-cc.dt-inicio.
   DISPLAY wforn apdat WITH FRAME   {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-consulta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-consulta C-Win
ON CHOOSE OF bt-consulta IN FRAME DEFAULT-FRAME /* Consulta Tabela */
DO:
   RUN ../especificos/wpsf/wpc0102a.w. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-consulta-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-consulta-2 C-Win
ON CHOOSE OF bt-consulta-2 IN FRAME DEFAULT-FRAME /* Procura Plano */
DO:
    RUN inicia.
    ASSIGN wemit-con.
    IF wemit-con <> 0  THEN DO:
      FIND FIRST emitente WHERE cod-emitente = wemit-con NO-LOCK NO-ERROR.
      IF AVAILABLE emitente THEN DO:
         DISPLAY nome-emit @ wnome-emit WITH FRAME {&FRAME-NAME}.
         FIND FIRST tb-pr-cc WHERE tb-pr-cc.cod-emitente = wemit-con
                             NO-LOCK NO-ERROR.
         IF AVAILABLE tb-pr-cc THEN DO:
           OPEN QUERY browse-3 FOR EACH tb-pr-cc WHERE tb-pr-cc.cod-emitente = wemit-con
                               NO-LOCK.

           GET LAST browse-3.
           ASSIGN wforn = tb-pr-cc.cod-emitente
                apdat = tb-pr-cc.dt-inicio.
           DISPLAY wforn apdat WITH FRAME   {&FRAME-NAME}.
           END.
         END.
       IF NOT AVAILABLE tb-pr-cc THEN do:
          OPEN QUERY browse-3 FOR EACH tb-pr-cc WHERE tb-pr-cc.cod-emitente = wemit-con
                               NO-LOCK.

           GET LAST browse-3.
           MESSAGE "N�o existem tabelas para este fornecedor:"
                                 wemit-con VIEW-AS ALERT-BOX.
         
          END.
     IF NOT AVAILABLE emitente THEN MESSAGE "Emitente:" wemit-con "n�o encontrado."
                               VIEW-AS ALERT-BOX.
      END.
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN w-imp.
  ASSIGN wforn apdat apref wobs wrel.
  CASE w-imp:
    WHEN 1 THEN DO:
      OUTPUT TO VALUE(wrel) PAGE-SIZE 63.
      RUN imprimir. 
      END. 
     
     WHEN 2 THEN DO:
     /* impressora */
        SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
        IF wconf-imp = YES THEN DO:
           OUTPUT TO PRINTER PAGE-SIZE 63.
          {wpsf/imp.p}
          PUT CONTROL wcomprime /*wpaisagem*/ a4. 
          RUN imprimir. 
          END.
        END.
      WHEN 3 THEN DO:
       RUN comprime-arquivo.
       END.
     END CASE.
     MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-3 C-Win
ON CHOOSE OF bt-sair-3 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-4 C-Win
ON CHOOSE OF bt-sair-4 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME DEFAULT-FRAME /* Confirma ordem */
DO:
    ASSIGN wforn apdat.
    RUN confirma.
    ENABLE apref wobs wrel WITH FRAME    {&FRAME-NAME}.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo C-Win 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
 DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
 DEFINE VARIABLE a4 AS CHAR.


RUN ../especificos/wpsf/wp9901.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    END.
/* HP */
IF wtipo = 2 THEN DO:
    /*ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ */
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
OUTPUT TO value(wrel) page-size 63.
PUT CONTROL wcomprime a4. 
RUN imprimir.  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirma C-Win 
PROCEDURE confirma :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  FIND FIRST emitente WHERE emitente.cod-emitente = wforn NO-LOCK.
  IF AVAILABLE emitente THEN DO:
    DISPLAY emitente.nome-emit @ wemit WITH FRAME {&FRAME-NAME}.
  
    IF apdat <> ? THEN DO:
      FIND FIRST tb-pr-cc WHERE tb-pr-cc.nome-abrev = emitente.nome-abrev
                          AND   tb-pr-cc.dt-inicio  = apdat
                          NO-LOCK NO-ERROR.
      IF NOT AVAILABLE tb-pr-cc THEN DO:
         MESSAGE  "Emitente:" emitente.nome-abrev
                   SKIP
                   "Data de in�cio n�o encontrada: " string(apdat,"99/99/9999") 
                   VIEW-AS alert-box.
         RUN inicia.
         END.
      END.
    END.
  IF NOT AVAILABLE emitente THEN MESSAGE "Fornecedor Desconhecido:" wforn
                            VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wemit-con wforn apdat wemit wnome-emit apref wobs wrel w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-14 RECT-18 RECT-19 RECT-2 RECT-3 RECT-4 RECT-5 wemit-con 
         bt-consulta-2 wforn apdat wemit wnome-emit BUTTON-2 bt-sair-4 BROWSE-3 
         apref wobs wrel w-imp bt-executar bt-sair-3 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-bk C-Win 
PROCEDURE gera-bk :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE BUFFER xtb-pr-cc     FOR tb-pr-cc.
  DEFINE BUFFER xitem-tab     FOR item-tab.
  
  IF AVAILABLE tb-pr-cc THEN DO:
/*    OUTPUT TO VALUE(wrel) PAGED.*/
    FOR EACH item-tab WHERE item-tab.cod-emitente = tb-pr-cc.cod-emitente
                      AND   item-tab.cod-cond     = tb-pr-cc.cod-cond
                      AND   item-tab.nr-tab       = tb-pr-cc.nr-tab
                      NO-LOCK BREAK BY item-tab.nome-abrev
                      BY item-tab.it-codigo:
      
        IF AVAILABLE item-tab THEN DO:
          FOR EACH xtb-pr-cc
                             WHERE xtb-pr-cc.nome-abrev = emitente.nome-abrev
                             AND   xtb-pr-cc.dt-inicio LT apdat
                             NO-LOCK
                             BREAK BY xtb-pr-cc.nome-abrev
                                   BY xtb-pr-cc.dt-inicio:
          IF LAST-OF(xtb-pr-cc.dt-inicio) THEN DO:
            FIND  FIRST xitem-tab WHERE xitem-tab.nome-abrev 
                                  = xtb-pr-cc.nome-abrev
                                  AND   xitem-tab.nr-tab 
                                  = xtb-pr-cc.nr-tab
                                  AND   xitem-tab.it-codigo
                                  = item-tab.it-codigo
                                  NO-LOCK NO-ERROR.
            IF AVAILABLE xitem-tab THEN DO:
              ASSIGN wvlprat = xitem-tab.pr-item
                     wtab    = xitem-tab.nr-tab.
              END.
            IF NOT AVAILABLE xitem-tab THEN ASSIGN wvlprat = 0.
            END.
          END.                         
        FIND item OF item-tab NO-LOCK.
        /* Procura condicao de pagto */
        FIND cond-pagto WHERE cond-pagto.cod-cond-pag = 
                              tb-pr-cc.cod-cond-pag 
                              NO-LOCK NO-ERROR.
                              
      
FORM
item.it-codigo                   AT 15 LABEL "Codigo Item" FORMAT "X(13)"
item.descricao-1                 LABEL "Descricao Item"
item-tab.aliquota-ipi            LABEL "%IPI"
wvlprat                          LABEL "Valor Praticado"
                                 FORMAT ">>>,>>>,>>9.9999"
item-tab.pr-item                 LABEL "Valor Negociado"
                                 FORMAT ">>>,>>>,>>9.9999"
HEADER
wemp                                AT 15    "                   *****************************"
SKIP
"                                                             *    TABELA DE REAJUSTE     *" AT 15
SKIP
"Rua Luiz Lawrie Reid, 250                                    *    ==================     *" AT 15
SKIP
"Telefone (011) 4057-8400 - DIADEMA - SP                      *                           *" AT 15
SKIP
"FAX: (011) 4057-2228                                         *         DE PRECO          *" AT 15
SKIP
"C.G.C.M.F.     - 02.442.683/0001.62                          *         ========          *" AT 15
SKIP
"Insc. Estadual -    286.151.087.112                          *****************************" AT 15
SKIP
"TABELA VR. PRATICADO:" AT 24 wtab  "/  TABELA VR. NEGOCIADO:"  tb-pr-cc.nr-tab 
SKIP
"------------------------------------------------------------------------------------------" AT 15
SKIP
"Fornecedor... " AT 15 emitente.nome-emit  AT 32 "    Cod.:"emitente.cod-emitente 
SKIP
"Endereco..... " AT 15 emitente.endereco   AT 32  
SKIP
"Bairro....... " AT 15 emitente.bairro     AT 32
SKIP
"Cidade....... " AT 15 emitente.cidade     AT 32  "CEP:" emitente.cep  SPACE(05)
"Estado.......:" emitente.estado      
SKIP
"Telefone..... " AT 15 emitente.telefone[1] AT 32 SPACE(11)
"Ramal....:"     emitente.ramal[1]  
SKIP
"Cond.Pagto... " AT 15 cond-pagto.descricao  AT 32
SKIP
"==========================================================================================" AT 15
SKIP
        WITH FRAME fcons1 11 DOWN NO-BOX ROW 01 WIDTH 150 NO-LABELS OVERLAY.
        FIND FIRST item-fornec WHERE item-fornec.it-codigo    
                                  =  item.it-codigo
                               AND   item-fornec.cod-emitente 
                                  =  emitente.cod-emitente
                               NO-LOCK NO-ERROR.
        DISPLAY item.it-codigo 
                item.descricao-1 wvlprat
                item-tab.pr-item item-tab.aliquota-ipi 
                CAPS(item-fornec.unid-med-for) FORMAT "X(02)" 
                LABEL "UN." WITH FRAME fcons1.
        PUT SCREEN ROW 23 "Item: " + STRING(item.it-codigo).
        wctf = wctf + 1.
        IF LAST-OF(item-tab.nome-abrev) THEN DO:
          DOWN (29 - wctf) WITH FRAME fcons1.
          wctf = 30.
          END.
        IF wctf > 28 THEN DO:
          wctf = 0.
          DOWN 1 WITH FRAME fcons1.
DISPLAY
SKIP
"=========================================================================================" AT 15
SKIP
"ESTA ALTERACAO DE PEDIDO REFERE-SE A:" AT 15 apref                                               
SKIP
"QUE  ENTRARA  EM  VIGOR  A PARTIR DE:" AT 15 apdat                                               
SKIP
"==========================================================================================" AT 15
SKIP
"*** OBSERVACAO : ***" AT 15 wobs COLON 34 
SKIP
"==========================================================================================" AT 15
SKIP
"     " 
    "           Diadema,  "  AT 15  DAY(TODAY) "de" wltmes[MONTH(TODAY)] "de" YEAR(TODAY) SKIP
"     " 
SKIP
"                                       SEEBER FASTPLAS LTDA."  AT 15
SKIP
"     " 
SKIP
"     " 
SKIP
"                                  -------------------------------      " AT 15
SKIP
"==========================================================================================" AT 15
          WITH FRAME fobs NO-BOX NO-LABEL OVERLAY WITH WIDTH 120.
          END.
        END.
      END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE imprimir C-Win 
PROCEDURE imprimir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE BUFFER xtb-pr-cc     FOR tb-pr-cc.
  DEFINE BUFFER xitem-tab     FOR item-tab.
  
  IF AVAILABLE tb-pr-cc THEN DO:
    FOR EACH item-tab WHERE item-tab.cod-emitente = tb-pr-cc.cod-emitente
                      AND   item-tab.cod-cond     = tb-pr-cc.cod-cond
                      AND   item-tab.nr-tab       = tb-pr-cc.nr-tab
                      NO-LOCK BREAK BY item-tab.nome-abrev
                      BY item-tab.it-codigo:
      
        IF AVAILABLE item-tab THEN DO:
          FOR EACH xtb-pr-cc
                             WHERE xtb-pr-cc.nome-abrev = emitente.nome-abrev
                             AND   xtb-pr-cc.dt-inicio LT apdat
                             NO-LOCK
                             BREAK BY xtb-pr-cc.nome-abrev
                                   BY xtb-pr-cc.dt-inicio:
          IF LAST-OF(xtb-pr-cc.dt-inicio) THEN DO:
            FIND  FIRST xitem-tab WHERE xitem-tab.nome-abrev 
                                  = xtb-pr-cc.nome-abrev
                                  AND   xitem-tab.nr-tab 
                                  = xtb-pr-cc.nr-tab
                                  AND   xitem-tab.it-codigo
                                  = item-tab.it-codigo
                                  NO-LOCK NO-ERROR.
            IF AVAILABLE xitem-tab THEN DO:
              ASSIGN wvlprat = xitem-tab.pr-item
                     wtab    = xitem-tab.nr-tab.
              END.
            IF NOT AVAILABLE xitem-tab THEN ASSIGN wvlprat = 0.
            END.
          END.                         
        FIND item OF item-tab NO-LOCK.
        /* Procura condicao de pagto */
        FIND cond-pagto WHERE cond-pagto.cod-cond-pag = 
                              tb-pr-cc.cod-cond-pag 
                              NO-LOCK NO-ERROR.
                              
      
FORM
item.it-codigo                   AT 15 LABEL "Codigo Item" FORMAT "X(13)"
item.descricao-1                 LABEL "Descricao Item"
item-tab.aliquota-ipi            LABEL "%IPI"
wvlprat                          LABEL "Valor Praticado"
                                 FORMAT ">>>,>>>,>>9.9999"
item-tab.pr-item                 LABEL "Valor Negociado"
                                 FORMAT ">>>,>>>,>>9.9999"
HEADER
wemp                                AT 15    "                   *****************************"
SKIP
"                                                             *    TABELA DE REAJUSTE     *" AT 15
SKIP
"Rua Luiz Lawrie Reid, 250                                    *    ==================     *" AT 15
SKIP
"Telefone (011) 4057-8400 - DIADEMA - SP                      *                           *" AT 15
SKIP
"FAX: (011) 4057-2228                                         *         DE PRECO          *" AT 15
SKIP
"C.G.C.M.F.     - 02.442.683/0001.62                          *         ========          *" AT 15
SKIP
"Insc. Estadual -    286.151.087.112                          *****************************" AT 15
SKIP
"TABELA VR. PRATICADO:" AT 24 wtab  "/  TABELA VR. NEGOCIADO:"  tb-pr-cc.nr-tab 
SKIP
"------------------------------------------------------------------------------------------" AT 15
SKIP
"Fornecedor... " AT 15 emitente.nome-emit  AT 32 "    Cod.:"emitente.cod-emitente 
SKIP
"Endereco..... " AT 15 emitente.endereco   AT 32  
SKIP
"Bairro....... " AT 15 emitente.bairro     AT 32
SKIP
"Cidade....... " AT 15 emitente.cidade     AT 32  "CEP:" emitente.cep  SPACE(05)
"Estado.......:" emitente.estado      
SKIP
"Telefone..... " AT 15 emitente.telefone[1] AT 32 SPACE(11)
"Ramal....:"     emitente.ramal[1]  
SKIP
"Cond.Pagto... " AT 15 cond-pagto.descricao  AT 32
SKIP
"==========================================================================================" AT 15
SKIP
        WITH FRAME fcons1 11 DOWN NO-BOX ROW 01 WIDTH 150 NO-LABELS OVERLAY.
        FIND FIRST item-fornec WHERE item-fornec.it-codigo    
                                  =  item.it-codigo
                               AND   item-fornec.cod-emitente 
                                  =  emitente.cod-emitente
                               NO-LOCK NO-ERROR.
        
        DISPLAY item.it-codigo 
                item.descricao-1 wvlprat
                item-tab.pr-item item-tab.aliquota-ipi 
                WITH FRAME fcons1.
       IF AVAILABLE item-fornec THEN DISPLAY
                CAPS(item-fornec.unid-med-for) FORMAT "X(02)" 
                LABEL "UN." WITH FRAME fcons1.
        PUT SCREEN ROW 23 "Item: " + STRING(item.it-codigo).
        wctf = wctf + 1.
        IF LAST-OF(item-tab.nome-abrev) THEN DO:
          DOWN (26 - wctf) WITH FRAME fcons1.
          wctf = 27.
          END.
        IF wctf > 25 THEN DO:
          wctf = 0.
          DOWN 1 WITH FRAME fcons1.
DISPLAY
SKIP
"=========================================================================================" AT 15
SKIP
"ESTA ALTERACAO DE PEDIDO REFERE-SE A:" AT 15 apref                                               
SKIP
"QUE  ENTRARA  EM  VIGOR  A PARTIR DE:" AT 15 apdat                                               
SKIP
"==========================================================================================" AT 15
SKIP
"*** OBSERVACAO : ***" AT 15 wobs COLON 34 
SKIP
"==========================================================================================" AT 15
SKIP
"     " 
    "           Diadema,  "  AT 15  DAY(TODAY) "de" wltmes[MONTH(TODAY)] "de" YEAR(TODAY) SKIP
"     " 
SKIP
"                                       SEEBER FASTPLAS LTDA."  AT 15
SKIP
"     " 
SKIP
"     " 
SKIP
"                                  -------------------------------      " AT 15
SKIP
"==========================================================================================" AT 15
          WITH FRAME fobs NO-BOX NO-LABEL OVERLAY WITH WIDTH 120.
          END.
        END.
      END.
    END.
  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN   apdat = 01/01/0001
         wrel = "V:\spool\TABREAJUSTE.TXT"
         wforn = 0
         wobs  = ""
         apref = ""
         wemit = "".
DISPLAY apdat wrel wforn wobs  apref wemit WITH FRAME  {&FRAME-NAME}.
FIND FIRST mgadm.empresa NO-LOCK NO-ERROR.
ASSIGN wemp = "SEEBER FASTPLAS LTDA".
DISABLE apref wobs wrel wemit wnome-emit WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vazia C-Win 
PROCEDURE vazia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

