&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBulder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEF INPUT PARAMETER i-cod-emitente AS INTEGER FORMAT ">>>>>>>>9".
DEF INPUT PARAMETER i-nr-pedcli    AS CHAR    FORMAT "X(12)".
DEF INPUT PARAMETER i-wemb         AS CHAR    FORMAT "X(35)".
DEF INPUT PARAMETER i-wqt-emb      AS INTEGER FORMAT "ZZZZZZ99".
DEF INPUT PARAMETER i-wreq         AS CHAR    FORMAT "X(20)".
DEF INPUT PARAMETER i-cod-estab    AS CHAR    FORMAT "X(03)".
/* Local Variable Definitions ---                                       */


DEFINE WORKFILE b-itens 
       FIELD    b-it            AS CHAR FORMAT "X(16)" COLUMN-LABEL "Item"
       FIELD    b-qt            AS INTEGER FORMAT "ZZZZ,ZZ9"
                                COLUMN-LABEL "Quantidade"
       FIELD    b-nr-pedcli     LIKE ped-venda.nr-pedcli
       FIELD    b-cod-emitente  LIKE ped-venda.cod-emitente
       FIELD    b-nr-pedido     LIKE ped-venda.nr-pedido
       FIELD    b-nat-operacao  LIKE ped-venda.nat-operacao
       FIELD    b-nome-abrev    LIKE ped-venda.nome-abrev
       FIELD    b-vl-preuni     LIKE ped-item.vl-preuni
       FIELD    b-emb           AS CHAR FORMAT "X(35)"
       FIELD    b-qt-emb        AS INTE FORMAT "ZZZZZZ99"
       FIELD    b-req           AS CHAR FORMAT "X(20)"
       FIELD    b-item-do-cli   LIKE item-cli.item-do-cli.
DEFINE WORKFILE wsequencias 
       FIELD    ww-nr-pedcli    LIKE ped-item.nr-pedcli
       FIELD    ww-nr-sequencia LIKE ped-item.nr-sequencia
       FIELD    ww-it-codigo    LIKE ped-item.it-codigo
       FIELD    ww-saldo        LIKE ped-item.qt-atendida COLUMN-LABEL "Saldo Item"
       FIELD    ww-qt-pedida    LIKE ped-item.qt-pedida.

DEFINE VARIABLE t-item          AS CHAR FORMAT "X(16)"    EXTENT 20. 
DEFINE variable t-quant         AS INTE FORMAT "ZZZZ,ZZ9" EXTENT 20.
DEFINE VARIABLE wvl-total   LIKE ped-item.vl-preuni.      
DEFINE VARIABLE wvl-preuni  LIKE ped-item.vl-preuni.       
DEFINE VARIABLE wsaldo      LIKE ped-item.qt-pedida.
DEFINE VARIABLE wsal-it     LIKE ped-item.qt-pedida COLUMN-LABEL "Saldo do item".
DEFINE VARIABLE wsal-atend  LIKE ped-item.qt-pedida.
DEFINE VARIABLE wit-ant     LIKE ped-item.it-codigo.

DEFINE VARIABLE wseqs       LIKE ped-item.nr-sequencia.


DEFINE VARIABLE c-rodape1   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape2   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape3   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape4   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape5   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape6   AS CHAR  FORMAT "x(148)".

DEFINE VARIABLE wped-fim    LIKE ped-venda.nr-pedcli.
DEFINE VARIABLE wdesc       LIKE item.descricao-1.
DEFINE VARIABLE wobs        AS CHAR FORMAT "X(100)".
DEFINE VARIABLE wlinha      AS CHAR FORMAT "X(148)".
DEFINE VARIABLE wlinha1     AS CHAR FORMAT "X(133)".
DEFINE VARIABLE wlinha2     AS CHAR FORMAT "X(133)".
DEFINE VARIABLE wlinha-item AS CHAR FORMAT "X(133)".
DEFINE VARIABLE wcont       AS INTE FORMAT "99".
DEFINE VARIABLE wcont1      AS INTE FORMAT "99".
DEFINE VARIABLE windex      AS INTE FORMAT "99".
DEFINE VARIABLE wcont2      AS INTE FORMAT "99".
DEFINE VARIABLE wvl-tot     AS DEC FORMAT ">>>,>>>,>>>,>>9.99".
DEFINE VARIABLE wrel        AS CHAR FORMAT "X(21)".
DEFINE VARIABLE w-it-ant    AS CHAR FORMAT "X(16)".
DEFINE VARIABLE wconf-imp   AS LOGICAL NO-UNDO.
DEFINE VARIABLE wemp        AS CHAR FORMAT "X(40)".
/* BUFFER */
DEFINE BUFFER   b-ped-item  FOR ped-item.
DEFINE BUFFER   b-ped-ent   FOR ped-ent.
DEFINE BUFFER   b-ped-venda FOR ped-venda.
DEFINE VARIABLE wtipo       AS INTEGER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-10 RECT-8 RECT-9 witem wquant Bt-CONSID ~
witem-2 wquant-2 witem-3 wquant-3 bt-rel witem-4 wquant-4 bt-arqcomp ~
witem-5 wquant-5 witem-6 wquant-6 bt-arq witem-7 wquant-7 witem-8 wquant-8 ~
bt-sair witem-9 wquant-9 witem-10 wquant-10 witem-11 wquant-11 witem-12 ~
wquant-12 witem-13 wquant-13 witem-14 wquant-14 witem-15 wquant-15 witem-16 ~
wquant-16 witem-17 wquant-17 witem-18 wquant-18 witem-19 wquant-19 witem-20 ~
wquant-20 
&Scoped-Define DISPLAYED-OBJECTS witem wquant witem-2 wquant-2 witem-3 ~
wquant-3 witem-4 wquant-4 witem-5 wquant-5 witem-6 wquant-6 witem-7 ~
wquant-7 witem-8 wquant-8 witem-9 wquant-9 witem-10 wquant-10 witem-11 ~
wquant-11 witem-12 wquant-12 witem-13 wquant-13 witem-14 wquant-14 witem-15 ~
wquant-15 witem-16 wquant-16 witem-17 wquant-17 witem-18 wquant-18 witem-19 ~
wquant-19 witem-20 wquant-20 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-arq AUTO-GO 
     LABEL "Arquivo Texto" 
     SIZE 16 BY 1.13 TOOLTIP "Arquivo texto - sem formata��o"
     BGCOLOR 8 FONT 1.

DEFINE BUTTON bt-arqcomp 
     LABEL "Arquivo impress�o" 
     SIZE 16 BY 1.13 TOOLTIP "Gerar arquivo para imprimir posteriormente"
     FONT 1.

DEFINE BUTTON Bt-CONSID 
     LABEL "Considera��es" 
     SIZE 16 BY 1.13 TOOLTIP "Considera��es gerais"
     FONT 1.

DEFINE BUTTON bt-rel 
     LABEL "Relat�rio - Impressora" 
     SIZE 16 BY 1.13 TOOLTIP "Imprimir escolhendo a impressora"
     FONT 1.

DEFINE BUTTON bt-sair AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 7 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-3 
     LABEL "Gerar arquivo" 
     SIZE 15 BY 1.13.

DEFINE VARIABLE witem AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [1]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-10 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [10]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-11 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [11]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-12 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [12]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-13 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [13]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-14 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [14]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-15 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [15]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-16 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [16]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-17 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [17]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-18 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [18]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-19 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [19]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-2 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [2]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-20 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [20]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-3 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [3]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-4 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [4]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-5 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [5]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-6 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [6]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-7 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [7]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-8 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [8]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-9 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [9]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE wquant AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [1]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-10 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [10]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-11 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [11]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-12 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [12]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-13 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [13]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-14 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [14]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-15 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [15]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-16 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [16]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-17 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [17]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-18 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [18]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-19 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [19]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [2]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-20 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [20]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-3 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [3]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-4 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [4]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-5 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [5]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-6 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [6]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-7 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [7]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-8 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [8]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-9 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [9]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 54 BY 15.75.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 78 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     witem AT ROW 2.75 COL 17 COLON-ALIGNED
     wquant AT ROW 2.75 COL 49 COLON-ALIGNED
     Bt-CONSID AT ROW 3 COL 63
     witem-2 AT ROW 3.5 COL 17 COLON-ALIGNED
     wquant-2 AT ROW 3.5 COL 49 COLON-ALIGNED
     witem-3 AT ROW 4.25 COL 17 COLON-ALIGNED
     wquant-3 AT ROW 4.25 COL 49 COLON-ALIGNED
     bt-rel AT ROW 4.25 COL 63
     witem-4 AT ROW 5 COL 17 COLON-ALIGNED
     wquant-4 AT ROW 5 COL 49 COLON-ALIGNED
     bt-arqcomp AT ROW 5.5 COL 63
     witem-5 AT ROW 5.75 COL 17 COLON-ALIGNED
     wquant-5 AT ROW 5.75 COL 49 COLON-ALIGNED
     witem-6 AT ROW 6.5 COL 17 COLON-ALIGNED
     wquant-6 AT ROW 6.5 COL 49 COLON-ALIGNED
     bt-arq AT ROW 6.75 COL 63
     witem-7 AT ROW 7.25 COL 17 COLON-ALIGNED
     wquant-7 AT ROW 7.25 COL 49 COLON-ALIGNED
     witem-8 AT ROW 8 COL 17 COLON-ALIGNED
     wquant-8 AT ROW 8 COL 49 COLON-ALIGNED
     bt-sair AT ROW 8.5 COL 68
     witem-9 AT ROW 8.75 COL 17 COLON-ALIGNED
     wquant-9 AT ROW 8.75 COL 49 COLON-ALIGNED
     witem-10 AT ROW 9.5 COL 17 COLON-ALIGNED
     wquant-10 AT ROW 9.5 COL 49 COLON-ALIGNED
     witem-11 AT ROW 10.25 COL 17 COLON-ALIGNED
     wquant-11 AT ROW 10.25 COL 49 COLON-ALIGNED
     BUTTON-3 AT ROW 10.5 COL 66
     witem-12 AT ROW 11 COL 17 COLON-ALIGNED
     wquant-12 AT ROW 11 COL 49 COLON-ALIGNED
     witem-13 AT ROW 11.75 COL 17 COLON-ALIGNED
     wquant-13 AT ROW 11.75 COL 49 COLON-ALIGNED
     witem-14 AT ROW 12.5 COL 17 COLON-ALIGNED
     wquant-14 AT ROW 12.5 COL 49 COLON-ALIGNED
     witem-15 AT ROW 13.25 COL 17 COLON-ALIGNED
     wquant-15 AT ROW 13.25 COL 49 COLON-ALIGNED
     witem-16 AT ROW 14 COL 17 COLON-ALIGNED
     wquant-16 AT ROW 14 COL 49 COLON-ALIGNED
     witem-17 AT ROW 14.75 COL 17 COLON-ALIGNED
     wquant-17 AT ROW 14.75 COL 49 COLON-ALIGNED
     witem-18 AT ROW 15.5 COL 17 COLON-ALIGNED
     wquant-18 AT ROW 15.5 COL 49 COLON-ALIGNED
     witem-19 AT ROW 16.25 COL 17 COLON-ALIGNED
     wquant-19 AT ROW 16.25 COL 49 COLON-ALIGNED
     witem-20 AT ROW 17 COL 17 COLON-ALIGNED
     wquant-20 AT ROW 17 COL 49 COLON-ALIGNED
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 10.25 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 8.75 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 5 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 8 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 6.5 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 3.5 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 5.75 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 4.25 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 15.5 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 17 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 13.25 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 16.25 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 14.75 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 11 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 14 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 12.5 COL 38
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 9.5 COL 38
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         CANCEL-BUTTON bt-sair.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Dialog-Frame
     "......." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 11.75 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 7.25 COL 38
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 2.75 COL 38
     RECT-10 AT ROW 2.25 COL 8
     RECT-8 AT ROW 1.25 COL 3
     RECT-9 AT ROW 18.25 COL 2
     SPACE(1.13) SKIP(0.07)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "ITENS - Solicita��o de Nota Fiscal  - wpf0104.w"
         CANCEL-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON BUTTON-3 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       BUTTON-3:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* ITENS - Solicita��o de Nota Fiscal  - wpf0104.w */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-arq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arq Dialog-Frame
ON CHOOSE OF bt-arq IN FRAME Dialog-Frame /* Arquivo Texto */
DO:
  DISABLE bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  ASSIGN witem wquant witem-2 wquant-2 witem-3 wquant-3 witem-4 wquant-4 
         witem-5 wquant-5 witem-6 wquant-6 witem-7 wquant-7 witem-8 wquant-8 witem-9 wquant-9
         witem-10 wquant-10 witem-11 wquant-11 witem-12 wquant-12 witem-13 wquant-13
         witem-14 wquant-14 witem-15 wquant-15 witem-16 wquant-16 witem-17 wquant-17
         witem-18 wquant-18 witem-19 wquant-19 witem-20 wquant-20.
  ASSIGN  t-item[1]  =  witem
         t-quant[1]  =  wquant
          t-item[2]  =  witem-2
         t-quant[2]  =  wquant-2
          t-item[3]  =  witem-3
         t-quant[3]  =  wquant-3  
          t-item[4]  =  witem-4
         t-quant[4]  =  wquant-4
          t-item[5]  =  witem-5
         t-quant[5]  =  wquant-5
          t-item[6]  =  witem-6
         t-quant[6]  =  wquant-6
          t-item[7]  =  witem-7
         t-quant[7]  =  wquant-7
          t-item[8]  =  witem-8
         t-quant[8]  =  wquant-8  
          t-item[9]  =  witem-9
         t-quant[9]  =  wquant-9
          t-item[10] =  witem-10
         t-quant[10] =  wquant-10
          t-item[11] =  witem-11
         t-quant[11] =  wquant-11
          t-item[12] =  witem-12
         t-quant[12] =  wquant-12
          t-item[13] =  witem-13
         t-quant[13] =  wquant-13  
          t-item[14] =  witem-14
         t-quant[14] =  wquant-14
          t-item[15] =  witem-15
         t-quant[15] =  wquant-15
          t-item[16] =  witem-16
         t-quant[16] =  wquant-16
          t-item[17] =  witem-17
         t-quant[17] =  wquant-17
          t-item[18] =  witem-18
         t-quant[18] =  wquant-18  
          t-item[19] =  witem-19
         t-quant[19] =  wquant-19
          t-item[20] =  witem-20
         t-quant[20] =  wquant-20.
  RUN itens.
  ASSIGN wrel = "V:\spool\SOL" + STRING(TIME,"999999") + ".TXT".
  OUTPUT TO VALUE(wrel).
  RUN relatorio.
  OUTPUT CLOSE.
  ENABLE  bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  MESSAGE "GERA��O DO RELAT�RIO CONCLU�DA."
           SKIP "NOME DO RELATORIO: " wrel
           VIEW-AS ALERT-BOX WARNING.

  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-arqcomp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arqcomp Dialog-Frame
ON CHOOSE OF bt-arqcomp IN FRAME Dialog-Frame /* Arquivo impress�o */
DO:
    DISABLE bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  ASSIGN witem wquant witem-2 wquant-2 witem-3 wquant-3 witem-4 wquant-4 
         witem-5 wquant-5 witem-6 wquant-6 witem-7 wquant-7 witem-8 wquant-8 witem-9 wquant-9
         witem-10 wquant-10 witem-11 wquant-11 witem-12 wquant-12 witem-13 wquant-13
         witem-14 wquant-14 witem-15 wquant-15 witem-16 wquant-16 witem-17 wquant-17
         witem-18 wquant-18 witem-19 wquant-19 witem-20 wquant-20.
  ASSIGN  t-item[1]  =  witem
         t-quant[1]  =  wquant
          t-item[2]  =  witem-2
         t-quant[2]  =  wquant-2
          t-item[3]  =  witem-3
         t-quant[3]  =  wquant-3  
          t-item[4]  =  witem-4
         t-quant[4]  =  wquant-4
          t-item[5]  =  witem-5
         t-quant[5]  =  wquant-5
          t-item[6]  =  witem-6
         t-quant[6]  =  wquant-6
          t-item[7]  =  witem-7
         t-quant[7]  =  wquant-7
          t-item[8]  =  witem-8
         t-quant[8]  =  wquant-8  
          t-item[9]  =  witem-9
         t-quant[9]  =  wquant-9
          t-item[10] =  witem-10
         t-quant[10] =  wquant-10
          t-item[11] =  witem-11
         t-quant[11] =  wquant-11
          t-item[12] =  witem-12
         t-quant[12] =  wquant-12
          t-item[13] =  witem-13
         t-quant[13] =  wquant-13  
          t-item[14] =  witem-14
         t-quant[14] =  wquant-14
          t-item[15] =  witem-15
         t-quant[15] =  wquant-15
          t-item[16] =  witem-16
         t-quant[16] =  wquant-16
          t-item[17] =  witem-17
         t-quant[17] =  wquant-17
          t-item[18] =  witem-18
         t-quant[18] =  wquant-18  
          t-item[19] =  witem-19
         t-quant[19] =  wquant-19
          t-item[20] =  witem-20
         t-quant[20] =  wquant-20.
  RUN itens.
  ASSIGN wrel = "V:\spool\SOL" + STRING(TIME,"999999") + ".TXT".
  RUN comprime-arquivo.
  ENABLE  bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  MESSAGE "GERA��O DO RELAT�RIO CONCLU�DA."
           SKIP "NOME DO RELATORIO: " wrel
           VIEW-AS ALERT-BOX WARNING.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Bt-CONSID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Bt-CONSID Dialog-Frame
ON CHOOSE OF Bt-CONSID IN FRAME Dialog-Frame /* Considera��es */
DO:
  RUN consid.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-rel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-rel Dialog-Frame
ON CHOOSE OF bt-rel IN FRAME Dialog-Frame /* Relat�rio - Impressora */
DO:
  DISABLE bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  ASSIGN witem wquant witem-2 wquant-2 witem-3 wquant-3 witem-4 wquant-4 
         witem-5 wquant-5 witem-6 wquant-6 witem-7 wquant-7 witem-8 wquant-8 witem-9 wquant-9
         witem-10 wquant-10 witem-11 wquant-11 witem-12 wquant-12 witem-13 wquant-13
         witem-14 wquant-14 witem-15 wquant-15 witem-16 wquant-16 witem-17 wquant-17
         witem-18 wquant-18 witem-19 wquant-19 witem-20 wquant-20.
  ASSIGN  t-item[1]  =  witem
         t-quant[1]  =  wquant
          t-item[2]  =  witem-2
         t-quant[2]  =  wquant-2
          t-item[3]  =  witem-3
         t-quant[3]  =  wquant-3  
          t-item[4]  =  witem-4
         t-quant[4]  =  wquant-4
          t-item[5]  =  witem-5
         t-quant[5]  =  wquant-5
          t-item[6]  =  witem-6
         t-quant[6]  =  wquant-6
          t-item[7]  =  witem-7
         t-quant[7]  =  wquant-7
          t-item[8]  =  witem-8
         t-quant[8]  =  wquant-8  
          t-item[9]  =  witem-9
         t-quant[9]  =  wquant-9
          t-item[10] =  witem-10
         t-quant[10] =  wquant-10
          t-item[11] =  witem-11
         t-quant[11] =  wquant-11
          t-item[12] =  witem-12
         t-quant[12] =  wquant-12
          t-item[13] =  witem-13
         t-quant[13] =  wquant-13  
          t-item[14] =  witem-14
         t-quant[14] =  wquant-14
          t-item[15] =  witem-15
         t-quant[15] =  wquant-15
          t-item[16] =  witem-16
         t-quant[16] =  wquant-16
          t-item[17] =  witem-17
         t-quant[17] =  wquant-17
          t-item[18] =  witem-18
         t-quant[18] =  wquant-18  
          t-item[19] =  witem-19
         t-quant[19] =  wquant-19
          t-item[20] =  witem-20
         t-quant[20] =  wquant-20.
  RUN itens.
  DISABLE  bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
  IF wconf-imp THEN DO:
    OUTPUT TO PRINTER PAGE-SIZE 44.
    {wpsf\imp.p}
    PUT CONTROL wnormaliza wcomprime a4 . 
    RUN relatorio.
    END.

  ENABLE bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-3 Dialog-Frame
ON CHOOSE OF BUTTON-3 IN FRAME Dialog-Frame /* Gerar arquivo */
DO: 
 DISABLE bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
 ASSIGN wrel = "V:\spool\SOL" + STRING(TIME,"999999") + ".TXT".
 OUTPUT TO VALUE(wrel) PAGE-SIZE 64.
 RUN relatorio.
 OUTPUT CLOSE.
 ENABLE bt-sair bt-consid bt-arq bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
 MESSAGE "GERA��O DO RELAT�RIO CONCLU�DA." VIEW-AS ALERT-BOX WARNING.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  RUN consid.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo Dialog-Frame 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
DEFINE VARIABLE a4 AS CHAR.
DEFINE VARIABLE wnormaliza AS CHAR.   /* NORMALIZA */


RUN ../especificos/wpsf/wp9901.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    ASSIGN wnormaliza  = CHR(18).
    END.
/* HP */
IF wtipo = 2 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN wnormaliza = "~033~046~153~060~123".   /* NORMALIZA */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
OUTPUT TO value(wrel) page-size 44.
PUT CONTROL wnormaliza wcomprime /*wpaisagem*/ a4. 
RUN relatorio.                 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consid Dialog-Frame 
PROCEDURE consid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
MESSAGE 
  "                                 IMPORTANTE !!!!" 
  SKIP(1)
  "1) N�O REPETIR O MESMO C�DIGO DO ITEM."
  skip
  "2) N�O DEIXAR QUANTIDADE IGUAL A ZERO."
  SKIP
  "3) DIGITAR OS ITENS NA SEQU�NCIA NUM�RICA DA TELA."
   SKIP
  "4) N�O DEIXAR ITEM E QUANTIDADE EM BRANCO ENTRE UM ITEM E OUTRO."
   SKIP(2)
   "SE ESTAS INSTRU��ES N�O FOREM SEGUIDAS A SOLICITA��O SER� GERADA COM PROBLEMAS."
  VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY witem wquant witem-2 wquant-2 witem-3 wquant-3 witem-4 wquant-4 
          witem-5 wquant-5 witem-6 wquant-6 witem-7 wquant-7 witem-8 wquant-8 
          witem-9 wquant-9 witem-10 wquant-10 witem-11 wquant-11 witem-12 
          wquant-12 witem-13 wquant-13 witem-14 wquant-14 witem-15 wquant-15 
          witem-16 wquant-16 witem-17 wquant-17 witem-18 wquant-18 witem-19 
          wquant-19 witem-20 wquant-20 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-10 RECT-8 RECT-9 witem wquant Bt-CONSID witem-2 wquant-2 witem-3 
         wquant-3 bt-rel witem-4 wquant-4 bt-arqcomp witem-5 wquant-5 witem-6 
         wquant-6 bt-arq witem-7 wquant-7 witem-8 wquant-8 bt-sair witem-9 
         wquant-9 witem-10 wquant-10 witem-11 wquant-11 witem-12 wquant-12 
         witem-13 wquant-13 witem-14 wquant-14 witem-15 wquant-15 witem-16 
         wquant-16 witem-17 wquant-17 witem-18 wquant-18 witem-19 wquant-19 
         witem-20 wquant-20 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia Dialog-Frame 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN   wdesc          = " "
         wobs           = FILL("_",81)
         wlinha         = FILL("_",132)
         wlinha1        = FILL("_",132)
         wlinha2        = FILL("-",132)
         wlinha-item    = FILL("=",132)
         witem          = " "
         wquant         = 0
         wcont          = 0
         wcont1         = 0
         wvl-tot        = 0.
  FOR EACH b-itens:
    DELETE  b-itens.
    END.
  FOR EACH wsequencias:
    DELETE wsequencias.
    END.
  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ITENS Dialog-Frame 
PROCEDURE ITENS :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 RUN inicia.
 DO wcont = 1 TO 20 ON ERROR UNDO, RETRY:
 ASSIGN wsal-it = 0.
    IF t-item[wcont] = "" THEN NEXT.
    /*
    IF t-item[wcont] = w-it-ant THEN NEXT.
    */
    IF t-item[wcont] <> " " THEN DO:
    /*  Calcula Saldo */  
      FIND FIRST b-ped-venda WHERE (b-ped-venda.cod-sit-ped <> 3
                             AND b-ped-venda.cod-sit-ped <> 5
                             AND b-ped-venda.cod-sit-ped <> 6)
                             AND b-ped-venda.nr-pedcli    = i-nr-pedcli 
                             AND b-ped-venda.cod-emitente = i-cod-emitente
                             AND b-ped-venda.cod-estab    = i-cod-estab
                             NO-LOCK NO-ERROR.
      
      IF NOT AVAILABLE b-ped-venda THEN MESSAGE
      "Estab.:" i-cod-estab "Pedido:" i-nr-pedcli "Cliente:" i-cod-emitente
      "nao encontrado" VIEW-AS ALERT-BOX.
      ASSIGN wsaldo  = 0
             wsal-it = 0.
      /* Checa se o codigo do item existe ------------------------*/
      FIND FIRST b-ped-item  WHERE b-ped-item.it-codigo = t-item[wcont]
                             AND   b-ped-item.nr-pedcli = i-nr-pedcli                   
                             AND   b-ped-item.nome-abrev = 
                                   b-ped-venda.nome-abrev
                             AND   b-ped-venda.cod-estab = i-cod-estab
                             NO-LOCK NO-ERROR.
      IF NOT AVAILABLE b-ped-item THEN DO: 
        MESSAGE 
        "ITEM: " t-item[wcont] SKIP
        "NAO ENCONTRADO NO PEDIDO: " b-ped-venda.nr-pedcli 
        VIEW-AS ALERT-BOX.
        ASSIGN t-item[wcont] = ""
               t-quant[wcont] = 0.
        END.
      /* Calcula saldo ------------------------------------------------*/
      FOR EACH b-ped-item WHERE b-ped-item.it-codigo = t-item[wcont]
                          AND   b-ped-item.nr-pedcli = i-nr-pedcli 
                          AND   b-ped-item.nome-abrev = 
                                b-ped-venda.nome-abrev
                          AND   b-ped-venda.cod-estab = i-cod-estab
                          NO-LOCK:

          /* alterado aqui valeria - 16/08/10 - 206*/
            FIND FIRST item-cli WHERE  item.it-codigo   =  b-ped-item.it-codigo
                                AND   ITEM-cli.nome-abrev = b-ped-item.nome-abrev 
                                NO-LOCK NO-ERROR.
        IF NOT AVAILABLE item-cli THEN DO:
          MESSAGE "NAO FOI ENCONTRADO RELACIONAMENTO ITEM/CLIENTE DO CLIENTE"
          SKIP
          "REQUISICAO NAO PODE SER GERADA, FAVOR INSERIR O RELACIONAMENTO"
          VIEW-AS ALERT-BOX.
          END.
          
        ASSIGN wvl-preuni = 0.
        ASSIGN wvl-preuni = b-ped-item.vl-preuni.
        ASSIGN wsal-it = 0. 
        FOR EACH b-ped-ent OF b-ped-item NO-LOCK:
          IF b-ped-ent.cod-sit-ent = 1 
          or b-ped-ent.cod-sit-ent = 2 
          or b-ped-ent.cod-sit-ent = 4 THEN DO:
          ASSIGN wsal-it = (b-ped-ent.qt-pedida
                           - (b-ped-ent.qt-atendida 
                           - b-ped-ent.qt-pendente)).
          ASSIGN wsaldo = wsaldo + wsal-it.
          END.
      
          IF wsaldo GE t-quant[wcont] THEN DO:
            CREATE wsequencias.
            ASSIGN 
                   wsequencias.ww-nr-sequencia = b-ped-item.nr-sequencia
                   wsequencias.ww-it-codigo    = b-ped-item.it-codigo
                   wsequencias.ww-nr-pedcli    = b-ped-item.nr-pedcli
                   wsequencias.ww-qt-pedida    = b-ped-item.qt-pedida
                   wsequencias.ww-saldo        = wsal-it.
            END.
        END.
      END.
        
      /* Fim do calculo do saldo --------------------------------------*/
      IF t-quant[wcont] > wsaldo THEN DO:
        MESSAGE 
        "REQUISICAO NAO PODE SER GERADA PARA O ITEM:" t-item[wcont]
        SKIP "Item nao possui saldo suficiente para a quantidade digitada:"
        t-quant[wcont] "."
        SKIP "Saldo em aberto do pedido/item = " wsaldo
        VIEW-AS ALERT-BOX. 
        UNDO, RETRY.         
        END.
      IF t-item[wcont] <> "" then
        IF t-quant[wcont] = 0 THEN DO:
        MESSAGE "ITEM: " + STRING(t-item[wcont]) + "/QUANTIDADE NAO PODE SER 0"
        VIEW-AS ALERT-BOX.
        END.
        END.
    IF t-quant[wcont] LE wsaldo THEN DO:
      CREATE b-itens.
      ASSIGN b-itens.b-it           = t-item[wcont]
             b-itens.b-qt           = t-quant[wcont]
             b-itens.b-nr-pedcli    = i-nr-pedcli
             b-itens.b-cod-emitente = i-cod-emitente
             b-itens.b-nr-pedido    = b-ped-venda.nr-pedido
             b-itens.b-nat-operacao = b-ped-venda.nat-operacao
             b-itens.b-nome-abrev   = b-ped-venda.nome-abrev
             b-itens.b-vl-preuni    = wvl-preuni
             b-itens.b-emb          = i-wemb
             b-itens.b-qt-emb       = i-wqt-emb
             b-itens.b-req          = i-wreq
             b-itens.b-item-do-cli  = item-cli.item-do-cli.
      END.
    ASSIGN w-it-ant = t-item[wcont].
    END.
    /*RUN relatorio.*/
ENABLE bt-sair WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio Dialog-Frame 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    /*ASSIGN wrel = "V:\spool\SOL" + STRING(TIME,"999999") + ".TXT".
    OUTPUT TO VALUE(wrel) PAGE-SIZE 64.
    */
    FIND FIRST b-itens NO-LOCK NO-ERROR.
    IF NOT AVAILABLE b-itens THEN DO: 
      DISPLAY "ITENS INVALIDOS PARA A GERA��O DO ARQUIVO".
      NEXT.
      END.
    /*******************************FORMULARIO**********************************/
    FORM HEADER
    c-rodape6
    WITH FRAME f-desenv PAGE-BOTTOM WIDTH 150 NO-LABELS. 
    FORM HEADER 
    wlinha2
    SKIP(5)
    "********** INFORMACOES PARA EMBALAGEM E INSUMOS **********" AT 40      
    SKIP(1)
         c-rodape1 SKIP(1) c-rodape2 SKIP(1) c-rodape3 SKIP(1)
         c-rodape4 SKIP(1) c-rodape5 SKIP(1) 
    SKIP(1)
    c-rodape6
    /*
    wlinha1
    */
    WITH FRAME f-rodape  PAGE-BOTTOM WIDTH 150 NO-LABELS ROW 35.
   /****************************************************************************/
   /* Form itens ***************************************************************/ 
    FORM HEADER
      "         Item                Seq.        Qtd.   Descricao           It.Cli."
      "Preco unit.          Preco total"  AT 97
      /*wlinha1 */
      SKIP WITH FRAME f-itens WIDTH 150 DOWN.
    /***************************************************************************/
    VIEW FRAME f-cabec.   
    ASSIGN wemp = "SEEBER FASTPLAS LTDA.". 
    
    FOR EACH b-itens WHERE b-itens.b-it <> "" NO-LOCK:
     FIND FIRST item WHERE item.it-codigo = b-itens.b-it NO-LOCK NO-ERROR.
      /* FORMULARIO CABECALHO */
      FORM HEADER         
          wemp
          TODAY  FORMAT "99/99/9999" AT 70
          STRING(TIME,"HH:MM") "Hs"
          "Pagina"   AT 120 PAGE-NUMBER  FORMAT "ZZZ9"
          SKIP
          "SOLICITACAO DE EMISSAO DE NOTAS FISCAIS" AT 45 
          wlinha
          SKIP
          "Cliente: " b-ped-venda.nome-abrev
          "Codigo do Cliente:" AT 40 b-itens.b-cod-emitente
          "PEDIDO DO CLIENTE:" AT 72 b-itens.b-nr-pedcli  
          "   Pedido: " b-itens.b-nr-pedido
          SKIP
          "Transportadora:" 
          "N/N.F. no.:" AT 40
          "Nat.Operacao:" AT 78 b-itens.b-nat-operacao      
          wlinha1
          WITH FRAME f-cabec NO-LABELS WIDTH 150 PAGE-TOP.
                          
    ASSIGN 
    c-rodape1  = "Ins._______________________________________"
               + " Ins.________________________________________"
               + " Ins._______________________________________"
    c-rodape2  =  "Ins._______________________________________"
               + " Ins.________________________________________"
               + " Ins._______________________________________"
    c-rodape3  = "Baixa Estoque: Sim (  )  ou Nao (  )"
               + "    Disp./N.: _____________________________________" 
               + "_____________________________________________"
    c-rodape4  = "Observacao:" 
               + " ___________________________________________________"
               + " Disp. _______________"
               + " _____________________ Cx. ____________________"
    c-rodape5 = "Embalagem: " + STRING(i-wemb,"X(35)")
              + "  Qtd.: " + STRING(i-wqt-emb,"ZZZZZZ99")
              + "  Requisitante: " + STRING(i-wreq,"X(20)")
              + "   Data:  _______/_______/________"
    c-rodape6 = "Desenvolvido por Seeber Fastplas - Valeria - 09/2003".     

  /**********************************************************************/  
      ASSIGN wvl-total = 0.
      ASSIGN wvl-total = b-itens.b-qt * b-vl-preuni.
     
      
      FIND FIRST wsequencias WHERE wsequencias.ww-it-codigo = b-itens.b-it
                           NO-LOCK NO-ERROR.

      DISPLAY "****" AT 05 b-itens.b-it          
                           wsequencias.ww-nr-sequencia 
                           b-itens.b-qt      
                           item.descricao-1      
                           b-itens.b-item-do-cli
                           b-itens.b-vl-preuni   
                           wvl-total            
                           WITH FRAME f-itens WIDTH 150 NO-LABEL.
      DOWN WITH FRAME f-itens.
      
      /*  inibido display de todas as sequencias 
      FOR EACH wsequencias WHERE wsequencias.ww-it-codigo = b-itens.b-it
                           NO-LOCK:
        IF wsequencias.ww-saldo <> 0 THEN DO:
          DISPLAY  
                  SKIP WITH  WIDTH 150.
          DISPLAY "- Item -         - Ped.Cli. -" AT 48
                  "- Qt. Pedida -" AT 82
                  "- Saldo -"      AT 104
                  SKIP
                  "SEQUENCIA: " AT 25 wsequencias.ww-nr-sequencia NO-LABEL 
                  wsequencias.ww-it-codigo      
                  wsequencias.ww-nr-pedcli      
                  wsequencias.ww-qt-pedida      
                  wsequencias.ww-saldo          
                  WITH FRAME f-seq WIDTH 150 NO-LABEL.
          DOWN WITH FRAME f-seq NO-LABEL.
          DISPLAY wlinha2 NO-LABEL WITH WIDTH 150. 
          END.
        END.*/
      END.
      /* rodape */
      FIND FIRST b-itens NO-LOCK NO-ERROR.
      /*PAGE.
      VIEW FRAME f-rodape. */
    DISPLAY "" WITH FRAME f-rodape. 
      OUTPUT CLOSE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

