&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBulder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEF INPUT PARAMETER i-cod-emitente AS INTEGER FORMAT ">>>>>>>>9".
DEF INPUT PARAMETER i-nr-pedcli    AS CHAR    FORMAT "X(12)".
DEF INPUT PARAMETER i-wemb         AS CHAR    FORMAT "X(35)".
DEF INPUT PARAMETER i-wqt-emb      AS INTEGER FORMAT "ZZZZZZ99".
DEF INPUT PARAMETER i-wreq         AS CHAR    FORMAT "X(20)".
DEF INPUT PARAMETER i-cod-estab    AS CHAR    FORMAT "X(03)".
DEF INPUT PARAMETER i-nome-abrev   LIKE ped-venda.nome-abrev.
DEF INPUT PARAMETER i-nat-operacao LIKE ped-venda.nat-operacao.
DEF INPUT PARAMETER i-nr-pedido    LIKE ped-venda.nr-pedido.
DEF INPUT PARAMETER i-transp       AS CHAR FORMAT "x(40)".
DEF INPUT PARAMETER i-placa        AS CHAR FORMAT "x(15)".
DEF INPUT PARAMETER i-motorista    AS CHAR FORMAT "x(50)".
/* Local Variable Definitions ---                                       */


DEFINE WORKFILE b-itens 
       FIELD    b-it            AS CHAR FORMAT "X(16)" COLUMN-LABEL "Item"
       FIELD    b-qt            AS INTEGER FORMAT "ZZZZ,ZZ9"
                                COLUMN-LABEL "Quantidade"
       FIELD    b-nr-pedcli     LIKE ped-venda.nr-pedcli
       FIELD    b-cod-emitente  LIKE ped-venda.cod-emitente
       FIELD    b-nr-pedido     LIKE ped-venda.nr-pedido
       FIELD    b-nat-operacao  LIKE ped-venda.nat-operacao
       FIELD    b-nome-abrev    LIKE ped-venda.nome-abrev
       FIELD    b-vl-preuni     LIKE ped-item.vl-preuni
       FIELD    b-emb           AS CHAR FORMAT "X(35)"
       FIELD    b-qt-emb        AS INTE FORMAT "ZZZZZZ99"
       FIELD    b-req           AS CHAR FORMAT "X(20)"
       FIELD    b-item-do-cli   LIKE item-cli.item-do-cli
       FIELD    b-narrativa     LIKE item-cli.narrativa.
DEFINE WORKFILE wsequencias 
       FIELD    ww-nr-pedcli    LIKE ped-item.nr-pedcli
       FIELD    ww-nr-sequencia LIKE ped-item.nr-sequencia
       FIELD    ww-it-codigo    LIKE ped-item.it-codigo
       FIELD    ww-saldo        LIKE ped-item.qt-atendida COLUMN-LABEL "Saldo Item"
       FIELD    ww-qt-pedida    LIKE ped-item.qt-pedida.

DEFINE VARIABLE t-item          AS CHAR FORMAT "X(16)"    EXTENT 20. 
DEFINE variable t-quant         AS INTE FORMAT "ZZZZ,ZZ9" EXTENT 20.
DEFINE VARIABLE wvl-total   LIKE ped-item.vl-preuni.      
DEFINE VARIABLE wvl-preuni  LIKE ped-item.vl-preuni.       
DEFINE VARIABLE wsaldo      LIKE ped-item.qt-pedida.
DEFINE VARIABLE wsal-it     LIKE ped-item.qt-pedida COLUMN-LABEL "Saldo do item".
DEFINE VARIABLE wsal-atend  LIKE ped-item.qt-pedida.
DEFINE VARIABLE wit-ant     LIKE ped-item.it-codigo.

DEFINE VARIABLE wseqs       LIKE ped-item.nr-sequencia.


DEFINE VARIABLE c-rodape1   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape2   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape3   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape4   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape5   AS CHAR  FORMAT "X(148)".
DEFINE VARIABLE c-rodape6   AS CHAR  FORMAT "x(148)".
DEFINE VARIABLE c-rodape-transp   AS CHAR  FORMAT "x(148)".

DEFINE VARIABLE wped-fim    LIKE ped-venda.nr-pedcli.
DEFINE VARIABLE wdesc       LIKE item.descricao-1.
DEFINE VARIABLE wobs        AS CHAR FORMAT "X(100)".
DEFINE VARIABLE wlinha      AS CHAR FORMAT "X(148)".
DEFINE VARIABLE wlinha1     AS CHAR FORMAT "X(133)".
DEFINE VARIABLE wlinha2     AS CHAR FORMAT "X(133)".
DEFINE VARIABLE wlinha-item AS CHAR FORMAT "X(133)".
DEFINE VARIABLE wcont       AS INTE FORMAT "99".
DEFINE VARIABLE wcont1      AS INTE FORMAT "99".
DEFINE VARIABLE windex      AS INTE FORMAT "99".
DEFINE VARIABLE wcont2      AS INTE FORMAT "99".
DEFINE VARIABLE wvl-tot     AS DEC FORMAT ">>>,>>>,>>>,>>9.99".
DEFINE VARIABLE wtotal      AS DEC FORMAT ">>>,>>>,>>>,>>>,>>9.99".
DEFINE VARIABLE wrel        AS CHAR FORMAT "X(21)".
DEFINE VARIABLE w-it-ant    AS CHAR FORMAT "X(16)".
DEFINE VARIABLE wconf-imp   AS LOGICAL NO-UNDO.
DEFINE VARIABLE wemp        AS CHAR FORMAT "X(40)".
/* BUFFER */
/* antes 206
DEFINE BUFFER   b-ped-item  FOR ped-item.*/
DEFINE BUFFER   b-ped-item  FOR ped-item.
DEFINE BUFFER   b-ped-ent   FOR ped-ent.
DEFINE BUFFER   b-ped-venda FOR ped-venda.
DEFINE VARIABLE wtipo       AS INTEGER NO-UNDO.
DEFINE VARIABLE wrejeitado  AS LOGICAL INITIAL NO.

 
/* laser */
DEFINE VARIABLE wpaisagem AS CHAR.
DEFINE VARIABLE wnormaliza AS CHAR.
DEFINE VARIABLE wcomprime AS CHAR.
DEFINE VARIABLE a4 AS CHAR.



/*
DEFINE VARIABLE comprime-2 AS CHAR.
DEFINE VARIABLE comprime-3 AS CHAR.
DEFINE VARIABLE comprime-4 AS CHAR.
DEFINE VARIABLE wnormal AS CHAR.

DEFINE VARIABLE wnormal-1 AS CHAR.
DEFINE VARIABLE woficio2 AS CHAR.
DEFINE VARIABLE retrato AS CHAR.
DEFINE VARIABLE wpaisagem AS CHAR.
DEFINE VARIABLE expande AS CHAR.
DEFINE VARIABLE expande-1 AS CHAR.
DEFINE VARIABLE expande-2 AS CHAR.
*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-8 RECT-9 witem wquant BUTTON-8 witem-2 ~
wquant-2 BUTTON-9 witem-3 wquant-3 BUTTON-11 witem-4 wquant-4 BUTTON-10 ~
witem-5 wquant-5 BUTTON-12 witem-6 wquant-6 BUTTON-13 witem-7 wquant-7 ~
BUTTON-14 witem-8 wquant-8 BUTTON-15 witem-9 wquant-9 BUTTON-16 witem-10 ~
wquant-10 BUTTON-17 witem-11 wquant-11 BUTTON-18 witem-12 wquant-12 ~
BUTTON-19 witem-13 wquant-13 BUTTON-20 witem-14 wquant-14 BUTTON-21 ~
witem-15 wquant-15 BUTTON-22 witem-16 wquant-16 BUTTON-23 bt-rel Bt-CONSID ~
bt-arqcomp bt-sair 
&Scoped-Define DISPLAYED-OBJECTS witem wquant witem-2 wquant-2 witem-3 ~
wquant-3 witem-4 wquant-4 witem-5 wquant-5 witem-6 wquant-6 witem-7 ~
wquant-7 witem-8 wquant-8 witem-9 wquant-9 witem-10 wquant-10 witem-11 ~
wquant-11 witem-12 wquant-12 witem-13 wquant-13 witem-14 wquant-14 witem-15 ~
wquant-15 witem-16 wquant-16 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-arq AUTO-GO 
     LABEL "Arquivo Texto(desat)" 
     SIZE 16 BY 1.13 TOOLTIP "Arquivo texto - sem formata��o"
     BGCOLOR 8 FONT 1.

DEFINE BUTTON bt-arqcomp 
     LABEL "Arquivo c/compress�o" 
     SIZE 16 BY 1.

DEFINE BUTTON Bt-CONSID 
     LABEL "Considera��es" 
     SIZE 14 BY 1 TOOLTIP "Considera��es gerais"
     FONT 1.

DEFINE BUTTON bt-rel 
     IMAGE-UP FILE "image/im-cfprt.bmp":U
     LABEL "Relat�rio - Impressora" 
     SIZE 4 BY 1.13 TOOLTIP "Imprimir escolhendo a impressora"
     BGCOLOR 7 FGCOLOR 7 FONT 1.

DEFINE BUTTON bt-sair AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-10 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 10" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-11 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 11" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-12 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 12" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-13 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 13" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-14 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 14" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-15 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 15" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-16 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 16" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-17 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 17" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-18 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 18" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-19 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 19" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-20 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 20" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-21 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 21" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-22 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 22" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-23 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 23" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-8 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 8" 
     SIZE 3 BY .79.

DEFINE BUTTON BUTTON-9 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 9" 
     SIZE 3 BY .79.

DEFINE VARIABLE witem AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [1]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-10 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [10]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-11 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [11]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-12 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [12]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-13 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [13]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-14 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [14]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-15 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [15]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-16 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [16]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-2 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [2]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-3 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [3]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-4 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [4]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-5 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [5]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-6 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [6]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-7 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [7]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-8 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [8]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE witem-9 AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item [9]" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .75 NO-UNDO.

DEFINE VARIABLE wquant AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [1]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-10 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [10]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-11 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [11]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-12 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [12]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-13 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [13]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-14 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [14]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-15 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [15]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-16 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [16]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-2 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [2]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-3 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [3]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-4 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [4]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-5 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [5]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-6 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [6]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-7 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [7]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-8 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [8]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE VARIABLE wquant-9 AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Qtd. [9]" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .75 NO-UNDO.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 78 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 81 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     witem AT ROW 2.5 COL 21 COLON-ALIGNED
     wquant AT ROW 2.5 COL 53 COLON-ALIGNED
     BUTTON-8 AT ROW 2.5 COL 63
     witem-2 AT ROW 3.25 COL 21 COLON-ALIGNED
     wquant-2 AT ROW 3.25 COL 53 COLON-ALIGNED
     BUTTON-9 AT ROW 3.25 COL 63
     witem-3 AT ROW 4 COL 21 COLON-ALIGNED
     wquant-3 AT ROW 4 COL 53 COLON-ALIGNED
     BUTTON-11 AT ROW 4 COL 63
     witem-4 AT ROW 4.75 COL 21 COLON-ALIGNED
     wquant-4 AT ROW 4.75 COL 53 COLON-ALIGNED
     BUTTON-10 AT ROW 4.75 COL 63
     witem-5 AT ROW 5.5 COL 21 COLON-ALIGNED
     wquant-5 AT ROW 5.5 COL 53 COLON-ALIGNED
     BUTTON-12 AT ROW 5.5 COL 63
     witem-6 AT ROW 6.25 COL 21 COLON-ALIGNED
     wquant-6 AT ROW 6.25 COL 53 COLON-ALIGNED
     BUTTON-13 AT ROW 6.25 COL 63
     witem-7 AT ROW 7 COL 21 COLON-ALIGNED
     wquant-7 AT ROW 7 COL 53 COLON-ALIGNED
     BUTTON-14 AT ROW 7 COL 63
     witem-8 AT ROW 7.75 COL 21 COLON-ALIGNED
     wquant-8 AT ROW 7.75 COL 53 COLON-ALIGNED
     BUTTON-15 AT ROW 7.75 COL 63
     witem-9 AT ROW 8.5 COL 21 COLON-ALIGNED
     wquant-9 AT ROW 8.5 COL 53 COLON-ALIGNED
     BUTTON-16 AT ROW 8.5 COL 63
     witem-10 AT ROW 9.25 COL 21 COLON-ALIGNED
     wquant-10 AT ROW 9.25 COL 53 COLON-ALIGNED
     BUTTON-17 AT ROW 9.25 COL 63
     witem-11 AT ROW 10 COL 21 COLON-ALIGNED
     wquant-11 AT ROW 10 COL 53 COLON-ALIGNED
     BUTTON-18 AT ROW 10 COL 63
     bt-arq AT ROW 10.25 COL 66
     witem-12 AT ROW 10.75 COL 21 COLON-ALIGNED
     wquant-12 AT ROW 10.75 COL 53 COLON-ALIGNED
     BUTTON-19 AT ROW 10.75 COL 63
     witem-13 AT ROW 11.5 COL 21 COLON-ALIGNED
     wquant-13 AT ROW 11.5 COL 53 COLON-ALIGNED
     BUTTON-20 AT ROW 11.5 COL 63
     witem-14 AT ROW 12.25 COL 21 COLON-ALIGNED
     wquant-14 AT ROW 12.25 COL 53 COLON-ALIGNED
     BUTTON-21 AT ROW 12.25 COL 63
     witem-15 AT ROW 13 COL 21 COLON-ALIGNED
     wquant-15 AT ROW 13 COL 53 COLON-ALIGNED
     BUTTON-22 AT ROW 13 COL 63
     witem-16 AT ROW 13.75 COL 21 COLON-ALIGNED
     wquant-16 AT ROW 13.75 COL 53 COLON-ALIGNED
     BUTTON-23 AT ROW 13.75 COL 63
     bt-rel AT ROW 15.75 COL 8
     Bt-CONSID AT ROW 16 COL 26
     bt-arqcomp AT ROW 16 COL 48
     bt-sair AT ROW 16 COL 77
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 11 COL 42
     "................" VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 2.5 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 4.25 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 6.5 COL 42
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 12.5 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 8 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 7.25 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 5.75 COL 42
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 14 COL 42
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 9.5 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 5 COL 42
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 10.25 COL 42
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 11.75 COL 42
     "........." VIEW-AS TEXT
          SIZE 5 BY .67 AT ROW 8.75 COL 42
     "................" VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 3.5 COL 42
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         CANCEL-BUTTON bt-sair.

/* DEFINE FRAME statement is approaching 4K Bytes.  Breaking it up   */
DEFINE FRAME Dialog-Frame
     "........." VIEW-AS TEXT
          SIZE 4 BY .67 AT ROW 13.25 COL 42
     "Imprime Relat�rio" VIEW-AS TEXT
          SIZE 13 BY .54 AT ROW 16.25 COL 13
     RECT-8 AT ROW 1.25 COL 3
     RECT-9 AT ROW 15 COL 2
     SPACE(0.56) SKIP(1.62)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "ITENS - Solicita��o de Nota Fiscal  - wpf0106a.w"
         CANCEL-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON bt-arq IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       bt-arq:HIDDEN IN FRAME Dialog-Frame           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* ITENS - Solicita��o de Nota Fiscal  - wpf0106a.w */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-arq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arq Dialog-Frame
ON CHOOSE OF bt-arq IN FRAME Dialog-Frame /* Arquivo Texto(desat) */
DO:
  DISABLE bt-sair bt-consid /*bt-arq*/ bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  ASSIGN wrel = "V:\spool\SOL" + STRING(TIME,"999999") + ".TXT".
  OUTPUT TO VALUE(wrel).
  RUN relatorio.
  OUTPUT CLOSE.
  ENABLE  bt-sair bt-consid /*bt-arq*/ bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
 /* MESSAGE "GERA��O DO RELAT�RIO CONCLU�DA."
           SKIP "NOME DO RELATORIO: " wrel
           VIEW-AS ALERT-BOX WARNING.*/

  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-arqcomp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arqcomp Dialog-Frame
ON CHOOSE OF bt-arqcomp IN FRAME Dialog-Frame /* Arquivo c/compress�o */
DO:
  DISABLE bt-sair bt-consid /*bt-arq*/ bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  ASSIGN wrel = "V:\spool\SOL" + STRING(TIME,"999999") + ".TXT".
  RUN comprime-arquivo.
  ENABLE  bt-sair bt-consid /*bt-arq*/ bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  MESSAGE "Nome do arquivo gerado: " wrel VIEW-AS ALERT-BOX.
 /* DOS SILENT NOTEPAD.EXE VALUE(wrel). */
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Bt-CONSID
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Bt-CONSID Dialog-Frame
ON CHOOSE OF Bt-CONSID IN FRAME Dialog-Frame /* Considera��es */
DO:
  RUN consid.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-rel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-rel Dialog-Frame
ON CHOOSE OF bt-rel IN FRAME Dialog-Frame /* Relat�rio - Impressora */
DO:
  DISABLE  bt-sair bt-consid /*bt-arq*/ bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
  IF wconf-imp THEN DO:
    OUTPUT TO PRINTER PAGE-SIZE 60.

    /* para pegar comandos pcl5 - era utilizados para matricial e hp
    {wpsf\utilitarios\imp.p}
    PUT CONTROL wnormaliza wcomprime a4 . */

    /* PCL5 PARA LASER 
    ASSIGN wcomprime = CHR(27) + "(s0p18h0s3b4102T".
    ASSIGN comprime-2 = CHR(27) + "(s0p23h0s3b4102T".
    ASSIGN comprime-4 = CHR(27) + "(s0p20h0s3b4102T".
    ASSIGN A4 = CHR(27) + "&l26a".
    ASSIGN wnormal = CHR(27) + "(s0p11h0s3b4102T".
    ASSIGN wnormal-1 = CHR(27) + "(s0p15h0s3b4102T".
    ASSIGN woficio2 = CHR(27) + "&l3001a".

    ASSIGN retrato = CHR(27) + CHR(38) + CHR(108) + CHR(48) + CHR(79).
    /*ASSIGN paisagem = CHR(27) + CHR(38) + CHR(108) + CHR(49) + CHR(79).*/
    ASSIGN wpaisagem = CHR(27) + CHR(38) + CHR(108) + CHR(49) + CHR(79).
    ASSIGN expande = CHR(27) + "(12u" + CHR(27) + "(s0p13h0s3b4102T".
    ASSIGN expande-1 = CHR(27) + "(12u" + CHR(27) + "(s0p11h0s3b4102T".
    ASSIGN expande-2 = CHR(27) + "(12u" + CHR(27) + "(s0p9h0s3b4102T".
    ASSIGN b-curta = CHR(27) + "&l0S".
    PUT CONTROL A4 wnormal wcomprime.
   */

   /* Comando para compress�o hp e laser */ 
 
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN wnormaliza = "~033~046~153~060~123".   /* NORMALIZA */
    ASSIGN A4 = CHR(27) + "&l26A".
    PUT CONTROL A4 wnormaliza wcomprime.

    RUN relatorio.
    END.
  ENABLE bt-sair bt-consid /*bt-arq*/ bt-rel bt-arqcomp WITH FRAME {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-10 Dialog-Frame
ON CHOOSE OF BUTTON-10 IN FRAME Dialog-Frame /* Button 10 */
DO:
    ASSIGN wsal-it = 0
          wcont   = 4.
    ASSIGN witem-4 wquant-4.
    ASSIGN  t-item[wcont]    =  witem-4
            t-quant[wcont]   =  wquant-4.
    IF t-item[wcont] = "" THEN NEXT.
    IF t-item[wcont] <> " " THEN DO:
    RUN calcula.
    IF wrejeitado = YES THEN DO:
      ASSIGN witem-4 = "" wquant-4 = 0.
      DISPLAY witem-4 wquant-4 WITH FRAME  {&FRAME-NAME}.
      END.
    DISABLE witem-4 wquant-4 button-10 WITH FRAME {&FRAME-NAME}.
    ENABLE witem-5 wquant-5   button-12 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-11 Dialog-Frame
ON CHOOSE OF BUTTON-11 IN FRAME Dialog-Frame /* Button 11 */
DO :
   ASSIGN wsal-it = 0
        wcont   = 3.
   ASSIGN witem-3 wquant-3.
   ASSIGN  t-item[wcont]    =  witem-3
           t-quant[wcont]   =  wquant-3.
   IF t-item[wcont] = "" THEN NEXT.
   IF t-item[wcont] <> " " THEN DO:
   RUN calcula.
   IF wrejeitado = YES THEN DO:
     ASSIGN witem-3 = "" wquant-3 = 0.
     DISPLAY witem-3 wquant-3 WITH FRAME  {&FRAME-NAME}.
     END.
   DISABLE witem-3 wquant-3 BUTTON-11 WITH FRAME {&FRAME-NAME}.
   ENABLE witem-4 wquant-4 button-10 WITH FRAME {&FRAME-NAME}.
   END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-12
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-12 Dialog-Frame
ON CHOOSE OF BUTTON-12 IN FRAME Dialog-Frame /* Button 12 */
DO:
   ASSIGN wsal-it = 0
          wcont   = 5.
   ASSIGN witem-5 wquant-5.
   ASSIGN  t-item[wcont]    =  witem-5
           t-quant[wcont]   =  wquant-5.
   IF t-item[wcont] = "" THEN NEXT.
   IF t-item[wcont] <> " " THEN DO:
   RUN calcula.
   IF wrejeitado = YES THEN DO:
     ASSIGN witem-5 = "" wquant-5 = 0.
     DISPLAY witem-5 wquant-5 WITH FRAME  {&FRAME-NAME}.
     END.
   DISABLE witem-5 wquant-5   button-12 WITH FRAME {&FRAME-NAME}.
   ENABLE witem-6 wquant-6  button-13 WITH FRAME {&FRAME-NAME}.
   END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-13
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-13 Dialog-Frame
ON CHOOSE OF BUTTON-13 IN FRAME Dialog-Frame /* Button 13 */
DO:
   ASSIGN wsal-it = 0
          wcont   = 6.
   ASSIGN witem-6 wquant-6.
   ASSIGN  t-item[wcont]    =  witem-6
           t-quant[wcont]   =  wquant-6.
   IF t-item[wcont] = "" THEN NEXT.
   IF t-item[wcont] <> " " THEN DO:
   RUN calcula.
   IF wrejeitado = YES THEN DO:
     ASSIGN witem-6 = "" wquant-6 = 0.
     DISPLAY witem-6 wquant-6 WITH FRAME  {&FRAME-NAME}.
   END.
   DISABLE witem-6 wquant-6  button-13 WITH FRAME {&FRAME-NAME}.
   ENABLE witem-7 wquant-7  button-14 WITH FRAME {&FRAME-NAME}.
   END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-14
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-14 Dialog-Frame
ON CHOOSE OF BUTTON-14 IN FRAME Dialog-Frame /* Button 14 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 7.
 ASSIGN witem-7 wquant-7.
 ASSIGN  t-item[wcont]    =  witem-7    
         t-quant[wcont]   =  wquant-7.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-7 = "" wquant-7 = 0.
   DISPLAY witem-7 wquant-7 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-7 wquant-7  button-14 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-8 wquant-8 button-15 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-15
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-15 Dialog-Frame
ON CHOOSE OF BUTTON-15 IN FRAME Dialog-Frame /* Button 15 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 8.
 ASSIGN witem-8 wquant-8.
 ASSIGN  t-item[wcont]    =  witem-8
         t-quant[wcont]   =  wquant-8.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
IF wrejeitado = YES THEN DO:
   ASSIGN witem-8 = "" wquant-8 = 0.
   DISPLAY witem-8 wquant-8 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-8 wquant-8 button-15 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-9 wquant-9   button-16 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-16
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-16 Dialog-Frame
ON CHOOSE OF BUTTON-16 IN FRAME Dialog-Frame /* Button 16 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 9.
 ASSIGN witem-9  wquant-9.
 ASSIGN  t-item[wcont]    =  witem-9
         t-quant[wcont]   =  wquant-9.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-9 = "" wquant-9 = 0.
   DISPLAY witem-9 wquant-9 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-9 wquant-9   button-16 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-10 wquant-10  button-17 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-17
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-17 Dialog-Frame
ON CHOOSE OF BUTTON-17 IN FRAME Dialog-Frame /* Button 17 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 10.
 ASSIGN witem-10 wquant-10.
 ASSIGN  t-item[wcont]    =  witem-10
         t-quant[wcont]   =  wquant-10.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-10 = "" wquant-10 = 0.
   DISPLAY witem-10 wquant-10 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-10 wquant-10  button-17 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-11 wquant-11  button-18 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-18
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-18 Dialog-Frame
ON CHOOSE OF BUTTON-18 IN FRAME Dialog-Frame /* Button 18 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 11.
 ASSIGN witem-11 wquant-11.
 ASSIGN  t-item[wcont]    =  witem-11
         t-quant[wcont]   =  wquant-11.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-11 = "" wquant-11 = 0.
   DISPLAY witem-11 wquant-11 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE  witem-11 wquant-11  button-18 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-12 wquant-12  button-19 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-19
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-19 Dialog-Frame
ON CHOOSE OF BUTTON-19 IN FRAME Dialog-Frame /* Button 19 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 12.
 ASSIGN witem-12 wquant-12.
 ASSIGN  t-item[wcont]    =  witem-12
         t-quant[wcont]   =  wquant-12.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-12 = "" wquant-12 = 0.
   DISPLAY witem-12 wquant-12 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-12 wquant-12  button-19 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-13 wquant-13  button-20 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-20
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-20 Dialog-Frame
ON CHOOSE OF BUTTON-20 IN FRAME Dialog-Frame /* Button 20 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 13.
 ASSIGN witem-13 wquant-13.
 ASSIGN  t-item[wcont]    =  witem-13
         t-quant[wcont]   =  wquant-13.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-13 = "" wquant-13 = 0.
   DISPLAY witem-13 wquant-13 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-13 wquant-13  button-20 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-14 wquant-14  button-21 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-21
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-21 Dialog-Frame
ON CHOOSE OF BUTTON-21 IN FRAME Dialog-Frame /* Button 21 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 14.
 ASSIGN witem-14 wquant-14.
 ASSIGN  t-item[wcont]    =  witem-14
         t-quant[wcont]   =  wquant-14.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-14 = "" wquant-14 = 0.
   DISPLAY witem-14 wquant-14 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-14 wquant-14  button-21 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-15 wquant-15 button-22 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-22
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-22 Dialog-Frame
ON CHOOSE OF BUTTON-22 IN FRAME Dialog-Frame /* Button 22 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 15.
 ASSIGN witem-15 wquant-15.
 ASSIGN  t-item[wcont]    =  witem-15
         t-quant[wcont]   =  wquant-15.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-15 = "" wquant-15 = 0.
   DISPLAY witem-15 wquant-15 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-15 wquant-15 button-22 WITH FRAME {&FRAME-NAME}.
 ENABLE witem-16 wquant-16  button-23 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-23
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-23 Dialog-Frame
ON CHOOSE OF BUTTON-23 IN FRAME Dialog-Frame /* Button 23 */
DO:
 ASSIGN wsal-it = 0
        wcont   = 16.
 ASSIGN witem-16 wquant-16.
 ASSIGN  t-item[wcont]    =  witem-16
         t-quant[wcont]   =  wquant-16.
 IF t-item[wcont] = "" THEN NEXT.
 IF t-item[wcont] <> " " THEN DO:
 RUN calcula.
 IF wrejeitado = YES THEN DO:
   ASSIGN witem-16 = "" wquant-16 = 0.
   DISPLAY witem-16 wquant-16 WITH FRAME  {&FRAME-NAME}.
   END.
 DISABLE witem-16 wquant-16  button-23 WITH FRAME {&FRAME-NAME}.
 ENABLE  button-8 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-8 Dialog-Frame
ON CHOOSE OF BUTTON-8 IN FRAME Dialog-Frame /* Button 8 */
DO:
  ASSIGN wsal-it = 0
         wcont   = 1.
  ASSIGN witem wquant.
  ASSIGN  t-item[wcont]    =  witem
          t-quant[wcont]   =  wquant.
  IF t-item[wcont] = "" THEN NEXT.
  IF t-item[wcont] <> " " THEN DO:
  RUN calcula.
  IF wrejeitado = YES THEN DO:
    ASSIGN witem = "" wquant = 0.
    DISPLAY witem wquant WITH FRAME  {&FRAME-NAME}.
    END.
  DISABLE witem wquant button-8 WITH FRAME  {&FRAME-NAME}.
  ENABLE witem-2 wquant-2 button-9 WITH FRAME {&FRAME-NAME}.
 END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-9 Dialog-Frame
ON CHOOSE OF BUTTON-9 IN FRAME Dialog-Frame /* Button 9 */
DO:
   ASSIGN wsal-it = 0
        wcont   = 2.
   ASSIGN witem-2 wquant-2.
   ASSIGN  t-item[wcont]    =  witem-2
         t-quant[wcont]   =  wquant-2.
   IF t-item[wcont] = "" THEN NEXT.
   IF t-item[wcont] <> " " THEN DO:
   RUN calcula.
   IF wrejeitado = YES THEN DO:
     ASSIGN witem-2 = "" wquant-2 = 0.
     DISPLAY witem-2 wquant-2 WITH FRAME  {&FRAME-NAME}.
     END.
   DISABLE witem-2 wquant-2  button-9 WITH FRAME {&FRAME-NAME}.
   ENABLE witem-3 wquant-3  button-11 WITH FRAME {&FRAME-NAME}.
   END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE calcula Dialog-Frame 
PROCEDURE calcula :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*  Calcula Saldo */  
    ASSIGN   wsaldo  = 0
             wsal-it = 0
             wrejeitado   = NO.
    IF t-item[wcont] = "" THEN NEXT.
    IF t-item[wcont] <> " " THEN DO:
      /* Checa se o codigo do item existe ------------------------*/
    FIND FIRST b-ped-item    WHERE b-ped-item.it-codigo = t-item[wcont]
                             AND   b-ped-item.nr-pedcli = i-nr-pedcli                   
                             AND   b-ped-item.nome-abrev = i-nome-abrev
                             AND   b-ped-item.nr-sequencia GE 0
                             AND (cod-refer = ""
                             OR  cod-refer <> "")
                             USE-INDEX ch-item-ped NO-LOCK NO-ERROR.
      
      /* MESSAGE "item1 "  t-item[wcont] "pedido" i-nr-pedcli 
        "nome" i-nome-abrev "sequencia" b-ped-item.nr-sequencia 
        "codigo refer" cod-refer
        VIEW-AS ALERT-BOX.*/

      IF NOT AVAILABLE b-ped-item THEN DO: 
        MESSAGE 
        "ITEM: " t-item[wcont] SKIP
        "NAO ENCONTRADO NO PEDIDO." /*b-ped-item.nr-pedcli */
        SKIP "ITEM REJEITADO!!!"
        VIEW-AS ALERT-BOX.
        ASSIGN wrejeitado = YES.
        END.
      /* Calcula saldo ------------------------------------------------*/
      
      FOR EACH b-ped-item WHERE b-ped-item.it-codigo = t-item[wcont]
                          AND   b-ped-item.nr-pedcli = i-nr-pedcli 
                          AND   b-ped-item.nome-abrev = i-nome-abrev
                          AND   b-ped-item.nr-sequencia GE 0
                          AND   (cod-refer = ""
                          OR     cod-refer <> "") 
                          USE-INDEX ch-cli-ped NO-LOCK:
        
   /* MESSAGE "item2 " b-ped-item.it-codigo VIEW-AS ALERT-BOX.*/
               
       /*  /* alterado aqui valeria - 16/08/10 - 206*/
            FIND FIRST item-cli WHERE  item.it-codigo   =  b-ped-item.it-codigo
                                AND   ITEM-cli.nome-abrev = b-ped-item.nome-abrev 
                                NO-LOCK NO-ERROR.
               
         */

         
         FIND FIRST item-cli WHERE item-cli.it-codigo  =  b-ped-item.it-codigo
                             AND   item-cli.nome-abrev =  b-ped-item.nome-abrev
                             USE-INDEX ch-cli-item NO-LOCK NO-ERROR.
             
              
         /*
        FIND FIRST item-cli WHERE  item.it-codigo     BEGINS  t-item[wcont]
                            AND   ITEM-cli.nome-abrev BEGINS  i-nome-abrev
                            NO-LOCK.
        */
        IF NOT AVAILABLE item-cli THEN DO:
          MESSAGE "Item: " t-item[wcont] "Nome: " i-nome-abrev
          SKIP 
          "NAO FOI ENCONTRADO RELACIONAMENTO ITEM/CLIENTE DO CLIENTE."
          SKIP
          "REQUISICAO NAO PODE SER GERADA, FAVOR INSERIR O RELACIONAMENTO."
          SKIP "ITEM REJEITADO!!!"
          VIEW-AS ALERT-BOX.
          ASSIGN wrejeitado = YES.
          END.
          
        ASSIGN wvl-preuni = 0.
        ASSIGN wvl-preuni = b-ped-item.vl-preuni.
        ASSIGN wsal-it = 0. 
        FOR EACH b-ped-ent OF b-ped-item NO-LOCK:
          IF b-ped-ent.cod-sit-ent = 1 
          or b-ped-ent.cod-sit-ent = 2 
          or b-ped-ent.cod-sit-ent = 4 THEN DO:
          ASSIGN wsal-it = (b-ped-ent.qt-pedida
                           - (b-ped-ent.qt-atendida 
                           - b-ped-ent.qt-pendente)).
          ASSIGN wsaldo = wsaldo + wsal-it.
          END.
        END.
          IF wsaldo GE t-quant[wcont] THEN DO:
            CREATE wsequencias.
            ASSIGN 
                   wsequencias.ww-nr-sequencia = b-ped-item.nr-sequencia
                   wsequencias.ww-it-codigo    = b-ped-item.it-codigo
                   wsequencias.ww-nr-pedcli    = b-ped-item.nr-pedcli
                   wsequencias.ww-qt-pedida    = b-ped-item.qt-pedida
                   wsequencias.ww-saldo        = wsal-it.
            END.
      END.
      /* Fim do calculo do saldo --------------------------------------*/
      IF t-quant[wcont] > wsaldo THEN DO:
        MESSAGE 
        "REQUISICAO NAO PODE SER GERADA PARA O ITEM:" t-item[wcont]
        SKIP "Item nao possui saldo suficiente para a quantidade digitada:"
        t-quant[wcont] "."
        SKIP "Saldo em aberto do pedido/item = " wsaldo
        SKIP "ITEM REJEITADO!!!"
        VIEW-AS ALERT-BOX. 
        ASSIGN wrejeitado = YES.
        END.
      /* se o item for diferente de branco */
      IF t-item[wcont] = "" THEN ASSIGN wrejeitado = YES.
      IF t-item[wcont] <> "" THEN DO:
        IF t-quant[wcont] = 0 THEN DO:
          MESSAGE "ITEM: " + STRING(t-item[wcont]) + " - QUANTIDADE NAO PODE SER 0."
          SKIP "ITEM REJEITADO!!!"
          VIEW-AS ALERT-BOX.
          ASSIGN wrejeitado = YES.
          END.
       
     
    IF t-quant[wcont] LE wsaldo THEN DO:
      CREATE b-itens.
      ASSIGN b-itens.b-it           = t-item[wcont]
             b-itens.b-qt           = t-quant[wcont]
             b-itens.b-nr-pedcli    = i-nr-pedcli
             b-itens.b-cod-emitente = i-cod-emitente
             b-itens.b-nr-pedido    = i-nr-pedido
             b-itens.b-nat-operacao = i-nat-operacao
             b-itens.b-nome-abrev   = i-nome-abrev
             b-itens.b-vl-preuni    = wvl-preuni
             b-itens.b-emb          = i-wemb
             b-itens.b-qt-emb       = i-wqt-emb
             b-itens.b-req          = i-wreq
             b-itens.b-item-do-cli  = item-cli.item-do-cli
             b-itens.b-narrativa      = SUBSTRING(item-cli.narrativa,1,40).
      END.
     ASSIGN w-it-ant = t-item[wcont].
     END.
    END.
 
  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo Dialog-Frame 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
DEFINE VARIABLE a4 AS CHAR.
DEFINE VARIABLE wnormaliza AS CHAR.   /* NORMALIZA */

RUN wpsf/wp9902.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    ASSIGN wnormaliza  = CHR(18).
    END.
/* HP */
IF wtipo = 2 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN wnormaliza = "~033~046~153~060~123".   /* NORMALIZA */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
/* HP */
IF wtipo = 3 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN wnormaliza = "~033~046~153~060~123".   /* NORMALIZA */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.

OUTPUT TO value(wrel) page-size 44.
PUT CONTROL wnormaliza wcomprime /*wpaisagem*/ a4. 
RUN relatorio.                 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consid Dialog-Frame 
PROCEDURE consid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
MESSAGE 
     "****** NOVAS INSTRU��ES ******" 
     SKIP "1) Quando o item e a quantidade for branco o programa n�o aceitar� a linha,"
     SKIP "   devendo ser digitado um ITEM v�lido com QUANTIDADE."
     SKIP "2) Se for digitado um item com quantidade zero, a linha ser� rejeitada."
     SKIP "3) Se a quantidade for superior ao saldo do pedido, a linha ser� rejeitada."
     SKIP "4) Quando o item n�o for encontrado no pedido ou n�o existir, a linha ser� rejeitada."
     VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY witem wquant witem-2 wquant-2 witem-3 wquant-3 witem-4 wquant-4 
          witem-5 wquant-5 witem-6 wquant-6 witem-7 wquant-7 witem-8 wquant-8 
          witem-9 wquant-9 witem-10 wquant-10 witem-11 wquant-11 witem-12 
          wquant-12 witem-13 wquant-13 witem-14 wquant-14 witem-15 wquant-15 
          witem-16 wquant-16 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-8 RECT-9 witem wquant BUTTON-8 witem-2 wquant-2 BUTTON-9 witem-3 
         wquant-3 BUTTON-11 witem-4 wquant-4 BUTTON-10 witem-5 wquant-5 
         BUTTON-12 witem-6 wquant-6 BUTTON-13 witem-7 wquant-7 BUTTON-14 
         witem-8 wquant-8 BUTTON-15 witem-9 wquant-9 BUTTON-16 witem-10 
         wquant-10 BUTTON-17 witem-11 wquant-11 BUTTON-18 witem-12 wquant-12 
         BUTTON-19 witem-13 wquant-13 BUTTON-20 witem-14 wquant-14 BUTTON-21 
         witem-15 wquant-15 BUTTON-22 witem-16 wquant-16 BUTTON-23 bt-rel 
         Bt-CONSID bt-arqcomp bt-sair 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia Dialog-Frame 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DISABLE ALL EXCEPT  bt-sair bt-rel bt-consid bt-arq
                      bt-arqcomp WITH FRAME   {&FRAME-NAME}.
  ASSIGN wdesc          = " "
         wobs           = FILL("_",81)
         wlinha         = FILL("_",132)
         wlinha1        = FILL("_",132)
         wlinha2        = FILL("-",132)
         wlinha-item    = FILL("=",132)
         wcont          = 0
         wcont1         = 0
         wvl-tot        = 0
         wtotal         = 0.
  ASSIGN  t-item[1]  = ""
         t-quant[1]  =  0
          t-item[2]  =  ""
         t-quant[2]  =  0
          t-item[3]  =  ""
         t-quant[3]  =  0 
          t-item[4]  =  ""
         t-quant[4]  =  0
          t-item[5]  =  ""
         t-quant[5]  =  0
          t-item[6]  =  ""
         t-quant[6]  =  0
          t-item[7]  =  ""
         t-quant[7]  =  0
          t-item[8]  =  ""
         t-quant[8]  =  0  
          t-item[9]  =  ""
         t-quant[9]  =  0
          t-item[10] =  ""
         t-quant[10] =  0
          t-item[11] =  ""
         t-quant[11] =  0
          t-item[12] =  ""
         t-quant[12] =  0
          t-item[13] =  ""
         t-quant[13] =  0
          t-item[14] =  ""
         t-quant[14] =  0
          t-item[15] =  ""
         t-quant[15] =  0
          t-item[16] =  ""
         t-quant[16] =  0.
  FOR EACH b-itens:
    DELETE  b-itens.
    END.
  FOR EACH wsequencias:
    DELETE wsequencias.
    END.
    /*
    FIND FIRST b-ped-venda WHERE (b-ped-venda.cod-sit-ped <> 3
                               AND b-ped-venda.cod-sit-ped <> 5
                               AND b-ped-venda.cod-sit-ped <> 6)
                               AND b-ped-venda.nr-pedcli    = i-nr-pedcli 
                               AND b-ped-venda.cod-emitente = i-cod-emitente
                               AND b-ped-venda.cod-estab    = i-cod-estab
                               AND(b-ped-venda.nome-abrev  <> ""
                               OR  b-ped-venda.nome-abrev  = "")
                               USE-INDEX ch-rep-cli NO-LOCK NO-ERROR.
    */
    ENABLE witem wquant button-8 WITH FRAME   {&FRAME-NAME}.
    END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ITENS Dialog-Frame 
PROCEDURE ITENS :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 RUN inicia.
 DO wcont = 1 TO 20 ON ERROR UNDO, RETRY:
 ASSIGN wsal-it = 0.
    IF t-item[wcont] = "" THEN NEXT.
    /*
    IF t-item[wcont] = w-it-ant THEN NEXT.
    */
    IF t-item[wcont] <> " " THEN DO:
    /*  Calcula Saldo */  
      FIND FIRST b-ped-venda WHERE (b-ped-venda.cod-sit-ped <> 3
                             AND b-ped-venda.cod-sit-ped <> 5
                             AND b-ped-venda.cod-sit-ped <> 6)
                             AND b-ped-venda.nr-pedcli    = i-nr-pedcli 
                             AND b-ped-venda.cod-emitente = i-cod-emitente
                             AND b-ped-venda.cod-estab    = i-cod-estab
                             NO-LOCK NO-ERROR.
      
      IF NOT AVAILABLE b-ped-venda THEN MESSAGE
      "Estab.:" i-cod-estab "Pedido:" i-nr-pedcli "Cliente:" i-cod-emitente
      "nao encontrado" VIEW-AS ALERT-BOX.
      ASSIGN wsaldo  = 0
             wsal-it = 0.
      /* Checa se o codigo do item existe ------------------------*/
      FIND FIRST b-ped-item  WHERE b-ped-item.it-codigo = t-item[wcont]
                             AND   b-ped-item.nr-pedcli = i-nr-pedcli                   
                             AND   b-ped-item.nome-abrev = 
                                   b-ped-venda.nome-abrev
                             AND   b-ped-venda.cod-estab = i-cod-estab
                             NO-LOCK NO-ERROR.
      IF NOT AVAILABLE b-ped-item THEN DO: 
        MESSAGE 
        "ITEM: " t-item[wcont] SKIP
        "NAO ENCONTRADO NO PEDIDO: " b-ped-venda.nr-pedcli 
        VIEW-AS ALERT-BOX.
        ASSIGN t-item[wcont] = ""
               t-quant[wcont] = 0.
        END.
      /* Calcula saldo ------------------------------------------------*/
      FOR EACH b-ped-item WHERE b-ped-item.it-codigo = t-item[wcont]
                          AND   b-ped-item.nr-pedcli = i-nr-pedcli 
                          AND   b-ped-item.nome-abrev = 
                                b-ped-venda.nome-abrev
                          AND   b-ped-venda.cod-estab = i-cod-estab
                          NO-LOCK:
        /*FIND FIRST item-cli OF b-ped-item NO-LOCK NO-ERROR.*/
                    /* alterado aqui valeria - 16/08/10 - 206*/
      FIND FIRST item-cli WHERE  item.it-codigo   =  b-ped-item.it-codigo
                          AND   ITEM-cli.nome-abrev = b-ped-item.nome-abrev 
                          NO-LOCK NO-ERROR.





        IF NOT AVAILABLE item-cli THEN DO:
          MESSAGE "NAO FOI ENCONTRADO RELACIONAMENTO ITEM/CLIENTE DO CLIENTE"
          SKIP
          "REQUISICAO NAO PODE SER GERADA, FAVOR INSERIR O RELACIONAMENTO"
          VIEW-AS ALERT-BOX.
          END.
          
        ASSIGN wvl-preuni = 0.
        ASSIGN wvl-preuni = b-ped-item.vl-preuni.
        ASSIGN wsal-it = 0. 
        FOR EACH b-ped-ent OF b-ped-item NO-LOCK:
          IF b-ped-ent.cod-sit-ent = 1 
          or b-ped-ent.cod-sit-ent = 2 
          or b-ped-ent.cod-sit-ent = 4 THEN DO:
          ASSIGN wsal-it = (b-ped-ent.qt-pedida
                           - (b-ped-ent.qt-atendida 
                           - b-ped-ent.qt-pendente)).
          ASSIGN wsaldo = wsaldo + wsal-it.
          END.
      
          IF wsaldo GE t-quant[wcont] THEN DO:
            CREATE wsequencias.
            ASSIGN 
                   wsequencias.ww-nr-sequencia = b-ped-item.nr-sequencia
                   wsequencias.ww-it-codigo    = b-ped-item.it-codigo
                   wsequencias.ww-nr-pedcli    = b-ped-item.nr-pedcli
                   wsequencias.ww-qt-pedida    = b-ped-item.qt-pedida
                   wsequencias.ww-saldo        = wsal-it.
            END.
        END.
      END.
        
      /* Fim do calculo do saldo --------------------------------------*/
      IF t-quant[wcont] > wsaldo THEN DO:
        MESSAGE 
        "REQUISICAO NAO PODE SER GERADA PARA O ITEM:" t-item[wcont]
        SKIP "Item nao possui saldo suficiente para a quantidade digitada:"
        t-quant[wcont] "."
        SKIP "Saldo em aberto do pedido/item = " wsaldo
        VIEW-AS ALERT-BOX. 
        UNDO, RETRY.         
        END.
      IF t-item[wcont] <> "" then
        IF t-quant[wcont] = 0 THEN DO:
        MESSAGE "ITEM: " + STRING(t-item[wcont]) + "/QUANTIDADE NAO PODE SER 0"
        VIEW-AS ALERT-BOX.
        END.
        END.
    IF t-quant[wcont] LE wsaldo THEN DO:
      CREATE b-itens.
      ASSIGN b-itens.b-it           = t-item[wcont]
             b-itens.b-qt           = t-quant[wcont]
             b-itens.b-nr-pedcli    = i-nr-pedcli
             b-itens.b-cod-emitente = i-cod-emitente
             b-itens.b-nr-pedido    = b-ped-venda.nr-pedido
             b-itens.b-nat-operacao = b-ped-venda.nat-operacao
             b-itens.b-nome-abrev   = b-ped-venda.nome-abrev
             b-itens.b-vl-preuni    = wvl-preuni
             b-itens.b-emb          = i-wemb
             b-itens.b-qt-emb       = i-wqt-emb
             b-itens.b-req          = i-wreq
             b-itens.b-item-do-cli  = item-cli.item-do-cli.
      END.
    ASSIGN w-it-ant = t-item[wcont].
    END.
    /*RUN relatorio.*/
ENABLE bt-sair WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio Dialog-Frame 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   
    FIND FIRST b-itens NO-LOCK NO-ERROR.
    IF NOT AVAILABLE b-itens THEN DO: 
      DISPLAY "ITENS INVALIDOS PARA A GERA��O DO ARQUIVO".
      NEXT.
      END.
    /*******************************FORMULARIO**********************************/
    FORM HEADER
    c-rodape6
    WITH FRAME f-desenv PAGE-BOTTOM WIDTH 150 NO-LABELS. 
    FORM HEADER 
    wlinha2
    skip(1) "******* INFORMACOES PARA EMBALAGEM E INSUMOS *******" AT 50      
    SKIP(1) c-rodape1 SKIP c-rodape2 SKIP c-rodape3 SKIP
            c-rodape4 SKIP c-rodape5 SKIP(1) 
    SKIP(1)
    "******* TRANSPORTE *******" AT 62
    SKIP(1)
    c-rodape-transp
    SKIP(1)
    c-rodape6
    WITH FRAME f-rodape  PAGE-BOTTOM WIDTH 150 NO-LABELS.
   /****************************************************************************/
   /* Form itens ***************************************************************/ 
    FORM HEADER
      "     Item                Seq.    Qtd.    Descricao          It.Cli./Pedido"
      "       Preco unit.     Preco total"  AT 97
      /*wlinha1 */
      SKIP WITH FRAME f-itens WIDTH 150 DOWN.
    /***************************************************************************/
    VIEW FRAME f-cabec.   
    ASSIGN wemp   = "SEEBER FASTPLAS LTDA."
           wtotal = 0. 
    
    FOR EACH b-itens WHERE b-itens.b-it <> ""
                     AND   b-itens.b-qt <> 0  NO-LOCK:
     FIND FIRST item WHERE item.it-codigo = b-itens.b-it NO-LOCK NO-ERROR.
      /* FORMULARIO CABECALHO */
      FORM HEADER         
          wemp AT 02
          TODAY  FORMAT "99/99/9999" AT 70
          STRING(TIME,"HH:MM") "Hs"
          "Pagina"   AT 120 PAGE-NUMBER  FORMAT "ZZZ9"
          SKIP
          "SOLICITACAO DE EMISSAO DE NOTAS FISCAIS" AT 45 
          wlinha
          SKIP
          "Cliente: " SPACE(02) i-nome-abrev
          "Cod. Cliente:"  AT 40  SPACE(3)  b-itens.b-cod-emitente
          "PEDIDO DO CLIENTE:" AT 72 b-itens.b-nr-pedcli  
          "   Pedido: " b-itens.b-nr-pedido
          SKIP
          "Transportadora: _________________"
          "N/N.F. no.: _________________" AT 40
          "Nat.Operacao:"  AT 72 SPACE(2) b-itens.b-nat-operacao      
          wlinha1
          WITH FRAME f-cabec NO-LABELS WIDTH 150 PAGE-TOP.
                          
    ASSIGN 
    c-rodape1  = "Ins._______________________________________"
               + " Ins.________________________________________"
               + " Ins._______________________________________"
    c-rodape2  =  "Ins._______________________________________"
               + " Ins.________________________________________"
               + " Ins._______________________________________"
    c-rodape3  = "Baixa Estoque: Sim (  )  ou Nao (  )"
               + "    Disp./N.: _____________________________________" 
               + "_____________________________________________"
    c-rodape4  = "Observacao:" 
               + " ___________________________________________________"
               + " Disp. _______________"
               + " _____________________ Cx. ____________________"
    c-rodape5 = "Embalagem: " + STRING(i-wemb,"X(35)")
              + "  Qtd.: " + STRING(i-wqt-emb,"ZZZZZZZZ")
              + "  Requisitante: " + STRING(i-wreq,"X(20)")
              + "   Data:  _______/_______/________"
    c-rodape-transp = "Transportadora: " + STRING(i-transp,"X(40)") + "    Placa: " + STRING(i-placa, "x(12)") + "   Motorista: " + STRING(i-motorista,"x(40)").
    c-rodape6 = "Procedimentos Especiais - Solicitacao de NF - (wpf0106/Totvs - 12/15)".     

  /**********************************************************************/  
      ASSIGN wvl-total = 0.
      ASSIGN wvl-total = b-itens.b-qt * b-vl-preuni
             wtotal    = wtotal + wvl-total.
     
      
      FIND FIRST wsequencias WHERE wsequencias.ww-it-codigo = b-itens.b-it
                           NO-LOCK NO-ERROR.
        DISPLAY "***" AT 02 b-itens.b-it        
                       wsequencias.ww-nr-sequencia 
                       b-itens.b-qt SPACE(4)
                       item.descricao-1      
                       b-itens.b-item-do-cli
                       "    " b-itens.b-vl-preuni   
                       wvl-total  
                       WITH FRAME f-itens WIDTH 150 NO-LABEL.
                       DOWN WITH FRAME f-itens STREAM-IO.
       PUT b-itens.b-narrativa AT 61 .
     END.
   PUT SKIP(1) "TOTAL GERAL:" AT 97 wtotal.
   FIND FIRST b-itens NO-LOCK NO-ERROR.
   DISPLAY "" WITH FRAME f-rodape. 
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

