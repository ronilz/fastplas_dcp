/* gera cartinha word para o rh - 24/02/15 - valeria */
DEFINE VARIABLE wemp                AS CHAR FORMAT "X(50)".
DEFINE VARIABLE westab              AS CHAR FORMAT "X(05)".
DEFINE VARIABLE wcgc                LIKE rh_pessoa_jurid.cod_id_feder.
DEFINE VARIABLE wmatric             LIKE funcionario.cdn_funcionario.
DEFINE VARIABLE wdig                LIKE funcionario.num_digito_verfdor_func.
DEFINE VARIABLE wnome               LIKE rh_pessoa_fisic.nom_pessoa_fisic.
DEFINE VARIABLE wcpf                LIKE rh_pessoa_fisic.cod_id_feder /* cpf */.
DEFINE VARIABLE wrg                 LIKE rh_pessoa_fisic.cod_id_estad_fisic.
DEFINE VARIABLE wendereco           LIKE rh_pessoa_fisic.nom_ender_rh. 
DEFINE VARIABLE wnro                LIKE rh_pessoa_fisic.cod_num_ender.
DEFINE VARIABLE wcomplemento        LIKE rh_pessoa_fisic.nom_pto_refer.
DEFINE VARIABLE wbairro             LIKE rh_pessoa_fisic.nom_bairro_rh.
DEFINE VARIABLE wcep                LIKE rh_pessoa_fisic.cod_cep_rh.
DEFINE VARIABLE widade              LIKE rh_pessoa_fisic.nom_cidad_rh.
DEFINE VARIABLE wrenda              LIKE histor_sal_func.val_salario_mensal.

DEFINE VARIABLE AppWord AS COM-HANDLE NO-UNDO.
DEFINE VARIABLE vfile AS CHAR FORMAT "c:/spool/teste-rh.doc".

MESSAGE "executando ..." VIEW-AS ALERT-BOX.

FIND FIRST rh_estab WHERE rh_estab.cdn_estab = "1"
                    AND   rh_estab.cdn_empresa = "4" NO-LOCK.

FIND FIRST rh_pessoa_jurid WHERE rh_pessoa_jurid.num_pessoa_jurid = rh_estab.num_pessoa_jurid NO-LOCK.
 

FOR EACH funcionario WHERE  funcionario.cdn_empresa         = "4"
                     AND    funcionario.cdn_estab           = "1"
                     AND    funcionario.dat_desligto_func   = ?
                       AND   (funcionario.CDN_FUNCIONARIO     GE 0   
                       OR     funcionario.CDN_FUNCIONARIO     LE 10)
                     NO-LOCK: 
     FIND RH_pessoa_fisic OF funcionario NO-LOCK.
     FIND LAST histor_sal_func OF funcionario NO-LOCK.
     ASSIGN wcgc        =  rh_pessoa_jurid.cod_id_feder
            wmatric     =  funcionario.cdn_funcionario
             wdig       = funcionario.num_digito_verfdor_func
            wnome       = rh_pessoa_fisic.nom_pessoa_fisic
            wcpf        = rh_pessoa_fisic.cod_id_feder /* cpf */
            wrg         = rh_pessoa_fisic.cod_id_estad_fisic
            wendereco   = rh_pessoa_fisic.nom_ender_rh 
            wnro        = rh_pessoa_fisic.cod_num_ender
            wcomplemento = rh_pessoa_fisic.nom_pto_refer
            wbairro     = rh_pessoa_fisic.nom_bairro_rh
            wcep        =  rh_pessoa_fisic.cod_cep_rh
            widade      = rh_pessoa_fisic.nom_cidad_rh
            wrenda      = histor_sal_func.val_salario_mensal.
 /*    
END.*/


CREATE "Word.Application" AppWord.

AppWord:Documents:Open("c:\spool\modelo-rh-new.doc",False,False,False,"","",False).

AppWord:Selection:Find:Text = "#wcgc".
AppWord:Selection:Find:Replacement:Text = wcgc.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).


AppWord:Selection:Find:Text = "#nome".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.nom_pessoa_fisic.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).


AppWord:Selection:Find:Text = "#matric".
AppWord:Selection:Find:Replacement:Text = funcionario.cdn_funcionario.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#dig".
AppWord:Selection:Find:Replacement:Text = funcionario.num_digito_verfdor_func.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#cpf".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.cod_id_feder .
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#rg".
AppWord:Selection:Find:Replacement:Text = cod_id_estad_fisic.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#endereco".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.nom_ender_rh.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#nro".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.cod_num_ender.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#complemento".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.nom_pto_refer.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#bairro".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.nom_bairro_rh.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#cep".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.cod_cep_rh.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#cidade".
AppWord:Selection:Find:Replacement:Text = rh_pessoa_fisic.nom_cidad_rh.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).

AppWord:Selection:Find:Text = "#renda".
AppWord:Selection:Find:Replacement:Text = histor_sal_func.val_salario_mensal.
AppWord:Selection:Find:Forward = True.
AppWord:Selection:Collapse.
AppWord:Selection:Find:Execute(,,,,,,,,,,2,,,,).



AppWord:visible = false.
Appword:Selection:Font:Size = 12. 
Appword:Selection:Font:Name = "Arial Narrow" . 
Appword:Selection:Font:Bold = true. 

Appword:ActiveDocument:SaveAs("C:\spool\Carta" +  STRING(TIME) + ".doc"). 
Appword:ActiveDocument:Close. 
Appword:Quit().
release object appWord.
END.
      
/*pular p�gina e ir para o FINAL DO arquivo
 Appword:SELECTION:EndKey(6). /* Posiciona cursor no final do arquivo */
Appword:SELECTION:InsertBreak(7).  /* posicionar final arquivo*/
*/ 
/* para escrever algo */
/* AppWord:SELECTION:TypeText("OL� MUNDO !!"). */
/* paragrafo 
AppWord:Selection:MoveDown.
Appword:Selection:TypeParagraph. */
/* direto na impressora 
Appword:ActivePrinter = "CutePdf Writter".
Appword:PrintOut().
*/ 

MESSAGE "FINALIZADO" VIEW-AS ALERT-BOX.
