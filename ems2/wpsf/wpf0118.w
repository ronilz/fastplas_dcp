&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          movdis           PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
  /* 25/07/22 - CONF. SOL. FACIO/ALESSANDRO E JOSE LUIZ 
  AUTOR: VALERIA 
  PROGRAMA EMITIRA PEDIDO SOMENTE PARA PEDIDOS QUE CONTENHAM A LETRA Z NA POSI��O 11, COMBINADO COM REGIANE
  PROGRAMA WPF0118 FOI COPIADO DO PROGRAMA WPF0106 - QUE N�O TINHA OS NOVOS PARAMETROS */




/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-3

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ped-venda

/* Definitions for BROWSE BROWSE-3                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-3 ped-venda.cod-estabel ~
ped-venda.nome-abrev ped-venda.cod-emitente ped-venda.nr-pedcli ~
ped-venda.nr-pedido ped-venda.dt-implant 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-3 
&Scoped-define QUERY-STRING-BROWSE-3 FOR EACH ped-venda ~
      WHERE ped-venda.cod-sit-ped <> 3 ~
 AND ped-venda.cod-sit-ped <> 5 ~
 AND ped-venda.cod-sit-ped <> 6 ~
 and ped-venda.cod-estab = wcod-estab NO-LOCK ~
    BY ped-venda.cod-estabel ~
       BY ped-venda.cod-emitente ~
        BY ped-venda.nr-pedido INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-3 OPEN QUERY BROWSE-3 FOR EACH ped-venda ~
      WHERE ped-venda.cod-sit-ped <> 3 ~
 AND ped-venda.cod-sit-ped <> 5 ~
 AND ped-venda.cod-sit-ped <> 6 ~
 and ped-venda.cod-estab = wcod-estab NO-LOCK ~
    BY ped-venda.cod-estabel ~
       BY ped-venda.cod-emitente ~
        BY ped-venda.nr-pedido INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-3 ped-venda
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-3 ped-venda


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-3}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-6 RECT-7 RECT-8 RECT-9 wemp BROWSE-3 ~
wcod-estab wcli BUTTON-7 wped-ini wemb wqt-emb wreq b-consi bt-gera bt-sair ~
wtransp wplaca bt-sair-2 wmotorista 
&Scoped-Define DISPLAYED-OBJECTS wemp wcod-estab wcli wped-ini wemb wqt-emb ~
wreq wtransp wplaca wmotorista 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON b-consi 
     LABEL "Instru��es" 
     SIZE 15 BY 1.

DEFINE BUTTON bt-gera 
     LABEL "Solicita��o" 
     SIZE 13 BY 1
     BGCOLOR 15 .

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-7  NO-CONVERT-3D-COLORS
     LABEL "Procura Pedidos" 
     SIZE 19 BY 1.13.

DEFINE VARIABLE wcli AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Cliente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wcod-estab AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wemb AS CHARACTER FORMAT "X(35)":U 
     LABEL "Embalagem" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wemp AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wmotorista AS CHARACTER FORMAT "X(50)":U 
     LABEL "Motorista" 
     VIEW-AS FILL-IN 
     SIZE 46 BY 1 NO-UNDO.

DEFINE VARIABLE wped-ini AS CHARACTER FORMAT "X(12)":U 
     LABEL "Pedido do Cliente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wplaca AS CHARACTER FORMAT "X(15)":U 
     LABEL "Placa do caminh�o" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wqt-emb AS INTEGER FORMAT "ZZZZZZ99":U INITIAL 0 
     LABEL "Qtd. Embalagem" 
     VIEW-AS FILL-IN 
     SIZE 10 BY 1 NO-UNDO.

DEFINE VARIABLE wreq AS CHARACTER FORMAT "X(20)":U 
     LABEL "Requisitante" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wtransp AS CHARACTER FORMAT "X(40)":U 
     LABEL "Transportadora" 
     VIEW-AS FILL-IN 
     SIZE 39 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 38 BY 10.5.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 96 BY 1
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 96 BY 1
     BGCOLOR 7 FGCOLOR 3 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 57 BY 12.25.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-3 FOR 
      ped-venda SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-3 C-Win _STRUCTURED
  QUERY BROWSE-3 NO-LOCK DISPLAY
      ped-venda.cod-estabel FORMAT "x(5)":U
      ped-venda.nome-abrev FORMAT "x(12)":U
      ped-venda.cod-emitente FORMAT ">>>>>>>>9":U WIDTH 8.43
      ped-venda.nr-pedcli FORMAT "x(12)":U WIDTH 10.29
      ped-venda.nr-pedido FORMAT ">>>,>>>,>>9":U
      ped-venda.dt-implant FORMAT "99/99/9999":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 54 BY 10
         FONT 1 ROW-HEIGHT-CHARS .67 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wemp AT ROW 3.5 COL 21 COLON-ALIGNED
     BROWSE-3 AT ROW 3.75 COL 43
     wcod-estab AT ROW 4.5 COL 21 COLON-ALIGNED
     wcli AT ROW 5.5 COL 21 COLON-ALIGNED
     BUTTON-7 AT ROW 7.25 COL 20
     wped-ini AT ROW 9 COL 20 COLON-ALIGNED
     wemb AT ROW 10 COL 20 COLON-ALIGNED
     wqt-emb AT ROW 11 COL 20 COLON-ALIGNED
     wreq AT ROW 12 COL 20 COLON-ALIGNED
     b-consi AT ROW 14 COL 3
     bt-gera AT ROW 14 COL 20
     bt-sair AT ROW 14 COL 35
     wtransp AT ROW 16.25 COL 20 COLON-ALIGNED WIDGET-ID 2
     wplaca AT ROW 17.25 COL 20 COLON-ALIGNED WIDGET-ID 6
     bt-sair-2 AT ROW 17.25 COL 85 WIDGET-ID 16
     wmotorista AT ROW 18.25 COL 20 COLON-ALIGNED WIDGET-ID 8
     "   SOLICITA��O DE NOTAS FISCAIS ESPECIAIS" VIEW-AS TEXT
          SIZE 44 BY .67 AT ROW 1.5 COL 29 WIDGET-ID 14
          BGCOLOR 0 FGCOLOR 11 
     "Par�metros" VIEW-AS TEXT
          SIZE 12 BY .67 AT ROW 2.75 COL 18
     "Consulta de Pedidos" VIEW-AS TEXT
          SIZE 19 BY .67 AT ROW 2.75 COL 63
     "V.2022.08.01" VIEW-AS TEXT
          SIZE 15 BY .54 TOOLTIP "Vers�o para pedidos com final z" AT ROW 18.75 COL 82 WIDGET-ID 12
     RECT-6 AT ROW 3 COL 3
     RECT-7 AT ROW 19.75 COL 3
     RECT-8 AT ROW 1.25 COL 3
     RECT-9 AT ROW 3 COL 42
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 101.86 BY 19.83.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Solicita��o de Notas Fiscais - Pedidos Especiais - wpf0118"
         HEIGHT             = 19.83
         WIDTH              = 101.86
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB BROWSE-3 wemp DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-3
/* Query rebuild information for BROWSE BROWSE-3
     _TblList          = "movdis.ped-venda"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "movdis.ped-venda.cod-estabel|yes,movdis.ped-venda.cod-emitente|yes,movdis.ped-venda.nr-pedido|yes"
     _Where[1]         = "movdis.ped-venda.cod-sit-ped <> 3
 AND movdis.ped-venda.cod-sit-ped <> 5
 AND movdis.ped-venda.cod-sit-ped <> 6
 and movdis.ped-venda.cod-estab = wcod-estab"
     _FldNameList[1]   = movdis.ped-venda.cod-estabel
     _FldNameList[2]   = movdis.ped-venda.nome-abrev
     _FldNameList[3]   > movdis.ped-venda.cod-emitente
"ped-venda.cod-emitente" ? ? "integer" ? ? ? ? ? ? no ? no no "8.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[4]   > movdis.ped-venda.nr-pedcli
"ped-venda.nr-pedcli" ? ? "character" ? ? ? ? ? ? no ? no no "10.29" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[5]   = movdis.ped-venda.nr-pedido
     _FldNameList[6]   = movdis.ped-venda.dt-implant
     _Query            is OPENED
*/  /* BROWSE BROWSE-3 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Solicita��o de Notas Fiscais - Pedidos Especiais - wpf0118 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Solicita��o de Notas Fiscais - Pedidos Especiais - wpf0118 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b-consi
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b-consi C-Win
ON CHOOSE OF b-consi IN FRAME DEFAULT-FRAME /* Instru��es */
DO:
  RUN consid.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-3
&Scoped-define SELF-NAME BROWSE-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BROWSE-3 C-Win
ON VALUE-CHANGED OF BROWSE-3 IN FRAME DEFAULT-FRAME
DO:
  ASSIGN wped-ini = ped-venda.nr-pedcli
         wcli     = ped-venda.cod-emitente.
  DISPLAY wcli wped-ini WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-gera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-gera C-Win
ON CHOOSE OF bt-gera IN FRAME DEFAULT-FRAME /* Solicita��o */
DO:
   ENABLE wemp wcod-estab wcli wped-ini wemb wqt-emb wreq 
          bt-gera bt-sair WITH FRAME  {&FRAME-NAME}. 
  ASSIGN wemp wcod-estab wcli wped-ini wemb wqt-emb wreq wtransp wplaca wmotorista.
  FIND FIRST emitente  WHERE emitente.cod-emitente = wcli NO-LOCK NO-ERROR.
  IF NOT AVAILABLE emitente THEN MESSAGE "Emitente n�o encontrado:" + STRING(wcli) VIEW-AS ALERT-BOX.
  FIND FIRST ped-venda WHERE ped-venda.nr-pedcli = wped-ini 
                                    AND   ped-venda.cod-emitente = wcli
                                    AND   (ped-venda.cod-sit-ped  <> 3
                                    AND   ped-venda.cod-sit-ped   <> 5
                                    AND   ped-venda.cod-sit-ped   <> 6)  NO-LOCK NO-ERROR.

  /* 25/07/22 - CONF. SOL. FACIO/ALESSANDRO E JOSE LUIZ 
  PROGRAMA EMITIRA PEDIDO SOMENTE PARA PEDIDOS QUE CONTENHAM A LETRA Z NA POSI��O 11, COMBINADO COM REGIANE
  PROGRAMA WPF0118 FOI COPIADO DO PROGRAMA WPF0106 - QUE N�O TINHA OS NOVOS PARAMETROS */

  
  IF SUBSTRING(ped-venda.nr-pedcli,1,1) <> "z"  THEN MESSAGE "A SOLICITA��O DE NOTA FISCAL PARA ESTE PEDIDO TEM QUE SER FEITA ATRAV�S DO SISTEMA DO  COLETOR"
                                                 SKIP "ENTRE EM CONTATO COM COMERCIAL/PEDIDOS OU PCP/CHEFIA" VIEW-AS ALERT-BOX.


  IF NOT AVAILABLE ped-venda THEN MESSAGE "Pedido n�o encontrado:" + STRING(nr-pedcli) VIEW-AS ALERT-BOX.
  IF SUBSTRING(ped-venda.nr-pedcli,1,1) = "z" THEN DO:
  IF AVAILABLE ped-venda THEN RUN  wpsf\wpf0118a.w
                                  (INPUT ped-venda.cod-emitente, INPUT ped-venda.nr-pedcli, 
                                   INPUT wemb, INPUT wqt-emb, INPUT wreq, INPUT wcod-estab, 
                                   INPUT ped-venda.nome-abrev, INPUT ped-venda.nat-operacao,
                                   INPUT ped-venda.nr-pedido, INPUT wtransp, INPUT wplaca, INPUT wmotorista).
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-7 C-Win
ON CHOOSE OF BUTTON-7 IN FRAME DEFAULT-FRAME /* Procura Pedidos */
DO:
   RUN procura-pedido.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  /*RUN mensagem-ini.*/
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consid C-Win 
PROCEDURE consid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
MESSAGE 
  "IMPORTANTE !!!!  05/2003 - NOVO PROGRAMA DE SOLICITACAO DE NOTA FISCAL"
  SKIP(1)
  "Ao digitar a quantidade o programa ira checar o saldo em aberto do pedido."
  SKIP
  "Se o pedido/item nao possuir saldo suficiente, ira aparecer uma mensagem na"
  SKIP
  "tela informando a falta de saldo."
  SKIP(2)
  "Por favor observar essas mensagens que irao facilitar a digitacao de notas."
  SKIP 
  "E' importante que seja digitado o estabelecimento correto, para"
  SKIP
  "a geracao de pedidos."
  SKIP "SERA LISTADO NA REQUISICAO AS SEQUENCIAS COM SALDO DO PEDIDO."
  SKIP(2)
  "*** AVISO ***"
          SKIP "1) OCORRERAM MUDAN�AS NA INCLUS�O DOS ITENS, FAVOR OBSERVAR AS MENSAGENS"
          SKIP "    QUE APARECEM NA TELA."
          SKIP "2) A OP��O PROCURA PEDIDOS TAMB�M FOI ALTERADA PARA SER MAIS R�PIDA."
         
  VIEW-AS ALERT-BOX.
RUN mensagem.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consistencia C-Win 
PROCEDURE consistencia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Relatorio de Consistencia */

         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wemp wcod-estab wcli wped-ini wemb wqt-emb wreq wtransp wplaca 
          wmotorista 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-6 RECT-7 RECT-8 RECT-9 wemp BROWSE-3 wcod-estab wcli BUTTON-7 
         wped-ini wemb wqt-emb wreq b-consi bt-gera bt-sair wtransp wplaca 
         bt-sair-2 wmotorista 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN wped-ini       = ""
         wemp           = 1
         wcod-estab     = "1"
         wcli           = 0.
         /*wcod-estab2    = "1".*/
  ASSIGN wtransp = ""
         wplaca = ""
         wmotorista = "".
  DISPLAY wemp wcod-estab wcli wped-ini wemb wqt-emb wreq /*wcod-estab2*/ wtransp wplaca wmotorista WITH FRAME {&FRAME-NAME}.
  ASSIGN wemp wcod-estab wcli wped-ini wemb wqt-emb wreq /*wcod-estab2*/ wtransp wplaca wmotorista.
  /*
  MESSAGE         "1) A REQUISI��O EST� COM UM NOVO FORMATO DE IMPRESS�O, CONFORME"
          SKIP    "   SOLICITA��O DO SETOR FATURAMENTO."
          SKIP(1) "2) A pesquisa: PROCURA PEDIDOS agora est� mais r�pida." 
           VIEW-AS ALERT-BOX.
 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ITENS C-Win 
PROCEDURE ITENS :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*
FORM
   SKIP " [1]"  witem[1]  wquant[1]  "[11]" witem[11] wquant[11]
   SKIP " [2]"  witem[2]  wquant[2]  "[12]" witem[12] wquant[12]
   SKIP " [3]"  witem[3]  wquant[3]  "[13]" witem[13] wquant[13]
   SKIP " [4]"  witem[4]  wquant[4]  "[14]" witem[14] wquant[14]
   SKIP " [5]"  witem[5]  wquant[5]  "[15]" witem[15] wquant[15]
   SKIP " [6]"  witem[6]  wquant[6]  "[16]" witem[16] wquant[16]
   SKIP " [7]"  witem[7]  wquant[7]  "[17]" witem[17] wquant[17]
   SKIP " [8]"  witem[8]  wquant[8]  "[18]" witem[18] wquant[18]
   SKIP " [9]"  witem[9]  wquant[9]  "[19]" witem[19] wquant[19]
   SKIP "[10]" witem[10] wquant[10]  "[20]" witem[20] wquant[20]
HEADER SKIP(1) "Item                  Qtd.          Item             Qtd."
   WITH FRAME f-itens ROW 05 NO-LABELS CENTERED TITLE "ITENS E QUANTIDADES".
  DO wcont = 1 TO 20 ON ERROR UNDO,RETRY:
    UPDATE witem[wcont] wquant[wcont] 
    WITH FRAME f-itens.
    ASSIGN wsal-it = 0.
      
    FIND FIRST b-itens WHERE b-it = witem[wcont] NO-LOCK NO-ERROR.
    IF AVAILABLE b-itens THEN DO:
      MESSAGE "Item ja digitado" VIEW-AS ALERT-BOX.
      UNDO,RETRY.
      END.

    IF witem[wcont] = "" THEN NEXT.
    IF witem[wcont] <> "" THEN DO:
    /*  Calcula Saldo */
        
      FIND FIRST b-ped-venda WHERE (b-ped-venda.cod-sit-ped <> 9
                             OR b-ped-venda.cod-sit-ped <> 2
                             OR b-ped-venda.cod-sit-ped <> 4)
                             AND b-ped-venda.nr-pedcli    = wped-ini 
                             AND b-ped-venda.cod-emitente = wcli
                             AND b-ped-venda.cod-estab    = wcod-estab
                             NO-LOCK NO-ERROR.
      
      IF NOT AVAILABLE b-ped-venda THEN MESSAGE
      "Estab.:" wcod-estab "Pedido:" wped-ini "Cliente:" wcli
      "nao encontrado" VIEW-AS ALERT-BOX.
      ASSIGN wsaldo  = 0
             wsal-it = 0.
      /* Checa se o codigo do item existe ------------------------*/
      FIND FIRST b-ped-item  WHERE b-ped-item.it-codigo = witem[wcont]
                             AND   b-ped-item.nr-pedcli =                    
                                   b-ped-venda.nr-pedcli
                             AND   b-ped-item.nome-abrev = 
                                   b-ped-venda.nome-abrev
                             AND   b-ped-venda.cod-estab = wcod-estab
                             NO-LOCK NO-ERROR.
      IF NOT AVAILABLE b-ped-item THEN DO: 
        MESSAGE 
        "ITEM: " witem[wcont] SKIP
        "NAO ENCONTRADO NO PEDIDO: " b-ped-venda.nr-pedcli 
         VIEW-AS ALERT-BOX.
         UNDO,RETRY.
         END.
      /* Calcula saldo ------------------------------------------------*/
      FOR EACH b-ped-item WHERE b-ped-item.it-codigo = witem[wcont]
                          AND   b-ped-item.nr-pedcli = 
                                b-ped-venda.nr-pedcli
                          AND   b-ped-item.nome-abrev = 
                                b-ped-venda.nome-abrev
                          AND   b-ped-venda.cod-estab = wcod-estab
                          NO-LOCK:
        FIND FIRST item-cli OF b-ped-item NO-LOCK NO-ERROR.
        IF NOT AVAILABLE item-cli THEN DO:
          MESSAGE "NAO FOI ENCONTRADO RELACIONAMENTO ITEM/CLIENTE DO CLIENTE"
          SKIP
          "REQUISICAO NAO PODE SER GERADA, FAVOR INSERIR O RELACIONAMENTO"
          VIEW-AS ALERT-BOX.
          END.
          
        ASSIGN wvl-preuni = 0.
        ASSIGN wvl-preuni = b-ped-item.vl-preuni.
        ASSIGN wsal-it = 0. 
        FOR EACH b-ped-ent OF b-ped-item NO-LOCK:
          IF b-ped-ent.cod-sit-ent = 0 
          or b-ped-ent.cod-sit-ent = 1 
          or b-ped-ent.cod-sit-ent = 3 THEN DO:
            ASSIGN wsal-it = (b-ped-ent.qt-pedida
                           - (b-ped-ent.qt-atendida 
                           - b-ped-ent.qt-pendente)).
            ASSIGN wsaldo = wsaldo + wsal-it.
            END.
          IF wsaldo GE wquant[wcont] THEN DO:
            CREATE wsequencias.
            ASSIGN 
                   wsequencias.ww-nr-sequencia = b-ped-item.nr-sequencia
                   wsequencias.ww-it-codigo    = b-ped-item.it-codigo
                   wsequencias.ww-nr-pedcli    = b-ped-item.nr-pedcli
                   wsequencias.ww-qt-pedida    = b-ped-item.qt-pedida
                   wsequencias.ww-saldo        = wsal-it.
            END.
          END.  
        END. 
      /* Fim do calculo do saldo --------------------------------------*/
      IF wquant[wcont] > wsaldo THEN DO:
        MESSAGE 
        "REQUISICAO NAO PODE SER GERADA PARA O ITEM:" witem[wcont]
        SKIP "Item nao possui saldo suficiente para a quantidade digitada:"
        wquant[wcont] "."
        SKIP "Saldo em aberto do pedido/item = " wsaldo
        VIEW-AS ALERT-BOX. 
        UNDO, RETRY.         
        END.
      IF wquant[wcont] = 0 THEN DO:
        MESSAGE "QUANTIDADE NAO PODE SER 0"
        VIEW-AS ALERT-BOX.
        UNDO,RETRY.
        END.
      END.
    IF wquant[wcont] LE wsaldo THEN DO:
      CREATE b-itens.
      ASSIGN b-itens.b-it           = witem[wcont]
             b-itens.b-qt           = wquant[wcont]
             b-itens.b-nr-pedcli    = wped-ini
             b-itens.b-cod-emitente = wcli
             b-itens.b-nr-pedido    = b-ped-venda.nr-pedido
             b-itens.b-nat-operacao = b-ped-venda.nat-operacao
             b-itens.b-nome-abrev   = b-ped-venda.nome-abrev
             b-itens.b-vl-preuni    = wvl-preuni
             b-itens.b-emb          = wemb
             b-itens.b-qt-emb       = wqt-emb
             b-itens.b-req          = wreq
             b-itens.b-item-do-cli  = item-cli.item-do-cli.
      END.
    END.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mensagem C-Win 
PROCEDURE mensagem :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 MESSAGE
  "                  O Usu�rio pode digitar a Empresa/Estabelecimento/Cliente e Pedido,"
   SKIP
  "                              ou pode consultar o n�mero do pedido."
  SKIP(1)
  "COMO CONSULTAR O PEDIDO?"
  SKIP(2)
  "1) Digitar Empresa/Estabelecimento/Cliente."
  SKIP(1)
  "2) Clicar em procurar pedido."
  SKIP(1)
  "3) AGUARDAR AT� QUE OS PEDIDOS APARE�AM NA CAIXA DE CONSULTA DE PEDIDOS."
  SKIP(1)
  "4) Todos os pedidos da Empresa/Estabelecimento/Cliente ser�o mostrados na"
  SKIP(1)
  "   Caixa de CONSULTA DE PEDIDOS."
  SKIP(1)
  "5) O �ltimo pedido do cliente ser� levado automaticamente para os par�metros."
  SKIP(1)
  "6) Ou ent�o voc� pode clicar no pedido e o mesmo ser� levado para a tela de par�metros."
  SKIP(2)
  "VOCE PODE TAMB�M CONSULTAR TODOS OS PEDIDOS EM ABERTO POR ESTABELECIMENTO."
  SKIP(1)
  "1) Basta digitar o estabelecimento."
   SKIP
  "2) Clicar em Consultar todos os pedidos do Estabelecimento."
  VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE mensagem-ini C-Win 
PROCEDURE mensagem-ini :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
MESSAGE   "                                            IMPORTANTE !!!!"
          SKIP
          "A PARTIR DE HOJE A IMPRESS�O DA SOLICITA��O DE N.F. SAIR� RESUMIDA,"
           SKIP
          "CONFORME SOLICITA��O DO DEPTO. DE FATURAMENTO."
          SKIP(2)
          "MAS N�O HOUVE ALTERA��ES NA FORMA DE PREENCHIMENTO DA SOLICITA��O."
          VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE procura-pedido C-Win 
PROCEDURE procura-pedido :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DISABLE ALL WITH FRAME   {&FRAME-NAME}.
   ASSIGN wemp wcod-estab wcli wped-ini wemb wqt-emb wreq.
   FIND FIRST emitente  WHERE emitente.cod-emitente = wcli NO-LOCK NO-ERROR.
   IF NOT AVAILABLE emitente THEN MESSAGE "Emitente n�o encontrado:" 
   + STRING(wcli) VIEW-AS ALERT-BOX.
   FIND FIRST emitente WHERE emitente.cod-emitente = wcli NO-LOCK NO-ERROR.
   
   /* 27/07/22 - AJUSTES - PARA ATENDER FIFO - CONF. FABIO / ALESSANDRO - FATURAR SOMENTE PEDIDOS COM inicio z
   PEDIDOS SEM FIFO*/
   
   FIND FIRST ped-venda WHERE ped-venda.nome-abrev = emitente.nome-abrev
                                          AND   ped-venda.cod-estab  = wcod-estab 
                                          AND  (ped-venda.cod-sit-ped  <> 3
                                          AND   ped-venda.cod-sit-ped   <> 5
                                          AND   ped-venda.cod-sit-ped   <> 6) 
                                          AND   SUBSTRING(ped-venda.nr-pedcl,1,1) = "z" 
                                          USE-INDEX ch-estcli   NO-LOCK NO-ERROR.
   IF NOT AVAILABLE ped-venda  THEN DO:
         MESSAGE "ALERTA!!!"
                 SKIP "CLIENTE: " wcli " N�O POSSUI PEDIDO ESPECIAL COM INICIAL - z - "
                 SKIP(1) "Esta solicita��o de Notas fiscais � somente para pedidos especiais."
                 SKIP(1) "Demais pedidos devem ser faturados via SISTEMA DE COLETOR DE DADOS"
                 SKIP "Qualquer d�vida procurar os Setores: Comercial/Pedidos / PCP / Expedi��o"
                 VIEW-AS ALERT-BOX.
               apply "close":U to this-procedure.
                 
         END.
   
   IF AVAILABLE ped-venda  THEN DO:
   OPEN QUERY browse-3 FOR EACH ped-venda WHERE ped-venda.nome-abrev = emitente.nome-abrev
                                          AND   ped-venda.cod-estab  = wcod-estab 
                                          AND  (ped-venda.cod-sit-ped  <> 3
                                          AND   ped-venda.cod-sit-ped   <> 5
                                          AND   ped-venda.cod-sit-ped   <> 6) 
                                          AND   SUBSTRING(ped-venda.nr-pedcl,1,1) = "z" 
                                          USE-INDEX ch-estcli NO-LOCK.
     
   GET LAST browse-3.
   ENABLE ALL WITH FRAME   {&FRAME-NAME}.
   ASSIGN wped-ini = ped-venda.nr-pedcli.
   DISPLAY wped-ini WITH FRAME {&FRAME-NAME}.
   END.
 END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio C-Win 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*********************************  LISTANDO **************************/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE solicitacao C-Win 
PROCEDURE solicitacao :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

