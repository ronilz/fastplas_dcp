&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wfornec LIKE emitente.cod-emitente.
DEFINE VARIABLE wdata   AS   DATE INITIAL TODAY.

DEFINE WORKFILE avirec    FIELD  aritem   LIKE item.it-codigo
                          FIELD  arfornec LIKE emitente.cod-emitente
                          FIELD  arnomef  LIKE emitente.nome-abrev
                          FIELD  arlote   AS CHAR FORMAT "X(09)"
                          FIELD  arped    LIKE ordem-compra.num-pedido
                          FIELD  arordem  LIKE recebimento.numero-ordem
                          FIELD  ardata   AS   DATE FORMAT "99/99/99"
                          INITIAL TODAY
                          FIELD  arunid   LIKE item.un
                          FIELD  aritnome AS CHAR FORMAT "X(37)"
                          FIELD  ardatnot AS DATE FORMAT "99/99/99" INITIAL TODAY
                          FIELD  arnronot AS INTE FORMAT ">,>>>,>>9"
                          FIELD  arsernot LIKE recebimento.serie-nota
                          FIELD  arqtdrec AS   DECI FORMAT ">>>,>>9.9999"
                          FIELD  arficha  AS   INTE FORMAT ">>>,>>9".

DEFINE VARIABLE wconf-imp       AS LOGICAL.
DEFINE VARIABLE wcon            AS CHAR FORMAT "X(35)" INITIAL "".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 RECT-4 RECT-5 RECT-6 ~
RECT-7 wordem BUTTON-1 bt-sair-3 wnronot wficha wsernot wped wdatnot wnomef ~
wqtdrec witem wunid witnome wlote bt-salva bt-limpa wrel w-imp bt-executar ~
bt-sair-2 
&Scoped-Define DISPLAYED-OBJECTS wordem wnronot wficha wsernot wped wdatnot ~
wnomef wqtdrec witem wunid witnome wlote wrel w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 11 BY 1.08 TOOLTIP "Imprimir relat�rio"
     FONT 1.

DEFINE BUTTON bt-limpa 
     LABEL "Limpa arquivo" 
     SIZE 12 BY 1 TOOLTIP "Limpa todas as fichas"
     FONT 1.

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON bt-sair-3 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON bt-salva  NO-CONVERT-3D-COLORS
     LABEL "Salvar  Ficha" 
     SIZE 12 BY 1 TOOLTIP "Salva ficha digitada"
     BGCOLOR 4 FONT 1.

DEFINE BUTTON BtnDone DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     LABEL "Confirma ordem" 
     SIZE 15 BY 1.13.

DEFINE BUTTON BUTTON-11 
     IMAGE-UP FILE "adeicon/1.bmp":U
     LABEL "Confirma Ordem" 
     SIZE 13 BY 1 TOOLTIP "Confirma Ordem"
     FONT 1.

DEFINE BUTTON BUTTON-7 
     LABEL "Arquivo" 
     SIZE 10 BY 1.13.

DEFINE BUTTON wimprime 
     LABEL "Imprimir" 
     SIZE 10 BY 1.13.

DEFINE VARIABLE wdatnot AS DATE FORMAT "99/99/9999":U 
     LABEL "Data da nota" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wficha AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Ficha de identificacao" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE witem AS CHARACTER FORMAT "X(16)":U 
     LABEL "C�digo do item" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE witnome AS CHARACTER FORMAT "X(18)":U 
     LABEL "Descri��o" 
     VIEW-AS FILL-IN 
     SIZE 27 BY 1 NO-UNDO.

DEFINE VARIABLE wlote AS CHARACTER FORMAT "X(9)":U INITIAL "0" 
     LABEL "N�mero do lote" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wnomef AS CHARACTER FORMAT "X(16)":U 
     LABEL "Fornecedor" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnronot AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "N�mero da nota" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wordem AS INTEGER FORMAT "zzzzz9,99":U INITIAL 0 
     LABEL "N�mero da ordem" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE wped AS INTEGER FORMAT ">,>>>,>>9":U INITIAL 0 
     LABEL "Pedido" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wqtdrec AS DECIMAL FORMAT ">>>,>>9.9999":U INITIAL 0 
     LABEL "Qtd. recebida" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(25)":U 
     LABEL "Nome do Relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE wsernot AS CHARACTER FORMAT "X(3)":U 
     LABEL "S�rie da nota" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE wunid AS CHARACTER FORMAT "X(2)":U 
     LABEL "Unidade medida" 
     VIEW-AS FILL-IN 
     SIZE 17 BY 1 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 1
     SIZE 10 BY .75
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 42 BY 6.75
     BGCOLOR 0 FGCOLOR 0 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 36 BY 6.5
     BGCOLOR 15 .

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 1.75
     FGCOLOR 8 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 1.5.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wordem AT ROW 2.25 COL 34 COLON-ALIGNED
     BUTTON-1 AT ROW 2.25 COL 52 WIDGET-ID 4
     bt-sair-3 AT ROW 2.25 COL 74
     wnronot AT ROW 4.25 COL 60 COLON-ALIGNED
     wficha AT ROW 4.5 COL 24 COLON-ALIGNED
     wsernot AT ROW 5.25 COL 60 COLON-ALIGNED
     wped AT ROW 5.5 COL 24 COLON-ALIGNED
     wdatnot AT ROW 6.25 COL 60 COLON-ALIGNED
     wnomef AT ROW 6.5 COL 24 COLON-ALIGNED
     wqtdrec AT ROW 7.25 COL 60 COLON-ALIGNED
     witem AT ROW 7.5 COL 24 COLON-ALIGNED
     wunid AT ROW 8.25 COL 60 COLON-ALIGNED
     witnome AT ROW 8.5 COL 11 COLON-ALIGNED
     wlote AT ROW 9.25 COL 60 COLON-ALIGNED
     BUTTON-7 AT ROW 9.5 COL 4
     wimprime AT ROW 9.5 COL 14
     BtnDone AT ROW 9.5 COL 24
     bt-salva AT ROW 11.5 COL 57
     bt-limpa AT ROW 11.5 COL 69
     wrel AT ROW 13 COL 25 COLON-ALIGNED
     w-imp AT ROW 13.25 COL 67 RIGHT-ALIGNED NO-LABEL
     BUTTON-11 AT ROW 15 COL 44
     bt-executar AT ROW 15 COL 65
     bt-sair-2 AT ROW 15 COL 76
     RECT-1 AT ROW 4 COL 2
     RECT-2 AT ROW 4 COL 45
     RECT-3 AT ROW 2 COL 2
     RECT-4 AT ROW 1.25 COL 2
     RECT-5 AT ROW 10.75 COL 2
     RECT-6 AT ROW 12.75 COL 2
     RECT-7 AT ROW 14.25 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81 BY 15.29
         FONT 1
         DEFAULT-BUTTON BtnDone.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Aviso de Recebimento de Material - wpc0101"
         HEIGHT             = 15.29
         WIDTH              = 81
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = 15
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON BtnDone IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       BtnDone:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR BUTTON BUTTON-11 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       BUTTON-11:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR BUTTON BUTTON-7 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       BUTTON-7:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
/* SETTINGS FOR BUTTON wimprime IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
ASSIGN 
       wimprime:HIDDEN IN FRAME DEFAULT-FRAME           = TRUE.

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Aviso de Recebimento de Material - wpc0101 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Aviso de Recebimento de Material - wpc0101 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN wrel    = "V:\spool\Aviso" + STRING(TIME) + ".txt".
  DISPLAY wrel WITH FRAME   {&FRAME-NAME}.
  ASSIGN w-imp wrel.
  CASE w-imp:
   WHEN 1 THEN DO:
      /* gera arq. conferencia */
      ASSIGN wcon = "V:\spool\CONAVI" + SUBSTRING(wrel,15,5) + ".txt".
      RUN consis.
    
      /* IMPRIMIR */
      OUTPUT TO VALUE(wrel).
      RUN imprimir.
      OUTPUT CLOSE.
      END.
     /*
     WHEN 2 THEN DO:
     /* impressora */
        SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
        IF wconf-imp = YES THEN DO:
          OUTPUT TO PRINTER.                   
          {../especificos/wpsf/utilitarios/imp.p}
          PUT CONTROL wnormaliza. 
          PUT CONTROL "~117".
          RUN imprimir. 
          OUTPUT CLOSE.
          END.
        END.*/
     END CASE. 
     RUN inicia.
  RUN limpa-arq.
  MESSAGE "FIM DA GERA��O DO ARQUIVO!" 
          SKIP "Nome Arquivo para impress�o  : "   wrel
          SKIP "Nome Arquivo para confer�ncia: " wcon VIEW-AS ALERT-BOX WARNING. 
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-limpa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-limpa C-Win
ON CHOOSE OF bt-limpa IN FRAME DEFAULT-FRAME /* Limpa arquivo */
DO:
  RUN limpa-arq.
  MESSAGE "LIMPEZA DE ARQUIVO - OK !!!" VIEW-AS ALERT-BOX WARNING.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-3 C-Win
ON CHOOSE OF bt-sair-3 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-salva
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-salva C-Win
ON CHOOSE OF bt-salva IN FRAME DEFAULT-FRAME /* Salvar  Ficha */
DO:
  
  ASSIGN wficha wped wnomef witem witnome wnronot wdatnot wqtdrec wunid wlote wsernot.
  CREATE avirec.
  ASSIGN arficha   = wficha 
         aritem    = witem 
         arfornec  = wfornec
         arnomef   = wnomef    
         arlote    = wlote      
         arped     = wped 
         arordem   = wordem 
         ardata    = wdata 
         arunid    = wunid  
         aritnome  = witnome  
         ardatnot  = wdatnot   
         arnronot  = wnronot      
         arsernot  = wsernot
         arqtdrec  = wqtdrec.
         RUN inicia.
         
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnDone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnDone C-Win
ON CHOOSE OF BtnDone IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Confirma ordem */
DO:
 ASSIGN wrel = "".
 DISPLAY wrel WITH FRAME  {&FRAME-NAME}.
 ASSIGN wordem.
 RUN confirma.
 ENABLE bt-salva WITH FRAME {&FRAME-NAME}.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-11 C-Win
ON CHOOSE OF BUTTON-11 IN FRAME DEFAULT-FRAME /* Confirma Ordem */
DO:
 ASSIGN wrel = "".
 DISPLAY wrel WITH FRAME  {&FRAME-NAME}.
 ASSIGN wordem.
 RUN confirma.
 ENABLE bt-salva WITH FRAME {&FRAME-NAME}.
 END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-7 C-Win
ON CHOOSE OF BUTTON-7 IN FRAME DEFAULT-FRAME /* Arquivo */
DO:
  OUTPUT TO VALUE(wrel) PAGE-SIZE 60.
  RUN imprimir. 
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME wimprime
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wimprime C-Win
ON CHOOSE OF wimprime IN FRAME DEFAULT-FRAME /* Imprimir */
DO:
  SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
  IF wconf-imp = YES THEN DO:
    OUTPUT TO PRINTER PAGE-SIZE 62.
    {wpsf/utilitarios/imp.p}
    /*
    PUT CONTROL wcomprime  a4. 
    */
    RUN imprimir. 
  END.

  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo C-Win 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
OUTPUT TO value(wrel) page-size 44.
{wpsf/utilitarios/imp.p}
PUT CONTROL wcomprime wpaisagem a4. 
RUN imprimir.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE confirma C-Win 
PROCEDURE confirma :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  
  FIND ordem-compra WHERE wordem = ordem-compra.numero-ordem NO-ERROR.
  IF NOT AVAILABLE ordem-compra THEN MESSAGE "N�O ENCONTROU ORDEM: " 
                                + STRING(wordem)
                                VIEW-AS ALERT-BOX.
  IF AVAIL ordem-compra THEN DO:
     ASSIGN witem = ordem-compra.it-codigo
            wped  = ordem-compra.num-pedido
            wdatnot = data-emissao.
  FIND emitente WHERE emitente.cod-emitente = ordem-compra.cod-emitente NO-LOCK NO-ERROR.
  IF NOT AVAILABLE emitente THEN MESSAGE "NAO ENCONTROU EMITENTE: " 
                            + STRING(emitente.cod-emitente)
                            VIEW-AS ALERT-BOX.
     ASSIGN wnomef  = nome-abrev
            wfornec = ordem-compra.cod-emitente
            wnomef  = emitente.nome-abrev.
  FIND item WHERE item.it-codigo = witem NO-LOCK NO-ERROR.
     ASSIGN witnome = descricao-1 + " " + descricao-2
            wunid   = item.un
            wficha = INTEGER(string(time,"999,999")).
  IF witem = " " THEN DO:
     ASSIGN  witem = "S/ CODIGO"
             witnome = SUBSTRING(ordem-compra.narrativa,1,16).
   END.
  END.
  DISPLAY  wficha witem wped wdatnot wnomef wunid witnome WITH FRAME  {&FRAME-NAME}.
  ENABLE wnronot wdatnot wqtdrec wunid wlote  wsernot  WITH FRAME {&FRAME-NAME}.
  ASSIGN wordem wnronot wdatnot wqtdrec wunid wlote  wsernot wrel wficha
         witem wped wdatnot wnomef wunid witnome.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consis C-Win 
PROCEDURE consis :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
      OUTPUT TO VALUE(wcon) PAGED.
      FORM header
      SKIP(1)
      "*** FICHAS GERADAS *** Arquivo gerado: " wrel "Consistencia: " wcon
      SKIP(2)
      WITH FRAME f-con WIDTH 150.

     FOR EACH avirec NO-LOCK:
        DISPLAY  arficha    COLUMN-LABEL "Ficha"
                arfornec    COLUMN-LABEL "Codigo"
                arped       COLUMN-LABEL "Pedido"
                arordem     COLUMN-LABEL "Ordem"
                ardata      COLUMN-LABEL "Data"
                aritem      COLUMN-LABEL "Item"
                arunid      COLUMN-LABEL "UN"
                arnronot    COLUMN-LABEL "Nota"
                ardatnot    COLUMN-LABEL "Data"
                arqtdrec    COLUMN-LABEL "Qt."
                arlote      COLUMN-LABEL "Lote"
                arsernot    COLUMN-LABEL "Serie"
                WITH FRAME f-con DOWN.
        DOWN WITH FRAME f-con.
      END.
      OUTPUT CLOSE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wordem wnronot wficha wsernot wped wdatnot wnomef wqtdrec witem wunid 
          witnome wlote wrel w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-3 RECT-4 RECT-5 RECT-6 RECT-7 wordem BUTTON-1 
         bt-sair-3 wnronot wficha wsernot wped wdatnot wnomef wqtdrec witem 
         wunid witnome wlote bt-salva bt-limpa wrel w-imp bt-executar bt-sair-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE imp C-Win 
PROCEDURE imp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FOR EACH avirec WITH FRAME WWW.
/*FORM
     SKIP(2)
     arficha AT 67    /* ficha */
     SKIP(4)
     arfornec AT 06   /* cod-fornec */
     SKIP 
     arnomef  FORMAT "X(12)" AT 06   /* nome fornec */
     arped    FORMAT "zz,zzz,99" /* pedido */ AT 23 space(0) "/" SPACE(0) arordem  /* ordem */
     arlote /* lote */  AT 47
     ardata /* data */  AT 68
     SKIP(6)
     aritem   AT 10   /* item */
     aritnome AT 40   /* desc. item */
     SKIP(5)
     arnronot AT 07   /* numero nota */
     " "
     arsernot          /* serie */
     ardatnot          /* data */
     "        " 
     arqtdrec          /* quantidade */
     arunid   AT 70    /* unidade */ 
     skip(30)
     WITH FRAME WWW NO-LABELS NO-BOX DOWN WIDTH 100.
 */    
 PUT SKIP(2)
     arficha        FORMAT ">>>,>>9"    AT 67    /* ficha */
     SKIP
     arfornec       FORMAT ">>>>>>>>9"  AT 06    /* cod-fornec */
     SKIP 
     arnomef        FORMAT "X(12)"      AT 06    /* nome fornec */
     arped          FORMAT "zz,zzz,99"  AT 20    /* pedido */ 
     space(0) 
     "/" 
     SPACE(0) 
     arordem            /* ordem */
     arlote                             AT 47    /* lote */  
     ardata                             AT 68    /* data */  
     SKIP(6)
     aritem         FORMAT "X(18)"      AT 10   /* item */
     aritnome       FORMAT "X(40)"      AT 40   /* desc. item */
     SKIP(6)
     arnronot                           AT 07   /* numero nota */
     "    "
     arsernot       FORMAT "X(03)"              /* serie */
     "   "
     ardatnot       FORMAT "99/99/99"           /* data */
     "        " 
     arqtdrec                           AT 45   /* quantidade */
     arunid        FORMAT "X(02)"       AT 70   /* unidade */   
     skip(29).
/*DISPLAY arficha  arfornec arnomef  arped    arordem
        arlote   ardata   aritem   aritnome arnronot
        arsernot ardatnot arqtdrec arunid WITH FRAME WWW.
*/
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE imprimir C-Win 
PROCEDURE imprimir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FOR EACH avirec WITH FRAME WWW.
/*FORM
     SKIP(2)
     arficha AT 67    /* ficha */
     SKIP(4)
     arfornec AT 06   /* cod-fornec */
     SKIP 
     arnomef  FORMAT "X(12)" AT 06   /* nome fornec */
     arped    FORMAT "zz,zzz,99" /* pedido */ AT 23 space(0) "/" SPACE(0) arordem  /* ordem */
     arlote /* lote */  AT 47
     ardata /* data */  AT 68
     SKIP(6)
     aritem   AT 10   /* item */
     aritnome AT 40   /* desc. item */
     SKIP(5)
     arnronot AT 07   /* numero nota */
     " "
     arsernot          /* serie */
     ardatnot          /* data */
     "        " 
     arqtdrec          /* quantidade */
     arunid   AT 70    /* unidade */ 
     skip(30)
     WITH FRAME WWW NO-LABELS NO-BOX DOWN WIDTH 100.
 */    
 PUT SKIP(2)
     arficha        FORMAT ">>>,>>9"    AT 67    /* ficha */
     SKIP(4)
     arfornec       FORMAT ">>>>>>>>9"  AT 06    /* cod-fornec */
     SKIP 
     arnomef        FORMAT "X(12)"      AT 06    /* nome fornec */
     arped          FORMAT "zz,zzz,99"  AT 20    /* pedido */ 
     space(0) 
     "/" 
     SPACE(0) 
     arordem            /* ordem */
     arlote                             AT 47    /* lote */  
     ardata                             AT 68    /* data */  
     SKIP(6)
     aritem         FORMAT "X(18)"      AT 10   /* item */
     aritnome       FORMAT "X(40)"      AT 40   /* desc. item */
     SKIP(5)
     arnronot                           AT 07   /* numero nota */
     "    "
     arsernot       FORMAT "X(03)"              /* serie */
     "   "
     ardatnot       FORMAT "99/99/99"           /* data */
     "        " 
     arqtdrec                           AT 45   /* quantidade */
     arunid        FORMAT "X(02)"       AT 70   /* unidade */   
     skip(30).
/*DISPLAY arficha  arfornec arnomef  arped    arordem
        arlote   ardata   aritem   aritnome arnronot
        arsernot ardatnot arqtdrec arunid WITH FRAME WWW.
*/
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN wordem = 0
         wlote = ""
         wnronot = 0
         wsernot = ""
         wqtdrec = 0.
         
  ASSIGN wficha = 0
         witem  = ""
         wped   = 0
         wdatnot = ?
         wnomef = ""
         wunid  = ""
         witnome = "".
        
  DISPLAY wordem wlote wnronot wsernot wqtdrec wrel w-imp WITH FRAME     {&FRAME-NAME}.
  DISPLAY  wficha witem wped wdatnot wnomef wunid witnome WITH FRAME  {&FRAME-NAME}.
  DISABLE  wped wficha wped wnomef witem wnronot wdatnot wqtdrec wunid wlote
           wsernot witnome bt-salva wrel WITH FRAME  {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE limpa-arq C-Win 
PROCEDURE limpa-arq :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FOR EACH avirec.
  DELETE avirec.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

