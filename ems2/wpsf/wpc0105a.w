&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR waltera AS LOGICAL INITIAL NO NO-UNDO.
 /* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.

DEF VAR wnumero-modalidade AS CHAR INITIAL "1, 2, 3, 4, 5, 6, 7, 8, 9, 10".
DEF VAR wchoice-modalidade AS CHAR.
DEF VAR stlist AS CHAR
    INITIAL "Cb Simples,Desconto,Cau��o,Judicial,Repres,Carteira,Vendor,Cheque,Nota Promiss�ria".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-15 RECT-16 RECT-17 RECT-18 RECT-19 ~
bt-ok bt-cancela bt-sair wcliente-ini wcliente-fim wport-atu wport-novo ~
wmodalidade wrel 
&Scoped-Define DISPLAYED-OBJECTS wcliente-ini wcliente-fim wport-atu ~
wport-novo wmodalidade wrel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancela AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-cancel.bmp":U
     LABEL "Cancela Opera��o" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON bt-ok 
     IMAGE-UP FILE "image/im-chck3.bmp":U
     LABEL "Alterar" 
     SIZE 5 BY 1.13 TOOLTIP "Confirma altera��o".

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wmodalidade AS CHARACTER FORMAT "X(16)":U INITIAL "Cb Simples" 
     LABEL "Modalidade" 
     VIEW-AS COMBO-BOX INNER-LINES 9
     LIST-ITEMS "Cb Simples","Desconto","Cau��o","Judicial","Repres","Carteira","Vendor","Cheque","Nota Promiss�ria" 
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE wcliente-fim AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Cliente final" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .88 NO-UNDO.

DEFINE VARIABLE wcliente-ini AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Cliente inicial" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .88 NO-UNDO.

DEFINE VARIABLE wport-atu AS INTEGER FORMAT ">>>>9":U INITIAL 0 
     LABEL "Portador ATUAL" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .88 NO-UNDO.

DEFINE VARIABLE wport-novo AS INTEGER FORMAT ">>>>9":U INITIAL 0 
     LABEL "Portador NOVO" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .88 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(40)":U 
     LABEL "Arquivo para confer�ncia" 
     VIEW-AS FILL-IN 
     SIZE 28 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-15
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 77 BY 1.5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-16
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 1.5.

DEFINE RECTANGLE RECT-17
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 4.75.

DEFINE RECTANGLE RECT-18
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 2.

DEFINE RECTANGLE RECT-19
     EDGE-PIXELS 4 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bt-ok AT ROW 1.75 COL 32
     bt-cancela AT ROW 1.75 COL 37
     bt-sair AT ROW 1.75 COL 42
     wcliente-ini AT ROW 3.5 COL 23 COLON-ALIGNED
     wcliente-fim AT ROW 3.5 COL 48 COLON-ALIGNED
     wport-atu AT ROW 6.25 COL 23 COLON-ALIGNED
     wport-novo AT ROW 6.25 COL 48 COLON-ALIGNED
     wmodalidade AT ROW 8 COL 32 COLON-ALIGNED
     wrel AT ROW 10.25 COL 30 COLON-ALIGNED
     RECT-15 AT ROW 1.5 COL 2
     RECT-16 AT ROW 3.25 COL 2
     RECT-17 AT ROW 5 COL 2
     RECT-18 AT ROW 9.75 COL 2
     RECT-19 AT ROW 12 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 79 BY 12.25
         FONT 1
         DEFAULT-BUTTON bt-sair CANCEL-BUTTON bt-cancela.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Altera��o Portador e Modalidade p/faixa de clientes - wpc0105a"
         HEIGHT             = 12.25
         WIDTH              = 79
         MAX-HEIGHT         = 29.79
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.79
         VIRTUAL-WIDTH      = 146.29
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = 7
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Altera��o Portador e Modalidade p/faixa de clientes - wpc0105a */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Altera��o Portador e Modalidade p/faixa de clientes - wpc0105a */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancela
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancela C-Win
ON CHOOSE OF bt-cancela IN FRAME DEFAULT-FRAME /* Cancela Opera��o */
DO:
  QUIT.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok C-Win
ON CHOOSE OF bt-ok IN FRAME DEFAULT-FRAME /* Alterar */
DO:
  
  DISABLE ALL WITH FRAME {&FRAME-NAME}.
  ASSIGN wcliente-ini wcliente-fim wport-atu wport-novo wrel.
  RUN relatorio.
  ENABLE ALL WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME wmodalidade
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wmodalidade C-Win
ON VALUE-CHANGED OF wmodalidade IN FRAME DEFAULT-FRAME /* Modalidade */
DO:
  ASSIGN wchoice-modalidade = entry(self:LOOKUP(SELF:SCREEN-VALUE),wnumero-modalidade).
  /*MESSAGE  wchoice-modalidade VIEW-AS ALERT-BOX.*/
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wcliente-ini wcliente-fim wport-atu wport-novo wmodalidade wrel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-15 RECT-16 RECT-17 RECT-18 RECT-19 bt-ok bt-cancela bt-sair 
         wcliente-ini wcliente-fim wport-atu wport-novo wmodalidade wrel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN warquivo = wrel.

  get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
  if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
  end.
    
  run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wcliente-ini = 0
       wcliente-fim = 999999
       wrel         = "V:\spool\ALTERAPORT.TXT"
       wport-atu    = 0
       wport-novo   = 0
       wmodalidade  = "CB Simples"
       wchoice-modalidade = "1".
    
DISPLAY wcliente-ini wcliente-fim wrel wport-atu wport-novo WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio C-Win 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
 FORM HEADER
     "SEEBER FASTPLAS LTDA." 
     TODAY FORMAT  "99/99/9999" STRING(TIME,"hh:mm:ss") "hs"
     SKIP
     "Alteracao de portador - "  "Faixa de clientes: "  wcliente-ini "a" wcliente-fim
     SKIP
     "Portador Anterior: "  wport-atu " Portador Atual " wport-novo
     SKIP(1)
     WITH FRAME f-lista DOWN WIDTH 80.
     
 MESSAGE "Confirma a altera��o dos Clientes, do portador:" wport-atu
           "para o portador:" wport-novo 
         SKIP
         " E a modalidade para:" wmodalidade "?"
     VIEW-AS ALERT-BOX BUTTON YES-NO UPDATE waltera.

  OUTPUT TO VALUE(wrel) PAGED.
  IF waltera = YES THEN DO:
  FOR EACH emitente WHERE emitente.cod-emitente  GE wcliente-ini
                  AND   emitente.cod-emitente    LE wcliente-fim 
                  AND   emitente.portador        = wport-atu:

      ASSIGN emitente.portador    = wport-novo
             emitente.port-prefer = wport-novo.
      ASSIGN modalidade = INTEGER(wchoice-modalidade)
             mod-prefer = integer(wchoice-modalidade).
      DISPLAY portador cod-emitente nome-emit 
            WITH FRAME f-lista DOWN CENTERED WIDTH 150.
            
      DOWN WITH FRAME f-lista.
      END. 
   END.
  OUTPUT CLOSE.
  RUN gera-tela.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

