&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */


DEFINE VARIABLE wemp        LIKE estabelec.nome.
DEFINE VARIABLE wqt-it     AS   INTEGER FORMAT "999".
DEFINE VARIABLE wtot-nf    AS CHAR FORMAT "X(17)".
DEFINE VARIABLE wdesenho   AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcgccli    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcgcfor    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcont      AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont1     AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont2     AS INTEGER FORMAT "99".
DEFINE VARIABLE wclass     AS CHAR FORMAT "X(10)".
DEFINE VARIABLE wdif       AS INTE FORMAT 99.
DEFINE VARIABLE wdif1      AS INTE FORMAT 99.
DEFINE VARIABLE wabrecon   AS CHAR FORMAT "X(20)".
DEFINE STREAM   w-log.
DEFINE VARIABLE wfab       AS INTEGER FORMAT "999".

/* codigo item e numero pedido */
DEFINE VARIABLE wconta     AS INTEGER FORMAT 99.
DEFINE VARIABLE wconta1    AS INTEGER FORMAT 99.
DEFINE VARIABLE wnarrativa LIKE item-cli.narrativa[1].
DEFINE VARIABLE wwitem     AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wwpedido   AS CHAR FORMAT "X(12)".
DEFINE VARIABLE wauxped    AS CHAR FORMAT "X(12)".
DEFINE VARIABLE wauxcont   AS INTE FORMAT "99".
DEFINE VARIABLE wvenc      LIKE fat-duplic.dt-venciment.

DEFINE VARIABLE wtot-itens            AS INTEGER.
DEFINE VARIABLE wun-cond              AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wqt-cond              AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wun-mov               AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wdent-embal           AS CHAR FORMAT "X(20)".
DEFINE VARIABLE wpc-embal             AS INTEGER FORMAT "99999999".
DEFINE VARIABLE wnf-embal             AS INTEGER FORMAT "999999".
DEFINE VARIABLE wserie-ori            AS CHAR FORMAT "X(04)".
DEFINE VARIABLE wdt-nf-for            AS INTEGER FORMAT "999999".
DEFINE VARIABLE wqt-embals            AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wae5-espaco           AS CHAR FORMAT "X(03)".

DEFINE VARIABLE wqt-registro          AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wtot-valores          LIKE it-nota-fisc.vl-preuni.
DEFINE VARIABLE wcont-item            AS INTEGER FORMAT "99".
DEFINE VARIABLE witem                 LIKE ITEM.codigo-refer.
DEFINE VARIABLE wseq-it               AS INTEGER FORMAT "999".
DEFINE VARIABLE wtam-item             AS INTEGER FORMAT "99".

DEFINE BUFFER   b-it-auxiliar        FOR it-nota-fisc.
DEFINE VARIABLE wespaco              AS CHAR FORMAT "x(93)".
DEFINE VARIABLE wreceptor            AS CHAR FORMAT "x(25)".
/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR FORMAT "x(40)" NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-6 RECT-7 RECT-8 wcod-est wcod-ini ~
wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog bt-gera bt-sair ~
bt-ajuda 
&Scoped-Define DISPLAYED-OBJECTS wcod-est wcod-ini wdt-ini wdt-fim wnf-ini ~
wnf-fim wserie wrel wrellog 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 13 BY 1.13.

DEFINE BUTTON bt-gera 
     LABEL "Gera arquivo" 
     SIZE 13 BY 1.13.

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 6 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wcod-est AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wcod-ini AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "C�digo do Cliente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-fim AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o Final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnf-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnf-ini AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome do relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE wrellog AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome Rel. de consist�ncia" 
     VIEW-AS FILL-IN 
     SIZE 34 BY 1 NO-UNDO.

DEFINE VARIABLE wserie AS CHARACTER FORMAT "X(5)":U 
     LABEL "S�rie" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 65 BY 10.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 65 BY .5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 65 BY .5
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wcod-est AT ROW 2.75 COL 35 COLON-ALIGNED
     wcod-ini AT ROW 3.75 COL 35 COLON-ALIGNED HELP
          "C�digos: 3500-3501-3502-3504-3507-3512-3513-3517-3518-3519 -833"
     wdt-ini AT ROW 4.75 COL 35 COLON-ALIGNED
     wdt-fim AT ROW 5.75 COL 35 COLON-ALIGNED
     wnf-ini AT ROW 6.75 COL 35 COLON-ALIGNED
     wnf-fim AT ROW 7.75 COL 35 COLON-ALIGNED
     wserie AT ROW 8.75 COL 35 COLON-ALIGNED
     wrel AT ROW 9.75 COL 35 COLON-ALIGNED
     wrellog AT ROW 10.75 COL 35 COLON-ALIGNED
     bt-gera AT ROW 12.25 COL 7
     bt-sair AT ROW 12.25 COL 20
     bt-ajuda AT ROW 12.25 COL 59
     "C�d.Cliente:  3500-3501-3502-3504-3507-3512-3513-3517-3518" VIEW-AS TEXT
          SIZE 54 BY .54 AT ROW 14.25 COL 11
     RECT-6 AT ROW 2.25 COL 7
     RECT-7 AT ROW 13.5 COL 7
     RECT-8 AT ROW 1.75 COL 7
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 77.43 BY 14.29
         FONT 1
         DEFAULT-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Aviso de Embarque - MBB -PADR�O RND 004 V.04 - wpf0116 - v.002"
         HEIGHT             = 14.29
         WIDTH              = 77.43
         MAX-HEIGHT         = 35.21
         MAX-WIDTH          = 205.72
         VIRTUAL-HEIGHT     = 35.21
         VIRTUAL-WIDTH      = 205.72
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Aviso de Embarque - MBB -PADR�O RND 004 V.04 - wpf0116 - v.002 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Aviso de Embarque - MBB -PADR�O RND 004 V.04 - wpf0116 - v.002 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda C-Win
ON CHOOSE OF bt-ajuda IN FRAME DEFAULT-FRAME /* Ajuda */
DO:
  MESSAGE 
        "Os avisos ASN poder�o ser gerados para os seguintes c�digos do cliente VW:"
   SKIP "3500 / 3504 / 3502 / 3507 / 3512 / 3513 / 3501 / 3517 / 3518"
   SKIP "N�O � EMITIDO AVISO PARA ITENS COM FAMILIA FM-89 - VENDAS EXTRAORDIN�RIAS"
   VIEW-AS alert-box.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-gera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-gera C-Win
ON CHOOSE OF bt-gera IN FRAME DEFAULT-FRAME /* Gera arquivo */
DO:
  DISABLE wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog
          bt-gera bt-sair  WITH FRAME {&FRAME-NAME}.
  /* Relatorio de Consistencia */
FORM
    nota-fiscal.nr-nota-fis     COLUMN-LABEL "NF"
    nota-fiscal.cod-emitente    COLUMN-LABEL "Emitente"
    nota-fiscal.dt-emis-nota    COLUMN-LABEL "Dt.nf"
    it-nota-fisc.it-codigo      COLUMN-LABEL "Item"
    witem                       COLUMN-LABEL "Codigo Cliente"
    it-nota-fisc.vl-preuni      COLUMN-LABEL "Preco unit"
    nota-fiscal.vl-tot-nota     COLUMN-LABEL "Vr.Total NF"
    it-nota-fisc.qt-faturada[1] COLUMN-LABEL "Qt.Fat."
    wwpedido                    COLUMN-LABEL "Pedido"
    estabelec.cgc               COLUMN-LABEL "CGC FASTPLAS"
    emitente.cgc                COLUMN-LABEL "CGC MBB"
    it-nota-fisc.class-fiscal   COLUMN-LABEL "Classifica��o"
    nota-fiscal.nat-operacao    COLUMN-LABEL "Natureza de Opera��o"

    skip
HEADER 
  wemp
  TODAY          FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS")
  "HS"
  "Pag.:"                                          AT 69
  PAGE-NUMBER    FORMAT "999"        
  SKIP
  "Arquivos -> Geracao:" wrel "Consistencia:" wrellog
  SKIP(1)
  "** Relatorio de Consistencia de geracao do arquivo de aviso de Embarque MBB **"
  AT 02 SKIP
  "Estabelecimento: " AT 07  wcod-est " / Cod.Emitente:   " wcod-ini " / Serie:   " wserie
  SKIP
  "Referente dias:" AT 07 wdt-ini "a" wdt-fim "Notas:" wnf-ini "a" wnf-fim
  SKIP(1)
  WITH FRAME f-log DOWN WIDTH 300 STREAM-IO.
  
ASSIGN wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog.
  /* Fabricas */ 
       IF wcod-ini = 2261 /* mbb */
       THEN ASSIGN wfab = 540. 
       
       /* AQUI - FALTA CAMPINAS E JUIZ DE FORA */
  
     /*  IF wcod-ini <> 3519 THEN  ASSIGN wreceptor = "VOLKSWAGEN DO BRASIL S/A".*/
    IF wcod-ini = 2261  THEN  ASSIGN wreceptor = "MERCEDES BENZ DO BRASIL S".  

    IF wfab = 0 THEN DO:
      message
      "CLIENTE:" wcod-ini "NAO POSSUI CODIGO DE FABRICA DEFINIDO."
      "SE FOR NOVO C�DIGO ENTRE EM CONTATO COM VALERIA - R. 8527 / INFORM�TICA." 
          VIEW-AS ALERT-BOX.               
      quit.
      END.                                    

  OUTPUT TO VALUE (wrel).
  OUTPUT STREAM w-log TO VALUE(wrellog).
  /* PROCURA CGC */
  
  FIND FIRST estabelec WHERE estabelec.cod-estabel = wcod-est 
                         NO-LOCK NO-ERROR.
  FIND FIRST emitente WHERE emitente.cod-emitente /*emitente.nome-abrev = "MBB" */ = wcod-ini NO-LOCK NO-ERROR.
        /* CGC CLIENTE */
        DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(emitente.cgc,wcont,1)) = 45
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 46
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(emitente.cgc,wcont,1).
          END.
       
        /* CGC FORNECEDOR */
        DO wcont1 = 1 TO 19:
          IF ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  45
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  46
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgcfor = wcgcfor + SUBSTRING(estabelec.cgc,wcont1,1).
          END.
       
  /*ITP*/
  PUT UNFORMATTED 
      "ITP" /* REGISTRO*/ 
      "004" /* TIPO RND 004*/ /* processo*/
      "06"  /* VERS�O RND */ /*vers�o transacao*/
      "00001" /* n.controle transmiss�o*/
      SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99" /* identificacao ger. movto at� hora*/
      MONTH(TODAY) FORMAT "99"
      DAY(TODAY) FORMAT "99"
      SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
      TRIM(wcgcfor) FORMAT "X(14)" /* identificacao transmissor*/
      TRIM(wcgccli) FORMAT "X(14)" /* identificacao receptor*/
      FILL(" ",16)/* cod. interno transmissor(8) e receptor(8)*/
      "SEEBER FASTPLAS LTDA."   /* nome transmissor */
      wreceptor /* nome do receptor */ AT 95
      /*"VOLKSWAGEN DO BRASIL S/A" */ 
      FILL(" ",9)
      SKIP.

    ASSIGN wemp = estabelec.nome.
    FOR EACH nota-fiscal where nota-fiscal.cod-emitente =  wcod-ini
                         AND   nota-fiscal.nr-nota-fis  GE wnf-ini 
                         AND   nota-fiscal.nr-nota-fis  LE wnf-fim
                         AND   nota-fiscal.dt-emis-nota GE wdt-ini
                         AND   nota-fiscal.dt-emis-nota LE wdt-fim
                         AND   nota-fiscal.dt-cancel    = ?
                         AND   nota-fiscal.cod-estabel  = wcod-est
                         AND   nota-fiscal.serie        = wserie
                         NO-LOCK BREAK BY nota-fiscal.dt-emis-nota:

        ASSIGN wtot-itens = 0
               wseq-it    = 0.
       /* buffer para somar total de intes da nota */
       FOR EACH b-it-auxiliar OF nota-fiscal NO-LOCK:
          ASSIGN wtot-itens = wtot-itens + 1.
          END.   

         
       FIND FIRST natur-oper WHERE natur-oper.nat-operacao =
                                   nota-fiscal.nat-operacao
                                   NO-LOCK NO-ERROR.
       IF natur-oper.emite-duplic = yes THEN DO:
       FIND FIRST natur-oper WHERE natur-oper.nat-operacao = 
                                   nota-fiscal.nat-operacao NO-LOCK NO-ERROR.
       ASSIGN wqt-it  = 0
              wcont   = 0
              wcont1  = 0
              wcont2  = 0
              wcgccli = " "
              wcgcfor = " "
              wnarrativa = ""
              wwitem     = ""
              wvenc      = ?.


       FOR EACH doc-fiscal WHERE doc-fiscal.cod-emitente 
                               = nota-fiscal.cod-emitente
                           AND doc-fiscal.dt-emis-doc  
                               = nota-fiscal.dt-emis-nota
                           AND doc-fiscal.nr-doc-fis   
                               = nota-fiscal.nr-nota-fis
                           NO-LOCK:


         FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.nr-nota-fis:

            FIND FIRST ITEM OF it-nota-fisc NO-LOCK.
             
            /* tratamento vw
            /* FM-89 � FAMILIA VENDAS EXTRAORDIN�RIAS N�O EMITE AVISO */
             IF fm-codigo = "FM-89" THEN MESSAGE "!!! OBSERVA��O !!!"
                                    SKIP "A NOTA: " nota-fiscal.nr-nota-fis
                                    "N�O FOI GERADA, POIS A FAM�LIA DO ITEM �:" ITEM.fm-codigo 
                                    "VENDAS EXTRAORDIN�RIAS"
                                    VIEW-AS ALERT-BOX.
             IF fm-codigo = "FM-89" THEN NEXT.
           */
           ASSIGN wqt-it  = wqt-it + 1
                  wclass  = " "
                  wdif    = 0
                  wdif1   = 0
                  wseq-it = wseq-it + 1
                  wtam-item = 0.

         FIND item WHERE item.it-codigo = it-nota-fisc.it-codigo
                   NO-LOCK NO-ERROR.
         ASSIGN witem = "".
         ASSIGN witem = SUBSTRING(codigo-refer,1,17).
 
         /* Tratamento para extrar codigo do cliente e pedido */
         FIND FIRST item-cli 
              WHERE (item-cli.nome-abrev   = nota-fiscal.nome-ab-cli                         
              OR    item-cli.cod-emitente  = nota-fiscal.cod-emitente)
              AND   item-cli.it-codigo    = it-nota-fisc.it-codigo
                    NO-LOCK NO-ERROR.

         IF NOT AVAILABLE item-cli THEN DO:
           MESSAGE item-cli.cod-emitente item-cli.it-codigo 
           "ERROR NOTA FISCAL: " nota-fiscal.nr-nota-fis 
           SKIP "NAO EXISTE RELACIONAMENTO ITEM/CLIENTE PARA:"
           SKIP "CLIENTE:" nota-fiscal.cod-emitente 
           "ITEM:" it-nota-fisc.it-codigo
           VIEW-AS ALERT-BOX.
           LEAVE.
           END.

         /* - = 45 / . = 46 / P = 80 / "/" = 47 / p = 112*/
         IF AVAILABLE item-cli THEN DO:

           ASSIGN wnarrativa = item-cli.narrativa.
             DO wconta = 1 TO 76:
              IF  ASC(SUBSTRING(wnarrativa,wconta,1))     = 80  /*P*/
              OR  ASC(SUBSTRING(wnarrativa,wconta,1))     = 112 /*p*/ 
              THEN DO:
              wconta = wconta + 1.
              IF (ASC(SUBSTRING(wnarrativa,wconta,1))    = 46  /* . */
              OR  ASC(SUBSTRING(wnarrativa,wconta,1))    = 45)  /* - */
              THEN
                 ASSIGN wauxped = SUBSTRING(wnarrativa,wconta - 1,14).
                  ASSIGN wwpedido = SUBSTRING(wauxped,3,12).
                  MESSAGE wwpedido VIEW-AS ALERT-BOX.
                 /*
                 message wauxped view-as alert-box. pause 5.
                 */
                 END.
        
                END.
    /* procura data de vencimento */
    FOR EACH fat-duplic
             where fat-duplic.cod-estabel = nota-fiscal.cod-estabel
             and   fat-duplic.serie       = nota-fiscal.serie
             and   fat-duplic.nr-fatura   = nota-fiscal.nr-fatura
             and   fat-duplic.parcela     = "01" NO-LOCK:
       ASSIGN wvenc = fat-duplic.dt-vencimen.
      END.
    IF wvenc = ? THEN DO:
    MESSAGE "ERROR"
    SKIP "NAO ENCONTROU DATA DE VENCIMENTO PARA NF:" nota-fiscal.nr-nota-fis
    VIEW-AS ALERT-BOX.     
    LEAVE.
    END.
         DISPLAY STREAM w-log
                 nota-fiscal.nr-nota-fis
                 nota-fiscal.cod-emitente 
                 nota-fiscal.dt-emis-nota 
                 it-nota-fisc.it-codigo
                 witem
                 it-nota-fisc.qt-faturada[1] 
                 it-nota-fisc.vl-preuni
                 nota-fiscal.vl-tot-nota 
                 wwpedido
                 estabelec.cgc   
                 emitente.cgc    
                 it-nota-fisc.class-fiscal  
                 nota-fiscal.nat-operacao 
                 WITH FRAME f-log.
      DOWN STREAM w-log WITH FRAME f-log.
     
      /* Classifica��o fiscal */
        DO wcont2 = 1 TO 19:
          IF ASC(SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1)) <> 46
          THEN DO:
            ASSIGN wclass = wclass
                          + SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1).
          END.                   
        END.
        ASSIGN wclass = TRIM(wclass).
          IF LENGTH(wclass) < 10 THEN DO:
            ASSIGN wdif = 10 - LENGTH(wclass).
            DO wdif1 = 1 TO wdif:
              ASSIGN wclass = wclass + "0".
              END.
            END.

         IF FIRST-of(it-nota-fisc.nr-nota-fis) THEN RUN  DISP_ae1.
         RUN DISP_dados.
        END.
      END.
    END.
    END.
   END.
   RUN encerra.
    OUTPUT CLOSE.
    OUTPUT STREAM w-log CLOSE.
    PAUSE 0.
  
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
  ENABLE wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie
         bt-gera bt-sair wrel wrellog WITH FRAME {&FRAME-NAME}.
  RUN gera-tela.
  RUN inicia.
 END.

/*--------------------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE back C-Win 
PROCEDURE back :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO:
  DISABLE wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog
          bt-gera bt-sair  WITH FRAME {&FRAME-NAME}.
  /* Relatorio de Consistencia */
FORM
      "  NF:"               nota-fiscal.nr-nota-fis
      "Cliente:"            nota-fiscal.cod-emitente 
      "Dt:"                 nota-fiscal.dt-emis-nota 
      "Item:"               it-nota-fisc.it-codigo
      SKIP
      "  Vr.Peca:"          it-nota-fisc.vl-preuni NO-LABEL
      "            Vr.Total NF.:"     
                            nota-fiscal.vl-tot-nota NO-LABEL 
      SKIP
      "  Qt.:"              it-nota-fisc.qt-faturada[1]
      "Narrativa:"          item-cli.narrativa NO-LABEL FORMAT "X(40)"
      SKIP(1)
HEADER 
  wemp
  TODAY          FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS")
  "HS"
  "Pag.:"                                          AT 69
  PAGE-NUMBER    FORMAT "999"        
  SKIP
  "Arquivos -> Geracao:" wrel "Consistencia:" wrellog
  SKIP(1)
  "** Relatorio de Consistencia de geracao do arquivo de aviso de Embarque **"
  AT 02 SKIP
  "Referente dias:" AT 07 wdt-ini "a" wdt-fim "Notas:" wnf-ini "a" wnf-fim
  SKIP(1)
  WITH FRAME f-log DOWN WIDTH 150 NO-LABELS.
  
  ASSIGN wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog.
  /* Fabricas */ 
       IF wcod-ini = 3500
       OR wcod-ini = 3504
       OR wcod-ini = 3502
       OR wcod-ini = 3507 THEN ASSIGN wfab = 11.
       IF wcod-ini = 3512 
       OR wcod-ini = 3513 THEN ASSIGN wfab = 18.
       IF wcod-ini = 3501 THEN ASSIGN wfab = 13.
    IF wfab = 0 THEN DO:
      message
      "CLIENTE:" wcod-ini "NAO POSSUI CODIGO DE FABRICA DEFINIDO."
      SKIP
      "ENTRE EM CONTATO COM VALERIA / CPD." VIEW-AS ALERT-BOX.               
      RETURN.
      END.                                    

  OUTPUT TO VALUE (wrel).
  OUTPUT STREAM w-log TO VALUE(wrellog).
  /* PROCURA CGC */
  
  FIND FIRST estabelec WHERE estabelec.cod-estabel = wcod-est 
                         NO-LOCK NO-ERROR.
  FIND FIRST emitente WHERE emitente.nome-abrev = "VW SBC" NO-LOCK NO-ERROR.
        /* CGC CLIENTE */
        DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(emitente.cgc,wcont,1)) = 45
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 46
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(emitente.cgc,wcont,1).
          END.
       
        /* CGC FORNECEDOR */
        DO wcont1 = 1 TO 19:
          IF ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  45
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  46
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgcfor = wcgcfor + SUBSTRING(estabelec.cgc,wcont1,1).
          END.
       
  /*ITP*/
  PUT UNFORMATTED 
      "ITP" /* REGISTRO*/
      "004" /* TIPO RND 004*/
      "15"  /* VERS�O RND */
      "00001"
      SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99"
      MONTH(TODAY) FORMAT "99"
      DAY(TODAY) FORMAT "99"
      SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
      TRIM(wcgccli) FORMAT "X(14)"
      TRIM(wcgcfor) FORMAT "X(14)"
      FILL(" ",16)
      "SEEBER FASTPLAS LTDA."
      "VOLKSWAGEN DO BRASIL S/A" AT 95
      FILL(" ",9)
      SKIP.

    ASSIGN wemp = estabelec.nome.
    FOR EACH nota-fiscal where nota-fiscal.cod-emitente =  wcod-ini
                         AND   nota-fiscal.nr-nota-fis  GE wnf-ini 
                         AND   nota-fiscal.nr-nota-fis  LE wnf-fim
                         AND   nota-fiscal.dt-emis-nota GE wdt-ini
                         AND   nota-fiscal.dt-emis-nota LE wdt-fim
                         AND   nota-fiscal.dt-cancel    = ?
                         AND   nota-fiscal.cod-estabel  = wcod-est
                         AND   nota-fiscal.serie        = wserie
                         NO-LOCK BREAK BY nota-fiscal.dt-emis-nota:
       ASSIGN wtot-itens  = 0.
       /* buffer para somar total de intes da nota */
       FOR EACH b-it-auxiliar OF nota-fiscal NO-LOCK:
          ASSIGN wtot-itens = wtot-itens + 1.
          MESSAGE wtot-itens VIEW-AS ALERT-BOX.
       END.                                        

       FIND FIRST natur-oper WHERE natur-oper.nat-operacao =
                                   nota-fiscal.nat-operacao
                                   NO-LOCK NO-ERROR.
       IF natur-oper.emite-duplic = yes THEN DO:
       
       FIND FIRST natur-oper WHERE natur-oper.nat-operacao = 
                                   nota-fiscal.nat-operacao NO-LOCK NO-ERROR.
       ASSIGN wqt-it  = 0
              wcont   = 0
              wcont1  = 0
              wcont2  = 0
              wcgccli = " "
              wcgcfor = " "
              wnarrativa = ""
              wwitem     = ""
              wvenc      = ?.
       FOR EACH doc-fiscal WHERE doc-fiscal.cod-emitente 
                               = nota-fiscal.cod-emitente
                           AND doc-fiscal.dt-emis-doc  
                               = nota-fiscal.dt-emis-nota
                           AND doc-fiscal.nr-doc-fis   
                               = nota-fiscal.nr-nota-fis
                           NO-LOCK:

         FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK:
           ASSIGN wqt-it  = wqt-it + 1
                  wclass  = " "
                  wdif    = 0
                  wdif1   = 0.

         FIND item WHERE item.it-codigo = it-nota-fisc.it-codigo
                   NO-LOCK NO-ERROR.
         /* Tratamento para extrar codigo do cliente e pedido */
         FIND FIRST item-cli 
              WHERE (item-cli.nome-abrev   = nota-fiscal.nome-ab-cli                         
              OR    item-cli.cod-emitente  = nota-fiscal.cod-emitente)
              AND   item-cli.it-codigo    = it-nota-fisc.it-codigo
                    NO-LOCK NO-ERROR.

         IF NOT AVAILABLE item-cli THEN DO:
           MESSAGE item-cli.cod-emitente item-cli.it-codigo 
           "ERROR NOTA FISCAL: " nota-fiscal.nr-nota-fis 
           SKIP "NAO EXISTE RELACIONAMENTO ITEM/CLIENTE PARA:"
           SKIP "CLIENTE:" nota-fiscal.cod-emitente 
           "ITEM:" it-nota-fisc.it-codigo
           VIEW-AS ALERT-BOX.
           LEAVE.
           END.

         /* - = 45 / . = 46 / P = 80 / "/" = 47 / p = 112*/
         IF AVAILABLE item-cli THEN DO:

           ASSIGN wnarrativa = item-cli.narrativa.
             DO wconta = 1 TO 76:
              IF  ASC(SUBSTRING(wnarrativa,wconta,1))     = 80  /*P*/
              OR  ASC(SUBSTRING(wnarrativa,wconta,1))     = 112 /*p*/ 
              THEN DO:
              wconta = wconta + 1.
              IF (ASC(SUBSTRING(wnarrativa,wconta,1))    = 46  /* . */
              OR  ASC(SUBSTRING(wnarrativa,wconta,1))    = 45)  /* - */
              THEN
                 ASSIGN wauxped = SUBSTRING(wnarrativa,wconta - 1,12).
                 /*
                 message wauxped view-as alert-box. pause 5.
                 */
                 END.
                      
                DO wauxcont = 1 TO 12:
                  IF ASC(SUBSTRING(wauxped,wauxcont,1))  GE 48
                  AND ASC(SUBSTRING(wauxped,wauxcont,1)) LE 57 
                  THEN DO:
                    ASSIGN wwpedido = SUBSTRING(wauxped,wauxcont,7).
                    ASSIGN wauxcont = 12.
                    ASSIGN wwpedido = STRING(INTEGER(wwpedido)).
                    END.
                  END.
                END.
    /* procura data de vencimento */
    FOR EACH fat-duplic
             where fat-duplic.cod-estabel = nota-fiscal.cod-estabel
             and   fat-duplic.serie       = nota-fiscal.serie
             and   fat-duplic.nr-fatura   = nota-fiscal.nr-fatura
             and   fat-duplic.parcela     = "01" NO-LOCK:
       ASSIGN wvenc = fat-duplic.dt-vencimen.
      END.
    IF wvenc = ? THEN DO:
    MESSAGE "ERROR"
    SKIP "NAO ENCONTROU DATA DE VENCIMENTO PARA NF:" nota-fiscal.nr-nota-fis
    VIEW-AS ALERT-BOX.     
    LEAVE.
    END.
         DISPLAY STREAM w-log
                 nota-fiscal.nr-nota-fis
                 nota-fiscal.cod-emitente 
                 nota-fiscal.dt-emis-nota 
                 it-nota-fisc.it-codigo
                 SUBSTRING(item-cli.narrativa,1,20)
                 it-nota-fisc.qt-faturada[1] 
                 it-nota-fisc.vl-preuni
                 nota-fiscal.vl-tot-nota 
                 WITH FRAME f-log.
      DOWN STREAM w-log WITH FRAME f-log.
      /* AE1 */
      PUT UNFORMATTED
      "AE1"
      INTEGER(nota-fiscal.nr-nota-fis)   FORMAT "999999"
            nota-fiscal.serie            FORMAT "X(4)"
            nota-fiscal.dt-emis-nota     FORMAT "999999"
            wtot-itens                   FORMAT "999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),17,2) 
            FORMAT "99"
            /* anterior "2" */
            "0" /* no. casas decimais para quant. */
      SUBSTRING(nota-fiscal.nat-operacao,1,3) FORMAT "99999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),17,2)
            FORMAT "99" 
            wvenc FORMAT "999999"
            "02" /* nota-fiscal.esp-docto */
             SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
            SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),17,2)
            FORMAT "99" 
            wfab FORMAT "999"
            "311299" /* nota-fiscal.dt-entr-cli FORMAT "999999" */
              "    "                  /* periodo de entrega */
            natur-oper.denominacao FORMAT "X(15)" 
            SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99"
            MONTH(TODAY) FORMAT "99"
            DAY(TODAY) FORMAT "99"
            SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
            "   "
            SKIP.

          
      
      /* AE2*/
      PUT UNFORMATTED 
      "AE2"
      it-nota-fisc.nr-seq-fat     FORMAT "999"
      wwpedido                    FORMAT "X(12)"
      /* cod-item */
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),1,7)
      FORMAT "9999999"
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),9,2)
      FORMAT "99"
      it-nota-fisc.un-fatur[1].

                
      /* Classifica��o fiscal */
        DO wcont2 = 1 TO 19:
          IF ASC(SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1)) <> 46
          THEN DO:
            ASSIGN wclass = wclass
                          + SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1).
          END.                   
        END.
        ASSIGN wclass = TRIM(wclass).
          IF LENGTH(wclass) < 10 THEN DO:
            ASSIGN wdif = 10 - LENGTH(wclass).
            DO wdif1 = 1 TO wdif:
              ASSIGN wclass = wclass + "0".
              END.
            END.
      PUT UNFORMATTED
            wclass 
            SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),2,2)
            FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),5,2) 
            FORMAT "99"
            SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999999.99999"),1,10)
            FORMAT "9999999999"
            SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999999.99999"),12,2)
            FORMAT "99"
            "000000000"             /* qt.item estoque       */
            item.un                 FORMAT "X(02)"
            "000000000"             /* qt.unidade compra     */
            "  "                    /* unidade medida compra */
            "P"                     /* tipo de fornecimento  */
            SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),2,2)
            FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),5,2)
            FORMAT "99"
            "00000000000"           /* Vr.tot.desc.item      */
            "    "                  /* alteracao tec.item    */
            " "                     /* espa�o*/
            SKIP
            /*AE4 */
            "AE4"
            SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),2,2)
            FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),5,2)
            FORMAT "99"
      SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),17 ,2)
            FORMAT "99"
      SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),17 ,2)
            FORMAT "99"
      SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),17 ,2)
            FORMAT "99"
            "  "                    /* sit.tributaria            */
            wdesenho                /* numero do desenho do item */
            "311299"                /* dt-val-desenho            */
            "             "         /* PED REVENDA */
      SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),4,3)
            FORMAT "999"
      SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),8,2)
            FORMAT "99"
            "0"
      SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),1,10)
            FORMAT "9999999999"
      SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),12,2)
            FORMAT "99"
            " " /* situacao tributaria      */
            SKIP.
      /* AE5*/
     PUT UNFORMATTED 
      "AE5"
      wun-cond              FORMAT "X(30)"
      wqt-cond              FORMAT "999999999"
      wun-mov               FORMAT "X(30)"
      wdent-embal           FORMAT "X(20)"
      wpc-embal             FORMAT "99999999"
      wnf-embal             FORMAT "999999"
      wserie-ori            FORMAT "X(04)"
      wdt-nf-for            FORMAT "999999"
      wqt-embals            FORMAT "999999999"
      wae5-espaco           FORMAT "X(03)".
                              

   
        END.
      END.
    END.
  END.
    END.
    OUTPUT CLOSE.
    OUTPUT STREAM w-log CLOSE.
    PAUSE 0.
  
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
  ENABLE wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie
         bt-gera bt-sair wrel wrellog WITH FRAME {&FRAME-NAME}.
  RUN inicia.
END.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consistencia C-Win 
PROCEDURE consistencia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Relatorio de Consistencia */

         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_ae1 C-Win 
PROCEDURE disp_ae1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  /* AE1 */
      PUT UNFORMATTED
      "AE1"
      INTEGER(nota-fiscal.nr-nota-fis)   FORMAT "999999"
            CAPS(nota-fiscal.serie)            FORMAT "X(4)"
      SUBSTRING(STRING(YEAR(dt-emis-nota),"9999"),3,4) FORMAT "99"
            MONTH(dt-emis-nota) FORMAT "99"
            DAY(dt-emis-nota) FORMAT "99"
      wtot-itens              FORMAT "999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),17,2) 
            FORMAT "99"
            /* anterior "2" */
            "0" /* no. casas decimais para quant. */
      SUBSTRING(nota-fiscal.nat-operacao,1,3) FORMAT "99999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),17,2)
            FORMAT "99" 
      SUBSTRING(STRING(YEAR(wvenc),"9999"),3,4) FORMAT "99"
            MONTH(wvenc) FORMAT "99"
            DAY(wvenc) FORMAT "99"
            "02" /*nota-fiscal.esp-docto */
             SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
            SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),17,2)
            FORMAT "99" 
            wfab FORMAT "999"
            "991231" /* nota-fiscal.dt-entr-cli FORMAT "999999" */
              "    "                  /* periodo de entrega */
            natur-oper.denominacao FORMAT "X(15)" 
            "          "
           /* vw SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99"
            MONTH(TODAY) FORMAT "99"
            DAY(TODAY) FORMAT "99"
            SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
            "   "*/
            SKIP.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_dados C-Win 
PROCEDURE disp_dados :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

      /* AE2*/
     
      PUT UNFORMATTED 
      "AE2"                                        /* tipo do registro */
      wseq-it     FORMAT "999"    /* num. item na nota */
      wwpedido                    FORMAT "X(12)"  /* pedido */
      witem                       FORMAT "X(30)" /* cod-item */
      /*SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),1,7)
      FORMAT "9999999"
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),9,2)
      FORMAT "99"*/
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"999999999.9999"),1,9)
      FORMAT "999999999"
      it-nota-fisc.un-fatur[1] FORMAT "X(02)"
                  wclass FORMAT "X(10)"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),5,2) 
                  FORMAT "99"
                  SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999.99999"),1,7)
                  FORMAT "9999999"
                  SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999.99999"),9,5)
                  /* corrigido o formato para 5 posi��es e nao 6, conf. sol. marcos gedas 
                  15/09/06" */
                  FORMAT "99999" 
                  "000000000"             /* qt.item estoque       */
                  item.un                 FORMAT "X(02)"
                  "000000000"             /* qt.unidade compra     */
                  "  "                    /* unidade medida compra */
                  "P"                     /* tipo de fornecimento  */
                  SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),5,2)
                  FORMAT "99"
                  /* VW
                  "00000000000"           /* Vr.tot.desc.item      */
                  "    "                  /* alteracao tec.item    */
                  " "                     /* espa�o*/*/
                  "     "
                  SKIP
            /*AE4 */
                  "AE4"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),5,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
                  /*vw "  "  */
                  "00"                    /* sit.tributaria            */
                  wdesenho                /* numero do desenho do item */
                  "311299"                /* dt-val-desenho            */
          "                               "
          SKIP. 
           /* vw        
           "             "         /* PED REVENDA */
            SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),4,3)
                  FORMAT "999"
            SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),8,2)
                  FORMAT "99"
                  "0"
            SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),1,10)
                  FORMAT "9999999999"
            SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),12,2)
                  FORMAT "99"
                  " " /* situacao tributaria      */

                  SKIP.*/
            /* Referente embalagem  AE5*/

           /* Campos de embalagem assinalados conforme orienta��o
           do Sr. Marcos da Gedas em 15/08/2005, para evitar
           mensagens de alerta para o Sr. Nelson Yamaguti 
           Campos: numero da nota embalagem e data iguais 
           aos da nota fiscal de faturamento*/
           
    /*vw  ASSIGN wnf-embal = INTEGER(nota-fiscal.nr-nota-fis).*/
    
         
      /* TOTAL DE TRANSA��ES */
       ASSIGN wqt-registro  =  wqt-registro + 1
              wtot-valores  = wtot-valores + it-nota-fisc.vl-preuni.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-6 RECT-7 RECT-8 wcod-est wcod-ini wdt-ini wdt-fim wnf-ini wnf-fim 
         wserie wrel wrellog bt-gera bt-sair bt-ajuda 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE encerra C-Win 
PROCEDURE encerra :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  PUT UNFORMATTED 
      "FTP"
      "00000"  /*nO.CONTROLE TRANSMISS�O */
      wqt-registro + 1 /* qt registros */ FORMAT "999999999"
      SUBSTRING(STRING(wtot-valores,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(wtot-valores,"999999999999999.99"),17,2) 
            FORMAT "99"
      " "
      wespaco FORMAT "X(03)".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN warquivo = wrellog.
  get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
  if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
  end.
  run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* AVISO */
ASSIGN     wdt-ini    = TODAY
           wdt-fim    = TODAY
           wcod-est   = "1"
           wdesenho   = FILL(" ",30)
           wnf-ini    = "0"
           wnf-fim    = "9999999"
           wrel       = "V:\spool\AVM"   
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
           wrellog    = "V:\spool\CVM"
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
           wserie     = "3"
           wcod-ini   = 0
           wtot-itens = 0
           wqt-registro = 0
           wtot-valores = 0
           wcont-item   = 0
           witem        = ""
           wseq-it      = 0 
           wtam-item    = 0.

DISPLAY wcod-est wdt-ini wcod-ini wdt-fim wnf-ini wnf-fim wserie 
        wrel wrellog WITH FRAME    {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

