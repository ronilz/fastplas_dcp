&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* *** ANTIGO PROGRAMA WP1206 *** */
/* Local Variable Definitions ---                                       */
 DEFINE VARIABLE wemp             LIKE mgadm.estabel.nome.
/* Workfile para controle de saldo */
DEFINE WORKFILE saldo
  FIELD fcod-emitente           LIKE lo-matplano.cod-emitente INITIAL 0
  FIELD femp                    LIKE lo-matplano.emp          INITIAL 0
  FIELD festab                  LIKE lo-matplano.estab        INITIAL ""
  FIELD fnr-pl                  LIKE lo-matplano.nr-pl        INITIAL " "
  FIELD fnro-docto              LIKE lo-matplano.nro-docto    INITIAL 0
  FIELD fit-codigo              LIKE lo-matplano.it-codigo    INITIAL " "
  FIELD fimediato               AS INTE FORMAT "->,>>>,>>9"   INITIAL 0   
  FIELD ftotent                 AS INTE FORMAT ">>>,>>>,>>9"  INITIAL 0
  FIELD fqt-sem1                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem2                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem3                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem4                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem5                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    

  FIELD fqt-sem1-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem2-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem3-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem4-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem5-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fmes1                   AS INTE FORMAT ">,>>>,>>9"    INITIAL 0
  FIELD fmes2                   AS INTE FORMAT ">,>>>,>>9"    INITIAL 0.
/* Totais */
DEFINE VARIABLE wtotent         AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wresul          AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wtotplano       AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wtottela        AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wproxtela       AS LOGICAL FORMAT "Sim/Nao".

DEFINE VARIABLE wdatapesq      LIKE recebimento.data-nota    INITIAL ?.
DEFINE VARIABLE wit-codigo     LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wit-ant        LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wlinha         AS CHAR FORMAT "X(128)".
DEFINE VARIABLE wlinha1        AS CHAR FORMAT "X(128)".
DEFINE VARIABLE wpag           AS INTE FORMAT "9999".
DEFINE VARIABLE wconf-imp      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE warq           AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wtipo          AS INTEGER NO-UNDO. /* tipo de impressora */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 RECT-5 wep westab wcod-emitente ~
wnr-pl wrel w-imp bt-executar bt-sair-2 
&Scoped-Define DISPLAYED-OBJECTS wep westab wcod-emitente wnr-pl wrel w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 11 BY 1.13
     FONT 1.

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wcod-emitente AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "Cod.Fornecedor" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(3)":U INITIAL "0" 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnr-pl AS CHARACTER FORMAT "999999X":U INITIAL "0" 
     LABEL "Numero Plano inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(23)":U 
     LABEL "Nome do Relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 22 BY 1 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 1,
"Impressora", 2,
"Arquivo com compress�o", 3
     SIZE 55 BY 1.13 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 13
     FGCOLOR 8 .

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY 1.41
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wep AT ROW 4.5 COL 38 COLON-ALIGNED
     westab AT ROW 5.5 COL 38 COLON-ALIGNED
     wcod-emitente AT ROW 6.5 COL 38 COLON-ALIGNED
     wnr-pl AT ROW 7.5 COL 38 COLON-ALIGNED
     wrel AT ROW 8.5 COL 38 COLON-ALIGNED
     w-imp AT ROW 10.5 COL 68 RIGHT-ALIGNED NO-LABEL
     bt-executar AT ROW 14.5 COL 35
     bt-sair-2 AT ROW 14.5 COL 46
     RECT-3 AT ROW 1.25 COL 2
     RECT-5 AT ROW 14.29 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81 BY 15.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Saldo do Plano de Entrega de Materiais - wpp0103"
         HEIGHT             = 15
         WIDTH              = 81.43
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = 15
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Saldo do Plano de Entrega de Materiais - wpp0103 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Saldo do Plano de Entrega de Materiais - wpp0103 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN w-imp.
  DISABLE ALL WITH FRAME  {&FRAME-NAME}.
  ASSIGN wep westab wcod-emitente wnr-pl wrel.
  CASE w-imp:
    WHEN 1 THEN DO:
      OUTPUT TO VALUE(wrel) PAGE-SIZE 60.
      RUN imprimir. 
      END. 
     
     WHEN 2 THEN DO:
     /* impressora */
        SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
        IF wconf-imp = YES THEN DO:
           OUTPUT TO PRINTER PAGE-SIZE 44.
          {wpsf/imp.p}
          PUT CONTROL wcomprime wpaisagem a4. 
          RUN imprimir. 
          END.
        END.
      WHEN 3 THEN DO:
       RUN comprime-arquivo.
       END.
     END CASE. 
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo C-Win 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
 DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
 DEFINE VARIABLE a4 AS CHAR.


RUN ../especificos/wpsf/wp9901.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    END.
/* HP */
IF wtipo = 2 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
OUTPUT TO value(wrel) page-size 44.
PUT CONTROL wcomprime wpaisagem a4. 
RUN imprimir.         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_nf C-Win 
PROCEDURE disp_nf :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  CLEAR FRAME f-nf ALL NO-PAUSE.
  ASSIGN wtottela = 0.
    FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     saldo.fcod-emitente
                           AND   INTEGER(recebimento.numero-nota)
                           =     saldo.fnro-docto
                           AND   recebimento.it-codigo
                           =     saldo.fit-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               saldo.fcod-emitente
                         AND   recebimento.it-codigo    =
                               saldo.fit-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   INTEGER(recebimento.numero-nota)  GT
                               saldo.fnro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
      
      ASSIGN wtottela = wtottela + recebimento.quant-receb.           
      HIDE MESSAGE NO-PAUSE.
      DISPLAY recebimento.it-codigo recebimento.numero-nota 
              recebimento.data-nota  recebimento.quant-receb
              COLUMN-LABEL "Quantidade" 
              wtottela LABEL "TOTAL ENTRADA"
              WITH FRAME f-nf CENTERED DOWN ROW 10 OVERLAY.
      DOWN WITH FRAME f-nf.
      END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wep westab wcod-emitente wnr-pl wrel w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-3 RECT-5 wep westab wcod-emitente wnr-pl wrel w-imp bt-executar 
         bt-sair-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE IMPRIMIR C-Win 
PROCEDURE IMPRIMIR :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Form do Saldo Relatorio --------------------------------------------------*/
FORM 
  wlinha1
  SKIP
  "Cod.Item           NfCorte   TOT.ENTRADA        ATRASO"
  lo-matdatas.titsem1   AT 60
  lo-matdatas.titsem2   AT 74  
  lo-matdatas.titsem3   AT 88         
  lo-matdatas.titsem4   AT 102
  lo-matdatas.titsem5   AT 116
  SKIP
    /*
  "__________________ _______ ___________ __________"*/
  "_________" AT 60
  "_________" AT 74
  "_________" AT 88
  "_________" AT 102
  "_________" AT 116

  SKIP
  saldo.fit-codigo  "  "
  saldo.fnro-docto  
  saldo.ftotent    
  saldo.fimediato 
  saldo.fqt-sem1  AT 60
  saldo.fqt-sem2  AT 74
  saldo.fqt-sem3  AT 88
  saldo.fqt-sem4  AT 102
  saldo.fqt-sem5  AT 116
  SKIP
  item.descricao-1
  lo-matdatas.titsem1-2 AT 60 
  lo-matdatas.titsem2-2 AT 74 
  lo-matdatas.titsem3-2 AT 88
  lo-matdatas.titsem4-2 AT 102
  lo-matdatas.titsem5-2 AT 116
  SKIP
    "_________" AT 60
    "_________" AT 74
    "_________" AT 88
    "_________" AT 102
    "_________" AT 116
    
  SKIP
  saldo.fqt-sem1-2 AT 60 
  saldo.fqt-sem2-2 AT 74
  saldo.fqt-sem3-2 AT 88
  saldo.fqt-sem4-2 AT 102
  saldo.fqt-sem5-2 AT 116
  SKIP
  lo-matdatas.titmes1   AT 60
  lo-matdatas.titmes2   AT 74
  SKIP
  "_________" AT 60
  "_________" AT 74
  SKIP
  saldo.fmes1      AT 60
  saldo.fmes2      AT 74
  wlinha
  SKIP(1)
  WITH FRAME f-saldo NO-LABELS DOWN WIDTH 130.
/* Form Cabecalho e Rodape -------------------------------------------------*/
FORM     
  wemp
  SKIP
  "*** SALDO DO PLANO DE ENTREGA DE MATERIAIS NO." 
  wnr-pl FORMAT "99/9999X" "Empresa:" wep "Estabelecimento:" westab "***"
  SKIP
  "Fornecedor:"         AT 45 wcod-emitente     
  emitente.nome-ab
  SKIP
HEADER "Pag.:" AT 110 PAGE-NUMBER FORMAT ">>99"  
  "**** S A L D O ****"  AT 53 
  WITH FRAME f-cabec PAGE-TOP WIDTH 130.
ASSIGN wnr-pl = CAPS(wnr-pl).
FOR EACH lo-matplano WHERE lo-matplano.cod-emitente = wcod-emitente
                       AND   lo-matplano.emp          = wep
                       AND   lo-matplano.estab        = westab
                       AND   lo-matplano.nr-pl        = wnr-pl
                       NO-LOCK:
    ASSIGN wtotent   = 0
           wresul    = 0
           wdatapesq = ?.
    
    /* Cria workfile saldo */
    CREATE saldo.
    ASSIGN saldo.fcod-emitente = lo-matplano.cod-emitente
           saldo.femp          = lo-matplano.emp 
           saldo.festab        = lo-matplano.estab
           saldo.fnr-pl        = lo-matplano.nr-pl    
           saldo.fit-codigo    = lo-matplano.it-codigo
           saldo.fnro-docto    = lo-matplano.nro-docto
           saldo.fimediato     = lo-matplano.imediato
           saldo.fqt-sem1      = lo-matplano.qt-sem1
           saldo.fqt-sem2      = lo-matplano.qt-sem2
           saldo.fqt-sem3      = lo-matplano.qt-sem3
           saldo.fqt-sem4      = lo-matplano.qt-sem4
           saldo.fqt-sem5      = lo-matplano.qt-sem5
           saldo.fqt-sem1-2    = lo-matplano.qt-sem1-2
           saldo.fqt-sem2-2    = lo-matplano.qt-sem2-2
           saldo.fqt-sem3-2    = lo-matplano.qt-sem3-2
           saldo.fqt-sem4-2    = lo-matplano.qt-sem4-2
           saldo.fqt-sem5-2    = lo-matplano.qt-sem5-2
           saldo.fmes1         = lo-matplano.mes1   
           saldo.fmes2         = lo-matplano.mes2.  

    /* Procura / Soma Total de Entradas ------------------------------------*/
       
    FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     lo-matplano.cod-emitente
                           AND   INTEGER(recebimento.numero-nota)
                           =     lo-matplano.nro-docto
                           AND   recebimento.it-codigo
                           =     lo-matplano.it-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               lo-matplano.cod-emitente
                         AND   recebimento.it-codigo    =
                               lo-matplano.it-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   INTEGER(recebimento.numero-nota)  GT
                               lo-matplano.nro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
       ASSIGN wtotent = wtotent + quant-receb.
       ASSIGN saldo.ftotent = wtotent.
       END. 
     
     
     /***** MES 1 ABERTO *****/
     /* ATRASO */
     ASSIGN wresul = (lo-matplano.imediato)
                   - wtotent.
     IF wresul LE 0 THEN ASSIGN fimediato = 0.
     ELSE IF wresul < imediato THEN ASSIGN fimediato = wresul.

     /* Mes 1 aberto - 1a. semana */
     ASSIGN wresul = (lo-matplano.imediato + lo-matplano.qt-sem1) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem1 = 0.
     ELSE IF wresul < fqt-sem1 THEN ASSIGN fqt-sem1 = wresul.
   
    /* Mes 1 aberto - 2a. semana */
     ASSIGN wresul = (lo-matplano.imediato + lo-matplano.qt-sem1 +
                      lo-matplano.qt-sem2)  - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem2 = 0.
     ELSE IF wresul < fqt-sem2 THEN ASSIGN fqt-sem2 = wresul.
    
    /* Mes 1 aberto - 3a. semana */
     ASSIGN wresul = (lo-matplano.imediato 
                   + lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   + lo-matplano.qt-sem3) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem3 = 0.
     ELSE IF wresul < fqt-sem3 THEN ASSIGN fqt-sem3 = wresul.

     /* Mes 1 aberto - 4a. semana */
     ASSIGN wresul = (lo-matplano.imediato 
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem4 = 0.
     ELSE IF wresul < fqt-sem4 THEN ASSIGN fqt-sem4 = wresul.

     /* Mes 1 aberto - 5a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem5 = 0.
     ELSE IF wresul < fqt-sem5 THEN ASSIGN fqt-sem5 = wresul.

     /* Mes 2 aberto - 1a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem1-2 = 0.
     ELSE IF wresul < fqt-sem1-2 THEN ASSIGN fqt-sem1-2 = wresul.
    
     /* Mes 2 aberto - 2a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem2-2 = 0.
     ELSE IF wresul < fqt-sem2-2 THEN ASSIGN fqt-sem2-2 = wresul.
     
     /* Mes 2 aberto - 3a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem3-2 = 0.
     ELSE IF wresul < fqt-sem3-2 THEN ASSIGN fqt-sem3-2 = wresul.

     /* Mes 2 aberto - 4a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem4-2 = 0.
     ELSE IF wresul < fqt-sem4-2 THEN ASSIGN fqt-sem4-2 = wresul.

     /* Mes 2 aberto - 5a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem5-2 = 0.
     ELSE IF wresul < fqt-sem5-2 THEN ASSIGN fqt-sem5-2 = wresul.

     /* Mes 1 FECHADO */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2 
                   +  lo-matplano.mes1)
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fmes1 = 0.
     ELSE IF wresul < fmes1 THEN ASSIGN fmes1 = wresul.

     /* Mes 2 FECHADO */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2 
                   +  lo-matplano.mes1 + lo-matplano.mes2)
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fmes2 = 0.
     ELSE IF wresul < fmes2 THEN ASSIGN fmes2 = wresul.
     
     ASSIGN wtotplano = (lo-matplano.imediato 
               + lo-matplano.qt-sem1 + lo-matplano.qt-sem2 
               + lo-matplano.qt-sem3 + lo-matplano.qt-sem4
               + lo-matplano.qt-sem1-2 + lo-matplano.qt-sem2-2
               + lo-matplano.qt-sem3-2 + lo-matplano.qt-sem4-2
               + lo-matplano.qt-sem5-2 + lo-matplano.mes1
               + lo-matplano.mes2).
     
     IF wtotent > wtotplano THEN 
     ASSIGN fimediato = (wtotplano - wtotent).
   END.

/* DISPLAY DOS DADOS DO SALDO ----------------------------------------------*/
DO:
  HIDE MESSAGE NO-PAUSE.
  FIND FIRST emitente WHERE emitente.cod-emitente = wcod-emitente NO-LOCK
                      NO-ERROR.
  FIND FIRST mgadm.estabel WHERE estabel.ep-codigo = string(wep)
                     AND   estabel.cod-estabel = westab 
                     NO-LOCK NO-ERROR.
  ASSIGN wemp = estabel.nome.
    DISPLAY wemp wnr-pl wcod-emitente wep westab
            emitente.nome-ab 
            WITH FRAME f-cabec NO-LABELS PAGE-TOP.
    FOR EACH saldo WHERE saldo.fcod-emitente  = wcod-emitente
                   AND   saldo.femp           = wep
                   AND   saldo.festab         = westab
                   AND   saldo.fnr-pl         = wnr-pl.
      FIND FIRST lo-matdatas 
                 WHERE lo-matdatas.emp   = saldo.femp
                 AND   lo-matdatas.estab = saldo.festab
                 AND   lo-matdatas.nr-pl = SUBSTRING(saldo.fnr-pl,1,6)
                 NO-LOCK.
      FIND FIRST item    WHERE item.it-codigo = saldo.fit-codigo 
                         NO-LOCK NO-ERROR. 
        DISPLAY /* linha 1 */
            wlinha1
            lo-matdatas.titsem1 lo-matdatas.titsem2 lo-matdatas.titsem3 
            lo-matdatas.titsem4 lo-matdatas.titsem5
            saldo.fit-codigo saldo.fnro-docto saldo.ftotent
            saldo.fimediato 
            saldo.fqt-sem1 saldo.fqt-sem2 saldo.fqt-sem3
            saldo.fqt-sem4 saldo.fqt-sem5 
            /* linha 2 */
            item.descricao-1 
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2
            lo-matdatas.titsem5-2 lo-matdatas.titmes1 lo-matdatas.titmes2
            saldo.fqt-sem1-2 saldo.fqt-sem2-2 saldo.fqt-sem3-2
            saldo.fqt-sem4-2 saldo.fqt-sem5-2 saldo.fmes1
            saldo.fmes2 
            wlinha    
        WITH FRAME f-saldo OVERLAY NO-LABELS DOWN.
        DOWN WITH FRAME f-saldo.
        END.
      END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   ASSIGN wep           = 1
          westab        = "1"
          wcod-emitente = 0
          wnr-pl        = ""
          wrel          = "V:\spool\SALDO.TXT"
          wlinha        = FILL("_",128)
          wlinha1       = FILL("_",128).
   DISPLAY wep westab wcod-emitente wnr-pl wrel
          WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE it_video C-Win 
PROCEDURE it_video :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*DISPLAY /* linha 1 */
            lo-matdatas.titsem1 lo-matdatas.titsem2 lo-matdatas.titsem3 
            lo-matdatas.titsem4 lo-matdatas.titsem5
            saldo.fit-codigo @ wit-codigo saldo.fnro-docto saldo.ftotent
            saldo.fimediato 
            saldo.fqt-sem1 saldo.fqt-sem2 saldo.fqt-sem3
            saldo.fqt-sem4 saldo.fqt-sem5 
            /* linha 2 */
            item.descricao-1 
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2
            lo-matdatas.titsem5-2 lo-matdatas.titmes1 lo-matdatas.titmes2
            saldo.fqt-sem1-2 saldo.fqt-sem2-2 saldo.fqt-sem3-2
            saldo.fqt-sem4-2 saldo.fqt-sem5-2 saldo.fmes1
            saldo.fmes2 
            wlinha    
            WITH FRAME f-salvideo NO-LABELS WIDTH 125.
            DOWN WITH FRAME f-salvideo.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

