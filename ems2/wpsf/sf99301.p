/*****************************************************************************
**       Programa: sf99301.p
**       Data....: 17/05/06
**       Autor...: DATASUL S.A.
**       Objetivo: Relat�rio de Titulos em Cobran�a Banc�ria
**       Vers�o..: 1.00.000 - super
**       OBS.....: Este fonte foi gerado pelo Data Viewer 3.00
*******************************************************************************/

define buffer empresa for mgcad.empresa. 

define variable c-prog-gerado 

 as character no-undo initial "SF99301".

def new global shared var c-arquivo-log    as char  format "x(60)"no-undo.
def new global shared var c-prg-vrs as char no-undo.
def new global shared var c-prg-obj as char no-undo.

run grapi/gr2013.p (input c-prog-gerado, input "2.00.00.000").

/****************** Defini��o de Tabelas Tempor�rias do Relat�rio **********************/

define temp-table tt-raw-digita
    field raw-digita as raw.

define temp-table tt-param
    field destino              as integer
    field arquivo              as char
    field usuario              as char
    field data-exec            as date
    field hora-exec            as integer
    field parametro            as logical
    field formato              as integer
    field v_num_tip_aces_usuar as integer
    field ep-codigo            as integer
    field c-cod-estabel-ini like titulo.cod-estabel
    field c-cod-estabel-fim like titulo.cod-estabel
    field da-dt-vencimen-ini like titulo.dt-vencimen
    field da-dt-vencimen-fim like titulo.dt-vencimen
    field i-cod-emitente-ini like titulo.cod-emitente
    field i-cod-emitente-fim like titulo.cod-emitente
.

/****************** INCLUDE COM VARI�VEIS GLOBAIS *********************/

def new global shared var i-ep-codigo-usuario  like mguni.empresa.ep-codigo no-undo.
def new Global shared var l-implanta           as logical    init no.
def new Global shared var c-seg-usuario        as char format "x(12)" no-undo.
def new global shared var i-num-ped-exec-rpw  as integer no-undo.   
def new global shared var i-pais-impto-usuario as integer format ">>9" no-undo.
def new global shared var l-rpc as logical no-undo.
def new global shared var r-registro-atual as rowid no-undo.
def new global shared var c-arquivo-log    as char  format "x(60)"no-undo.
def new global shared var i-num-ped as integer no-undo.         
def new global shared var v_cdn_empres_usuar   like mguni.empresa.ep-codigo        no-undo.
def new global shared var v_cod_usuar_corren   like usuar_mestre.cod_usuario no-undo.
def new global shared var h_prog_segur_estab     as handle                   no-undo.
def new global shared var v_cod_grp_usuar_lst    as char                     no-undo.
def new global shared var v_num_tip_aces_usuar   as int                      no-undo.
def new global shared var rw-log-exec            as rowid                    no-undo.


def new global shared var c-dir-spool-servid-exec as CHAR no-undo.
/****************** Defini�ao de Par�metros do Relat�rio *********************/ 

/****************** Defini�ao de Vari�veis de Sele��o do Relat�rio *********************/ 

def new shared var c-cod-estabel-ini like titulo.cod-estabel format "x(3)" initial "" no-undo.
def new shared var c-cod-estabel-fim like titulo.cod-estabel format "x(3)" initial "ZZZ" no-undo.
def new shared var da-dt-vencimen-ini like titulo.dt-vencimen format "99/99/9999" initial "01/01/1800" no-undo.
def new shared var da-dt-vencimen-fim like titulo.dt-vencimen format "99/99/9999" initial "12/31/9999" no-undo.
def new shared var i-cod-emitente-ini like titulo.cod-emitente format ">>>>>>>>9" initial 0 no-undo.
def new shared var i-cod-emitente-fim like titulo.cod-emitente format ">>>>>>>>9" initial 999999999 no-undo.

/****************** Defini�ao de Vari�veis p/ Campos Virtuais do Relat�rio *******************/ 

/****************** Defini�ao de Vari�veis Campo Calculado do Relat�rio **********************/ 

/****************** Defini�ao de Vari�veis do Relat�rio N�o Pedidas em Tela ******************/ 

/****************** Defini�ao de Vari�veis de Total do Relat�rio *****************************/ 

def var de-vl-liquido-tt-002 like titulo.vl-liquido no-undo.
def var de-vl-liquido-tt-005 like titulo.vl-liquido no-undo.
def var de-vl-liquido-tt-008 like titulo.vl-liquido no-undo.
def var de-vl-original-tt-001 like titulo.vl-original no-undo.
def var de-vl-original-tt-004 like titulo.vl-original no-undo.
def var de-vl-original-tt-007 like titulo.vl-original no-undo.
def var de-vl-saldo-tt-003 like titulo.vl-saldo no-undo.
def var de-vl-saldo-tt-006 like titulo.vl-saldo no-undo.
def var de-vl-saldo-tt-009 like titulo.vl-saldo no-undo.

/****************** Defini�ao de Vari�veis dos Calculos do Relat�rio *************************/ 

def input param raw-param as raw no-undo.
def input param table for tt-raw-digita.

/***************** Defini�ao de Vari�veis de Processamento do Relat�rio *********************/

def var h-acomp              as handle no-undo.
def var h-FunctionLibrary    as handle no-undo.
def var v-cod-destino-impres as char   no-undo.
def var v-num-reg-lidos      as int    no-undo.
def var v-num-point          as int    no-undo.
def var v-num-set            as int    no-undo.
def var v-num-linha          as int    no-undo.
def var v-cont-registro      as int    no-undo.
def var v-des-retorno        as char   no-undo.
def var v-des-local-layout   as char   no-undo.

/****************** Defini�ao de Forms do Relat�rio 132 Colunas ***************************************/ 

form titulo.cod-estabel column-label "Est" format "x(3)" at 001
     with down width 132 no-box stream-io frame f-relat-01-132.

form titulo.dt-vencimen   format "99/99/9999"          column-label "Dt Vcto"
     titulo.cod-port      format ">>>>9"               column-label "Port" 
     titulo.cod-emitente                               COLUMN-LABEL "Cliente "
     emitente.nome-abrev  format "X(12)"               column-label "Nome Abreviado" 
     titulo.titulo-banco  format "x(20)"               column-label "Num Titulo Banco"
     titulo.nr-docto      format "x(16)"               column-label "Documento"
     titulo.parcela       format "x(2)"                column-label "/P"
     titulo.vl-original   format ">>>>>>>,>>9.99"      column-label "Vl Original "  
     titulo.vl-liquido    format ">>>,>>>,>>>,>>9.99"  column-label "Vl L�quido " 
     titulo.vl-saldo      format ">>>>>>>,>>9.99"      column-label "Valor Saldo "
     with down width 150 no-box stream-io frame f-relat-09-132.

create tt-param.
raw-transfer raw-param to tt-param.

def temp-table tt-editor no-undo
    field linha      as integer
    field conteudo   as character format "x(80)"
    index editor-id is primary unique linha.


def var rw-log-exec                            as rowid no-undo.
def var c-erro-rpc as character format "x(60)" initial " " no-undo.
def var c-erro-aux as character format "x(60)" initial " " no-undo.
def var c-ret-temp as char no-undo.
def var h-servid-rpc as handle no-undo.     
define var c-empresa       as character format "x(40)"      no-undo.
define var c-titulo-relat  as character format "x(50)"      no-undo.
define var i-numper-x      as integer   format "ZZ"         no-undo.
define var da-iniper-x     as date      format "99/99/9999" no-undo.
define var da-fimper-x     as date      format "99/99/9999" no-undo.
define var i-page-size-rel as integer                       no-undo.
define var c-programa      as character format "x(08)"      no-undo.
define var c-versao        as character format "x(04)"      no-undo.
define var c-revisao       as character format "999"        no-undo.

define new shared var c-impressora   as character                      no-undo.
define new shared var c-layout       as character                      no-undo.
define new shared var v_num_count     as integer                       no-undo.
define new shared var c-arq-control   as character                     no-undo.
define new shared var c-sistema       as character format "x(25)"      no-undo.
define new shared var c-rodape        as character                     no-undo.

define new shared buffer b_ped_exec_style for ped_exec.
define new shared buffer b_servid_exec_style for servid_exec.

define new shared stream str-rp.

assign c-programa     = "sf99301"
       c-versao       = "2.00"
       c-revisao      = ".00.000"
       c-titulo-relat = "Relat�rio de Titulos em Cobran�a Banc�ria"
       c-sistema      = "".


find first mguni.empresa no-lock
    where mguni.empresa.ep-codigo = i-ep-codigo-usuario no-error.
if  avail mguni.empresa
then
    assign c-empresa = mguni.empresa.razao-social.
else
    assign c-empresa = "".

if  tt-param.formato = 2 then do:


form header
    fill("-", 132) format "x(132)" skip
    c-empresa c-titulo-relat at 50
    "Folha:" at 122 page-number(str-rp) at 128 format ">>>>9" skip
    fill("-", 112) format "x(110)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabec.

form header
    fill("-", 132) format "x(132)" skip
    c-empresa c-titulo-relat at 50
    "Folha:" at 122 page-number(str-rp) at 128 format ">>>>9" skip
    "Periodo:" i-numper-x at 08 "-"
    da-iniper-x at 14 "to" da-fimper-x
    fill("-", 74) format "x(72)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabper.

run grapi/gr2004.p.

form header
    c-rodape format "x(132)"
    with stream-io width 132 no-labels no-box page-bottom frame f-rodape.


end. /* tt-param.formato = 2 */


run grapi/gr2009.p (input tt-param.destino,
                    input tt-param.arquivo,
                    input tt-param.usuario,
                    input no).

assign i-ep-codigo-usuario = string(tt-param.ep-codigo)
       v_cdn_empres_usuar  = i-ep-codigo-usuario
       c-cod-estabel-ini = tt-param.c-cod-estabel-ini
       c-cod-estabel-fim = tt-param.c-cod-estabel-fim
       da-dt-vencimen-ini = tt-param.da-dt-vencimen-ini
       da-dt-vencimen-fim = tt-param.da-dt-vencimen-fim
       i-cod-emitente-ini = tt-param.i-cod-emitente-ini
       i-cod-emitente-fim = tt-param.i-cod-emitente-fim.


def var l-imprime as logical no-undo.


        assign de-vl-liquido-tt-002 = 0
               de-vl-original-tt-001 = 0
               de-vl-saldo-tt-003 = 0.
assign l-imprime = no.
if  tt-param.destino = 1 then
    assign v-cod-destino-impres = "Impressora".
else
    if  tt-param.destino = 2 then
        assign v-cod-destino-impres = "Arquivo".
    else
        assign v-cod-destino-impres = "Terminal".


run utp/ut-acomp.p persistent set h-acomp.

run pi-inicializar in h-acomp(input "Acompanhamento Relat�rio").

assign v-num-reg-lidos = 0.

/* gr9020a.p */

for each titulo WHERE (titulo.cod-emitente GE i-cod-emitente-ini 
                and    titulo.cod-emitente LE i-cod-emitente-fim) 
                AND   (titulo.cod-estabel  GE c-cod-estabel-ini 
                and    titulo.cod-estabel  LE c-cod-estabel-fim)
                AND   (titulo.dt-vencimen  GE da-dt-vencimen-ini
                and    titulo.dt-vencimen  LE da-dt-vencimen-fim)
                AND   (titulo.modalidade  = 1 or
                       titulo.modalidade  = 2 or
                       titulo.modalidade  = 3 or
                       titulo.modalidade  = 4) no-lock
          break by titulo.cod-estabel
          by titulo.dt-vencimen
          by titulo.cod-emitente:
   FIND FIRST emitente WHERE   emitente.cod-emitente = titulo.cod-emitente NO-LOCK NO-ERROR.

    assign v-num-reg-lidos = v-num-reg-lidos + 1.
    run pi-acompanhar in h-acomp(input string(v-num-reg-lidos)).

    if  first-of(titulo.cod-estabel) then do:
        assign de-vl-liquido-tt-005 = 0
               de-vl-original-tt-004 = 0
               de-vl-saldo-tt-006 = 0.
    end.
    if  first-of(titulo.dt-vencimen) then do:
        assign de-vl-liquido-tt-008 = 0
               de-vl-original-tt-007 = 0
               de-vl-saldo-tt-009 = 0.
    end.
    assign de-vl-original-tt-001 = de-vl-original-tt-001 + 
                                           titulo.vl-original
           de-vl-liquido-tt-002 = de-vl-liquido-tt-002 + 
                                          titulo.vl-liquido
           de-vl-saldo-tt-003 = de-vl-saldo-tt-003 + 
                                        titulo.vl-saldo
           de-vl-original-tt-004 = de-vl-original-tt-004 + 
                                           titulo.vl-original
           de-vl-liquido-tt-005 = de-vl-liquido-tt-005 + 
                                          titulo.vl-liquido
           de-vl-saldo-tt-006 = de-vl-saldo-tt-006 + 
                                        titulo.vl-saldo
           de-vl-original-tt-007 = de-vl-original-tt-007 + 
                                           titulo.vl-original
           de-vl-liquido-tt-008 = de-vl-liquido-tt-008 + 
                                          titulo.vl-liquido
           de-vl-saldo-tt-009 = de-vl-saldo-tt-009 + 
                                        titulo.vl-saldo.

    /***  C�DIGO PARA SA�DA EM 132 COLUNAS ***/

    if  tt-param.formato = 2 then do:

        view stream str-rp frame f-cabec.
        view stream str-rp frame f-rodape.
        assign l-imprime = yes.
        if  first-of(titulo.cod-estabel) then do:
            display stream str-rp " " with stream-io no-box frame f-branco.
            display stream str-rp titulo.cod-estabel
                    with stream-io frame f-relat-01-132.
            display stream str-rp " " with stream-io no-box frame f-branco.
        end.

        display stream str-rp titulo.dt-vencimen
            titulo.cod-port
            titulo.cod-emitente
            emitente.nome-abrev
            titulo.titulo-banco
            titulo.nr-docto
            titulo.parcela
            titulo.vl-original
            titulo.vl-liquido
            titulo.vl-saldo
                with stream-io frame f-relat-09-132.
            down stream str-rp with frame f-relat-09-132.
    end.
    if  last-of(titulo.dt-vencimen) then do:
        if  tt-param.formato = 2 then do:
            display stream str-rp "------------------" @ 
                titulo.vl-liquido
                "--------------" @ 
                titulo.vl-original
                "--------------" @ 
                titulo.vl-saldo
                with stream-io frame f-relat-09-132.
            down stream str-rp with frame f-relat-09-132.
        end.
        else  do:
            display stream str-rp "------------------" @ 
                titulo.vl-liquido
                "--------------" @ 
                titulo.vl-original
                "--------------" @ 
                titulo.vl-saldo
                with stream-io frame f-relat-09-80.
            down stream str-rp with frame f-relat-09-80.
        end.
        put stream str-rp de-vl-original-tt-007 format ">>>>>>>,>>9.99" to 098.
        put stream str-rp de-vl-liquido-tt-008 format ">>>,>>>,>>>,>>9.99" to 117.
        put stream str-rp de-vl-saldo-tt-009 format ">>>>>>>,>>9.99" to 132.
        put stream str-rp unformatted skip(1).
       put stream str-rp unformatted skip(1).
    end.
    if  last-of(titulo.cod-estabel) then do:
        if  tt-param.formato = 2 then do:
            display stream str-rp "------------------" @ 
                titulo.vl-liquido
                "--------------" @ 
                titulo.vl-original
                "--------------" @ 
                titulo.vl-saldo
                with stream-io frame f-relat-09-132.
            down stream str-rp with frame f-relat-09-132.
        end.
        else  do:
            display stream str-rp "------------------" @ 
                titulo.vl-liquido
                "--------------" @ 
                titulo.vl-original
                "--------------" @ 
                titulo.vl-saldo
                with stream-io frame f-relat-09-80.
            down stream str-rp with frame f-relat-09-80.
        end.
        put stream str-rp de-vl-original-tt-004 format ">>>>>>>,>>9.99" to 098.
        put stream str-rp de-vl-liquido-tt-005 format ">>>,>>>,>>>,>>9.99" to 117.
        put stream str-rp de-vl-saldo-tt-006 format ">>>>>>>,>>9.99" to 132.
        put stream str-rp unformatted skip(1).
       put stream str-rp unformatted skip(1).
    end.
    
end.


if  l-imprime = no then do:
    if  tt-param.formato = 2 then do:
        view stream str-rp frame f-cabec.
        view stream str-rp frame f-rodape.
    end.
    disp stream str-rp " " with stream-io frame f-nulo.
end.
    display stream str-rp "------------------" @ 
            titulo.vl-liquido
                "--------------" @ 
            titulo.vl-original
                "--------------" @ 
            titulo.vl-saldo
            with stream-io frame f-relat-09-132.
    down stream str-rp with frame f-relat-09-132.
        put stream str-rp de-vl-original-tt-001 format ">>>>>>>,>>9.99" to 098.
        put stream str-rp de-vl-liquido-tt-002 format ">>>,>>>,>>>,>>9.99" to 117.
        put stream str-rp de-vl-saldo-tt-003 format ">>>>>>>,>>9.99" to 132.

if  tt-param.destino <> 1 then

    page stream str-rp.

else do:

    if   tt-param.parametro = yes then

         page stream str-rp.

end.

if  tt-param.parametro then do:


   disp stream str-rp "SELE��O" skip(01) with stream-io frame f-imp-sel.
   disp stream str-rp 
      c-cod-estabel-ini colon 18 "|< >|"   at 42 c-cod-estabel-fim no-label
      da-dt-vencimen-ini colon 18 "|< >|"   at 42 da-dt-vencimen-fim no-label
      i-cod-emitente-ini colon 18 "|< >|"   at 42 i-cod-emitente-fim no-label
        with stream-io side-labels overlay row 034 frame f-imp-sel.

   put stream str-rp unformatted skip(1) "IMPRESS�O" skip(1).

   put stream str-rp unformatted skip "    " "Destino : " v-cod-destino-impres " - " tt-param.arquivo format "x(40)".
   put stream str-rp unformatted skip "    " "Execu��o: " if i-num-ped-exec-rpw = 0 then "On-Line" else "Batch".
   put stream str-rp unformatted skip "    " "Formato : " if tt-param.formato = 1 then "80 colunas" else "132 colunas".
   put stream str-rp unformatted skip "    " "Usu�rio : " tt-param.usuario.

end.

    output stream str-rp close.

procedure pi-print-editor:

    def input param c-editor    as char    no-undo.
    def input param i-len       as integer no-undo.

    def var i-linha  as integer no-undo.
    def var i-aux    as integer no-undo.
    def var c-aux    as char    no-undo.
    def var c-ret    as char    no-undo.

    for each tt-editor:
        delete tt-editor.
    end.

    assign c-ret = chr(255) + chr(255).

    do  while c-editor <> "":
        if  c-editor <> "" then do:
            assign i-aux = index(c-editor, chr(10)).
            if  i-aux > i-len or (i-aux = 0 and length(c-editor) > i-len) then
                assign i-aux = r-index(c-editor, " ", i-len + 1).
            if  i-aux = 0 then
                assign c-aux = substr(c-editor, 1, i-len)
                       c-editor = substr(c-editor, i-len + 1).
            else
                assign c-aux = substr(c-editor, 1, i-aux - 1)
                       c-editor = substr(c-editor, i-aux + 1).
            if  i-len = 0 then
                assign entry(1, c-ret, chr(255)) = c-aux.
            else do:
                assign i-linha = i-linha + 1.
                create tt-editor.
                assign tt-editor.linha    = i-linha
                       tt-editor.conteudo = c-aux.
            end.
        end.
        if  i-len = 0 then
            return c-ret.
    end.
    return c-ret.
end procedure.

IF VALID-HANDLE(h-acomp) THEN /*gr9030g*/
    RUN pi-finalizar IN h-acomp NO-ERROR.

return 'OK'.

/* fim do programa */
