&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wconf AS LOGICAL FORMAT "Sim/Nao".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BUTTON-5 wemi-ini wemi-fim wcond-ant ~
wcond-new wrel wrel-alt b-lista b-altera bt-conf-alt bt-cancelar bt-ajuda ~
IMAGE-8 IMAGE-9 RECT-2 RECT-7 RECT-8 
&Scoped-Define DISPLAYED-OBJECTS wemi-ini wemi-fim wcond-ant wcond-new wrel ~
wrel-alt 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON b-altera 
     LABEL "Alterar" 
     SIZE 9 BY 1.

DEFINE BUTTON b-lista 
     LABEL "Relat�rio" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Fechar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-conf-alt 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Button 1" 
     SIZE 5 BY 1.04 TOOLTIP "Abre Campo".

DEFINE BUTTON BUTTON-5 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Button 5" 
     SIZE 4 BY 1.13.

DEFINE VARIABLE wcond-ant AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Condi��o de Pagamento Atual" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE VARIABLE wcond-new AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "NOVA CONDI��O DE PAGAMENTO" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88
     FONT 0 NO-UNDO.

DEFINE VARIABLE wemi-fim AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 10 BY .88 NO-UNDO.

DEFINE VARIABLE wemi-ini AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .88 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome do Relat�rio (op��o lista)" 
     VIEW-AS FILL-IN 
     SIZE 28 BY .88 NO-UNDO.

DEFINE VARIABLE wrel-alt AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome do Relat�rio (op��o altera��o)" 
     VIEW-AS FILL-IN 
     SIZE 28 BY .88 NO-UNDO.

DEFINE IMAGE IMAGE-8
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-9
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1.5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 3 GRAPHIC-EDGE  NO-FILL 
     SIZE 87 BY 1.25.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 87 BY 13.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     BUTTON-5 AT ROW 2.25 COL 84
     wemi-ini AT ROW 4.75 COL 35 COLON-ALIGNED
     wemi-fim AT ROW 4.75 COL 52 COLON-ALIGNED NO-LABEL
     wcond-ant AT ROW 5.75 COL 35 COLON-ALIGNED
     wcond-new AT ROW 8 COL 35 COLON-ALIGNED
     wrel AT ROW 10 COL 35 COLON-ALIGNED
     wrel-alt AT ROW 11.5 COL 35 COLON-ALIGNED
     b-lista AT ROW 17 COL 3
     b-altera AT ROW 17 COL 14
     bt-conf-alt AT ROW 17 COL 23
     bt-cancelar AT ROW 17 COL 68 HELP
          "Fechar"
     bt-ajuda AT ROW 17 COL 78 HELP
          "Ajuda"
     RECT-6 AT ROW 16.75 COL 2
     IMAGE-8 AT ROW 4.75 COL 51
     IMAGE-9 AT ROW 4.75 COL 48
     RECT-2 AT ROW 2 COL 2
     RECT-7 AT ROW 18.25 COL 2
     RECT-8 AT ROW 3.5 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 88.72 BY 18.58
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Listagem e Altera��o de Condi��o de Pagto Emitentes - wpp0106"
         HEIGHT             = 18.58
         WIDTH              = 88.72
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = 15
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Listagem e Altera��o de Condi��o de Pagto Emitentes - wpp0106 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Listagem e Altera��o de Condi��o de Pagto Emitentes - wpp0106 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b-altera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b-altera C-Win
ON CHOOSE OF b-altera IN FRAME DEFAULT-FRAME /* Alterar */
DO:
    ENABLE wcond-new bt-conf-alt wrel-alt WITH FRAME {&FRAME-NAME}.
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b-lista
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b-lista C-Win
ON CHOOSE OF b-lista IN FRAME DEFAULT-FRAME /* Relat�rio */
DO:
    ASSIGN wemi-ini wemi-fim wcond-ant wrel.
    OUTPUT TO value(wrel) PAGED.
    FOR EACH emitente WHERE cod-emitente GE wemi-ini
                      AND   cod-emitente LE wemi-fim
                      AND   cod-cond-pag = wcond-ant
                      NO-LOCK:
     DISPLAY cod-emitente nome-abrev nome-emit cod-cond-pag 
             nr-tabpre telefone[1] WITH TITLE 
            "LISTAGEM CONDICAO DE PAGAMENTO - Emitente: " + string(wemi-ini) + 
            " a " + string(wemi-fim) WIDTH 120.
     END.
     MESSAGE  "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda C-Win
ON CHOOSE OF bt-ajuda IN FRAME DEFAULT-FRAME /* Ajuda */
DO:
  RUN pi_ajuda.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancelar C-Win
ON CHOOSE OF bt-cancelar IN FRAME DEFAULT-FRAME /* Fechar */
DO:
   apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-conf-alt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-conf-alt C-Win
ON CHOOSE OF bt-conf-alt IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  
  ASSIGN wemi-ini wemi-fim wrel-alt wcond-ant wcond-new.
    IF   wemi-ini  <> 0
    AND  wemi-fim  <> 0
    AND  wcond-ant <> 0 
    AND  wcond-new <> 0 THEN DO:
      ASSIGN wemi-ini wemi-fim wrel-alt wcond-ant wcond-new.
      MESSAGE "CONFIRMA ALTERA��O? Emitente " wemi-ini "a" wemi-fim
      SKIP "Condi��o de pagamento atual: " wcond-ant 
      "para condi��o: " wcond-new VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wconf.
  
      IF wconf  THEN DO:
        OUTPUT TO value(wrel-alt) PAGED.
        FOR EACH emitente WHERE cod-emitente GE wemi-ini
                          AND   cod-emitente LE wemi-fim
                          AND   cod-cond-pag = wcond-ant:
          ASSIGN cod-cond-pag = wcond-new.
          DISPLAY cod-emitente nome-abrev nome-emit cod-cond-pag 
               nr-tabpre telefone[1] WITH TITLE 
              "LISTAGEM DE ALTERA��O COND.PAGTO - Emitente: " + string(wemi-ini) + 
              " a " + string(wemi-fim) + " /Cond.Pagto de: " + string(wcond-ant) + " para: " 
              + string(wcond-new) WIDTH 120.
          END.
        OUTPUT CLOSE.
        MESSAGE  "ALTERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
        END.
      END.
    ELSE MESSAGE "Conte�do dos campos n�o pode ser ZERO" VIEW-AS ALERT-BOX.
    RUN inicia.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-5 C-Win
ON CHOOSE OF BUTTON-5 IN FRAME DEFAULT-FRAME /* Button 5 */
DO:
   apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wemi-ini wemi-fim wcond-ant wcond-new wrel wrel-alt 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE BUTTON-5 wemi-ini wemi-fim wcond-ant wcond-new wrel wrel-alt b-lista 
         b-altera bt-conf-alt bt-cancelar bt-ajuda IMAGE-8 IMAGE-9 RECT-2 
         RECT-7 RECT-8 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wemi-ini  = 0
       wemi-fim  = 0
       wcond-ant = 0
       wcond-new = 0
       wrel      = "V:\spool\CONDICAOPG.TXT"
       wrel-alt  = "V:\spool\CONDALTERACAO.TXT"
       wconf     = NO.
DISPLAY wemi-ini wemi-fim wcond-ant wcond-new wrel wrel-alt WITH FRAME {&FRAME-NAME}. 
DISABLE wcond-new bt-conf-alt wrel-alt WITH FRAME   {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi_ajuda C-Win 
PROCEDURE pi_ajuda :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  MESSAGE 
      "BOT�O RELAT�RIO: gera em arquivo o relat�rio dos pedidos, conforme par�metros selecionados."
      SKIP
      "BOT�O ALTERAR:   altera a condi��o de pagamento dos pedidos, conforme par�metros selecionados."
      VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

