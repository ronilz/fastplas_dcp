&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBulder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE VARIABLE wnr-ant   LIKE lo-matplano.nr-pl.
/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME Dialog-Frame
&Scoped-define BROWSE-NAME BROWSE-2

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES lo-matplano

/* Definitions for BROWSE BROWSE-2                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-2 lo-matplano.emp lo-matplano.estab ~
lo-matplano.cod-emitente lo-matplano.nr-pl lo-matplano.it-codigo ~
lo-matplano.seq-it lo-matplano.imediato lo-matplano.nro-docto ~
lo-matplano.data-nota lo-matplano.qt-sem1 lo-matplano.qt-sem1-2 ~
lo-matplano.qt-sem2 lo-matplano.qt-sem2-2 lo-matplano.qt-sem3 ~
lo-matplano.qt-sem3-2 lo-matplano.qt-sem4 lo-matplano.qt-sem4-2 ~
lo-matplano.qt-sem5 lo-matplano.qt-sem5-2 lo-matplano.mes1 lo-matplano.mes2 ~
lo-matplano.mes3 lo-matplano.mes4 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-2 
&Scoped-define QUERY-STRING-BROWSE-2 FOR EACH lo-matplano NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-2 OPEN QUERY BROWSE-2 FOR EACH lo-matplano NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-2 lo-matplano
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-2 lo-matplano


/* Definitions for DIALOG-BOX Dialog-Frame                              */
&Scoped-define OPEN-BROWSERS-IN-QUERY-Dialog-Frame ~
    ~{&OPEN-QUERY-BROWSE-2}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wfor-ini wep wnr-plini westab wit-codigo ~
BUTTON-9 BUTTON-10 Btn_Cancel BROWSE-2 RECT-1 RECT-2 RECT-8 RECT-9 
&Scoped-Define DISPLAYED-OBJECTS wfor-ini wep wnr-plini westab wit-codigo 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-10 
     IMAGE-UP FILE "image/im-getfl.bmp":U
     LABEL "Procura Emitente/Item" 
     SIZE 5 BY 1.13 TOOLTIP "Procura Emitente/Item".

DEFINE BUTTON BUTTON-9 
     IMAGE-UP FILE "image/im-getfl.bmp":U
     LABEL "Procura Emitente" 
     SIZE 5 BY 1.13 TOOLTIP "Procura emitente".

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(256)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wfor-ini AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "C�d.Emitente" 
     VIEW-AS FILL-IN 
     SIZE 11 BY 1 NO-UNDO.

DEFINE VARIABLE wit-codigo AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE wnr-plini AS CHARACTER FORMAT "999999x":U 
     LABEL "Planos do M�s" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 87 BY 4.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1.5
     BGCOLOR 8 .

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-2 FOR 
      lo-matplano SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-2 Dialog-Frame _STRUCTURED
  QUERY BROWSE-2 NO-LOCK DISPLAY
      lo-matplano.emp FORMAT ">>9":U WIDTH 4.43
      lo-matplano.estab FORMAT "x(3)":U WIDTH 3.43
      lo-matplano.cod-emitente FORMAT ">>>>>9":U WIDTH 6.43
      lo-matplano.nr-pl FORMAT "999999X":U WIDTH 7.43
      lo-matplano.it-codigo FORMAT "X(16)":U WIDTH 9.43
      lo-matplano.seq-it FORMAT ">>>>9":U WIDTH 5.14
      lo-matplano.imediato FORMAT ">,>>>,>>9":U WIDTH 6.43
      lo-matplano.nro-docto FORMAT ">>>>>9":U WIDTH 8.43
      lo-matplano.data-nota FORMAT "99/99/9999":U WIDTH 8.86
      lo-matplano.qt-sem1 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem1-2 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem2 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem2-2 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem3 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem3-2 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem4 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem4-2 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem5 FORMAT ">,>>>,>>9":U
      lo-matplano.qt-sem5-2 FORMAT ">,>>>,>>9":U
      lo-matplano.mes1 FORMAT ">,>>>,>>9":U
      lo-matplano.mes2 FORMAT ">,>>>,>>9":U
      lo-matplano.mes3 FORMAT ">,>>>,>>9":U
      lo-matplano.mes4 FORMAT ">,>>>,>>9":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 87 BY 10.25
         FONT 1 EXPANDABLE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     wfor-ini AT ROW 2.5 COL 59 COLON-ALIGNED
     wep AT ROW 3.25 COL 21 COLON-ALIGNED
     wnr-plini AT ROW 3.5 COL 59 COLON-ALIGNED
     westab AT ROW 4.25 COL 21 COLON-ALIGNED
     wit-codigo AT ROW 4.5 COL 59 COLON-ALIGNED
     BUTTON-9 AT ROW 6.75 COL 19
     BUTTON-10 AT ROW 6.75 COL 51
     Btn_Cancel AT ROW 6.75 COL 81
     BROWSE-2 AT ROW 8.5 COL 3
     RECT-1 AT ROW 2.25 COL 3
     RECT-2 AT ROW 6.5 COL 3
     RECT-8 AT ROW 1.25 COL 3
     RECT-9 AT ROW 18.75 COL 3
     "Procura Emitente" VIEW-AS TEXT
          SIZE 12 BY .67 AT ROW 7 COL 25
          FONT 1
     "Procura Item" VIEW-AS TEXT
          SIZE 10 BY .67 AT ROW 7 COL 57
          FONT 1
     SPACE(24.42) SKIP(12.70)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Consulta Plano por EMITENTE ou por ITEM"
         CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
                                                                        */
/* BROWSE-TAB BROWSE-2 Btn_Cancel Dialog-Frame */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-2
/* Query rebuild information for BROWSE BROWSE-2
     _TblList          = "mgfas.lo-matplano"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _FldNameList[1]   > mgfas.lo-matplano.emp
"lo-matplano.emp" ? ? "integer" ? ? ? ? ? ? no ? no no "4.43" yes no no "U" "" ""
     _FldNameList[2]   > mgfas.lo-matplano.estab
"lo-matplano.estab" ? ? "character" ? ? ? ? ? ? no ? no no "3.43" yes no no "U" "" ""
     _FldNameList[3]   > mgfas.lo-matplano.cod-emitente
"lo-matplano.cod-emitente" ? ? "integer" ? ? ? ? ? ? no ? no no "6.43" yes no no "U" "" ""
     _FldNameList[4]   > mgfas.lo-matplano.nr-pl
"lo-matplano.nr-pl" ? ? "character" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" ""
     _FldNameList[5]   > mgfas.lo-matplano.it-codigo
"lo-matplano.it-codigo" ? ? "character" ? ? ? ? ? ? no ? no no "9.43" yes no no "U" "" ""
     _FldNameList[6]   > mgfas.lo-matplano.seq-it
"lo-matplano.seq-it" ? ? "integer" ? ? ? ? ? ? no ? no no "5.14" yes no no "U" "" ""
     _FldNameList[7]   > mgfas.lo-matplano.imediato
"lo-matplano.imediato" ? ? "integer" ? ? ? ? ? ? no ? no no "6.43" yes no no "U" "" ""
     _FldNameList[8]   > mgfas.lo-matplano.nro-docto
"lo-matplano.nro-docto" ? ? "integer" ? ? ? ? ? ? no ? no no "8.43" yes no no "U" "" ""
     _FldNameList[9]   > mgfas.lo-matplano.data-nota
"lo-matplano.data-nota" ? ? "date" ? ? ? ? ? ? no ? no no "8.86" yes no no "U" "" ""
     _FldNameList[10]   = mgfas.lo-matplano.qt-sem1
     _FldNameList[11]   = mgfas.lo-matplano.qt-sem1-2
     _FldNameList[12]   = mgfas.lo-matplano.qt-sem2
     _FldNameList[13]   = mgfas.lo-matplano.qt-sem2-2
     _FldNameList[14]   = mgfas.lo-matplano.qt-sem3
     _FldNameList[15]   = mgfas.lo-matplano.qt-sem3-2
     _FldNameList[16]   = mgfas.lo-matplano.qt-sem4
     _FldNameList[17]   = mgfas.lo-matplano.qt-sem4-2
     _FldNameList[18]   = mgfas.lo-matplano.qt-sem5
     _FldNameList[19]   = mgfas.lo-matplano.qt-sem5-2
     _FldNameList[20]   = mgfas.lo-matplano.mes1
     _FldNameList[21]   = mgfas.lo-matplano.mes2
     _FldNameList[22]   = mgfas.lo-matplano.mes3
     _FldNameList[23]   = mgfas.lo-matplano.mes4
     _Query            is OPENED
*/  /* BROWSE BROWSE-2 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Consulta Plano por EMITENTE ou por ITEM */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-10 Dialog-Frame
ON CHOOSE OF BUTTON-10 IN FRAME Dialog-Frame /* Procura Emitente/Item */
DO:
  ASSIGN wfor-ini wep westab wit-codigo wnr-plini.
  IF wfor-ini <> 0 AND wep <> 0 AND westab <> "" AND wit-codigo <> "" 
                   AND wnr-plini <> ""  THEN DO:
    OPEN QUERY browse-2 FOR EACH lo-matplano WHERE lo-matplano.cod-emitente = wfor-ini
                        AND   lo-matplano.emp             = wep
                        AND   lo-matplano.estab           = westab 
                        AND   lo-matplano.it-codigo       = wit-codigo
                        AND   lo-matplano.nr-pl           BEGINS wnr-plini
                        NO-LOCK USE-INDEX ch-primaria.
    GET FIRST browse-2.
    END.
   ELSE DO:
     MESSAGE "Empresa / Estabelecimento / C�digo emitente /Item /Plano"
     SKIP 
     "devem ser diferentes de 0 ou branco"
     VIEW-AS ALERT-BOX.
   END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-9 Dialog-Frame
ON CHOOSE OF BUTTON-9 IN FRAME Dialog-Frame /* Procura Emitente */
DO:
    ASSIGN wfor-ini wep westab wnr-plini.
    IF wfor-ini <> 0 AND wep <> 0 AND westab <> "" AND wnr-plini <> "" 
                      THEN DO:
    ASSIGN wit-codigo = "". DISPLAY wit-codigo WITH FRAME   {&FRAME-NAME}.
    OPEN QUERY browse-2 FOR EACH lo-matplano WHERE lo-matplano.cod-emitente = wfor-ini
                        AND   lo-matplano.emp             = wep
                        AND   lo-matplano.estab           = westab 
                        AND   lo-matplano.nr-pl           BEGINS wnr-plini
                        NO-LOCK 
                        USE-INDEX ch-primaria.
    GET FIRST browse-2.
    END.
    ELSE DO:
    MESSAGE "Empresa / Estabelecimento / C�digo emitente / N�mero Plano"
    SKIP "devem ser diferentes de 0 ou branco"
    VIEW-AS ALERT-BOX.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-2
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wfor-ini wep wnr-plini westab wit-codigo 
      WITH FRAME Dialog-Frame.
  ENABLE wfor-ini wep wnr-plini westab wit-codigo BUTTON-9 BUTTON-10 Btn_Cancel 
         BROWSE-2 RECT-1 RECT-2 RECT-8 RECT-9 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia Dialog-Frame 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wfor-ini     = 0
       wep          = 1
       westab       = "1"
       wit-codigo   = "".
       wnr-plini    = string(MONTH(TODAY),"99") + STRING(YEAR(TODAY)).
           
DISPLAY wfor-ini wep westab wit-codigo wnr-plini  WITH FRAME   {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

