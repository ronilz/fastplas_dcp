REGISTRO TIPO 1:
 
** INFORMA��O **        ** POSI��O **     ** OBSERVA��O **   
           
 TIPO REGISTRO              1  A   1       VALOR FIXO = 1                
 CNPJ FORD                  2  A  15       FORMATO =  99999999999999  -> EX:  (03470727000120)                   
 CNPJ FORNECEDOR           16  A  29       FORMATO =  99999999999999  -> EX:  (04095571000108)                      
 NR. NOTA FISCAL COMPL     30  A  35       FORMATO =          999999  -> EX:          (999999)                      
 DATA DE EMISS�O           36  A  45       FORMATO =      DD.MM.AAAA  -> EX:      (01.01.2005)         
 BASE DE ICMS              46  A  60       FORMATO = 999999999999999  -> EX: (000000000121200)
 VALOR DE ICMS             61  A  75       FORMATO = 999999999999999  -> EX: (000000000021816)   
 VALOR L�QUIDO             76  A  90       FORMATO = 999999999999999  -> EX: (000000000121200) 
 VALOR BRUTO               91  A 105       FORMATO = 999999999999999  -> EX: (000000000121200)   
 VALOR IPI                106  A 120       FORMATO = 999999999999999  -> EX: (000000000012120) 
 PERCENTUAL DE IPI        121  A 125       FORMATO =           99999  -> EX:           (01000)   
 
 


EXEMPLO DO REGISTRO TIPO 1 MONTADO:   
 
1034707270001200409557100010899999901.01.200500000000012120000000000002181600000000012120000000000012120000000000012120001000




REGISTRO TIPO 2:

** INFORMA��O **        ** POSI��O **     ** OBSERVA��O **   
           
 TIPO REGISTRO              1  A   1       VALOR FIXO = 2              
 CNPJ FORD                  2  A  15       FORMATO    =  99999999999999  -> EX: (03470727000120)                   
 CNPJ FORNECEDOR           16  A  29       FORMATO    =  99999999999999  -> EX: (04095571000108)                      
 NR. NOTA FISCAL COMPL     30  A  35       FORMATO    =          999999  -> EX:         (999999)
 PREFIXO DA PE�A           36  A  42       FORMATO    =         XXXXXXX  -> EX:        (   2S65)                      
 BASICO  DA PE�A           43  A  51       FORMATO    =       XXXXXXXXX  -> EX:      (   A26087)
 SUFIXO  DA PE�A           52  A  57       FORMATO    =          XXXXXX  -> EX:         (AC    )
 NOTA FISCAL ORIGINAL      58  A  63       FORMATO    =          999999  -> EX:         (019876)
 DIFEREN�A DE PRE�O        64  A  78       FORMATO    = 999999999999999  -> EX:(000000000000020)
 ESPA�O LIVRE              79  A 125       FORMATO    = ESPA�OS 

 

EXEMPLO DO REGISTRO TIPO 2 MONTADO: 

"20347072700012003509521000167999999...2S65...A26087AC....019876000000000000020................................................................................."
     



obs: 1.) Os valores n�mericos n�o devem conter virgulas

     2.) Caracter ponto(.) no registro tipo 2 equivale a espa�os, deixar em branco.

	  3.) A pe�a(posi��o 36) � composta de 3 partes:
            1 � BASICO  com 7 posi��es alinhadas a direita. 
    2 � PREFIXO com 9 posi��es alinhadas a direita.
    3 � SUFIXO  com 6 posi��es alinhadas a esquerda

     4.) A NF Complementar � composta no arquivo por 1 registro tipo 1 e 
 tantos registros tipo 2 quanto forem os itens da nf complementar ou seja, cada registro tipo 1, � uma nova nota complementar e os registros tipo 2 subsequentes s�o os itens pertencentes a ela.

     5.) O CNPJ FORD, CNPJ FORN e NR. NOTA FISCAL COMPL, devem ser iguais no registro
 tipo 1 e registro tipo 2.

