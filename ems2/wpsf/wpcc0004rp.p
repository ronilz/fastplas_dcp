&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
/*{wpsf/include/i-prgvrs.i wpcc0001rp 2.04.00.007}*/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i wpcc0004rp 2.04.00.007}
/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.


/****************** Defini�ao de Vari�veis Campo Calculado do Relat�rio **********************/ 

def var wpiscofins          as decimal   FORMAT ">>>>>>,>>9.99-" NO-UNDO COLUMN-LABEL "PIS/COFINS".
DEF VAR wmes                AS INTEGER   FORMAT "99" NO-UNDO.
DEF VAR wano                AS INTEGER   FORMAT "9999" NO-UNDO.
DEF VAR wperpis             AS DEC       FORMAT ">9.99-" NO-UNDO.
DEF VAR wpercofins          AS DEC       FORMAT ">9.99-" NO-UNDO.
DEF VAR wpis                AS DECI      FORMAT ">>>>>>,>>9.99-" NO-UNDO.
DEF VAR wcofins             AS DECI      FORMAT ">>>>>>,>>9.99-" NO-UNDO.
DEF VAR wtotitem            AS DECI      FORMAT ">>>>,>>>,>>>,>>9.99" NO-UNDO.
DEF VAR wvltot              AS DECI      FORMAT ">>>>,>>>9.99" NO-UNDO.
DEF VAR wvl-icms-it         AS DECI      FORMAT ">>>>>,>>9.99" NO-UNDO.
DEF VAR wvl-ipi-it          AS DECI      FORMAT ">>>>>,>>9.99" NO-UNDO.
DEFINE VARIABLE wliqdet     AS DECI      FORMAT ">,>>>,>>>,>>9.99-" NO-UNDO COLUMN-LABEL "Vlrf".
DEFINE VARIABLE wcomibruta  AS DECI      FORMAT ">>>,>>>,>>9.99" NO-UNDO.
DEFINE VARIABLE wirrfbruto  AS DECI      FORMAT ">>>,>>>,>>9.99" NO-UNDO.
DEFINE VARIABLE wcomiliqui  AS DECI      FORMAT ">>>,>>>,>>9.99" NO-UNDO.
DEFINE VARIABLE wpercentual AS DECI      FORMAT ">9.99" NO-UNDO.
DEFINE VARIABLE wdn         AS CHAR      FORMAT "x(04)"  NO-UNDO.
DEFINE VARIABLE wforn       AS CHAR      FORMAT "x(04)" NO-UNDO.

/* variaveis de totais */
DEF VAR wtot-qt-faturada LIKE  it-nota-fisc.qt-faturada[1] NO-UNDO.
DEFINE VARIABLE wtot-vl-tot-item LIKE it-nota-fisc.vl-tot-item NO-UNDO.


define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    /* defini��es da sele��o */
    field c-cod-estabel-ini like nota-fiscal.cod-estabel
    field c-cod-estabel-fim like nota-fiscal.cod-estabel
    field i-cod-rep-ini like nota-fiscal.cod-rep
    field i-cod-rep-fim like nota-fiscal.cod-rep
    field da-dt-emis-nota-ini like nota-fiscal.dt-emis-nota
    field da-dt-emis-nota-fim like nota-fiscal.dt-emis-nota
    field c-nr-nota-fis-ini like nota-fiscal.nr-nota-fis
    field c-nr-nota-fis-fim like nota-fiscal.nr-nota-fis
    field c-serie-ini like nota-fiscal.serie
    field c-serie-fim like nota-fiscal.serie
    field c-estado-ini like nota-fiscal.estado
    field c-estado-fim like nota-fiscal.estado
    field i-cod-emitente-ini like nota-fiscal.cod-emitente
    field i-cod-emitente-fim like nota-fiscal.cod-emitente
    field c-fm-codigo-ini like item.fm-codigo
    field c-fm-codigo-fim like item.fm-codigo
    /* campos de parametros */
    FIELD p-cod-rep-1 LIKE nota-fiscal.cod-emitente
    FIELD p-fm-codigo-1 LIKE ITEM.fm-codigo
    FIELD p-percentual-1 AS DECI FORMAT "99.99"
    FIELD p-cod-rep-2 LIKE nota-fiscal.cod-emitente
    FIELD p-fm-codigo-2 LIKE ITEM.fm-codigo
    FIELD p-percentual-2 AS DECI FORMAT "99.99"
    FIELD p-cod-rep-3 LIKE nota-fiscal.cod-emitente
    FIELD p-fm-codigo-3 LIKE ITEM.fm-codigo
    FIELD p-percentual-3 AS DECI FORMAT "99.99"
    FIELD p-cod-rep-4 LIKE nota-fiscal.cod-emitente
    FIELD p-fm-codigo-4 LIKE ITEM.fm-codigo
    FIELD p-percentual-4 AS DECI FORMAT "99.99"
    FIELD c-irrfper      AS DECI FORMAT "99.99"
        /* field exce��o item - conf. Leia e Rosario */
    FIELD witem-1        AS CHAR FORMAT "X(16)"
    FIELD witem-2        AS CHAR FORMAT "X(16)"
    FIELD wvalor-1       AS DEC  FORMAT ">>>,>>>,>>9.99"
    FIELD wvalor-2       AS DEC  FORMAT ">>>,>>>,>>9.99".
    
    
    .
DEFINE TEMP-TABLE tt-relatorio-excel
    FIELD wcod-forn                 AS CHAR FORMAT "(04)"
    FIELD ww-nome                   LIKE estabelec.nome
    FIELD ww-mes                    LIKE wmes
    FIELD ww-ano                    LIKE wano
    FIELD ww-dn                     LIKE wdn
    FIELD ww-nome-ab-cli            LIKE nota-fiscal.nome-ab-cli 
    FIELD ww-codigo-refer           LIKE item.codigo-refer
    FIELD ww-descricao-1            LIKE item.descricao-1 
    FIELD ww-nr-pedfor              AS CHAR FORMAT "X(09)"
    FIELD ww-nr-nota-fis            LIKE it-nota-fisc.nr-nota-fis
    FIELD ww-dt-emis-nota           LIKE it-nota-fisc.dt-emis-nota
    FIELD ww-qt-faturada            LIKE it-nota-fisc.qt-faturada[1]
    FIELD ww-vl-preuni              LIKE it-nota-fisc.vl-preuni
    FIELD ww-vl-tot-item            LIKE it-nota-fisc.vl-tot-item
    /*FIELD ww-totitem                LIKE wtotitem  */
    FIELD ww-vl-ipi-it              LIKE  it-nota-fisc.vl-ipi-it
    FIELD ww-vl-icms-it             LIKE it-nota-fisc.vl-icms-it
    FIELD ww-vl-icmsub-it           LIKE it-nota-fisc.vl-icmsub-it
    FIELD ww-piscofins              LIKE wpiscofins 
    FIELD ww-vl-despes-it           LIKE it-nota-fisc.vl-despes-it
    FIELD ww-liqdet                 LIKE wliqdet
    FIELD ww-percentual             LIKE wpercentual
    FIELD ww-comibruta              LIKE wcomibruta
    FIELD ww-comiliqui              LIKE wcomiliqui
    FIELD ww-estado                 LIKE nota-fiscal.estado
    FIELD ww-irrfper                 AS DECI FORMAT "99.99"
    /* totais */
    FIELD tot-qt-faturada           LIKE it-nota-fisc.qt-faturada[1]
    FIELD tot-vl-preuni             LIKE it-nota-fisc.vl-preuni
    FIELD tot-vl-tot-item           LIKE it-nota-fisc.vl-tot-item     
  /*  FIELD tot-totitem               LIKE wtotitem            */             
    FIELD tot-vl-ipi-it             LIKE it-nota-fisc.vl-ipi-it         
    FIELD tot-vl-icms-it            LIKE  it-nota-fisc.vl-icms-it   
    FIELD tot-vl-icmssub-it         LIKE it-nota-fisc.vl-icmsub-it   
    FIELD tot-wpiscofins            LIKE wpiscofins
    FIELD tot-vl-despes-it          LIKE it-nota-fisc.vl-despes-it
    FIELD tot-wliqdet               LIKE wliqdet
    FIELD tot-wcomibruta            LIKE wcomibruta           
    FIELD tot-wcomiliqui            LIKE wcomiliqui.


 /* variaveis planilha excel */
DEFINE VARIABLE c-arq-orig                  AS CHARACTER                 NO-UNDO.
    DEFINE VARIABLE c-arq-dest              AS CHARACTER                 NO-UNDO.
    DEFINE VARIABLE chExcelApplication      AS office.iface.excel.ExcelWrapper.
    DEFINE VARIABLE chWorkbook              AS office.iface.excel.Workbook.
    DEFINE VARIABLE chWorksheet             AS office.iface.excel.WorkSheet.
    define var      ActiveChart             as com-handle.
    DEFINE VARIABLE iLinha                  AS INTEGER INITIAL 1.
    def var c-anexo-excel as char.
/* outros */ 
DEFINE VARIABLE vnr-pedfor                  AS CHAR FORMAT "X(09)".


define temp-table tt-digita no-undo
    field ordem                     as integer   format ">>>>9"
    field exemplo                   as character format "x(30)"
    index id ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 350 frame f-impressao.

form
     wforn COLUMN-LABEL "Forn"
     /*estabelec.nome COLUMN-LABEL "Nome" FORMAT "x(20)"*/
     nota-fiscal.cod-rep
     ITEM.fm-codigo
     wmes column-label "Mes" format "99" 
     wano column-label "Ano" format "9999" 
     wdn  COLUMN-LABEL "DN" 
     nota-fiscal.nome-ab-cli column-label "DN" format "x(12)" 
     it-doc-fis.it-codigo
     item.codigo-refer column-label "Peca" FORMAT "X(16)"
     /*item.descricao-1 column-label "DescPeca" format "x(12)"*/
     vnr-pedfor column-label "PedFord" format "X(09)"
     it-doc-fis.nr-doc-fis column-label "NF" format "x(10)" 
     it-doc-fis.dt-emis-doc column-label "DataNF" format "99/99/99" 
     it-doc-fis.quantidade column-label "Qtde" 
     it-nota-fisc.vl-preuni    column-label "VlrUnit" 
     it-doc-fis.vl-tot-item   COLUMN-LABEL "VlrTotal" FORMAT ">>>>,>>>,>>>,>>9.99" 
    /*
     wtotitem                 COLUMN-LABEL "VlrTotal" FORMAT ">>>>,>>>,>>>,>>9.99" 
     */
     it-doc-fis.vl-ipi-it column-label "IPI" 
     it-doc-fis.vl-icms-it column-label "ICMS" 
     it-doc-fis.vl-icmsub-it column-label "ICMSSubst" 
     wpiscofins column-label "PIS/COFINS" 
     it-doc-fis.vl-despes-it COLUMN-LABEL "DespFinanc"
     wliqdet COLUMN-LABEL "VlrLiq" 
     wpercentual COLUMN-LABEL "PerCom" 
     wcomibruta  COLUMN-LABEL "VlrCombruto" 
     wcomiliqui  COLUMN-LABEL "VLrComLiq" 
     nota-fiscal.estado COLUMN-LABEL "UF" FORMAT "x(02)"
     with no-box width 350 down stream-io frame f-relat.

create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

/* find empresa                                      */
/*     where empresa.ep-codigo = v_cdn_empres_usuar  */
/*     no-lock no-error.                             */
find first param-global no-lock no-error.

{utp/ut-liter.i especifico * }
assign c-sistema = "EMS 2.04".
{utp/ut-liter.i repres * }
assign c-titulo-relat = "Relat�rio de Comiss�es - Ford".
assign c-empresa     = param-global.grupo
    /*   c-programa    = "{&programa}":U*/
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 2
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp (input "processando":U). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
    IF tt-param.classifica = 01  THEN DO:
    /*--------------------------fastplas --------------------------------*/

        FOR EACH tt-relatorio-excel:
           DELETE tt-relatorio-excel.
           END.
        for each nota-fiscal 
            where nota-fiscal.cod-emitente >= i-cod-emitente-ini and 
                       nota-fiscal.cod-emitente <= i-cod-emitente-fim and
                       nota-fiscal.cod-estabel >= c-cod-estabel-ini and 
                       nota-fiscal.cod-estabel <= c-cod-estabel-fim and
                       nota-fiscal.cod-rep >= i-cod-rep-ini and 
                       nota-fiscal.cod-rep <= i-cod-rep-fim and
                       nota-fiscal.dt-emis-nota >= da-dt-emis-nota-ini and 
                       nota-fiscal.dt-emis-nota <= da-dt-emis-nota-fim and
                       nota-fiscal.estado >= c-estado-ini and 
                       nota-fiscal.estado <= c-estado-fim and
                       nota-fiscal.nr-nota-fis >= c-nr-nota-fis-ini and 
                       nota-fiscal.nr-nota-fis <= c-nr-nota-fis-fim and
                       nota-fiscal.serie >= c-serie-ini and 
                       nota-fiscal.serie <= c-serie-fim AND
                       nota-fiscal.emite-duplic  = YES AND
                       nota-fiscal.dt-cancela  = ? NO-LOCK
                       USE-INDEX ch-distancia BREAK BY nota-fiscal.dt-emis-nota 
                       BY nota-fiscal.cod-estabel  by nota-fiscal.nome-ab-cli:
           FOR each it-nota-fisc
                 where it-nota-fisc.cod-estabel = nota-fiscal.cod-estabel and
                       it-nota-fisc.serie = nota-fiscal.serie and
                       it-nota-fisc.nr-nota-fis = nota-fiscal.nr-nota-fis AND 
                       it-nota-fisc.baixa-estoq  = YES NO-LOCK:

            /*24/10/12 - passa a pegar dados do documento fiscal*/
            FIND FIRST doc-fiscal WHERE doc-fiscal.cod-emitente = nota-fiscal.cod-emitente
                                AND doc-fiscal.dt-emis-doc    = nota-fiscal.dt-emis-nota
                                AND doc-fiscal.nr-doc-fis     = nota-fiscal.nr-nota-fis
                                NO-LOCK NO-ERROR.
              FIND FIRST it-doc-fisc OF doc-fiscal WHERE it-doc-fis.it-codigo   = it-nota-fisc.it-codigo
                                            AND    it-doc-fis.nr-seq-doc = it-nota-fisc.nr-seq-fat    NO-LOCK.
                 
                 ASSIGN  wpiscofins          = it-doc-fis.val-pis + it-doc-fis.val-cofins
                         wliqdet             = it-doc-fis.vl-merc-liq - it-doc-fisc.vl-ipi-it   - it-doc-fisc.vl-icms-it -  it-doc-fis.vl-icmsub-it  - wpiscofins.
           
               /*
                 MESSAGE 
                     doc-fiscal.cod-estabel doc-fiscal.cod-emitente it-doc-fis.nr-doc-fis it-codigo vl-merc-liq vl-tot-item vl-icms-it  vl-ipi-it vl-icmsub-it val-pis val-cofins VIEW-AS ALERT-BOX. */
            
 
               /* limpa percentual comiss�o e irpf  */
               ASSIGN wcomibruta = 0
                      wirrfbruto = 0
                      wcomiliqui = 0.
              FIND FIRST ITEM WHERE item.it-codigo = it-nota-fisc.it-codigo 
                       NO-LOCK NO-ERROR.
                    
              FIND FIRST emitente WHERE emitente.cod-emitente = nota-fiscal.cod-emitente
              NO-LOCK NO-ERROR.
              FIND FIRST ped-item WHERE ped-item.nr-pedcli   = it-nota-fisc.nr-pedcli
                                  AND   ped-item.it-codigo   = it-nota-fisc.it-codigo
                                  AND   ped-item.nome-abrev  = emitente.nome-abrev
                                  NO-LOCK NO-ERROR.
             
              ASSIGN vnr-pedfor  = STRING(SUBSTRING(ped-item.observacao,1,9)).
              CREATE tt-relatorio-excel.
              RUN pi-acompanhar in h-acomp (input "DN: " + nota-fiscal.nome-ab-cli).

              find first estabelec no-lock
                where estabelec.cod-estabel = c-cod-estabel-ini no-error.
                if  avail estabelec THEN assign c-empresa = estabelec.nome.
              else assign c-empresa = "".
              /* mes e ano */
              ASSIGN wmes = MONTH(da-dt-emis-nota-ini)
                   wano = YEAR(da-dt-emis-nota-ini).

            
             
              /* CALCULA COMISSAO */
              /* 1a. sele��o de familia e percentual */
              IF nota-fiscal.cod-rep      =  p-cod-rep-1 
              AND ITEM.fm-codigo          =  p-fm-codigo-1 
              THEN DO:
                  ASSIGN wcomibruta = (wliqdet * p-percentual-1) / 100.
              END.
           
              /* 2a. sele��o de familia e percentual */
              IF nota-fiscal.cod-rep =  p-cod-rep-2 
              AND ITEM.fm-codigo     =  p-fm-codigo-2 
              THEN DO:
                  ASSIGN wcomibruta = (wliqdet * p-percentual-2) / 100.
              END.
    
              /* 3a. sele��o de familia e percentual */
              IF nota-fiscal.cod-rep =  p-cod-rep-3 
              AND ITEM.fm-codigo     =  p-fm-codigo-3 
              THEN DO:
                  ASSIGN wcomibruta = (wliqdet * p-percentual-3) / 100.
              END.
             /* 4a. sele��o de familia e percentual */
              IF nota-fiscal.cod-rep      =  p-cod-rep-4 
              AND ITEM.fm-codigo          =  p-fm-codigo-4 
              THEN DO:
                  ASSIGN wcomibruta = (wliqdet * p-percentual-4) / 100.
              END.  

          

          /* ir e comissao liquida */
          ASSIGN wirrfbruto = (wcomibruta * c-irrfper) / 100
                wcomiliqui = (wcomibruta - wirrfbruto).

         /* COMISS�O ESPECIAL POR ITEM - SOL. LEILA E ROSARIO - 18/04/12 */
          /* 1002.0430.000 e 1002.0433.000*/
          IF it-nota-fisc.it-codigo = tt-param.witem-1 THEN DO:
             ASSIGN wcomibruta = it-nota-fisc.qt-faturada[1] * wvalor-1
                    wcomiliqui = wcomibruta.
             END.
         /* 1002.0433.000*/
         IF it-nota-fisc.it-codigo = tt-param.witem-2 THEN DO:
               ASSIGN wcomibruta = it-nota-fisc.qt-faturada[1] * wvalor-2
                      wcomiliqui = wcomibruta.
               END.

        /* Acumula totais */
        ASSIGN wtot-qt-faturada = wtot-qt-faturada  + it-nota-fisc.qt-faturada[1]
               wtot-vl-tot-item = wtot-vl-tot-item  + it-nota-fisc.vl-tot-item.
        /* assign dn e percentual */
        ASSIGN wdn = SUBSTRING(string(nota-fiscal.cod-emitente),3,4).
        IF  ITEM.fm-codigo =  p-fm-codigo-1 THEN ASSIGN wpercentual = p-percentual-1.
        IF  ITEM.fm-codigo =  p-fm-codigo-2 THEN ASSIGN wpercentual = p-percentual-2.
        IF  ITEM.fm-codigo =  p-fm-codigo-3 THEN ASSIGN wpercentual = p-percentual-3.
        IF  ITEM.fm-codigo =  p-fm-codigo-4 THEN ASSIGN wpercentual = p-percentual-4. 

    

          ACCUMULATE it-doc-fis.quantidade          (TOTAL).   
          ACCUMULATE it-nota-fisc.vl-preuni         (TOTAL).
          ACCUMULATE it-doc-fis.vl-tot-item         (TOTAL).
          /*
          ACCUMULATE wtotitem                       (TOTAL).    
          */
          ACCUMULATE it-doc-fis.vl-ipi-it         (TOTAL).    
          ACCUMULATE it-doc-fis.vl-icms-it          (TOTAL).    
          ACCUMULATE it-nota-fisc.vl-icmsub-it      (TOTAL).    
          ACCUMULATE wpiscofins                     (TOTAL).    
          ACCUMULATE wliqdet                        (TOTAL).    
          ACCUMULATE wcomibruta                     (TOTAL). 
          ACCUMULATE it-doc-fis.vl-despes-it        (TOTAL).
          ACCUMULATE wcomibruta                     (TOTAL).
          ACCUMULATE wcomiliqui                     (TOTAL).    


    /*
        ASSIGN
                  w-vl-ipi-it          =  it-doc-fis.vl-ipi-it
                  w-vl-icms-it         =  it-doc-fis.vl-icms-it 
                  w-vl-icmsub-it       = it-doc-fis.vl-icmsub-it 
                  w-piscofins          = it-doc-fis.val-pis + it-doc-fis.val-cofins
                  w-vl-despes-it       = it-doc-fis.vl-despes-it
                  w-liqdet             = it-doc-fis.vl-merc-liq. 
*/

           
            
          /***  C�DIGO PARA SA�DA EM 132 COLUNAS ***/
             display 
                "V277" @ wforn
               /* estabelec.nome */
                nota-fiscal.cod-rep
                ITEM.fm-codigo
                wmes 
                wano 
                SUBSTRING(string(nota-fiscal.cod-emitente),3,4) @ wdn
                nota-fiscal.nome-ab-cli 
                it-doc-fis.it-codigo
                item.codigo-refer 
                /*item.descricao-1 */
                vnr-pedfor
                it-doc-fis.nr-doc-fis
                it-doc-fis.dt-emis-doc
                it-doc-fis.quantidade FORMAT ">>>>,>>9.9999"
                it-nota-fisc.vl-preuni    FORMAT ">,>>>,>>>,>>9.99"
                it-doc-fis.vl-tot-item    FORMAT ">>>>,>>>,>>>,>>9.99" 
                /*
                wtotitem                    FORMAT ">>>>,>>>,>>>,>>9.99" 
                */
                it-doc-fisc.vl-ipi-it     
                it-doc-fisc.vl-icms-it     
                it-doc-fis.vl-icmsub-it   FORMAT ">>>>>>>9.99"
                wpiscofins /*wpis wcofins*/ 
                it-doc-fis.vl-despes-it   FORMAT ">>>>>>>9.99"
                wliqdet LABEL "LIQUIDO"
                with stream-io frame f-relat WIDTH 350.
                IF  ITEM.fm-codigo =  p-fm-codigo-1 THEN DISPLAY p-percentual-1  @ wpercentual with stream-io frame f-relat WIDTH 350.
                IF  ITEM.fm-codigo =  p-fm-codigo-2 THEN DISPLAY p-percentual-2  @ wpercentual with stream-io frame f-relat WIDTH 350.
                IF  ITEM.fm-codigo =  p-fm-codigo-3 THEN DISPLAY p-percentual-3  @ wpercentual with stream-io frame f-relat WIDTH 350.
                IF  ITEM.fm-codigo =  p-fm-codigo-4 THEN DISPLAY p-percentual-4  @ wpercentual with stream-io frame f-relat WIDTH 350.
                DISPLAY wcomibruta         
                        wcomiliqui          
                        nota-fiscal.estado  
                        with stream-io frame f-relat WIDTH 350.
                        down  with frame f-relat WIDTH 350.


           /* cria arquivo para gera��o em excel */
           
             ASSIGN wcod-forn = "V277"
                  ww-nome               = estabelec.nome
                  ww-mes                = wmes
                  ww-ano                = wano
                  ww-dn                 = wdn
                  ww-nome-ab-cli        = nota-fiscal.nome-ab-cli 
                  ww-codigo-refer       = item.codigo-refer
                  ww-descricao-1        = item.descricao-1 
                  ww-nr-pedfor          = vnr-pedfor
                  ww-nr-nota-fis        = it-doc-fis.nr-doc-fis
                  ww-dt-emis-nota       = it-doc-fis.dt-emis-doc
                  ww-qt-faturada        = it-doc-fis.quantidade
                  ww-vl-preuni          = it-nota-fisc.vl-preuni
                  ww-vl-tot-item        = it-doc-fis.vl-tot-item 
                  /*
                  ww-totitem            = wtotitem  
                  */
                  ww-vl-ipi-it          = it-doc-fis.vl-ipi-it
                  ww-vl-icms-it         = it-doc-fis.vl-icms-it
                  ww-vl-icmsub-it       = it-doc-fis.vl-icmsub-it
                  ww-piscofins          = wpiscofins 
                  ww-vl-despes-it       = it-doc-fis.vl-despes-it
                  ww-liqdet             = it-doc-fisc.vl-merc-liq
                  ww-percentual         = wpercentual
                  ww-comibruta          = wcomibruta
                  ww-comiliqui          = wcomiliqui
                  ww-estado             = nota-fiscal.estado
                  ww-irrfper            = c-irrfper.
            END.
        END.
    /*
        DISPLAY SKIP(1)
                "TOTAIS" 
                (ACCUM TOTAL it-doc-fisc.quantidade)        TO 108
                (ACCUM TOTAL it-nota-fisc.vl-preuni)             TO 125
                (ACCUM TOTAL it-doc-fis.vl-tot-item)           TO 145 FORMAT ">>>>,>>>,>>>,>>9.99" 
                /*
                (ACCUM TOTAL wtotitem)                           TO 132 FORMAT ">>>>,>>>,>>>,>>9.99" 
                */
                (ACCUM TOTAL it-doc-fis.vl-ipi-it)             TO 162
                (ACCUM TOTAL it-doc-fis.vl-icms-it)            TO 179
                (ACCUM TOTAL it-doc-fis.vl-icmsub-it)          TO 191
                (ACCUM TOTAL wpiscofins)                         TO 205
                (ACCUM TOTAL it-doc-fis.vl-despes-it)          TO 218
                (ACCUM TOTAL wliqdet)                            TO 235
                (ACCUM TOTAL wcomibruta)                         TO 258                       
                (ACCUM TOTAL wcomiliqui)                         TO 273
            with no-box width 350 down stream-io frame f-tot NO-LABEL.



        */

        DISPLAY SKIP(2)  "******************** Par�metros utilizados: ********************"
                SKIP(1)  "SELE��O"
                SKIP(1)  "Estabelecimento: " c-cod-estabel-ini   TO 35  "-" c-cod-estabel-fim
                SKIP     "Representante:   " i-cod-rep-ini       TO 35 "-" i-cod-rep-fim
                SKIP     "Emiss�o:         " da-dt-emis-nota-ini TO 35 "-" da-dt-emis-nota-fim
                SKIP     "Nr.Nota Fiscal:  " c-nr-nota-fis-ini   TO 35 "-" c-nr-nota-fis-fim
                SKIP     "S�rie:           " c-serie-ini         TO 35 "-" c-serie-fim
                SKIP     "Estado:          " c-estado-ini        TO 35 "-" c-estado-fim
                SKIP     "Cliente/Forned.: " i-cod-emitente-ini  TO 35 "-" i-cod-emitente-fim
                /*SKIP     "Fam�lia:         " c-fm-codigo-ini     TO 35 "-" c-fm-codigo-fim*/
                SKIP(2) 
                SKIP     "PAR�METROS"
                SKIP(1) "[1] Cliente: " p-cod-rep-1 " / Fam�lia:  " p-fm-codigo-1 " / Percentual:  " p-percentual-1
                SKIP    "[2] Cliente: " p-cod-rep-2 " / Fam�lia:  " p-fm-codigo-2 " / Percentual:  " p-percentual-2
                SKIP    "[3] Cliente: " p-cod-rep-3 " / Fam�lia:  " p-fm-codigo-3 " / Percentual:  " p-percentual-3
                SKIP    "[4] Cliente: " p-cod-rep-4 " / Fam�lia:  " p-fm-codigo-4 " / Percentual:  " p-percentual-4
                SKIP(1) "Percentual IRPF: " c-irrfper
                WITH FRAME f-selec WITH WIDTH 200 NO-LABELS.


        /* assinala totais para o excel */                    
        FIND LAST tt-relatorio-excel NO-LOCK NO-ERROR.
        IF AVAILABLE tt-relatorio-excel THEN DO:
          /*
            
            ASSIGN 
             tot-qt-faturada       =    (ACCUM TOTAL it-nota-fisc.qt-faturada[1])
             tot-vl-preuni         =    (ACCUM TOTAL it-nota-fisc.vl-preuni)
             tot-vl-tot-item       =    (ACCUM TOTAL it-nota-fisc.vl-tot-item)  
             /*
             tot-totitem           =    (ACCUM TOTAL wtotitem)                   
             */
             tot-vl-ipi-it         =    (ACCUM TOTAL it-nota-fisc.vl-ipi-it)    
             tot-vl-icms-it        =    (ACCUM TOTAL it-nota-fisc.vl-icms-it) 
             tot-vl-icmssub-it     =    (ACCUM TOTAL it-nota-fisc.vl-icmsub-it) 
             tot-wpiscofins        =    (ACCUM TOTAL wpiscofins)
             tot-vl-despes-it      =    (ACCUM TOTAL it-nota-fisc.vl-despes-it) 
             tot-wliqdet           =    (ACCUM TOTAL wliqdet)
             tot-wcomibruta        =    (ACCUM TOTAL wcomibruta) 
             tot-wcomiliqui        =    (ACCUM TOTAL wcomiliqui). */
          END.
          IF NOT AVAILABLE tt-relatorio-excel THEN MESSAGE "N�o EXISTE MOVIMENTA��O PARA O PER�ODO SELECIONADO." VIEW-AS ALERT-BOX
              WARNING.
         /* RUN gera_rel_excel.*/
        END.
   
 

       
   
 /* procedure excel */
PROCEDURE gera_rel_excel.
     RUN pi-acompanhar in h-acomp ("Gerando Excel. Aguarde ...").

    /*================================================================================
      Include para criacao de Planilha Excel
      ================================================================================*/
   
    assign c-arq-orig = "\\10.0.1.212\erp\esp\ems2\wpsf\origem.xls"
           c-arq-dest = "V:\spool\wpcc0004.xls".

/*
   assign c-arq-orig = "f:\desenv\Cria-Excel.xls"
           c-arq-dest = "V:\spool\wpcc0001.xls".*/

    def var i-cont as int.
    /*=== Cria Planilha Excel ===*/
    DO:

        IF SEARCH(c-arq-dest) <> ? THEN DOS SILENT DEL value(c-arq-dest).
        {office/office.i Excel chExcelApplication}
        /* cria uma nova Workbook */
        chWorkbook = chExcelApplication:Workbooks:Add(c-arq-orig) NO-ERROR.
        /* obt�m uma ativa Worksheet */
        chWorkSheet = chExcelApplication:Sheets:Item(1) NO-ERROR.
        /*
        ActiveChart = chExcelApplication:ChartObjects("Gr�fico 1") NO-ERROR.
        */
    END.

    /*
        chWorkSheet:ChartObjects("Gr�fico 1"):Activate().
        ActiveChart:PlotArea:Select().
        */

        /*
        ActiveChart:SetSourceData.

        ActiveChart:Source:Sheets("Plan1"):Range("C6:C22").
        */
       

        chExcelApplication:Columns("A:A"):Select().
        chExcelApplication:Selection:ColumnWidth = 6.
        
        chExcelApplication:Columns("B:B"):Select().
        chExcelApplication:Selection:ColumnWidth = 23.
        chExcelApplication:Selection:FONT:ColorIndex = 1.
                                                 
        chExcelApplication:Columns("C:C"):Select().
        chExcelApplication:Selection:NumberFormat = "00".
    
        chExcelApplication:Columns("f:f"):Select().
        chExcelApplication:Selection:ColumnWidth = 20.


        chExcelApplication:Columns("G:G"):Select().
        chExcelApplication:Selection:ColumnWidth = 25.

        chExcelApplication:Columns("H:H"):Select().
        chExcelApplication:Selection:ColumnWidth = 25.

        chExcelApplication:Columns("I:I"):Select().
        chExcelApplication:Selection:ColumnWidth = 15.
        chExcelApplication:Selection:NumberFormat = "@".

        chExcelApplication:Columns("J:J"):Select().
        chExcelApplication:Selection:NumberFormat = "0000000".
       
        chExcelApplication:Columns("I:I"):Select().
        chExcelApplication:Selection:ColumnWidth = 10.

        chExcelApplication:Columns("V:V"):Select().
        chExcelApplication:Selection:ColumnWidth = 12.

        chExcelApplication:Range("M3:T3"):Select().
        chExcelApplication:Selection:ColumnWidth = 12.

       chExcelApplication:Range("U:U"):Select().
       chExcelApplication:Selection:ColumnWidth = 08.

       chExcelApplication:Range("A4:W4"):Select().
       chExcelApplication:Selection:Font:Bold = True.


       /* TITULOS  */
       chExcelApplication:Range("F1"):Select().
       chExcelApplication:ActiveCell:FormulaR1C1 = "RELAT�RIO DE COMISS�ES - FORD".
       chExcelApplication:Selection:Font:Bold = True.

       chWorkSheet:Range("A4"):Value =  "Forn".
       chWorkSheet:Range("B4"):Value =  "Nome".
       chWorkSheet:Range("c4"):Value =  "Mes".
       chWorkSheet:Range("d4"):Value =  "Ano". 
       chWorkSheet:Range("e4"):Value =  "DN".  
       chWorkSheet:Range("F4"):Value =  "Nomedn".
       chWorkSheet:Range("G4"):Value =  "Peca".
       chWorkSheet:Range("H4"):Value =  "DescPeca".
       chWorkSheet:Range("I4"):Value =  "PedFord".      
       chWorkSheet:Range("J4"):Value =  "NF".         
       chWorkSheet:Range("K4"):Value =  "DataNF".
       chWorkSheet:Range("L4"):Value =  "Qtde".          
       chWorkSheet:Range("M4"):Value =  "VlrUnit". /*"VlrUnit.S/IPI"*.*/
       chWorkSheet:Range("N4"):Value =  "Vlrtotal". /*"VlrTotal C/IPI". */             
       chWorkSheet:Range("O4"):Value =  "IPI".            
       chWorkSheet:Range("P4"):Value =  "ICMS".        
       chWorkSheet:Range("Q4"):Value =  "ICMSSubst".      
       chWorkSheet:Range("R4"):Value =  "PIS/COFINS".       
       chWorkSheet:Range("S4"):Value =  "DespFinanc".            
       chWorkSheet:Range("T4"):Value =  "Vlrliq". /*"VlrLiq NF".*/
       chWorkSheet:Range("U4"):Value =  "PercCom". /*"%PerCom".*/  
       chWorkSheet:Range("v4"):Value =  "Vlrcom". /*"VLrComBruta".*/ 
       chWorkSheet:Range("w4"):Value =  "UF". 
       /*
       chWorkSheet:Range("X4"):Value =  "IRPF".
       chWorkSheet:Range("y4"):Value =  "VlrComLiqui".
      */
        ASSIGN i-cont = 4.
              
              FOR EACH  tt-relatorio-excel NO-LOCK:
               assign i-cont = i-cont + 1. 
               assign chWorkSheet:Range("A" + string(i-cont)):Value = string(tt-relatorio-excel.wcod-forn)   
                chWorkSheet:Range("B" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-nome) 
                chWorkSheet:Range("C" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-mes)              
                chWorkSheet:Range("D" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-ano)               
                chWorkSheet:Range("E" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-dn)    
                chWorkSheet:Range("F" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-nome-ab-cli)
                chWorkSheet:Range("G" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-codigo-refer)     
                chWorkSheet:Range("H" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-descricao-1)        
                chWorkSheet:Range("I" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-nr-pedfor)          
                chWorkSheet:Range("J" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-nr-nota-fis)     
                chWorkSheet:Range("K" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-dt-emis-nota)      
                chWorkSheet:Range("L" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-qt-faturada)      
                chWorkSheet:Range("M" + string(i-cont)):DecimalValue = (tt-relatorio-excel.ww-vl-preuni) 
                chWorkSheet:Range("N" + string(i-cont)):DecimalValue = (tt-relatorio-excel.ww-vl-tot-item)
                /*
                chWorkSheet:Range("N" + string(i-cont)):Value = (tt-relatorio-excel.ww-totitem)            
                */
                chWorkSheet:Range("O" + string(i-cont)):DecimalValue = (tt-relatorio-excel.ww-vl-ipi-it)          
                chWorkSheet:Range("P" + string(i-cont)):DecimalValue = (tt-relatorio-excel.ww-vl-icms-it)         
                chWorkSheet:Range("Q" + string(i-cont)):DecimalValue = (tt-relatorio-excel.ww-vl-icmsub-it)       
                chWorkSheet:Range("R" + string(i-cont)):Value = STRING(ROUND((tt-relatorio-excel.ww-piscofins),2))           
                chWorkSheet:Range("S" + string(i-cont)):DecimalValue = (tt-relatorio-excel.ww-vl-despes-it)       
                chWorkSheet:Range("T" + string(i-cont)):Value = STRING(ROUND((tt-relatorio-excel.ww-liqdet),2))             
                chWorkSheet:Range("U" + string(i-cont)):Value = STRING(tt-relatorio-excel.ww-percentual)         
                chWorkSheet:Range("V" + string(i-cont)):Value = STRING(ROUND((tt-relatorio-excel.ww-comibruta),2))          
                chWorkSheet:Range("W" + string(i-cont)):Value = STRING((tt-relatorio-excel.ww-estado)).  
                END.
                
                 FIND LAST tt-relatorio-excel NO-LOCK.
                 assign i-cont = i-cont + 2.   
                 ASSIGN chWorkSheet:Range("A" + string(i-cont)):Value = STRING("TOTAIS").   
                 chWorkSheet:Range("L" + string(i-cont)):SetValue((tt-relatorio-excel.tot-qt-faturada)).
                 chWorkSheet:Range("M" + string(i-cont)):SetValue((tt-relatorio-excel.tot-vl-preuni)).
                 chWorkSheet:Range("N" + string(i-cont)):SetValue((tt-relatorio-excel.tot-vl-tot-item)).
                 /*
                 chWorkSheet:Range("N" + string(i-cont)):SetValue((tt-relatorio-excel.tot-totitem)).
                 */
                 chWorkSheet:Range("O" + string(i-cont)):SetValue((tt-relatorio-excel.tot-vl-ipi-it)).
                 chWorkSheet:Range("P" + string(i-cont)):SetValue((tt-relatorio-excel.tot-vl-icms-it)).
                 chWorkSheet:Range("Q" + string(i-cont)):SetValue((tt-relatorio-excel.tot-vl-icmssub-it)).
                 chWorkSheet:Range("R" + string(i-cont)):SetValue(ROUND((tt-relatorio-excel.tot-wpiscofins),2)).
                 chWorkSheet:Range("S" + string(i-cont)):SetValue((tt-relatorio-excel.tot-vl-despes-it)).
                 chWorkSheet:Range("T" + string(i-cont)):SetValue(ROUND((tt-relatorio-excel.tot-wliqdet),2)).
                 chWorkSheet:Range("V" + string(i-cont)):SetValue(ROUND((tt-relatorio-excel.tot-wcomibruta),2)).
                 /*
                 chWorkSheet:Range("X" + string(i-cont)):SetValue(tt-relatorio-excel.ww-irrfper).
                 chWorkSheet:Range("Y" + string(i-cont)):SetValue(ROUND((tt-relatorio-excel.tot-wcomiliqui),2)).
                 */
               END.

               /*chWorkSheet:ActiveChart:SetSourceData. chWorkSheet:Source:Sheets("Plan1"):Range("A1:A15").*/

    /*==== Finaliza e Imprime Planilha  ====*/
    DO:
         chWorkbook:saveas(c-arq-dest) NO-ERROR.
        /*chworksheet:printout() NO-ERROR. */

        chExcelApplication:workbooks:Close() NO-ERROR.
        chExcelApplication:Quit() NO-ERROR.

        /* release com-handles */
        DELETE OBJECT chExcelApplication.      
        DELETE OBJECT chWorkbook.
        DELETE OBJECT chWorksheet.

    END.

    /* ---------------------------------------------------------------------------------------- fim procedure excel */
    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
