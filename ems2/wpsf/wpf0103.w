&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */


DEFINE VARIABLE wemp        LIKE estabelec.nome.
DEFINE VARIABLE wqt-it     AS   INTEGER FORMAT "999".
DEFINE VARIABLE wtot-nf    AS CHAR FORMAT "X(17)".
DEFINE VARIABLE wdesenho   AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcgccli    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcgcfor    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcgcford    LIKE emitente.cgc INITIAL "".
DEFINE VARIABLE wcont      AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont1     AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont2     AS INTEGER FORMAT "99".
DEFINE VARIABLE wclass     AS CHAR FORMAT "X(10)".
DEFINE VARIABLE wdif       AS INTE FORMAT 99.
DEFINE VARIABLE wdif1      AS INTE FORMAT 99.
DEFINE VARIABLE wabrecon   AS CHAR FORMAT "X(20)".
DEFINE VARIABLE wped-rev   AS INTEGER FORMAT "9999999999999".
DEFINE VARIABLE wpedido    AS INTEGER FORMAT "9999999".
DEFINE STREAM   w-log.
DEFINE VARIABLE wloc-entrega AS CHAR FORMAT "x(05)".
DEFINE VARIABLE wdata-emb    AS CHAR FORMAT "x(06)".
DEFINE VARIABLE wlinha       AS CHAR FORMAT "x(118)".

DEFINE BUFFER b-it-auxiliar FOR it-nota-fisc.
DEFINE VARIABLE wtot-itens   AS INTEGER FORMAT "999".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-6 bt-sair BUTTON-7 wcod-est ~
wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog bt-gera bt-cancelar ~
bt-ajuda 
&Scoped-Define DISPLAYED-OBJECTS wcod-est wdt-ini wdt-fim wnf-ini wnf-fim ~
wserie wrel wrellog 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Fechar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-gera 
     LABEL "Gerar arquivo" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-sair 
     LABEL "Sair" 
     SIZE 6 BY 1.13.

DEFINE BUTTON BUTTON-7 
     IMAGE-UP FILE "image/im-hel.bmp":U
     LABEL "Button 7" 
     SIZE 5 BY 1.13.

DEFINE VARIABLE wcod-est AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wdt-fim AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o Final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wdt-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wnf-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wnf-ini AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(25)":U 
     LABEL "Nome do relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 23 BY .88 NO-UNDO.

DEFINE VARIABLE wrellog AS CHARACTER FORMAT "X(25)":U 
     LABEL "Nome rel. de consist�ncia" 
     VIEW-AS FILL-IN 
     SIZE 23 BY .88 NO-UNDO.

DEFINE VARIABLE wserie AS CHARACTER FORMAT "X(5)":U 
     LABEL "S�rie" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 11.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bt-sair AT ROW 1.5 COL 66 WIDGET-ID 2
     BUTTON-7 AT ROW 1.5 COL 72
     wcod-est AT ROW 4.25 COL 35 COLON-ALIGNED
     wdt-ini AT ROW 5.25 COL 35 COLON-ALIGNED
     wdt-fim AT ROW 6.25 COL 35 COLON-ALIGNED
     wnf-ini AT ROW 7.25 COL 35 COLON-ALIGNED
     wnf-fim AT ROW 8.25 COL 35 COLON-ALIGNED
     wserie AT ROW 9.25 COL 35 COLON-ALIGNED
     wrel AT ROW 10.25 COL 35 COLON-ALIGNED
     wrellog AT ROW 11.25 COL 35 COLON-ALIGNED
     bt-gera AT ROW 14.25 COL 3
     bt-cancelar AT ROW 14.25 COL 55 HELP
          "Fechar"
     bt-ajuda AT ROW 14.25 COL 68 HELP
          "Ajuda"
     RECT-2 AT ROW 14 COL 2
     RECT-1 AT ROW 1.25 COL 1
     RECT-6 AT ROW 2.75 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81 BY 15
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Aviso de Embarque DSH - FORD - wpf0103 - V.01.004"
         HEIGHT             = 14.67
         WIDTH              = 81
         MAX-HEIGHT         = 18.71
         MAX-WIDTH          = 320
         VIRTUAL-HEIGHT     = 18.71
         VIRTUAL-WIDTH      = 320
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Aviso de Embarque DSH - FORD - wpf0103 - V.01.004 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Aviso de Embarque DSH - FORD - wpf0103 - V.01.004 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda C-Win
ON CHOOSE OF bt-ajuda IN FRAME DEFAULT-FRAME /* Ajuda */
DO:
  RUN pi_ajuda.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancelar C-Win
ON CHOOSE OF bt-cancelar IN FRAME DEFAULT-FRAME /* Fechar */
DO:
   apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-gera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-gera C-Win
ON CHOOSE OF bt-gera IN FRAME DEFAULT-FRAME /* Gerar arquivo */
DO:
  DISABLE wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie
          wrel wrellog bt-gera bt-sair WITH FRAME {&FRAME-NAME}.
  FORM
      nota-fiscal.nr-nota-fis  FORMAT "X(10)"
      nota-fiscal.cod-emitente 
      nota-fiscal.dt-emis-nota 
      it-nota-fisc.it-codigo
      it-nota-fisc.nr-pedcli COLUMN-LABEL "Ped.EMS"
      ped-item.nr-sequencia  
      it-nota-fisc.qt-faturada[1] 
      wpedido                COLUMN-LABEL "Pedido"
      SKIP
      "Vr.Unit.:   " it-nota-fisc.vl-preuni NO-LABEL
      "Vr.Tot.NF:"   nota-fiscal.vl-tot-nota NO-LABEL 
      SKIP
  HEADER 
  wemp
  TODAY          FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS")         
  "HS"
  "Pag.:"                                          AT 69
  PAGE-NUMBER    FORMAT "999"        
  SKIP
  "Arquivos -> Geracao:" wrel "Consistencia:" wrellog
  SKIP(1)
  "** Relatorio de Consistencia de geracao do arquivo de aviso de Embarque **"
  AT 02 SKIP
  "Referente dias:" AT 07 wdt-ini "a" wdt-fim "Notas:" wnf-ini "a" wnf-fim
  SKIP(1)
  WITH FRAME f-log DOWN WIDTH 120.
  
  ASSIGN wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog.

 OUTPUT TO VALUE (wrel).
    OUTPUT STREAM w-log TO VALUE(wrellog).
    /* procurar cnpf ford */
    FIND FIRST emitente WHERE emitente.nome-abrev = "FORD" NO-LOCK.
    ASSIGN wcgcford = emitente.cgc.

    FIND FIRST estabelec WHERE estabelec.cod-estabel = wcod-est 
                         NO-LOCK NO-ERROR.
    ASSIGN wemp = estabelec.nome.
    FOR EACH nota-fiscal where nota-fiscal.cod-emitente GE 200000
                         AND   nota-fiscal.cod-emitente LT 300000
                         AND   nota-fiscal.nr-nota-fis  GE wnf-ini 
                         AND   nota-fiscal.nr-nota-fis  LE wnf-fim
                         AND   nota-fiscal.dt-emis-nota GE wdt-ini
                         AND   nota-fiscal.dt-emis-nota LE wdt-fim
                         AND   nota-fiscal.dt-cancel    = ?
                         AND   nota-fiscal.cod-estabel  = wcod-est
                         AND   nota-fiscal.serie        = wserie
                         NO-LOCK BREAK BY nota-fiscal.dt-emis-nota:

       ASSIGN wtot-itens = 0.
       /* buffer para somar total de intes da nota */
       FOR EACH b-it-auxiliar OF nota-fiscal NO-LOCK:
          ASSIGN wtot-itens = wtot-itens + 1.
          END.   


       FIND FIRST natur-oper WHERE natur-oper.nat-operacao = 
                                   nota-fiscal.nat-operacao 
                                   NO-LOCK NO-ERROR.
  
       /* Se emitir duplicata gera arquivo */
       
       IF natur-oper.emite-duplic = yes THEN  DO:
         ASSIGN wqt-it  = 0
                wcont   = 0
                wcont1  = 0
                wcont2  = 0
                wcgccli = " "
                wcgcfor = " ".
              
         FOR EACH doc-fiscal WHERE doc-fiscal.cod-emitente 
                             = nota-fiscal.cod-emitente
                             AND doc-fiscal.dt-emis-doc  
                             = nota-fiscal.dt-emis-nota
                             AND doc-fiscal.nr-doc-fis   
                               = nota-fiscal.nr-nota-fis
                           NO-LOCK:
         FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK:
     
           ASSIGN wqt-it  = wqt-it + 1
                  wclass  = " "
                  wdif    = 0
                  wdif1   = 0.
           FIND FIRST item WHERE item.it-codigo = it-nota-fisc.it-codigo
                         NO-LOCK NO-ERROR.
           FIND FIRST fat-duplic WHERE fat-dupli.cod-estabel 
                                   = nota-fiscal.cod-estabel
                               AND fat-dupli.serie       
                                   = nota-fiscal.serie
                               AND fat-dupli.nr-fatura   
                                   = nota-fiscal.nr-fatura
                               NO-LOCK NO-ERROR.
           FIND FIRST ped-venda OF it-nota-fisc NO-LOCK NO-ERROR.
           FIND FIRST ped-item OF ped-venda 
                    WHERE ped-item.it-codigo = it-nota-fisc.it-codigo
                    NO-LOCK NO-ERROR.
           IF AVAILABLE ped-item THEN DO:
             ASSIGN wpedido = INTEGER(SUBSTRING(ped-item.observacao,1,7)).
             END.
           IF NOT AVAILABLE ped-item THEN DO: 
             MESSAGE 
             "PEDIDO NAO ENCONTRADO PARA O ITEM:" it-nota-fisc.it-codigo
             SKIP "NOTA:" it-nota-fisc.nr-nota-fis
             SKIP "OU NOTA GERADA SEM A UTILIZACAO DE PEDIDOS"
             SKIP "FAVOR VERIFICAR"
             SKIP "GERACAO DE ARQUIVO COM ERROS !!!"
             VIEW-AS ALERT-BOX.
             NEXT.
             END.
         PUT SCREEN ROW 23   "NF.: "     + STRING(nota-fiscal.nr-nota-fis)
                           + " Item: "   + STRING(ped-item.it-codigo)
                           + " Pedido: " + STRING(ped-venda.nr-pedcli).
         DISPLAY STREAM w-log
                 nota-fiscal.nr-nota-fis
                 nota-fiscal.cod-emitente 
                 nota-fiscal.dt-emis-nota 
                 it-nota-fisc.it-codigo
                 it-nota-fisc.nr-pedcli
                 ped-item.nr-sequencia  
                 it-nota-fisc.qt-faturada[1] 
                 it-nota-fisc.vl-preuni
                 nota-fiscal.vl-tot-nota 
                 wpedido
                 WITH FRAME f-log.
                 IF string(substring(ped-venda.observacoes,14,26)) = " " THEN
                   DISPLAY  STREAM w-log "!!!! ALERTA !!!! PEDIDO EM BRANCO"   
                   WITH FRAME f-log.
                   DISPLAY STREAM w-log "Pedido Revenda (campo observacao 14 a 26)= "
                   string(substring(ped-venda.observacoes,14,26)) FORMAT "x(13)"
                   wlinha SKIP(1) 
                   WITH FRAME f-log.
      DOWN STREAM w-log WITH FRAME f-log.
      PUT UNFORMATTED 
      "   "   /* reservado */
      
      integer(nota-fiscal.nr-nota-fis)    FORMAT "999999"
      nota-fiscal.serie          FORMAT "X(4)"    /* serie */
      nota-fiscal.dt-emis-nota   FORMAT "999999"  /* dt.nota */
      wtot-itens                 FORMAT "999" /* qt. itens por nota */
      /* alterado para wtot-itens em 29/02/09 conf. sol. Gaves - MGR
      wqt-it                     FORMAT "999"     /* qt.item */   */
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),17,2) 
            FORMAT "99"  /* vr.total nf */
      /*
      "2"*/ "0"         /* no. casas decimais para quant. */
      SUBSTRING(nota-fiscal.nat-operacao,1,3) FORMAT "999" /*nat oper */
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),17,2)
            FORMAT "99"   /* vr. icms */
      fat-duplic.dt-venciment FORMAT "999999"
      "02" /* nota-fiscal.esp-docto */
      SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),17,2)
            FORMAT "99"  /* vr. ipi */
      "020"              /* cod. fabrica */
      nota-fiscal.dt-emis-nota FORMAT "999999" /* previsao entrega */
      /*
      311299             /* nota-fiscal.dt-entr-cli FORMAT "999999" */
      */
      "    "
      natur-oper.denominacao FORMAT "X(20)" 
      "   "              /* reservado */
      SUBSTRING(STRING(it-nota-fisc.vl-despes-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-despes-it,"999999999999999.99"),17,2)
            FORMAT "99"       /* vr. despesas item */
      SUBSTRING(STRING(nota-fiscal.vl-frete,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-frete,"999999999999999.99"),17,2)
            FORMAT "99"       /* vr. frete */
      SUBSTRING(STRING(nota-fiscal.vl-seguro,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-seguro,"999999999999999.99"),17,2)
            FORMAT "99"       /* vr. seguro */
      "00000000000000000"     /* vr.desconto nf */
      SUBSTRING(STRING(doc-fiscal.vl-bicms,"999999999999999.99"),1,15)
            FORMAT "999999999999999"   
      SUBSTRING(STRING(doc-fiscal.vl-bicms,"999999999999999.99"),17,2)
            FORMAT "99"      /* vr. base icms */
      "00000000000000000"    /* vr.desconto icms */
      "   "                  /* reservado */
      it-nota-fisc.nr-seq-fat     FORMAT "999"        /* seq.fat */
      /* alterado conf. solicita��o da Ford. Sr. Carlos A. R. Alonso
     dia 02/03/2007*/
      STRING(wpedido,"9999999")  FORMAT "X(07)"       /* pedido */
      "     "                     
      ITEM.codigo-refer              FORMAT "x(30)"      /* item cli */
      /* alterado conf sol. ford 22/08/2006 - Carlos Alberto Alonso*/
      /*it-nota-fisc.qt-faturada[1] FORMAT "999999999"*/
      
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),1,7)
      FORMAT "9999999"
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"9999999.9999"),9,2)
      FORMAT "99"   
      

      it-nota-fisc.un-fatur[1].                      /* un-fatur. */
      
      
      /* CLASSIFICACAO */
        DO wcont2 = 1 TO 19:
          IF ASC(SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1)) <> 46
          THEN DO:
            ASSIGN wclass = wclass
                          + SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1).
          END.                   
        END.
        ASSIGN wclass = TRIM(wclass).
          IF LENGTH(wclass) < 10 THEN DO:
            ASSIGN wdif = 10 - LENGTH(wclass).
            DO wdif1 = 1 TO wdif:
              ASSIGN wclass = wclass + "0".
              END.
            END.
      PUT UNFORMATTED
      wclass            /* classificacao fiscal */
      SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),2,2)
            FORMAT "99"
      SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),5,2) 
            FORMAT "99" /* aliquota ipi */
      /* alterado conf.sol. ford. 22/08/2006* - Carlos Alberto Alonso */
      
      SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999999.99"),1,10)
            FORMAT "9999999999"
      SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999999.99"),12,2)
            FORMAT "99" /* vr. preco unitario */
      
      "000000000"       /* qt.item estoque       */

      item.un                 FORMAT "X(02)"   /* un. item */
      "000000000"             /* qt.unidade compra     */
      "  "                    /* unidade medida compra */
      "D"                     /* tipo de fornecimento  */
      SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),2,2)
            FORMAT "99"
      SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),5,2)
            FORMAT "99"       /* percentual desconto item */
      "00000000000"           /* Vr.tot.desc.item      */
      "   "                   /* reservado             */
      "   "                   /* reservado             */
      SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),2,2)
            FORMAT "99"
      SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),5,2)
            FORMAT "99"       /* aliquota icm */
      SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),17 ,2)
            FORMAT "99"      /* base icms item */
      SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),17 ,2)
            FORMAT "99"      /* vr. icms item */
      SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),17 ,2)
            FORMAT "99"       /* vr. ipi item */
      "  "                    /* sit.tributaria            */
      wdesenho                /* numero do desenho do item */
      "311299"                /* dt-val-desenho            */
      /* "AQUI"  novo campo com numero de pedido revenda */ 
      string(substring(ped-venda.observacoes,14,26)) FORMAT "x(13)"

      /*
      STRING(it-nota-fisc.nr-pedcli) FORMAT "X(13)"  /* pedido revenda */
      */
      SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),4,3)
            FORMAT "999"
      SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),8,2)
            FORMAT "99"       /* peso liq fat */
      "0" /* multiplicador de preco unitario*/
      SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),1,10)
            FORMAT "9999999999"
      SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),12,2)
            FORMAT "99"      /* vr. mercadoria */
            " "              /* situacao tributaria      */.
      FIND FIRST emitente WHERE emitente.nome-abrev = "FORD" NO-LOCK NO-ERROR.
        
      /* CGC CLIENTE */
      /*
      DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(emitente.cgc,wcont,1)) = 45
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 46
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(emitente.cgc,wcont,1).
          END.
      */    
      /* alterado 22/08 - conf.solicita��o do Sr. Carlos Alberto Alonso */
      /* CGC CLIENTE ford */
      
      DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(wcgcford,wcont,1)) = 45
          OR ASC(SUBSTRING(wcgcford,wcont,1)) = 46
          OR ASC(SUBSTRING(wcgcford,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(wcgcford,wcont,1).
          END.
         


      PUT UNFORMATTED TRIM(wcgccli) FORMAT "X(14)".  /* cgc cliente */
        /* CGC FORNECEDOR */
        DO wcont1 = 1 TO 19:
          IF ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  45
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  46
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgcfor = wcgcfor + SUBSTRING(estabelec.cgc,wcont1,1).
          END.
       ASSIGN wloc-entrega = "0" + SUBSTRING(STRING(nota-fiscal.cod-emitente),3,4).
       /*
       MESSAGE "Pedido" wpedido "Revenda" it-nota-fisc.nr-pedcli  view-as alert-box.
       */
       ASSIGN wdata-emb =  SUBSTRING(STRING(year(nota-fiscal.dt-emis-nota)),3,4) 
                        +  STRING(MONTH(nota-fiscal.dt-emis-nota),"99") 
                        +  STRING(DAY(nota-fiscal.dt-emis-nota),"99").
       

       PUT UNFORMATTED TRIM(wcgcfor) FORMAT "X(14)" /* cgc fornec.*/
          "000000000000" /* vl.tot desc icms item */
          "000"          /* cpof iten */
          "    "         /* alt. tec. item */
          "    "         /* ident.tipo transporte */
          wloc-entrega   /* codigo do  dn*/
          wdata-emb FORMAT "999999"
          "0000"
          SKIP.

      END.
    END.
  END. 
 END.     
 OUTPUT CLOSE.
 OUTPUT STREAM w-log CLOSE.
  
  MESSAGE "GERA��O CONCLU�DA, VEJA ARQUIVO LOG E TXT." VIEW-AS ALERT-BOX.
  ENABLE wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie
         bt-gera bt-sair wrel wrellog WITH FRAME {&FRAME-NAME}.
  DOS SILENT NOTEPAD.EXE VALUE(WREL).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-7 C-Win
ON CHOOSE OF BUTTON-7 IN FRAME DEFAULT-FRAME /* Button 7 */
DO:
  RUN pi_ajuda.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consistencia C-Win 
PROCEDURE consistencia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Relatorio de Consistencia */

         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-6 bt-sair BUTTON-7 wcod-est wdt-ini wdt-fim wnf-ini 
         wnf-fim wserie wrel wrellog bt-gera bt-cancelar bt-ajuda 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* AVISO */
ASSIGN     wdt-ini    = TODAY
           wdt-fim    = TODAY
           wcod-est   = "1"
           wdesenho   = FILL(" ",30)
           wnf-ini    = "0"
           wnf-fim    = "9999999"
           wrel       = "V:\spool\AF"   
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
           wrellog    = "V:\spool\CF"
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
           wserie       = "1"
           wcgccli      = ""
           wcgcfor      = ""
           wloc-entrega = "".
ASSIGN     wlinha       = FILL("_",118).
DISPLAY wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog
        WITH FRAME    {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi_ajuda C-Win 
PROCEDURE pi_ajuda :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
MESSAGE "ESTE PROGRAMA GERA AVISO DE EMBARQUE PARA FORD DSH"
        "PARA SER IMPORTADO NO EDIWISE"
        SKIP
        "ALTERA��ES:"
        SKIP
        "22/08/2006 - SOLICITA��O SR. CARLOS ALBERTO ALONSO - FORD, "
        "mudan�a no formato do campo preco-unit�rio e quantidade faturada e cgc"
        VIEW-AS ALERT-BOX.

/*layout mgr 20/02/2008

Posi��o                                                 
De      At�     Nome do Elemento        Tipo    Tam     Dec     Observa��es     Uso
                                                        
001     003     Reservado       A       3       0               M
004     009     N�mero Nf Origem        N       6       0               M
010     013     S�rie da Nota Fiscal Origem     A       4       0               M
014     019     Data da Nota Fiscal     N       6       0       AAMMDD ( Todos os Campos data )         M
020     022     Quant. Itens por Nota Fiscal    N       3       0               M
023     039     Valor Total da Nota Fiscal      N       17      2               M
040     040     Quant. de Casas Decimais        N       1       0       Casas decimais utilizadas nas quant.    M
041     043     Cod. Fiscal Opera��o    N       3       0               M
044     060     Valor Total do Icms Aplicado    N       17      2       Valor do ICMS Aplic. (NF)       M
061     066     Data de Vencimento do Docto.    N       6       0               M
067     068     Esp�cie da Nota Fiscal  N       2       0               M
069     085     Valor Total do IPI Aplicado     N       17      2       Valor do IPI Aplic (NF) M
086     088     C�d. F�brica de Destino A       3       0               M
089     094     Data Previs�o Entrega N.Fiscal  N       6       0               M
095     098     Per�odo de Entrega/Embarque     A       4       0               O
099     118     Descri��o da Natureza Opera��o  A       20      0               O
119     121     Reservado       A       3       0               M
122     138     Valor Despesas Acessorias       N       17      2               O
139     155     Valor do Frete  N       17      2               O
156     172     Valor do Seguro N       17      2               O
173     189     Valor do Desconto da NF N       17      2               O
190     206     Base de C�lculo do ICMS N       17      2       Base de Calculo do ICMS total da NF     M
207     223     Valor do Desc. Total do ICMS    N       17      2               O
224     226     Reservado       A       3       0               M
227     229     N�m. Sequ�ncia do Item na NF    N       3       0               M
230     241     N�mero do Pedido de Compra      A       12      0               M
242     271     C�digo do Item  A       30      0               M
272     280     Quant. Item na Nota Fiscal      N       9       0               M
281     282     Unidade Medida N.F.     A       2       0               M
283     292     C�digo Classifica��o Fiscal     N       10      0               M
293     296     Al�quota do IPI N       4       2       Al�quota de IPI do ITEM M
297     308     Valor Unit�rio do Item  N       12      5               M
309     317     Qtde do Item Unidade de Estoque N       9       0               O
318     319     Unidade Medida Estoque  A       2       0               O
320     328     Qtde Unidade Compra     N       9       0               O
329     330     Unidade Medida Compras  A       2       0               O
331     331     C�d. Tipo de Fornecimento       A       1       0               M
332     335     Quant. Percentual Desconto      N       4       2               O
336     346     Valor Total do Desconto Item    N       11      2               O
347     349     Reservado       A       3       0               M
350     352     Reservado       A       3       0               M
353     356     Al�quota do ICMS        N       4       2       Aliquota de ICMS no Item        M
357     373     Base de C�lculo do ICMS N       17      2       Base de calculo do ICMS no ITEM M
374     390     Valor do Icms Aplicado  N       17      2       Valor do ICMS Aplic. Para o ITEM        M
391     407     Valor do IPI Aplicado   N       17      2       Valor do IPI aplic para o ITEM  M
408     409     C�d. da Situa��o Tribut�ria     A       2       0               M
410     439     N�mero do Desenho do Item       A       30      0               O
440     445     Data de Validade do Desenho     N       6       0               O
446     458     Pedido da Revenda       A       13      0               O
459     463     Peso L�quido do Item    A       5       0               O
464     464     Multiplicador do Pre�o Unit�rio N       1       0               O
465     476     Pre�o Total da Mercadoria       N       12      2               O
477     477     Situa��o Tribut�ria Federal     A       1       0               O
478     491     Cnpj do Cliente N       14      0               M
492     505     Cnpj do Fornecedor      N       14      0               M
506     517     Vl. Total Desc. ICMS Item       N       12      2               O
518     520     CFO por item    N       3       0               O
521     524     Altera��o T�cnica do Item       A       4       0               O
525     528     Ident. Do Tipo de Tranporte     A       4       0               O
/* extras */
529     533     Codigo do DN    N       5                       
                Data de Embalagem       N       6                       
                vazio   N       4                       
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

