/*****************************************************************************
**
**       Programa: sf99191.p
**
**       Data....: 26/08/04
**
**       Autor...: DATASUL S.A.
**
**       Objetivo: REL.DETAL.FAT. FAM.COML./CLI/ITEM - 1 - PIS/COFINS
**
**       Vers�o..: 1.00.000 - super
**
**       OBS.....: Este fonte foi gerado pelo Data Viewer
**
*******************************************************************************/

define buffer empresa for mgcad.empresa. 

define variable c-prog-gerado 

 as character no-undo initial "SF99191".

def new global shared var c-arquivo-log    as char  format "x(60)"no-undo.
def new global shared var c-prg-vrs as char no-undo.
def new global shared var c-prg-obj as char no-undo.

run grapi/gr2013.p (input c-prog-gerado, input "2.00.00.000").

/****************** Defini��o de Tabelas Tempor�rias do Relat�rio **********************/

def temp-table tt-raw-digita
    field raw-digita as raw.

define temp-table tt-param
    field destino              as integer
    field arquivo              as char
    field usuario              as char
    field data-exec            as date
    field hora-exec            as integer
    field parametro            as logical
    field formato              as integer
    field v_num_tip_aces_usuar as integer
    field ep-codigo            as integer
    field da-dt-emis-nota-ini like nota-fiscal.dt-emis-nota
    field da-dt-emis-nota-fim like nota-fiscal.dt-emis-nota
    field i-cod-emitente-ini like nota-fiscal.cod-emitente
    field i-cod-emitente-fim like nota-fiscal.cod-emitente
    field c-serie-ini like nota-fiscal.serie
    field c-serie-fim like nota-fiscal.serie
    field c-fm-cod-com-ini like item.fm-cod-com
    field c-fm-cod-com-fim like item.fm-cod-com
.

/****************** INCLUDE COM VARI�VEIS GLOBAIS *********************/

def new global shared var i-ep-codigo-usuario  like empresa.ep-codigo no-undo.
def new Global shared var l-implanta           as logical    init no.
def new Global shared var c-seg-usuario        as char format "x(12)" no-undo.
def new global shared var i-num-ped-exec-rpw  as integer no-undo.   
def new global shared var i-pais-impto-usuario as integer format ">>9" no-undo.
def new global shared var l-rpc as logical no-undo.
def new global shared var r-registro-atual as rowid no-undo.
def new global shared var c-arquivo-log    as char  format "x(60)"no-undo.
def new global shared var i-num-ped as integer no-undo.         
def new global shared var v_cdn_empres_usuar   like empresa.ep-codigo        no-undo.
def new global shared var v_cod_usuar_corren   like usuar_mestre.cod_usuario no-undo.
def new global shared var h_prog_segur_estab     as handle                   no-undo.
def new global shared var v_cod_grp_usuar_lst    as char                     no-undo.
def new global shared var v_num_tip_aces_usuar   as int                      no-undo.
def new global shared var rw-log-exec            as rowid                    no-undo.


/****************** FIM INCLUDE COM VARI�VEIS GLOBAIS *********************/
/****************** Defini�ao de Par�metros do Relat�rio *********************/ 

/****************** Defini�ao de Vari�veis de Sele��o do Relat�rio *********************/ 

def new shared var da-dt-emis-nota-ini like nota-fiscal.dt-emis-nota format "99/99/9999" initial "01/01/0001" no-undo.
def new shared var da-dt-emis-nota-fim like nota-fiscal.dt-emis-nota format "99/99/9999" initial "12/31/9999" no-undo.
def new shared var i-cod-emitente-ini like nota-fiscal.cod-emitente format ">>>>>>>>9" initial 0 no-undo.
def new shared var i-cod-emitente-fim like nota-fiscal.cod-emitente format ">>>>>>>>9" initial 999999999 no-undo.
def new shared var c-serie-ini like nota-fiscal.serie format "x(5)" initial "" no-undo.
def new shared var c-serie-fim like nota-fiscal.serie format "x(5)" initial "ZZZZZ" no-undo.
def new shared var c-fm-cod-com-ini like item.fm-cod-com format "x(8)" initial "" no-undo.
def new shared var c-fm-cod-com-fim like item.fm-cod-com format "x(8)" initial "ZZZZZZZZ" no-undo.

/****************** Defini�ao de Vari�veis p/ Campos Virtuais do Relat�rio *******************/ 

/****************** Defini�ao de Vari�veis Campo do Layout do Relat�rio **********************/ 

def var wcofins as decimal label "COFINS".
def var wpis as decimal label "PIS".

/****************** Defini�ao de Vari�veis do Relat�rio N�o Pedidas em Tela ******************/ 

/****************** Defini�ao de Vari�veis de Total do Relat�rio *****************************/ 

def var v-wcofins-tt-022 like wcofins no-undo.
def var v-wcofins-tt-024 like wcofins no-undo.
def var v-wcofins-tt-026 like wcofins no-undo.
def var v-wpis-tt-021 like wpis no-undo.
def var v-wpis-tt-023 like wpis no-undo.
def var v-wpis-tt-025 like wpis no-undo.
def var de-quantidade-tt-001 like it-doc-fisc.quantidade no-undo.
def var de-quantidade-tt-011 like it-doc-fisc.quantidade no-undo.
def var de-quantidade-tt-016 like it-doc-fisc.quantidade no-undo.
def var de-vl-icms-it-tt-003 like it-doc-fisc.vl-icms-it no-undo.
def var de-vl-icms-it-tt-013 like it-doc-fisc.vl-icms-it no-undo.
def var de-vl-icms-it-tt-018 like it-doc-fisc.vl-icms-it no-undo.
def var de-vl-ipi-it-tt-004 like it-doc-fisc.vl-ipi-it no-undo.
def var de-vl-ipi-it-tt-014 like it-doc-fisc.vl-ipi-it no-undo.
def var de-vl-ipi-it-tt-019 like it-doc-fisc.vl-ipi-it no-undo.
def var de-vl-merc-liq-tt-002 like it-doc-fisc.vl-merc-liq no-undo.
def var de-vl-merc-liq-tt-012 like it-doc-fisc.vl-merc-liq no-undo.
def var de-vl-merc-liq-tt-017 like it-doc-fisc.vl-merc-liq no-undo.
def var de-vl-tot-item-tt-005 like it-doc-fisc.vl-tot-item no-undo.
def var de-vl-tot-item-tt-015 like it-doc-fisc.vl-tot-item no-undo.
def var de-vl-tot-item-tt-020 like it-doc-fisc.vl-tot-item NO-UNDO.
DEF VAR de-vl-icms-st-tt-it  LIKE  it-doc-fisc.vl-icmsub-it NO-UNDO.
DEF VAR de-vl-icms-st-tt-fam  LIKE  it-doc-fisc.vl-icmsub-it NO-UNDO.
DEF VAR de-vl-icms-st-tt-tot  LIKE  it-doc-fisc.vl-icmsub-it NO-UNDO.

/****************** Defini�ao de Vari�veis dos Calculos do Relat�rio *************************/ 

def input param raw-param as raw no-undo.
def input param table for tt-raw-digita.

/****************** Defini�ao de Vari�veis de Processamento do Relat�rio *********************/

def var h-acomp              as handle no-undo.
def var v-cod-destino-impres as char   no-undo.
def var v-num-reg-lidos      as int    no-undo.
def var v-num-point          as int    no-undo.
def var v-num-set            as int    no-undo.
def var v-num-linha          as int    no-undo.

/****************** Defini�ao de Forms do Relat�rio 132 Colunas ***************************************/ 
FORM HEADER
    "Parametros: Emitente: " i-cod-emitente-ini  " a " i-cod-emitente-fim 
    "Dt.Emissao: "           da-dt-emis-nota-ini " a " da-dt-emis-nota-fim 
    "Serie: "                c-serie-ini         " a " c-serie-fim
    "Familia: "              c-fm-cod-com-ini    " a " c-fm-cod-com-fim
    SKIP(1)
    WITH FRAME f-relat-09-132.

form nota-fiscal.cod-estabel column-label "Est" format "X(3)" 
     item.fm-cod-com column-label "Fam Coml" format "x(8)"  
     item.descricao-1 column-label "Descri��o" format "x(18)"  
     nota-fiscal.cod-emitente column-label "Cli/For" format ">>>>>>>>9"  
     it-doc-fisc.it-codigo column-label "Item" format "x(13)"  
     nota-fiscal.nr-nota-fis column-label "Nota Fis" format "x(12)"  
     it-doc-fisc.nat-operacao COLUMN-LABEL "Nat.Oper"
     it-doc-fisc.quantidade column-label "Qt" format "->,>>>,>>9.9999" 
     it-doc-fisc.vl-merc-liq column-label "Vl Mercad Liq" format ">,>>>,>>>,>>9.99" 
     it-doc-fisc.vl-icms-it column-label "Vl ICMS Item" format ">>,>>>,>>9.99" 
     it-doc-fisc.vl-icmsub-it COLUMN-LABEL "Vl ICMS ST" format ">>,>>>,>>9.99"  
     it-doc-fisc.vl-ipi-it format ">>>>,>>>,>>9.99" column-label "Vl IPI" 
     wcofins column-label "COFINS" format ">>>,>>>,>>>.99" 
     wpis column-label "PIS" format ">>>,>>>,>>>.99" 
     it-doc-fisc.vl-tot-item TO 205 format ">,>>>,>>>,>>9.99" column-label "Vl Tot It" 
     with down WIDTH 220 no-box stream-io frame f-relat-09-132.


create tt-param.
raw-transfer raw-param to tt-param.

def temp-table tt-editor no-undo
    field linha      as integer
    field conteudo   as character format "x(80)"
    index editor-id is primary unique linha.


def var rw-log-exec                            as rowid no-undo.
def var c-erro-rpc as character format "x(60)" initial " " no-undo.
def var c-erro-aux as character format "x(60)" initial " " no-undo.
def var c-ret-temp as char no-undo.
def var h-servid-rpc as handle no-undo.     
define var c-empresa       as character format "x(40)"      no-undo.
define var c-titulo-relat  as character format "x(50)"      no-undo.
define var i-numper-x      as integer   format "ZZ"         no-undo.
define var da-iniper-x     as date      format "99/99/9999" no-undo.
define var da-fimper-x     as date      format "99/99/9999" no-undo.
define var i-page-size-rel as integer                       no-undo.
define var c-programa      as character format "x(08)"      no-undo.
define var c-versao        as character format "x(04)"      no-undo.
define var c-revisao       as character format "999"        no-undo.

define new shared var c-impressora   as character                      no-undo.
define new shared var c-layout       as character                      no-undo.
define new shared var v_num_count     as integer                       no-undo.
define new shared var c-arq-control   as character                     no-undo.
define new shared var c-sistema       as character format "x(25)"      no-undo.
define new shared var c-rodape        as character                     no-undo.

define new shared buffer b_ped_exec_style for ped_exec.
define new shared buffer b_servid_exec_style for servid_exec.

define new shared stream str-rp.


if connected("dthrpyc") then do:
  def var v_han_fpapi003 as handle no-undo.
  run prghur/fpp/fpapi003.p persistent set v_han_fpapi003 (input tt-param.usuario,
                                                           input tt-param.v_num_tip_aces_usuar).
end.


assign c-programa     = "sf99191"
       c-versao       = "2.00"
       c-revisao      = ".00.000"
       c-titulo-relat = "REL.DETAL.FAT. FAM.COML./CLI/ITEM - 1 - PIS/COFINS"
       c-sistema      = "".

if  tt-param.formato = 2 then do:


form header
    fill("-", 132) format "x(132)" skip
    c-empresa c-titulo-relat at 50
    "Folha:" at 122 page-number(str-rp) at 128 format ">>>>9" skip
    fill("-", 112) format "x(110)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabec.

form header
    fill("-", 132) format "x(132)" skip
    c-empresa c-titulo-relat at 50
    "Folha:" at 122 page-number(str-rp) at 128 format ">>>>9" skip
    "Periodo:" i-numper-x at 08 "-"
    da-iniper-x at 14 "to" da-fimper-x
    fill("-", 74) format "x(72)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabper.

run grapi/gr2004.p.

form header
    c-rodape format "x(132)"
    with stream-io width 132 no-labels no-box page-bottom frame f-rodape.


end. /* tt-param.formato = 2 */


run grapi/gr2009.p (input tt-param.destino,
                    input tt-param.arquivo,
                    input tt-param.usuario,
                    input no).

    assign i-ep-codigo-usuario = string(tt-param.ep-codigo)
           v_cdn_empres_usuar  = i-ep-codigo-usuario.

    assign da-dt-emis-nota-ini = tt-param.da-dt-emis-nota-ini
           da-dt-emis-nota-fim = tt-param.da-dt-emis-nota-fim
           da-dt-emis-nota-fim = tt-param.da-dt-emis-nota-fim
           i-cod-emitente-ini = tt-param.i-cod-emitente-ini
           i-cod-emitente-fim = tt-param.i-cod-emitente-fim
           c-serie-ini = tt-param.c-serie-ini
           c-serie-fim = tt-param.c-serie-fim
           c-fm-cod-com-ini = tt-param.c-fm-cod-com-ini
           c-fm-cod-com-fim = tt-param.c-fm-cod-com-fim
.


find first empresa no-lock
    where empresa.ep-codigo = i-ep-codigo-usuario no-error.
if  avail empresa
then
    assign c-empresa = empresa.razao-social.
else
    assign c-empresa = "".

/* for each e disp */

def var l-imprime as logical no-undo.


        assign v-wcofins-tt-022 = 0
               v-wpis-tt-021 = 0
               de-quantidade-tt-001 = 0
               de-vl-icms-it-tt-003 = 0
               de-vl-ipi-it-tt-004 = 0
               de-vl-merc-liq-tt-002 = 0
               de-vl-tot-item-tt-005 = 0
               de-vl-icms-st-tt-it = 0.
assign l-imprime = no.
if  tt-param.destino = 1 then
    assign v-cod-destino-impres = "Impressora".
else
    if  tt-param.destino = 2 then
        assign v-cod-destino-impres = "Arquivo".
    else
        assign v-cod-destino-impres = "Terminal".


run utp/ut-acomp.p persistent set h-acomp.

run pi-inicializar in h-acomp(input "Acompanhamento Relat�rio").

assign v-num-reg-lidos = 0.

for each nota-fiscal no-lock
         where nota-fiscal.cod-emitente >= i-cod-emitente-ini and 
               nota-fiscal.cod-emitente <= i-cod-emitente-fim and
               nota-fiscal.dt-emis-nota >= da-dt-emis-nota-ini and 
               nota-fiscal.dt-emis-nota <= da-dt-emis-nota-fim and
               nota-fiscal.serie >= c-serie-ini and 
               nota-fiscal.serie <= c-serie-fim,
    each doc-fiscal no-lock
         where doc-fiscal.cod-estabel = nota-fiscal.cod-estabel and
               doc-fiscal.cod-emitente = nota-fiscal.cod-emitente and
               doc-fiscal.dt-emis-doc = nota-fiscal.dt-emis-nota and
               doc-fiscal.serie = nota-fiscal.serie and
               doc-fiscal.nat-operacao = nota-fiscal.nat-operacao and
               doc-fiscal.nr-doc-fis = nota-fiscal.nr-nota-fis,
    each it-doc-fisc no-lock
         where it-doc-fisc.cod-estabel = doc-fiscal.cod-estabel and
               it-doc-fisc.serie = doc-fiscal.serie and
               it-doc-fisc.nr-doc-fis = doc-fiscal.nr-doc-fis and
               it-doc-fisc.cod-emitente = doc-fiscal.cod-emitente and
               it-doc-fisc.nat-operacao = doc-fiscal.nat-operacao,
    each item no-lock
         where item.it-codigo = it-doc-fisc.it-codigo and
               item.fm-cod-com >= c-fm-cod-com-ini and 
               item.fm-cod-com <= c-fm-cod-com-fim and
               nota-fiscal.cod-estabel  = "1" and
               nota-fiscal.emite-duplic  = YES and
               nota-fiscal.dt-cancela  = ?
    break by nota-fiscal.cod-estabel
          by item.fm-cod-com
          by nota-fiscal.cod-emitente
          by it-doc-fisc.it-codigo:

    assign v-num-reg-lidos = v-num-reg-lidos + 1.
    run pi-acompanhar in h-acomp(input string(v-num-reg-lidos)).

    if  first-of(item.fm-cod-com) then do:
        assign v-wcofins-tt-026 = 0
               v-wpis-tt-025 = 0
               de-quantidade-tt-016 = 0
               de-vl-icms-it-tt-018 = 0
               de-vl-ipi-it-tt-019 = 0
               de-vl-merc-liq-tt-017 = 0
               de-vl-tot-item-tt-020 = 0
               de-vl-icms-st-tt-fam = 0.
    end.
    if  first-of(it-doc-fisc.it-codigo) then do:
        assign v-wcofins-tt-024 = 0
               v-wpis-tt-023 = 0
               de-quantidade-tt-011 = 0
               de-vl-icms-it-tt-013 = 0
               de-vl-ipi-it-tt-014 = 0
               de-vl-merc-liq-tt-012 = 0
               de-vl-tot-item-tt-015 = 0
               de-vl-icms-st-tt-it = 0.
    end.
 

    /* campos para ems204 - at� 17/12/10
    assign wpis = DECIMAL(SUBSTRING(it-doc-fisc.char-1,116,14)) .
    assign wcofins = DECIMAL(SUBSTRING(it-doc-fisc.char-1,144,14)) .
    */
    
    /* campos para EMS206 - a partir de 19/12/10*/
    assign wpis = val-pis
           wcofins = val-cofins.


    /***  C�DIGO PARA SA�DA EM 132 COLUNAS ***/

    if  tt-param.formato = 2 then do:

        view stream str-rp frame f-cabec.
        view stream str-rp frame f-rodape.
        assign l-imprime = yes.
        display stream str-rp nota-fiscal.cod-estabel
                              item.fm-cod-com
                              item.descricao-1
                              nota-fiscal.cod-emitente
                              it-doc-fisc.it-codigo
                              nota-fiscal.nr-nota-fis
                              it-doc-fisc.nat-operacao
                              it-doc-fisc.quantidade
                              it-doc-fisc.vl-merc-liq
                              it-doc-fisc.vl-icms-it
                              it-doc-fisc.vl-icmsub-it
                              it-doc-fisc.vl-ipi-it
                              wpis
                              wcofins
                              it-doc-fisc.vl-tot-item
                with stream-io frame f-relat-09-132.
            down stream str-rp with frame f-relat-09-132.
    end.
    IF it-doc-fisc.vl-merc-liq <> 0 THEN
    assign de-quantidade-tt-001 = de-quantidade-tt-001 + 
                                          it-doc-fisc.quantidade
           de-vl-merc-liq-tt-002 = de-vl-merc-liq-tt-002 + 
                                           it-doc-fisc.vl-merc-liq
           de-vl-icms-it-tt-003 = de-vl-icms-it-tt-003 + 
                                          it-doc-fisc.vl-icms-it
           de-vl-ipi-it-tt-004 = de-vl-ipi-it-tt-004 + 
                                         it-doc-fisc.vl-ipi-it
           de-vl-tot-item-tt-005 = de-vl-tot-item-tt-005 + 
                                           it-doc-fisc.vl-tot-item
           de-quantidade-tt-011 = de-quantidade-tt-011 + 
                                          it-doc-fisc.quantidade
           de-vl-merc-liq-tt-012 = de-vl-merc-liq-tt-012 + 
                                           it-doc-fisc.vl-merc-liq
           de-vl-icms-it-tt-013 = de-vl-icms-it-tt-013 + 
                                          it-doc-fisc.vl-icms-it
           de-vl-ipi-it-tt-014 = de-vl-ipi-it-tt-014 + 
                                         it-doc-fisc.vl-ipi-it
           de-vl-tot-item-tt-015 = de-vl-tot-item-tt-015 + 
                                           it-doc-fisc.vl-tot-item
           de-quantidade-tt-016 = de-quantidade-tt-016 + 
                                          it-doc-fisc.quantidade
           de-vl-merc-liq-tt-017 = de-vl-merc-liq-tt-017 + 
                                           it-doc-fisc.vl-merc-liq
           de-vl-icms-it-tt-018 = de-vl-icms-it-tt-018 + 
                                          it-doc-fisc.vl-icms-it
           de-vl-ipi-it-tt-019 = de-vl-ipi-it-tt-019 + 
                                         it-doc-fisc.vl-ipi-it
           de-vl-tot-item-tt-020 = de-vl-tot-item-tt-020 + 
                                           it-doc-fisc.vl-tot-item

          /* icms st valeria - 16/11/2010*/
           de-vl-icms-st-tt-it  =  de-vl-icms-st-tt-it +  it-doc-fisc.vl-icmsub-it 
           de-vl-icms-st-tt-fam =  de-vl-icms-st-tt-fam + it-doc-fisc.vl-icmsub-it 
           de-vl-icms-st-tt-tot =  de-vl-icms-st-tt-tot +  it-doc-fisc.vl-icmsub-it 
           v-wpis-tt-021 = v-wpis-tt-021 + 
                                   wpis
           v-wcofins-tt-022 = v-wcofins-tt-022 + 
                                      wcofins
           v-wpis-tt-023 = v-wpis-tt-023 + 
                                   wpis
           v-wcofins-tt-024 = v-wcofins-tt-024 + 
                                      wcofins
           v-wpis-tt-025 = v-wpis-tt-025 + 
                                   wpis
           v-wcofins-tt-026 = v-wcofins-tt-026 + 
                                      wcofins.

           

    if  last-of(it-doc-fisc.it-codigo) then do:
        if  tt-param.formato = 2 then do:
            display stream str-rp "--------------" @ 
                wcofins
                "--------------" @ 
                wpis
                "---------------" @ 
                it-doc-fisc.quantidade
                "-------------" @ 
                it-doc-fisc.vl-icms-it
                "---------------" @ 
                it-doc-fisc.vl-icmsub-it 
                "---------------" @ 
                it-doc-fisc.vl-ipi-it
                "----------------" @ 
                it-doc-fisc.vl-merc-liq
                "----------------" @ 
                it-doc-fisc.vl-tot-item
                with stream-io frame f-relat-09-132.
            down stream str-rp with frame f-relat-09-132.
        end.
        else  do:
            display stream str-rp "--------------" @ 
                wcofins
                "--------------" @ 
                wpis
                "---------------" @ 
                it-doc-fisc.quantidade
                "-------------" @ 
                it-doc-fisc.vl-icms-it
                 "---------------" @ 
                it-doc-fisc.vl-icmsub-it 
                "---------------" @ 
                it-doc-fisc.vl-ipi-it
                "----------------" @ 
                it-doc-fisc.vl-merc-liq
                "----------------" @ 
                it-doc-fisc.vl-tot-item
                with stream-io frame f-relat-09-80.
            down stream str-rp with frame f-relat-09-80.
        end.

        put stream  str-rp de-quantidade-tt-011 format "->,>>>,>>9.9999" TO 93.
        put stream str-rp  de-vl-merc-liq-tt-012     format ">,>>>,>>>,>>9.99" TO 110. 
        put stream str-rp  de-vl-icms-it-tt-013      format ">>,>>>,>>9.99"    TO 124.
        put stream str-rp  de-vl-icms-st-tt-it       format ">>,>>>,>>9.99"    TO 140.
        put stream str-rp  de-vl-ipi-it-tt-014       format ">>>>,>>>,>>9.99"  TO 156.
        put stream str-rp  v-wcofins-tt-024          format ">>>,>>>,>>>.99"   TO 171.
        put stream str-rp  v-wpis-tt-023             format ">>>,>>>,>>>.99"   TO 186.
        put stream str-rp  de-vl-tot-item-tt-015     format ">,>>>,>>>,>>9.99" TO 205.
        put stream str-rp unformatted skip(1).
        put stream str-rp unformatted skip(1).


    end.
    if  last-of(item.fm-cod-com) then do:
        if  tt-param.formato = 2 then do:
            display stream str-rp "--------------" @ 
                wcofins
                "--------------" @ 
                wpis
                "---------------" @ 
                it-doc-fisc.quantidade
                "-------------" @ 
                it-doc-fisc.vl-icms-it
                 "---------------" @ 
                it-doc-fisc.vl-icmsub-it 
                "---------------" @ 
                it-doc-fisc.vl-ipi-it
                "----------------" @ 
                it-doc-fisc.vl-merc-liq
                "----------------" @ 
                it-doc-fisc.vl-tot-item
                with stream-io frame f-relat-09-132.
            down stream str-rp with frame f-relat-09-132.
        end.
        else  do:
            display stream str-rp "--------------" @ 
                wcofins
                "--------------" @ 
                wpis
                "---------------" @ 
                it-doc-fisc.quantidade
                "-------------" @ 
                it-doc-fisc.vl-icms-it
                 "---------------" @ 
                it-doc-fisc.vl-icmsub-it 
                "---------------" @ 
                it-doc-fisc.vl-ipi-it
                "----------------" @ 
                it-doc-fisc.vl-merc-liq
                "----------------" @ 
                it-doc-fisc.vl-tot-item
                with stream-io frame f-relat-09-80.
            down stream str-rp with frame f-relat-09-80.
        end.
        put stream str-rp de-quantidade-tt-016 format "->,>>>,>>9.9999" TO 93.
        put stream str-rp de-vl-merc-liq-tt-017 format ">,>>>,>>>,>>9.99" TO 110.
        put stream str-rp de-vl-icms-it-tt-018 format ">>,>>>,>>9.99" TO 124.
        PUT STREAM str-rp de-vl-icms-st-tt-fam  format ">>,>>>,>>9.99" TO 140. 
        put stream str-rp de-vl-ipi-it-tt-019 format ">>>>,>>>,>>9.99" TO 156 .
        put stream str-rp v-wcofins-tt-026 format ">>>,>>>,>>>.99" TO 171.
        put stream str-rp v-wpis-tt-025 format ">>>,>>>,>>>.99" TO 186.
        put stream str-rp de-vl-tot-item-tt-020 format ">,>>>,>>>,>>9.99" TO 205.
        put stream str-rp unformatted skip(1).
       put stream str-rp unformatted skip(1).
    end.
    
end.

 

if  l-imprime = no then do:
    if  tt-param.formato = 2 then do:
        view stream str-rp frame f-cabec.
        view stream str-rp frame f-rodape.
    end.
    disp stream str-rp " " with stream-io frame f-nulo.
end.
    display stream str-rp "--------------" @ 
            wcofins
                "--------------" @ 
            wpis
                "---------------" @ 
            it-doc-fisc.quantidade
                "-------------" @ 
            it-doc-fisc.vl-icms-it
            "---------------" @ 
            it-doc-fisc.vl-icmsub-it 
            "---------------" @ 
            it-doc-fisc.vl-ipi-it
                "----------------" @ 
            it-doc-fisc.vl-merc-liq
                "----------------" @ 
            it-doc-fisc.vl-tot-item
            with stream-io frame f-relat-09-132.
    down stream str-rp with frame f-relat-09-132.
put stream str-rp de-quantidade-tt-001 format "->,>>>,>>9.9999" TO 93.
put stream str-rp de-vl-merc-liq-tt-002 format ">,>>>,>>>,>>9.99" TO 110.
put stream str-rp de-vl-icms-it-tt-003 format ">>,>>>,>>9.99" TO 124.
PUT STREAM str-rp de-vl-icms-st-tt-tot  format ">>,>>>,>>9.99" TO 140.
put stream str-rp de-vl-ipi-it-tt-004 format ">>>>,>>>,>>9.99" TO 156.
put stream str-rp v-wcofins-tt-022 format ">>>,>>>,>>>.99" TO 171.
put stream str-rp v-wpis-tt-021 format ">>>,>>>,>>>.99" TO 186.
put stream str-rp de-vl-tot-item-tt-005 format ">,>>>,>>>,>>9.99" TO 205.

run pi-finalizar in h-acomp.

if  tt-param.destino <> 1 then

    page stream str-rp.

if  tt-param.parametro then do:


   disp stream str-rp "SELE��O" skip(01) with stream-io frame f-imp-sel.
   disp stream str-rp 
      da-dt-emis-nota-ini colon 23 "|< >|"   at 47 da-dt-emis-nota-fim no-label
      i-cod-emitente-ini colon 23 "|< >|"   at 47 i-cod-emitente-fim no-label
      c-serie-ini colon 23 "|< >|"   at 47 c-serie-fim no-label
      c-fm-cod-com-ini colon 23 "|< >|"   at 47 c-fm-cod-com-fim no-label
        with stream-io side-labels overlay row 032 frame f-imp-sel.

   put stream str-rp unformatted skip(1) "IMPRESS�O" skip(1).

   put stream str-rp unformatted skip "    " "Destino : " v-cod-destino-impres " - " tt-param.arquivo format "x(40)".
   put stream str-rp unformatted skip "    " "Execu��o: " if i-num-ped-exec-rpw = 0 then "On-Line" else "Batch".
   put stream str-rp unformatted skip "    " "Formato : " if tt-param.formato = 1 then "80 colunas" else "132 colunas".
   put stream str-rp unformatted skip "    " "Usu�rio : " tt-param.usuario.

end.

else
output stream str-rp close.


if connected("dthrpyc") then
  delete procedure v_han_fpapi003.

procedure pi-print-editor:

    def input param c-editor    as char    no-undo.
    def input param i-len       as integer no-undo.

    def var i-linha  as integer no-undo.
    def var i-aux    as integer no-undo.
    def var c-aux    as char    no-undo.
    def var c-ret    as char    no-undo.

    for each tt-editor:
        delete tt-editor.
    end.

    assign c-ret = chr(255) + chr(255).

    do  while c-editor <> "":
        if  c-editor <> "" then do:
            assign i-aux = index(c-editor, chr(10)).
            if  i-aux > i-len or (i-aux = 0 and length(c-editor) > i-len) then
                assign i-aux = r-index(c-editor, " ", i-len + 1).
            if  i-aux = 0 then
                assign c-aux = substr(c-editor, 1, i-len)
                       c-editor = substr(c-editor, i-len + 1).
            else
                assign c-aux = substr(c-editor, 1, i-aux - 1)
                       c-editor = substr(c-editor, i-aux + 1).
            if  i-len = 0 then
                assign entry(1, c-ret, chr(255)) = c-aux.
            else do:
                assign i-linha = i-linha + 1.
                create tt-editor.
                assign tt-editor.linha    = i-linha
                       tt-editor.conteudo = c-aux.
            end.
        end.
        if  i-len = 0 then
            return c-ret.
    end.
    return c-ret.
end procedure.

return 'OK'.

/* fim do programa */
