&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */


DEFINE VARIABLE wemp        LIKE estabelec.nome.
DEFINE VARIABLE wqt-it     AS   INTEGER FORMAT "999".
DEFINE VARIABLE wtot-nf    AS CHAR FORMAT "X(17)".
DEFINE VARIABLE wdesenho   AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcgccli    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcgcfor    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcont      AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont1     AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont2     AS INTEGER FORMAT "99".
DEFINE VARIABLE wclass     AS CHAR FORMAT "X(10)".
DEFINE VARIABLE wdif       AS INTE FORMAT 99.
DEFINE VARIABLE wdif1      AS INTE FORMAT 99.
DEFINE VARIABLE wabrecon   AS CHAR FORMAT "X(20)".
DEFINE STREAM   w-log.


DEFINE VARIABLE wvenc      LIKE fat-duplic.dt-venciment.
DEFINE VARIABLE wtot-itens            AS INTEGER.
DEFINE VARIABLE wfab       AS CHAR FORMAT "x(03)".

DEFINE VARIABLE wseq-it               AS INTEGER FORMAT "999".
DEFINE VARIABLE wtam-item             AS INTEGER FORMAT "99".

DEFINE VARIABLE wun-cond              AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wqt-cond              AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wun-mov               AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wdent-embal           AS CHAR FORMAT "X(20)".
DEFINE VARIABLE wpc-embal             AS INTEGER FORMAT "99999999".
DEFINE VARIABLE wnf-embal             AS INTEGER FORMAT "999999".
DEFINE VARIABLE wserie-ori            AS CHAR FORMAT "X(04)".
DEFINE VARIABLE wdt-nf-for            AS INTEGER FORMAT "999999".
DEFINE VARIABLE wqt-embals            AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wae5-espaco           AS CHAR FORMAT "X(03)".



DEFINE VARIABLE witem                 LIKE ITEM.codigo-refer.
DEFINE VARIABLE wwpedido   AS CHAR FORMAT "X(12)".
DEFINE VARIABLE wqt-registro          AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wtot-valores          LIKE it-nota-fisc.vl-preuni.
DEFINE BUFFER   b-it-auxiliar         FOR it-nota-fisc.
DEFINE VARIABLE wnarrativa            LIKE item-cli.narrativa[1].
DEFINE VARIABLE wwitem                AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcont-item            AS INTEGER FORMAT "99".
DEFINE VARIABLE wespaco               AS CHAR FORMAT "x(93)".
/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR FORMAT "x(40)" NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.

/* Vers�o 08/2005 
   - Inclusa� de linha do pedido
   - insercao n. nota de saida no campo nota fiscal embalagem */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-6 RECT-8 RECT-9 wcod-est wdt-ini ~
wdt-fim wnf-ini wnf-fim wserie WREL wrellog bt-gera bt-sair 
&Scoped-Define DISPLAYED-OBJECTS wcod-est wdt-ini wdt-fim wnf-ini wnf-fim ~
wserie WREL wrellog 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-gera 
     LABEL "Gera arquivo" 
     SIZE 13 BY 1.13.

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wcod-est AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-fim AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o Final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnf-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnf-ini AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE WREL AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome do relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE wrellog AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome rel. de consist�ncia" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE wserie AS CHARACTER FORMAT "X(5)":U 
     LABEL "S�rie" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 68 BY 10.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 69 BY .5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 69 BY .5
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wcod-est AT ROW 3.75 COL 34 COLON-ALIGNED
     wdt-ini AT ROW 4.75 COL 34 COLON-ALIGNED
     wdt-fim AT ROW 5.75 COL 34 COLON-ALIGNED
     wnf-ini AT ROW 6.75 COL 34 COLON-ALIGNED
     wnf-fim AT ROW 7.75 COL 34 COLON-ALIGNED
     wserie AT ROW 8.75 COL 34 COLON-ALIGNED
     WREL AT ROW 9.75 COL 34 COLON-ALIGNED
     wrellog AT ROW 10.75 COL 34 COLON-ALIGNED
     bt-gera AT ROW 13.25 COL 6
     bt-sair AT ROW 13.25 COL 19
     RECT-6 AT ROW 3 COL 6
     RECT-8 AT ROW 2 COL 5
     RECT-9 AT ROW 14.25 COL 6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 80.72 BY 15
         FONT 1
         DEFAULT-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Aviso de Embarque - DSH/ VW -PADR�O RND 004 V.15 - wpf0109 V.08/2005"
         HEIGHT             = 15
         WIDTH              = 80.72
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Aviso de Embarque - DSH/ VW -PADR�O RND 004 V.15 - wpf0109 V.08/2005 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Aviso de Embarque - DSH/ VW -PADR�O RND 004 V.15 - wpf0109 V.08/2005 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-gera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-gera C-Win
ON CHOOSE OF bt-gera IN FRAME DEFAULT-FRAME /* Gera arquivo */
DO:
  DISABLE wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog
          bt-gera bt-sair WITH FRAME {&FRAME-NAME}.
  FORM
      nota-fiscal.nr-nota-fis  FORMAT "X(10)"
      nota-fiscal.cod-emitente 
      nota-fiscal.dt-emis-nota 
      it-nota-fisc.it-codigo
      witem
      it-nota-fisc.nr-pedcli 
      ped-item.nr-sequencia  
      it-nota-fisc.qt-faturada[1] 
      it-nota-fisc.vl-preuni COLUMN-LABEL "Vr.unit."
      nota-fiscal.vl-tot-nota COLUMN-LABEL "Vr.Tot.NF."
  HEADER 
  wemp
  TODAY          FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS")         
  "HS"
  "Pag.:"                                          AT 69
  PAGE-NUMBER    FORMAT "999"        
  SKIP
  "Arquivos -> Geracao:" wrel "Consistencia:" wrellog
  SKIP(1)
  "** Relatorio de Consistencia de geracao do arquivo de aviso de Embarque DSH**"
  AT 02 SKIP
  "Referente dias:" AT 07 wdt-ini "a" wdt-fim "Notas:" wnf-ini "a" wnf-fim
  SKIP(1)
  WITH FRAME f-log DOWN WIDTH 250.
  
  ASSIGN wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog.

  OUTPUT TO VALUE (wrel).
  OUTPUT STREAM w-log TO VALUE(wrellog).
  FIND FIRST estabelec WHERE estabelec.cod-estabel = wcod-est 
                         NO-LOCK NO-ERROR.
  FIND FIRST emitente WHERE emitente.nome-abrev = "VW SBC" NO-LOCK NO-ERROR.
        /* CGC CLIENTE */
        DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(emitente.cgc,wcont,1)) = 45
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 46
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(emitente.cgc,wcont,1).
          END.
        /* CGC FORNECEDOR */
        DO wcont1 = 1 TO 19:
          IF ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  45
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  46
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgcfor = wcgcfor + SUBSTRING(estabelec.cgc,wcont1,1).
          END.
   RUN DISP_itp.

  ASSIGN  wemp = estabelec.nome.
  FOR EACH nota-fiscal where nota-fiscal.cod-emitente GE 100000 
                         AND   nota-fiscal.cod-emitente LT 200000 
                         AND   nota-fiscal.nr-nota-fis  GE wnf-ini 
                         AND   nota-fiscal.nr-nota-fis  LE wnf-fim
                         AND   nota-fiscal.dt-emis-nota GE wdt-ini
                         AND   nota-fiscal.dt-emis-nota LE wdt-fim
                         AND   nota-fiscal.dt-cancel    = ?
                         AND   nota-fiscal.cod-estabel  = wcod-est
                         AND   nota-fiscal.serie        = wserie
                         NO-LOCK BREAK BY nota-fiscal.dt-emis-nota:
       ASSIGN wtot-itens  = 0
              wseq-it     = 0.
             
       /* buffer para somar total de intes da nota */
       FOR EACH b-it-auxiliar OF nota-fiscal NO-LOCK:
          ASSIGN wtot-itens = wtot-itens + 1.
       END.  
       FIND FIRST natur-oper WHERE natur-oper.nat-operacao = 
                                   nota-fiscal.nat-operacao 
                                   NO-LOCK NO-ERROR.
    
       /* Se emitir duplicata gera arquivo */
       
       IF natur-oper.emite-duplic = yes THEN  DO:
         ASSIGN wqt-it  = 0
                wcont   = 0
                wcont1  = 0
                wcont2  = 0
                wcgccli = " "
                wcgcfor = " "
                wnarrativa = ""
                wwitem     = ""
                wvenc      = ?.
         FOR EACH doc-fiscal WHERE doc-fiscal.cod-emitente 
                             = nota-fiscal.cod-emitente
                             AND doc-fiscal.dt-emis-doc  
                             = nota-fiscal.dt-emis-nota
                             AND doc-fiscal.nr-doc-fis   
                               = nota-fiscal.nr-nota-fis
                           NO-LOCK:
         
             
       FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.nr-nota-fis:
         FIND FIRST ITEM OF it-nota-fisc NO-LOCK.
         IF fm-codigo = "FM-89" THEN NEXT.
         ASSIGN wqt-it  = wqt-it + 1
                  wclass  = " "
                  wdif    = 0
                  wdif1   = 0
                  wseq-it = wseq-it + 1
                  wtam-item = 0.
             
         FIND FIRST item WHERE item.it-codigo = it-nota-fisc.it-codigo
                         NO-LOCK NO-ERROR.

         ASSIGN witem = " ".
         ASSIGN witem = SUBSTRING(codigo-refer,1,17).

         /*
        /* CODIGO DO ITEM DO CLIENTE */
        DO wcont-item = 1 TO 20:
          IF ASC(SUBSTRING(ITEM.codigo-refer,wcont-item,1)) = 46
          THEN DO:
            NEXT.
            END.
          ASSIGN witem = witem + SUBSTRING(item.codigo-refer,wcont-item,1).
         
          ASSIGN wtam-item = LENGTH(witem).
              /* Tira pontos dos itens e coloca no formato da VW */
              IF length(item.codigo-refer) = 13 
              OR length(item.codigo-refer) = 14 THEN DO:
                ASSIGN witem = SUBSTRING(ITEM.codigo-refer,1,3)
                    + "   "
                    + SUBSTRING(ITEM.codigo-refer,5,3)
                    + SUBSTRING(ITEM.codigo-refer,9,3)
                    + SUBSTRING(ITEM.CODIGO-REFER,13,2).
                END.
              IF length(item.codigo-refer) = 17 THEN DO:
                ASSIGN witem = SUBSTRING(ITEM.codigo-refer,1,3)
                    + "   "
                    + SUBSTRING(ITEM.codigo-refer,5,3)
                    + SUBSTRING(ITEM.codigo-refer,9,3)
                    + SUBSTRING(ITEM.CODIGO-REFER,13,1)
                    + " " 
                    + SUBSTRING(item.codigo-refer,15,3).
                END.
             IF length(item.codigo-refer) = 15 THEN DO:
                ASSIGN witem = SUBSTRING(ITEM.codigo-refer,1,3)
                    + "   "
                    + SUBSTRING(ITEM.codigo-refer,5,3)
                    + SUBSTRING(ITEM.codigo-refer,9,3)
                    + "  " 
                    + SUBSTRING(item.codigo-refer,13,3).
                END.
             IF LENGTH(ITEM.codigo-refer) = 18 THEN DO:
                ASSIGN witem = SUBSTRING(ITEM.codigo-refer,1,3)
                    + "   "
                    + SUBSTRING(ITEM.codigo-refer,5,3)
                    + SUBSTRING(ITEM.codigo-refer,9,3)
                    + SUBSTRING(ITEM.CODIGO-REFER,13,2)
                    + SUBSTRING(item.codigo-refer,16,3).
                END.
            END.

           */
         FIND FIRST fat-duplic WHERE fat-dupli.cod-estabel 
                                   = nota-fiscal.cod-estabel
                               AND fat-dupli.serie       
                                   = nota-fiscal.serie
                               AND fat-dupli.nr-fatura   
                                   = nota-fiscal.nr-fatura
                               NO-LOCK NO-ERROR.
           ASSIGN wvenc = fat-duplic.dt-vencimen.
        IF wvenc = ? THEN DO:
        MESSAGE "ERROR"
        SKIP "NAO ENCONTROU DATA DE VENCIMENTO PARA NF:" nota-fiscal.nr-nota-fis
        VIEW-AS ALERT-BOX.     
        LEAVE.
        END.
         FIND FIRST ped-venda OF it-nota-fisc NO-LOCK NO-ERROR.
         FIND FIRST ped-item OF ped-venda 
                    WHERE ped-item.it-codigo = it-nota-fisc.it-codigo
                    NO-LOCK NO-ERROR.
         IF NOT AVAILABLE ped-item THEN DO: 
           MESSAGE 
           "PEDIDO NAO ENCONTRADO PARA O ITEM:" it-nota-fisc.it-codigo
           SKIP "NOTA:" it-nota-fisc.nr-nota-fis
           SKIP "OU NOTA GERADA SEM A UTILIZACAO DE PEDIDOS"
           SKIP "FAVOR VERIFICAR"
           SKIP "GERACAO DE ARQUIVO COM ERROS !!!"
           VIEW-AS ALERT-BOX.
           NEXT.
           END.
         PUT SCREEN ROW 23   "NF.: "     + STRING(nota-fiscal.nr-nota-fis)
                           + " Item: "   + STRING(ped-item.it-codigo)
                           + " Pedido: " + STRING(ped-venda.nr-pedcli).
         DISPLAY STREAM w-log
                 nota-fiscal.nr-nota-fis
                 nota-fiscal.cod-emitente 
                 nota-fiscal.dt-emis-nota 
                 it-nota-fisc.it-codigo
                 witem
                 it-nota-fisc.nr-pedcli 
                 ped-item.nr-sequencia  
                 it-nota-fisc.qt-faturada[1] 
                 it-nota-fisc.vl-preuni
                 nota-fiscal.vl-tot-nota 
                 WITH FRAME f-log.
      DOWN STREAM w-log WITH FRAME f-log.
     
      /* Classifica��o  */
        DO wcont2 = 1 TO 19:
          IF ASC(SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1)) <> 46
          THEN DO:
            ASSIGN wclass = wclass
                          + SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1).
          END.                   
        END.
        ASSIGN wclass = TRIM(wclass).
          IF LENGTH(wclass) < 10 THEN DO:
            ASSIGN wdif = 10 - LENGTH(wclass).
            DO wdif1 = 1 TO wdif:
              ASSIGN wclass = wclass + "0".
              END.
            END.

       IF FIRST-of(it-nota-fisc.nr-nota-fis) THEN RUN  DISP_ae1.
         RUN DISP_dados.
       END.
   END.
  END. 
  END.
  RUN encerra.
  OUTPUT CLOSE.
  OUTPUT STREAM w-log CLOSE.
  MESSAGE "GERA��O CONCLU�DA" SKIP "NOME DO ARQUIVO: " + STRING(WREL)
           VIEW-AS ALERT-BOX.
  ENABLE wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie
         bt-gera bt-sair wrel wrellog WITH FRAME {&FRAME-NAME}.
  RUN ver-consis.
  RUN inicia.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consistencia C-Win 
PROCEDURE consistencia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Relatorio de Consistencia */

         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_ae1 C-Win 
PROCEDURE disp_ae1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/* AE1 */
      PUT UNFORMATTED
      "AE1"
      INTEGER(nota-fiscal.nr-nota-fis)   FORMAT "999999"
            nota-fiscal.serie            FORMAT "X(4)"
      SUBSTRING(STRING(YEAR(dt-emis-nota),"9999"),3,4) FORMAT "99"
            MONTH(dt-emis-nota) FORMAT "99"
            DAY(dt-emis-nota) FORMAT "99"
      wtot-itens              FORMAT "999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),17,2) 
            FORMAT "99"
            /* anterior "2" */
            "0" /* no. casas decimais para quant. */
      SUBSTRING(nota-fiscal.nat-operacao,1,3) FORMAT "99999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),17,2)
            FORMAT "99" 
      SUBSTRING(STRING(YEAR(wvenc),"9999"),3,4) FORMAT "99"
            MONTH(wvenc) FORMAT "99"
            DAY(wvenc) FORMAT "99"
            "02" /* nota-fiscal.esp-docto */
             SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
            SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),17,2)
            FORMAT "99" 
            wfab FORMAT "999"
            "991231" /* nota-fiscal.dt-entr-cli FORMAT "999999" */
              "    "                  /* periodo de entrega */
            natur-oper.denominacao FORMAT "X(15)" 
            SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99"
            MONTH(TODAY) FORMAT "99"
            DAY(TODAY) FORMAT "99"
            SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
            SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
            "   "
            SKIP.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_dados C-Win 
PROCEDURE disp_dados :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* PROCEDURES */
    
      /* AE2*/
      PUT UNFORMATTED 
      "AE2"                        /* tipo do registro */
      wseq-it     FORMAT "999"    /* num. item na nota */
      /* altera��o feita para inserir numero da linha - conforme instru��o marcos - gedas
      24/07/2005 */
      it-nota-fisc.nr-pedcli      FORMAT "9999999"    /* pedcli  */
      ped-item.nr-sequencia       FORMAT "99999"      /* seq. ped  - numero da linha */
      /*
      it-nota-fisc.nr-pedcli      FORMAT "X(12)"  /* pedido */
      */
      witem                       FORMAT "X(30)" /* cod-item */
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"999999999.9999"),1,9)
      FORMAT "999999999"
      it-nota-fisc.un-fatur[1] FORMAT "X(02)"
                  wclass FORMAT "X(10)"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),5,2) 
                  FORMAT "99"
                  SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999.99999"),1,7)
                  FORMAT "9999999"
                  SUBSTRING(string(it-nota-fisc.vl-preuni,"9999999.99999"),9,5)
                  FORMAT "99999"
                  "000000000"             /* qt.item estoque       */
                  item.un                 FORMAT "X(02)"
                  "000000000"             /* qt.unidade compra     */
                  "  "                    /* unidade medida compra */
                  "R"                     /* tipo de fornecimento  */
                  SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),2,2)
                  FORMAT "99"
                  SUBSTRING(STRING(it-nota-fisc.per-des-item,"-99.99999"),5,2)
                  FORMAT "99"
                  "00000000000"           /* Vr.tot.desc.item      */
                  "    "                  /* alteracao tec.item    */
                  " "                     /* espa�o*/
                  SKIP
            /*AE4 */
                  "AE4"
            SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),2,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),5,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),17 ,2)
                  FORMAT "99"
                  "  "                    /* sit.tributaria            */
                  wdesenho                /* numero do desenho do item */
                  "311299"                /* dt-val-desenho            */
                  "             "         /* PED REVENDA */
            SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),4,3)
                  FORMAT "999"
            SUBSTRING(STRING(it-nota-fisc.peso-liq-fat,"999999.9999"),8,2)
                  FORMAT "99"
                  "0"
            SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),1,10)
                  FORMAT "9999999999"
            SUBSTRING(string(it-nota-fisc.vl-merc-ori,"9999999999.99999"),12,2)
                  FORMAT "99"
                  " " /* situacao tributaria      */
                  SKIP.
            /* Referente embalagem  AE5*/
/* Campos de embalagem assinalados conforme orienta��o
           do Sr. Marcos da Gedas em 15/08/2005, para evitar
           mensagens de alerta para o Sr. Nelson Yamaguti 
           Campos: numero da nota embalagem e data iguais 
           aos da nota fiscal de faturamento*/
           
     ASSIGN wnf-embal = INTEGER(nota-fiscal.nr-nota-fis).
    
           PUT UNFORMATTED 
           "AE5"
            wun-cond              FORMAT "X(30)"
            wqt-cond              FORMAT "999999999"
            wun-mov               FORMAT "X(30)"
            wdent-embal           FORMAT "X(20)"
            wpc-embal             FORMAT "99999999"
            wnf-embal             FORMAT "999999"
            wserie-ori            FORMAT "X(04)"
            /*wdt-nf-for            FORMAT "999999"*/
            SUBSTRING(STRING(YEAR(nota-fiscal.dt-emis-nota),"9999"),3,4) FORMAT "99"
            MONTH(nota-fiscal.dt-emis-nota) FORMAT "99"
            DAY(nota-fiscal.dt-emis-nota) FORMAT "99" 
            wqt-embals            FORMAT "999999999"
            wae5-espaco           FORMAT "X(03)"
            SKIP. 

      /* TOTAL DE TRANSA��ES */
       ASSIGN wqt-registro  =  wqt-registro + 1
              wtot-valores  = wtot-valores + it-nota-fisc.vl-preuni.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_itp C-Win 
PROCEDURE disp_itp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

   PUT UNFORMATTED 
      "ITP" /* REGISTRO*/
      "004" /* TIPO RND 004*/
      "15"  /* VERS�O RND */
      "00001"
      SUBSTRING(STRING(YEAR(TODAY),"9999"),3,4) FORMAT "99"
      MONTH(TODAY) FORMAT "99"
      DAY(TODAY) FORMAT "99"
      SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
      SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
      TRIM(wcgcfor) FORMAT "X(14)"
      TRIM(wcgccli) FORMAT "X(14)"
      FILL(" ",16)
      "SEEBER FASTPLAS LTDA."
      "VOLKSWAGEN DO BRASIL S/A" AT 95
      FILL(" ",9)
      SKIP.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie WREL wrellog 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-6 RECT-8 RECT-9 wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie 
         WREL wrellog bt-gera bt-sair 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE encerra C-Win 
PROCEDURE encerra :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/********************************************************/

  PUT UNFORMATTED 
      "FTP"
      "00000"  /*nO.CONTROLE TRANSMISS�O */
      wqt-registro + 1 /* qt registros */ FORMAT "999999999"
      SUBSTRING(STRING(wtot-valores,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(wtot-valores,"999999999999999.99"),17,2) 
            FORMAT "99"
      " "
      wespaco FORMAT "X(93)".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN warquivo = wrellog.
  get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
  if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
  end.
  run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* AVISO */
ASSIGN     wdt-ini    = TODAY
           wdt-fim    = TODAY
           wcod-est   = "1"
           wdesenho   = FILL(" ",30)
           wnf-ini    = "0"
           wnf-fim    = "9999999"
           wrel       = "V:\spool\ADSH"   
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
           wrellog    = "V:\spool\CDSH"
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
          wfab       = "DSH"
          wserie     = "un2"
          wtot-itens = 0
          wqt-registro = 0
          wtot-valores = 0
          witem        = ""
          wseq-it      = 0 
          wtam-item    = 0.
DISPLAY wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog 
        WITH FRAME    {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TESTE C-Win 
PROCEDURE TESTE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ver-consis C-Win 
PROCEDURE ver-consis :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN gera-tela.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

