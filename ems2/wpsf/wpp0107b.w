&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wdelped    AS INTEGER FORMAT ">>>>>,>>9" NO-UNDO.
DEFINE VARIABLE wconf-del  AS LOGICAL FORMAT "Sim/Nao" NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-2

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES lo-pedferra

/* Definitions for BROWSE BROWSE-2                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-2 lo-pedferra.b-pedido ~
lo-pedferra.b-emitente lo-pedferra.b-contato lo-pedferra.b-extenso ~
lo-pedferra.b-imposto lo-pedferra.b-pag1 lo-pedferra.b-pag2 ~
lo-pedferra.b-pag3 lo-pedferra.b-pag4 lo-pedferra.b-pag5 lo-pedferra.b-pag6 ~
lo-pedferra.b-pag7 lo-pedferra.b-pag8 lo-pedferra.b-pag9 ~
lo-pedferra.b-pag10 lo-pedferra.b-pag11 lo-pedferra.b-pag12 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-2 
&Scoped-define OPEN-QUERY-BROWSE-2 OPEN QUERY BROWSE-2 FOR EACH lo-pedferra NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-2 lo-pedferra
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-2 lo-pedferra


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-2}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BROWSE-2 bt-exc bt-sair-2 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-exc 
     IMAGE-UP FILE "image/gr-eli.bmp":U
     LABEL "Button 1" 
     SIZE 4 BY 1.13 TOOLTIP "Exclui Detalhe pedidos".

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 4 BY 1.13 TOOLTIP "Sair"
     BGCOLOR 8 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-2 FOR 
      lo-pedferra SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-2 C-Win _STRUCTURED
  QUERY BROWSE-2 NO-LOCK DISPLAY
      lo-pedferra.b-pedido FORMAT ">>>>>,>>9":U
      lo-pedferra.b-emitente FORMAT ">>>>>>>>9":U
      lo-pedferra.b-contato FORMAT "X(20)":U WIDTH 13.29
      lo-pedferra.b-extenso FORMAT "X(50)":U WIDTH 25.43
      lo-pedferra.b-imposto FORMAT "99":U
      lo-pedferra.b-pag1 FORMAT "X(60)":U
      lo-pedferra.b-pag2 FORMAT "X(60)":U
      lo-pedferra.b-pag3 FORMAT "X(60)":U WIDTH 28.57
      lo-pedferra.b-pag4 FORMAT "X(60)":U
      lo-pedferra.b-pag5 FORMAT "X(60)":U WIDTH 30
      lo-pedferra.b-pag6 FORMAT "X(60)":U WIDTH 40.14
      lo-pedferra.b-pag7 FORMAT "X(60)":U
      lo-pedferra.b-pag8 FORMAT "X(60)":U
      lo-pedferra.b-pag9 FORMAT "X(60)":U
      lo-pedferra.b-pag10 FORMAT "X(60)":U
      lo-pedferra.b-pag11 FORMAT "X(60)":U
      lo-pedferra.b-pag12 FORMAT "X(60)":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 71 BY 9.75
         FONT 2 ROW-HEIGHT-CHARS .54 EXPANDABLE.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     BROWSE-2 AT ROW 1.5 COL 4
     bt-exc AT ROW 5.5 COL 76
     bt-sair-2 AT ROW 7 COL 76
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81.29 BY 12.08
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Detalhes dos Pedidos de Ferramental"
         HEIGHT             = 12.08
         WIDTH              = 81.29
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* BROWSE-TAB BROWSE-2 1 DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-2
/* Query rebuild information for BROWSE BROWSE-2
     _TblList          = "mgfas.lo-pedferra"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _FldNameList[1]   = mgfas.lo-pedferra.b-pedido
     _FldNameList[2]   = mgfas.lo-pedferra.b-emitente
     _FldNameList[3]   > mgfas.lo-pedferra.b-contato
"b-contato" ? ? "character" ? ? ? ? ? ? no ? no no "13.29" yes no no "U" "" ""
     _FldNameList[4]   > mgfas.lo-pedferra.b-extenso
"b-extenso" ? ? "character" ? ? ? ? ? ? no ? no no "25.43" yes no no "U" "" ""
     _FldNameList[5]   = mgfas.lo-pedferra.b-imposto
     _FldNameList[6]   = mgfas.lo-pedferra.b-pag1
     _FldNameList[7]   = mgfas.lo-pedferra.b-pag2
     _FldNameList[8]   > mgfas.lo-pedferra.b-pag3
"b-pag3" ? ? "character" ? ? ? ? ? ? no ? no no "28.57" yes no no "U" "" ""
     _FldNameList[9]   = mgfas.lo-pedferra.b-pag4
     _FldNameList[10]   > mgfas.lo-pedferra.b-pag5
"b-pag5" ? ? "character" ? ? ? ? ? ? no ? no no "30" yes no no "U" "" ""
     _FldNameList[11]   > mgfas.lo-pedferra.b-pag6
"b-pag6" ? ? "character" ? ? ? ? ? ? no ? no no "40.14" yes no no "U" "" ""
     _FldNameList[12]   = mgfas.lo-pedferra.b-pag7
     _FldNameList[13]   = mgfas.lo-pedferra.b-pag8
     _FldNameList[14]   = mgfas.lo-pedferra.b-pag9
     _FldNameList[15]   = mgfas.lo-pedferra.b-pag10
     _FldNameList[16]   = mgfas.lo-pedferra.b-pag11
     _FldNameList[17]   = mgfas.lo-pedferra.b-pag12
     _Query            is OPENED
*/  /* BROWSE BROWSE-2 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Detalhes dos Pedidos de Ferramental */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Detalhes dos Pedidos de Ferramental */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-exc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-exc C-Win
ON CHOOSE OF bt-exc IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  ASSIGN wdelped = lo-pedferra.b-pedido.
  FOR EACH mgfas.lo-pedferra WHERE lo-pedferra.b-pedido = wdelped.
    MESSAGE "DESEJA ELIMINAR DETALHES DO PEDIDO FERRAMENTAL: "
             lo-pedferra.b-pedido "?" VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wconf-del.
    IF wconf-del THEN DO:
      DELETE mgfas.lo-pedferra.
      END.
    END.
      FIND FIRST mgfas.lo-pedferra NO-LOCK NO-ERROR.
      REPOSITION BROWSE-2 TO ROWID ROWID(mgfas.lo-pedferra).
      
      END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-2
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE BROWSE-2 bt-exc bt-sair-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

