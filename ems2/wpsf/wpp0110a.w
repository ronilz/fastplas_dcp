&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE welim AS LOGICAL FORMAT "Sim/N�o" INITIAL NO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS IMAGE-1 IMAGE-2 RECT-20 RECT-21 wep westab ~
wnr-pl wcod-emitente-ini wcod-emitente-fim bt-consulta BUTTON-5 BtnOK 
&Scoped-Define DISPLAYED-OBJECTS wep westab wnr-pl wcod-emitente-ini ~
wcod-emitente-fim 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-consulta 
     LABEL "Consulta  Plano" 
     SIZE 13 BY 1.

DEFINE BUTTON BtnOK AUTO-GO DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "OK" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-5 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "bt-conf-elim" 
     SIZE 5 BY 1 TOOLTIP "Confirma elimina��o de plano".

DEFINE VARIABLE wcod-emitente-fim AS INTEGER FORMAT ">>>>>9" INITIAL 999999 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .88 NO-UNDO.

DEFINE VARIABLE wcod-emitente-ini AS INTEGER FORMAT ">>>>>9" INITIAL 1 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .88 NO-UNDO.

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 1 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 4 BY .88 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(3)":U INITIAL "1" 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 4 BY .88 NO-UNDO.

DEFINE VARIABLE wnr-pl AS CHARACTER FORMAT "999999x":U INITIAL "0" 
     LABEL "N�mero do Plano" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 TOOLTIP "M�s (2 d�gitos) e Ano (4 d�gitos)" NO-UNDO.

DEFINE IMAGE IMAGE-1
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-2
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 78 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-20
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 7.75.

DEFINE RECTANGLE RECT-21
     EDGE-PIXELS 3 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     wep AT ROW 4 COL 31 COLON-ALIGNED
     westab AT ROW 5 COL 31 COLON-ALIGNED
     wnr-pl AT ROW 6 COL 31 COLON-ALIGNED
     wcod-emitente-ini AT ROW 7 COL 31 COLON-ALIGNED
     wcod-emitente-fim AT ROW 7 COL 47 COLON-ALIGNED NO-LABEL
     bt-consulta AT ROW 10 COL 3
     BUTTON-5 AT ROW 10 COL 69
     BtnOK AT ROW 10 COL 74 HELP
          "999999"
     "Elimina��o de plano" VIEW-AS TEXT
          SIZE 15 BY .54 AT ROW 1.75 COL 5
     RECT-2 AT ROW 9.75 COL 2
     IMAGE-1 AT ROW 7 COL 42
     IMAGE-2 AT ROW 7 COL 46
     RECT-20 AT ROW 2 COL 2
     RECT-21 AT ROW 11.25 COL 2
     SPACE(1.56) SKIP(0.41)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Elimina��o de plano - wpp0110a".


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
ASSIGN 
       RECT-21:SELECTABLE IN FRAME Dialog-Frame       = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Elimina��o de plano - wpp0110a */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-consulta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-consulta Dialog-Frame
ON CHOOSE OF bt-consulta IN FRAME Dialog-Frame /* Consulta  Plano */
DO:
  RUN wpsf/wpp0111.r.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-5 Dialog-Frame
ON CHOOSE OF BUTTON-5 IN FRAME Dialog-Frame /* bt-conf-elim */
DO:
  ASSIGN wep westab wnr-pl wcod-emitente-ini wcod-emitente-fim.
  DISABLE ALL WITH FRAME    {&FRAME-NAME}.
  FIND FIRST lo-matplano WHERE lo-matplano.emp   = wep
                         AND   lo-matplano.estab = westab
                         AND   lo-matplano.nr-pl = wnr-pl 
                         AND   (lo-matplano.cod-emitente GE wcod-emitente-ini
                         AND   lo-matplano.cod-emitente LE wcod-emitente-fim) NO-LOCK NO-ERROR.

  IF NOT AVAILABLE lo-matplano THEN DO:
      MESSAGE "N�O EXISTE A FAIXA DE PLANO SELECIONADA"
              VIEW-AS ALERT-BOX.
      RUN inicia.
      END.

  IF AVAILABLE lo-matplano THEN DO:
    MESSAGE "Confirma a elimina��o do Plano:"  lo-matplano.nr-pl  SKIP  
            "Emp./Estab.:" lo-matplano.emp lo-matplano.estab skip
            "Cliente: " wcod-emitente-ini "a"  wcod-emitente-fim
             VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE welim.
    IF welim = YES THEN DO:
      FOR EACH lo-matplano WHERE emp = wep
                         AND   estab = westab
                         AND   nr-pl = wnr-pl 
                         AND   (lo-matplano.cod-emitente GE wcod-emitente-ini
                         AND   lo-matplano.cod-emitente LE wcod-emitente-fim):
        
        DELETE lo-matplano.
       END.
        MESSAGE "ELIMINA��O CONCLU�DA" VIEW-AS ALERT-BOX.
      END.
    IF welim = NO THEN MESSAGE "CANCELADA A ELIMINA��O" VIEW-AS ALERT-BOX.
    END.
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wep westab wnr-pl wcod-emitente-ini wcod-emitente-fim 
      WITH FRAME Dialog-Frame.
  ENABLE IMAGE-1 IMAGE-2 RECT-20 RECT-21 wep westab wnr-pl wcod-emitente-ini 
         wcod-emitente-fim bt-consulta BUTTON-5 BtnOK 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia Dialog-Frame 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-- ----------------------------------------------------------------------------*/
APPLY "ENTRY" TO wep IN FRAME {&FRAME-NAME}.
ASSIGN welim = NO
       wep = 1
       westab = "1"
       wnr-pl = "0"
       wcod-emitente-ini =  0
       wcod-emitente-fim = 999999.
DISPLAY wep westab wnr-pl wcod-emitente-ini wcod-emitente-fim WITH FRAME   {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

