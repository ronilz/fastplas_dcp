&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF VAR went-per LIKE movto-estoq.quantidade EXTENT 31.
DEF VAR wsai-per LIKE movto-estoq.quantidade EXTENT 31.

DEF VAR went-ini LIKE movto-estoq.quantidade EXTENT 31.
DEF VAR wsai-ini LIKE movto-estoq.quantidade EXTENT 31.

DEF VAR wqt-atu LIKE movto-estoq.quantidade.


DEF VAR wsaldo-ini LIKE saldo-estoq.qtidade-atu EXTENT 31.
DEF VAR wcont      AS INTEGER FORMAT 99.

DEFINE BUFFER wmov FOR movto-estoq.

    /* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.


DEF VAR wcont-mes AS INTEGER FORMAT "99".
DEF VAR wrel      AS CHAR FORMAT "X(40)".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 RECT-4 RECT-6 wit-ini ~
wit-fim wmes-ini wmes-fim wano wdep-ini wdep-fim w-imp bt-executar ~
bt-sair-2 
&Scoped-Define DISPLAYED-OBJECTS wit-ini wit-fim wmes-ini wmes-fim wano ~
wdep-ini wdep-fim w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 8 BY 1
     FONT 1.

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE wano AS INTEGER FORMAT "9999":U INITIAL 0 
     LABEL "Ano" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .79 NO-UNDO.

DEFINE VARIABLE wdep-fim AS CHARACTER FORMAT "X(3)":U 
     LABEL "Dep�sito inicial" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .79 NO-UNDO.

DEFINE VARIABLE wdep-ini AS CHARACTER FORMAT "X(3)":U 
     LABEL "Dep�sito inicial" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .79 NO-UNDO.

DEFINE VARIABLE wit-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item Final" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .79 NO-UNDO.

DEFINE VARIABLE wit-ini AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item inicial" 
     VIEW-AS FILL-IN 
     SIZE 20 BY .79 NO-UNDO.

DEFINE VARIABLE wmes-fim AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "M�s Final" 
     VIEW-AS FILL-IN 
     SIZE 4 BY .79 NO-UNDO.

DEFINE VARIABLE wmes-ini AS INTEGER FORMAT "99":U INITIAL 0 
     LABEL "M�s inicial" 
     VIEW-AS FILL-IN 
     SIZE 4 BY .79 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 2,
"Impressora", 3
     SIZE 25 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 8.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 71 BY 1
     BGCOLOR 7 FGCOLOR 7 .

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 71 BY .5
     BGCOLOR 7 FGCOLOR 7 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 71 BY .5
     BGCOLOR 7 FGCOLOR 7 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 1.75.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wit-ini AT ROW 2.75 COL 33 COLON-ALIGNED
     wit-fim AT ROW 3.5 COL 33 COLON-ALIGNED
     wmes-ini AT ROW 5 COL 33 COLON-ALIGNED
     wmes-fim AT ROW 5.75 COL 33 COLON-ALIGNED
     wano AT ROW 6.5 COL 33 COLON-ALIGNED
     wdep-ini AT ROW 8 COL 33 COLON-ALIGNED
     wdep-fim AT ROW 8.75 COL 33 COLON-ALIGNED
     w-imp AT ROW 11.25 COL 40 RIGHT-ALIGNED NO-LABEL
     bt-executar AT ROW 11.25 COL 40
     bt-sair-2 AT ROW 11.25 COL 52
     "Par�metros" VIEW-AS TEXT
          SIZE 12 BY .54 AT ROW 1.75 COL 33
          BGCOLOR 7 FGCOLOR 15 FONT 0
     RECT-1 AT ROW 2.5 COL 4
     RECT-2 AT ROW 1.5 COL 4
     RECT-3 AT ROW 10.5 COL 4
     RECT-4 AT ROW 12.75 COL 4
     RECT-6 AT ROW 11 COL 4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 76.86 BY 13.04
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Totais de transa��es no estoque por Item - wpes003"
         HEIGHT             = 13.04
         WIDTH              = 76.86
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Totais de transa��es no estoque por Item - wpes003 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Totais de transa��es no estoque por Item - wpes003 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
    ASSIGN w-imp.
     DISABLE ALL WITH FRAME  {&FRAME-NAME}.
     ASSIGN wit-ini wit-fim wmes-ini wmes-fim wdep-ini wdep-fim wano.


     CASE w-imp: /*
       WHEN 1 THEN DO:
         ASSIGN wmes-ini wmes-fim.
         DO wcont-mes = wmes-ini TO wmes-fim :
           ASSIGN wit-ini wit-fim wmes-ini wmes-fim wdep-ini wdep-fim wano.
           wrel = "V:\spool\TRANSACOES" + SUBSTRING(STRING(TIME),1,3) + STRING(wcont-mes)  + "-" + STRING(wano) + ".txt".
           OUTPUT TO VALUE(wrel) PAGED.
           RUN relatorio.
           OUTPUT CLOSE.
           RUN gera-tela.
           END.
        END.   */

       WHEN 2 THEN DO:
       ASSIGN wmes-ini wmes-fim.
       DO wcont-mes = wmes-ini TO wmes-fim :
         ASSIGN wit-ini wit-fim wmes-ini wmes-fim wdep-ini wdep-fim wano.
         wrel = "V:\spool\TRANS" + SUBSTRING(STRING(TIME),1,3) + "-" + STRING(wcont-mes)  + "-" + STRING(wano) + ".txt".
         OUTPUT TO VALUE(wrel) PAGED.
         RUN relatorio.
         OUTPUT CLOSE.
         END.
       END. 

        WHEN 3 THEN DO:
        /* impressora */
           ASSIGN wmes-ini wmes-fim.
           DO wcont-mes = wmes-ini TO wmes-fim :
             SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
             IF wconf-imp = YES THEN DO:
                OUTPUT TO PRINTER PAGE-SIZE 44.
               {wpsf/imp.p}
               PUT CONTROL wcomprime wpaisagem a4. 
               RUN relatorio.
               OUTPUT CLOSE.
               END.
             END.
           END.
         END CASE. 
     ENABLE ALL WITH FRAME  {&FRAME-NAME}.
     IF w-imp <> 1 THEN MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
   END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wit-ini wit-fim wmes-ini wmes-fim wano wdep-ini wdep-fim w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-3 RECT-4 RECT-6 wit-ini wit-fim wmes-ini wmes-fim 
         wano wdep-ini wdep-fim w-imp bt-executar bt-sair-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO wcont-mes = wmes-ini TO wmes-fim:
  ASSIGN warquivo = wrel.

  get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
  if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
  end.
    
  run winexec (input c-key-value + chr(32) + warquivo, input 1).
END.
  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wit-ini = ""
       wit-fim = "ZZZZZZZZZZZZZZZZ"
       wmes-ini = 01
       wmes-fim = month(today)
       wano    = YEAR(TODAY)
       wdep-ini = ""
       wdep-fim = "ZZZ".
       wrel = "V:\spool\TRANSACOES-IT.TXT".

       
DISPLAY wit-ini wit-fim wmes-ini wmes-fim wdep-ini wdep-fim wano
        WITH FRAME          {&FRAME-NAME}.
ENABLE ALL WITH FRAME      {&FRAME-NAME}.
APPLY "enter" TO wit-ini.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio C-Win 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FORM HEADER
        "SEEBER FASTPLAS LTDA."  AT 10
        TODAY FORMAT "99/99/9999" STRING(TIME,"HH:MM:SS") "HS"
        "Pagina:" AT 140 PAGE-NUMBER FORMAT "999"
        "*** RELATORIO DE TOTAIS DE TRANSACOES POR ITEM ***" AT 58
        SKIP(1)
        "*** Mes: " AT 55 wcont-mes "/" wano 
        "Deposito:" wdep-ini "a" wdep-fim "***" 
        SKIP(1)
        "Dia           Item               Descricao               Saldo Inicial               Entrada                  Saida    Saldo Final"  AT 10
         WITH FRAME f-lista DOWN NO-LABELS WIDTH 155.
    

    FOR EACH ITEM WHERE it-codigo GE wit-ini
                  AND   it-codigo LE wit-fim
                  NO-LOCK BREAK BY it-codigo:
    ASSIGN wqt-atu = 0.

    FOR EACH saldo-estoq WHERE saldo-estoq.it-codigo = ITEM.it-codigo
                         NO-LOCK:
       ASSIGN wqt-atu = wqt-atu + saldo-estoq.qtidade-atu.
      END.

     IF FIRST-OF(item.it-codigo) THEN DO:
      DO wcont = 1 TO 31:
         ASSIGN went-per[wcont]      = 0 
              wsai-per[wcont]       = 0
              wsaldo-ini[wcont]     = 0
              went-ini[wcont]       = 0
              wsai-ini[wcont]       = 0.
             END.
      PAGE.
    END.
    DO wcont = 1 TO 31:
        ASSIGN went-per[wcont]      = 0 
              wsai-per[wcont]       = 0
              wsaldo-ini[wcont]     = 0
              went-ini[wcont]       = 0
              wsai-ini[wcont]       = 0.
  
        
        /* CALCULA SALDO INICIAL */
        FOR EACH wmov  WHERE wmov.it-codigo      = ITEM.it-codigo
                     AND   wmov.dt-trans         GE DATE(wcont-mes,wcont,wano)
                     AND   (wmov.cod-depos       GE wdep-ini
                     AND   wmov.cod-depos        LE wdep-fim)
                     NO-LOCK:

        IF wmov.tipo-trans = 1 THEN ASSIGN went-ini[wcont] = went-ini[wcont] + wmov.quantidade.
        IF wmov.tipo-trans = 2 THEN ASSIGN wsai-ini[wcont] = wsai-ini[wcont] + wmov.quantidade.
        ASSIGN wsaldo-ini[wcont]  =  (wqt-atu - went-ini[wcont])  + wsai-ini[wcont].
       
        END.
    /* termino saldo inicial */     
      FOR EACH movto-estoq WHERE movto-estoq.it-codigo = item.it-codigo
                         AND   movto-estoq.dt-trans  = DATE(wcont-mes,wcont,wano)
                         AND  (movto-estoq.cod-depos GE wdep-ini
                         AND   movto-estoq.cod-depos LE wdep-fim)  
                         NO-LOCK BREAK BY movto-estoq.it-codigo
                         BY movto-estoq.dt-trans.
       IF movto-estoq.tipo-trans = 1 THEN ASSIGN went-per[wcont]  = went-per[wcont] + movto-estoq.quantidade.
       IF movto-estoq.tipo-trans = 2 THEN ASSIGN  wsai-per[wcont] = wsai-per[wcont] + movto-estoq.quantidade.
      END.

    IF    went-per[wcont]   = 0                    
      AND wsai-per[wcont]   = 0 THEN NEXT.
    DISPLAY 
            wcont AT 10
            wcont-mes
            wano
            item.it-codigo 
            descricao-1 
            wsaldo-ini[wcont]
            went-per[wcont]                     
            wsai-per[wcont]
           (wsaldo-ini[wcont] + went-per[wcont]) - wsai-per[wcont]
           WITH FRAME f-lista WIDTH 155 DOWN.
           DOWN WITH FRAME f-lista.
           END.
    END.
    
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio1 C-Win 
PROCEDURE relatorio1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE roda C-Win 
PROCEDURE roda :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE saldo-inicial C-Win 
PROCEDURE saldo-inicial :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /*FOR EACH saldo-estoq WHERE  saldo-estoq.it-codigo = movto-estoq.it-codigo
                         AND  saldo-estoq.cod-depos = "EXP"
                         NO-LOCK:
    ASSIGN wsaldo-ini    = 0
           went-ini      = 0
           wsai-ini      = 0 .
      FOR EACH wmov  WHERE wmov.it-codigo = movto-estoq.it-codigo
                            AND  wmov.dt-trans LT wdt-ini /*
                            AND  cod-depos = "EXP"         */
                             NO-LOCK:
      
      /* DISPLAY dt-trans quantidade.*/
    IF wmov.tipo-trans = 1 THEN ASSIGN went-ini = went-ini + wmov.quantidade.
    IF wmov.tipo-trans = 2 THEN ASSIGN wsai-ini = wsai-ini + wmov.quantidade.
   END. 
   ASSIGN wsaldo-ini  =  went-ini - wsai-ini.
  END.
  DISPLAY wsaldo-ini.
   */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vazio C-Win 
PROCEDURE vazio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE vazio1 C-Win 
PROCEDURE vazio1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

