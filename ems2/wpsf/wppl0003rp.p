&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i WPPL0003RP 0.00.04.002}

/* ***************************  Definitions  ************************** */
&global-define programa nome-do-programa

def var c-liter-par                  as character format "x(13)":U.
def var c-liter-sel                  as character format "x(10)":U.
def var c-liter-imp                  as character format "x(12)":U.    
def var c-destino                    as character format "x(15)":U.


define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD wep              AS INTEGER FORMAT ">>9"
    FIELD westab           AS CHAR FORMAT "x(03)"
    FIELD wemi-ini         AS INTEGER FORMAT ">>>>>>9"
    FIELD wdata-corte      AS DATE FORMAT "99/99/9999"
    FIELD wnr-pl           AS CHAR FORMAT "999999X".

/* FASTPLAS - VALERIA - 19/05/06 */
DEFINE WORKFILE saldo
  FIELD fcod-emitente           LIKE lo-matplano.cod-emitente INITIAL 0
  FIELD femp                    LIKE lo-matplano.emp          INITIAL 0
  FIELD festab                  LIKE lo-matplano.estab        INITIAL ""
  FIELD fnr-pl                  AS CHAR FORMAT "999999X"      INITIAL " "
  FIELD fnro-docto              LIKE lo-matplano.nro-docto    INITIAL 0
  FIELD fit-codigo              LIKE lo-matplano.it-codigo    INITIAL " "
  FIELD fimediato               AS INTE FORMAT "->,>>>,>>9"   INITIAL 0   
  FIELD ftotent                 AS INTE FORMAT ">>>,>>>,>>9"  INITIAL 0
  FIELD fqt-sem1                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem2                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem3                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem4                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem5                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    

  FIELD fqt-sem1-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem2-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem3-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem4-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem5-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fmes1                   AS INTE FORMAT ">,>>>,>>9"    INITIAL 0
  FIELD fmes2                   AS INTE FORMAT ">,>>>,>>9"    INITIAL 0.
/* Totais */
DEFINE VARIABLE wtotent         AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wresul          AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wtotplano       AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wtottela        AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wproxtela       AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wit-ini         LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wit-fim         LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wdatapesq       LIKE recebimento.data-nota    INITIAL ?.
DEFINE VARIABLE wemi            LIKE lo-matplano.cod-emitente INITIAL 0.
DEFINE VARIABLE wit-codigo      LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wit-ant         LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wlinha          AS CHAR FORMAT "X(143)".
DEFINE VARIABLE wlinha1         AS CHAR FORMAT "X(143)".
DEFINE VARIABLE wpag            AS INTE FORMAT "9999".
DEFINE VARIABLE wemp            LIKE mgadm.estabel.nome.
DEFINE VARIABLE wnome-ab        LIKE emitente.nome-ab.

DEFINE VARIABLE wconf-imp      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE warq           AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wtipo          AS INTEGER NO-UNDO. /* tipo de impressora */
DEFINE VARIABLE wsaldo-estoq   AS DEC  FORMAT ">>>>,>>9.9999-"  NO-UNDO.
DEFINE VARIABLE wsaldo-cq      AS DEC  FORMAT ">>>>,>>9.9999-"  NO-UNDO.
DEFINE VARIABLE wsaldo-img     AS DEC  FORMAT ">>>>,>>9.9999-"  NO-UNDO.
/* DEFINICOES FASTPLAS */

define temp-table tt-digita
    field ordem            as integer   format ">>>>9":U
    field exemplo          as character format "x(30)":U
    index id is primary unique
        ordem.

def temp-table tt-raw-digita
    field raw-digita as raw.
 
def input parameter raw-param as raw no-undo.
def input parameter table for tt-raw-digita.

def var h-acomp         as handle no-undo.    

form
/*form-selecao-ini*/
    skip(1)
    c-liter-sel         no-label
    skip(1)
    /*form-selecao-usuario*/
    skip(1)
/*form-selecao-fim*/
/*form-parametro-ini*/
    skip(1)
    c-liter-par         no-label
    skip(1)
    /*form-parametro-usuario*/
    skip(1)
/*form-parametro-fim*/
/*form-impressao-ini*/
    skip(1)
    c-liter-imp         no-label
    skip(1)
    c-destino           colon 40 "-"
    tt-param.arquivo    no-label
    tt-param.usuario    colon 40
    skip(1)
/*form-impressao-fim*/
    with stream-io side-labels no-attr-space no-box width 132 frame f-impressao.



create tt-param.
raw-transfer raw-param to tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.

/*inicio-traducao*/
/*traducao-default*/
{utp/ut-liter.i PAR�METROS * r}
assign c-liter-par = return-value.
{utp/ut-liter.i SELE��O * r}
assign c-liter-sel = return-value.
{utp/ut-liter.i IMPRESS�O * r}
assign c-liter-imp = return-value.
{utp/ut-liter.i Destino * l}
assign c-destino:label in frame f-impressao = return-value.
{utp/ut-liter.i Usu�rio * l}
assign tt-param.usuario:label in frame f-impressao = return-value.   
/*fim-traducao*/

{include/i-rpvar.i}

/*find empresa
    where empresa.ep-codigo = v_cdn_empres_usuar
    no-lock no-error.*/
find first param-global no-lock no-error.

{utp/ut-liter.i titulo_sistema * }
assign c-sistema = "EMS 2.04". /*Return-value.*/
{utp/ut-liter.i titulo_relatorio * }
assign c-titulo-relat = "SALDO MENSAL FASTPLAS - SOROCABA" /*return-value */.
assign c-empresa     = param-global.grupo
       c-programa    = "{&programa}":U
       c-versao      = "1.00":U
       c-revisao     = "000"
       c-destino     = {varinc/var00002.i 04 tt-param.destino}.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 1.99
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Procedure 
/* ************************* Included-Libraries *********************** */

{include/i-rpcab.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

do on stop undo, leave:
    {include/i-rpout.i}
    view frame f-cabec.
    view frame f-rodape.    
    run utp/ut-acomp.p persistent set h-acomp.  
    
    {utp/ut-liter.i aaaaaaaaaaaaaaaaaa bbb c}
    
    run pi-inicializar in h-acomp ("Gerando ..."). 
    
    /*:T --- Colocar aqui o c�digo de impress�o --- */
    /* fastplas - valeria - 19/05/06 */
    RUN imprimir.
    /*FOR EACH lo-matplano WHERE nr-pl BEGINS "012006" NO-LOCK:
        DISPLAY lo-matplano.
    END.*/

     /*   run pi-acompanhar in h-acomp (input lo-matplano.nr-pl).*/



    run pi-finalizar in h-acomp.
    {include/i-rpclo.i}

/* PROCEDURES IMPRIMIR */

PROCEDURE imprimir.
    
/* Form do Saldo Relatorio --------------------------------------------------*/
form
  "Cod.Item          UltProg     NfCorte   TOT.ENTRADA        ATRASO  "
  lo-matdatas.titsem1  AT 78
  lo-matdatas.titsem2  AT 92
  lo-matdatas.titsem3  AT 106     
  lo-matdatas.titsem4  AT 120
  lo-matdatas.titsem5  AT 134
  SKIP
  /*"__________________ ________ _______ ___________ __________" */
  "_________" AT 78
  "_________" AT 92
  "_________" AT 106
  "_________" AT 120
  "_________" AT 134 

  SKIP
  saldo.fit-codigo " "
  saldo.fnr-pl  " "
  saldo.fnro-docto  
  saldo.ftotent
  saldo.fimediato TO 65
  saldo.fqt-sem1  AT 78
  saldo.fqt-sem2  AT 92
  saldo.fqt-sem3  AT 106
  saldo.fqt-sem4  AT 120
  saldo.fqt-sem5  AT 134
  SKIP
  item.descricao-1
  lo-matdatas.titsem1-2 AT 78
  lo-matdatas.titsem2-2 AT 92
  lo-matdatas.titsem3-2 AT 106
  lo-matdatas.titsem4-2 AT 120      
  lo-matdatas.titsem5-2 AT 134
  SKIP
 "Saldo em estoque no ALM:" wsaldo-estoq TO 40
  "_________"   AT 78
  "__________"  AT 92
  "_________"   AT 106
  "_________"   AT 120
  "_________"   AT 134
  
  SKIP
 "Saldo em estoque no  CQ:" wsaldo-cq TO 40
  saldo.fqt-sem1-2 AT 78
  saldo.fqt-sem2-2 AT 92
  saldo.fqt-sem3-2 AT 106
  saldo.fqt-sem4-2 AT 120
  saldo.fqt-sem5-2 AT 134
  SKIP
  "Saldo em estoque no IMG:" wsaldo-img TO 40
  lo-matdatas.titmes1   AT 78
  lo-matdatas.titmes2   AT 92  
  SKIP
  "_________"   AT 78
  "_________"   AT 92
  SKIP
  saldo.fmes1   AT 78
  saldo.fmes2   AT 92 
  wlinha 
  SKIP(1)
  wlinha1
  HEADER
    "*** SALDO DO PLANO DE ENTREGA DE MATERIAIS NO."  AT 37
  tt-param.wnr-pl FORMAT "99/9999X" "***"
  SKIP
  "Empresa:" AT 32 tt-param.wep "Estabelecimento:" tt-param.westab 
  "Fornecedor:"  emitente.cod-emitente wnome-ab
with no-box width 180 down stream-io frame f-relat.

/* for each do relatorio */
FOR EACH lo-matplano 
       WHERE lo-matplano.cod-emitente                 = tt-param.wemi-ini
                       AND   lo-matplano.emp          =  tt-param.wep
                       AND   lo-matplano.estab        =  tt-param.westab
                       AND   lo-matplano.nr-pl        BEGINS  tt-param.wnr-pl
                       NO-LOCK BREAK BY lo-matplano.cod-emitente
                       BY lo-matplano.it-codigo 
                       BY lo-matplano.nr-pl:
    run pi-acompanhar in h-acomp ("Item:" + STRING(lo-matplano.it-codigo)).
    /*PUT SCREEN ROW 22   "Fornecedor: " + STRING(lo-matplano.cod-emitente)
                      + " Item: " + STRING(lo-matplano.it-codigo). */
    ASSIGN wtotent   = 0
           wresul    = 0
           wdatapesq = ?.
           

    IF LAST-OF(lo-matplano.it-codigo) THEN DO:    
    /* Cria workfile saldo */
    CREATE saldo.
    ASSIGN saldo.fcod-emitente = lo-matplano.cod-emitente
           saldo.fnr-pl        = lo-matplano.nr-pl
           saldo.femp          = lo-matplano.emp
           saldo.festab        = lo-matplano.estab
           saldo.fit-codigo    = lo-matplano.it-codigo
           saldo.fnro-docto    = lo-matplano.nro-docto
           saldo.fimediato     = lo-matplano.imediato
           saldo.fqt-sem1      = lo-matplano.qt-sem1
           saldo.fqt-sem2      = lo-matplano.qt-sem2
           saldo.fqt-sem3      = lo-matplano.qt-sem3
           saldo.fqt-sem4      = lo-matplano.qt-sem4
           saldo.fqt-sem5      = lo-matplano.qt-sem5
           saldo.fqt-sem1-2    = lo-matplano.qt-sem1-2
           saldo.fqt-sem2-2    = lo-matplano.qt-sem2-2
           saldo.fqt-sem3-2    = lo-matplano.qt-sem3-2
           saldo.fqt-sem4-2    = lo-matplano.qt-sem4-2
           saldo.fqt-sem5-2    = lo-matplano.qt-sem5-2
           saldo.fmes1         = lo-matplano.mes1   
           saldo.fmes2         = lo-matplano.mes2.
        /*11/11/15 - Inserido esp-docto nft - conf. Fabio Gamarano */
        /* Procura / Soma Total de Entradas ------------------------------------*/
        FOR EACH movto-estoq WHERE movto-estoq.cod-emitente =  lo-matplano.cod-emitente
                             AND   movto-estoq.it-codigo    =  lo-matplano.it-codigo
                             AND   movto-estoq.cod-estabel  = lo-matplano.estab
/*                              AND   movto-estoq.dt-trans     GE wdata-corte */
                             AND   integer(movto-estoq.nro-docto) GT integer(saldo.fnro-docto) 
                             AND   movto-estoq.esp-docto    = 23 /* nft - conf. Fabio Gamarano pcp*/
                             AND   movto-estoq.tipo-trans   = 1
                             NO-LOCK:
/*             MESSAGE "MOVTO"  movto-estoq.nro-docto wtotent VIEW-AS ALERT-BOX. */
          ASSIGN wtotent = wtotent + movto-estoq.quantidade.
          ASSIGN saldo.ftotent = wtotent.                       
/*           MESSAGE movto-estoq.tipo-trans movto-estoq.quantidade wtotent saldo.ftotent  VIEW-AS ALERT-BOX.  */
          END.
    
    /*FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     lo-matplano.cod-emitente
                           AND   INTEGER(recebimento.numero-nota)
                           =     lo-matplano.nro-docto
                           AND   recebimento.it-codigo
                           =     lo-matplano.it-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               lo-matplano.cod-emitente
                         AND   recebimento.it-codigo    =
                               lo-matplano.it-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   INTEGER(recebimento.numero-nota)  GT
                               lo-matplano.nro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
       ASSIGN wtotent = wtotent + quant-receb.
       ASSIGN saldo.ftotent = wtotent.
       END. 
     */
     
     /***** MES 1 ABERTO *****/
     /* ATRASO */
     ASSIGN wresul = (lo-matplano.imediato)
                   - wtotent.
     IF wresul LE 0 THEN ASSIGN fimediato = 0.
     ELSE IF wresul < imediato THEN ASSIGN fimediato = wresul.

     /* Mes 1 aberto - 1a. semana */
     ASSIGN wresul = (lo-matplano.imediato + lo-matplano.qt-sem1) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem1 = 0.
     ELSE IF wresul < fqt-sem1 THEN ASSIGN fqt-sem1 = wresul.
   
    /* Mes 1 aberto - 2a. semana */
     ASSIGN wresul = (lo-matplano.imediato + lo-matplano.qt-sem1 +
                      lo-matplano.qt-sem2)  - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem2 = 0.
     ELSE IF wresul < fqt-sem2 THEN ASSIGN fqt-sem2 = wresul.
    
    /* Mes 1 aberto - 3a. semana */
     ASSIGN wresul = (lo-matplano.imediato 
                   + lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   + lo-matplano.qt-sem3) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem3 = 0.
     ELSE IF wresul < fqt-sem3 THEN ASSIGN fqt-sem3 = wresul.

     /* Mes 1 aberto - 4a. semana */
     ASSIGN wresul = (lo-matplano.imediato 
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem4 = 0.
     ELSE IF wresul < fqt-sem4 THEN ASSIGN fqt-sem4 = wresul.

     /* Mes 1 aberto - 5a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem5 = 0.
     ELSE IF wresul < fqt-sem5 THEN ASSIGN fqt-sem5 = wresul.

     /* Mes 2 aberto - 1a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem1-2 = 0.
     ELSE IF wresul < fqt-sem1-2 THEN ASSIGN fqt-sem1-2 = wresul.
    
     /* Mes 2 aberto - 2a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem2-2 = 0.
     ELSE IF wresul < fqt-sem2-2 THEN ASSIGN fqt-sem2-2 = wresul.
     
     /* Mes 2 aberto - 3a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem3-2 = 0.
     ELSE IF wresul < fqt-sem3-2 THEN ASSIGN fqt-sem3-2 = wresul.

     /* Mes 2 aberto - 4a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem4-2 = 0.
     ELSE IF wresul < fqt-sem4-2 THEN ASSIGN fqt-sem4-2 = wresul.

     /* Mes 2 aberto - 5a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem5-2 = 0.
     ELSE IF wresul < fqt-sem5-2 THEN ASSIGN fqt-sem5-2 = wresul.

     /* Mes 1 FECHADO */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2 
                   +  lo-matplano.mes1)
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fmes1 = 0.
     ELSE IF wresul < fmes1 THEN ASSIGN fmes1 = wresul.

     /* Mes 2 FECHADO */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2 
                   +  lo-matplano.mes1 + lo-matplano.mes2)
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fmes2 = 0.
     ELSE IF wresul < fmes2 THEN ASSIGN fmes2 = wresul.
     
     ASSIGN wtotplano = (lo-matplano.imediato 
               + lo-matplano.qt-sem1 + lo-matplano.qt-sem2 
               + lo-matplano.qt-sem3 + lo-matplano.qt-sem4
               + lo-matplano.qt-sem1-2 + lo-matplano.qt-sem2-2
               + lo-matplano.qt-sem3-2 + lo-matplano.qt-sem4-2
               + lo-matplano.qt-sem5-2 + lo-matplano.mes1
               + lo-matplano.mes2).
     
     IF wtotent > wtotplano THEN 
     ASSIGN fimediato = (wtotplano - wtotent).
   END.
 END.

/* DISPLAY DOS DADOS DO SALDO ----------------------------------------------*/
DO:
  HIDE MESSAGE NO-PAUSE.
  FIND FIRST mgadm.estabel WHERE estabel.ep-codigo = string(wep)
                     AND   estabel.cod-estabel = westab
                     NO-LOCK NO-ERROR.
  ASSIGN wemp     = estabel.nome.
    FOR EACH saldo WHERE saldo.fcod-emitente = wemi-ini
                   AND   saldo.femp          = wep
                   AND   saldo.festab        = westab
                   AND   saldo.fnr-pl        BEGINS  wnr-pl
                   NO-LOCK
                   BREAK BY saldo.fcod-emitente
                   BY saldo.fit-codigo:
      
      FIND FIRST emitente WHERE emitente.cod-emitente = saldo.fcod-emitente 
                          NO-LOCK NO-ERROR.
      ASSIGN wnome-ab     = emitente.nome-ab
             wsaldo-estoq = 0
             wsaldo-cq = 0
             wsaldo-img = 0.
      IF FIRST-OF(saldo.fcod-emitente) THEN DO:
        ASSIGN wemi = saldo.fcod-emitente.
        PAGE.
        END.
      FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl
                             =     SUBSTRING(saldo.fnr-pl,1,6)
                             AND   lo-matdatas.emp   = saldo.femp
                             AND   lo-matdatas.estab = saldo.festab
                             NO-LOCK.
      FIND FIRST item    WHERE item.it-codigo = saldo.fit-codigo 
                         NO-LOCK NO-ERROR. 
      FOR EACH saldo-estoq WHERE saldo-estoq.cod-depos = "ALM" 
                           AND   saldo-estoq.it-codigo = item.it-codigo
                           NO-LOCK:
        ASSIGN wsaldo-estoq = wsaldo-estoq 
                            + (saldo-estoq.qtidade-atu - saldo-estoq.qt-alocada).
        END.

            /* saldo no cq */
       FOR EACH saldo-estoq WHERE saldo-estoq.cod-depos = "CQ" 
                           AND   saldo-estoq.it-codigo = item.it-codigo
                           NO-LOCK:
        ASSIGN wsaldo-cq = wsaldo-cq
                            + (saldo-estoq.qtidade-atu - saldo-estoq.qt-alocada).
        END.
      /* saldo no img - sol. rosa - estabelecimento imigrantes - 13/11/2011*/
       FOR EACH saldo-estoq WHERE saldo-estoq.cod-depos = "IMG" 
                           AND   saldo-estoq.it-codigo = item.it-codigo
                           NO-LOCK:
        ASSIGN wsaldo-img = wsaldo-img
                            + (saldo-estoq.qtidade-atu - saldo-estoq.qt-alocada).
        END.
        DISPLAY /* linha 1 */
            wlinha1
            lo-matdatas.titsem1 lo-matdatas.titsem2 lo-matdatas.titsem3 
            lo-matdatas.titsem4 lo-matdatas.titsem5
            saldo.fit-codigo saldo.fnr-pl saldo.fnro-docto saldo.ftotent
            saldo.fimediato 
            saldo.fqt-sem1 saldo.fqt-sem2 saldo.fqt-sem3
            saldo.fqt-sem4 saldo.fqt-sem5 
            /* linha 2 */
            item.descricao-1 wsaldo-estoq wsaldo-cq wsaldo-img
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2
            lo-matdatas.titsem5-2 
            lo-matdatas.titmes1 lo-matdatas.titmes2
            saldo.fqt-sem1-2 saldo.fqt-sem2-2 saldo.fqt-sem3-2
            saldo.fqt-sem4-2 saldo.fqt-sem5-2 
            saldo.fmes1
            saldo.fmes2 
            wlinha   
        WITH FRAME f-relat OVERLAY NO-LABELS DOWN.
        DOWN WITH FRAME f-relat.
        END.
      END.
     END PROCEDURE.

/* FIM PROCEDURE IMPRIMIR */
/*
/* PROCEDURE DISP_NF */
PROCEDURE DISP_nf.
    CLEAR FRAME f-nf ALL NO-PAUSE.
  ASSIGN wtottela = 0.
    FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     saldo.fcod-emitente
                           AND   integer(recebimento.numero-nota)
                           =     saldo.fnro-docto
                           AND   recebimento.it-codigo
                           =     saldo.fit-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               saldo.fcod-emitente
                         AND   recebimento.it-codigo    =
                               saldo.fit-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   integer(recebimento.numero-nota) GT
                               saldo.fnro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
      
      ASSIGN wtottela = wtottela + recebimento.quant-receb.           
      HIDE MESSAGE NO-PAUSE.
      DISPLAY recebimento.it-codigo recebimento.numero-nota 
              recebimento.data-nota  recebimento.quant-receb
              COLUMN-LABEL "Quantidade" 
              wtottela LABEL "TOTAL ENTRADA"
              WITH FRAME f-nf CENTERED DOWN ROW 10 OVERLAY.
      DOWN WITH FRAME f-nf.
      END.
END PROCEDURE.
/* FIM PROCEDURE DISP_NF */
*/
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


