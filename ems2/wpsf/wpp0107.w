&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEFINE VARIABLE wped LIKE lo-pedferra.b-pedido.
/* Local Variable Definitions ---                                       */

DEFINE VARIABLE wconf     AS LOGICAL FORMAT "Sim/N�o" NO-UNDO.
DEFINE VARIABLE wconf-alt AS LOGICAL FORMAT "Sim/N�o" NO-UNDO.
DEFINE VARIABLE wrel      AS CHAR FORMAT "X(30)" NO-UNDO.
DEFINE VARIABLE wlinha    AS CHARACTER FORMAT "X(65)".
DEFINE VARIABLE wcont     AS INTEGER FORMAT "999".

DEF VAR c-key-value       AS char no-undo.
DEF VAR warquivo          AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wpedido bt-inc bt-codigo wcod-emitente ~
wfornecedor wendereco wcgc wcontato bt-confirma bt-cancela bt-arq bt-exc ~
bt-sair-2 wimposto waliq-icm wpreco waliq-ipi wextenso wpag-1 wpag-2 wpag-3 ~
wpag-4 wpag-5 wpag-6 wpag-7 wpag-8 wpag-9 wpag-10 wpag-11 wpag-12 RECT-1 ~
RECT-2 RECT-3 RECT-5 RECT-7 RECT-8 
&Scoped-Define DISPLAYED-OBJECTS wpedido wcod-emitente wfornecedor ~
wendereco wcgc wcontato wimposto waliq-icm wpreco waliq-ipi wextenso wpag-1 ~
wpag-2 wpag-3 wpag-4 wpag-5 wpag-6 wpag-7 wpag-8 wpag-9 wpag-10 wpag-11 ~
wpag-12 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-arq 
     IMAGE-UP FILE "image/im-pri.bmp":U
     LABEL "Button 1" 
     SIZE 4 BY 1.08 TOOLTIP "Gera pedido".

DEFINE BUTTON bt-cancela AUTO-END-KEY DEFAULT 
     IMAGE-UP FILE "image/im-cancel.bmp":U
     LABEL "Cancela" 
     SIZE 4 BY 1.08 TOOLTIP "Cancela Opera��o"
     BGCOLOR 8 .

DEFINE BUTTON bt-codigo 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Pedido" 
     SIZE 4 BY 1.04 TOOLTIP "V� para o pedido"
     FONT 1.

DEFINE BUTTON bt-confirma 
     IMAGE-UP FILE "image/im-ok.bmp":U
     LABEL "Confirma" 
     SIZE 4 BY 1.08 TOOLTIP "Confirma Opera��o".

DEFINE BUTTON bt-exc 
     IMAGE-UP FILE "image/im-joi.bmp":U
     LABEL "Button 1" 
     SIZE 4 BY 1.08 TOOLTIP "Consulta e exclus�o de detalhe pedidos".

DEFINE BUTTON bt-inc 
     IMAGE-UP FILE "image/im-new.bmp":U
     LABEL "Pedido Ferramental" 
     SIZE 4 BY 1.04 TOOLTIP "Abre pedido".

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 4 BY 1.08 TOOLTIP "Sair"
     BGCOLOR 8 .

DEFINE VARIABLE waliq-icm AS DECIMAL FORMAT ">>9,99":U INITIAL 0 
     LABEL "Aliquota ICM" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE waliq-ipi AS DECIMAL FORMAT ">>9,99":U INITIAL 0 
     LABEL "Aliquota IPI" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wcgc AS CHARACTER FORMAT "X(20)":U 
     LABEL "CNPJ/MF" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE wcod-emitente AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wcontato AS CHARACTER FORMAT "X(33)":U 
     LABEL "Representante do Fornecedor" 
     VIEW-AS FILL-IN 
     SIZE 29 BY .79
     FONT 1 NO-UNDO.

DEFINE VARIABLE wendereco AS CHARACTER FORMAT "X(40)":U 
     LABEL "Endere�o" 
     VIEW-AS FILL-IN 
     SIZE 32 BY .79 NO-UNDO.

DEFINE VARIABLE wextenso AS CHARACTER FORMAT "X(59)":U 
     LABEL "Valor por extenso" 
     VIEW-AS FILL-IN 
     SIZE 36 BY .79 NO-UNDO.

DEFINE VARIABLE wfornecedor AS CHARACTER FORMAT "X(38)":U 
     LABEL "Fornecedor" 
     VIEW-AS FILL-IN 
     SIZE 28 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-1 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [1]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .71 NO-UNDO.

DEFINE VARIABLE wpag-10 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [10]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-11 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [11]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-12 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [12]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-2 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [2]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-3 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [3]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-4 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [4]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-5 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [5]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-6 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [6]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-7 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [7]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-8 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [8]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpag-9 AS CHARACTER FORMAT "X(60)":U 
     LABEL "Linha [9]" 
     VIEW-AS FILL-IN 
     SIZE 54.29 BY .79 NO-UNDO.

DEFINE VARIABLE wpedido AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N�mero do Pedido" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY 1
     FONT 0 NO-UNDO.

DEFINE VARIABLE wpreco AS DECIMAL FORMAT ">>>,>>>,>>>,>>>,>>9.99":U INITIAL 0 
     LABEL "Pre�o Ferramental" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .79 NO-UNDO.

DEFINE VARIABLE wimposto AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Com Tributos", 1,
"Sem Tributos", 2
     SIZE 28 BY .75
     BGCOLOR 15  NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 91 BY 4.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 91 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 91 BY 9.5.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 91 BY 2.75.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 91 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 91 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wpedido AT ROW 2.25 COL 44 COLON-ALIGNED
     bt-inc AT ROW 2.25 COL 55
     bt-codigo AT ROW 2.25 COL 59
     wcod-emitente AT ROW 3 COL 12 COLON-ALIGNED
     wfornecedor AT ROW 3.75 COL 12 COLON-ALIGNED
     wendereco AT ROW 3.75 COL 57 COLON-ALIGNED
     wcgc AT ROW 4.5 COL 12 COLON-ALIGNED
     wcontato AT ROW 4.75 COL 57 COLON-ALIGNED
     bt-confirma AT ROW 5.75 COL 37
     bt-cancela AT ROW 5.75 COL 41
     bt-arq AT ROW 5.75 COL 45
     bt-exc AT ROW 5.75 COL 49
     bt-sair-2 AT ROW 5.75 COL 53
     wimposto AT ROW 7.75 COL 31 NO-LABEL
     waliq-icm AT ROW 8.5 COL 18 COLON-ALIGNED
     wpreco AT ROW 8.5 COL 46 COLON-ALIGNED
     waliq-ipi AT ROW 9.25 COL 18 COLON-ALIGNED
     wextenso AT ROW 9.25 COL 46 COLON-ALIGNED
     wpag-1 AT ROW 11.25 COL 20 COLON-ALIGNED
     wpag-2 AT ROW 12 COL 20 COLON-ALIGNED
     wpag-3 AT ROW 12.75 COL 20 COLON-ALIGNED
     wpag-4 AT ROW 13.5 COL 20 COLON-ALIGNED
     wpag-5 AT ROW 14.25 COL 20 COLON-ALIGNED
     wpag-6 AT ROW 15 COL 20 COLON-ALIGNED
     wpag-7 AT ROW 15.75 COL 20 COLON-ALIGNED
     wpag-8 AT ROW 16.5 COL 20 COLON-ALIGNED
     wpag-9 AT ROW 17.25 COL 20 COLON-ALIGNED
     wpag-10 AT ROW 18 COL 20 COLON-ALIGNED
     wpag-11 AT ROW 18.75 COL 20 COLON-ALIGNED
     wpag-12 AT ROW 19.5 COL 20 COLON-ALIGNED
     RECT-1 AT ROW 1.75 COL 2
     RECT-2 AT ROW 1 COL 2
     RECT-3 AT ROW 11 COL 2
     RECT-5 AT ROW 7.5 COL 2
     RECT-7 AT ROW 6.75 COL 2
     RECT-8 AT ROW 10.25 COL 2
     "Dados do Cliente ou Fornecedor" VIEW-AS TEXT
          SIZE 24 BY .54 AT ROW 1.25 COL 32
     "Condi��o de Pagamento" VIEW-AS TEXT
          SIZE 17 BY .54 AT ROW 10.5 COL 36
     " Tributos e Pre�o do Ferramental" VIEW-AS TEXT
          SIZE 23 BY .54 AT ROW 7 COL 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 92.86 BY 23.58
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Pedido de Compra e Venda de Ferramental - wpp0107"
         HEIGHT             = 20.08
         WIDTH              = 93.14
         MAX-HEIGHT         = 23.58
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 23.58
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Pedido de Compra e Venda de Ferramental - wpp0107 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Pedido de Compra e Venda de Ferramental - wpp0107 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-arq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arq C-Win
ON CHOOSE OF bt-arq IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  RUN wpsf/wpp0107a.r.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancela
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancela C-Win
ON CHOOSE OF bt-cancela IN FRAME DEFAULT-FRAME /* Cancela */
DO:
  RUN inicia.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-codigo C-Win
ON CHOOSE OF bt-codigo IN FRAME DEFAULT-FRAME /* Pedido */
DO:
ASSIGN wpedido.
DISABLE bt-codigo wpedido WITH FRAME {&FRAME-NAME}.

/* procura pedido NO EMS */
FIND FIRST pedido-compr WHERE num-pedido = wpedido NO-LOCK NO-ERROR.
IF NOT AVAILABLE pedido-compr THEN MESSAGE "PEDIDO: " wpedido "N�O ENCONTRADO" 
                              VIEW-AS ALERT-BOX.
IF AVAILABLE pedido-compr THEN DO:
    ENABLE bt-confirma bt-sair-2 bt-cancela wimposto
           wpedido wcontato wextenso wpag-1 wpag-2 wpag-3 wpag-4 wpag-5
           wpag-6  wpag-7 wpag-8 wpag-9 wpag-10 wpag-11
           wpag-12 WITH FRAME {&FRAME-NAME}.

    FIND first mgadm.emitente OF pedido-compr NO-LOCK.
    /* Dados do emitente */
    ASSIGN     wfornecedor      = emitente.nome-emit
               wendereco       = emitente.endereco
               wcgc            = emitente.cgc
               wcontato        = contato[1]
               wcod-emitente   = emitente.cod-emitente.
    DISPLAY wfornecedor wendereco wcgc FORMAT "99.999.999/9999-99"
            wcontato wcod-emitente WITH FRAME  {&FRAME-NAME}.

    FOR EACH ordem-compra WHERE  ordem-compra.cod-emitente 
                               = pedido-compr.cod-emitente
                          AND   ordem-compra.num-pedido = 
                                pedido-compr.num-pedido NO-LOCK:
        
        ASSIGN wpreco    = ordem-compra.preco-fornec
               waliq-ipi = aliquota-ipi 
               waliq-icm = aliquota-icm
               wpedido   = pedido-compr.num-pedido. 
        DISPLAY waliq-ipi waliq-icm wpreco WITH FRAME  {&FRAME-NAME}.
           
       FOR EACH cond-pagto WHERE cond-pagto.cod-cond-pag = 
                emitente.cod-cond-pag  NO-LOCK:
         /*  DISPLAY PER-PG-DUP[1] PER-PG-DUP[2] PER-PG-DUP[3] cod-ven-par[1]
               prazo[1] WITH FRAME xx TITLE "Condi��o".*/
       END.
    END.
/* procura lo-pedferra */
 FIND FIRST  mgfas.lo-pedferra WHERE lo-pedferra.b-pedido = pedido-compr.num-pedido
                               NO-LOCK NO-ERROR.
 IF AVAILABLE mgfas.lo-pedferra THEN DO:
      IF mgfas.lo-pedferra.b-imposto = 1 THEN ASSIGN wimposto = 1.
      IF mgfas.lo-pedferra.b-imposto = 2 THEN ASSIGN wimposto = 2.
      DISPLAY 
         mgfas.lo-pedferra.b-contato         @ wcontato
         mgfas.lo-pedferra.b-extenso         @ wextenso
         mgfas.lo-pedferra.b-pag1            @ wpag-1 
         mgfas.lo-pedferra.b-pag2            @ wpag-2  
         mgfas.lo-pedferra.b-pag3            @ wpag-3
         mgfas.lo-pedferra.b-pag4            @ wpag-4
         mgfas.lo-pedferra.b-pag5            @ wpag-5
         mgfas.lo-pedferra.b-pag6            @ wpag-6
         mgfas.lo-pedferra.b-pag7            @ wpag-7
         mgfas.lo-pedferra.b-pag8            @ wpag-8  
         mgfas.lo-pedferra.b-pag9            @ wpag-9
         mgfas.lo-pedferra.b-pag10           @ wpag-10
         mgfas.lo-pedferra.b-pag11           @ wpag-11
         mgfas.lo-pedferra.b-pag12           @ wpag-12 
         wimposto
         WITH FRAME {&FRAME-NAME}.
     
        
      END. 
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-confirma
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-confirma C-Win
ON CHOOSE OF bt-confirma IN FRAME DEFAULT-FRAME /* Confirma */
DO:
 ASSIGN wpedido wcontato wextenso wpag-1 wpag-2 wpag-3 wpag-4 wpag-5
         wpag-6 wpag-7 wpag-8 wpag-9 wpag-10 wpag-11 wpag-12 wcod-emitente wimposto.
 FIND FIRST  mgfas.lo-pedferra WHERE lo-pedferra.b-pedido = wpedido NO-ERROR.
 IF AVAILABLE mgfas.lo-pedferra THEN DO:
   MESSAGE "Confirma altera��o do pedido ferramental?" VIEW-AS ALERT-BOX BUTTONS YES-NO
           UPDATE wconf-alt.
  IF wconf-alt = YES THEN  RUN atualiza.
  RUN inicia.
  END.
 IF NOT AVAILABLE mgfas.lo-pedferra THEN DO:
     MESSAGE "CONFIRMA A INCLUS�O DO PEDIDO FERRAMENTAL?" VIEW-AS ALERT-BOX BUTTONS YES-NO
     UPDATE wconf.
     IF wconf = YES THEN DO:
       CREATE mgfas.lo-pedferra.
       RUN atualiza.
       END.
 END. 
 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-exc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-exc C-Win
ON CHOOSE OF bt-exc IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
  RUN wpsf/wpp0107b.r.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-inc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-inc C-Win
ON CHOOSE OF bt-inc IN FRAME DEFAULT-FRAME /* Pedido Ferramental */
DO:
  RUN inicia.
  ENABLE wpedido bt-codigo WITH FRAME {&FRAME-NAME}.
  APPLY "entry":u TO wpedido.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE atualiza C-Win 
PROCEDURE atualiza :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN mgfas.lo-pedferra.b-pedido       = wpedido
         mgfas.lo-pedferra.b-emitente   = wcod-emitente
         mgfas.lo-pedferra.b-contato    = wcontato
         mgfas.lo-pedferra.b-extenso    = wextenso
         mgfas.lo-pedferra.b-pag1       = wpag-1 
         mgfas.lo-pedferra.b-pag2       = wpag-2  
         mgfas.lo-pedferra.b-pag3       = wpag-3
         mgfas.lo-pedferra.b-pag4       = wpag-4
         mgfas.lo-pedferra.b-pag5       = wpag-5
         mgfas.lo-pedferra.b-pag6       = wpag-6
         mgfas.lo-pedferra.b-pag7       = wpag-7 
         mgfas.lo-pedferra.b-pag8       = wpag-8  
         mgfas.lo-pedferra.b-pag9       = wpag-9
         mgfas.lo-pedferra.b-pag10      = wpag-10
         mgfas.lo-pedferra.b-pag11      = wpag-11
         mgfas.lo-pedferra.b-pag12      = wpag-12
         mgfas.lo-pedferra.b-imposto    = wimposto.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wpedido wcod-emitente wfornecedor wendereco wcgc wcontato wimposto 
          waliq-icm wpreco waliq-ipi wextenso wpag-1 wpag-2 wpag-3 wpag-4 wpag-5 
          wpag-6 wpag-7 wpag-8 wpag-9 wpag-10 wpag-11 wpag-12 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE wpedido bt-inc bt-codigo wcod-emitente wfornecedor wendereco wcgc 
         wcontato bt-confirma bt-cancela bt-arq bt-exc bt-sair-2 wimposto 
         waliq-icm wpreco waliq-ipi wextenso wpag-1 wpag-2 wpag-3 wpag-4 wpag-5 
         wpag-6 wpag-7 wpag-8 wpag-9 wpag-10 wpag-11 wpag-12 RECT-1 RECT-2 
         RECT-3 RECT-5 RECT-7 RECT-8 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE INICIA C-Win 
PROCEDURE INICIA :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DISABLE ALL WITH FRAME  {&FRAME-NAME}.
ASSIGN wfornecedor      = ""
       wendereco        = ""
       wcgc             = ""
       wcontato         = ""
       wcod-emitente    = 0
       wpreco           = 0
       waliq-ipi        = 0
       waliq-icm        = 0
       wpedido          = 0. 
ASSIGN wextenso         = ""
       wpag-1           = ""
       wpag-2           = ""
       wpag-3           = ""
       wpag-4           = ""
       wpag-5           = ""
       wpag-6           = ""
       wpag-7           = ""
       wpag-8           = ""
       wpag-9           = ""
       wpag-10          = ""
       wpag-11          = ""
       wpag-12          = ""
       wconf            = NO
       wconf-alt        = NO
       wimposto         = 2.
DISPLAY wfornecedor  wendereco wcgc
       wcontato wcod-emitente wpreco waliq-ipi 
       waliq-icm wpedido wextenso wpag-1 wpag-2  
       wpag-3 wpag-4 wpag-5 wpag-6 wpag-7 wpag-8 
       wpag-9 wpag-10 wpag-11 wpag-12 wimposto  WITH FRAME  {&FRAME-NAME}.
ENABLE bt-inc bt-sair-2 bt-arq bt-exc  WITH FRAME  {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE limpa C-Win 
PROCEDURE limpa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wcontato         = ""
       wextenso         = ""
       wpag-1           = ""
       wpag-2           = ""
       wpag-3           = ""
       wpag-4           = ""
       wpag-5           = ""
       wpag-6           = ""
       wpag-7           = ""
       wpag-8           = ""
       wpag-9           = ""
       wpag-10          = ""
       wpag-11          = ""
       wpag-12          = "".
DISPLAY {&FIELDS-IN-QUERY-{&FRAME-NAME}}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

