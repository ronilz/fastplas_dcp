&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE TEMP-TABLE b-lancamentos-exp
    FIELD b-mod                 AS CHAR FORMAT "X(03)"
    FIELD b-emp                 AS CHAR FORMAT "X(03)"
    FIELD b-data-lote           AS DATE FORMAT "99/99/9999"
    FIELD b-num-lanc            AS INTEGER FORMAT ">>9"
    FIELD b-data-lancamento     AS DATE FORMAT "99/99/9999"
    FIELD b-seq                 AS INTEGER FORMAT ">>9"
    FIELD b-plano               AS CHARACTER FORMAT "X(3)"
    FIELD b-estab               AS CHARACTER FORMAT "X(2)"
    FIELD b-conta-contabil      AS CHARACTER FORMAT "X(6)"
    FIELD b-unidade             AS CHARACTER FORMAT "X(3)"
    FIELD b-valor-lancamento    AS CHAR FORMAT "X(18)" 
    /*AS DECIMAL FORMAT ">>>>>>>>>>>>>>>9.99"*/
    FIELD b-natureza            AS CHAR    FORMAT "x(02)"
    FIELD b-valor-lanca-apropr  AS CHAR FORMAT "X(18)" 
    /*DECIMAL FORMAT ">>>>>>>>>>>>>>>9.99"*/
    FIELD b-historico           AS CHAR FORMAT "X(40)"
    FIELD b-num-reg             AS INTEGER FORMAT "999".
DEFINE VARIABLE wgerou          AS LOGICAL NO-UNDO.
DEFINE VARIABLE wsair           AS LOGICAL FORMAT "Sim/N�o" NO-UNDO.

/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS bt-inc bt-salva-registro bt-gera ~
br-limpa-arq bt-zoom bt-sair wnum-reg wmodulo wempresa wdata-lote wnum-lanc ~
wdata-lancamento wseq wplano westab wconta-contabil wunidade ~
wvalor-lancamento wnatureza wvalor-lanca-apropr whistorico wrel RECT-1 ~
RECT-10 RECT-2 RECT-3 RECT-4 RECT-9 
&Scoped-Define DISPLAYED-OBJECTS wnum-reg wmodulo wempresa wdata-lote ~
wnum-lanc wdata-lancamento wseq wplano westab wconta-contabil wunidade ~
wvalor-lancamento wnatureza wvalor-lanca-apropr whistorico wrel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON br-limpa-arq 
     IMAGE-UP FILE "image/im-era.bmp":U
     LABEL "Limpa Arquivo" 
     SIZE 4 BY 1.13 TOOLTIP "Limpa arquivo".

DEFINE BUTTON bt-gera 
     IMAGE-UP FILE "image/im-exp.bmp":U
     LABEL "Gera Arquivo para Exporta��o" 
     SIZE 4 BY 1.13 TOOLTIP "Gera Arquivo para Exporta��o"
     BGCOLOR 4 FONT 1.

DEFINE BUTTON bt-inc 
     IMAGE-UP FILE "image/im-add.bmp":U
     LABEL "Button 2" 
     SIZE 4 BY 1.08 TOOLTIP "Inclus�o de Registro".

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 4 BY 1.13 TOOLTIP "Sair"
     BGCOLOR 8 .

DEFINE BUTTON bt-salva-registro 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Salva Registro" 
     SIZE 4 BY 1.13 TOOLTIP "Salva Registro".

DEFINE BUTTON bt-zoom 
     IMAGE-UP FILE "image/im-zoom.bmp":U
     LABEL "Button 1" 
     SIZE 5 BY 1.13 TOOLTIP "Verifica arquivo na tela".

DEFINE VARIABLE wconta-contabil AS CHARACTER FORMAT "X(6)":U 
     LABEL "Conta Cont�bil" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wdata-lancamento AS DATE FORMAT "99/99/9999":U 
     LABEL "Data do Lan�amento" 
     VIEW-AS FILL-IN 
     SIZE 12 BY .79 NO-UNDO.

DEFINE VARIABLE wdata-lote AS DATE FORMAT "99/99/9999":U 
     LABEL "Data do Lote" 
     VIEW-AS FILL-IN 
     SIZE 12 BY .79 NO-UNDO.

DEFINE VARIABLE wempresa AS CHARACTER FORMAT "X(3)":U 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(2)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .79 NO-UNDO.

DEFINE VARIABLE whistorico AS CHARACTER FORMAT "X(40)":U 
     LABEL "Hist�rico" 
     VIEW-AS FILL-IN 
     SIZE 26 BY .79 NO-UNDO.

DEFINE VARIABLE wmodulo AS CHARACTER FORMAT "X(3)":U 
     LABEL "M�dulo" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wnum-lanc AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "N�mero do Lan�amento" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wnum-reg AS INTEGER FORMAT "999":U INITIAL 0 
     LABEL "N�mero do Registro" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wplano AS CHARACTER FORMAT "X(3)":U 
     LABEL "Plano de Contas" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE {&NEW} SHARED VARIABLE wrel AS CHARACTER FORMAT "X(30)":U INITIAL ? 
     LABEL "Nome do Arquivo para Exporta��o" 
     VIEW-AS FILL-IN 
     SIZE 26 BY .79 NO-UNDO.

DEFINE VARIABLE wseq AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Sequ�ncia" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wunidade AS CHARACTER FORMAT "X(3)":U 
     LABEL "Unidade de neg�cio" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wvalor-lanca-apropr AS DECIMAL FORMAT ">>>>>>>>>>>>>>>9.99":U INITIAL 0 
     LABEL "Valor do lan�amento Apropria��o" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .79 NO-UNDO.

DEFINE VARIABLE wvalor-lancamento AS DECIMAL FORMAT ">>>>>>>>>>>>>>>9.99":U INITIAL 0 
     LABEL "Valor do Lan�amento" 
     VIEW-AS FILL-IN 
     SIZE 19 BY .79 NO-UNDO.

DEFINE VARIABLE wnatureza AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "DB", 1,
"CR", 2
     SIZE 12 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 72 BY .5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 54 BY 1.5
     BGCOLOR 15 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 72 BY 14.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 72 BY .5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 72 BY .5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 72 BY 1.5
     BGCOLOR 8 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     bt-inc AT ROW 2 COL 25
     bt-salva-registro AT ROW 2 COL 29
     bt-gera AT ROW 2 COL 33
     br-limpa-arq AT ROW 2 COL 37
     bt-zoom AT ROW 2 COL 41
     bt-sair AT ROW 2 COL 46
     wnum-reg AT ROW 4.25 COL 31 COLON-ALIGNED
     wmodulo AT ROW 5 COL 31 COLON-ALIGNED
     wempresa AT ROW 5.75 COL 31 COLON-ALIGNED
     wdata-lote AT ROW 6.5 COL 31 COLON-ALIGNED
     wnum-lanc AT ROW 7.25 COL 31 COLON-ALIGNED
     wdata-lancamento AT ROW 8 COL 31 COLON-ALIGNED
     wseq AT ROW 8.75 COL 31 COLON-ALIGNED
     wplano AT ROW 9.5 COL 31 COLON-ALIGNED
     westab AT ROW 10.25 COL 31 COLON-ALIGNED
     wconta-contabil AT ROW 11 COL 31 COLON-ALIGNED
     wunidade AT ROW 11.75 COL 31 COLON-ALIGNED
     wvalor-lancamento AT ROW 12.5 COL 31 COLON-ALIGNED
     wnatureza AT ROW 13.25 COL 33 NO-LABEL
     wvalor-lanca-apropr AT ROW 14.25 COL 31 COLON-ALIGNED
     whistorico AT ROW 15 COL 31 COLON-ALIGNED
     wrel AT ROW 16.25 COL 34 COLON-ALIGNED
     RECT-1 AT ROW 17.75 COL 3
     RECT-10 AT ROW 16 COL 11
     RECT-2 AT ROW 3.75 COL 3
     RECT-3 AT ROW 1.25 COL 3
     RECT-4 AT ROW 3.25 COL 3
     RECT-9 AT ROW 1.75 COL 3
     "Natureza" VIEW-AS TEXT
          SIZE 8 BY .54 AT ROW 13.5 COL 21
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 75.57 BY 17.92
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Gera��o de Arquivo para Importa��o de Lan�amentos Cont�beis"
         HEIGHT             = 17.92
         WIDTH              = 75.57
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* SETTINGS FOR FILL-IN wrel IN FRAME DEFAULT-FRAME
   SHARED                                                               */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Gera��o de Arquivo para Importa��o de Lan�amentos Cont�beis */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Gera��o de Arquivo para Importa��o de Lan�amentos Cont�beis */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME br-limpa-arq
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-limpa-arq C-Win
ON CHOOSE OF br-limpa-arq IN FRAME DEFAULT-FRAME /* Limpa Arquivo */
DO:
  RUN inicia.
  MESSAGE "Arquivo Zerado!!" VIEW-AS ALERT-BOX BUTTONS OK.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-gera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-gera C-Win
ON CHOOSE OF bt-gera IN FRAME DEFAULT-FRAME /* Gera Arquivo para Exporta��o */
DO:
    ASSIGN wrel.
    OUTPUT TO VALUE(wrel).
    FOR EACH b-lancamentos-exp NO-LOCK:
     PUT UNFORMATTED  b-mod       FORMAT "X(03)"
     b-emp                  FORMAT "X(03)"
     b-data-lote            FORMAT "99/99/9999"
     b-num-lanc             FORMAT ">>9"
     b-data-lancamento      FORMAT "99/99/9999"
     b-seq                  FORMAT ">>9"
     FILL(" ",3) /* espa�os p/ acertar formato do plano */
     b-plano                FORMAT "X(3)" 
     FILL(" ",1) /* espa�os p/ acertar formato do plano */
     b-estab                FORMAT "X(2)"
     b-conta-contabil       FORMAT "X(6)"  
     b-unidade              FORMAT "X(3)" 
     b-valor-lancamento     FORMAT "x(18)" AT 62
     b-natureza             FORMAT "x(02)" AT 80
     b-valor-lanca-apropr   FORMAT "X(18)" AT 86
     b-historico            FORMAT "X(40)" AT 104
     SKIP.
  END.
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
  ASSIGN wgerou = YES.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-inc
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-inc C-Win
ON CHOOSE OF bt-inc IN FRAME DEFAULT-FRAME /* Button 2 */
DO:
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  IF wgerou = NO THEN DO: 
    MESSAGE "Arquivo n�o foi gerado!" SKIP "Deseja realmente sair?"
    VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wsair.
  END.
  IF wsair = YES
  OR wgerou = YES  THEN DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-salva-registro
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-salva-registro C-Win
ON CHOOSE OF bt-salva-registro IN FRAME DEFAULT-FRAME /* Salva Registro */
DO:
  
  ASSIGN wmodulo wempresa
      wdata-lote wnum-lanc wdata-lancamento
      wseq wplano westab wconta-contabil
      wunidade wvalor-lancamento wnatureza
      wvalor-lanca-apropr whistorico.
  
  CREATE  b-lancamentos-exp.
    ASSIGN b-mod                 = wmodulo
           b-emp                 = wempresa
           b-data-lote           = wdata-lote
           b-num-lanc            = wnum-lanc
           b-data-lancamento     = wdata-lancamento
           b-seq                 = wseq
           b-plano               = wplano
           b-estab               = westab
           b-conta-contabil      = wconta-contabil
           b-unidade             = wunidade     
           b-valor-lancamento    = SUBSTRING(STRING(wvalor-lancamento,">>>>>>>>>>>>>>>9.99"),1,16)
                                 + SUBSTRING(STRING(wvalor-lancamento,">>>>>>>>>>>>>>>9.99"),18,2).
  

    IF wnatureza = 1 THEN b-natureza = "DB".
    IF wnatureza = 2 THEN b-natureza = "CR".
          
    ASSIGN b-valor-lanca-apropr  = substring(string(wvalor-lanca-apropr,">>>>>>>>>>>>>>>9.99"),1,16)
                                 + substring(string(wvalor-lanca-apropr,">>>>>>>>>>>>>>>9.99"),18,2)
           b-historico           = whistorico
           wnum-reg              = wnum-reg + 1
           wseq                  = wnum-reg.
    DISPLAY wnum-reg wseq WITH FRAME {&FRAME-NAME}. 
    RUN limpa.
    /*MESSAGE "INCLUS�O OK DO REGISTRO:" wnum-reg - 1 VIEW-AS ALERT-BOX.*/
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-zoom
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-zoom C-Win
ON CHOOSE OF bt-zoom IN FRAME DEFAULT-FRAME /* Button 1 */
DO:
 IF wgerou = NO THEN MESSAGE "Arquivo ainda n�o foi gerado!" VIEW-AS ALERT-BOX.
  IF wgerou = YES  THEN RUN gera-tela.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wnum-reg wmodulo wempresa wdata-lote wnum-lanc wdata-lancamento wseq 
          wplano westab wconta-contabil wunidade wvalor-lancamento wnatureza 
          wvalor-lanca-apropr whistorico wrel 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE bt-inc bt-salva-registro bt-gera br-limpa-arq bt-zoom bt-sair wnum-reg 
         wmodulo wempresa wdata-lote wnum-lanc wdata-lancamento wseq wplano 
         westab wconta-contabil wunidade wvalor-lancamento wnatureza 
         wvalor-lanca-apropr whistorico wrel RECT-1 RECT-10 RECT-2 RECT-3 
         RECT-4 RECT-9 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

ASSIGN warquivo = wrel.

get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
end.
    
run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DISABLE ALL EXCEPT bt-inc bt-sair WITH FRAME    {&FRAME-NAME}. 
 RUN limpa.
 ASSIGN wrel = "V:\spool\LANCAM" + STRING(TIME) + ".tmp"
        wnum-reg = 1
        wseq     = 1
        wgerou   = NO.
 DISPLAY wnum-reg wrel wseq  WITH FRAME {&FRAME-NAME}.
 FOR EACH  b-lancamentos-exp:
   DELETE  b-lancamentos-exp.
 END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE limpa C-Win 
PROCEDURE limpa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 ASSIGN wmodulo                 = "FGL"
        wempresa                = "999"
        wdata-lote              = TODAY
        wnum-lanc               = 1
        wdata-lancamento        = TODAY
        wplano                  = "999"
        westab                  = "99"
        wconta-contabil         = ""
        wunidade                = "000"
        wvalor-lancamento       = 0
        wnatureza               = 1
        wvalor-lanca-apropr     = 0
        whistorico              = "".
 DISPLAY wmodulo wempresa
      wdata-lote wnum-lanc wdata-lancamento
      wseq wplano westab wconta-contabil
      wunidade wvalor-lancamento wnatureza
      wvalor-lanca-apropr whistorico  WITH FRAME  {&FRAME-NAME}.
 APPLY "entry":u TO wmodulo.
 DISABLE ALL EXCEPT bt-inc bt-sair br-limpa-arq bt-gera 
                    bt-zoom WITH FRAME    {&FRAME-NAME}. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

