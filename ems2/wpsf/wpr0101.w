&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          movind           PROGRESS
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

 DEFINE VARIABLE wcod-t           LIKE  movind.saldo-terc.cod-emitente.      
 DEFINE VARIABLE wserie-t         LIKE  movind.saldo-terc.serie-docto.  
 DEFINE VARIABLE wnro-t           LIKE  movind.saldo-terc.nro-docto.    
 DEFINE VARIABLE wnat-t           LIKE  movind.saldo-terc.nat-operacao. 
 DEFINE VARIABLE wit-codigo-t     LIKE  movind.saldo-terc.it-codigo.  
 DEFINE VARIABLE wsequencia-t     LIKE  movind.saldo-terc.sequencia.
 DEFINE VARIABLE wconf            AS    LOGICAL FORMAT "Sim/Nao".
 DEFINE VARIABLE wrec             AS    RECID.
 DEFINE VARIABLE wconf-exc        AS    LOGICAL FORMAT "Sim/Nao".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME BROWSE-1

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES saldo-terc componente

/* Definitions for BROWSE BROWSE-1                                      */
&Scoped-define FIELDS-IN-QUERY-BROWSE-1 saldo-terc.cod-emitente ~
saldo-terc.it-codigo saldo-terc.dt-retorno saldo-terc.serie-docto ~
saldo-terc.nro-docto saldo-terc.nat-operacao saldo-terc.sequencia ~
saldo-terc.quantidade 
&Scoped-define ENABLED-FIELDS-IN-QUERY-BROWSE-1 
&Scoped-define QUERY-STRING-BROWSE-1 FOR EACH saldo-terc ~
      WHERE saldo-terc.quantidade <> 0 NO-LOCK, ~
      EACH componente OF saldo-terc NO-LOCK ~
    BY saldo-terc.cod-emitente INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-BROWSE-1 OPEN QUERY BROWSE-1 FOR EACH saldo-terc ~
      WHERE saldo-terc.quantidade <> 0 NO-LOCK, ~
      EACH componente OF saldo-terc NO-LOCK ~
    BY saldo-terc.cod-emitente INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-BROWSE-1 saldo-terc componente
&Scoped-define FIRST-TABLE-IN-QUERY-BROWSE-1 saldo-terc
&Scoped-define SECOND-TABLE-IN-QUERY-BROWSE-1 componente


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-BROWSE-1}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 RECT-4 wcod wcod-fim ~
wdatai wdataf wserie wserie-fim wnro wnro-fim wnat wnat-fim wit-codigo ~
wit-codigo-fim wsequencia wsequencia-fim BUTTON-1 BUTTON-4 BROWSE-1 ~
BUTTON-2 BUTTON-3 bt-sair 
&Scoped-Define DISPLAYED-OBJECTS wcod wcod-fim wdatai wdataf wserie ~
wserie-fim wnro wnro-fim wnat wnat-fim wit-codigo wit-codigo-fim wsequencia ~
wsequencia-fim 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 4 BY 1.25
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Procura" 
     SIZE 4 BY 1.13.

DEFINE BUTTON BUTTON-2 
     IMAGE-UP FILE "image/im-chck1.bmp":U
     LABEL "Listar Todos" 
     SIZE 4 BY 1.25.

DEFINE BUTTON BUTTON-3 
     IMAGE-UP FILE "image/im-era.bmp":U
     LABEL "Exclus�o Individual" 
     SIZE 4 BY 1.25.

DEFINE BUTTON BUTTON-4 
     IMAGE-UP FILE "image/im-era1.bmp":U
     LABEL "Excluir Todos" 
     SIZE 4 BY 1.13.

DEFINE VARIABLE wcod AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "C�d. Emitente Inicial" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .75 NO-UNDO.

DEFINE VARIABLE wcod-fim AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .79 NO-UNDO.

DEFINE VARIABLE wdataf AS DATE FORMAT "99/99/9999":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .79 NO-UNDO.

DEFINE VARIABLE wdatai AS DATE FORMAT "99/99/9999":U 
     LABEL "Data de retorno" 
     VIEW-AS FILL-IN 
     SIZE 11 BY .79 NO-UNDO.

DEFINE VARIABLE wit-codigo AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item inicial" 
     VIEW-AS FILL-IN 
     SIZE 13 BY .79 NO-UNDO.

DEFINE VARIABLE wit-codigo-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 13.86 BY .79 NO-UNDO.

DEFINE VARIABLE wnat AS CHARACTER FORMAT "X(6)":U 
     LABEL "Natureza Opera��o inicial" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wnat-fim AS CHARACTER FORMAT "X(6)":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wnro AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nro. Documento Inicial" 
     VIEW-AS FILL-IN 
     SIZE 13.86 BY .79 NO-UNDO.

DEFINE VARIABLE wnro-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 13.86 BY .79 NO-UNDO.

DEFINE VARIABLE wsequencia AS DECIMAL FORMAT ">>>>9":U INITIAL 0 
     LABEL "Sequ�ncia inicial" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wsequencia-fim AS INTEGER FORMAT ">>>>9":U INITIAL 0 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wserie AS CHARACTER FORMAT "X(5)":U 
     LABEL "S�rie inicial" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE wserie-fim AS CHARACTER FORMAT "X(5)":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 6.5.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 77 BY .75
     BGCOLOR 7 FGCOLOR 7 .

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 77 BY .75
     BGCOLOR 7 FGCOLOR 7 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 77 BY .75
     BGCOLOR 7 FGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY BROWSE-1 FOR 
      saldo-terc, 
      componente SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE BROWSE-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS BROWSE-1 C-Win _STRUCTURED
  QUERY BROWSE-1 NO-LOCK DISPLAY
      saldo-terc.cod-emitente FORMAT ">>>>>>>>9":U WIDTH 7.43
      saldo-terc.it-codigo FORMAT "x(16)":U WIDTH 12.43
      saldo-terc.dt-retorno FORMAT "99/99/9999":U WIDTH 12.43
      saldo-terc.serie-docto FORMAT "x(5)":U WIDTH 6.43
      saldo-terc.nro-docto FORMAT "x(16)":U WIDTH 8.43
      saldo-terc.nat-operacao FORMAT "x(06)":U WIDTH 7.43
      saldo-terc.sequencia FORMAT ">>>>9":U WIDTH 5.43
      saldo-terc.quantidade FORMAT ">>>>>,>>9.9999":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 77 BY 6.25
         FONT 1 ROW-HEIGHT-CHARS .75 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wcod AT ROW 2.75 COL 30 COLON-ALIGNED
     wcod-fim AT ROW 2.75 COL 46 COLON-ALIGNED
     wdatai AT ROW 3.5 COL 30 COLON-ALIGNED
     wdataf AT ROW 3.5 COL 46 COLON-ALIGNED
     wserie AT ROW 4.25 COL 30 COLON-ALIGNED
     wserie-fim AT ROW 4.25 COL 46 COLON-ALIGNED
     wnro AT ROW 5 COL 30 COLON-ALIGNED
     wnro-fim AT ROW 5 COL 46 COLON-ALIGNED
     wnat AT ROW 5.75 COL 30 COLON-ALIGNED
     wnat-fim AT ROW 5.75 COL 46 COLON-ALIGNED
     wit-codigo AT ROW 6.5 COL 30 COLON-ALIGNED
     wit-codigo-fim AT ROW 6.5 COL 46 COLON-ALIGNED
     wsequencia AT ROW 7.25 COL 30 COLON-ALIGNED
     wsequencia-fim AT ROW 7.25 COL 46 COLON-ALIGNED
     BUTTON-1 AT ROW 8.75 COL 24
     BUTTON-4 AT ROW 8.75 COL 41
     BROWSE-1 AT ROW 11 COL 3
     BUTTON-2 AT ROW 18.25 COL 17
     BUTTON-3 AT ROW 18.25 COL 35
     bt-sair AT ROW 18.25 COL 56
     "Lista Todos" VIEW-AS TEXT
          SIZE 8 BY .54 AT ROW 18.75 COL 22
     "Exclui Individual" VIEW-AS TEXT
          SIZE 13 BY .54 AT ROW 18.75 COL 40
     "Exclui Todos da Sele��o" VIEW-AS TEXT
          SIZE 18 BY .54 AT ROW 9 COL 46
     "Procura" VIEW-AS TEXT
          SIZE 8 BY .54 AT ROW 9 COL 28
     "Par�metros" VIEW-AS TEXT
          SIZE 8 BY .54 AT ROW 2 COL 34
          FONT 1
     RECT-1 AT ROW 2.25 COL 3
     RECT-2 AT ROW 1.25 COL 3
     RECT-3 AT ROW 17.5 COL 3
     RECT-4 AT ROW 10.25 COL 3
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 84 BY 19.63
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Acertos de Saldo em Poder de Terceiros - ELIMINA��O - wpr0101"
         HEIGHT             = 18.75
         WIDTH              = 80
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB BROWSE-1 BUTTON-4 DEFAULT-FRAME */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE BROWSE-1
/* Query rebuild information for BROWSE BROWSE-1
     _TblList          = "movind.saldo-terc,movind.componente OF movind.saldo-terc"
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _OrdList          = "movind.saldo-terc.cod-emitente|yes"
     _Where[1]         = "movind.saldo-terc.quantidade <> 0"
     _FldNameList[1]   > movind.saldo-terc.cod-emitente
"saldo-terc.cod-emitente" ? ? "integer" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[2]   > movind.saldo-terc.it-codigo
"saldo-terc.it-codigo" ? ? "character" ? ? ? ? ? ? no ? no no "12.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[3]   > movind.saldo-terc.dt-retorno
"saldo-terc.dt-retorno" ? ? "date" ? ? ? ? ? ? no ? no no "12.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[4]   > movind.saldo-terc.serie-docto
"saldo-terc.serie-docto" ? ? "character" ? ? ? ? ? ? no ? no no "6.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[5]   > movind.saldo-terc.nro-docto
"saldo-terc.nro-docto" ? ? "character" ? ? ? ? ? ? no ? no no "8.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[6]   > movind.saldo-terc.nat-operacao
"saldo-terc.nat-operacao" ? ? "character" ? ? ? ? ? ? no ? no no "7.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[7]   > movind.saldo-terc.sequencia
"saldo-terc.sequencia" ? ? "integer" ? ? ? ? ? ? no ? no no "5.43" yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _FldNameList[8]   = movind.saldo-terc.quantidade
     _Query            is OPENED
*/  /* BROWSE BROWSE-1 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Acertos de Saldo em Poder de Terceiros - ELIMINA��O - wpr0101 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Acertos de Saldo em Poder de Terceiros - ELIMINA��O - wpr0101 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Procura */
DO:
  ASSIGN wcod wcod-fim wdatai wdataf wserie wserie-fim wnro wnro-fim wnat wnat-fim wit-codigo
         wit-codigo-fim wsequencia wsequencia-fim.
  DISABLE ALL WITH FRAME  {&FRAME-NAME}.
  OPEN QUERY browse-1  FOR EACH movind.saldo-terc NO-LOCK
        WHERE movind.saldo-terc.cod-emitente      ge wcod
        AND movind.saldo-terc.cod-emitente le wcod-fim
        AND movind.saldo-terc.dt-retorno   GE wdatai
        AND movind.saldo-terc.dt-retorno   LE wdataf
        AND movind.saldo-terc.serie-docto  ge wserie
        and movind.saldo-terc.serie-docto  le wserie-fim
        and movind.saldo-terc.nro-docto    ge wnro
        and movind.saldo-terc.nro-docto    le wnro-fim
        and movind.saldo-terc.nat-operacao ge wnat
        and movind.saldo-terc.nat-operacao le wnat-fim
        and movind.saldo-terc.it-codigo    ge wit-codigo
        AND movind.saldo-terc.it-codigo    le wit-codigo-fim
        AND movind.saldo-terc.sequencia    ge wsequencia
        and movind.saldo-terc.sequencia    le wsequencia-fim
        AND movind.saldo-terc.quantidade   <> 0,
        EACH movind.componente NO-LOCK OF movind.saldo-terc 
        BY movind.saldo-terc.cod-emitente.
      GET FIRST browse-1.  
      ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-2 C-Win
ON CHOOSE OF BUTTON-2 IN FRAME DEFAULT-FRAME /* Listar Todos */
DO:
   OPEN QUERY browse-1 FOR EACH movind.saldo-terc  
                           WHERE movind.saldo-terc.quantidade <> 0 NO-LOCK,
   EACH movind.componente NO-LOCK OF movind.saldo-terc
   BY saldo-terc.cod-emitente.
   GET FIRST browse-1.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-3 C-Win
ON CHOOSE OF BUTTON-3 IN FRAME DEFAULT-FRAME /* Exclus�o Individual */
DO:
  
   MESSAGE "CONFIRMA A ELIMINA��O DO REGISTRO?" 
           VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wconf.
   ASSIGN wrec = RECID(movind.saldo-terc).
  
   IF wconf = YES THEN DO:
      FOR EACH  movind.saldo-terc WHERE recid(saldo-terc) = wrec,
          EACH  componente OF saldo-terc:
       DELETE movind.saldo-terc.
       DELETE movind.componente.
       END.
      /*FIND NEXT  movind.saldo-terc no-lock.
      IF AVAILABLE movind.saldo-terc THEN REPOSITION browse-1 TO ROWID ROWID(saldo-terc).
      /*IF NOT AVAILABLE movind.saldo-terc THEN DO:
        FIND PREV movind.saldo-terc no-lock.
        IF AVAILABLE movind.saldo-terc THEN REPOSITION browse-1 TO ROWID ROWID(saldo-terc).
      END.*/*/
      REPOSITION browse-1  BACKWARDS 1 . 
      /*{&OPEN-QUERY-{&BROWSE-1}}*/
      END.
   IF wconf = NO  THEN MESSAGE "EXCLUS�O CANCELADA!" VIEW-AS ALERT-BOX.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-4 C-Win
ON CHOOSE OF BUTTON-4 IN FRAME DEFAULT-FRAME /* Excluir Todos */
DO:
   MESSAGE "ESTA OP��O EXCLUIR� TODOS OS REGISTROS SELECIONADOS NA CAIXA DE PAR�METROS."
           VIEW-AS ALERT-BOX.
   MESSAGE "CONFIRMA A ELIMINA��O DOS REGISTROS LISTADOS?" 
           VIEW-AS ALERT-BOX BUTTONS YES-NO UPDATE wconf.
  
   IF wconf = YES THEN DO:
       ASSIGN wcod wcod-fim wdatai wdataf wserie wserie-fim wnro wnro-fim wnat wnat-fim wit-codigo
         wit-codigo-fim wsequencia wsequencia-fim.
       DISABLE ALL WITH FRAME  {&FRAME-NAME}.
       FOR EACH movind.saldo-terc 
        WHERE movind.saldo-terc.cod-emitente      ge wcod
        AND movind.saldo-terc.cod-emitente le wcod-fim
        AND movind.saldo-terc.dt-retorno   GE wdatai
        AND movind.saldo-terc.dt-retorno   LE wdataf
        AND movind.saldo-terc.serie-docto  ge wserie
        and movind.saldo-terc.serie-docto  le wserie-fim
        and movind.saldo-terc.nro-docto    ge wnro
        and movind.saldo-terc.nro-docto    le wnro-fim
        and movind.saldo-terc.nat-operacao ge wnat
        and movind.saldo-terc.nat-operacao le wnat-fim
        and movind.saldo-terc.it-codigo    ge wit-codigo
        AND movind.saldo-terc.it-codigo    le wit-codigo-fim
        AND movind.saldo-terc.sequencia    ge wsequencia
        and movind.saldo-terc.sequencia    le wsequencia-fim
        AND movind.saldo-terc.quantidade   <> 0,
        EACH movind.componente OF movind.saldo-terc 
        BY movind.saldo-terc.cod-emitente.
        DELETE movind.componente.
        DELETE movind.saldo-terc.
       END.
      END.
   IF wconf = NO  THEN MESSAGE "EXCLUS�O CANCELADA!" VIEW-AS ALERT-BOX.
   ENABLE ALL WITH FRAME   {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME BROWSE-1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  /*11/10/18 / DEVIDO ESTAR APARECENDO PARA TODOS OS USUARIOS, MESMO COM PERMISS�O SOMENTE PARA BRUM*/
  MESSAGE "PROGRAMA DESATIVADO EM 11/10/18, POR MOTIVOS DE PERMISS�ES, ENTRE EM CONTATO COM VAL�RIA-TI."
          VIEW-AS ALERT-BOX.
  RUN FIM.
  /* INIBIDO DEVIDO PERMISS�O - 11/10/18
  RUN enable_UI.
  RUN inicia.
  */
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wcod wcod-fim wdatai wdataf wserie wserie-fim wnro wnro-fim wnat 
          wnat-fim wit-codigo wit-codigo-fim wsequencia wsequencia-fim 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 RECT-3 RECT-4 wcod wcod-fim wdatai wdataf wserie 
         wserie-fim wnro wnro-fim wnat wnat-fim wit-codigo wit-codigo-fim 
         wsequencia wsequencia-fim BUTTON-1 BUTTON-4 BROWSE-1 BUTTON-2 BUTTON-3 
         bt-sair 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE FIM C-Win 
PROCEDURE FIM :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 ASSIGN wcod        = 1
        wcod-fim    = 999999999
        wserie      = ""
        wserie-fim  = "ZZZZZ"
        wnro        = ""
        wnro-fim    = "ZZZZZZZZZZZZZZZZ"
        wnat        = ""
        wnat-fim    = "ZZZZZZ"
        wit-codigo  = ""
        wit-codigo-fim  = "ZZZZZZZZZZZZZZZZ"
        wsequencia      = 0
        wsequencia-fim  = 99999
        wdatai          = ?
        wdataf          = 12/31/9999.
 DISPLAY  wcod wcod-fim wdatai wdataf wserie wserie-fim wnro wnro-fim wnat wnat-fim wit-codigo
         wit-codigo-fim wsequencia wsequencia-fim WITH FRAME    {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE procura C-Win 
PROCEDURE procura :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

