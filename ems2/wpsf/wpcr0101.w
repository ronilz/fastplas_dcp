&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

    DEFINE VARIABLE wtrans      AS CHAR FORMAT "x(03)".
    DEFINE VARIABLE wtot-atu    LIKE titulo.vl-original.
    DEFINE VARIABLE wtot-baixa  LIKE mov-tit.vl-baixa.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS wdt-ini wdt-fim wemi-ini wemi-fim BUTTON-1 ~
BtnDone 
&Scoped-Define DISPLAYED-OBJECTS wdt-ini wdt-fim wemi-ini wemi-fim 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON BtnDone DEFAULT 
     LABEL "Sair" 
     SIZE 15 BY 1.13
     BGCOLOR 8 .

DEFINE BUTTON BUTTON-1 
     LABEL "Executa" 
     SIZE 15 BY 1.13.

DEFINE VARIABLE wdt-fim AS DATE FORMAT "99/99/9999":U 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Data de Vencimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wemi-fim AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "a" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY 1 NO-UNDO.

DEFINE VARIABLE wemi-ini AS INTEGER FORMAT ">>>>>>>>9":U INITIAL 0 
     LABEL "Emitente" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wdt-ini AT ROW 2 COL 19 COLON-ALIGNED
     wdt-fim AT ROW 2 COL 35 COLON-ALIGNED
     wemi-ini AT ROW 4.25 COL 22 COLON-ALIGNED
     wemi-fim AT ROW 4.25 COL 33 COLON-ALIGNED
     BUTTON-1 AT ROW 6.5 COL 15
     BtnDone AT ROW 6.75 COL 33
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 61.86 BY 8.5
         FONT 1
         DEFAULT-BUTTON BtnDone.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Relat�rio de Titulos C.Receber Abertos - wpcr0101"
         HEIGHT             = 8.5
         WIDTH              = 61.86
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Relat�rio de Titulos C.Receber Abertos - wpcr0101 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Relat�rio de Titulos C.Receber Abertos - wpcr0101 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BtnDone
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BtnDone C-Win
ON CHOOSE OF BtnDone IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-1 C-Win
ON CHOOSE OF BUTTON-1 IN FRAME DEFAULT-FRAME /* Executa */
DO:
  ASSIGN  wdt-ini wdt-fim wemi-ini wemi-fim. 
  OUTPUT TO "V:\spool\TIT-ABER.TXT" PAGED.
 /* FORM HEADER
      "  Cliente   Nome Abreviado   Documento       Est      Esp  Serie        Dt Vcto         Vl Original   Dt.Cred.         Vl.Baixa       Valor Saldo"
      WITH FRAME xx NO-LABEL WIDTH 350.
   */
   DISABLE ALL WITH FRAME  {&FRAME-NAME}.
   FOR EACH titulo WHERE (dt-vencimen GE wdt-ini
                   AND   dt-vencimen LE wdt-fim)
                   AND   (cod-emitente GE wemi-ini
                   AND   cod-emitente LE wemi-fim) 
                   AND   nr-docto = "0138450"
                   NO-LOCK BREAK 
                   BY cod-emitente BY nr-docto:
     IF FIRST-OF(titulo.nr-docto) THEN  ASSIGN wtot-baixa = 0.
     /*IF titulo.vl-saldo = 0 THEN NEXT.  */
     FIND FIRST emitente WHERE emitente.cod-emitente = titulo.cod-emitente 
                       NO-LOCK NO-ERROR .
    

     FOR EACH mov-tit  WHERE (transacao = 2
                              OR    transacao = 13
                              OR    transacao = 3) 
                              AND   mov-tit.ep-codigo = titulo.ep-codigo
                              AND   mov-tit.cod-estabel = titulo.cod-estabel
                              AND   mov-tit.cod-esp = titulo.cod-esp
                              AND   mov-tit.serie       = titulo.serie
                              AND   mov-tit.nr-docto    = titulo.nr-docto
        NO-LOCK BREAK BY mov-tit.nr-docto:
        ASSIGN wtot-baixa = wtot-baixa + vl-baixa.

       IF LAST-OF(mov-tit.nr-docto) THEN do:
           ASSIGN wtot-atu = titulo.vl-original - wtot-baixa.
       /*IF wtot-atu <> 0 THEN DO:*/
          DISPLAY titulo.cod-emitente COLUMN-LABEL "Cli"
           emitente.nome-abrev 
           titulo.nr-docto            
           titulo.cod-estabel         
           titulo.cod-esp titulo.serie   COLUMN-LABEL "Serie"    
           titulo.dt-vencimen         
           titulo.vl-original   
           mov-tit.dt-credito 
           wtot-baixa 
           wtot-atu  WITH FRAME xx WIDTH 300.
     /* Verifica hist�rico */
       FOR EACH his-tit OF titulo WHERE his-tit.sequencia <> 0999999 NO-LOCK
                  BREAK BY titulo.nr-docto BY dt-his-tit DESCENDING:
         IF FIRST-OF(titulo.nr-docto) THEN DISPLAY  dt-his-tit substring(string(historico,"x(1500)"),1,128) FORMAT "X(128)" 
         WITH FRAME xx.
         DOWN WITH FRAME xx.
         END. 
       
       END.
     END.
   END.
   OUTPUT CLOSE.
   MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX.
   ENABLE ALL WITH FRAME  {&FRAME-NAME}.
   END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN  inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wdt-ini wdt-fim wemi-ini wemi-fim 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE wdt-ini wdt-fim wemi-ini wemi-fim BUTTON-1 BtnDone 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN wdt-ini = TODAY
       wdt-fim  = TODAY
       wemi-ini = 0
       wemi-fim = 999999999
       wtot-baixa = 0
       wtot-atu   = 0.
DISPLAY   wdt-ini wdt-fim wemi-ini wemi-fim WITH FRAME {&FRAME-NAME}. 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

