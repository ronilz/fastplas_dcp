OUTPUT TO "V:\spool\pedido.lst" PAGED.
DEFINE VARIABLE wpedido LIKE pedido-compr.num-pedido NO-UNDO COLUMN-LABEL "Pedido".
UPDATE wpedido.
  FOR EACH pedido-compr WHERE pedido-compr.num-pedido = wpedido
                       /* WHERE cod-emitente = 113 */NO-LOCK:
    DISPLAY pedido-compr.cod-emitente COLUMN-LABEL "Emitente"
            pedido-compr.num-pedido   COLUMN-LABEL "Pedido" 
            WITH FRAME f-lista.
    FOR EACH ordem-compra WHERE ordem-compra.num-pedido = pedido-compr.num-pedido NO-LOCK:
      DISPLAY ordem-compra.numero-ordem COLUMN-LABEL "Ordem"
              WITH FRAME f-lista.
      FOR EACH prazo-compra OF ordem-compra NO-LOCK:
        DISPLAY prazo-compra.parcela    COLUMN-LABEL "Parcela"
                ordem-compra.it-codigo  COLUMN-LABEL "Item"
                WITH FRAME f-lista.
         FOR EACH  recebimento WHERE   recebimento.cod-movto    = 1
                       AND recebimento.num-pedido   = pedido-compr.num-pedido
                       AND recebimento.numero-ordem = ordem-compra.numero-ordem
                       AND recebimento.parcela      = prazo-compra.parcela
                       AND recebimento.it-codigo    = ordem-compra.it-codigo
                       AND recebimento.cod-emitente = pedido-compr.cod-emitente
                       NO-LOCK BREAK BY it-codigo BY recebimento.data-nota
                       BY recebimento.numero-nota:
            IF  last-of(recebimento.it-codigo) AND LAST-OF(recebimento.data-nota)  
            AND LAST-OF(recebimento.numero-nota)
            THEN DO:
         
          DISPLAY /*recebimento.num-pedido recebimento.numero-ordem recebimento.num-pedido 
                      recebimento.it-codigo RECEBIMENTO.PARCELA*/
                      recebimento.data-nota                 COLUMN-LABEL "Dt.Nota"
                      recebimento.numero-nota               COLUMN-LABEL "Nr.Nota"
                      substring(ordem-compra.narrativa,1,6) COLUMN-LABEL "Nota Corte Narrativa"
                      WITH FRAME F-LISTA WIDTH 200 DOWN STREAM-IO.
                      DOWN WITH FRAME f-lista.
          END.
              END.
            END.
          END.
       END.
