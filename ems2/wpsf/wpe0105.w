&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */


/*DEFINE VARIABLE wwcc      LIKE grup-maquina.cc-codigo EXTENT 20 
                          COLUMN-LABEL "C.C".*/
DEFINE VARIABLE wwcc      AS CHAR FORMAT "X(06)" EXTENT 20.
DEFINE VARIABLE wquant    AS DECIMAL FORMAT ">,>>9.9999" EXTENT 20
                          COLUMN-LABEL "Temp.Homem".
DEFINE VARIABLE wquantr   AS DECIMAL FORMAT "ZZZ,ZZ9.9999"
                          COLUMN-LABEL "Temp.Homem".
DEFINE VARIABLE wcont     AS INTEGER FORMAT "99".
DEFINE VARIABLE wlabel    AS INTEGER FORMAT "99".
DEFINE VARIABLE wtotcc    AS DECIMAL FORMAT "999,999.9999".
DEFINE VARIABLE wtotccr   AS DECIMAL FORMAT "999,999.9999".
DEFINE BUFFER b-estnivel2 FOR estrutura.                                      
DEFINE BUFFER b-estnivel3 FOR estrutura.
DEFINE BUFFER b-estnivel4 FOR estrutura.
DEFINE BUFFER b-estnivel5 FOR estrutura.
DEFINE BUFFER b-estnivel6 FOR estrutura.
DEFINE BUFFER b-estnivel7 FOR estrutura.
DEFINE BUFFER b-estnivel8 FOR estrutura.
DEFINE BUFFER b-estnivel9 FOR estrutura.
DEFINE BUFFER b-estnivel10 FOR estrutura.
DEFINE STREAM reldet.
DEFINE WORKFILE wcctempo
  FIELD witpai              LIKE item.it-codigo
  FIELD witfilho            LIKE item.it-codigo
  FIELD wcc                 LIKE grup-maquina.cc-codigo
  FIELD wtempo-homem        LIKE operacao.tempo-homem
  FIELD wproporcao          LIKE operacao.proporcao.
DEFINE VARIABLE wemp        AS CHAR FORMAT "X(40)".
DEFINE VARIABLE wlinha      AS CHAR FORMAT "X(315)".

DEFINE VARIABLE wconf-imp   AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE warq        AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wtipo       AS INTEGER NO-UNDO. /* tipo de impressora */
DEFINE VARIABLE warq1       AS CHAR FORMAT "X(30)".

DEFINE VARIABLE wseq        AS INTEGER FORMAT "99".
DEFINE VARIABLE wlimpa      AS INTEGER FORMAT "99".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-6 RECT-7 westab wfm-ini wfm-fim wit-ini ~
wit-fim wdt-termino wrel w-imp bt-executar bt-sair 
&Scoped-Define DISPLAYED-OBJECTS westab wfm-ini wfm-fim wit-ini wit-fim ~
wdt-termino wrel w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 11 BY 1.13
     FONT 1.

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wdt-termino AS DATE FORMAT "99/99/9999":U 
     LABEL "Data de corte" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 TOOLTIP "O relat�rio verifica tudo com data de t�rmino maior que a data digitada" NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "x(3)":U INITIAL "0" 
     LABEL "Cod.Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE wfm-fim AS CHARACTER FORMAT "X(8)":U 
     LABEL "Fam�lia Final" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE wfm-ini AS CHARACTER FORMAT "X(8)":U 
     LABEL "Fam�lia inicial" 
     VIEW-AS FILL-IN 
     SIZE 12 BY 1 NO-UNDO.

DEFINE VARIABLE wit-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item final" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE wit-ini AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item inicial" 
     VIEW-AS FILL-IN 
     SIZE 21 BY 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(30)":U 
     LABEL "Nome do arquivo" 
     VIEW-AS FILL-IN 
     SIZE 25 BY 1 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 1,
"Impressora", 2,
"Arquivo com compress�o", 3
     SIZE 48 BY 1.13
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 67 BY .75
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 66 BY .75
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     westab AT ROW 2.25 COL 30 COLON-ALIGNED
     wfm-ini AT ROW 3.25 COL 30 COLON-ALIGNED
     wfm-fim AT ROW 4.25 COL 30 COLON-ALIGNED
     wit-ini AT ROW 5.25 COL 30 COLON-ALIGNED
     wit-fim AT ROW 6.25 COL 30 COLON-ALIGNED
     wdt-termino AT ROW 7.25 COL 30 COLON-ALIGNED
     wrel AT ROW 9.75 COL 30 COLON-ALIGNED
     w-imp AT ROW 11.75 COL 56 RIGHT-ALIGNED NO-LABEL
     bt-executar AT ROW 13.25 COL 21
     bt-sair AT ROW 13.25 COL 33
     "maior  ou igual que a data digitada acima." VIEW-AS TEXT
          SIZE 31 BY .67 AT ROW 8.75 COL 25
          FONT 1
     "O relat�rio utiliza opera��o com data de t�rmino" VIEW-AS TEXT
          SIZE 32 BY .67 AT ROW 8.25 COL 25
          FONT 1
     RECT-6 AT ROW 1 COL 1
     RECT-7 AT ROW 11 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 67.29 BY 14.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "TOTAL TEMPO HOMEM - P/ITEM E C.C. POR HORA - wpe0105"
         HEIGHT             = 14
         WIDTH              = 67.29
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* TOTAL TEMPO HOMEM - P/ITEM E C.C. POR HORA - wpe0105 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* TOTAL TEMPO HOMEM - P/ITEM E C.C. POR HORA - wpe0105 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  FOR EACH wcctempo:
    DELETE wcctempo.
    END.
  ASSIGN w-imp.
  ASSIGN wfm-ini wfm-fim wit-ini wit-fim westab
         wdt-termino wrel.
  DISABLE ALL WITH FRAME  {&FRAME-NAME}.
  CASE w-imp:
    WHEN 1 THEN DO:
      OUTPUT TO VALUE(wrel) PAGE-SIZE 44.
      RUN geracao.
      RUN ARQUIVO.
      END. 
     
     WHEN 2 THEN DO:
     /* impressora */
        SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
        IF wconf-imp = YES THEN DO:
           OUTPUT TO PRINTER PAGE-SIZE 44.
          {wpsf/imp.p}
          PUT CONTROL wcomprime wpaisagem a4. 
          RUN geracao.
          RUN ARQUIVO.
          END.
        END.
      WHEN 3 THEN DO:
       RUN comprime-arquivo.
       END.
     END CASE. 
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE arquivo C-Win 
PROCEDURE arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
FORM
  SKIP(1) "Familia inicial:" AT 22 wfm-ini
  SKIP    "Familia final  :" AT 22 wfm-fim
  SKIP    "Item inicial   :" AT 22 wit-ini
  SKIP    "Item final     :" AT 22 wit-fim
  SKIP    "Data de corte  :" AT 22 wdt-termino
  SKIP(1) "                             *** CONSIDERACOES ***"
  SKIP    " 1 - Este programa considera a data de termino maior or igual a data"
          "     digitada no parametro acima."
  SKIP    " 2 - Este programa considera ate 6 niveis da estrutura conforme pre'"
          "definido"
  SKIP    "     na confeccao do mesmo."
  SKIP    " 3 - Somente serao listados os itens ativos (codigo obsoleto = 0)."
  SKIP(1)
  WITH FRAME f-param row 04 NO-LABELS WIDTH 80.

/* ARQUIVO */
FORM 
  item.fm-codigo  
  wcctempo.witpai item.un  
  ITEM.descricao-1 
  wwcc[1]  AT 65   wquant[1] 
  wwcc[2]     wquant[2] 
  wwcc[3]     wquant[3] 
  wwcc[4]     wquant[4]      
  wwcc[5]     wquant[5]        
  wwcc[6]     wquant[6]     
  wwcc[7]     wquant[7]      
  wwcc[8]     wquant[8]
  wwcc[9]     wquant[9]   
  wwcc[10]    wquant[10]
  skip 
  wwcc[11]  AT 65  wquant[11]       
  wwcc[12]    wquant[12]     
  wwcc[13]    wquant[13]      
  wwcc[14]    wquant[14]       
  wwcc[15]    wquant[15]       
  wwcc[16]    wquant[16]      
  wwcc[17]    wquant[17]      
  wwcc[18]    wquant[18]      
  wwcc[19]    wquant[19]      
  wwcc[20]    wquant[20]   
  SKIP 
  wlinha NO-LABEL 
  HEADER
  wemp
  TODAY FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS") SPACE(1) "HS"  
  "Pag.:" PAGE-NUMBER FORMAT "999"
  SKIP
  "RELATORIO DE TOTAIS DE TEMPO HOMEM POR ITEM E POR C.C."            
  "- Familia:  " wfm-ini " a " wfm-fim " - Item: " wit-ini " a " wit-fim
  SKIP(1)
  "Familia"
  "Item"      AT 14
  "UN"        AT 32
  "Descricao" AT 38
  "CC."       AT 65
  "T. Hs."    AT 80
  "CC."       AT 89
  "T. Hs."    AT 104
  "CC."       AT 114
  "T. Hs."    AT 129
  "CC."       AT 139
  "T. Hs."    AT 154
  "CC."       AT 163
  "T. Hs."    AT 179
  "CC."       AT 188
  "T. Hs."    AT 203
  "CC."       AT 213
  "T. Hs."    AT 228
  "CC."       AT 238
  "T. Hs."    AT 253
  "CC."       AT 262
  "T. Hs."    AT 277
  "CC."      AT 287
  "T. Hs."   AT 302 
  WITH FRAME f-arquivo DOWN WIDTH 600 NO-LABEL.
 /* RELAT�RIO */
 FORM 
  item.fm-codigo  
  wcctempo.witpai item.un  
  ITEM.descricao-1 
  "CC."             AT 63
  "T. Hs."          AT 78
  "CC."             AT 87
  "T. Hs."          AT 102
  "CC."             AT 112
  "T. Hs."          AT 127
  "CC."             AT 137
  "T. Hs."          AT 152
  "CC."             AT 161
  "T. Hs."          AT 177
  wwcc[1]  AT 63    wquant[1] 
  wwcc[2]           wquant[2] 
  wwcc[3]           wquant[3] 
  wwcc[4]           wquant[4]      
  wwcc[5]           wquant[5]   
  SKIP(1)
  "CC."             AT 63
  "T. Hs."          AT 78
  "CC."             AT 87
  "T. Hs."          AT 102
  "CC."             AT 112
  "T. Hs."          AT 127
  "CC."             AT 137
  "T. Hs."          AT 152
  "CC."             AT 161
  "T. Hs."          AT 177
  wwcc[6]  AT 63    wquant[6]     
  wwcc[7]           wquant[7]      
  wwcc[8]           wquant[8]
  wwcc[9]           wquant[9]   
  wwcc[10]          wquant[10]
  skip(1)
  "CC."             AT 63
  "T. Hs."          AT 78
  "CC."             AT 87
  "T. Hs."          AT 102
  "CC."             AT 112
  "T. Hs."          AT 127
  "CC."             AT 137
  "T. Hs."          AT 152
  "CC."             AT 161
  "T. Hs."          AT 177
  wwcc[11]  AT 63   wquant[11]       
  wwcc[12]          wquant[12]     
  wwcc[13]          wquant[13]      
  wwcc[14]          wquant[14]       
  wwcc[15]          wquant[15]       
  SKIP(1)
  "CC."             AT 63
  "T. Hs."          AT 78
  "CC."             AT 87
  "T. Hs."          AT 102
  "CC."             AT 112
  "T. Hs."          AT 127
  "CC."             AT 137
  "T. Hs."          AT 152
  "CC."             AT 161
  "T. Hs."          AT 177
  wwcc[16]  AT 63   wquant[16]      
  wwcc[17]          wquant[17]      
  wwcc[18]          wquant[18]      
  wwcc[19]          wquant[19]      
  wwcc[20]          wquant[20]   
  SKIP 
  wlinha NO-LABEL 
  HEADER
  wemp
  TODAY FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS") SPACE(1) "HS"  
  "Pag.:" PAGE-NUMBER FORMAT "999"
  SKIP
  "RELATORIO DE TOTAIS DE TEMPO HOMEM POR ITEM E POR C.C."            
  "- Familia:  " wfm-ini " a " wfm-fim " - Item:  " wit-ini " a " wit-fim
  SKIP(1)
  "Familia"
  "Item"        AT 14
  "UN"          AT 32
  "Descricao"   AT 38
 /*
  "CC."       AT 188
  "Qtd."      AT 203
  "CC."       AT 213
  "Qtd."      AT 228
  "CC."       AT 238
  "Qtd."      AT 253
  "CC."       AT 262
  "Qtd."      AT 277
  "CC."      AT 287
  "Qtd."     AT 302 
  */
  WITH FRAME f-impressora DOWN WIDTH 600 NO-LABEL.


/* GERACAO DO ARQUIVO  */
  /*OUTPUT TO VALUE(warq1).*/
  FOR EACH wcctempo NO-LOCK BREAK BY wcctempo.witpai BY wcctempo.wcc: 
   IF FIRST-OF(wcctempo.witpai) THEN DO: 

     /*
      IF wlabel > 08 THEN MESSAGE "ITEM POSSUI MAIS QUE 07 N�VEIS DE ESTRUTURA!"
                                  SKIP "*** GERA��O DO RELAT�RIO COM PROBLEMA. ***"
                                  wcctempo.witpai VIEW-AS ALERT-BOX.
      */
      ASSIGN wcont  = 0.
             wlabel = 0.
      
      END.
    
      IF FIRST-OF(wcctempo.wcc) THEN DO:
        ASSIGN wtotcc = 0.
        FIND FIRST item WHERE item.it-codigo = wcctempo.witpai NO-LOCK NO-ERROR.
        END.
      ASSIGN wtotcc = wtotcc + wcctempo.wtempo-homem.
    
      IF LAST-OF(wcctempo.wcc) THEN DO:
        wlabel = wlabel + 1.
        ASSIGN wwcc[wlabel]    = wcctempo.wcc.
        IF wproporcao =  100 THEN DO:
           ASSIGN wquant[wlabel]  = wtotcc / 60.
           END.
        ELSE DO: 
           ASSIGN wquant[wlabel]  = ((wtotcc * wproporcao) / 100) / 60.
           END.
        END.       
      IF LAST-OF(wcctempo.witpai) THEN DO:
        ASSIGN wwcc[1]   = string(INTEGER(wwcc[1]),"999999")
               wwcc[2]   = string(INTEGER(wwcc[2]),"999999")
               wwcc[3]   = string(INTEGER(wwcc[3]),"999999")
               wwcc[4]   = string(INTEGER(wwcc[4]),"999999")
               wwcc[5]   = string(INTEGER(wwcc[5]),"999999")
               wwcc[6]   = string(INTEGER(wwcc[6]),"999999")
               wwcc[7]   = string(INTEGER(wwcc[7]),"999999")
               wwcc[8]   = string(INTEGER(wwcc[8]),"999999")
               wwcc[9]   = string(INTEGER(wwcc[9]),"999999")
               wwcc[10]  = string(INTEGER(wwcc[10]),"999999")
               wwcc[11]  = string(INTEGER(wwcc[11]),"999999")
               wwcc[12]  = string(INTEGER(wwcc[12]),"999999")
               wwcc[13]  = string(INTEGER(wwcc[13]),"999999")
               wwcc[14]  = string(INTEGER(wwcc[14]),"999999")
               wwcc[15]  = string(INTEGER(wwcc[15]),"999999")
               wwcc[16]  = string(INTEGER(wwcc[16]),"999999")
               wwcc[17]  = string(INTEGER(wwcc[17]),"999999")
               wwcc[18]  = string(INTEGER(wwcc[18]),"999999")
               wwcc[19]  = string(INTEGER(wwcc[19]),"999999")
               wwcc[20]  = string(INTEGER(wwcc[20]),"999999").
        IF w-imp = 1 THEN DO:
          /* inicio geracao arquivo */
          DISPLAY item.fm-codigo   COLUMN-LABEL "Familia"
                wcctempo.witpai  COLUMN-LABEL "Item"     
                item.un          COLUMN-LABEL "UN"
                item.descricao-1 COLUMN-LABEL "Descricao" 
                wwcc             COLUMN-LABEL "C.C."
                wquant           COLUMN-LABEL "Qtd."      
                wlinha
                WITH FRAME f-arquivo.  
          DOWN WITH FRAME f-arquivo WIDTH 500.
          /* inicio geracao arquivo */
          END.
         IF w-imp = 2
         OR w-imp = 3 THEN DO:
           /* inicio impressao */
           DISPLAY item.fm-codigo   COLUMN-LABEL "Familia"
                wcctempo.witpai  COLUMN-LABEL "Item"     
                item.un          COLUMN-LABEL "UN"
                item.descricao-1 COLUMN-LABEL "Descricao" 
                wwcc             COLUMN-LABEL "C.C."
                wquant           COLUMN-LABEL "Qtd."      
                wlinha
                WITH FRAME f-impressora.  
           DOWN WITH FRAME f-impressora WIDTH 500.
           END.
           /* fim impressao */
         END.
      END.
      OUTPUT CLOSE.
    END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo C-Win 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
 DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
 DEFINE VARIABLE a4 AS CHAR.


RUN ../especificos/wpsf/wp9901.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    END.
/* HP */
IF wtipo = 2 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
OUTPUT TO value(wrel) page-size 44.
PUT CONTROL wcomprime wpaisagem a4. 
RUN geracao.
RUN arquivo.
  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY westab wfm-ini wfm-fim wit-ini wit-fim wdt-termino wrel w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-6 RECT-7 westab wfm-ini wfm-fim wit-ini wit-fim wdt-termino wrel 
         w-imp bt-executar bt-sair 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE geracao C-Win 
PROCEDURE geracao :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

 /*FIND FIRST empresa NO-LOCK NO-ERROR.*/
  FOR EACH item WHERE item.fm-codigo GE wfm-ini
                AND   item.fm-codigo LE wfm-fim 
                AND   item.it-codigo GE wit-ini
                AND   item.it-codigo LE wit-fim
                AND   item.cod-obsoleto  = 1
                AND   ITEM.cod-estabel   = westab
                NO-LOCK:
   FOR EACH operacao OF item 
                      WHERE data-termino  GE wdt-termino
                      NO-LOCK BREAK 
                      BY operacao.it-codigo 
                      BY operacao.gm-codigo:
      /*
      FOR EACH grup-maquina OF operacao 
                            NO-LOCK 
                            BREAK BY grup-maquina.cc-codigo:
        */
      FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
     
        CREATE wcctempo.
        ASSIGN witpai                = item.it-codigo
               witfilho              = operacao.it-codigo
               wcctempo.wcc          = gm-estab.cc-codigo
               wcctempo.wtempo-homem = operacao.tempo-homem
               wproporcao            = operacao.proporcao.
        
        END.
      END.    
      /* controle de niveis */
      FOR EACH estrutura  WHERE estrutura.it-codigo = item.it-codigo 
                         AND   estrutura.data-termino GE TODAY 
                         NO-LOCK BREAK BY estrutura.it-codigo :
         IF FIRST-OF(estrutura.it-codigo)  THEN ASSIGN wseq = 0.
         ASSIGN wseq = wseq + 1.
         IF LASt-OF(estrutura.it-codigo) THEN  DO: 
           IF wseq > 10 THEN MESSAGE
           "Estouro de niveis de estrutura tratado pelo relat�rio !"
           SKIP 
           "Item:" estrutura.it-codigo  VIEW-AS ALERT-BOX.
           END.
        END.
      /* NIVEL 1 */
      FOR EACH estrutura WHERE estrutura.it-codigo = item.it-codigo 
                         AND   estrutura.data-termino GE TODAY NO-LOCK:
        FOR EACH operacao WHERE operacao.it-codigo = estrutura.es-codigo
                          AND   operacao.data-termino GE TODAY
                          NO-LOCK BREAK BY operacao.it-codigo 
                                      BY operacao.gm-codigo:
          /*FOR EACH grup-maquina OF operacao 
                                NO-LOCK 
                                BREAK BY grup-maquina.cc-codigo:
            */
          FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
            CREATE wcctempo.
            ASSIGN witpai                = item.it-codigo
                   witfilho              = operacao.it-codigo
                   wcctempo.wcc          = gm-estab.cc-codigo
                   wcctempo.wtempo-homem = operacao.tempo-homem
                   wproporcao            = operacao.proporcao.  
             
            END.
           
          END.
        /* NIVEL 2 */
        FOR EACH b-estnivel2 WHERE b-estnivel2.it-codigo = 
                                   estrutura.es-codigo
                             AND   b-estnivel2.data-termino GE TODAY 
                             NO-LOCK:
          FOR EACH operacao WHERE operacao.it-codigo = b-estnivel2.es-codigo
                            AND   operacao.data-termino GE TODAY
                            NO-LOCK BREAK BY operacao.it-codigo 
                                        BY operacao.gm-codigo:
            /*FOR EACH grup-maquina OF operacao 
                                  NO-LOCK BREAK BY grup-maquina.cc-codigo:
                                  */
            FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
              CREATE wcctempo. 
              ASSIGN witpai                = item.it-codigo
                     witfilho              = operacao.it-codigo
                     wcctempo.wcc          = gm-estab.cc-codigo
                     wcctempo.wtempo-homem = operacao.tempo-homem
                     wproporcao            = operacao.proporcao.
              END.
            END.
          /* NIVEL 3 */
          FOR EACH b-estnivel3 WHERE b-estnivel3.it-codigo = 
                                     b-estnivel2.es-codigo 
                               AND   b-estnivel3.data-termino GE TODAY 
                               NO-LOCK:
            FOR EACH operacao WHERE operacao.it-codigo = b-estnivel3.es-codigo
                              AND   operacao.data-termino GE TODAY
                              NO-LOCK BREAK BY operacao.it-codigo 
                                            BY operacao.gm-codigo:
                /*
                FOR EACH grup-maquina OF operacao 
                                    NO-LOCK 
                                    BREAK BY grup-maquina.cc-codigo:
                                    */
                FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                CREATE wcctempo.
                ASSIGN wcctempo.witpai       = item.it-codigo
                       witfilho              = operacao.it-codigo
                       wcctempo.wcc          = gm-estab.cc-codigo
                       wcctempo.wtempo-homem = operacao.tempo-homem
                       wproporcao            = operacao.proporcao.
                END.
              END.
            /* NIVEL 4 */
            FOR EACH b-estnivel4 WHERE b-estnivel4.it-codigo = 
                                       b-estnivel3.es-codigo 
                                 AND   b-estnivel4.data-termino GE TODAY 
                                 NO-LOCK:
              FOR EACH operacao WHERE operacao.it-codigo = b-estnivel4.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                /*
                FOR EACH grup-maquina OF operacao 
                                      NO-LOCK 
                                      BREAK BY grup-maquina.cc-codigo:
                                      */
                FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                  CREATE wcctempo.
                  ASSIGN wcctempo.witpai       = item.it-codigo
                         wcctempo.witfilho     = operacao.it-codigo
                         wcctempo.wcc          = gm-estab.cc-codigo
                         wcctempo.wtempo-homem = operacao.tempo-homem
                         wproporcao            = operacao.proporcao.
                  END.
                END.
              /* NIVEL 5 */
              FOR EACH b-estnivel5 WHERE b-estnivel5.it-codigo = 
                                         b-estnivel4.es-codigo 
                                   AND   b-estnivel5.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel5.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                  /*FOR EACH grup-maquina OF operacao 
                                        NO-LOCK 
                                        BREAK BY grup-maquina.cc-codigo:*/
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
         
              /* NIVEL  6*/
              FOR EACH b-estnivel6 WHERE b-estnivel6.it-codigo = 
                                         b-estnivel5.es-codigo 
                                   AND   b-estnivel6.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel6.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                  /*FOR EACH grup-maquina OF operacao 
                                        NO-LOCK 
                                        BREAK BY grup-maquina.cc-codigo:*/
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              END.
         
            /* NIVEL  7*/
              FOR EACH b-estnivel7 WHERE b-estnivel7.it-codigo = 
                                         b-estnivel6.es-codigo 
                                   AND   b-estnivel7.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel7.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                  /*FOR EACH grup-maquina OF operacao 
                                        NO-LOCK 
                                        BREAK BY grup-maquina.cc-codigo:*/
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              END.
           /* NIVEL  8*/
              FOR EACH b-estnivel8 WHERE b-estnivel8.it-codigo = 
                                         b-estnivel7.es-codigo 
                                   AND   b-estnivel8.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel8.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                  /*FOR EACH grup-maquina OF operacao 
                                        NO-LOCK 
                                        BREAK BY grup-maquina.cc-codigo:*/
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              END.
              /* NIVEL  9*/
              FOR EACH b-estnivel9 WHERE b-estnivel9.it-codigo = 
                                         b-estnivel8.es-codigo 
                                   AND   b-estnivel9.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel9.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                  /*FOR EACH grup-maquina OF operacao 
                                        NO-LOCK 
                                        BREAK BY grup-maquina.cc-codigo:*/
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              END.
              /* NIVEL  10*/
              FOR EACH b-estnivel10 WHERE b-estnivel10.it-codigo = 
                                         b-estnivel9.es-codigo 
                                   AND   b-estnivel10.data-termino GE TODAY 
                                   NO-LOCK:
                FOR EACH operacao 
                                WHERE operacao.it-codigo = b-estnivel10.es-codigo
                                AND   operacao.data-termino GE TODAY
                                NO-LOCK BREAK BY operacao.it-codigo 
                                              BY operacao.gm-codigo:
                  /*FOR EACH grup-maquina OF operacao 
                                        NO-LOCK 
                                        BREAK BY grup-maquina.cc-codigo:*/
                    FOR EACH gm-estab WHERE gm-estab.cod-estab = ITEM.cod-estab
                        AND   gm-estab.gm-codigo = operacao.gm-codigo
                        NO-LOCK:
                    CREATE wcctempo.
                    ASSIGN wcctempo.witpai       = item.it-codigo
                           wcctempo.witfilho     = operacao.it-codigo
                           wcctempo.wcc          = gm-estab.cc-codigo
                           wcctempo.wtempo-homem = operacao.tempo-homem
                           wproporcao            = operacao.proporcao.
                    END.
                  END.
              END.

           /*fim dos niveis */
           END.      
          END.
         END.
       END.     
     END.
    END.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 ASSIGN wfm-ini      = ""
         wfm-fim     = "ZZZZZZZZ"
         wit-ini     = " "
         wit-fim     = "ZZZZZZZZZZZZZZZZ"
         wdt-termino = TODAY
         wrel        = "c:/spool/ARQTEMPO.LST"
         warq1       = "c:/spool/TEMPCC.LST"
         wemp        = "SEEBER FASTPLAS LTDA."
         wlinha      = FILL("_",360)
         westab      = "1".
 FOR EACH wcctempo:
    DELETE wcctempo.
    END.
 DISPLAY wfm-ini wfm-fim wit-ini wit-fim westab
         wdt-termino wrel WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE limpa C-Win 
PROCEDURE limpa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DO wlimpa = 1 TO 20:
    ASSIGN wwcc[wlimpa]   = ""
           wquant[wlimpa] = 0.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio C-Win 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* RELATORIO */
/* EMISSAO DO RELATORIO */   
  DISPLAY WITH FRAME f-relatorio PAGE-TOP.
  PUT SCREEN ROW 23 FILL(" ",80).
  FOR EACH wcctempo NO-LOCK BREAK BY wcctempo.witpai BY wcctempo.wcc: 
    PUT SCREEN ROW 23 "Listando item: " + STRING(witpai).

    IF FIRST-OF(wcctempo.witpai) THEN DO: 
      FIND FIRST item WHERE item.it-codigo = wcctempo.witpai NO-LOCK NO-ERROR.
      PUT wcctempo.witpai " " item.un " " item.descricao-1.
      END.
    IF FIRST-OF(wcctempo.wcc) THEN DO:
      ASSIGN wtotccr = 0.
      END.
    
    ASSIGN wtotccr = wtotccr + wcctempo.wtempo-homem.
      IF LAST-OF(wcctempo.wcc) THEN DO:
        ASSIGN wquantr = wtotccr / 60.
        PUT " " wcc AT 41 " " wquantr SKIP.
        END.
      END.
  OUTPUT CLOSE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

