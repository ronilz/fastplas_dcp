&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
/* *** ANTIGO PROGRAMA WP1206 *** */
/* Local Variable Definitions ---                                       */
/* Workfile para controle de saldo */
DEFINE WORKFILE saldo
  FIELD fcod-emitente           LIKE lo-matplano.cod-emitente INITIAL 0
  FIELD femp                    LIKE lo-matplano.emp          INITIAL 0
  FIELD festab                  LIKE lo-matplano.estab        INITIAL ""
  FIELD fnr-pl                  AS CHAR FORMAT "999999X"      INITIAL " "
  FIELD fnro-docto              LIKE lo-matplano.nro-docto    INITIAL 0
  FIELD fit-codigo              LIKE lo-matplano.it-codigo    INITIAL " "
  FIELD fimediato               AS INTE FORMAT "->,>>>,>>9"   INITIAL 0   
  FIELD ftotent                 AS INTE FORMAT ">>>,>>>,>>9"  INITIAL 0
  FIELD fqt-sem1                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem2                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem3                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem4                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    
  FIELD fqt-sem5                AS INTE FORMAT ">,>>>,>>9"    INITIAL 0    

  FIELD fqt-sem1-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem2-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem3-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem4-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fqt-sem5-2              AS INTE FORMAT ">,>>>,>>9"    INITIAL 0   
  FIELD fmes1                   AS INTE FORMAT ">,>>>,>>9"    INITIAL 0
  FIELD fmes2                   AS INTE FORMAT ">,>>>,>>9"    INITIAL 0.
/* Totais */
DEFINE VARIABLE wtotent         AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wresul          AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wtotplano       AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wtottela        AS INTE FORMAT ">>>,>>>,>>9" INITIAL 0.
DEFINE VARIABLE wproxtela       AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wit-ini         LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wit-fim         LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wdatapesq       LIKE recebimento.data-nota    INITIAL ?.
DEFINE VARIABLE wemi            LIKE lo-matplano.cod-emitente INITIAL 0.
DEFINE VARIABLE wit-codigo      LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wit-ant         LIKE lo-matplano.it-codigo    INITIAL " ".
DEFINE VARIABLE wlinha          AS CHAR FORMAT "X(143)".
DEFINE VARIABLE wlinha1         AS CHAR FORMAT "X(143)".
DEFINE VARIABLE wpag            AS INTE FORMAT "9999".
DEFINE VARIABLE wemp            LIKE mgadm.estabel.nome.
DEFINE VARIABLE wnome-ab        LIKE emitente.nome-ab.

DEFINE VARIABLE wconf-imp      AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE warq           AS LOGICAL FORMAT "Sim/Nao".
DEFINE VARIABLE wtipo          AS INTEGER NO-UNDO. /* tipo de impressora */
DEFINE VARIABLE wsaldo-estoq   LIKE saldo-estoq.qtidade-atu NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 RECT-6 wep westab wemi-ini wemi-fim ~
wnr-pl wrel w-imp bt-executar bt-sair-2 
&Scoped-Define DISPLAYED-OBJECTS wep westab wemi-ini wemi-fim wnr-pl wrel ~
w-imp 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 11 BY 1
     FONT 1.

DEFINE BUTTON bt-sair-2 DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1
     BGCOLOR 8 .

DEFINE VARIABLE wemi-fim AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "Cod.Fornecedor final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wemi-ini AS INTEGER FORMAT ">>>>>>9":U INITIAL 0 
     LABEL "Cod.Fornecedor inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wep AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Empresa" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE westab AS CHARACTER FORMAT "X(3)":U INITIAL "0" 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wnr-pl AS CHARACTER FORMAT "999999X":U INITIAL "0" 
     LABEL "M�s e ano inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(25)":U 
     LABEL "Nome do Relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 26 BY .88 NO-UNDO.

DEFINE VARIABLE w-imp AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 1,
"Impressora", 2,
"Arquivo com compress�o", 3
     SIZE 46 BY 1.13 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 77 BY 12.5
     FGCOLOR 8 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 78 BY 1.41
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wep AT ROW 3.5 COL 36 COLON-ALIGNED
     westab AT ROW 4.5 COL 36 COLON-ALIGNED
     wemi-ini AT ROW 5.5 COL 36 COLON-ALIGNED
     wemi-fim AT ROW 6.5 COL 36 COLON-ALIGNED
     wnr-pl AT ROW 7.5 COL 36 COLON-ALIGNED
     wrel AT ROW 9.5 COL 36 COLON-ALIGNED
     w-imp AT ROW 11.75 COL 65 RIGHT-ALIGNED NO-LABEL
     bt-executar AT ROW 14.25 COL 31
     bt-sair-2 AT ROW 14.25 COL 43
     RECT-3 AT ROW 1.25 COL 2
     RECT-6 AT ROW 14 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 79 BY 15
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Saldo MENSAL do Plano de Entrega  - wpp0104"
         HEIGHT             = 15.25
         WIDTH              = 79.29
         MAX-HEIGHT         = 22.88
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.88
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = 15
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* SETTINGS FOR RADIO-SET w-imp IN FRAME DEFAULT-FRAME
   ALIGN-R                                                              */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Saldo MENSAL do Plano de Entrega  - wpp0104 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Saldo MENSAL do Plano de Entrega  - wpp0104 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Executar */
DO:
  ASSIGN w-imp.
  DISABLE ALL WITH FRAME  {&FRAME-NAME}.
  ASSIGN wep westab wemi-ini wemi-fim wnr-pl wrel.
  CASE w-imp:
    WHEN 1 THEN DO:
      OUTPUT TO VALUE(wrel) PAGE-SIZE 66.
      RUN imprimir. 
      END. 
     
     WHEN 2 THEN DO:
     /* impressora */
        SYSTEM-DIALOG PRINTER-SETUP UPDATE wconf-imp.
        IF wconf-imp = YES THEN DO:
           OUTPUT TO PRINTER PAGE-SIZE 44.
          {wpsf/imp.p}
          PUT CONTROL wcomprime wpaisagem a4. 
          RUN imprimir. 
          END.
        END.
      WHEN 3 THEN DO:
       RUN comprime-arquivo.
       END.
     END CASE. 
  ENABLE ALL WITH FRAME  {&FRAME-NAME}.
  MESSAGE "GERA��O CONCLU�DA" VIEW-AS ALERT-BOX. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair-2 C-Win
ON CHOOSE OF bt-sair-2 IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE comprime-arquivo C-Win 
PROCEDURE comprime-arquivo :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 DEFINE VARIABLE wpaisagem  AS CHAR.   /* landscape*/  
 DEFINE VARIABLE wcomprime  AS CHAR.   /* COMPRIMIDO 16.5 */
 DEFINE VARIABLE a4 AS CHAR.


RUN ../especificos/wpsf/wp9901.w (OUTPUT wtipo). 
/* MATRICIAL */
IF wtipo = 1 THEN DO:
    ASSIGN wcomprime = "~017".
    END.
/* HP */
IF wtipo = 2 THEN DO:
    ASSIGN wpaisagem  = "~033~046~154~061~117".   /* landscape*/ 
    ASSIGN wcomprime  = "~033~046~153~062~123".   /* COMPRIMIDO 16.5 */
    ASSIGN A4 = CHR(27) + "&l26A".
   END.
OUTPUT TO value(wrel) page-size 44.
PUT CONTROL wcomprime wpaisagem a4. 
RUN imprimir.                         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_nf C-Win 
PROCEDURE disp_nf :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  CLEAR FRAME f-nf ALL NO-PAUSE.
  ASSIGN wtottela = 0.
    FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     saldo.fcod-emitente
                           AND   integer(recebimento.numero-nota)
                           =     saldo.fnro-docto
                           AND   recebimento.it-codigo
                           =     saldo.fit-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               saldo.fcod-emitente
                         AND   recebimento.it-codigo    =
                               saldo.fit-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   integer(recebimento.numero-nota) GT
                               saldo.fnro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
      
      ASSIGN wtottela = wtottela + recebimento.quant-receb.           
      HIDE MESSAGE NO-PAUSE.
      DISPLAY recebimento.it-codigo recebimento.numero-nota 
              recebimento.data-nota  recebimento.quant-receb
              COLUMN-LABEL "Quantidade" 
              wtottela LABEL "TOTAL ENTRADA"
              WITH FRAME f-nf CENTERED DOWN ROW 10 OVERLAY.
      DOWN WITH FRAME f-nf.
      END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wep westab wemi-ini wemi-fim wnr-pl wrel w-imp 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-3 RECT-6 wep westab wemi-ini wemi-fim wnr-pl wrel w-imp 
         bt-executar bt-sair-2 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE imprimir C-Win 
PROCEDURE imprimir :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Form do Saldo Relatorio --------------------------------------------------*/
FORM 
  "Cod.Item          UltProg       NfCorte   TOT.ENTRADA        ATRASO"
  lo-matdatas.titsem1  AT 78
  lo-matdatas.titsem2  AT 92
  lo-matdatas.titsem3  AT 106     
  lo-matdatas.titsem4  AT 120
  lo-matdatas.titsem5  AT 134
  SKIP
  /*"__________________ ________ _______ ___________ __________" */
  "_________" AT 78
  "_________" AT 92
  "_________" AT 106
  "_________" AT 120
  "_________" AT 134 

  SKIP
  saldo.fit-codigo " "
  saldo.fnr-pl  " "
  saldo.fnro-docto  
  saldo.ftotent
  saldo.fimediato  
  saldo.fqt-sem1  AT 78
  saldo.fqt-sem2  AT 92
  saldo.fqt-sem3  AT 106
  saldo.fqt-sem4  AT 120
  saldo.fqt-sem5  AT 134
  SKIP
  item.descricao-1
  lo-matdatas.titsem1-2 AT 78
  lo-matdatas.titsem2-2 AT 92
  lo-matdatas.titsem3-2 AT 106
  lo-matdatas.titsem4-2 AT 120      
  lo-matdatas.titsem5-2 AT 134
  SKIP
 "Saldo em estoque no dia: "wsaldo-estoq 
  "_________"   AT 78
  "__________"  AT 92
  "_________"   AT 106
  "_________"   AT 120
  "_________"   AT 134
  
  SKIP
  saldo.fqt-sem1-2 AT 78
  saldo.fqt-sem2-2 
  saldo.fqt-sem3-2
  saldo.fqt-sem4-2 
  saldo.fqt-sem5-2 
  SKIP
  /*09/05/06 - Altera��o efetuado conf.solicita��o da Rosa,
  para n�o dar display das previs�es dos meses 
  lo-matdatas.titmes1   AT 78
  lo-matdatas.titmes2   AT 92  
  SKIP
  "_________"   AT 78
  "_________"   AT 92
  SKIP
  saldo.fmes1   AT 78
  saldo.fmes2   AT 92 
  wlinha 
  SKIP(1)*/
  wlinha1
  HEADER
  "**** S A L D O    M E N S A L ****" AT 55 
  wlinha
  SKIP
  wemp      
  TODAY FORMAT "99/99/9999"
  string(time,"HH:MM") "Hs"
  "Pag.:" AT 100 PAGE-NUMBER FORMAT ">>99"  
  /*SKIP(1)*/
  SKIP
  "*** SALDO DO PLANO DE ENTREGA DE MATERIAIS NO."  AT 37
  wnr-pl FORMAT "99/9999X" "***"
  SKIP
  "Empresa:" AT 32 wep "Estabelecimento:" westab 
  "Fornecedor:"  wemi
  wnome-ab
  SKIP
  WITH FRAME f-saldo NO-LABELS DOWN WIDTH 147 NO-BOX.
FOR EACH lo-matplano WHERE lo-matplano.cod-emitente GE wemi-ini
                       AND   lo-matplano.cod-emitente LE wemi-fim
                       AND   lo-matplano.emp          = wep
                       AND   lo-matplano.estab        = westab
                       AND   lo-matplano.nr-pl        BEGINS  wnr-pl
                       NO-LOCK BREAK BY lo-matplano.cod-emitente
                       BY lo-matplano.it-codigo 
                       BY lo-matplano.nr-pl:
    PUT SCREEN ROW 22   "Fornecedor: " + STRING(lo-matplano.cod-emitente)
                      + " Item: " + STRING(lo-matplano.it-codigo). 
    ASSIGN wtotent   = 0
           wresul    = 0
           wdatapesq = ?.

    IF LAST-OF(lo-matplano.it-codigo) THEN DO:    
    /* Cria workfile saldo */
    CREATE saldo.
    ASSIGN saldo.fcod-emitente = lo-matplano.cod-emitente
           saldo.fnr-pl        = lo-matplano.nr-pl
           saldo.femp          = lo-matplano.emp
           saldo.festab        = lo-matplano.estab
           saldo.fit-codigo    = lo-matplano.it-codigo
           saldo.fnro-docto    = lo-matplano.nro-docto
           saldo.fimediato     = lo-matplano.imediato
           saldo.fqt-sem1      = lo-matplano.qt-sem1
           saldo.fqt-sem2      = lo-matplano.qt-sem2
           saldo.fqt-sem3      = lo-matplano.qt-sem3
           saldo.fqt-sem4      = lo-matplano.qt-sem4
           saldo.fqt-sem5      = lo-matplano.qt-sem5
           saldo.fqt-sem1-2    = lo-matplano.qt-sem1-2
           saldo.fqt-sem2-2    = lo-matplano.qt-sem2-2
           saldo.fqt-sem3-2    = lo-matplano.qt-sem3-2
           saldo.fqt-sem4-2    = lo-matplano.qt-sem4-2
           saldo.fqt-sem5-2    = lo-matplano.qt-sem5-2.
          /* inibido conf. solicitacao da Rosa - 09/05/06
           saldo.fmes1         = lo-matplano.mes1   
           saldo.fmes2         = lo-matplano.mes2.   */    

    /* Procura / Soma Total de Entradas ------------------------------------*/
    FIND FIRST recebimento WHERE recebimento.cod-emitente
                           =     lo-matplano.cod-emitente
                           AND   INTEGER(recebimento.numero-nota)
                           =     lo-matplano.nro-docto
                           AND   recebimento.it-codigo
                           =     lo-matplano.it-codigo
                           NO-LOCK NO-ERROR.

    IF AVAILABLE recebimento THEN ASSIGN wdatapesq = recebimento.data-nota.
    IF NOT AVAILABLE recebimento THEN ASSIGN wdatapesq = ?.
    FOR EACH recebimento WHERE recebimento.cod-emitente = 
                               lo-matplano.cod-emitente
                         AND   recebimento.it-codigo    =
                               lo-matplano.it-codigo
                         AND   recebimento.data-nota    GE
                               wdatapesq
                         AND   INTEGER(recebimento.numero-nota)  GT
                               lo-matplano.nro-docto
                         AND   recebimento.cod-movto    = 1
                         NO-LOCK BREAK BY recebimento.data-nota 
                         BY recebimento.numero-nota:
       ASSIGN wtotent = wtotent + quant-receb.
       ASSIGN saldo.ftotent = wtotent.
       END. 
     
     
     /***** MES 1 ABERTO *****/
     /* ATRASO */
     ASSIGN wresul = (lo-matplano.imediato)
                   - wtotent.
     IF wresul LE 0 THEN ASSIGN fimediato = 0.
     ELSE IF wresul < imediato THEN ASSIGN fimediato = wresul.

     /* Mes 1 aberto - 1a. semana */
     ASSIGN wresul = (lo-matplano.imediato + lo-matplano.qt-sem1) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem1 = 0.
     ELSE IF wresul < fqt-sem1 THEN ASSIGN fqt-sem1 = wresul.
   
    /* Mes 1 aberto - 2a. semana */
     ASSIGN wresul = (lo-matplano.imediato + lo-matplano.qt-sem1 +
                      lo-matplano.qt-sem2)  - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem2 = 0.
     ELSE IF wresul < fqt-sem2 THEN ASSIGN fqt-sem2 = wresul.
    
    /* Mes 1 aberto - 3a. semana */
     ASSIGN wresul = (lo-matplano.imediato 
                   + lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   + lo-matplano.qt-sem3) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem3 = 0.
     ELSE IF wresul < fqt-sem3 THEN ASSIGN fqt-sem3 = wresul.

     /* Mes 1 aberto - 4a. semana */
     ASSIGN wresul = (lo-matplano.imediato 
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem4 = 0.
     ELSE IF wresul < fqt-sem4 THEN ASSIGN fqt-sem4 = wresul.

     /* Mes 1 aberto - 5a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem5 = 0.
     ELSE IF wresul < fqt-sem5 THEN ASSIGN fqt-sem5 = wresul.

     /* Mes 2 aberto - 1a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem1-2 = 0.
     ELSE IF wresul < fqt-sem1-2 THEN ASSIGN fqt-sem1-2 = wresul.
    
     /* Mes 2 aberto - 2a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2) - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem2-2 = 0.
     ELSE IF wresul < fqt-sem2-2 THEN ASSIGN fqt-sem2-2 = wresul.
     
     /* Mes 2 aberto - 3a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem3-2 = 0.
     ELSE IF wresul < fqt-sem3-2 THEN ASSIGN fqt-sem3-2 = wresul.

     /* Mes 2 aberto - 4a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem4-2 = 0.
     ELSE IF wresul < fqt-sem4-2 THEN ASSIGN fqt-sem4-2 = wresul.

     /* Mes 2 aberto - 5a. semana */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2) 
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fqt-sem5-2 = 0.
     ELSE IF wresul < fqt-sem5-2 THEN ASSIGN fqt-sem5-2 = wresul.

     /* Mes 1 FECHADO */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2 
                   +  lo-matplano.mes1)
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fmes1 = 0.
     ELSE IF wresul < fmes1 THEN ASSIGN fmes1 = wresul.

     /* Mes 2 FECHADO */
     ASSIGN wresul = (lo-matplano.imediato
                   +  lo-matplano.qt-sem1 + lo-matplano.qt-sem2
                   +  lo-matplano.qt-sem3 + lo-matplano.qt-sem4
                   +  lo-matplano.qt-sem5 + lo-matplano.qt-sem1-2
                   +  lo-matplano.qt-sem2-2 + lo-matplano.qt-sem3-2
                   +  lo-matplano.qt-sem4-2 + lo-matplano.qt-sem5-2 
                   +  lo-matplano.mes1 + lo-matplano.mes2)
                   - wtotent.
     IF  wresul LE 0 THEN ASSIGN fmes2 = 0.
     ELSE IF wresul < fmes2 THEN ASSIGN fmes2 = wresul.
     
     ASSIGN wtotplano = (lo-matplano.imediato 
               + lo-matplano.qt-sem1 + lo-matplano.qt-sem2 
               + lo-matplano.qt-sem3 + lo-matplano.qt-sem4
               + lo-matplano.qt-sem1-2 + lo-matplano.qt-sem2-2
               + lo-matplano.qt-sem3-2 + lo-matplano.qt-sem4-2
               + lo-matplano.qt-sem5-2 + lo-matplano.mes1
               + lo-matplano.mes2).
     
     IF wtotent > wtotplano THEN 
     ASSIGN fimediato = (wtotplano - wtotent).
   END.
 END.

/* DISPLAY DOS DADOS DO SALDO ----------------------------------------------*/
DO:
  HIDE MESSAGE NO-PAUSE.
  FIND FIRST mgadm.estabel WHERE estabel.ep-codigo = string(wep)
                     AND   estabel.cod-estabel = westab
                     NO-LOCK NO-ERROR.
  ASSIGN wemp     = estabel.nome.
    FOR EACH saldo WHERE saldo.fcod-emitente GE wemi-ini
                   AND   saldo.fcod-emitente LE wemi-fim
                   AND   saldo.femp          = wep
                   AND   saldo.festab        = westab
                   AND   saldo.fnr-pl        BEGINS  wnr-pl
                   NO-LOCK
                   BREAK BY saldo.fcod-emitente
                   BY saldo.fit-codigo:
      
      FIND FIRST emitente WHERE emitente.cod-emitente = saldo.fcod-emitente 
                          NO-LOCK NO-ERROR.
      ASSIGN wnome-ab     = emitente.nome-ab
             wsaldo-estoq = 0.
      IF FIRST-OF(saldo.fcod-emitente) THEN DO:
        ASSIGN wemi = saldo.fcod-emitente.
        PAGE.
        END.
      FIND FIRST lo-matdatas WHERE lo-matdatas.nr-pl
                             =     SUBSTRING(saldo.fnr-pl,1,6)
                             AND   lo-matdatas.emp   = saldo.femp
                             AND   lo-matdatas.estab = saldo.festab
                             NO-LOCK.
      FIND FIRST item    WHERE item.it-codigo = saldo.fit-codigo 
                         NO-LOCK NO-ERROR. 
      FOR EACH saldo-estoq WHERE saldo-estoq.cod-depos = "ALM" 
                           AND   saldo-estoq.it-codigo = item.it-codigo
                           NO-LOCK:
        ASSIGN wsaldo-estoq = wsaldo-estoq 
                            + (saldo-estoq.qtidade-atu - saldo-estoq.qt-alocada).
        END.
        DISPLAY /* linha 1 */
            wlinha1
            lo-matdatas.titsem1 lo-matdatas.titsem2 lo-matdatas.titsem3 
            lo-matdatas.titsem4 lo-matdatas.titsem5
            saldo.fit-codigo saldo.fnr-pl saldo.fnro-docto saldo.ftotent
            saldo.fimediato 
            saldo.fqt-sem1 saldo.fqt-sem2 saldo.fqt-sem3
            saldo.fqt-sem4 saldo.fqt-sem5 
            /* linha 2 */
            item.descricao-1 wsaldo-estoq
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2
            lo-matdatas.titsem5-2 
            /* - inibido conf.solicitacao rosa 09/05/06
            lo-matdatas.titmes1 lo-matdatas.titmes2*/
            saldo.fqt-sem1-2 saldo.fqt-sem2-2 saldo.fqt-sem3-2
            saldo.fqt-sem4-2 saldo.fqt-sem5-2 
            /* - inibido conf.solicitacao rosa 09/05/06
            saldo.fmes1
            saldo.fmes2 
            wlinha   */ 
        WITH FRAME f-saldo OVERLAY NO-LABELS DOWN.
        DOWN WITH FRAME f-saldo.
        END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   ASSIGN wep           = 1
          westab        = "1"
          wemi-ini      = 0
          wemi-fim      = 99999
          wnr-pl        = ""
          wrel          = "V:\spool\SALDOMENSAL.TXT"
          wlinha        = FILL("_",143)
          wlinha1       = FILL("_",143)
          wsaldo-estoq  = 0.
   DISPLAY wep westab wemi-ini wemi-fim wnr-pl wrel
          WITH FRAME {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE it_video C-Win 
PROCEDURE it_video :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 /*DISPLAY /* linha 1 */
            lo-matdatas.titsem1 lo-matdatas.titsem2 lo-matdatas.titsem3 
            lo-matdatas.titsem4 lo-matdatas.titsem5
            saldo.fit-codigo @ wit-codigo saldo.fnro-docto saldo.ftotent
            saldo.fimediato 
            saldo.fqt-sem1 saldo.fqt-sem2 saldo.fqt-sem3
            saldo.fqt-sem4 saldo.fqt-sem5 
            /* linha 2 */
            item.descricao-1 
            lo-matdatas.titsem1-2 lo-matdatas.titsem2-2 
            lo-matdatas.titsem3-2 lo-matdatas.titsem4-2
            lo-matdatas.titsem5-2 lo-matdatas.titmes1 lo-matdatas.titmes2
            saldo.fqt-sem1-2 saldo.fqt-sem2-2 saldo.fqt-sem3-2
            saldo.fqt-sem4-2 saldo.fqt-sem5-2 saldo.fmes1
            saldo.fmes2 
            wlinha    
            WITH FRAME f-salvideo NO-LABELS WIDTH 125.
            DOWN WITH FRAME f-salvideo.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

