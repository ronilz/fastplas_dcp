&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE wconf AS LOGICAL FORMAT "Sim/N�o" INITIAL NO.

Form
    ped-venda.dt-implant      COLUMN-LABEL "Dt.Implant."
    ped-venda.cod-emitente    COLUMN-LABEL "Cod.Emit."
    ped-venda.nome-abrev      COLUMN-LABEL "Emitente"
    ped-venda.nr-pedido       COLUMN-LABEL "Nr.Ped."
    ped-venda.nr-pedcli       COLUMN-LABEL "Ped.Cli."
    ped-venda.vl-tot-ped      COLUMN-LABEL "Vlr.Tot.Ped."
    ped-venda.cod-cond-pag    COLUMN-LABEL "Cond.Pagto"
    WITH FRAME f-lista WIDTH 130 DOWN STREAM-IO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS BUTTON-5 BUTTON-7 wdt-implant-ini ~
wdt-implant-fim wcod-emitente-ini wcod-emitente-fim wvl-tot-ped wrel ~
wcod-cond-pag-para bt-executar-2 bt-executar bt-cancelar bt-ajuda IMAGE-1 ~
IMAGE-10 IMAGE-11 IMAGE-12 IMAGE-2 IMAGE-7 IMAGE-8 IMAGE-9 RECT-1 RECT-3 ~
RECT-4 RECT-7 
&Scoped-Define DISPLAYED-OBJECTS wdt-implant-ini wdt-implant-fim ~
wcod-emitente-ini wcod-emitente-fim wvl-tot-ped wrel wcod-cond-pag-para 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Fechar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-executar 
     LABEL "Alterar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-executar-2 
     LABEL "Relat�rio" 
     SIZE 10 BY 1.

DEFINE BUTTON BUTTON-5 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Button 5" 
     SIZE 4 BY 1.13.

DEFINE BUTTON BUTTON-7 
     IMAGE-UP FILE "image/im-hel.bmp":U
     LABEL "Button 7" 
     SIZE 5 BY 1.13.

DEFINE VARIABLE wcod-cond-pag-para AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "NOVA CONDI��O DE PAGAMENTO" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88
     FONT 0 NO-UNDO.

DEFINE VARIABLE wcod-emitente-fim AS INTEGER FORMAT ">>>,>>>,>>9":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wcod-emitente-ini AS INTEGER FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "C�digo Emitente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wdt-implant-fim AS DATE FORMAT "99/99/9999":U 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wdt-implant-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Data implanta��o do pedido" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(40)":U 
     LABEL "Nome do Arquivo" 
     VIEW-AS FILL-IN 
     SIZE 26 BY .88 NO-UNDO.

DEFINE VARIABLE wvl-tot-ped AS DECIMAL FORMAT "->>,>>9.99":U INITIAL 0 
     LABEL "PEDIDOS COM VALOR SUPERIOR A" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88
     BGCOLOR 15 FONT 0 NO-UNDO.

DEFINE IMAGE IMAGE-1
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-10
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-11
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-12
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-2
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-7
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-8
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-9
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1.5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 87 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 3 GRAPHIC-EDGE  NO-FILL 
     SIZE 87 BY 1.25.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 84 BY 3.25.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 87 BY 13.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     BUTTON-5 AT ROW 2.25 COL 79
     BUTTON-7 AT ROW 2.25 COL 83
     wdt-implant-ini AT ROW 5.25 COL 32 COLON-ALIGNED
     wdt-implant-fim AT ROW 5.25 COL 53 COLON-ALIGNED NO-LABEL
     wcod-emitente-ini AT ROW 6.25 COL 32 COLON-ALIGNED
     wcod-emitente-fim AT ROW 6.25 COL 53 COLON-ALIGNED NO-LABEL
     wvl-tot-ped AT ROW 7.25 COL 32 COLON-ALIGNED
     wrel AT ROW 8.25 COL 32 COLON-ALIGNED
     wcod-cond-pag-para AT ROW 12.5 COL 32 COLON-ALIGNED
     bt-executar-2 AT ROW 17 COL 3 HELP
          "Dispara a execu��o do relat�rio"
     bt-executar AT ROW 17 COL 14 HELP
          "Dispara a execu��o do relat�rio"
     bt-cancelar AT ROW 17 COL 68 HELP
          "Fechar"
     bt-ajuda AT ROW 17 COL 78 HELP
          "Ajuda"
     RECT-2 AT ROW 16.75 COL 2
     IMAGE-1 AT ROW 5.25 COL 48
     IMAGE-10 AT ROW 5.25 COL 52
     IMAGE-11 AT ROW 6.25 COL 48
     IMAGE-12 AT ROW 6.25 COL 52
     IMAGE-2 AT ROW 5.25 COL 52
     IMAGE-7 AT ROW 5.25 COL 48
     IMAGE-8 AT ROW 5.25 COL 52
     IMAGE-9 AT ROW 5.25 COL 48
     RECT-1 AT ROW 2 COL 2
     RECT-3 AT ROW 18.25 COL 2
     RECT-4 AT ROW 11.25 COL 3
     RECT-7 AT ROW 3.5 COL 2
     "Nova condi��o de Pagamento" VIEW-AS TEXT
          SIZE 31 BY .54 AT ROW 11 COL 31
          FONT 0
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 89.43 BY 18.71
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Altera��o Condi��o de Pagto p/Concession�rias Ford"
         HEIGHT             = 18.71
         WIDTH              = 89.43
         MAX-HEIGHT         = 29.79
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.79
         VIRTUAL-WIDTH      = 146.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
                                                                        */
/* SETTINGS FOR RECTANGLE RECT-2 IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Altera��o Condi��o de Pagto p/Concession�rias Ford */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Altera��o Condi��o de Pagto p/Concession�rias Ford */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda C-Win
ON CHOOSE OF bt-ajuda IN FRAME DEFAULT-FRAME /* Ajuda */
DO:
  RUN pi_ajuda.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancelar C-Win
ON CHOOSE OF bt-cancelar IN FRAME DEFAULT-FRAME /* Fechar */
DO:
   apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar C-Win
ON CHOOSE OF bt-executar IN FRAME DEFAULT-FRAME /* Alterar */
DO:
   do  on error undo, return no-apply:
       run pi-altera.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-executar-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar-2 C-Win
ON CHOOSE OF bt-executar-2 IN FRAME DEFAULT-FRAME /* Relat�rio */
DO:
   do  on error undo, return no-apply:
       run pi-executar.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-5 C-Win
ON CHOOSE OF BUTTON-5 IN FRAME DEFAULT-FRAME /* Button 5 */
DO:
   apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BUTTON-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BUTTON-7 C-Win
ON CHOOSE OF BUTTON-7 IN FRAME DEFAULT-FRAME /* Button 7 */
DO:
  RUN pi_ajuda.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
    RUN enable_UI.
    RUN iniciar.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wdt-implant-ini wdt-implant-fim wcod-emitente-ini wcod-emitente-fim 
          wvl-tot-ped wrel wcod-cond-pag-para 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE BUTTON-5 BUTTON-7 wdt-implant-ini wdt-implant-fim wcod-emitente-ini 
         wcod-emitente-fim wvl-tot-ped wrel wcod-cond-pag-para bt-executar-2 
         bt-executar bt-cancelar bt-ajuda IMAGE-1 IMAGE-10 IMAGE-11 IMAGE-12 
         IMAGE-2 IMAGE-7 IMAGE-8 IMAGE-9 RECT-1 RECT-3 RECT-4 RECT-7 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE iniciar C-Win 
PROCEDURE iniciar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
CLEAR  FRAME   {&FRAME-NAME}.
ASSIGN  wdt-implant-ini    = TODAY
        wdt-implant-fim    = TODAY
        wcod-emitente-ini  = 200000
        wcod-emitente-fim  = 299999
        wcod-cond-pag-para = 0
        wvl-tot-ped        = 0
        wrel = "c:/spool/ALTERAPAGTO.TXT"
        wconf = NO.
DISPLAY wdt-implant-ini wdt-implant-fim  wcod-emitente-ini wcod-emitente-fim
        wcod-cond-pag-para wrel wvl-tot-ped
        WITH FRAME    {&FRAME-NAME}.     
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-altera C-Win 
PROCEDURE pi-altera :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

MESSAGE "Confirma a altera��o de condi��o de pagamento?" VIEW-AS ALERT-BOX QUESTION  BUTTONS YES-NO UPDATE wconf.
IF wconf = YES  THEN DO:
  OUTPUT TO VALUE(wrel) PAGE-SIZE 66.
  FOR EACH ped-venda WHERE (ped-venda.dt-implant      GE  INPUT  FRAME default-frame  wdt-implant-ini
                     AND    ped-venda.dt-implant      LE  INPUT  FRAME default-frame  wdt-implant-fim)
                     AND    (ped-venda.cod-emitente   GE  INPUT FRAME default-frame  wcod-emitente-ini 
                     AND     ped-venda.cod-emitente   LE  INPUT FRAME default-frame  wcod-emitente-fim) 
                     AND    ped-venda.vl-tot-ped      GE  INPUT FRAME default-frame wvl-tot-ped:
    
    
    ASSIGN cod-cond-pag = INPUT FRAME default-frame wcod-cond-pag-para.
    DISPLAY dt-implant cod-emitente nome-abrev nr-pedido nr-pedcli vl-tot-ped cod-cond-pag WITH FRAME f-lista.
    DOWN WITH FRAME f-lista.
    END.
    OUTPUT CLOSE. 
    END.
IF wconf = YES THEN MESSAGE "ALTERA��O EFETUADA, VEJA ARQUIVO GERADO." VIEW-AS ALERT-BOX. 
IF wconf = NO THEN MESSAGE "ALTERA��O NAO EFETUADA." VIEW-AS ALERT-BOX.
RUN iniciar.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-executar C-Win 
PROCEDURE pi-executar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

OUTPUT TO VALUE(wrel) PAGE-SIZE 66.
FOR EACH ped-venda WHERE (ped-venda.dt-implant      GE  INPUT  FRAME default-frame  wdt-implant-ini
                   AND    ped-venda.dt-implant      LE  INPUT  FRAME default-frame  wdt-implant-fim)
                   AND    (ped-venda.cod-emitente   GE  INPUT FRAME default-frame  wcod-emitente-ini 
                   AND     ped-venda.cod-emitente   LE  INPUT FRAME default-frame  wcod-emitente-fim) 
                   AND    ped-venda.vl-tot-ped      GE  INPUT FRAME default-frame wvl-tot-ped                                   
                  NO-LOCK:
 DISPLAY dt-implant cod-emitente nome-abrev nr-pedido nr-pedcli vl-tot-ped cod-cond-pag WITH FRAME f-lista.
 DOWN WITH FRAME f-lista.
END.
OUTPUT CLOSE. 

MESSAGE "Fim da gera��o do Arquivo." VIEW-AS ALERT-BOX. 
RUN iniciar.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi_ajuda C-Win 
PROCEDURE pi_ajuda :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  MESSAGE 
      "BOT�O RELAT�RIO: gera em arquivo o relat�rio dos pedidos, conforme par�metros selecionados."
      SKIP
      "BOT�O ALTERAR:   altera a condi��o de pagamento dos pedidos, considerando pedidos"
      "acima do valor digitado nos par�metros."
      VIEW-AS ALERT-BOX.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

