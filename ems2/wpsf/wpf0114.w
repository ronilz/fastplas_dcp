&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */


DEFINE VARIABLE wemp        LIKE estabelec.nome.
DEFINE VARIABLE wqt-it     AS   INTEGER FORMAT "999".
DEFINE VARIABLE wtot-nf    AS CHAR FORMAT "X(17)".
DEFINE VARIABLE wdesenho   AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcgccli    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcgcfor    AS CHAR FORMAT "X(14)".
DEFINE VARIABLE wcont      AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont1     AS INTEGER FORMAT "99".
DEFINE VARIABLE wcont2     AS INTEGER FORMAT "99".
DEFINE VARIABLE wclass     AS CHAR FORMAT "X(10)".
DEFINE VARIABLE wdif       AS INTE FORMAT 99.
DEFINE VARIABLE wdif1      AS INTE FORMAT 99.
DEFINE VARIABLE wabrecon   AS CHAR FORMAT "X(20)".
DEFINE STREAM   w-log.


DEFINE VARIABLE wvenc      LIKE fat-duplic.dt-venciment.
DEFINE VARIABLE wtot-itens            AS INTEGER.
DEFINE VARIABLE wfab       AS CHAR FORMAT "x(03)".

DEFINE VARIABLE wseq-it               AS INTEGER FORMAT "999".
DEFINE VARIABLE wtam-item             AS INTEGER FORMAT "99".

DEFINE VARIABLE wun-cond              AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wqt-cond              AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wun-mov               AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wdent-embal           AS CHAR FORMAT "X(20)".
DEFINE VARIABLE wpc-embal             AS INTEGER FORMAT "99999999".
DEFINE VARIABLE wnf-embal             AS INTEGER FORMAT "999999".
DEFINE VARIABLE wserie-ori            AS CHAR FORMAT "X(04)".
DEFINE VARIABLE wdt-nf-for            AS INTEGER FORMAT "999999".
DEFINE VARIABLE wqt-embals            AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wae5-espaco           AS CHAR FORMAT "X(03)".



DEFINE VARIABLE witem                 LIKE ITEM.codigo-refer.
DEFINE VARIABLE wwpedido   AS CHAR FORMAT "X(12)".
DEFINE VARIABLE wqt-registro          AS INTEGER FORMAT "999999999".
DEFINE VARIABLE wtot-valores          LIKE it-nota-fisc.vl-preuni.
DEFINE BUFFER   b-it-auxiliar         FOR it-nota-fisc.
DEFINE VARIABLE wnarrativa            LIKE item-cli.narrativa[1].
DEFINE VARIABLE wwitem                AS CHAR FORMAT "X(30)".
DEFINE VARIABLE wcont-item            AS INTEGER FORMAT "99".
DEFINE VARIABLE wespaco               AS CHAR FORMAT "x(93)".
/* gera tela */
PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.
/* VARIAVEIS PARA LISTA NA TELA */
def var c-key-value as char no-undo.
DEF VAR warquivo AS CHAR FORMAT "x(40)" NO-UNDO.
DEF VAR wdir     AS CHAR NO-UNDO.

DEF VAR wconf-imp AS LOGICAL.

/* Vers�o 08/2005 
   - Inclusa� de linha do pedido
   - insercao n. nota de saida no campo nota fiscal embalagem */

/* variaveis para pis e cofins */
DEF VAR wvl-pis             AS DEC FORMAT ">>>,>>>,>>9.99".
DEF VAR wvl-cofins          AS DEC FORMAT ">>>,>>>,>>9.99".
DEF VAR wpis-tot-nota       AS DEC FORMAT ">>>,>>>,>>9.99".
DEF VAR wcofins-tot-nota    AS DEC FORMAT ">>>,>>>,>>9.99".

DEF VAR wperc-pis AS CHAR.
DEF VAR wperc-cofins AS CHAR.

/* CAMPOS IPI  - 21/07/11 - LOGICA DEFINADA PELO IDELCIO */
/* ref. ipi reduzido montadoras */
DEFINE VARIABLE waliquota-ipi LIKE it-nota-fisc.aliquota-ipi.
DEFINE VARIABLE wvl-ipi-it    LIKE it-nota-fisc.vl-ipi-it.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-6 RECT-8 RECT-9 wcod-est wdt-ini ~
wdt-fim wnf-ini wnf-fim wserie WREL wrellog bt-gera bt-sair 
&Scoped-Define DISPLAYED-OBJECTS wcod-est wdt-ini wdt-fim wnf-ini wnf-fim ~
wserie WREL wrellog 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-gera 
     LABEL "Gera arquivo" 
     SIZE 13 BY 1.13.

DEFINE BUTTON bt-sair DEFAULT 
     IMAGE-UP FILE "image/im-exi.bmp":U
     LABEL "Sair" 
     SIZE 5 BY 1.13
     BGCOLOR 8 .

DEFINE VARIABLE wcod-est AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-fim AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o Final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wdt-ini AS DATE FORMAT "99/99/9999":U 
     LABEL "Data Emiss�o inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnf-fim AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal final" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE wnf-ini AS CHARACTER FORMAT "X(16)":U 
     LABEL "Nota Fiscal inicial" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE WREL AS CHARACTER FORMAT "X(40)":U 
     LABEL "Nome do relat�rio" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE wrellog AS CHARACTER FORMAT "X(40)":U 
     LABEL "Nome rel. de consist�ncia" 
     VIEW-AS FILL-IN 
     SIZE 28 BY 1 NO-UNDO.

DEFINE VARIABLE wserie AS CHARACTER FORMAT "X(5)":U INITIAL "3" 
     LABEL "S�rie" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 68 BY 10.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 69 BY .5
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 69 BY .5
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     wcod-est AT ROW 3.75 COL 34 COLON-ALIGNED
     wdt-ini AT ROW 4.75 COL 34 COLON-ALIGNED
     wdt-fim AT ROW 5.75 COL 34 COLON-ALIGNED
     wnf-ini AT ROW 6.75 COL 34 COLON-ALIGNED
     wnf-fim AT ROW 7.75 COL 34 COLON-ALIGNED
     wserie AT ROW 8.75 COL 34 COLON-ALIGNED
     WREL AT ROW 9.75 COL 34 COLON-ALIGNED
     wrellog AT ROW 10.75 COL 34 COLON-ALIGNED
     bt-gera AT ROW 13.25 COL 6
     bt-sair AT ROW 13.25 COL 19
     "SALVAR O ARQUIVO AV* NA PASTA: ~\~\fastsrv3~\dirsaida~\" VIEW-AS TEXT
          SIZE 64 BY .54 AT ROW 12.25 COL 8 WIDGET-ID 2
          BGCOLOR 0 FGCOLOR 15 FONT 0
     RECT-6 AT ROW 3 COL 6
     RECT-8 AT ROW 2 COL 5
     RECT-9 AT ROW 14.25 COL 6
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 80.72 BY 15.04
         FONT 1
         DEFAULT-BUTTON bt-sair.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Aviso de Embarque - DSH/ VW - RND  004.04- ESP - wpf0114 - V.04/09"
         HEIGHT             = 15.04
         WIDTH              = 80.72
         MAX-HEIGHT         = 29.71
         MAX-WIDTH          = 146.29
         VIRTUAL-HEIGHT     = 29.71
         VIRTUAL-WIDTH      = 146.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* Aviso de Embarque - DSH/ VW - RND  004.04- ESP - wpf0114 - V.04/09 */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* Aviso de Embarque - DSH/ VW - RND  004.04- ESP - wpf0114 - V.04/09 */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-gera
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-gera C-Win
ON CHOOSE OF bt-gera IN FRAME DEFAULT-FRAME /* Gera arquivo */
DO:

  /* V.04/09 - 22/09/09 - CORRIGIDO CAMPOS PIS E COFINS - PARA CALCULAR APARTIR DO CHAR DO IT-NOTA-FISC,
     CONFORME SOLICITACAO DA FORD - PROGRAMAS EMS UTILIZADOS PARA CHEGAGEM DE VALORS  - FT0904 /
     FT0904C / OF0311*/

  DISABLE wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog
          bt-gera bt-sair WITH FRAME {&FRAME-NAME}.
  FORM
      nota-fiscal.nr-nota-fis  FORMAT "X(10)"
      nota-fiscal.cod-emitente 
      nota-fiscal.dt-emis-nota 
      it-nota-fisc.it-codigo
      witem
      it-nota-fisc.nr-pedcli 
      /*ped-item.nr-sequencia  */
      it-nota-fisc.nr-seq-ped
      it-nota-fisc.qt-faturada[1] 
      it-nota-fisc.vl-preuni COLUMN-LABEL "Vr.unit."
      nota-fiscal.vl-tot-nota COLUMN-LABEL "Vr.Tot.NF."
  HEADER 
  wemp
  TODAY          FORMAT "99/99/9999"
  STRING(TIME,"HH:MM:SS")         
  "HS"
  "Pag.:"                                          AT 69
  PAGE-NUMBER    FORMAT "999"        
  SKIP
  "Arquivos -> Geracao:" wrel "Consistencia:" wrellog
  SKIP(1)
  "** Relatorio de Consistencia de geracao do arquivo de aviso de Embarque DSH**"
  AT 02 SKIP
  "Referente dias:" AT 07 wdt-ini "a" wdt-fim "Notas:" wnf-ini "a" wnf-fim
  SKIP(1)
  WITH FRAME f-log DOWN WIDTH 600.
  
  ASSIGN wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog.

  OUTPUT TO VALUE (wrel).
  OUTPUT STREAM w-log TO VALUE(wrellog).
  FIND FIRST estabelec WHERE estabelec.cod-estabel = wcod-est 
                         NO-LOCK NO-ERROR.
  FIND FIRST emitente WHERE emitente.nome-abrev = "VW SBC" NO-LOCK NO-ERROR.
        /* CGC CLIENTE */
        DO wcont = 1 TO 19:
          IF ASC(SUBSTRING(emitente.cgc,wcont,1)) = 45
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 46
          OR ASC(SUBSTRING(emitente.cgc,wcont,1)) = 47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgccli = wcgccli + SUBSTRING(emitente.cgc,wcont,1).
          END.
        /* CGC FORNECEDOR */
        DO wcont1 = 1 TO 19:
          IF ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  45
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  46
          OR ASC(SUBSTRING(estabelec.cgc,wcont1,1)) =  47 
          THEN DO:
            NEXT.
            END.
          ASSIGN wcgcfor = wcgcfor + SUBSTRING(estabelec.cgc,wcont1,1).
          END.
   RUN DISP_itp.

  ASSIGN  wemp = estabelec.nome.
  FOR EACH nota-fiscal where nota-fiscal.cod-emitente GE 100000 
                         AND   nota-fiscal.cod-emitente LT 200000 
                         AND   nota-fiscal.nr-nota-fis  GE wnf-ini 
                         AND   nota-fiscal.nr-nota-fis  LE wnf-fim
                         AND   nota-fiscal.dt-emis-nota GE wdt-ini
                         AND   nota-fiscal.dt-emis-nota LE wdt-fim
                         AND   nota-fiscal.dt-cancel    = ?
                         AND   nota-fiscal.cod-estabel  = wcod-est
                         AND   nota-fiscal.serie        = wserie
                         NO-LOCK BREAK BY nota-fiscal.dt-emis-nota:
       ASSIGN wtot-itens  = 0
              wseq-it     = 0.
             
       /* buffer para somar total de intes da nota */
       FOR EACH b-it-auxiliar OF nota-fiscal NO-LOCK:
          ASSIGN wtot-itens = wtot-itens + 1.
       END.  
       FIND FIRST natur-oper WHERE natur-oper.nat-operacao = 
                                   nota-fiscal.nat-operacao 
                                   NO-LOCK NO-ERROR.
    
       /* Se emitir duplicata gera arquivo */
       
       IF natur-oper.emite-duplic = yes THEN  DO:
         ASSIGN wqt-it  = 0
                wcont   = 0
                wcont1  = 0
                wcont2  = 0
                wcgccli = " "
                wcgcfor = " "
                wnarrativa = ""
                wwitem     = ""
                wvenc      = ?.
         FOR EACH doc-fiscal WHERE doc-fiscal.cod-emitente 
                             = nota-fiscal.cod-emitente
                             AND doc-fiscal.dt-emis-doc  
                             = nota-fiscal.dt-emis-nota
                             AND doc-fiscal.nr-doc-fis   
                               = nota-fiscal.nr-nota-fis
                           NO-LOCK:
         
             
       FOR EACH it-nota-fisc OF nota-fiscal NO-LOCK BREAK BY it-nota-fisc.nr-nota-fis:
            ASSIGN wperc-pis    = " "
                   wperc-cofins = " "
                   wvl-pis      = 0
                   wvl-cofins   = 0
                   wpis-tot-nota =  0
                   wcofins-tot-nota = 0.

         FIND FIRST ITEM OF it-nota-fisc NO-LOCK.
         IF fm-codigo = "FM-89" THEN NEXT.
         ASSIGN wqt-it  = wqt-it + 1
                  wclass  = " "
                  wdif    = 0
                  wdif1   = 0
                  wseq-it = wseq-it + 1
                  wtam-item = 0.
             
         FIND FIRST item WHERE item.it-codigo = it-nota-fisc.it-codigo
                         NO-LOCK NO-ERROR.

         ASSIGN witem = " ".
         ASSIGN witem = SUBSTRING(codigo-refer,1,17).

     
         FIND FIRST fat-duplic WHERE fat-dupli.cod-estabel 
                                   = nota-fiscal.cod-estabel
                               AND fat-dupli.serie       
                                   = nota-fiscal.serie
                               AND fat-dupli.nr-fatura   
                                   = nota-fiscal.nr-fatura
                               NO-LOCK NO-ERROR.
           ASSIGN wvenc = fat-duplic.dt-vencimen.
        IF wvenc = ? THEN DO:
        MESSAGE "ERROR"
        SKIP "NAO ENCONTROU DATA DE VENCIMENTO PARA NF:" nota-fiscal.nr-nota-fis
        VIEW-AS ALERT-BOX.     
        LEAVE.
        END.
        /* 2706/11*/
         /*
        FIND FIRST ped-venda OF it-nota-fisc NO-LOCK NO-ERROR.
     
         FIND  ped-item OF ped-venda 
                   WHERE ped-item.it-codigo = it-nota-fisc.it-codigo
                    NO-LOCK NO-ERROR.
         /*MESSAGE ped-item.nr-sequencia VIEW-AS ALERT-BOX.*/
         MESSAGE ped-venda.nr-pedcli ped-item.it-codigo VIEW-AS ALERT-BOX.
         IF NOT AVAILABLE ped-item THEN DO: 
           MESSAGE 
           "PEDIDO NAO ENCONTRADO PARA O ITEM:" it-nota-fisc.it-codigo
           SKIP "NOTA:" it-nota-fisc.nr-nota-fis
           SKIP "OU NOTA GERADA SEM A UTILIZACAO DE PEDIDOS"
           SKIP "FAVOR VERIFICAR"
           SKIP "GERACAO DE ARQUIVO COM ERROS !!!"
           VIEW-AS ALERT-BOX.
           NEXT.
           END.
         END.
*/
        
           /*
           PUT ROW 23   "NF.: "     + STRING(nota-fiscal.nr-nota-fis)
                           + " Item: "   + STRING(ped-item.it-codigo)
                           + " Pedido: " + STRING(ped-venda.nr-pedcli).
           */
         
         /* 22/09/09 calculo pis e cofins - pois os mesmos n�o est�o gravados, estao gravados somente
         os percentuais de pis e cofins nos char do it-nota-fisc*/
          ASSIGN wperc-pis     =  STRING(substring(it-nota-fisc.char-2,76,5))  
                 wperc-cofins  = string(SUBSTRING(it-nota-fisc.char-2,81,5))
                 wvl-pis       = ROUND((it-nota-fisc.vl-merc-ori * decimal(wperc-pis)) / 100,2)
                 wvl-cofins    =  ROUND((it-nota-fisc.vl-merc-ori * decimal(wperc-cofins)) / 100,2).
         /* calculo do pis e cofins total da nota */
             ASSIGN wpis-tot-nota = ROUND((nota-fiscal.vl-mercad * decimal(wperc-pis)) / 100,2)
             wcofins-tot-nota = ROUND((nota-fiscal.vl-mercad * decimal(wperc-cofins)) / 100,2).
          


        /* MESSAGE wperc-pis  wperc-cofins wvl-pis  wvl-cofins VIEW-AS ALERT-BOX.  */

          /* 21/07/11 - tratamento IPI - REDUZIDO PARA MONTADORAS*/  
             FIND FIRST ITEM OF it-nota-fisc NO-LOCK.
             ASSIGN waliquota-ipi = it-nota-fisc.aliquota-ipi
                    wvl-ipi-it    = it-nota-fisc.vl-ipi-it.
            
            
            
            /* 21-07-11 - verifica a natureza e zera valores ipi, discutido com Idelcio e Jose Luiz */
            IF item.cd-trib-ipi = 4 /* reduzido */
              AND natur-oper.cd-trib-ipi = 3 /* outros */  THEN DO:
              ASSIGN waliquota-ipi = 0 
                     wvl-ipi-it    = 0.
              END.

         DISPLAY STREAM w-log
                 nota-fiscal.nr-nota-fis        COLUMN-LABEL "NF"       FORMAT "X(08)"
                 nota-fiscal.cod-emitente       COLUMN-LABEL "Emitente"  
                 nota-fiscal.dt-emis-nota       COLUMN-LABEL "DtEmis"   FORMAT "99/99/99"
                 it-nota-fisc.it-codigo         COLUMN-LABEL "Item"     FORMAT "X(13)"
                 witem                          COLUMN-LABEL "Desc"     FORMAT "X(13)"
                 it-nota-fisc.nr-pedcli         COLUMN-LABEL "PedCli"   FORMAT "X(08)"
                 it-nota-fisc.nr-seq-ped        COLUMN-LABEL "Seq."     FORMAT "99999"
                 /*ped-item.nr-sequencia          COLUMN-LABEL "Seq."   FORMAT "99999"*/
                 it-nota-fisc.qt-faturada[1]    COLUMN-LABEL "QTD"      FORMAT ">>>>9.99"
                 it-nota-fisc.vl-preuni         COLUMN-LABEL "PrecoUni" FORMAT ">>>>>>9.99"
                 nota-fiscal.vl-tot-nota        COLUMN-LABEL "VlTotNF." FORMAT ">>>>>>>9.99"
                 nota-fiscal.vl-frete           COLUMN-LABEL "Vlfrete"  FORMAT ">>>>>>>9.99"
                 nota-fiscal.vl-seguro          COLUMN-LABEL "VlSeguro" FORMAT ">>>>>>>9.99"
                 nota-fiscal.vl-desconto        COLUMN-LABEL "VlDesc" 
                 doc-fiscal.vl-icmsub           COLUMN-LABEL "VlICMSS"  FORMAT ">>>>>>9.99"
/* 22-09/09 - campos abaixo estavam zerados em 22/09/09 */ 
/*                  doc-fiscal.vl-pis              COLUMN-LABEL "VlPis"   FORMAT ">>>>>>9.99" */
/*                  doc-fiscal.vl-finsocial        COLUMN-LABEL "Vl.Cofins"                   */
            
                 it-nota-fisc.aliquota-icm      column-label "AliqICM"       FORMAT ">>9.99"
                 it-nota-fisc.vl-bicms-it       COLUMN-LABEL "BaseIcmsIt"    FORMAT ">>>>>>>9.99"
                 it-nota-fisc.vl-icms-it        COLUMN-LABEL "VrIcmsIt"      FORMAT ">>>>>>>9.99"
                /* comentado pois ipi aparece */
                /*                  it-nota-fisc.aliquota-ipi      COLUMN-LABEL "AliqIPI"       FORMAT ">>9.99"      */
                /*                  it-nota-fisc.vl-ipi-it         COLUMN-LABEL "VrIpiIt"       FORMAT ">>>>>>>9.99" */
                 waliquota-ipi      COLUMN-LABEL "AliqIPI"       FORMAT ">>9.99"
                 wvl-ipi-it         COLUMN-LABEL "VrIpiIt"       FORMAT ">>>>>>>9.99"
                 it-nota-fisc.vl-icmsub-it      COLUMN-LABEL "VrIcmsSubIit"  FORMAT ">>>>>>>9.99"
                 wvl-pis                        COLUMN-LABEL "PisIt"         FORMAT ">>>>>>>9.99"
                 wvl-cofins                     COLUMN-LABEL "CofinsIt"      FORMAT ">>>>>>>9.99"
                 wpis-tot-nota                  COLUMN-LABEL "PisTotNF"      FORMAT ">>>>>>>9.99"
                 wcofins-tot-nota               COLUMN-LABEL "CofinsTotNF"   FORMAT ">>>>>>>9.99"
               /* 22/09/09 campos abaixo estavam zerados em 22/09/09 */ 
/*                  it-nota-fisc.vl-pis            COLUMN-LABEL "Vr.pis-it"  */
/*                  it-nota-fisc.vl-finsocial      COLUMN-LABEL "Vr.Cofins-it"  */
                 it-nota-fisc.vl-despes-it      COLUMN-LABEL "VrDespIt"     FORMAT ">>>>>9.99"
                 doc-fiscal.vl-icms             COLUMN-LABEL "VTotICMS"     FORMAT ">>>>>>>9.99"
                 doc-fiscal.vl-ipi              COLUMN-LABEL "VTotIPI"      FORMAT ">>>>>>>9.99"
            
                 /* NEW**/
             WITH FRAME f-log STREAM-IO.
              
         
             DOWN WITH FRAME f-log.
            /* ---------------------------- DISPLAY VALORES P/ CONF. 
             DISPLAY  STREAM w-log
             "*********** NOTA ***********"
             SKIP  "Vr.nota:    "      nota-fiscal.vl-tot-nota  TO 70 NO-LABEL
             SKIP  "Nat.Oper:   "       nota-fiscal.nat-operacao  TO 70 NO-LABEL
             SKIP  "Vr.Icms:    "       doc-fiscal.vl-icms   TO 70 NO-LABEL
             SKIP  "Vr.IPI:     "       doc-fiscal.vl-ipi  TO 70 NO-LABEL
             SKIP  "Vr.Frete:   "       nota-fiscal.vl-frete  TO 70 NO-LABEL
             SKIP  "Vr.Seguro:  "       nota-fiscal.vl-seguro  TO 70 NO-LABEL
             SKIP  "Vr.Desconto:"       nota-fiscal.vl-desconto  TO 70 NO-LABEL
             SKIP  "Vr.Icms-sub:"       doc-fiscal.vl-icmsub  TO 70 NO-LABEL
             SKIP  "Vr.Pis:     "       doc-fiscal.vl-pis  TO 70 NO-LABEL
             SKIP  "Vr.Cofins:  "       doc-fiscal.vl-finsocial  TO 70 NO-LABEL
             SKIP  "Vr.Aliq.IPI:"       it-nota-fisc.aliquota-ipi  TO 70 NO-LABEL
             SKIP  "Vr.Unit:    "       it-nota-fisc.vl-preuni  TO 70 NO-LABEL
              
             SKIP "*********** ITEM ***********"
             SKIP "Aliq.ICM:      " it-nota-fisc.aliquota-icm TO 70   NO-LABEL
             SKIP  "Base Icms-it:  " it-nota-fisc.vl-bicms-it TO 70  NO-LABEL
             SKIP  "Vr.Icms-it:    " it-nota-fisc.vl-icms-it  TO 70 NO-LABEL
             SKIP  "Vr.Ipi-it:     " it-nota-fisc.vl-ipi-it  TO 70 NO-LABEL
             SKIP  "Vr.Icms-sub-it:" it-nota-fisc.vl-icmsub-it  TO 70 NO-LABEL
             SKIP  "Vr.pis-it:     " it-nota-fisc.vl-pis   TO 70 NO-LABEL
             SKIP  "Vr.Cofins-it:  " it-nota-fisc.vl-finsocial   TO 70 NO-LABEL
             SKIP "Vr.Despesa-it: " it-nota-fisc.vl-despes-it  TO 70 NO-LABEL
             SKIP
             WITH FRAME f-det WIDTH 80 WITH STREAM-IO.
             DOWN WITH FRAME f-det.
             DISPLAY VALORES                    */
/*       DOWN STREAM w-log  WITH FRAME f-log STREAM-IO. */
     
      /* Classifica��o  */
        DO wcont2 = 1 TO 19:
          IF ASC(SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1)) <> 46
          THEN DO:
            ASSIGN wclass = wclass
                          + SUBSTRING(it-nota-fisc.class-fiscal,wcont2,1).
          END.                   
        END.
        ASSIGN wclass = TRIM(wclass).
          IF LENGTH(wclass) < 10 THEN DO:
            ASSIGN wdif = 10 - LENGTH(wclass).
            DO wdif1 = 1 TO wdif:
              ASSIGN wclass = wclass + "0".
              END.
            END.

       IF FIRST-of(it-nota-fisc.nr-nota-fis) THEN RUN  DISP_ae1.
         RUN DISP_dados.
       END.
   END.
  END. 
  END.
  RUN encerra.
  OUTPUT CLOSE.
  OUTPUT STREAM w-log CLOSE.
  MESSAGE "GERA��O CONCLU�DA" SKIP "NOME DO ARQUIVO: " + STRING(WREL)
           VIEW-AS ALERT-BOX.
  ENABLE wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie
         bt-gera bt-sair wrel wrellog WITH FRAME {&FRAME-NAME}.
  RUN ver-consis.
  RUN inicia.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-sair C-Win
ON CHOOSE OF bt-sair IN FRAME DEFAULT-FRAME /* Sair */
DO:
  &IF "{&PROCEDURE-TYPE}" EQ "SmartPanel" &THEN
    &IF "{&ADM-VERSION}" EQ "ADM1.1" &THEN
      RUN dispatch IN THIS-PROCEDURE ('exit').
    &ELSE
      RUN exitObject.
    &ENDIF
  &ELSE
      APPLY "CLOSE":U TO THIS-PROCEDURE.
  &ENDIF
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  RUN inicia.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE consistencia C-Win 
PROCEDURE consistencia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Relatorio de Consistencia */

         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_ae1 C-Win 
PROCEDURE disp_ae1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/* AE1 */
      PUT UNFORMATTED
      "AE1"
      INTEGER(nota-fiscal.nr-nota-fis)   FORMAT "999999"
            nota-fiscal.serie            FORMAT "X(4)"
      SUBSTRING(STRING(YEAR(dt-emis-nota),"9999"),3,4) FORMAT "99"
            MONTH(dt-emis-nota) FORMAT "99"
            DAY(dt-emis-nota) FORMAT "99"
      wtot-itens              FORMAT "999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),1,15) 
            FORMAT "999999999999999"
      SUBSTRING(STRING(nota-fiscal.vl-tot-nota,"999999999999999.99"),17,2) 
      FORMAT "99"
      /* anterior "2" */
       "0" /* no. casas decimais para quant. */
      "000" /* cod-fiscal oper*/ /* SUBSTRING(nota-fiscal.nat-operacao,1,3) FORMAT "999" */
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
      SUBSTRING(STRING(doc-fiscal.vl-icms,"999999999999999.99"),17,2)
            FORMAT "99" 
      SUBSTRING(STRING(YEAR(wvenc),"9999"),3,4) FORMAT "99"
            MONTH(wvenc) FORMAT "99"
            DAY(wvenc) FORMAT "99"
            "00" /* nota-fiscal.esp-docto */
             SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),1,15)
            FORMAT "999999999999999"
            SUBSTRING(STRING(doc-fiscal.vl-ipi,"999999999999999.99"),17,2)
            FORMAT "99" 
            "DSH" /* wfab FORMAT "X(03)"  codigo f�brica DSH*/
            "000000" /* nota-fiscal.dt-entr-cli FORMAT "999999" - data prev entrega*/
            "0000"                  /* periodo de entrega */
            FILL(" ",20)  /*natur-oper.denominacao FORMAT "X(20)" */
            FILL(" ",10)
            SKIP.

     
      ASSIGN wqt-registro  =  wqt-registro + 1.
      /* DISPLAY NF2 */
      PUT UNFORMATTED
      "NF2"
      "00000000000000000" /* DESPE ACESSORIAS */
      SUBSTRING(STRING(nota-fiscal.vl-frete,"999999999999999.99"),1,15) FORMAT "X(15)"
      SUBSTRING(STRING(nota-fiscal.vl-frete,"999999999999999.99"),17,2) FORMAT "X(2)"

      SUBSTRING(STRING(nota-fiscal.vl-seguro,"999999999999999.99"),1,15) FORMAT "X(15)"
      SUBSTRING(STRING(nota-fiscal.vl-seguro,"999999999999999.99"),17,2) FORMAT "X(2)"
      
      SUBSTRING(STRING(nota-fiscal.vl-desconto,"999999999999999.99"),1,15) FORMAT "X(15)"
      SUBSTRING(STRING(nota-fiscal.vl-desconto,"999999999999999.99"),17,2) FORMAT "X(2)"

      SUBSTRING(STRING(doc-fiscal.vl-icmsub,"999999999999999.99"),1,15) FORMAT "X(15)"
      SUBSTRING(STRING(doc-fiscal.vl-icmsub,"999999999999999.99"),17,2) FORMAT "X(2)"
     /* 22/09/09 - estes campos est�o c/ valores zerados, o percentual do pis
     e cofins ficam no char do it-nota-fisc, para calcular O TOTAL DO PIS E CONFINS foi
     utilizado o vl-mercad da nota-fsical */
      SUBSTRING(STRING(wpis-tot-nota,"999999999.99"),1,9)  FORMAT "X(9)"
      SUBSTRING(STRING(wpis-tot-nota,"999999999.99"),11,2) FORMAT "X(02)"

      SUBSTRING(STRING(wcofins-tot-nota,"999999999.99"),1,9) FORMAT "X(09)"
      SUBSTRING(STRING(wcofins-tot-nota,"999999999.99"),11,2) FORMAT "X(02)"


/* INIBIDO POIS OS VALORES EST�O ZERADOS*/
/*                                                                                     */
/*       SUBSTRING(STRING(doc-fiscal.vl-pis,"999999999.99"),1,9)  FORMAT "X(9)"        */
/*       SUBSTRING(STRING(doc-fiscal.vl-pis,"999999999.99"),11,2) FORMAT "X(02)"       */
/*                                                                                     */
/*       SUBSTRING(STRING(doc-fiscal.vl-finsocial,"999999999.99"),1,9) FORMAT "X(09)"  */
/*       SUBSTRING(STRING(doc-fiscal.vl-finsocial,"999999999.99"),11,2) FORMAT "X(02)" */
      "00000000000"   /* despesa*/
      "0000000"      /* tx cambial */
      SKIP.
      ASSIGN wqt-registro  =  wqt-registro + 1.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_dados C-Win 
PROCEDURE disp_dados :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* PROCEDURES */
     /* AE2*/
      PUT UNFORMATTED 
      "AE2"                        /* t  ipo do registro */
      wseq-it     FORMAT "999"    /* num. item na nota */
      /* altera��o feita para inserir numero da linha - conforme instru��o marcos - gedas
      24/07/2005 */
      it-nota-fisc.nr-pedcli      FORMAT "9999999"    /* pedcli  */
      /*ped-item.nr-sequencia     FORMAT "99999"      /* seq. ped  - numero da linha */*/
      it-nota-fisc.nr-seq-ped     FORMAT "99999"      /* seq. ped  - numero da linha */
      /*
      it-nota-fisc.nr-pedcli      FORMAT "X(12)"  /* pedido */
      */
      witem                       FORMAT "X(30)" /* cod-item */
      SUBSTRING(STRING(it-nota-fisc.qt-faturada[1],"999999999.9999"),1,9)
      FORMAT "999999999"
      FILL(" ",2) /* UNIDADE */ /* it-nota-fisc.un-fatur[1] FORMAT "X(02)"*/
      wclass FORMAT "X(10)"
      SUBSTRING(STRING(waliquota-ipi,"999.99"),2,2)
      FORMAT "99"
/*       SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),2,2) */
/*       FORMAT "99"                                               */
      SUBSTRING(STRING(it-nota-fisc.aliquota-ipi,"999.99"),5,2) 
      FORMAT "99"
      SUBSTRING(string(it-nota-fisc.vl-preuni,"999999999999999.99999"),1,15)
      FORMAT "999999999999999"
      SUBSTRING(string(it-nota-fisc.vl-preuni,"999999999999999.99999"),17,2)
      FORMAT "99"
      "000000000"             /* qt.item estoque       */
      FILL(" ",2) /* UNIDADE */ /*item.un                 FORMAT "X(02)"*/
      "000000000"             /* qt.unidade compra     */
      "  "                    /* unidade medida compra */
      " "                     /* tipo de fornecimento  */
      "0000"
      "00000000000"           /* Vr.tot.desc.item      */
      SKIP.
       ASSIGN wqt-registro  =  wqt-registro + 1.    
             PUT UNFORMATTED
            /*AE4 */
                  "AE4"
            SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),2,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.aliquota-icm,"999.99"),5,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-bicms-it,"999999999999999.99"),17,2)
                  FORMAT "99"
            SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-icms-it,"999999999999999.99"),17,2)
                  FORMAT "99"
            /* INIBIDO POIS MONTADORES - IPI � REDUZIDO
            SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-ipi-it,"999999999999999.99"),17,2)
                  FORMAT "99"
            */
            SUBSTRING(STRING(wvl-ipi-it,"999999999999999.99"),1,15)
                  FORMAT "999999999999999"
            SUBSTRING(STRING(wvl-ipi-it,"999999999999999.99"),17,2)
                  FORMAT "99"

            "10"                    /* sit.tributaria            */
            SUBSTRING(STRING(it-nota-fisc.vl-icmsub-it,"999999999.99"),1,9) 
            FORMAT "999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-icmsub-it,"999999999.99"),11,2) 
            FORMAT "99"
/* 22/09/09 - estes campos est�o c/ valores zerados, o percentual do pis
e cofins ficam no char do it-nota-fisc, para calcular sobre o vl-merc-ori */

/*             SUBSTRING(STRING(it-nota-fisc.vl-pis,"999999999.99"),1,9)        */
/*             FORMAT "999999999"                                               */
/*             SUBSTRING(STRING(it-nota-fisc.vl-pis,"999999999.99"),11,2)       */
/*             FORMAT "99"                                                      */
/*                                                                              */
/*             SUBSTRING(STRING(it-nota-fisc.vl-finsocial,"999999999.99"),1,9)  */
/*             FORMAT "999999999"                                               */
/*             SUBSTRING(STRING(it-nota-fisc.vl-finsocial,"999999999.99"),11,2) */
/*             FORMAT "99"                                                      */


            /*22/09/09  valores calculados a partir do char do it-nota-fisc, onde estao
            os percentuais de pis e cofins * vl-merc-ori */
            SUBSTRING(STRING(wvl-pis,"999999999.99"),1,9)
            FORMAT "999999999"
            SUBSTRING(STRING(wvl-pis,"999999999.99"),11,2)
            FORMAT "99"

            SUBSTRING(STRING(wvl-cofins,"999999999.99"),1,9)
            FORMAT "999999999"
            SUBSTRING(STRING(wvl-cofins,"999999999.99"),11,2)
            FORMAT "99"

            SUBSTRING(STRING(it-nota-fisc.vl-despes-it,"999999999.99"),1,9) 
            FORMAT "999999999"
            SUBSTRING(STRING(it-nota-fisc.vl-despes-it,"999999999.99"),11,2) 
            FORMAT "99" 

            FILL(" ",24)
            SKIP.
            /* total de registros */
            ASSIGN wqt-registro  =  wqt-registro + 1.
            /* Referente embalagem  AE5*/
            /* Campos de embalagem assinalados conforme orienta��o
           do Sr. Marcos da Gedas em 15/08/2005, para evitar
           mensagens de alerta para o Sr. Nelson Yamaguti 
           Campos: numero da nota embalagem e data iguais 
           aos da nota fiscal de faturamento*/
           
     ASSIGN wnf-embal = INTEGER(nota-fiscal.nr-nota-fis).
    
          
      /* TOTAL DE TRANSA��ES */
      /*     wtot-valores  = wtot-valores + it-nota-fisc.vl-preuni. */
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disp_itp C-Win 
PROCEDURE disp_itp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   ASSIGN wqt-registro  =  wqt-registro + 1.
   PUT UNFORMATTED 
      "ITP" /* REGISTRO*/
      "004" /* TIPO RND 004*/
      "04"  /* VERS�O RND */
      "00000" /* controle movto */
      "000000000000" /* geracao movto */
      TRIM(wcgcfor) FORMAT "X(14)"     /* cgc fornecedor */
      "59104422005704" /* cgc receptor */
      /*TRIM(wcgccli) FORMAT "X(14)"*/ 
      FILL(" ",16)  /* cod interno transmi/recept*/
      FILL(" ",59) /* itp filler */
      SKIP.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie WREL wrellog 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-6 RECT-8 RECT-9 wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie 
         WREL wrellog bt-gera bt-sair 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE encerra C-Win 
PROCEDURE encerra :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/********************************************************/
  ASSIGN wqt-registro  =  wqt-registro + 1.
  PUT UNFORMATTED 
      "FTP"
      "00000"  /*nO.CONTROLE TRANSMISS�O */
      wqt-registro /* qt registros */ FORMAT "999999999"
      FILL(" ",111).
/*       SUBSTRING(STRING(wtot-valores,"999999999999999.99"),1,15) */
/*             FORMAT "999999999999999"                            */
/*       SUBSTRING(STRING(wtot-valores,"999999999999999.99"),17,2) */
/*             FORMAT "99"                                         */
/*       " "                                                       */
/*       wespaco FORMAT "X(93)".                                   */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gera-tela C-Win 
PROCEDURE gera-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
ASSIGN warquivo = wrellog.
  get-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value.
    
  if c-key-value = "":U or c-key-value = ?  then do:
    assign c-key-value = "Notepad.exe":U.
    put-key-value section "Datasul_EMS2":U key "Show-Report-Program":U value c-key-value no-error.
  end.
  run winexec (input c-key-value + chr(32) + warquivo, input 1).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia C-Win 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* cria pasta v:\spool\AVISO*/
DEFINE VARIABLE v-destino AS CHARACTER NO-UNDO.
ASSIGN v-destino = "v:\SPOOL\AVISO".
ASSIGN FILE-INFO:FILE-NAME = v-destino.
IF FILE-INFO:FULL-PATHNAME = ?  THEN
    os-create-dir value('v:\SPOOL\AVISO').

/* AVISO */
ASSIGN     wdt-ini    = TODAY
           wdt-fim    = TODAY
           wcod-est   = "1"
           wdesenho   = FILL(" ",30)
           wnf-ini    = "0"
           wnf-fim    = "9999999"

      /* comf.jose luiz e alessandro - 03/08/21 - programa passa a gerar
          novamente no spool/aviso  ---------- */
           wrel       = "v:\SPOOL\AVISO\ADSH"   
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
           wrellog    = "v:\SPOOL\AVISO\CDSH"
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),1,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),4,2)
                      + SUBSTRING(STRING(TIME,"HH:MM:SS"),7,2)
                      + ".TXT"
          wfab       = "DSH"
          wserie     = "3"
          wtot-itens = 0
          wqt-registro = 0
          wtot-valores = 0
          witem        = ""
          wseq-it      = 0 
          wtam-item    = 0.
DISPLAY wcod-est wdt-ini wdt-fim wnf-ini wnf-fim wserie wrel wrellog 
        WITH FRAME    {&FRAME-NAME}.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE TESTE C-Win 
PROCEDURE TESTE :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE ver-consis C-Win 
PROCEDURE ver-consis :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  RUN gera-tela.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

