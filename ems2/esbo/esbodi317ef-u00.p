/*************************************************************************
**
**  PROGRAMA - esbodi317ef-u00.p
**             
**  OBJETIVO - COMPLEMENTO DE MENSAGEM
**
**  AUTOR    - FLAVIO SERAPIAO
**
**  DATA     - 27/03/2009
**
**************************************************************************/

define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESBODI317EF-U00.P 2.04.00.000}

{utp/ut-glob.i}
{include/i-epc200.i bodi317}

/* Wanderlei Pereira agosto de 2002*/

DEF INPUT PARAM  p-ind-event AS  CHAR NO-UNDO.
DEF INPUT-OUTPUT PARAM TABLE FOR tt-epc.
DEF VAR l-erro AS LOGICAL.


RUN esbo/esbodi317ef-u02.p (INPUT p-ind-event,        
                           INPUT-OUTPUT TABLE tt-epc) .    

