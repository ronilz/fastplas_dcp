/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************
* Altera��es F�brica HCM
  
  Removido m�todo RUN
  Removido m�todo PrintOut

*/
{include/i-prgvrs.i ESCP009RP 2.08.00.001}

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i ESCP009RP MCP}
&ENDIF

/* ***************************  Definitions  ************************** */
&global-define programa ESCP009RP

/* definicao de temp-tables */
DEFINE TEMP-TABLE tt-param NO-UNDO
    FIELD destino          AS INTEGER
    FIELD arquivo          AS CHARACTER FORMAT "x(35)"
    FIELD usuario          AS CHARACTER FORMAT "x(12)"
    FIELD data-exec        AS DATE
    FIELD hora-exec        AS INTEGER
    FIELD nr-linha-ini     LIKE es-abastec-lin-item.nr-linha
    FIELD nr-linha-fim     LIKE es-abastec-lin-item.nr-linha
    FIELD data-ini         LIKE es-abastec-lin-item.data
    FIELD data-fim         LIKE es-abastec-lin-item.data
    FIELD nr-seq-ini       LIKE es-abastec-lin-item.nr-seq
    FIELD nr-seq-fim       LIKE es-abastec-lin-item.nr-seq
    FIELD it-codigo-ini    LIKE es-abastec-lin-item.it-codigo
    FIELD it-codigo-fim    LIKE es-abastec-lin-item.it-codigo.

DEF TEMP-TABLE tt-raw-digita
    FIELD raw-digita AS RAW.

DEF TEMP-TABLE tt-csv NO-UNDO
   FIELD nr-linha        LIKE es-abastec-lin-item.nr-linha              
   FIELD data            LIKE es-abastec-lin-item.data                  
   FIELD it-codigo       LIKE es-abastec-lin-item.it-codigo             
   FIELD c-codigo-refer  LIKE ITEM.codigo-refer                               
   FIELD c-desc-item-pai LIKE ITEM.desc-item                               
   FIELD quantidade      LIKE es-abastec-lin-item.quantidade            
   FIELD hora-ini        LIKE es-abastec-lin-item.hora-ini              
   FIELD qtd-peca-skid   LIKE es-abastec-lin-item.qtd-peca-skid         
   FIELD qtd-total-skid  LIKE es-abastec-lin-item.qtd-total-skid.        

DEF TEMP-TABLE tt-hora-normal NO-UNDO
    FIELD data      AS DATE
    FIELD hora-ini  AS CHAR
    FIELD hora-fim  AS CHAR
    INDEX idx IS PRIMARY
        data.

DEF TEMP-TABLE tt-hora-parada NO-UNDO
    FIELD data          AS DATE
    FIELD cod-motivo    LIKE es-motivo-parada.cod-motivo
    FIELD hora-ini      AS CHAR
    FIELD hora-fim      AS CHAR
    INDEX idx IS PRIMARY
        data
        cod-motivo.

/* definicao de parametros */
DEF INPUT PARAM raw-param AS RAW NO-UNDO.
DEF INPUT PARAM TABLE FOR tt-raw-digita.

/* definicao de variaveis */
DEF VAR h-acomp             AS HANDLE               NO-UNDO.    
DEF VAR c-arquivo           AS CHAR                 NO-UNDO.
DEF VAR l-vazio             AS LOGICAL INIT TRUE    NO-UNDO.
DEF VAR c-arq-xls           AS CHAR                 NO-UNDO.

/* definicao de streams */
DEF STREAM str-csv.

/* main block */
CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

{include/i-rpvar.i}

/*IF i-num-ped-exec-rpw <> 0 THEN
    tt-param.arquivo = "ESAPB010_" + STRING(i-num-ped-exec-rpw) + ".xlsx".*/

RUN pi-retorna-diretorio 
     (tt-param.usuario,
     OUTPUT c-arquivo).

ASSIGN c-arquivo = REPLACE(c-arquivo,"/","\") + "\"
       c-arquivo = REPLACE(c-arquivo,"\\","\") + "escp009_" + STRING(TIME) + ".csv".

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.

RUN pi-inicializar IN h-acomp (INPUT "Processando...":U).

OUTPUT STREAM str-csv TO VALUE(c-arquivo) NO-CONVERT.

blk-main:
REPEAT ON ERROR UNDO blk-main, LEAVE blk-main
       ON STOP  UNDO blk-main, LEAVE blk-main:

    RUN pi-carregar-tt.

    LEAVE blk-main.

END. /* DO ON ERROR UNDO, LEAVE */

OUTPUT STREAM str-csv CLOSE.

IF NOT l-vazio THEN
    RUN pi-excel.

RUN pi-finalizar IN h-acomp.

RETURN "OK".

/* definicao de procedures */
PROCEDURE pi-excel:

    DEF VAR ch_excel        AS office.iface.excel.ExcelWrapper   NO-UNDO.
    DEF VAR ch_workbook     AS office.iface.excel.Workbook   NO-UNDO.
    DEF VAR i-col           AS INT          NO-UNDO.
    DEF VAR c-range         AS CHAR         NO-UNDO.

    RUN pi-acompanhar IN h-acomp (INPUT "Criando planilha...").

    /* gera��o excel */
    {office/office.i Excel ch_excel}

    IF NOT VALID-OBJECT(ch_excel) THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "N�o foi possivel iniciar o Excel!~~N�o foi possivel iniciar o Excel!").

        RETURN "NOK".

    END. /* IF NOT VALID-OBJECT(ch_excel) */

    ASSIGN ch_excel:VISIBLE = FALSE
           ch_workbook      = ch_excel:WorkBooks:ADD(SEARCH("esp/escp009.xltm")).

    ch_excel:displayAlerts = NO.

    /**FSW
    ch_excel:RUN("carregarArquivo", c-arquivo).
    */

    RUN carregarCSV (c-arquivo, ch_excel).

    CASE tt-param.destino:

        WHEN 1 THEN DO: /* Impressora */

            /*
            ch_excel:ActivePrinter = tt-param.arquivo.
            ch_workbook:PrintOut().
            */
            ch_workbook:CLOSE(FALSE).
            ch_excel:QUIT().

        END. /* WHEN 1 */

        WHEN 2 THEN DO: /* Arquivo */

            ASSIGN
                c-arq-xls = REPLACE(tt-param.arquivo, '~/', '~\')
                c-arq-xls = REPLACE(c-arq-xls, '.LST', '.XLSX').

            ch_workbook:saveas(c-arq-xls).
            ch_workbook:CLOSE(FALSE).
            ch_excel:QUIT().

        END. /* WHEN 2 */

        WHEN 3 THEN DO: /* Terminal */

            ch_excel:VISIBLE = TRUE.

        END. /* WHEN 3 */

    END CASE. /* CASE tt-param.destino */

    /*OS-DELETE VALUE(c-arquivo).*/

    DELETE OBJECT ch_workbook NO-ERROR.
    DELETE OBJECT ch_excel NO-ERROR.

END PROCEDURE. /* PROCEDURE pi-excel */

PROCEDURE pi-carregar-tt:

    DEF VAR c-desc-item-pai     AS CHAR     NO-UNDO.
    DEF VAR c-codigo-refer      AS CHAR     NO-UNDO.
    DEF VAR d-total-skid        AS DEC      NO-UNDO.
    DEF VAR d-horas             AS DEC      NO-UNDO.
    DEF VAR d-horas-paradas     AS DEC      NO-UNDO.
    DEF VAR p-hora              AS DEC      NO-UNDO.
    DEF VAR d-skid-hora         AS DEC      NO-UNDO.

    EMPTY TEMP-TABLE tt-hora-normal.
    EMPTY TEMP-TABLE tt-hora-parada.

    ASSIGN d-total-skid    = 0
           d-horas         = 0
           d-horas-paradas = 0.

    FOR EACH es-abastec-lin WHERE 
        es-abastec-lin.nr-linha  >= tt-param.nr-linha-ini  AND
        es-abastec-lin.nr-linha  <= tt-param.nr-linha-fim  AND
        es-abastec-lin.data      >= tt-param.data-ini      AND
        es-abastec-lin.data      <= tt-param.data-fim      NO-LOCK:

        FOR EACH es-abastec-lin-item OF es-abastec-lin WHERE
            es-abastec-lin-item.nr-seq    >= tt-param.nr-seq-ini    AND
            es-abastec-lin-item.nr-seq    <= tt-param.nr-seq-fim    AND
            es-abastec-lin-item.it-codigo >= tt-param.it-codigo-ini AND
            es-abastec-lin-item.it-codigo <= tt-param.it-codigo-fim NO-LOCK:

            RUN pi-acompanhar IN h-acomp (INPUT "Linha/Data/Seq/Item: " + 
                                                STRING(es-abastec-lin-item.nr-linha) + "/" +
                                                STRING(es-abastec-lin-item.data,"99/99/9999") + "/" +
                                                STRING(es-abastec-lin-item.nr-seq) + "/" +
                                                es-abastec-lin-item.it-codigo).
    
            l-vazio = FALSE.
    
            ASSIGN c-desc-item-pai = ""
                   c-codigo-refer  = "".
            
            FOR FIRST ITEM FIELDS(desc-item codigo-refer) WHERE
                ITEM.it-codigo = es-abastec-lin-item.it-codigo NO-LOCK:
                ASSIGN c-desc-item-pai = ITEM.desc-item
                       c-codigo-refer  = ITEM.codigo-refer.
            END. /* FOR FIRST ITEM */
            
            PUT STREAM str-csv UNFORMATTED
                es-abastec-lin-item.nr-linha                    ";"
                es-abastec-lin-item.data                        ";"
                es-abastec-lin-item.it-codigo                   ";"
                c-codigo-refer                                  ";"
                c-desc-item-pai                                 ";"
                es-abastec-lin-item.quantidade                  ";"
                es-abastec-lin-item.hora-ini                    ";"
                es-abastec-lin-item.qtd-peca-skid               ";"
                es-abastec-lin-item.qtd-total-skid              ";"
                SKIP.

            d-total-skid = d-total-skid + es-abastec-lin-item.qtd-total-skid.

            /* Hora Normal */
            FIND tt-hora-normal WHERE
                tt-hora-normal.data = es-abastec-lin-item.data NO-ERROR.

            IF NOT AVAIL tt-hora-normal THEN DO:

                CREATE tt-hora-normal.
                ASSIGN tt-hora-normal.data     = es-abastec-lin-item.data
                       tt-hora-normal.hora-ini = es-abastec-lin-item.hora-ini
                       tt-hora-normal.hora-fim = es-abastec-lin-item.hora-ini.

            END. /* IF NOT AVAIL tt-hora-normal */

            IF es-abastec-lin-item.hora-ini < tt-hora-normal.hora-ini THEN
                tt-hora-normal.hora-ini = es-abastec-lin-item.hora-ini.

            IF es-abastec-lin-item.hora-ini > tt-hora-normal.hora-fim THEN
                tt-hora-normal.hora-fim = es-abastec-lin-item.hora-ini.
    
        END. /* FOR EACH es-abastec-lin-item */

        /* Paradas */
        FOR EACH es-abastec-lin-parada OF es-abastec-lin NO-LOCK:

            /* Hora Parada */
            CREATE tt-hora-parada.
            ASSIGN tt-hora-parada.cod-motivo = es-abastec-lin-parada.cod-motivo
                   tt-hora-parada.data       = es-abastec-lin-parada.data
                   tt-hora-parada.hora-ini   = es-abastec-lin-parada.hora-ini
                   tt-hora-parada.hora-fim   = es-abastec-lin-parada.hora-fim.

        END. /* FOR EACH es-abastec-lin-parada OF es-abastec-lin */

    END. /* FOR EACH es-abastec-lin */

    /*OUTPUT TO c:\temp\aaa.txt NO-CONVERT.*/

    FOR EACH tt-hora-normal:

        /*DISP tt-hora-normal
            WITH SCROLLABLE STREAM-IO.*/

        RUN pi-calc-hora (INPUT tt-hora-normal.hora-ini,
                          INPUT tt-hora-normal.hora-fim,
                          OUTPUT p-hora).

        /*DISP p-hora.*/

        d-horas = d-horas + p-hora.

    END.

    FOR EACH tt-hora-parada:

        /*DISP tt-hora-parada
            WITH SCROLLABLE STREAM-IO.*/
        
        RUN pi-calc-hora (INPUT tt-hora-parada.hora-ini,
                          INPUT tt-hora-parada.hora-fim,
                          OUTPUT p-hora).

        /*DISP p-hora.*/

        d-horas-paradas = d-horas-paradas + p-hora.

    END.

    d-skid-hora = d-total-skid / (d-horas - d-horas-paradas).

    IF d-skid-hora < 0 OR d-skid-hora = ? THEN
        d-skid-hora = 0.

    PUT STREAM str-csv UNFORMATTED
        SKIP(2)
        ";;;;Total de Skids Rodados:;"  STRING(d-total-skid)    SKIP
        ";;;;Total de Horas:;"          STRING(d-horas)         SKIP
        ";;;;Total de Horas paradas:;"  STRING(d-horas-paradas) SKIP
        ";;;;Skids por Hora:;"          STRING(d-skid-hora)     SKIP.

    /*OUTPUT CLOSE.

    DOS SILENT START notepad.exe c:\temp\aaa.txt.*/

END PROCEDURE. /* PROCEDURE pi-carregar-tt */

PROCEDURE pi-calc-hora:

    DEF INPUT  PARAM p-hora-ini  AS CHAR NO-UNDO.
    DEF INPUT  PARAM p-hora-fim  AS CHAR NO-UNDO.
    DEF OUTPUT PARAM p-hora-calc AS DEC  NO-UNDO.

    DEF VAR d-hora-ini AS DEC NO-UNDO.
    DEF VAR d-hora-fim AS DEC NO-UNDO.

    ASSIGN p-hora-ini = REPLACE(p-hora-ini,":","")
           p-hora-fim = REPLACE(p-hora-fim,":","").

    ASSIGN d-hora-ini = INT(SUBSTRING(p-hora-ini,1,2)) + (INT(SUBSTRING(p-hora-ini,3,2)) / 60).
           d-hora-fim = INT(SUBSTRING(p-hora-fim,1,2)) + (INT(SUBSTRING(p-hora-fim,3,2)) / 60).

    p-hora-calc = d-hora-fim - d-hora-ini.

END PROCEDURE. /* PROCEDURE pi-calc-hora */

PROCEDURE carregarCSV :

    DEF INPUT PARAM p-arq-csv AS CHAR NO-UNDO.
    DEF INPUT PARAM ch-excel AS office.iface.excel.ExcelWrapper NO-UNDO.

    DEF VAR i-linha AS INT NO-UNDO INIT 2.

    IF SEARCH(p-arq-csv) <> ? THEN DO:

        INPUT FROM VALUE(p-arq-csv) NO-CONVERT.

        REPEAT:

            CREATE tt-csv.

            IMPORT DELIMITER ';' tt-csv.

        END.

        INPUT CLOSE.

        FIND LAST tt-csv NO-ERROR.

        IF AVAIL tt-csv THEN DELETE tt-csv.

        FOR EACH tt-csv :

            i-linha = i-linha + 1.

            ASSIGN
                ch-excel:Worksheets(1):cells(i-linha, 1):integerVALUE =
                    tt-csv.nr-linha WHEN tt-csv.nr-linha > 0
                ch-excel:Worksheets(1):cells(i-linha, 2):dateVALUE =
                    tt-csv.data
                ch-excel:Worksheets(1):cells(i-linha, 3):VALUE =
                    tt-csv.it-codigo
                ch-excel:Worksheets(1):cells(i-linha, 4):VALUE =
                    tt-csv.c-codigo-refer
                ch-excel:Worksheets(1):cells(i-linha, 5):VALUE =
                    tt-csv.c-desc-item-pai
                ch-excel:Worksheets(1):cells(i-linha, 6):decimalVALUE =
                    tt-csv.quantidade
                ch-excel:Worksheets(1):cells(i-linha, 7):VALUE =
                    tt-csv.hora-ini
                ch-excel:Worksheets(1):cells(i-linha, 8):decimalVALUE =
                    tt-csv.qtd-peca-skid
                ch-excel:Worksheets(1):cells(i-linha, 9):decimalVALUE =
                    tt-csv.qtd-total-skid
                .

        END.

        ch-excel:Worksheets(1):Range("A:I"):EntireColumn:AutoFit().


    END.


END PROCEDURE.

{utils/retorna_diretorio_usuario.i}
