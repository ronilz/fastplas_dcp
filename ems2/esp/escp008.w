&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME w-livre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-livre 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESCP008 2.08.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i ESCP008 MCP}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR p-nr-linha-ini     LIKE es-abastec-lin-item.nr-linha  NO-UNDO.
DEF VAR p-nr-linha-fim     LIKE es-abastec-lin-item.nr-linha  NO-UNDO INIT 999.
DEF VAR p-data             LIKE es-abastec-lin-item.data      NO-UNDO INIT TODAY.
DEF VAR p-fm-codigo        LIKE ITEM.fm-codigo                NO-UNDO INIT "FM-50".
DEF VAR p-intervalo        AS INT                             NO-UNDO INIT 30.
DEF VAR l-atualizando      AS LOGICAL                         NO-UNDO.

DEF TEMP-TABLE tt-param-enapi205 NO-UNDO
    FIELD tipo                  AS INT
    FIELD nivel                 AS INT
    FIELD quant-itens           AS DEC
    FIELD validade              AS DATE
    FIELD item-compon           AS LOGICAL
    FIELD lista                 AS LOGICAL
    FIELD item-folha            AS LOGICAL
    FIELD ge-codigo-ini         AS INT
    FIELD ge-codigo-fim         AS INT
    FIELD fm-codigo-ini         AS CHAR
    FIELD fm-codigo-fim         AS CHAR
    FIELD it-codigo-ini         AS CHAR
    FIELD it-codigo-fim         AS CHAR
    FIELD descricao-ini         AS CHAR
    FIELD descricao-fim         AS CHAR
    FIELD ref-ini               AS CHAR
    FIELD ref-fim               AS CHAR
    FIELD ativo                 AS LOGICAL
    FIELD ordem-aut             AS LOGICAL
    FIELD tod-ordens            AS LOGICAL
    FIELD obsol-tot             AS LOGICAL
    FIELD lista-compon          AS LOGICAL
    FIELD desc-item             AS LOGICAL
    FIELD parametro             AS LOGICAL
    FIELD cod-unid-negoc-ini    AS CHAR
    FIELD cod-unid-negoc-fim    AS CHAR
    FIELD comprado              AS LOGICAL
    FIELD fabricado             AS LOGICAL
    FIELD processo              AS CHAR
    FIELD cod-estabel           AS CHAR
    FIELD nr-linha              AS INT
    FIELD cod-unid-negoc        AS CHAR
    FIELD c-processo            AS CHAR
    FIELD c-cod-estabel         AS CHAR
    FIELD i-nr-linha            AS INT
    FIELD c-cod-unid-negoc      AS CHAR.

DEF TEMP-TABLE tt-digita NO-UNDO
    FIELD it-codigo AS CHAR
    FIELD descricao AS CHAR
    FIELD un AS CHAR
    FIELD cod-refer AS CHAR
    FIELD data-corte AS DATE
    INDEX id
        it-codigo
        cod-refer.
    
DEF TEMP-TABLE tt-estrutura NO-UNDO
    FIELD i-sequen AS INT
    FIELD nivel AS CHAR
    FIELD it-codigo AS CHAR
    FIELD es-codigo AS CHAR
    FIELD sequencia AS INT
    FIELD chave AS CHAR
    FIELD chave-pai AS CHAR
    FIELD quantidade AS DEC
    FIELD quant-usada AS DEC
    FIELD quant-liquid AS DEC
    FIELD quant-sumariz AS DEC
    FIELD quant-liq-sum AS DEC
    FIELD fator-perda AS DEC
    FIELD cod-lista-compon AS CHAR
    FIELD cod-ref-it AS CHAR
    FIELD cod-ref-es AS CHAR
    FIELD revisao AS CHAR
    FIELD observacao AS CHAR
    FIELD log-quant-fix AS LOGICAL
    FIELD cod-unid-negoc AS CHAR
    FIELD row-estrutura AS ROWID
    FIELD fantasma AS CHAR
    FIELD data-valid AS CHAR
    FIELD alternativo AS CHAR
    FIELD compr-fabri AS CHAR
    INDEX tt-sum
        it-codigo
        es-codigo
        cod-ref-it
        cod-lista-compon.

DEF TEMP-TABLE tt-componente NO-UNDO
    FIELD nr-linha      LIKE es-abastec-lin-item.nr-linha
    FIELD data          LIKE es-abastec-lin-item.data
    FIELD nr-seq        LIKE es-abastec-lin-item.nr-seq
    FIELD it-codigo     LIKE ITEM.it-codigo
    FIELD desc-item     LIKE ITEM.desc-item
    FIELD quantidade    AS DEC  FORMAT ">>>,>>9.99" COLUMN-LABEL "Consumo Unit�rio"
    FIELD quant-usada   AS DEC  FORMAT ">>>,>>9.99" COLUMN-LABEL "Consumo Total"
    INDEX pky IS PRIMARY
        nr-linha 
        data     
        nr-seq   
        it-codigo.

DEF TEMP-TABLE tt-monitor NO-UNDO
    FIELD linha         AS INT
    FIELD tipo          AS INT
    FIELD nr-linha      LIKE es-abastec-lin-item.nr-linha
    FIELD desc-linha    LIKE lin-prod.descricao
    FIELD data          LIKE es-abastec-lin-item.data
    FIELD nr-seq        LIKE es-abastec-lin-item.nr-seq
    FIELD it-codigo     LIKE ITEM.it-codigo
    FIELD codigo-refer  AS CHAR FORMAT "x(20)" COLUMN-LABEL "C�digo Cliente"
    FIELD desc-item     LIKE ITEM.desc-item
    FIELD quantidade    AS DEC  FORMAT ">,>>>,>>9.9999" DECIMALS 4 COLUMN-LABEL "Quantidade"
    INDEX idx-lin
        linha.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE w-livre
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-cad
&Scoped-define BROWSE-NAME br-tt-monitor

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-monitor

/* Definitions for BROWSE br-tt-monitor                                 */
&Scoped-define FIELDS-IN-QUERY-br-tt-monitor /**/ /*tt-monitor.linha */ tt-monitor.nr-linha tt-monitor.desc-linha tt-monitor.data tt-monitor.nr-seq tt-monitor.it-codigo tt-monitor.codigo-refer tt-monitor.desc-item tt-monitor.quantidade   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-tt-monitor   
&Scoped-define SELF-NAME br-tt-monitor
&Scoped-define QUERY-STRING-br-tt-monitor FOR EACH tt-monitor
&Scoped-define OPEN-QUERY-br-tt-monitor OPEN QUERY {&SELF-NAME} FOR EACH tt-monitor.
&Scoped-define TABLES-IN-QUERY-br-tt-monitor tt-monitor
&Scoped-define FIRST-TABLE-IN-QUERY-br-tt-monitor tt-monitor


/* Definitions for FRAME f-cad                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f-cad ~
    ~{&OPEN-QUERY-br-tt-monitor}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rt-button RECT-11 bt-filtro br-tt-monitor ~
bt-liberar tg-atualiza-auto 
&Scoped-Define DISPLAYED-OBJECTS tg-atualiza-auto 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-livre AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU mi-programa 
       MENU-ITEM mi-consultas   LABEL "Co&nsultas"     ACCELERATOR "CTRL-L"
       MENU-ITEM mi-imprimir    LABEL "&Relat�rios"    ACCELERATOR "CTRL-P"
       RULE
       MENU-ITEM mi-sair        LABEL "&Sair"          ACCELERATOR "CTRL-X".

DEFINE SUB-MENU m_Ajuda 
       MENU-ITEM mi-conteudo    LABEL "&Conteudo"     
       MENU-ITEM mi-sobre       LABEL "&Sobre..."     .

DEFINE MENU m-livre MENUBAR
       SUB-MENU  mi-programa    LABEL "&Arquivo"      
       SUB-MENU  m_Ajuda        LABEL "&Ajuda"        .


/* Definitions of handles for OCX Containers                            */
DEFINE VARIABLE CtrlFrame AS WIDGET-HANDLE NO-UNDO.
DEFINE VARIABLE chCtrlFrame AS COMPONENT-HANDLE NO-UNDO.

/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_p-exihel AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-filtro 
     IMAGE-UP FILE "image/im-fil.bmp":U
     LABEL "Filtro" 
     SIZE 4 BY 1.13.

DEFINE BUTTON bt-liberar 
     LABEL "Liberar" 
     SIZE 15 BY 1.

DEFINE RECTANGLE RECT-11
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 123 BY 15.75.

DEFINE RECTANGLE rt-button
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 123 BY 1.46
     BGCOLOR 7 .

DEFINE VARIABLE tg-atualiza-auto AS LOGICAL INITIAL yes 
     LABEL "Atualiza��o autom�tica" 
     VIEW-AS TOGGLE-BOX
     SIZE 19 BY .83 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-tt-monitor FOR 
      tt-monitor SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-tt-monitor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-tt-monitor w-livre _FREEFORM
  QUERY br-tt-monitor DISPLAY
      /**/
    /*tt-monitor.linha      */
    tt-monitor.nr-linha   
    tt-monitor.desc-linha   WIDTH 16
    tt-monitor.data       
    tt-monitor.nr-seq     
    tt-monitor.it-codigo    WIDTH 18
    tt-monitor.codigo-refer
    tt-monitor.desc-item    WIDTH 37
    tt-monitor.quantidade
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 121 BY 14
         FONT 1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-cad
     bt-filtro AT ROW 1.17 COL 2 WIDGET-ID 10
     br-tt-monitor AT ROW 2.75 COL 2 WIDGET-ID 200
     bt-liberar AT ROW 16.92 COL 108 WIDGET-ID 16
     tg-atualiza-auto AT ROW 17 COL 3 WIDGET-ID 24
     rt-button AT ROW 1 COL 1
     RECT-11 AT ROW 2.5 COL 1 WIDGET-ID 12
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 123 BY 17.38
         FONT 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: w-livre
   Allow: Basic,Browse,DB-Fields,Smart,Window,Query
   Container Links: 
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-livre ASSIGN
         HIDDEN             = YES
         TITLE              = "Monitor Sala Pintura"
         HEIGHT             = 17.38
         WIDTH              = 123
         MAX-HEIGHT         = 17.38
         MAX-WIDTH          = 128
         VIRTUAL-HEIGHT     = 17.38
         VIRTUAL-WIDTH      = 128
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU m-livre:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-livre 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-livre.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-livre
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-cad
   FRAME-NAME L-To-R                                                    */
/* BROWSE-TAB br-tt-monitor bt-filtro f-cad */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-livre)
THEN w-livre:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-tt-monitor
/* Query rebuild information for BROWSE br-tt-monitor
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-monitor.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-tt-monitor */
&ANALYZE-RESUME

 


/* **********************  Create OCX Containers  ********************** */

&ANALYZE-SUSPEND _CREATE-DYNAMIC

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN

CREATE CONTROL-FRAME CtrlFrame ASSIGN
       FRAME           = FRAME f-cad:HANDLE
       ROW             = 1.04
       COLUMN          = 57
       HEIGHT          = 1.5
       WIDTH           = 5
       WIDGET-ID       = 22
       HIDDEN          = yes
       SENSITIVE       = yes.

PROCEDURE adm-create-controls:
      CtrlFrame:NAME = "CtrlFrame":U .
/* CtrlFrame OCXINFO:CREATE-CONTROL from: {F0B88A90-F5DA-11CF-B545-0020AF6ED35A} type: PSTimer */

END PROCEDURE.

&ENDIF

&ANALYZE-RESUME /* End of _CREATE-DYNAMIC */


/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-livre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-livre w-livre
ON END-ERROR OF w-livre /* Monitor Sala Pintura */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-livre w-livre
ON WINDOW-CLOSE OF w-livre /* Monitor Sala Pintura */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-tt-monitor
&Scoped-define SELF-NAME br-tt-monitor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-tt-monitor w-livre
ON ROW-DISPLAY OF br-tt-monitor IN FRAME f-cad
DO:
  
    CASE tt-monitor.tipo:

        WHEN 1 THEN DO: /* sequencia */

            ASSIGN tt-monitor.nr-linha:FGCOLOR   IN BROWSE br-tt-monitor = 0
                   tt-monitor.data:FGCOLOR       IN BROWSE br-tt-monitor = 0
                   tt-monitor.nr-seq:FGCOLOR     IN BROWSE br-tt-monitor = 0
                   tt-monitor.it-codigo:FGCOLOR  IN BROWSE br-tt-monitor = 0
                   tt-monitor.codigo-refer:FGCOLOR  IN BROWSE br-tt-monitor = 0
                   tt-monitor.desc-item:FGCOLOR  IN BROWSE br-tt-monitor = 0
                   tt-monitor.quantidade:FGCOLOR IN BROWSE br-tt-monitor = 0
                .

            ASSIGN tt-monitor.nr-linha:BGCOLOR   IN BROWSE br-tt-monitor = 15
                   tt-monitor.data:BGCOLOR       IN BROWSE br-tt-monitor = 15
                   tt-monitor.nr-seq:BGCOLOR     IN BROWSE br-tt-monitor = 15
                   tt-monitor.it-codigo:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.codigo-refer:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.desc-item:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.quantidade:BGCOLOR IN BROWSE br-tt-monitor = 15
                .

            ASSIGN tt-monitor.nr-linha:FONT   IN BROWSE br-tt-monitor = 1
                   tt-monitor.data:FONT       IN BROWSE br-tt-monitor = 1
                   tt-monitor.nr-seq:FONT     IN BROWSE br-tt-monitor = 1
                   tt-monitor.it-codigo:FONT  IN BROWSE br-tt-monitor = 1
                   tt-monitor.codigo-refer:FONT  IN BROWSE br-tt-monitor = 1
                   tt-monitor.desc-item:FONT  IN BROWSE br-tt-monitor = 1
                   tt-monitor.quantidade:FONT IN BROWSE br-tt-monitor = 1
                .

        END. /* WHEN 1 */

        WHEN 2 THEN DO: /* total */

            ASSIGN tt-monitor.nr-linha:FGCOLOR   IN BROWSE br-tt-monitor = 15
                   tt-monitor.data:FGCOLOR       IN BROWSE br-tt-monitor = 15
                   tt-monitor.nr-seq:FGCOLOR     IN BROWSE br-tt-monitor = 0
                   tt-monitor.it-codigo:FGCOLOR  IN BROWSE br-tt-monitor = 0
                   tt-monitor.codigo-refer:FGCOLOR  IN BROWSE br-tt-monitor = 0
                   tt-monitor.desc-item:FGCOLOR  IN BROWSE br-tt-monitor = 0
                   tt-monitor.quantidade:FGCOLOR IN BROWSE br-tt-monitor = 0
                .

            ASSIGN tt-monitor.nr-linha:BGCOLOR   IN BROWSE br-tt-monitor = 15
                   tt-monitor.data:BGCOLOR       IN BROWSE br-tt-monitor = 15
                   tt-monitor.nr-seq:BGCOLOR     IN BROWSE br-tt-monitor = 15
                   tt-monitor.it-codigo:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.codigo-refer:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.desc-item:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.quantidade:BGCOLOR IN BROWSE br-tt-monitor = 15
                .

            ASSIGN tt-monitor.nr-linha:FONT   IN BROWSE br-tt-monitor = 0
                   tt-monitor.data:FONT       IN BROWSE br-tt-monitor = 0
                   tt-monitor.nr-seq:FONT     IN BROWSE br-tt-monitor = 0
                   tt-monitor.it-codigo:FONT  IN BROWSE br-tt-monitor = 0
                   tt-monitor.codigo-refer:FONT  IN BROWSE br-tt-monitor = 0
                   tt-monitor.desc-item:FONT  IN BROWSE br-tt-monitor = 0
                   tt-monitor.quantidade:FONT IN BROWSE br-tt-monitor = 0
                .

        END. /* WHEN 2 */

        WHEN 3 THEN DO: /* branco */

            ASSIGN tt-monitor.nr-linha:FGCOLOR   IN BROWSE br-tt-monitor = 15
                   tt-monitor.data:FGCOLOR       IN BROWSE br-tt-monitor = 15
                   tt-monitor.nr-seq:FGCOLOR     IN BROWSE br-tt-monitor = 15
                   tt-monitor.it-codigo:FGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.codigo-refer:FGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.desc-item:FGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.quantidade:FGCOLOR IN BROWSE br-tt-monitor = 15
                .

            ASSIGN tt-monitor.nr-linha:BGCOLOR   IN BROWSE br-tt-monitor = 15
                   tt-monitor.data:BGCOLOR       IN BROWSE br-tt-monitor = 15
                   tt-monitor.nr-seq:BGCOLOR     IN BROWSE br-tt-monitor = 15
                   tt-monitor.it-codigo:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.codigo-refer:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.desc-item:BGCOLOR  IN BROWSE br-tt-monitor = 15
                   tt-monitor.quantidade:BGCOLOR IN BROWSE br-tt-monitor = 15
                .

            ASSIGN tt-monitor.nr-linha:FONT   IN BROWSE br-tt-monitor = 1
                   tt-monitor.data:FONT       IN BROWSE br-tt-monitor = 1
                   tt-monitor.nr-seq:FONT     IN BROWSE br-tt-monitor = 1
                   tt-monitor.it-codigo:FONT  IN BROWSE br-tt-monitor = 1
                   tt-monitor.codigo-refer:FONT  IN BROWSE br-tt-monitor = 1
                   tt-monitor.desc-item:FONT  IN BROWSE br-tt-monitor = 1
                   tt-monitor.quantidade:FONT IN BROWSE br-tt-monitor = 1
                .

        END. /* WHEN 3 */

    END CASE. /* CASE tt-monitor.tipo */
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-filtro
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-filtro w-livre
ON CHOOSE OF bt-filtro IN FRAME f-cad /* Filtro */
DO:
    DEF VAR p-ok AS LOGICAL NO-UNDO.
  
    RUN esp/escp008a.w (INPUT-OUTPUT p-nr-linha-ini, 
                        INPUT-OUTPUT p-nr-linha-fim, 
                        INPUT-OUTPUT p-data,
                        INPUT-OUTPUT p-fm-codigo,
                        INPUT-OUTPUT p-intervalo,
                        OUTPUT p-ok).

    IF NOT p-ok THEN
        RETURN.

    chCtrlFrame:PStimer:INTERVAL = p-intervalo * 1000.

    RUN pi-criar-tt-monitor.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-liberar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-liberar w-livre
ON CHOOSE OF bt-liberar IN FRAME f-cad /* Liberar */
DO:
    DEF BUFFER b-es-abastec-lin-item FOR es-abastec-lin-item.

    IF AVAIL tt-monitor     AND 
       tt-monitor.tipo <> 3 THEN DO:
  
        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 27100,
                           INPUT "Liberar sequ�ncia '" + STRING(tt-monitor.nr-seq) + "'?~~" +
                                 "Libera itens da sequ�ncia.").
    
        IF LOGICAL(RETURN-VALUE) THEN DO:
    
            FOR EACH b-es-abastec-lin-item WHERE
                b-es-abastec-lin-item.nr-linha = tt-monitor.nr-linha AND
                b-es-abastec-lin-item.data     = tt-monitor.data     AND 
                b-es-abastec-lin-item.nr-seq   = tt-monitor.nr-seq   NO-LOCK:
    
                FIND es-abastec-lin-item OF b-es-abastec-lin-item EXCLUSIVE-LOCK NO-ERROR.
    
                es-abastec-lin-item.liberado = TRUE.
    
                RELEASE es-abastec-lin-item.
    
            END. /* FOR EACH b-es-abastec-lin-item */
    
            RUN pi-criar-tt-monitor.
    
        END. /* IF LOGICAL(RETURN-VALUE) */

    END. /* ELSE IF NOT CAN-FIND(FIRST b-es-abastec-lin-item */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CtrlFrame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CtrlFrame w-livre OCX.Tick
PROCEDURE CtrlFrame.PSTimer.Tick .
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  None required for OCX.
  Notes:       
------------------------------------------------------------------------------*/

    RUN pi-criar-tt-monitor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-consultas
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-consultas w-livre
ON CHOOSE OF MENU-ITEM mi-consultas /* Consultas */
DO:
  RUN pi-consulta IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-conteudo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-conteudo w-livre
ON CHOOSE OF MENU-ITEM mi-conteudo /* Conteudo */
OR HELP OF FRAME {&FRAME-NAME}
DO:
  RUN pi-ajuda IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-imprimir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-imprimir w-livre
ON CHOOSE OF MENU-ITEM mi-imprimir /* Relat�rios */
DO:
  RUN pi-imprimir IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-programa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-programa w-livre
ON MENU-DROP OF MENU mi-programa /* Arquivo */
DO:
  run pi-disable-menu.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sair w-livre
ON CHOOSE OF MENU-ITEM mi-sair /* Sair */
DO:
  RUN pi-sair IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sobre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sobre w-livre
ON CHOOSE OF MENU-ITEM mi-sobre /* Sobre... */
DO:
  {include/sobre.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tg-atualiza-auto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tg-atualiza-auto w-livre
ON VALUE-CHANGED OF tg-atualiza-auto IN FRAME f-cad /* Atualiza��o autom�tica */
DO:

    chCtrlFrame:PStimer:ENABLED = SELF:CHECKED.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-livre 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-livre  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'panel/p-exihel.w':U ,
             INPUT  FRAME f-cad:HANDLE ,
             INPUT  'Edge-Pixels = 2,
                     SmartPanelType = NAV-ICON,
                     Right-to-Left = First-On-Left':U ,
             OUTPUT h_p-exihel ).
       RUN set-position IN h_p-exihel ( 1.17 , 107.00 ) NO-ERROR.
       /* Size in UIB:  ( 1.25 , 16.00 ) */

       /* Links to SmartPanel h_p-exihel. */
       RUN add-link IN adm-broker-hdl ( h_p-exihel , 'State':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-exihel ,
             bt-filtro:HANDLE IN FRAME f-cad , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-livre  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE control_load w-livre  _CONTROL-LOAD
PROCEDURE control_load :
/*------------------------------------------------------------------------------
  Purpose:     Load the OCXs    
  Parameters:  <none>
  Notes:       Here we load, initialize and make visible the 
               OCXs in the interface.                        
------------------------------------------------------------------------------*/

&IF "{&OPSYS}" = "WIN32":U AND "{&WINDOW-SYSTEM}" NE "TTY":U &THEN
DEFINE VARIABLE UIB_S    AS LOGICAL    NO-UNDO.
DEFINE VARIABLE OCXFile  AS CHARACTER  NO-UNDO.

OCXFile = SEARCH( "escp008.wrx":U ).
IF OCXFile = ? THEN
  OCXFile = SEARCH(SUBSTRING(THIS-PROCEDURE:FILE-NAME, 1,
                     R-INDEX(THIS-PROCEDURE:FILE-NAME, ".":U), "CHARACTER":U) + "wrx":U).

IF OCXFile <> ? THEN
DO:
  ASSIGN
    chCtrlFrame = CtrlFrame:COM-HANDLE
    UIB_S = chCtrlFrame:LoadControls( OCXFile, "CtrlFrame":U)
  .
  RUN DISPATCH IN THIS-PROCEDURE("initialize-controls":U) NO-ERROR.
END.
ELSE MESSAGE "escp008.wrx":U SKIP(1)
             "The binary control file could not be found. The controls cannot be loaded."
             VIEW-AS ALERT-BOX TITLE "Controls Not Loaded".

&ENDIF

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-livre  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-livre)
  THEN DELETE WIDGET w-livre.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-livre  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY tg-atualiza-auto 
      WITH FRAME f-cad IN WINDOW w-livre.
  ENABLE rt-button RECT-11 bt-filtro br-tt-monitor bt-liberar tg-atualiza-auto 
      WITH FRAME f-cad IN WINDOW w-livre.
  {&OPEN-BROWSERS-IN-QUERY-f-cad}
  VIEW w-livre.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy w-livre 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-livre 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  
  RETURN.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize w-livre 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  run pi-before-initialize.

  {include/win-size.i}

  {utp/ut9000.i "ESCP008" "2.08.00.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  run pi-after-initialize.

  chCtrlFrame:PStimer:ENABLED = TRUE.
  chCtrlFrame:PStimer:INTERVAL = p-intervalo * 1000.

  RUN pi-criar-tt-monitor.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-criar-tt-monitor w-livre 
PROCEDURE pi-criar-tt-monitor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEF VAR h-acomp AS HANDLE NO-UNDO.
    DEF VAR i-lin   AS INT    NO-UNDO.

    IF l-atualizando THEN
        LEAVE.

    l-atualizando = TRUE.

    RUN utp/ut-acomp.p PERSISTENT SET h-acomp.

    RUN pi-inicializar IN h-acomp (INPUT "Processando...").

    RUN pi-desabilita-cancela IN h-acomp.

    EMPTY TEMP-TABLE tt-monitor.

    FOR EACH es-abastec-lin-item WHERE
        es-abastec-lin-item.nr-linha    >= p-nr-linha-ini  AND
        es-abastec-lin-item.nr-linha    <= p-nr-linha-fim  AND
        es-abastec-lin-item.data         = p-data          AND
        es-abastec-lin-item.nr-seq      <> 0               AND 
        es-abastec-lin-item.liberado     = FALSE           NO-LOCK
        BREAK BY es-abastec-lin-item.nr-linha
              BY es-abastec-lin-item.data
              BY es-abastec-lin-item.nr-seq DESC:

        IF FIRST-OF(es-abastec-lin-item.nr-seq) THEN DO:

            EMPTY TEMP-TABLE tt-componente.

        END. /* IF FIRST-OF(es-abastec-lin-item.nr-seq) */

        /*IF p-liberado = 1 AND NOT es-abastec-lin-item.liberado OR 
           p-liberado = 2 AND es-abastec-lin-item.liberado     THEN
            NEXT.*/

        RUN pi-acompanhar IN h-acomp (INPUT "Linha/Data/Seq/Item: " + 
                                            STRING(es-abastec-lin-item.nr-linha) + "/" +
                                            STRING(es-abastec-lin-item.data,"99/99/9999") + "/" +
                                            STRING(es-abastec-lin-item.nr-seq) + "/" +
                                            es-abastec-lin-item.it-codigo).

        i-lin = i-lin + 1.

        FOR FIRST lin-prod WHERE
            lin-prod.nr-linha = es-abastec-lin-item.nr-linha NO-LOCK:
        END.

        CREATE tt-monitor.
        ASSIGN tt-monitor.linha       = i-lin
               tt-monitor.tipo        = 1
               tt-monitor.nr-linha    = es-abastec-lin-item.nr-linha
               tt-monitor.desc-linha  = lin-prod.descricao WHEN AVAIL lin-prod
               tt-monitor.data        = es-abastec-lin-item.data
               tt-monitor.nr-seq      = es-abastec-lin-item.nr-seq
               tt-monitor.it-codigo   = es-abastec-lin-item.it-codigo
               tt-monitor.quantidade  = es-abastec-lin-item.quantidade.

        FOR FIRST ITEM FIELDS(desc-item codigo-refer) WHERE
            ITEM.it-codigo = es-abastec-lin-item.it-codigo NO-LOCK:
            ASSIGN tt-monitor.desc-item    = ITEM.desc-item
                   tt-monitor.codigo-refer = ITEM.codigo-refer.
        END. /* FOR FIRST ITEM */

        EMPTY TEMP-TABLE tt-param-enapi205.

        CREATE tt-param-enapi205.
        ASSIGN /*tt-param-enapi205.tipo                 = */
               tt-param-enapi205.nivel                = 20
               tt-param-enapi205.quant-itens          = es-abastec-lin-item.quantidade
               /*tt-param-enapi205.validade             = */
               tt-param-enapi205.item-compon          = TRUE
               tt-param-enapi205.lista                = TRUE
               tt-param-enapi205.item-folha           = TRUE
               tt-param-enapi205.ge-codigo-ini        = 0
               tt-param-enapi205.ge-codigo-fim        = 99
               tt-param-enapi205.fm-codigo-ini        = ""
               tt-param-enapi205.fm-codigo-fim        = "Z"
               tt-param-enapi205.it-codigo-ini        = "" /*es-abastec-lin-item.it-codigo*/
               tt-param-enapi205.it-codigo-fim        = "Z" /*es-abastec-lin-item.it-codigo*/
               tt-param-enapi205.descricao-ini        = ""
               tt-param-enapi205.descricao-fim        = "Z"
               tt-param-enapi205.ref-ini              = ""
               tt-param-enapi205.ref-fim              = "Z"
               tt-param-enapi205.ativo                = TRUE
               tt-param-enapi205.ordem-aut            = TRUE
               tt-param-enapi205.tod-ordens           = TRUE
               tt-param-enapi205.obsol-tot            = TRUE
               /*tt-param-enapi205.lista-compon         = 
               tt-param-enapi205.desc-item            = 
               tt-param-enapi205.parametro            = */
               tt-param-enapi205.cod-unid-negoc-ini   = ""
               tt-param-enapi205.cod-unid-negoc-fim   = "Z"
               tt-param-enapi205.comprado             = TRUE
               tt-param-enapi205.fabricado            = TRUE
               /*tt-param-enapi205.processo             = 
               tt-param-enapi205.cod-estabel          = 
               tt-param-enapi205.nr-linha             = 
               tt-param-enapi205.cod-unid-negoc       = 
               tt-param-enapi205.c-processo           = 
               tt-param-enapi205.c-cod-estabel        = 
               tt-param-enapi205.i-nr-linha           = 
               tt-param-enapi205.c-cod-unid-negoc     = 
            */
            .

        EMPTY TEMP-TABLE tt-digita.

        CREATE tt-digita.
        ASSIGN tt-digita.it-codigo = es-abastec-lin-item.it-codigo.

        RUN pi-acompanhar IN h-acomp (INPUT "Buscando estrutura: " + es-abastec-lin-item.it-codigo).

        RUN enp/enapi205.p (INPUT  TABLE tt-param-enapi205,
                            INPUT  TABLE tt-digita,
                            OUTPUT TABLE tt-estrutura).

        /* a api padrao est� deixando handles dos programas cdapi024 e enapi607
           a procedure abaixo elimina esses handles */
        RUN pi-delete-handle.

        FOR EACH tt-estrutura,
            FIRST ITEM FIELDS(desc-item fm-codigo) WHERE
                ITEM.it-codigo = tt-estrutura.es-codigo NO-LOCK:

            IF p-fm-codigo <> ""              AND
               p-fm-codigo <> ITEM.fm-codigo  THEN
                NEXT.

            FIND tt-componente WHERE
                tt-componente.nr-linha    = es-abastec-lin-item.nr-linha    AND
                tt-componente.data        = es-abastec-lin-item.data        AND
                tt-componente.nr-seq      = es-abastec-lin-item.nr-seq      AND
                tt-componente.it-codigo   = tt-estrutura.es-codigo          NO-ERROR.

            IF NOT AVAIL tt-componente THEN DO:

                CREATE tt-componente.
                ASSIGN tt-componente.nr-linha    = es-abastec-lin-item.nr-linha
                       tt-componente.data        = es-abastec-lin-item.data
                       tt-componente.nr-seq      = es-abastec-lin-item.nr-seq
                       tt-componente.it-codigo   = tt-estrutura.es-codigo
                       tt-componente.desc-item   = ITEM.desc-item.

            END. /* IF NOT AVAIL tt-componente */

            ASSIGN tt-componente.quantidade  = tt-componente.quantidade + tt-estrutura.quantidade
                   tt-componente.quant-usada = tt-componente.quant-usada + tt-estrutura.quant-usada.

        END. /* FOR EACH tt-estrutura */

        IF LAST-OF(es-abastec-lin-item.nr-seq) THEN DO:

            i-lin = i-lin + 1.

            CREATE tt-monitor.
            ASSIGN tt-monitor.linha = i-lin
                   tt-monitor.tipo  = 3.

            FOR EACH tt-componente:

                i-lin = i-lin + 1.

                CREATE tt-monitor.
                ASSIGN tt-monitor.linha       = i-lin
                       tt-monitor.tipo        = 2
                       tt-monitor.nr-linha    = es-abastec-lin-item.nr-linha
                       tt-monitor.data        = es-abastec-lin-item.data
                       tt-monitor.nr-seq      = es-abastec-lin-item.nr-seq
                       tt-monitor.it-codigo   = tt-componente.it-codigo
                       tt-monitor.desc-item   = tt-componente.desc-item
                       tt-monitor.quantidade  = tt-componente.quant-usada.

            END. /* FOR EACH tt-componente */

            i-lin = i-lin + 1.

            CREATE tt-monitor.
            ASSIGN tt-monitor.linha = i-lin
                   tt-monitor.tipo  = 3.

        END. /* IF LAST-OF(es-abastec-lin-item.nr-seq) */
        
    END. /* FOR EACH es-abastec-lin-item */

    RUN pi-finalizar IN h-acomp.

    {&OPEN-QUERY-br-tt-monitor}

    l-atualizando = FALSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-delete-handle w-livre 
PROCEDURE pi-delete-handle :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEF VAR h1 AS HANDLE NO-UNDO.
    DEF VAR h2 AS HANDLE NO-UNDO.
    
    h1 = SESSION:FIRST-PROCEDURE.
    
    DO WHILE VALID-HANDLE(h1):
    
        IF h1:FILE-NAME MATCHES "*cdapi024.*" OR
           h1:FILE-NAME MATCHES "*enapi607.*" THEN
            h2 = h1.
        ELSE
            h2 = ?.
    
        h1 = h1:NEXT-SIBLING.
    
        IF h2 <> ? THEN
            DELETE PROCEDURE h2.
    
    END. /* DO WHILE VALID-HANDLE(h1) */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-livre  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-monitor"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-livre 
PROCEDURE state-changed :
/*:T -----------------------------------------------------------
  Purpose:     Manuseia trocas de estado dos SmartObjects
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

