define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD nome-abrev-ini   AS CHAR
    FIELD nome-abrev-fim   AS CHAR
    FIELD cod-emit-ini     AS INTEGER
    FIELD cod-emit-fim     AS INTEGER
    FIELD fm-codigo-ini    AS CHAR
    FIELD fm-codigo-fim    AS CHAR
    FIELD it-codigo-ini    AS CHAR
    FIELD it-codigo-fim    AS CHAR
    FIELD simulacao        AS LOG
    INDEX i-param  nome-abrev-ini 
    INDEX i-param2 nome-abrev-fim.

define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.
