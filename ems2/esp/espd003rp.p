/*****************************************************************************
* Empresa  : FASTPLAS
* Programa : ESPD003RP
* Descricao: IMPORTACAO PEDIDOS - TODOS OS CLIENTES
* Autor    : DATASUL METROPOLITANA
* Data     : MARCO/2003
* Versao   : 2.04.00.000 - Carlos Alberto - Versao inicial.
******************************************************************************/

/*INCLUDE DE CONTROLE DE VERS�O */
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPD003RP 2.06.00.000}

Define Temp-Table tt-pedidos   No-Undo
        Field nr-pedcli         Like ped-venda.nr-pedcli
        Field nr-sequencia      Like ped-item.nr-sequencia
        Field it-codigo         Like item.it-codigo
        Field qt-pedida         As   Decimal Format ">>>>,>>9.99"
        Field cod-cli           Like es-pedido-cli.cod-cli
        Field cod-emitente      Like emitente.cod-emitente
        Field observacao        As   Char Format "x(60)"
        Field situacao          As   Integer
        FIELD cod-ord-compra    AS   CHAR
        FIELD parcela           AS   INT
        Index idx-codigo        Is Unique Primary cod-emitente
                                                  nr-pedcli
                                                  nr-sequencia
        Index idx-situacao                        situacao.

/*DEFINI��O DAS TEMP-TABLES PARA RECEBIMENTO DE PARAMETROS */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD todos            AS INT
    FIELD arq-entrada      AS CHAR FORMAT "x(35)"
    FIELD arq-destino      AS CHAR FORMAT "x(35)".

DEFINE TEMP-TABLE tt-raw-digita 
    FIELD raw-digita AS RAW.

/*RECEBIMENTO DE PARAMETROS */
DEFINE INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEFINE INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param NO-ERROR.

/*INCLUDE PADR�O PARA VARIAVEIS PARA O LOG*/
{include/i-rpvar.i}

/*DEFINI��O DE VARIAVEIS E STREAMS */
DEFINE STREAM s-imp.

DEFINE VARIABLE h-acomp        AS HANDLE                             NO-UNDO.
DEFINE VARIABLE c-linha        AS CHARACTER                          NO-UNDO.
DEFINE VARIABLE c-cgc          AS CHARACTER                          NO-UNDO.
DEFINE VARIABLE c-it-codigo    AS CHARACTER                          NO-UNDO.
DEFINE VARIABLE i-nr-ord       AS INTEGER                            NO-UNDO.
DEFINE VARIABLE l-confirma     AS LOGICAL   FORMAT "Sim/Nao" INIT NO NO-UNDO.
DEFINE VARIABLE c-cod-emitente AS CHARACTER FORMAT "x(06)"           NO-UNDO.

/*DEFINI��O DE FRAMES DO LOG */
/*INCLUDE PADR�O PARA O OUTPUT DO LOG*/

{include/i-rpout.i & STREAM="stream str-rp" &TOFILE=tt-param.arq-destino} 

/*INCLUDE COM A DEFINI��O DA FRAME DE CABE�ALHO E RODAPE */
{include/i-rpcab.i &STREAM="str-rp"}

FIND FIRST param-global NO-LOCK NO-ERROR.

FIND empresa 
    WHERE empresa.ep-codigo = param-global.empresa-prin NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ESPD003RP"
       c-versao       = "2.06"
       c-revisao      = ".00.000"
       c-empresa      = empresa.razao-social
       c-sistema      = "Espec�fico"
       c-titulo-relat = "Importa��o de Pedidos".

VIEW STREAM str-rp FRAME f-cabec.
VIEW STREAM str-rp FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Importando *}

FIND FIRST tt-param NO-LOCK NO-ERROR.

FIND FIRST es-pedido-cli USE-INDEX idx-arquivo WHERE
    es-pedido-cli.nome-arq = tt-param.arq-entrada
    NO-LOCK NO-ERROR.

IF  AVAIL es-pedido-cli THEN DO:
    MESSAGE "Atencao. Ja existem pedidos importados de arquivo com este"
         "nome. Confirma ?" VIEW-AS ALERT-BOX QUESTION BUTTON YES-NO UPDATE l-confirma.
    IF  NOT l-confirma THEN
        RETURN.
END.

FOR EACH tt-pedidos EXCLUSIVE-LOCK:
    DELETE tt-pedidos.
END.

RUN pi-inicializar IN h-acomp (INPUT RETURN-VALUE).
/*define a saida para o arquivo de saida informado na pagina de parametros*/

INPUT STREAM s-imp FROM VALUE (tt-param.arq-entrada).
/*BLOCO PRINCIPAL DO PROGRAMA */

REPEAT: /* ON STOP  UNDO, LEAVE:*/

    IMPORT STREAM s-imp UNFORMATTED c-linha.
    RUN pi-acompanhar IN h-acomp (INPUT "Cliente: " + SUBSTRING(c-linha,59,06) +
                                        " Pedido Nr. " + SUBSTRING(c-linha,01,12)).

    find first tt-pedidos
        where tt-pedidos.nr-pedcli    = SUBSTRING(c-linha,01,12)
        and   tt-pedidos.cod-emitente = int(SUBSTRING(c-linha,59,06))
        and   tt-pedidos.nr-sequencia = INT(SUBSTRING(c-linha,13,05)) no-error.
        
    if  not avail tt-pedidos then
        CREATE tt-pedidos.
        
    ASSIGN tt-pedidos.nr-pedcli      = SUBSTRING(c-linha,01,12)
           tt-pedidos.nr-sequencia   = INT(SUBSTRING(c-linha,13,05))
           tt-pedidos.it-codigo      = SUBSTRING(c-linha,19,19)
           tt-pedidos.qt-pedida      = DEC(SUBSTRING(c-linha,49,10)) /* / 100 */
           tt-pedidos.cod-cli        = SUBSTRING(c-linha,59,06)
           tt-pedidos.cod-ord-compra = SUBSTRING(c-linha,65,16)
           tt-pedidos.parcela        = INT(SUBSTRING(c-linha,81,06)).
    

                                    /*"1" + SUBSTRING(tt-pedidos.cod-cli,2,5)*/
    ASSIGN c-cod-emitente          = tt-pedidos.cod-cli        
           tt-pedidos.cod-emitente = INT(c-cod-emitente).

    /*
    IF  SUBSTRING(tt-param.arq-entrada,1,10) = "pedinterno" THEN 
    DO:
        ASSIGN c-cod-emitente          = tt-pedidos.cod-cli.
               tt-pedidos.cod-emitente = INT(c-cod-emitente).
    END.
    */
end.
    
for each tt-pedidos:

    RUN pi-acompanhar IN h-acomp (INPUT "Atualizando Cliente: " + string(tt-pedidos.cod-emitente)).

    FIND FIRST emitente WHERE
         emitente.cod-emitente = tt-pedidos.cod-emitente
         NO-LOCK NO-ERROR.
    IF  NOT AVAIL emitente THEN DO:
        ASSIGN tt-pedidos.situacao   = 9
               tt-pedidos.observacao = "Cliente: " + TRIM(STRING(tt-pedidos.cod-emitente)) + " nao cadastrado.".
        NEXT.
    END.
                 
    FIND ITEM WHERE
        ITEM.it-codigo = tt-pedidos.it-codigo
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL ITEM THEN DO:
        ASSIGN tt-pedidos.situacao   = 9
               tt-pedidos.observacao = "Item: " + TRIM(tt-pedidos.it-codigo) + " nao cadastrado.".
        NEXT.
    END.
                 
    FIND es-pedido-cli WHERE
         es-pedido-cli.cod-cli    = tt-pedidos.cod-cli      AND
         es-pedido-cli.nr-pedcli  = tt-pedidos.nr-pedcli    AND
         es-pedido-cli.linha      = tt-pedidos.nr-sequencia
         NO-LOCK NO-ERROR.
    IF  AVAIL es-pedido-cli THEN DO:
        ASSIGN tt-pedidos.situacao   = 9
               tt-pedidos.observacao = "Pedido: " + TRIM(tt-pedidos.nr-pedcli) + "Cliente: " + TRIM(tt-pedidos.cod-cli) + 
                                       " Seq: "   + TRIM(STRING(tt-pedidos.nr-sequencia)) +
                                       " ja importado p/tabela temporaria (es-pedido-cli).".
        NEXT.
    END.
                 
    FIND ped-venda WHERE
         ped-venda.nome-abrev = emitente.nome-abrev AND
         ped-venda.nr-pedcli  = tt-pedidos.nr-pedcli
         NO-LOCK NO-ERROR.
    IF  AVAIL ped-venda THEN DO:
        ASSIGN tt-pedidos.situacao   = 9
               tt-pedidos.observacao = "Pedido: " + TRIM(tt-pedidos.nr-pedcli) + "Cliente: " + TRIM(tt-pedidos.cod-cli) + 
                                       " Seq: "   + TRIM(STRING(tt-pedidos.nr-sequencia)) +
                                       " ja cadastrado.".
        NEXT.
    END.
                 
    IF  tt-pedidos.qt-pedida = 0 THEN DO:
        ASSIGN tt-pedidos.situacao   = 9
               tt-pedidos.observacao = "Pedido: " + TRIM(tt-pedidos.nr-pedcli) + "Cliente: " + TRIM(tt-pedidos.cod-cli) + 
                                       " Seq: "   + TRIM(STRING(tt-pedidos.nr-sequencia)) +
                                       " com Quant.pedida deve ser diferente de 0 (ZERO).".
        NEXT.
    END.
    ASSIGN tt-pedidos.situacao   = 1
           tt-pedidos.observacao = "PENDENTE DE CONFIRMACAO.".
END.
INPUT STREAM s-imp CLOSE.


FOR EACH tt-pedidos 
    WHERE tt-pedidos.situacao = 1 NO-LOCK:

    CREATE es-pedido-cli.
    ASSIGN es-pedido-cli.nr-pedcli      = tt-pedidos.nr-pedcli
           es-pedido-cli.linha          = tt-pedidos.nr-sequencia
           es-pedido-cli.it-codigo      = tt-pedidos.it-codigo
           es-pedido-cli.qt-pedida      = tt-pedidos.qt-pedida
           es-pedido-cli.cod-cli        = tt-pedidos.cod-cli
           es-pedido-cli.cod-emitente   = tt-pedidos.cod-emitente
           es-pedido-cli.nome-arq       = tt-param.arq-entrada
           es-pedido-cli.user-import    = USERID("mgadm")
           es-pedido-cli.data-import    = TODAY
           es-pedido-cli.hora-import    = STRING(TIME,"HH:MM")
           es-pedido-cli.situacao       = 1
           es-pedido-cli.cod-ord-compra = tt-pedidos.cod-ord-compra
           es-pedido-cli.parcela        = tt-pedidos.parcela       .
END.

IF  CAN-FIND(FIRST tt-pedidos WHERE
                   tt-pedidos.situacao = 9) THEN DO:

    ASSIGN c-titulo-relat = "PEDIDOS - REJEITADOS".
                                                                  
    VIEW STREAM str-rp FRAME f-cabec.  PAUSE 0.
    VIEW STREAM str-rp FRAME f-rodape. PAUSE 0.

    FOR EACH tt-pedidos 
        WHERE tt-pedidos.situacao = 9 NO-LOCK:

        IF  tt-pedidos.nr-pedcli = "" THEN DO:
            DELETE tt-pedidos.
            NEXT.
        END.

        DISPLAY STREAM str-rp
                tt-pedidos.nr-pedcli       COLUMN-LABEL "Pedido"
                tt-pedidos.nr-sequencia    COLUMN-LABEL "Linha"
                tt-pedidos.it-codigo
                tt-pedidos.qt-pedida       COLUMN-LABEL "Quantidade" FORMAT ">>>>,>>9.99"
                tt-pedidos.cod-cli         COLUMN-LABEL "Cliente"
                tt-pedidos.cod-emitente    COLUMN-LABEL "Emitente"
                tt-pedidos.observacao      COLUMN-LABEL "Observacao"
                tt-pedidos.cod-ord-compra  COLUMN-LABEL "Ordem Compra"
                tt-pedidos.parcela         COLUMN-LABEL "Parcela"
                WITH WIDTH 132 FRAME f-impressao DOWN STREAM-IO.
        DOWN STREAM str-rp WITH FRAME f-impressao. 
    END.
    PAGE.
END.
           
IF  tt-param.todos = 1        AND
    CAN-FIND(FIRST tt-pedidos WHERE
                   tt-pedidos.situacao = 1) THEN DO:

    ASSIGN c-titulo-relat = "PEDIDOS - LIBERADOS PARA CONFIRMACAO".

    VIEW STREAM str-rp FRAME f-cabec.  PAUSE 0.
    VIEW STREAM str-rp FRAME f-rodape. PAUSE 0.

    FOR EACH tt-pedidos 
        WHERE tt-pedidos.situacao = 1 NO-LOCK:

        DISPLAY STREAM str-rp
                tt-pedidos.nr-pedcli       COLUMN-LABEL "Pedido"
                tt-pedidos.nr-sequencia    COLUMN-LABEL "Linha"
                tt-pedidos.it-codigo
                tt-pedidos.qt-pedida       COLUMN-LABEL "Quantidade" FORMAT ">>>>,>>9.99"
                tt-pedidos.cod-cli         COLUMN-LABEL "Cliente"
                tt-pedidos.cod-emitente    COLUMN-LABEL "Emitente"
                tt-pedidos.observacao      COLUMN-LABEL "Observacao"
                tt-pedidos.cod-ord-compra  COLUMN-LABEL "Ordem Compra"
                tt-pedidos.parcela         COLUMN-LABEL "Parcela"
                WITH WIDTH 132 FRAME f-impressao-ok DOWN STREAM-IO .
        DOWN STREAM str-rp WITH FRAME f_impressao-ok.
    END.
END.

{include/i-rpclo.i &STREAM="STREAM str-rp"} 
RUN pi-finalizar IN h-acomp.
RETURN "OK":U.
