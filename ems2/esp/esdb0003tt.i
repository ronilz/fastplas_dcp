define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD c-cod-estabel-ini  AS CHAR
    FIELD c-cod-estabel-fim  AS CHAR
    FIELD i-cod-emitente-ini AS INT
    FIELD i-cod-emitente-fim AS INT
    FIELD c-it-codigo-ini    AS CHAR
    FIELD c-it-codigo-fim    AS CHAR
    FIELD dt-programa-ini     AS DATE
    FIELD dt-programa-fim     AS DATE
    FIELD i-num-programa-ini AS CHAR
    FIELD i-num-programa-fim AS CHAR
    FIELD i-funcao           AS INT /* 1-exportacao 2-importacao */
    FIELD c-arq-import       AS CHAR
    FIELD c-cod-cenario      AS CHAR
    .
    
define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.
