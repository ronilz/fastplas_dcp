&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESCP006C-V01 2.08.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i ESCP006C-V01 MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES es-abastec-lin-parada
&Scoped-define FIRST-EXTERNAL-TABLE es-abastec-lin-parada


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR es-abastec-lin-parada.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS es-abastec-lin-parada.hora-ini ~
es-abastec-lin-parada.hora-fim 
&Scoped-define ENABLED-TABLES es-abastec-lin-parada
&Scoped-define FIRST-ENABLED-TABLE es-abastec-lin-parada
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold 
&Scoped-Define DISPLAYED-FIELDS es-abastec-lin-parada.nr-linha ~
es-abastec-lin-parada.data es-abastec-lin-parada.cod-motivo ~
es-abastec-lin-parada.hora-ini es-abastec-lin-parada.hora-fim 
&Scoped-define DISPLAYED-TABLES es-abastec-lin-parada
&Scoped-define FIRST-DISPLAYED-TABLE es-abastec-lin-parada
&Scoped-Define DISPLAYED-OBJECTS fi-desc-linha fi-des-motivo 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-CREATE-FIELDS es-abastec-lin-parada.cod-motivo 
&Scoped-define ADM-ASSIGN-FIELDS es-abastec-lin-parada.cod-motivo 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fi-des-motivo AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 55 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-linha AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 55 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 3.5.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 2.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     es-abastec-lin-parada.nr-linha AT ROW 1.25 COL 19 COLON-ALIGNED WIDGET-ID 10
          VIEW-AS FILL-IN 
          SIZE 4.57 BY .88
     fi-desc-linha AT ROW 1.25 COL 24 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     es-abastec-lin-parada.data AT ROW 2.25 COL 19 COLON-ALIGNED WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 10 BY .88
     es-abastec-lin-parada.cod-motivo AT ROW 3.25 COL 19 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 4.57 BY .88
     fi-des-motivo AT ROW 3.25 COL 24 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     es-abastec-lin-parada.hora-ini AT ROW 4.75 COL 19 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     es-abastec-lin-parada.hora-fim AT ROW 5.75 COL 19 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 4.5 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.es-abastec-lin-parada
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 6
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN es-abastec-lin-parada.cod-motivo IN FRAME f-main
   NO-ENABLE 1 2                                                        */
/* SETTINGS FOR FILL-IN es-abastec-lin-parada.data IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-des-motivo IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-linha IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN es-abastec-lin-parada.nr-linha IN FRAME f-main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME es-abastec-lin-parada.cod-motivo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-parada.cod-motivo V-table-Win
ON F5 OF es-abastec-lin-parada.cod-motivo IN FRAME f-main /* Motivo */
DO:
    {include/zoomvar.i &prog-zoom="esp/escp007-z01.w"
                       &campo=SELF
                       &campozoom=cod-motivo
                       &campo2=fi-des-motivo
                       &campozoom2=des-motivo}  
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-parada.cod-motivo V-table-Win
ON LEAVE OF es-abastec-lin-parada.cod-motivo IN FRAME f-main /* Motivo */
DO:
  FOR FIRST es-motivo-parada WHERE
      es-motivo-parada.cod-motivo = INT(SELF:SCREEN-VALUE) NO-LOCK:
  END.
  fi-des-motivo:SCREEN-VALUE IN FRAME {&FRAME-NAME} = IF AVAIL es-motivo-parada THEN es-motivo-parada.des-motivo
                                                      ELSE "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-parada.cod-motivo V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-abastec-lin-parada.cod-motivo IN FRAME f-main /* Motivo */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-abastec-lin-parada.nr-linha
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-parada.nr-linha V-table-Win
ON LEAVE OF es-abastec-lin-parada.nr-linha IN FRAME f-main /* Linha Produ��o */
DO:
    FOR FIRST lin-prod FIELDS(descricao) WHERE
        lin-prod.nr-linha = INT(SELF:SCREEN-VALUE) NO-LOCK:
    END.

    fi-desc-linha:SCREEN-VALUE IN FRAME {&FRAME-NAME} = IF AVAIL lin-prod THEN lin-prod.descricao
                                                        ELSE "".
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  IF es-abastec-lin-parada.cod-motivo:LOAD-MOUSE-POINTER("image/lupa.cur") IN FRAME {&FRAME-NAME} THEN.
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "es-abastec-lin-parada"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "es-abastec-lin-parada"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

    find es-abastec-lin where rowid (es-abastec-lin) = v-row-parent no-lock no-error.
    if available es-abastec-lin then do:
        assign es-abastec-lin-parada.nr-linha:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(es-abastec-lin.nr-linha)
               es-abastec-lin-parada.data:SCREEN-VALUE IN FRAME {&FRAME-NAME}     = STRING(es-abastec-lin.data)
               es-abastec-lin-parada.hora-ini:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(TIME,"HH:MM")
               es-abastec-lin-parada.hora-fim:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(TIME,"HH:MM").
        APPLY "LEAVE" TO es-abastec-lin-parada.nr-linha IN FRAME {&FRAME-NAME}.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    RUN pi-validate.
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.

    ASSIGN es-abastec-lin-parada.cod-usuario = c-seg-usuario
           es-abastec-lin-parada.data-trans = TODAY
           es-abastec-lin-parada.hora-trans = STRING(TIME,"HH:MM:SS").
    
    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-create-record V-table-Win 
PROCEDURE local-create-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

    RUN dispatch IN THIS-PROCEDURE ( INPUT 'create-record':U ) .

    find es-abastec-lin where rowid (es-abastec-lin) = v-row-parent no-lock no-error.
    if available es-abastec-lin then do:
        assign es-abastec-lin-parada.nr-linha = es-abastec-lin.nr-linha
               es-abastec-lin-parada.data     = es-abastec-lin.data.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

    APPLY "LEAVE" TO es-abastec-lin-parada.nr-linha IN FRAME {&FRAME-NAME}.
    APPLY "LEAVE" TO es-abastec-lin-parada.cod-motivo IN FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
    IF adm-new-record THEN DO:

        DEF BUFFER b-es-abastec-lin-parada FOR es-abastec-lin-parada.

        IF CAN-FIND(FIRST b-es-abastec-lin-parada WHERE
                    b-es-abastec-lin-parada.nr-linha     = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.nr-linha     AND
                    b-es-abastec-lin-parada.data         = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.data         AND
                    b-es-abastec-lin-parada.cod-motivo   = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.cod-motivo   AND
                    b-es-abastec-lin-parada.hora-ini     = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini)    THEN DO:

            RUN utp/ut-msgs.p (INPUT "show",
                               INPUT 17006,
                               INPUT "Registro j� existe!~~J� existe um registro com os dados informados.").

            RETURN "ADM-ERROR".

        END. /* IF CAN-FIND(FIRST b-es-abastec-lin-parada */

    END. /* IF adm-new-record */

    IF NOT CAN-FIND(FIRST es-motivo-parada WHERE
                    es-motivo-parada.cod-motivo = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.cod-motivo) THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "O motivo informado n�o existe!~~Informe um motivo v�lido.").

        RETURN "ADM-ERROR".

    END. /* IF NOT CAN-FIND(FIRST es-motivo-parada */

    IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini = "" THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "A hora in�cio informada inv�lida!~~Informe uma hora in�cio v�lida.").

        RETURN "ADM-ERROR".

    END. /* IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini = "" */

    IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim = "" THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "A hora t�rmino informada inv�lida!~~Informe uma hora t�rmino v�lida.").

        RETURN "ADM-ERROR".

    END. /* IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim = "" */

    IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim < INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "A hora de in�cio/t�rmino informada inv�lida!~~" +
                                 "Hora t�rmino n�o pode ser menor que a hora in�cio.").

        RETURN "ADM-ERROR".

    END. /* IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim < INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini  */

    IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "A hora de in�cio/t�rmino informada inv�lida!~~" +
                                 "Hora in�cio n�o pode ser igual � data t�rmino.").

        RETURN "ADM-ERROR".

    END. /* IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini  */

    FOR EACH b-es-abastec-lin-parada WHERE
        b-es-abastec-lin-parada.nr-linha     = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.nr-linha AND
        b-es-abastec-lin-parada.data         = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.data     NO-LOCK:

        IF ROWID(b-es-abastec-lin-parada) = ROWID(es-abastec-lin-parada) THEN
            NEXT.
            
        IF (INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini >= b-es-abastec-lin-parada.hora-ini  AND
            INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini <= b-es-abastec-lin-parada.hora-fim) OR
           (INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim >= b-es-abastec-lin-parada.hora-ini  AND
            INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim <= b-es-abastec-lin-parada.hora-fim) OR
           (b-es-abastec-lin-parada.hora-ini >= INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini  AND
            b-es-abastec-lin-parada.hora-ini <= INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim) OR
           (b-es-abastec-lin-parada.hora-fim >= INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini  AND
            b-es-abastec-lin-parada.hora-fim <= INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim) THEN DO:

            RUN utp/ut-msgs.p (INPUT "show",
                               INPUT 17006,
                               INPUT "A hora de in�cio/t�rmino informada inv�lida!~~" +
                                     "J� existe outra parada no per�odo informado.").
    
            RETURN "ADM-ERROR".

        END.

    END. /* FOR EACH b-es-abastec-lin-parada */

    FOR EACH es-abastec-lin-item WHERE
        es-abastec-lin-item.nr-linha     = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.nr-linha AND
        es-abastec-lin-item.data         = INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.data     NO-LOCK:

        IF REPLACE(es-abastec-lin-item.hora-ini,":","") >= INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-ini AND 
           REPLACE(es-abastec-lin-item.hora-ini,":","") <= INPUT FRAME {&FRAME-NAME} es-abastec-lin-parada.hora-fim THEN DO:

            RUN utp/ut-msgs.p (INPUT "show",
                               INPUT 17006,
                               INPUT "A hora de in�cio/t�rmino informada inv�lida!~~" +
                                     "J� existe um item apontado no per�odo informado.").
    
            RETURN "ADM-ERROR".

        END.

    END. /* FOR EACH es-abastec-lin-item */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "es-abastec-lin-parada"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

