/******************************************************************************
**  cd9999.i3 - define os campos ct-initial e sc-initial para usar valor
**              inicial da conta contabil independente do tipo (inteiro
**              ou caracter).
**  Versao: G.00 Alcides 07/10/93
*****************************************************************************/

def var ct-initial      like movind.conta.ct-codigo no-undo.
def var sc-initial      like movind.conta.sc-codigo no-undo.

/* fim */
