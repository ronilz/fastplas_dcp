&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/* --------------------------------------------------------------------------------------- */
/*                                                                                         */
/* Sistema................: EMS206                                                         */
/* Modulo.................: ESP - ESPECIFICOS                                              */
/*                                                                                         */
/* Programa...............: ESEN002                                                        */
/* Sub Programa...........: ESEN002RP.P                                                    */
/* Descricao..............: De-para grupo de maquina para opera��o                         */
/* Entidade Desenvolvedora: TOTVS TRIAH                                                */
/*                                                                                         */
/* Hist�rico Programa -------------------------------------------------------------------+ */
/* | Data       | Autor               | Descricao                                        | */
/* +----------- +---------------------+--------------------------------------------------+ */
/* | 02/07/2013 | Lazza - Totvs Triah | Desenvolvimento do Programa                      | */
/* +------------+---------------------+--------------------------------------------------+ */
/* | Observa��es:                                                                        | */
/* +-------------------------------------------------------------------------------------+ */
/*                                                                                         */
/* --------------------------------------------------------------------------------------- */
DEFINE BUFFER prog_dtsul             FOR prog_dtsul.
DEFINE BUFFER modul_dtsul            FOR modul_dtsul.
DEFINE BUFFER procedimento           FOR procedimento.
DEFINE BUFFER empresa                FOR mguni.empresa.
DEFINE BUFFER estabelec              FOR mguni.estabelec.
DEFINE BUFFER cidade                 FOR mguni.cidade.   
DEFINE BUFFER pais                   FOR mguni.pais.  
DEFINE BUFFER usuar_mestre           FOR usuar_mestre.
DEFINE BUFFER configur_layout_impres FOR configur_layout_impres.
DEFINE BUFFER configur_tip_imprsor   FOR configur_tip_imprsor.
DEFINE BUFFER ped_exec               FOR ped_exec.
DEFINE BUFFER servid_exec            FOR servid_exec.
DEFINE BUFFER layout_impres_padr     FOR layout_impres_padr.
DEFINE BUFFER imprsor_usuar          FOR imprsor_usuar.
DEFINE BUFFER layout_impres          FOR layout_impres.
DEFINE BUFFER tip_imprsor            FOR tip_imprsor. 
DEFINE BUFFER impressora             FOR impressora.
DEFINE BUFFER servid_exec_imprsor    FOR servid_exec_imprsor.
DEFINE BUFFER dwb_rpt_select         FOR dwb_rpt_select.
DEFINE BUFFER dwb_rpt_param          FOR dwb_rpt_param.
DEFINE BUFFER log_exec_prog_dtsul    FOR log_exec_prog_dtsul.

{include/i-prgvrs.i ESEN002RP 2.06.00.000}
{utp/ut-glob.i}
{include/tt-edit.i}
{include/pi-edit.i}

/* DEF VAR v_output_file AS CHAR. */
define temp-table tt-param no-undo
    field destino           as integer
    field arquivo           as char format "x(35)"
    field usuario           as char format "x(12)"
    field data-exec         as date
    field hora-exec         as integer
    field classifica        as integer
    field desc-classifica   as char format "x(40)"
    field fm-codigo-ini   as char
    field fm-codigo-fim   as char
    field it-codigo-ini     as char
    field it-codigo-fim     as char
    field fm-cod-com-ini    as char
    field fm-cod-com-fim    as char
    field cod-estabel-ini   as char
    field cod-estabel-fim   as char
    field cd-planejado-ini  as char
    field cd-planejado-fim  as char
    FIELD nr-linha-ini      AS INT
    FIELD nr-linha-fim      AS INT
    field cb-classe-repro   as int
    field cb-contr-plan     as int
    field cb-demanda        as int
    field cb-div-ordem      as int
    field cb-emissao-ord    as int
    field cb-politica       as int
    field cb-reab-estoq     as int
    FIELD cb-ind-lista-mrp  AS INT
    field lote-economi      like item-uni-estab.lote-economi
    field lote-minimo       like item-uni-estab.lote-minimo 
    field lote-multipl      like item-uni-estab.lote-multipl
    field periodo-fixo      like item-uni-estab.periodo-fixo
    field quant-segur       like item-uni-estab.quant-segur 
    field res-cq-comp       like item-uni-estab.res-cq-comp 
    field res-for-comp      like item-uni-estab.res-for-comp
    field res-int-comp      like item-uni-estab.res-int-comp
    field tempo-segur       like item-uni-estab.tempo-segur 
    field tipo-est-seg      like item-uni-estab.tipo-est-seg
    FIELD tb-conv-tempo-seg AS  LOG
    field tg-simulacao      as log
    FIELD rs-importa        AS INT
    FIELD cd-planejado      LIKE lin-prod.cd-planejado 
    FIELD nr-linha          LIKE lin-prod.nr-linha
    FIELD arquivo-importacao AS CHAR
    FIELD arquivo-exportacao AS CHAR.

DEF TEMP-TABLE tt-raw-digita
    FIELD raw-digita       AS RAW.

DEF TEMP-TABLE tt-arquivo-importado NO-UNDO
    FIELD num-linha AS INT
    FIELD linha AS CHAR
    INDEX num-linha AS PRIMARY num-linha.

/* definicao de parametros */
DEF INPUT PARAM raw-param  AS RAW                      NO-UNDO.
DEF INPUT PARAM TABLE           FOR tt-raw-digita.

/* transferencia de parametros */
CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

{include/i_dbvers.i}

/*define var c-titulo-relat  as character format "x(50)"      no-undo.
define var c-sistema       as character format "x(25)"      no-undo.
define var i-numper-x      as integer   format "ZZ"         no-undo.
define var da-iniper-x     as date      format "99/99/9999" no-undo.
define var da-fimper-x     as date      format "99/99/9999" no-undo.
define var c-rodape        as character                     no-undo.
define var v_num_count     as integer                       no-undo.
define var c-arq-control   as character                     no-undo.
define var i-page-size-rel as integer                       no-undo.
define var c-programa      as character format "x(08)"      no-undo.
define var c-versao        as character format "x(04)"      no-undo.
define var c-revisao       as character format "999"        no-undo.
define var c-impressora   as character                      no-undo.
define var c-layout       as character                      no-undo.

/*Defini��es inclu�das para corrigir problema de vari�veis j� definidas pois */
/*as vari�veis e temp-tables eram definidas na include --rpout.i que pode ser*/
/*executada mais de uma vez dentro do mesmo programa (FO 1.120.458) */
/*11/02/2005 - Ed�sio <tech14207>*/
/*-------------------------------------------------------------------------------------------*/
DEF VAR h-procextimpr                               AS HANDLE   NO-UNDO. 
DEF VAR i-num_lin_pag                               AS INT      NO-UNDO.    
DEF VAR c_process-impress                           AS CHAR     NO-UNDO.   
DEF VAR c-cod_pag_carac_conver                      AS CHAR     NO-UNDO.  

/*tech14207
FO 1663218
Inclu�das as defini��es das vari�veis e fun��es
*/

/*tech14207*/
/*tech30713 - fo:1262674 - Defini��o de no-undo na temp-table*/
DEFINE TEMP-TABLE tt-configur_layout_impres_inicio NO-UNDO
    FIELD num_ord_funcao_imprsor    LIKE configur_layout_impres.num_ord_funcao_imprsor
    FIELD cod_funcao_imprsor        LIKE configur_layout_impres.cod_funcao_imprsor
    FIELD cod_opc_funcao_imprsor    LIKE configur_layout_impres.cod_opc_funcao_imprsor
    FIELD num_carac_configur        LIKE configur_tip_imprsor.num_carac_configur
    INDEX ordem num_ord_funcao_imprsor .

/*tech30713 - fo:1262674 - Defini��o de no-undo na temp-table*/
DEFINE TEMP-TABLE tt-configur_layout_impres_fim NO-UNDO
    FIELD num_ord_funcao_imprsor    LIKE configur_layout_impres.num_ord_funcao_imprsor
    FIELD cod_funcao_imprsor        LIKE configur_layout_impres.cod_funcao_imprsor
    FIELD cod_opc_funcao_imprsor    LIKE configur_layout_impres.cod_opc_funcao_imprsor
    FIELD num_carac_configur        LIKE configur_tip_imprsor.num_carac_configur
    INDEX ordem num_ord_funcao_imprsor .
/*-------------------------------------------------------------------------------------------*/

define buffer b_ped_exec_style for ped_exec.
define buffer b_servid_exec_style for servid_exec.
&IF "{&SHARED}" = "YES":U &THEN
    define shared stream str-rp.
&ELSE
    define new shared stream str-rp.
&ENDIF*/
{include/i-lgcode.i}
{include/i-rpvar.i}
/*
define var c-empresa       as character format "x(40)"      no-undo.
define var c-titulo-relat  as character format "x(50)"      no-undo.
define var c-sistema       as character format "x(25)"      no-undo.
define var i-numper-x      as integer   format "ZZ"         no-undo.
define var da-iniper-x     as date      format "99/99/9999" no-undo.
define var da-fimper-x     as date      format "99/99/9999" no-undo.
define var c-rodape        as character                     no-undo.
define var v_num_count     as integer                       no-undo.
define var c-arq-control   as character                     no-undo.
define var i-page-size-rel as integer                       no-undo.
define var c-programa      as character format "x(08)"      no-undo.
define var c-versao        as character format "x(04)"      no-undo.
define var c-revisao       as character format "999"        no-undo.
define var c-impressora   as character                      no-undo.
define var c-layout       as character                      no-undo.

/*Defini��es inclu�das para corrigir problema de vari�veis j� definidas pois */
/*as vari�veis e temp-tables eram definidas na include --rpout.i que pode ser*/
/*executada mais de uma vez dentro do mesmo programa (FO 1.120.458) */
/*11/02/2005 - Ed�sio <tech14207>*/
/*-------------------------------------------------------------------------------------------*/
DEF VAR h-procextimpr                               AS HANDLE   NO-UNDO. 
DEF VAR i-num_lin_pag                               AS INT      NO-UNDO.    
DEF VAR c_process-impress                           AS CHAR     NO-UNDO.   
DEF VAR c-cod_pag_carac_conver                      AS CHAR     NO-UNDO.   

/*tech14207
FO 1663218
Inclu�das as defini��es das vari�veis e fun��es
*/
&IF "{&mguni_version}" >= "2.04" &THEN
    &IF "{&PDF-RP}" <> "YES" &THEN /*tech868*/
    
        /*Alteracao 03/04/2008 - tech40260 - FO 1746516 -  Feito valida��o para verificar se a variavel h_pdf_controller j� foi definida 
                                                       anteriormente, evitando erro de duplicidade*/

        &IF defined(def_pdf_controller) = 0 &THEN         
            DEFINE VARIABLE h_pdf_controller     AS HANDLE NO-UNDO.
    
            &GLOBAL-DEFINE def_pdf_controller YES
    
            DEFINE VARIABLE v_cod_temp_file_pdf  AS CHAR   NO-UNDO.
    
            DEFINE VARIABLE v_cod_relat          AS CHAR   NO-UNDO.
            DEFINE VARIABLE v_cod_file_config    AS CHAR   NO-UNDO.
    
            FUNCTION allowPrint RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION allowSelect RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION useStyle RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION usePDF RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION getPrintFileName RETURNS CHARACTER IN h_pdf_controller.
       
            RUN btb/btb920aa.p PERSISTENT SET h_pdf_controller.
        &ENDIF
        /*Alteracao 03/04/2008 - tech40260 - FO 1746516 -  Feito valida��o para verificar se a variavel h_pdf_controller j� foi definida 
                                                       anteriormente, evitando erro de duplicidade*/
    &ENDIF /*tech868*/
    
&endif
/*tech14207*/
/*tech30713 - fo:1262674 - Defini��o de no-undo na temp-table*/
DEFINE TEMP-TABLE tt-configur_layout_impres_inicio NO-UNDO
    FIELD num_ord_funcao_imprsor    LIKE configur_layout_impres.num_ord_funcao_imprsor
    FIELD cod_funcao_imprsor        LIKE configur_layout_impres.cod_funcao_imprsor
    FIELD cod_opc_funcao_imprsor    LIKE configur_layout_impres.cod_opc_funcao_imprsor
    FIELD num_carac_configur        LIKE configur_tip_imprsor.num_carac_configur
    INDEX ordem num_ord_funcao_imprsor .

/*tech30713 - fo:1262674 - Defini��o de no-undo na temp-table*/
DEFINE TEMP-TABLE tt-configur_layout_impres_fim NO-UNDO
    FIELD num_ord_funcao_imprsor    LIKE configur_layout_impres.num_ord_funcao_imprsor
    FIELD cod_funcao_imprsor        LIKE configur_layout_impres.cod_funcao_imprsor
    FIELD cod_opc_funcao_imprsor    LIKE configur_layout_impres.cod_opc_funcao_imprsor
    FIELD num_carac_configur        LIKE configur_tip_imprsor.num_carac_configur
    INDEX ordem num_ord_funcao_imprsor .
/*-------------------------------------------------------------------------------------------*/

define buffer b_ped_exec_style for ped_exec.
define buffer b_servid_exec_style for servid_exec.
&IF "{&SHARED}" = "YES":U &THEN
    define shared stream str-rp.
&ELSE
    define new shared stream str-rp.
&ENDIF
{include/i-lgcode.i}
/* i-rpvar.i */
*/ 

DEFINE TEMP-TABLE tt-item-uni-estab LIKE item-uni-estab
       field old-classe-repro   LIKE item-uni-estab.classe-repro   
       field old-contr-plan     like item-uni-estab.contr-plan    
       field old-demanda        like item-uni-estab.demanda       
       field old-div-ordem      like item-uni-estab.div-ordem     
       field old-emissao-ord    like item-uni-estab.emissao-ord   
       field old-politica       like item-uni-estab.politica      
       field old-lote-economi   like item-uni-estab.lote-economi  
       field old-lote-minimo    like item-uni-estab.lote-minimo   
       field old-lote-multipl   like item-uni-estab.lote-multipl  
       field old-periodo-fixo   like item-uni-estab.periodo-fixo  
       field old-quant-segur    like item-uni-estab.quant-segur   
       field old-res-cq-comp    like item-uni-estab.res-cq-comp   
       field old-res-for-comp   like item-uni-estab.res-for-comp  
       field old-res-int-comp   like item-uni-estab.res-int-comp  
       field old-tempo-segur    like item-uni-estab.tempo-segur   
       field old-tipo-est-seg   like item-uni-estab.tipo-est-seg  
       field old-conv-tempo-seg like item-uni-estab.conv-tempo-seg
       FIELD OLD-reab-estoq     AS   CHAR 
       FIELD old-processo-mrp   AS INT
       FIELD processo-mrp       AS INT
       field old-cd-planejado   like item-uni-estab.cd-planejado 
       field old-nr-linha       like item-uni-estab.nr-linha
    .
                                     
       
DEFINE TEMP-TABLE tt-item           LIKE ITEM
       field old-classe-repro   like item.classe-repro  
       field old-contr-plan     like item.contr-plan   
       field old-demanda        like item.demanda       
       field old-div-ordem      like item.div-ordem     
       field old-emissao-ord    like item.emissao-ord   
       field old-politica       like item.politica      
       field old-lote-economi   like item.lote-economi  
       field old-lote-minimo    like item.lote-minimo              
       field old-lote-multipl   like item.lote-multipl    
       field old-periodo-fixo   like item.periodo-fixo    
       field old-quant-segur    like item.quant-segur     
       field old-res-cq-comp    like item.res-cq-comp     
       field old-res-for-comp   like item.res-for-comp    
       field old-res-int-comp   like item.res-int-comp    
       field old-tempo-segur    like item.tempo-segur     
       field old-tipo-est-seg   like item.tipo-est-seg    
       field old-conv-tempo-seg like item.conv-tempo-seg
       FIELD OLD-reab-estoq     AS   CHAR 
       FIELD old-processo-mrp   AS INT
       FIELD processo-mrp       AS INT
       field old-cd-planejado   like item-uni-estab.cd-planejado 
       field old-nr-linha       like item.nr-linha.

DEFINE TEMP-TABLE tt-item-uni-estab-import LIKE item-uni-estab
       field old-classe-repro   LIKE item-uni-estab.classe-repro   
       field old-contr-plan     like item-uni-estab.contr-plan    
       field old-demanda        like item-uni-estab.demanda       
       field old-div-ordem      like item-uni-estab.div-ordem     
       field old-emissao-ord    like item-uni-estab.emissao-ord   
       field old-politica       like item-uni-estab.politica      
       field old-lote-economi   like item-uni-estab.lote-economi  
       field old-lote-minimo    like item-uni-estab.lote-minimo   
       field old-lote-multipl   like item-uni-estab.lote-multipl  
       field old-periodo-fixo   like item-uni-estab.periodo-fixo  
       field old-quant-segur    like item-uni-estab.quant-segur   
       field old-res-cq-comp    like item-uni-estab.res-cq-comp   
       field old-res-for-comp   like item-uni-estab.res-for-comp  
       field old-res-int-comp   like item-uni-estab.res-int-comp  
       field old-tempo-segur    like item-uni-estab.tempo-segur   
       field old-tipo-est-seg   like item-uni-estab.tipo-est-seg  
       field old-conv-tempo-seg like item-uni-estab.conv-tempo-seg
       FIELD OLD-reab-estoq     AS   CHAR 
       FIELD old-processo-mrp   AS INT
       FIELD processo-mrp       AS INT
       field old-cd-planejado   like item-uni-estab.cd-planejado 
       field old-nr-linha       like item-uni-estab.nr-linha.
                                     
       
DEFINE TEMP-TABLE tt-item-import           LIKE ITEM
       field old-classe-repro   like item.classe-repro  
       field old-contr-plan     like item.contr-plan   
       field old-demanda        like item.demanda       
       field old-div-ordem      like item.div-ordem     
       field old-emissao-ord    like item.emissao-ord   
       field old-politica       like item.politica      
       field old-lote-economi   like item.lote-economi  
       field old-lote-minimo    like item.lote-minimo              
       field old-lote-multipl   like item.lote-multipl    
       field old-periodo-fixo   like item.periodo-fixo    
       field old-quant-segur    like item.quant-segur     
       field old-res-cq-comp    like item.res-cq-comp     
       field old-res-for-comp   like item.res-for-comp    
       field old-res-int-comp   like item.res-int-comp    
       field old-tempo-segur    like item.tempo-segur     
       field old-tipo-est-seg   like item.tipo-est-seg    
       field old-conv-tempo-seg like item.conv-tempo-seg
       FIELD OLD-reab-estoq     AS   CHAR 
       FIELD old-processo-mrp   AS INT
       FIELD processo-mrp       AS INT
       field old-cd-planejado   like item-uni-estab.cd-planejado 
       field old-nr-linha       like item-uni-estab.nr-linha.

DEF TEMP-TABLE tt-erros 
    field seq-erro  AS INT
    field cod-erro  AS INT
    field desc-erro AS CHAR
    FIELD rw-gm     AS ROWID.
DEFINE BUFFER bf-tt-erros FOR tt-erros.


define variable chExcelApplication as com-handle.
define variable chWorkBook         as com-handle.
define variable chWorkSheet        as com-handle.

DEFINE VARIABLE h_acomp                      AS HANDLE      NO-UNDO.
DEF BUFFER b-grup-maquina  FOR grup-maquina. 
DEFINE VARIABLE l-recur-sec AS LOGICAL     NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 7.04
         WIDTH              = 43.72.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


FIND FIRST tt-param NO-LOCK NO-ERROR.
IF NOT AVAIL tt-param THEN NEXT.

FIND FIRST param-global NO-LOCK NO-ERROR.
assign c-empresa  = param-global.grupo.
assign  c-programa      = "ESEN001"
        c-versao        = "2.06"
        c-revisao       = ".00.000"
        c-sistema       = "Espec�ficos"
        c-titulo-relat  = "De-para Grupo de m�quina para Opera��es ".


/* def new global shared var c-dir-spool-servid-exec as char no-undo.  */
/*  def new global shared var i-num-ped-exec-rpw as int no-undo.       */
/* ASSIGN v_output_file = tt-param.arquivo.                            */
/* if  i-num-ped-exec-rpw <> 0 then do:                                */
/*     output to value(c-dir-spool-servid-exec + "~/" + v_output_file) */
/*            /*paged page-size value(i-page-size-rel)*/               */
/*            convert target "iso8859-1" .                             */
/* end.                                                                */
/* else do:                                                            */
/*     output to value(v_output_file)                                  */
/*         /*paged page-size value(i-page-size-rel)*/                  */
/*         convert target "iso8859-1" .                                */
/* end.                                                                */

RUN utp/ut-acomp.p PERSISTENT SET h_acomp.
RUN pi-inicializar IN h_acomp (INPUT "Aguarde, Processando...").
IF tt-param.rs-importa = 1  THEN
do:
    RUN pi-carrega-tt.
    IF tt-param.tg-simulacao = NO THEN
       RUN pi-processa-item.
    RUN pi-imprime.
END.
ELSE DO:
    RUN pi-importa-excel.
    RUN pi-processa-item.
    RUN pi-imprime-2.
END.


PAGE.

RUN pi-finalizar IN h_acomp.

/* {include/i-rpout.i} */
/* {include\i-rpclo.i} */

RETURN "OK".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&IF DEFINED(EXCLUDE-pi-carrega-tt) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-carrega-tt Procedure 
PROCEDURE pi-carrega-tt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
RUN pi-inicializar IN h_acomp (INPUT "Carregando Registros").



FOR EACH ITEM NO-LOCK
    WHERE ITEM.it-codigo  >= TT-PARAM.it-codigo-ini   
      AND ITEM.it-codigo  <= TT-PARAM.it-codigo-fim  
      and ITEM.fm-codigo  >= TT-PARAM.fm-codigo-ini   
      and ITEM.fm-codigo  <= TT-PARAM.fm-codigo-fim   
      and ITEM.fm-cod-com >= TT-PARAM.fm-cod-com-ini  
      and ITEM.fm-cod-com <= TT-PARAM.fm-cod-com-fim.
    
    RUN pi-acompanhar IN h_acomp (INPUT "Item " + ITEM.it-codigo ).
    CREATE tt-item.
    BUFFER-COPY ITEM TO tt-item.
    ASSIGN tt-item.old-classe-repro             = item.classe-repro   
           tt-item.old-contr-plan               = item.contr-plan     
           tt-item.old-demanda                  = item.demanda        
           tt-item.old-div-ordem                = item.div-ordem      
           tt-item.old-emissao-ord              = item.emissao-ord    
           tt-item.old-politica                 = item.politica       
           tt-item.old-lote-economi             = item.lote-economi   
           tt-item.old-lote-minimo              = item.lote-minimo               
           tt-item.old-lote-multipl             = item.lote-multipl     
           tt-item.old-periodo-fixo             = item.periodo-fixo     
           tt-item.old-quant-segur              = item.quant-segur
           tt-item.old-tempo-segur              = item.tempo-segur      
           tt-item.old-tipo-est-seg             = item.tipo-est-seg     
           tt-item.old-conv-tempo-seg           = item.conv-tempo-seg 
           tt-item.old-cd-planejado             = item.cd-planejado   
           tt-item.old-nr-linha                 = item.nr-linha
           TT-ITEM.OLD-REAB-ESTOQ               = IF substr(item.char-1,10,1) <> "" THEN substr(item.char-1,10,1) ELSE '1'
           tt-item.classe-repro                 = tt-param.cb-classe-repro  
           tt-item.contr-plan                   = tt-param.cb-contr-plan    
           tt-item.demanda                      = tt-param.cb-demanda       
           tt-item.div-ordem                    = tt-param.cb-div-ordem     
           tt-item.emissao-ord                  = tt-param.cb-emissao-ord   
           tt-item.politica                     = tt-param.cb-politica      
           tt-item.lote-economi                 = tt-param.lote-economi
           tt-item.lote-minimo                  = tt-param.lote-minimo                 
           tt-item.lote-multipl                 = tt-param.lote-multipl       
           tt-item.periodo-fixo                 = tt-param.periodo-fixo       
           tt-item.quant-segur                  = tt-param.quant-segur        
           tt-item.tempo-segur                  = tt-param.tempo-segur        
           tt-item.tipo-est-seg                 = tt-param.tipo-est-seg       
           tt-item.conv-tempo-seg               = tt-param.tb-conv-tempo-seg 
           OVERLAY(tt-item.char-1,10,1)         = STRING(TT-PARAM.cb-reab-estoq)
           tt-item.cd-planejado                 = tt-item.cd-planejado    
           tt-item.nr-linha                     = tt-item.nr-linha        
           .       
               .
        IF item.compr-fabric = 1 THEN
           ASSIGN tt-item.old-res-cq-comp           = item.res-cq-comp      
                  tt-item.old-res-for-comp          = item.res-for-comp     
                  tt-item.old-res-int-comp          = item.res-int-comp 
                  tt-item.res-cq-comp               = tt-param.res-cq-comp        
                  tt-item.res-for-comp              = tt-param.res-for-comp       
                  tt-item.res-int-comp              = tt-param.res-int-comp       .  
    find first item-man
         where item-man.it-codigo = item.it-codigo no-lock no-error.
    if avail item-man then
    DO: 
        ASSIGN tt-item.old-processo-mrp = item-man.ind-lista-mrp.
    END.
    ASSIGN tt-item.processo-mrp = tt-param.cb-ind-lista-mrp.

    FOR EACH  item-uni-estab NO-LOCK
        WHERE item-uni-estab.it-codigo     = ITEM.it-codigo
        AND   item-uni-estab.cod-estabel  >= tt-param.cod-estabel-ini 
        AND   item-uni-estab.cod-estabel  <= tt-param.cod-estabel-fim 
        AND   item-uni-estab.nr-linha     >= tt-param.nr-linha-ini
        AND   item-uni-estab.nr-linha     <= tt-param.nr-linha-fim
        AND   item-uni-estab.cd-planejado >= tt-param.cd-planejado-ini 
        AND   item-uni-estab.cd-planejado <= tt-param.cd-planejado-fim.
        RUN pi-acompanhar IN h_acomp (INPUT "Item / Estab" + ITEM.it-codigo + " / " + item-uni-estab.cod-estabel).
        CREATE tt-item-uni-estab.
        BUFFER-COPY item-uni-estab TO tt-item-uni-estab.
        ASSIGN tt-item-uni-estab.old-classe-repro   = item-uni-estab.classe-repro  
               tt-item-uni-estab.old-contr-plan     = item-uni-estab.contr-plan    
               tt-item-uni-estab.old-demanda        = item-uni-estab.demanda       
               tt-item-uni-estab.old-div-ordem      = item-uni-estab.div-ordem     
               tt-item-uni-estab.old-emissao-ord    = item-uni-estab.emissao-ord   
               tt-item-uni-estab.old-politica       = item-uni-estab.politica      
               tt-item-uni-estab.old-lote-economi   = item-uni-estab.lote-economi      
               tt-item-uni-estab.old-lote-minimo    = item-uni-estab.lote-minimo   
               tt-item-uni-estab.old-lote-multipl   = item-uni-estab.lote-multipl  
               tt-item-uni-estab.old-periodo-fixo   = item-uni-estab.periodo-fixo  
               tt-item-uni-estab.old-quant-segur    = item-uni-estab.quant-segur   
               tt-item-uni-estab.old-tempo-segur    = item-uni-estab.tempo-segur   
               tt-item-uni-estab.old-tipo-est-seg   = item-uni-estab.tipo-est-seg  
               tt-item-uni-estab.old-conv-tempo-seg = item-uni-estab.conv-tempo-seg
               tt-item-uni-estab.old-cd-planejado   = item-uni-estab.cd-planejado 
               tt-item-uni-estab.old-nr-linha       = item-uni-estab.nr-linha    
               tt-item-uni-estab.old-REAB-ESTOQ     = IF substr(item-uni-estab.char-1,10,1) <> "" THEN substr(item-uni-estab.char-1,10,1) ELSE '1' 
               tt-item-uni-estab.classe-repro       = tt-param.cb-classe-repro   
               tt-item-uni-estab.contr-plan         = tt-param.cb-contr-plan     
               tt-item-uni-estab.demanda            = tt-param.cb-demanda        
               tt-item-uni-estab.div-ordem          = tt-param.cb-div-ordem      
               tt-item-uni-estab.emissao-ord        = tt-param.cb-emissao-ord    
               tt-item-uni-estab.politica           = tt-param.cb-politica       
               tt-item-uni-estab.lote-economi       = tt-param.lote-economi          
               tt-item-uni-estab.lote-minimo        = tt-param.lote-minimo       
               tt-item-uni-estab.lote-multipl       = tt-param.lote-multipl      
               tt-item-uni-estab.periodo-fixo       = tt-param.periodo-fixo      
               tt-item-uni-estab.quant-segur        = tt-param.quant-segur       
               tt-item-uni-estab.tempo-segur        = tt-param.tempo-segur       
               tt-item-uni-estab.tipo-est-seg       = tt-param.tipo-est-seg      
               tt-item-uni-estab.conv-tempo-seg     = tt-param.tb-conv-tempo-seg 
               OVERLAY(tt-item-uni-estab.char-1,10,1) = STRING(TT-PARAM.cb-reab-estoq) 
               tt-item-uni-estab.cd-planejado       = tt-param.cd-planejado 
               tt-item-uni-estab.nr-linha           = tt-param.nr-linha    
               .
        IF item.compr-fabric = 1 THEN       
           ASSIGN tt-item-uni-estab.old-res-cq-comp    = item-uni-estab.res-cq-comp   
                  tt-item-uni-estab.old-res-for-comp   = item-uni-estab.res-for-comp  
                  tt-item-uni-estab.old-res-int-comp   = item-uni-estab.res-int-comp  
                  tt-item-uni-estab.res-cq-comp        = tt-param.res-cq-comp       
                  tt-item-uni-estab.res-for-comp       = tt-param.res-for-comp      
                  tt-item-uni-estab.res-int-comp       = tt-param.res-int-comp.
    END.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pi-imprime) = 0 &THEN
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-imprime Procedure 
PROCEDURE pi-imprime :

    OUTPUT TO VALUE(tt-param.arquivo-exportacao) CONVERT TARGET 'iso8859-1'.

    IF CAN-FIND(FIRST tt-item NO-LOCK) THEN do:

        PUT UNFORMATTED 
          /*1*/  "Item; " +
          /*2*/  "Descri��o; " + 
          /*3*/  "Estab;" +
          /*4*/  "Politica Ant; " +    
          /*5*/  "Politica Novo; " + 
          /*6*/  "Demanda Ant; " +    
          /*7*/  "Demanda Novo; " +       
          /*8*/  "Lote Multi Ant; " +
          /*9*/  "Lote Multi Novo; " +  
          /*10*/  "Lote Minimo Ant; " + 
          /*11*/  "Lote Minimo Novo; " +   
          /*12*/  "Lote Econom Ant; " +    
          /*13*/  "Lote Econom Novo; " + 
          /*14*/  "Periodo Fixo Ant; " +
          /*15*/  "Periodo Fixo Novo; " +  
          /*16*/  "Processo MRP Ant;"   +
          /*17*/  "Processo MRP Novo;"   +
          /*18*/  "Ressupr CQ Ant; " +
          /*19*/  "Ressupr CQ Novo; " +   
          /*20*/  "Ressupr Fornec Ant; " +
          /*21*/  "Ressupr Fornec Novo ; " +  
          /*22*/  "Ressupr Compras Ant; " +
          /*23*/  "Ressupr Compras Novo; " +
          /*24*/  "Tipo Estoq Seg Ant ; " +
          /*25*/  "Tipo Estoq Seg Novo; " +
          /*26*/  "Quant Segur Ant; " +
          /*27*/  "Quant Segur Novo; " +
          /*28*/  "Tempo Estoq Seg Ant ; " +
          /*29*/  "Tempo Estoq Seg Novo; " +   
          /*30*/  "Conv Tempo Seg Ant; " + 
          /*31*/  "Conv Tempo Seg Novo; " +
          /*32*/  "Reabastec Ant; " +
          /*33*/  "Reabastec Novo; " +
          /*34*/  "Emiss�o Ordens Ant; " +     
          /*35*/  "Emiss�o Ordens Novo; " +  
          /*36*/  "Classe Reprog Ant; " +
          /*37*/  "Classe Reprog Novo; " +
          /*38*/  "Control.Planej Item; " +    
          /*39*/  "Control.Planej Est; " +   
          /*40*/  "Divis�o Ordem Ant; " +       
          /*41*/  "Divis�o Ordem Novo;"  +
          /*42*/  "Cod Planejador Ant;" +
          /*43*/  "Cod Planejador Novo;" +
          /*44*/  "Nr. Linha Ant;" +
          /*45*/  "Nr. Linha Novo"
             skip.   
        FOR EACH tt-item-uni-estab NO-LOCK,
            FIRST tt-ITEM NO-LOCK
            WHERE tt-ITEM.it-codigo = tt-item-uni-estab.it-codigo:
            
            PUT  UNFORMATTED
                 tt-ITEM.it-codigo                                                                   ";"
                 tt-item.desc-item                                                                   ";"
                 tt-item-uni-estab.cod-estabel                                                       ";"
                {ininc/i04in122.i 04 tt-item.old-politica}                                           ";"  
                {ininc/i04in122.i 04 tt-item.politica}                                               ";"  
                {ininc/i02in122.i 04 tt-item.old-demanda}                                            ";" 
                {ininc/i02in122.i 04 tt-item.demanda}                                                ";" 
                tt-item-uni-estab.old-lote-multipl                                                   ";" 
                tt-item-uni-estab.lote-multipl                                                       ";" 
                tt-item-uni-estab.old-lote-minimo                                                    ";" 
                tt-item-uni-estab.lote-minimo                                                        ";" 
                tt-item-uni-estab.old-lote-economi                                                   ";" 
                tt-item-uni-estab.lote-economi                                                       ";" 
                tt-item-uni-estab.old-periodo-fixo                                                   ";" 
                tt-item-uni-estab.periodo-fixo                                                       ";" 
                {ininc/i03in180.i 04 int(tt-item.old-processo-mrp)}                                  ";"  
                {ininc/i03in180.i 04 int(tt-item.processo-mrp)}                                      ";" 
                tt-item-uni-estab.old-res-cq-comp                                                    ";" 
                tt-item-uni-estab.res-cq-comp                                                        ";" 
                tt-item-uni-estab.old-res-for-comp                                                   ";" 
                tt-item-uni-estab.res-for-comp                                                       ";" 
                tt-item-uni-estab.old-res-int-comp                                                   ";" 
                tt-item-uni-estab.res-int-comp                                                       ";" 
                IF tt-item-uni-estab.old-tipo-est-seg  = 1 THEN "Quantidade" ELSE "Tempo"            ";"
                IF tt-item-uni-estab.tipo-est-seg = 1 THEN "Quantidade" ELSE "Tempo"                 ";" 
                tt-item-uni-estab.old-quant-segur                                                    ";" 
                tt-item-uni-estab.quant-segur                                                        ";" 
                tt-item-uni-estab.old-tempo-segur                                                    ";" 
                tt-item-uni-estab.tempo-segur                                                        ";" 
                IF tt-item-uni-estab.old-conv-tempo-seg = YES THEN "Sim" ELSE "N�o"                  ";" 
                IF tt-item-uni-estab.conv-tempo-seg = YES THEN "Sim" ELSE "N�o"                      ";" 
                {ininc/i26in172.i 04 int(tt-item-uni-estab.OLD-REAB-ESTOQ)}                          ";"
                {ininc/i26in172.i 04 int(SUBSTRING(tt-item-uni-estab.char-1,10,1))}                  ";"
                IF tt-item-uni-estab.old-classe-repro = 1 THEN "Antecipa/Prorroga" ELSE              
                    IF tt-item-uni-estab.old-classe-repro = 2 THEN "Prorroga" ELSE                   
                        IF tt-item-uni-estab.old-classe-repro = 3 THEN "Antecipa" ELSE               
                             "N�o Reprograma"                                                        ";"
                IF tt-item-uni-estab.classe-repro = 1 THEN "Antecipa/Prorroga" ELSE                  
                    IF tt-item-uni-estab.classe-repro = 2 THEN  "Prorroga" ELSE                      
                        IF tt-item-uni-estab.classe-repro = 3 THEN "Antecipa" ELSE                   
                            "N�o Reprograma"                                                         ";"
                IF tt-item-uni-estab.old-emissao-ord = 1 THEN "Autom�tico" ELSE "Manual"             ";"
                IF tt-item-uni-estab.emissao-ord = 1 THEN "Autom�tico" ELSE "Manual"                 ";"
                IF tt-item-uni-estab.old-contr-plan =1 THEN "Produ��o" ELSE "Manuten��o Industrial"  ";"
                IF tt-item-uni-estab.contr-plan  =1 THEN "Produ��o" ELSE "Manuten��o Industrial"     ";"
                IF tt-item-uni-estab.old-div-ordem  = 1 THEN "N�o Divide"     ELSE
                    IF tt-item-uni-estab.old-div-ordem  = 2 THEN "Lote M�ltiplo"  ELSE 
                        "Lote Econ�mico"                                                             ";"
                IF tt-item-uni-estab.div-ordem  = 1 THEN "N�o Divide"     ELSE 
                    IF tt-item-uni-estab.div-ordem  = 2 THEN "Lote M�ltiplo"  ELSE 
                        "Lote Econ�mico"                                                             ";"
                tt-item-uni-estab.old-cd-planejado                                                   ";"
                tt-item-uni-estab.cd-planejado                                                       ";"
                tt-item-uni-estab.old-nr-linha                                                       ";"
                tt-item-uni-estab.nr-linha          SKIP.
        END.
        FOR EACH tt-item NO-LOCK.
            FIND FIRST tt-item-uni-estab NO-LOCK
                 WHERE tt-item-uni-estab.it-codigo = tt-item.it-codigo NO-ERROR.
            IF AVAIL tt-item-uni-estab THEN NEXT.
            
            PUT  UNFORMATTED
                 tt-ITEM.it-codigo                                          ";"
                 tt-item.desc-item                                          ";"
                 tt-item-uni-estab.cod-estabel                              ";"
                {ininc/i04in122.i 04 tt-item.old-politica}                  ";"  
                {ininc/i04in122.i 04 tt-item.politica}                      ";"  
                {ininc/i02in122.i 04 tt-item.old-demanda}                   ";" 
                {ininc/i02in122.i 04 tt-item.demanda}                       ";" 
                tt-item.old-lote-multipl                                    ";" 
                tt-item.lote-multipl                                        ";" 
                tt-item.old-lote-minimo                                     ";" 
                tt-item.lote-minimo                                         ";" 
                tt-item.old-lote-economi                                    ";" 
                tt-item.lote-economi                                        ";" 
                tt-item.old-periodo-fixo                                    ";" 
                tt-item.periodo-fixo                                        ";" 
                ";"  
                ";" 
                tt-item.old-res-cq-comp                                     ";" 
                tt-item.res-cq-comp                                         ";" 
                tt-item.old-res-for-comp                                    ";" 
                tt-item.res-for-comp                                        ";" 
                tt-item.old-res-int-comp                                    ";" 
                tt-item.res-int-comp                                        ";" 
                IF tt-item.old-tipo-est-seg  = 1 THEN "Quantidade" ELSE "Tempo"  ";"
                IF tt-item.tipo-est-seg = 1 THEN "Quantidade" ELSE "Tempo"  ";" 
                tt-item.old-quant-segur                                     ";" 
                tt-item.quant-segur                                         ";" 
                tt-item.old-tempo-segur                                     ";" 
                tt-item.tempo-segur                                         ";" 
                IF tt-item.old-conv-tempo-seg = YES THEN "Sim" ELSE "N�o"   ";" 
                IF tt-item.conv-tempo-seg = YES THEN "Sim" ELSE "N�o"       ";" 
                {ininc/i26in172.i 04 int(TT-ITEM.OLD-REAB-ESTOQ)}           ";"
                {ininc/i26in172.i 04 int(SUBSTRING(tt-item.char-1,10,1))}   ";"
                IF tt-item.old-classe-repro = 1 THEN "Antecipa/Prorroga" ELSE 
                    IF tt-item.old-classe-repro = 2 THEN "Prorroga" ELSE 
                        IF tt-item.old-classe-repro = 3 THEN "Antecipa" ELSE 
                             "N�o Reprograma" ";"
                IF tt-item.classe-repro = 1 THEN "Antecipa/Prorroga" ELSE 
                    IF tt-item.classe-repro = 2 THEN  "Prorroga" ELSE 
                        IF tt-item.classe-repro = 3 THEN "Antecipa" ELSE 
                            "N�o Reprograma"                                       ";"
                IF tt-item.old-emissao-ord = 1 THEN "Autom�tico" 
                ELSE "Manual"                              ";"
                IF tt-item.emissao-ord = 1 THEN "Autom�tico" 
                ELSE "Manual"                              ";"
                IF tt-item.old-contr-plan =1 THEN "Produ��o" 
                ELSE "Manuten��o Industrial"                 ";"
                IF tt-item.contr-plan  =1 THEN "Produ��o" 
                ELSE "Manuten��o Industrial"                 ";"
                IF tt-item.old-div-ordem  = 1 THEN "N�o Divide"     ELSE
                    IF tt-item.old-div-ordem  = 2 THEN "Lote M�ltiplo"  ELSE 
                        "Lote Econ�mico"                                            ";"
                IF tt-item.div-ordem  = 1 THEN "N�o Divide"     ELSE 
                    IF tt-item.div-ordem  = 2 THEN "Lote M�ltiplo"  ELSE 
                        "Lote Econ�mico"            ';'                                
                tt-item.old-cd-planejado  ';'
                tt-item.cd-planejado      ';'
                tt-item.old-nr-linha      ';'
                tt-item.nr-linha          SKIP.
        END.
    
    END.

    OUTPUT CLOSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pi-importa-excel) = 0 &THEN
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-importa-excel Procedure 
PROCEDURE pi-importa-excel :
    DEFINE VARIABLE i-cont AS INTEGER     NO-UNDO.
    DEFINE VARIABLE c-linha AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE c-item AS CHARACTER   NO-UNDO.
    DEFINE VARIABLE c-sep AS CHAR NO-UNDO.
    DEF VAR i-num-linha AS INT NO-UNDO.

    &SCOPED-DEFINE NUM-COLUNAS 45

    INPUT FROM VALUE(tt-param.arquivo-importacao) CONVERT SOURCE 'iso8859-1'.
    REPEAT:
        IMPORT UNFORMATTED c-linha.

        IF NUM-ENTRIES(c-linha, ';') = {&NUM-COLUNAS} THEN c-sep = ';'.
        ELSE IF NUM-ENTRIES(c-linha, '~t') = {&NUM-COLUNAS} THEN c-sep = '~t'. 
        ELSE LEAVE.

        CREATE tt-arquivo-importado.

        ASSIGN
            i-num-linha = i-num-linha + 1
            tt-arquivo-importado.num-linha = i-num-linha
            tt-arquivo-importado.linha     = REPLACE(c-linha, '~t', ';').


        ASSIGN c-item = ENTRY(1,c-linha,c-sep)
               c-item = REPLACE(c-item,'.','')
               c-item = string(c-item,'9999.9999.999').
        IF ENTRY(1,c-linha,c-sep)  = "item" THEN
            NEXT.

        c-item = ENTRY(1,c-linha,c-sep).

        FIND FIRST lin-prod NO-LOCK
                 WHERE lin-prod.nr-linha = int(ENTRY(45,c-linha,c-sep)) NO-ERROR.

        IF NOT AVAIL lin-prod THEN
        DO:

            FIND LAST tt-erros NO-ERROR.
            CREATE bf-tt-erros.
            ASSIGN bf-tt-erros.seq-erro  = IF AVAIL tt-erros THEN tt-erros.seq-erro + 1 ELSE 1
                   bf-tt-erros.cod-erro  = 99999
                   bf-tt-erros.desc-erro = "Linha, " + ENTRY(45,c-linha,c-sep) + ', n�o encontrada! Favor averiguar!'.

            tt-arquivo-importado.linha = tt-arquivo-importado.linha + ';' +
                                         bf-tt-erros.desc-erro.

            NEXT.
        END.

        FIND FIRST planejad NO-LOCK
             WHERE planejad.cd-planejado = ENTRY(43,c-linha,c-sep) NO-ERROR.
        IF NOT AVAIL planejad THEN
        DO:
            FIND LAST tt-erros NO-ERROR.
            CREATE bf-tt-erros.
            ASSIGN bf-tt-erros.seq-erro  = IF AVAIL tt-erros THEN tt-erros.seq-erro + 1 ELSE 1
                   bf-tt-erros.cod-erro  = 99999
                   bf-tt-erros.desc-erro = "Planejador, " + ENTRY(43,c-linha,c-sep) + ', n�o encontrado! Favor averiguar! '.

            tt-arquivo-importado.linha = tt-arquivo-importado.linha + ';' +
                                         bf-tt-erros.desc-erro.

            NEXT.
        END.
        FOR EACH ITEM NO-LOCK
            WHERE ITEM.it-codigo   = c-item.
            FIND FIRST lin-prod NO-LOCK
                 WHERE lin-prod.nr-linha = int(ENTRY(45,c-linha,c-sep)) NO-ERROR.
            
            IF NOT CAN-FIND(FIRST tt-item
                            WHERE tt-ITEM.it-codigo   = c-item) THEN
            do:
                CREATE tt-item.
                BUFFER-COPY ITEM TO tt-item.
                ASSIGN tt-item.old-classe-repro             = item.classe-repro   
                       tt-item.old-contr-plan               = item.contr-plan     
                       tt-item.old-demanda                  = item.demanda        
                       tt-item.old-div-ordem                = item.div-ordem      
                       tt-item.old-emissao-ord              = item.emissao-ord    
                       tt-item.old-politica                 = item.politica       
                       tt-item.old-lote-economi             = item.lote-economi   
                       tt-item.old-lote-minimo              = item.lote-minimo               
                       tt-item.old-lote-multipl             = item.lote-multipl     
                       tt-item.old-periodo-fixo             = item.periodo-fixo     
                       tt-item.old-quant-segur              = item.quant-segur
                       tt-item.old-tempo-segur              = item.tempo-segur      
                       tt-item.old-tipo-est-seg             = item.tipo-est-seg     
                       tt-item.old-conv-tempo-seg           = item.conv-tempo-seg 
                       TT-ITEM.OLD-REAB-ESTOQ               = IF substr(item.char-1,10,1) <> "" THEN substr(item.char-1,10,1) ELSE '1'
                       tt-item.old-cd-planejado             = item.cd-planejado    
                       tt-item.old-nr-linha                 = ITEM.nr-linha    .
                ASSIGN tt-item.classe-repro                 = IF ENTRY(35,c-linha,c-sep) = "Antecipa/Prorroga"             THEN 1 
                                                              ELSE IF ENTRY(35,c-linha,c-sep) = "Prorroga"                 THEN 2 
                                                                   ELSE IF ENTRY(35,c-linha,c-sep) = "Antecipa"            THEN 3 
                                                                        ELSE IF ENTRY(35,c-linha,c-sep) = "N�o Reprograma" THEN 4
                                                                             ELSE ?  
                       tt-item.contr-plan                   = IF ENTRY(39,c-linha,c-sep) = "Produ��o" THEN 1 ELSE 2
                       tt-item.demanda                      = IF ENTRY(7,c-linha,c-sep) = "Dependente" THEN 1 ELSE 2       
                       tt-item.div-ordem                    = IF ENTRY(41,c-linha,c-sep) = "N�o Divide" THEN 1 ELSE IF ENTRY(39,c-linha,c-sep) = "Lote M�ltiplo" THEN 2 ELSE 3    
                       tt-item.emissao-ord                  = IF ENTRY(37,c-linha,c-sep) = "Autom�tico" THEN 1 ELSE 2 
                       tt-item.politica                     = IF ENTRY(5,c-linha,c-sep) = "Per�odo Fixo" THEN 1 else
                                                              IF ENTRY(5,c-linha,c-sep) = "Lote Econ�mico" THEN 2 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "Ordem" THEN 3 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "N�vel Superior" THEN 4 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "Configurado" THEN 5 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "Composto" THEN 6 ELSE 
                                                              IF ENTRY(5,c-linha,c-sep) = "Ponto de Reposi��o" THEN 7 ELSE ?
                       tt-item.lote-economi                 = dec(ENTRY(13,c-linha,c-sep)) 
                       tt-item.lote-minimo                  = dec(ENTRY(11,c-linha,c-sep))
                       tt-item.lote-multipl                 = dec(ENTRY(9,c-linha,c-sep)) 
                       tt-item.periodo-fixo                 = int(ENTRY(15,c-linha,c-sep)) 
                       tt-item.quant-segur                  = dec(ENTRY(27,c-linha,c-sep)) 
                       tt-item.tempo-segur                  = int(ENTRY(29,c-linha,c-sep)) 
                       tt-item.tipo-est-seg                 = IF ENTRY(25,c-linha,c-sep) = "Quantidade" THEN 1 ELSE IF ENTRY(24,c-linha,c-sep) = "Tempo" THEN 2 ELSE ?
                       tt-item.conv-tempo-seg               = IF ENTRY(31,c-linha,c-sep) = "Sim" THEN YES ELSE NO       
                       OVERLAY(tt-item.char-1,10,1)         = IF ENTRY(33,c-linha,c-sep) = "Demanda" THEN "1" ELSE IF ENTRY(33,c-linha,c-sep) = "Quantidade" THEN "2" ELSE ?
                       tt-item.cd-planejado                 = ENTRY(43,c-linha,c-sep)
                       tt-item.nr-linha                     = int(ENTRY(45,c-linha,c-sep))    .
                IF item.compr-fabric = 1 THEN
                   ASSIGN tt-item.old-res-cq-comp           = item.res-cq-comp      
                          tt-item.old-res-for-comp          = item.res-for-comp     
                          tt-item.old-res-int-comp          = item.res-int-comp 
                          tt-item.res-cq-comp               = int(ENTRY(19,c-linha,c-sep))        
                          tt-item.res-for-comp              = int(ENTRY(21,c-linha,c-sep))         
                          tt-item.res-int-comp              = int(ENTRY(23,c-linha,c-sep)).        
                   
                find first item-man
                     where item-man.it-codigo = item.it-codigo no-lock no-error.
                if avail item-man then
                DO: 
                    ASSIGN tt-item.old-processo-mrp = item-man.ind-lista-mrp.
                END.
                ASSIGN tt-item.processo-mrp = IF ENTRY(17,c-linha,c-sep) = "Processo Principal" THEN 1 ELSE IF ENTRY(17,c-linha,c-sep) = "Todos os Processos" THEN 2 ELSE ?      .
                
            END.

            FOR EACH  item-uni-estab NO-LOCK
                WHERE item-uni-estab.it-codigo     = ITEM.it-codigo
                AND   item-uni-estab.cod-estabel   = ENTRY(3,c-linha,c-sep).
                RUN pi-acompanhar IN h_acomp (INPUT "Item / Estab" + ITEM.it-codigo + " / " + item-uni-estab.cod-estabel).
                CREATE tt-item-uni-estab.
                BUFFER-COPY item-uni-estab TO tt-item-uni-estab.
                ASSIGN tt-item-uni-estab.old-classe-repro   = item-uni-estab.classe-repro  
                       tt-item-uni-estab.old-contr-plan     = item-uni-estab.contr-plan    
                       tt-item-uni-estab.old-demanda        = item-uni-estab.demanda       
                       tt-item-uni-estab.old-div-ordem      = item-uni-estab.div-ordem     
                       tt-item-uni-estab.old-emissao-ord    = item-uni-estab.emissao-ord   
                       tt-item-uni-estab.old-politica       = item-uni-estab.politica      
                       tt-item-uni-estab.old-lote-economi   = item-uni-estab.lote-economi      
                       tt-item-uni-estab.old-lote-minimo    = item-uni-estab.lote-minimo   
                       tt-item-uni-estab.old-lote-multipl   = item-uni-estab.lote-multipl  
                       tt-item-uni-estab.old-periodo-fixo   = item-uni-estab.periodo-fixo  
                       tt-item-uni-estab.old-quant-segur    = item-uni-estab.quant-segur   
                       tt-item-uni-estab.old-tempo-segur    = item-uni-estab.tempo-segur   
                       tt-item-uni-estab.old-tipo-est-seg   = item-uni-estab.tipo-est-seg  
                       tt-item-uni-estab.old-conv-tempo-seg = item-uni-estab.conv-tempo-seg
                       tt-item-uni-estab.old-REAB-ESTOQ     = IF substr(item-uni-estab.char-1,10,1) <> "" THEN substr(item-uni-estab.char-1,10,1) ELSE '1' 
                       tt-item-uni-estab.old-cd-planejado   = item-uni-estab.cd-planejado
                       tt-item-uni-estab.old-nr-linha       = item-uni-estab.nr-linha
                           .
                ASSIGN tt-item-uni-estab.classe-repro       = IF ENTRY(35,c-linha,c-sep) = "Antecipa/Prorroga"             THEN 1 
                                                              ELSE IF ENTRY(35,c-linha,c-sep) = "Prorroga"                 THEN 2 
                                                                   ELSE IF ENTRY(35,c-linha,c-sep) = "Antecipa"            THEN 3 
                                                                        ELSE IF ENTRY(35,c-linha,c-sep) = "N�o Reprograma" THEN 4
                                                                             ELSE ?  
                       tt-item-uni-estab.contr-plan         = IF ENTRY(39,c-linha,c-sep) = "Produ��o" THEN 1 ELSE 2
                       tt-item-uni-estab.demanda            = IF ENTRY(7,c-linha,c-sep) = "Dependente" THEN 1 ELSE 2        
                       tt-item-uni-estab.div-ordem          = IF ENTRY(41,c-linha,c-sep) = "N�o Divide" THEN 1 ELSE IF ENTRY(39,c-linha,c-sep) = "Lote M�ltiplo" THEN 2 ELSE 3          
                       tt-item-uni-estab.emissao-ord        = IF ENTRY(37,c-linha,c-sep) = "Autom�tico" THEN 1 ELSE 2     
                       tt-item-uni-estab.politica           = IF ENTRY(5,c-linha,c-sep) = "Per�odo Fixo" THEN 1 else
                                                              IF ENTRY(5,c-linha,c-sep) = "Lote Econ�mico" THEN 2 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "Ordem" THEN 3 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "N�vel Superior" THEN 4 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "Configurado" THEN 5 ELSE
                                                              IF ENTRY(5,c-linha,c-sep) = "Composto" THEN 6 ELSE 
                                                              IF ENTRY(5,c-linha,c-sep) = "Ponto de Reposi��o" THEN 7 ELSE ?       
                       tt-item-uni-estab.lote-economi       = dec(ENTRY(13,c-linha,c-sep))           
                       tt-item-uni-estab.lote-minimo        = dec(ENTRY(11,c-linha,c-sep))       
                       tt-item-uni-estab.lote-multipl       = dec(ENTRY(9,c-linha,c-sep))       
                       tt-item-uni-estab.periodo-fixo       = int(ENTRY(15,c-linha,c-sep))       
                       tt-item-uni-estab.quant-segur        = dec(ENTRY(27,c-linha,c-sep))        
                       tt-item-uni-estab.tempo-segur        = int(ENTRY(29,c-linha,c-sep))        
                       tt-item-uni-estab.tipo-est-seg       = IF ENTRY(25,c-linha,c-sep) = "Quantidade" THEN 1 ELSE IF ENTRY(24,c-linha,c-sep) = "Tempo" THEN 2 ELSE ?      
                       tt-item-uni-estab.conv-tempo-seg       = IF ENTRY(31,c-linha,c-sep) = "Sim" THEN YES ELSE NO        
                       OVERLAY(tt-item-uni-estab.char-1,10,1) = IF ENTRY(33,c-linha,c-sep) = "Demanda" THEN "1" ELSE IF ENTRY(33,c-linha,c-sep) = "Quantidade" THEN "2" ELSE ?
                       tt-item-uni-estab.cd-planejado         = ENTRY(43,c-linha,c-sep)
                       tt-item-uni-estab.nr-linha             = int(ENTRY(45,c-linha,c-sep))        .
                IF item.compr-fabric = 1 THEN       
                   ASSIGN tt-item-uni-estab.old-res-cq-comp    = item-uni-estab.res-cq-comp   
                          tt-item-uni-estab.old-res-for-comp   = item-uni-estab.res-for-comp  
                          tt-item-uni-estab.old-res-int-comp   = item-uni-estab.res-int-comp  
                          tt-item-uni-estab.res-cq-comp        = int(ENTRY(19,c-linha,c-sep))       
                          tt-item-uni-estab.res-for-comp       = int(ENTRY(21,c-linha,c-sep))      
                          tt-item-uni-estab.res-int-comp       = int(ENTRY(23,c-linha,c-sep)).
                       
            END. 
        END.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pi-processa-item) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-processa-item Procedure 
PROCEDURE pi-processa-item :
RUN pi-inicializar IN h_acomp (INPUT "Atualizando Registros ").
FOR EACH tt-item NO-LOCK,
    FIRST ITEM OF tt-item EXCLUSIVE-LOCK:
    RUN pi-acompanhar IN h_acomp (INPUT "Item / Estab" + ITEM.it-codigo ).
    ASSIGN item.classe-repro             = tt-item.classe-repro   
           item.contr-plan               = tt-item.contr-plan     
           item.demanda                  = tt-item.demanda        
           item.div-ordem                = tt-item.div-ordem      
           item.emissao-ord              = tt-item.emissao-ord    
           item.politica                 = tt-item.politica       
           item.lote-economi             = tt-item.lote-economi   
           item.lote-minimo              = tt-item.lote-minimo               
           item.lote-multipl             = tt-item.lote-multipl     
           item.periodo-fixo             = tt-item.periodo-fixo     
           item.tipo-est-seg             = tt-item.tipo-est-seg     
           item.conv-tempo-seg           = tt-item.conv-tempo-seg 
           item.cd-planejado             = tt-item.cd-planejado
           item.nr-linha                 = tt-item.nr-linha   
   OVERLAY(ITEM.char-1,10,1)             = Substring(tt-item.char-1,10,1). 
    IF item.compr-fabric = 1 THEN       
       ASSIGN item.res-cq-comp              = tt-item.res-cq-comp      
              item.res-for-comp             = tt-item.res-for-comp     
              item.res-int-comp             = tt-item.res-int-comp.
    IF tt-param.tipo-est-seg = 1 THEN
        item.quant-segur              = tt-item.quant-segur.
    ELSE 
        item.tempo-segur              = tt-item.tempo-segur .
     

    find first item-man
         where item-man.it-codigo = item.it-codigo EXCLUSIVE-LOCK no-error.
    if avail item-man then
    DO: 
        ASSIGN item-man.ind-lista-mrp = tt-item.processo-mrp.
    END.
END.

FOR EACH tt-item-uni-estab NO-LOCK,
    FIRST item-uni-estab OF tt-item-uni-estab EXCLUSIVE-LOCK,
    FIRST ITEM NO-LOCK
    WHERE item.it-codigo = item-uni-estab.it-codigo.
    RUN pi-acompanhar IN h_acomp (INPUT "Item / Estab" + item-uni-estab.it-codigo + " / " + item-uni-estab.cod-estabel). 
	
	
	
    ASSIGN item-uni-estab.classe-repro   = tt-item-uni-estab.classe-repro  
           item-uni-estab.contr-plan     = tt-item-uni-estab.contr-plan    
           item-uni-estab.demanda        = tt-item-uni-estab.demanda       
           item-uni-estab.div-ordem      = tt-item-uni-estab.div-ordem     
           item-uni-estab.emissao-ord    = tt-item-uni-estab.emissao-ord   
           item-uni-estab.politica       = tt-item-uni-estab.politica      
           item-uni-estab.lote-economi   = tt-item-uni-estab.lote-economi      
           item-uni-estab.lote-minimo    = tt-item-uni-estab.lote-minimo   
           item-uni-estab.lote-multipl   = tt-item-uni-estab.lote-multipl  
           item-uni-estab.periodo-fixo   = tt-item-uni-estab.periodo-fixo  
           item-uni-estab.tipo-est-seg   = tt-item-uni-estab.tipo-est-seg  
           item-uni-estab.conv-tempo-seg = tt-item-uni-estab.conv-tempo-seg
           item-uni-estab.cd-planejado   = tt-item-uni-estab.cd-planejado
           item-uni-estab.nr-linha       = tt-item-uni-estab.nr-linha  
   OVERLAY(item-uni-estab.char-1,10,1)   = Substring(tt-item-uni-estab.char-1,10,1).
    IF tt-param.tipo-est-seg = 1 THEN
        item-uni-estab.quant-segur       = tt-item-uni-estab.quant-segur.
    ELSE 
        item-uni-estab.tempo-segur    = tt-item-uni-estab.tempo-segur  .
    IF item.compr-fabric = 1 THEN       
       ASSIGN item-uni-estab.res-cq-comp    = tt-item-uni-estab.res-cq-comp   
              item-uni-estab.res-for-comp   = tt-item-uni-estab.res-for-comp  
              item-uni-estab.res-int-comp   = tt-item-uni-estab.res-int-comp.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF

&IF DEFINED(EXCLUDE-pi-imprime-2) = 0 &THEN
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-imprime-2 Procedure 
PROCEDURE pi-imprime-2 :

    DEF VAR c-format AS CHAR NO-UNDO.

    OUTPUT TO VALUE(tt-param.arquivo-importacao) CONVERT TARGET 'iso8859-1'.

    IF CAN-FIND(FIRST tt-erros) THEN DO:

        PUT 'Aten��o foram encontrados erros. Verifique a coluna AT para detalhes' SKIP(1).

    END.

    FOR EACH tt-arquivo-importado :

        c-format = "X(" + STRING(MAX(LENGTH(tt-arquivo-importado.linha), 1)) + ")".

        PUT tt-arquivo-importado.linha FORMAT c-format SKIP.

    END.

    OUTPUT CLOSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF
