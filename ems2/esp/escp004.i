def temp-table tt-selecao no-undo
    field it-codigo-ini as char
    field it-codigo-fim as char
    field fm-codigo-ini as char
    field fm-codigo-fim as char
    field periodo-ini   as char format "9999/99"
    field periodo-fim   as char format "9999/99"
    field i-possui-op   as int.
