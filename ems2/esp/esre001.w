&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          mgadm            PROGRESS
*/
&Scoped-define WINDOW-NAME wReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wReport 
/********************************************************************************
** Copyright DATASUL S.A. (1999)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
/*
**
** emiyahira - 13/08/2010 - revisao para 2.06
**
*/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESRE001 2.06.00.000}

CREATE WIDGET-POOL.

/* Preprocessors Definitions ---                                      */
&GLOBAL-DEFINE Program        ESRE001
&GLOBAL-DEFINE Version        2.06.00.000
&GLOBAL-DEFINE VersionLayout  

&GLOBAL-DEFINE Folder         YES
&GLOBAL-DEFINE InitialPage    1
&GLOBAL-DEFINE FolderLabels   Par�metro,Digita��o,Impress�o

&GLOBAL-DEFINE PGLAY          NO
&GLOBAL-DEFINE PGSEL          NO
&GLOBAL-DEFINE PGCLA          NO
&GLOBAL-DEFINE PGPAR          YES
&GLOBAL-DEFINE PGDIG          YES
&GLOBAL-DEFINE PGIMP          NO
&GLOBAL-DEFINE PGLOG          YES

&GLOBAL-DEFINE page0Widgets   btOk ~
                              btCancel ~
                              btHelp2
&GLOBAL-DEFINE page1Widgets   
&GLOBAL-DEFINE page2Widgets   
&GLOBAL-DEFINE page3Widgets   
&GLOBAL-DEFINE page4Widgets   btInputFile
&GLOBAL-DEFINE page5Widgets   brDigita ~
                              btAtualizar ~
                              btSelecionar /*btOpen*/ bt-eliminar
&GLOBAL-DEFINE page6Widgets   
&GLOBAL-DEFINE page7Widgets   rsDestiny ~
                              btConfigImprDest ~
                              btDestinyFile ~
                              rsExecution
&GLOBAL-DEFINE page8Widgets   

&GLOBAL-DEFINE page0Text      
&GLOBAL-DEFINE page1Text      
&GLOBAL-DEFINE page2Text      
&GLOBAL-DEFINE page3Text      
&GLOBAL-DEFINE page4Text      text-entrada
&GLOBAL-DEFINE page5Text      
&GLOBAL-DEFINE page6Text      text-destino text-modo
&GLOBAL-DEFINE page7Text      text-destino text-modo
&GLOBAL-DEFINE page8Text   

&GLOBAL-DEFINE page1Fields    
&GLOBAL-DEFINE page2Fields    
&GLOBAL-DEFINE page3Fields    
&GLOBAL-DEFINE page4Fields    i-cod-emitente c-nome-abrev c-cod-estab c-nome-estab i-tp-codigo c-desc-despesa cInputFile 
&GLOBAL-DEFINE page5Fields    c-nat-entrada
&GLOBAL-DEFINE page6Fields    
&GLOBAL-DEFINE page7Fields    cDestinyFile
&GLOBAL-DEFINE page8Fields    

/* Parameters Definitions ---                                           */

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field arq-destino      as char format "x(35)"
    FIELD arq-entrada      AS CHAR FORMAT "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD todos            AS INT
    FIELD cod-emitente   LIKE emitente.cod-emitente
    FIELD cod-estabel    LIKE estabelec.cod-estabel
    FIELD tp-codigo      LIKE tipo-rec-desp.tp-codigo.


DEFINE TEMP-TABLE tt-digita NO-UNDO LIKE nota-fiscal
    FIELD nome-abrev LIKE emitente.nome-abrev.

DEF NEW GLOBAL SHARED TEMP-TABLE tt-nota-fiscal  LIKE nota-fiscal
    FIELD nat-entrada LIKE natur-oper.nat-operacao.
DEF NEW GLOBAL SHARED TEMP-TABLE tt-it-nota-fisc LIKE it-nota-fisc
    FIELD seq-comp LIKE it-nota-fisc.nr-seq-fat
    FIELD est-comp LIKE nota-fiscal.cod-estabel.
DEF NEW GLOBAL SHARED TEMP-TABLE tt-fat-ser-lote LIKE fat-ser-lote.
DEF NEW GLOBAL SHARED TEMP-TABLE tt-fat-duplic   LIKE fat-duplic.

/** ronil **/
DEF BUFFER b-tt-it-nota-fisc FOR tt-it-nota-fisc.
DEFINE TEMP-TABLE tt-agregado  NO-UNDO
    FIELD cod-estabel LIKE nota-fiscal.cod-estabel
    FIELD serie LIKE  nota-fiscal.serie
    FIELD nr-nota-fis LIKE nota-fiscal.nr-nota-fis
    FIELD estab-comp LIKE nota-fiscal.cod-estabel
    FIELD serie-comp LIKE nota-fiscal.serie
    FIELD nr-nota-comp LIKE nota-fiscal.nr-nota-fis
    FIELD nr-ord-produ LIKE ord-prod.nr-ord-produ.

/* {cdp/cd0666.i} */

DEF NEW GLOBAL SHARED VAR i-seq-tt-docum-est as int  format "999999".

{rep/reapi190.i}

DEFINE BUFFER b-tt-digita FOR tt-digita.

/* Transfer Definitions */

def var raw-param        as raw no-undo.

def var l-ok               as logical no-undo.
def var c-arq-digita       as char    no-undo.
def var c-terminal         as char    no-undo.
def var c-arq-layout       as char    no-undo.      
def var c-arq-temp         as char    no-undo.
DEF VAR i-cont             AS INT     NO-UNDO.

def stream s-imp.

def new global shared var l-implanta as logical init no. 
def new global shared var wh-fill    as widget-handle no-undo. 
def new global shared var wh-window  as handle no-undo. 
def new global shared var adm-broker-hdl as handle no-undo.

DEF TEMP-TABLE tt-item-doc-est-aux LIKE item-doc-est
    FIELD cod-estabel LIKE it-nota-fisc.cod-estabel
    FIELD serie       LIKE it-nota-fisc.serie
    FIELD nr-nota-fis LIKE it-nota-fisc.nr-nota-fis
    FIELD nr-seq-fat  LIKE it-nota-fisc.nr-seq-fat
    FIELD nat-oper-nf LIKE it-nota-fisc.nat-operacao
    FIELD nr-ord-ext  LIKE item-doc-est.nr-ord-produ
    FIELD qt-ord-ext  LIKE es-item-doc-est.qt-ord-ext.


DEF TEMP-TABLE tt-rat-lote      NO-UNDO LIKE rat-lote.      

DEF VAR l-confirma AS LOG INIT NO NO-UNDO.
DEF VAR l-erro     AS LOG INIT NO NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fpage0
&Scoped-define BROWSE-NAME brDigita

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-digita tt-nota-fiscal

/* Definitions for BROWSE brDigita                                      */
&Scoped-define FIELDS-IN-QUERY-brDigita tt-digita.cod-estabel tt-digita.serie tt-digita.nr-nota-fis tt-digita.cod-emitente tt-digita.nome-abrev tt-digita.nat-operacao tt-digita.dt-emis-nota tt-nota-fiscal.nat-entrada tt-digita.vl-tot-nota   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brDigita   
&Scoped-define SELF-NAME brDigita
&Scoped-define QUERY-STRING-brDigita FOR EACH tt-digita, ~
                               FIRST tt-nota-fiscal OF tt-digita
&Scoped-define OPEN-QUERY-brDigita OPEN QUERY brDigita FOR EACH tt-digita, ~
                               FIRST tt-nota-fiscal OF tt-digita.
&Scoped-define TABLES-IN-QUERY-brDigita tt-digita tt-nota-fiscal
&Scoped-define FIRST-TABLE-IN-QUERY-brDigita tt-digita
&Scoped-define SECOND-TABLE-IN-QUERY-brDigita tt-nota-fiscal


/* Definitions for FRAME fPage5                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-fPage5 ~
    ~{&OPEN-QUERY-brDigita}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS btOK btCancel btHelp2 rtToolBar 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fnTextoCSNFS wReport 
FUNCTION fnTextoCSNFS RETURNS CHARACTER
  ( INPUT pEstab AS CHAR,
    INPUT pSerie AS CHAR,
    INPUT pNota  AS CHAR)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wReport AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btCancel 
     LABEL "Fechar" 
     SIZE 10 BY 1.

DEFINE BUTTON btHelp2 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON btOK 
     LABEL "Importar" 
     SIZE 10 BY 1.

DEFINE RECTANGLE rtToolBar
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 90 BY 1.42
     BGCOLOR 7 .

DEFINE BUTTON btInputFile 
     IMAGE-UP FILE "image\im-sea":U
     IMAGE-INSENSITIVE FILE "image\ii-sea":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE VARIABLE cInputFile AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 256
     SIZE 40 BY .88
     BGCOLOR 15 FONT 1 NO-UNDO.

DEFINE VARIABLE c-cod-estab AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .88 TOOLTIP "Estabelecimento Destino" NO-UNDO.

DEFINE VARIABLE c-desc-despesa LIKE tipo-rec-desp.descricao
     VIEW-AS FILL-IN 
     SIZE 31.14 BY .88 NO-UNDO.

DEFINE VARIABLE c-nome-abrev LIKE emitente.nome-abrev
     VIEW-AS FILL-IN 
     SIZE 16.14 BY .88 NO-UNDO.

DEFINE VARIABLE c-nome-estab LIKE estabelec.nome
     VIEW-AS FILL-IN 
     SIZE 42 BY .88 NO-UNDO.

DEFINE VARIABLE i-cod-emitente AS INTEGER FORMAT ">>>>>>>>9" INITIAL 0 
     LABEL "Emitente":R8 
     VIEW-AS FILL-IN 
     SIZE 11.57 BY .88 NO-UNDO.

DEFINE VARIABLE i-tp-codigo AS INTEGER FORMAT ">>9" INITIAL 0 
     LABEL "Tipo Despesa" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .88 TOOLTIP "Tipo Despesa" NO-UNDO.

DEFINE VARIABLE text-entrada AS CHARACTER FORMAT "X(256)":U INITIAL "Arquivo de Entrada" 
      VIEW-AS TEXT 
     SIZE 13.86 BY .63
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 46.29 BY 2.

DEFINE RECTANGLE RECT-16
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 63.14 BY 4.

DEFINE BUTTON bt-eliminar 
     IMAGE-UP FILE "image\im-era":U
     IMAGE-INSENSITIVE FILE "image\ii-era":U
     LABEL "Eliminar" 
     SIZE 4 BY 1.25
     FONT 4.

DEFINE BUTTON btAtualizar 
     IMAGE-UP FILE "image\im-sav":U
     IMAGE-INSENSITIVE FILE "image\ii-sav":U
     LABEL "Atualizar Recebimento" 
     SIZE 4 BY 1.25 TOOLTIP "Atualizar Recebimento"
     FONT 4.

DEFINE BUTTON btNatOper 
     IMAGE-UP FILE "image/im-enter.bmp":U
     LABEL "Confirma Natureza de Operacao de Entrada" 
     SIZE 4 BY 1 TOOLTIP "Confirma Natureza de Operacao de Entrada"
     FONT 4.

DEFINE BUTTON btOpen 
     IMAGE-UP FILE "image/im-autom.bmp":U
     LABEL "Atualizar" 
     SIZE 4 BY 1.25 TOOLTIP "Atualizar".

DEFINE BUTTON btSelecionar 
     IMAGE-UP FILE "image/im-ran_a.bmp":U
     LABEL "Selecionar Todos" 
     SIZE 4 BY 1.25
     FONT 4.

DEFINE VARIABLE c-desc-nat AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 35 BY .88 NO-UNDO.

DEFINE VARIABLE c-nat-entrada AS CHARACTER FORMAT "X(256)":U 
     LABEL "Natureza Operacao Entrada" 
     VIEW-AS FILL-IN 
     SIZE 8 BY .88 NO-UNDO.

DEFINE BUTTON btConfigImprDest 
     IMAGE-UP FILE "image\im-cfprt":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE BUTTON btDestinyFile 
     IMAGE-UP FILE "image\im-sea":U
     IMAGE-INSENSITIVE FILE "image\ii-sea":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE VARIABLE cDestinyFile AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 256
     SIZE 40 BY .88
     BGCOLOR 15 FONT 1 NO-UNDO.

DEFINE VARIABLE text-destino AS CHARACTER FORMAT "X(256)":U INITIAL " Destino" 
      VIEW-AS TEXT 
     SIZE 8.57 BY .63
     FONT 1 NO-UNDO.

DEFINE VARIABLE text-modo AS CHARACTER FORMAT "X(256)":U INITIAL "Execu��o" 
      VIEW-AS TEXT 
     SIZE 10.86 BY .63
     FONT 1 NO-UNDO.

DEFINE VARIABLE rsDestiny AS INTEGER INITIAL 3 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Impressora", 1,
"Arquivo", 2,
"Terminal", 3
     SIZE 44 BY 1.08
     FONT 1 NO-UNDO.

DEFINE VARIABLE rsExecution AS INTEGER INITIAL 1 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "On-Line", 1,
"Batch", 2
     SIZE 27.72 BY .92
     FONT 1 NO-UNDO.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 47 BY 1.71.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 47 BY 2.92.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brDigita FOR 
      tt-digita, 
      tt-nota-fiscal SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brDigita
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brDigita wReport _FREEFORM
  QUERY brDigita DISPLAY
      tt-digita.cod-estabel 
tt-digita.serie
tt-digita.nr-nota-fis    COLUMN-LABEL "Nota Fiscal"
tt-digita.cod-emitente   COLUMN-LABEL "Fornecedor"
tt-digita.nome-abrev
tt-digita.nat-operacao
tt-digita.dt-emis-nota
tt-nota-fiscal.nat-entrada    COLUMN-LABEL "Nat.Entr"
tt-digita.vl-tot-nota    COLUMN-LABEL "Total Nota" FORMAT ">>>>,>>9.99"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE SIZE 76.57 BY 9.75
         BGCOLOR 15 FONT 1 ROW-HEIGHT-CHARS .46.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fpage0
     btOK AT ROW 16.75 COL 2
     btCancel AT ROW 16.75 COL 13
     btHelp2 AT ROW 16.75 COL 80
     rtToolBar AT ROW 16.54 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 90.14 BY 17
         FONT 1.

DEFINE FRAME fPage5
     brDigita AT ROW 1.5 COL 1
     btOpen AT ROW 1.67 COL 78 HELP
          "Atualizar"
     bt-eliminar AT ROW 2.88 COL 78 HELP
          "Elimina ocorr�ncia corrente"
     btSelecionar AT ROW 4.08 COL 78 HELP
          "Selecionar Todos"
     btAtualizar AT ROW 9.67 COL 78 HELP
          "Atualizar Recebimento"
     btNatOper AT ROW 11.17 COL 65 HELP
          "Confirma Natureza de Operacao de Entrada"
     c-nat-entrada AT ROW 11.25 COL 18.86 COLON-ALIGNED
     c-desc-nat AT ROW 11.25 COL 27.14 COLON-ALIGNED NO-LABEL
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 2.79
         SIZE 82 BY 11.46
         FONT 1.

DEFINE FRAME fPage7
     rsDestiny AT ROW 2 COL 3.29 HELP
          "Destino de Impress�o do Relat�rio" NO-LABEL
     btDestinyFile AT ROW 3.21 COL 43.29 HELP
          "Escolha do nome do arquivo"
     cDestinyFile AT ROW 3.25 COL 3.29 HELP
          "Nome do arquivo de destino do relat�rio" NO-LABEL
     btConfigImprDest AT ROW 3.25 COL 43 HELP
          "Configura��o da impressora"
     rsExecution AT ROW 5.38 COL 3.14 HELP
          "Modo de Execu��o" NO-LABEL
     text-destino AT ROW 1.25 COL 1.86 COLON-ALIGNED NO-LABEL
     text-modo AT ROW 4.63 COL 1.14 COLON-ALIGNED NO-LABEL
     RECT-10 AT ROW 4.92 COL 2
     RECT-8 AT ROW 1.54 COL 2
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 2.79
         SIZE 82 BY 11.46
         FONT 1.

DEFINE FRAME fPage4
     i-cod-emitente AT ROW 1.75 COL 17.14 COLON-ALIGNED
     c-nome-abrev AT ROW 1.75 COL 29.29 COLON-ALIGNED NO-LABEL
     c-cod-estab AT ROW 2.75 COL 17.14 COLON-ALIGNED HELP
          "Estabelecimento Destino"
     c-nome-estab AT ROW 2.75 COL 22.86 COLON-ALIGNED NO-LABEL
     i-tp-codigo AT ROW 3.75 COL 17.14 COLON-ALIGNED HELP
          "Tipo Despesa"
     c-desc-despesa AT ROW 3.75 COL 22.86 COLON-ALIGNED NO-LABEL
     cInputFile AT ROW 9.79 COL 3.29 HELP
          "Nome do arquivo de destino do relat�rio" NO-LABEL
     btInputFile AT ROW 9.79 COL 43.14 HELP
          "Escolha do nome do arquivo"
     text-entrada AT ROW 8.75 COL 4.14 NO-LABEL
     RECT-12 AT ROW 9 COL 2
     RECT-16 AT ROW 1.38 COL 4.86
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 7 ROW 2.79
         SIZE 82 BY 11.46
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wReport ASSIGN
         HIDDEN             = YES
         TITLE              = ""
         HEIGHT             = 17
         WIDTH              = 90.14
         MAX-HEIGHT         = 22
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wReport 
/* ************************* Included-Libraries *********************** */

{Report\Report.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wReport
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* REPARENT FRAME */
ASSIGN FRAME fPage4:FRAME = FRAME fpage0:HANDLE
       FRAME fPage5:FRAME = FRAME fpage0:HANDLE
       FRAME fPage7:FRAME = FRAME fpage0:HANDLE.

/* SETTINGS FOR FRAME fpage0
   NOT-VISIBLE FRAME-NAME                                               */
/* SETTINGS FOR FRAME fPage4
                                                                        */
/* SETTINGS FOR FILL-IN c-desc-despesa IN FRAME fPage4
   NO-ENABLE LIKE = mgadm.tipo-rec-desp.descricao EXP-SIZE              */
/* SETTINGS FOR FILL-IN c-nome-abrev IN FRAME fPage4
   NO-ENABLE LIKE = mgadm.emitente.nome-abrev EXP-SIZE                  */
/* SETTINGS FOR FILL-IN c-nome-estab IN FRAME fPage4
   NO-ENABLE LIKE = mgadm.estabelec.nome EXP-SIZE                       */
/* SETTINGS FOR FILL-IN text-entrada IN FRAME fPage4
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
ASSIGN 
       text-entrada:PRIVATE-DATA IN FRAME fPage4     = 
                "Arquivo de Entrada".

/* SETTINGS FOR FRAME fPage5
                                                                        */
/* BROWSE-TAB brDigita 1 fPage5 */
/* SETTINGS FOR BUTTON btOpen IN FRAME fPage5
   NO-ENABLE                                                            */
ASSIGN 
       btOpen:HIDDEN IN FRAME fPage5           = TRUE.

/* SETTINGS FOR FILL-IN c-desc-nat IN FRAME fPage5
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME fPage7
                                                                        */
ASSIGN 
       text-destino:PRIVATE-DATA IN FRAME fPage7     = 
                "Destino".

ASSIGN 
       text-modo:PRIVATE-DATA IN FRAME fPage7     = 
                "Execu��o".

IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wReport)
THEN wReport:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brDigita
/* Query rebuild information for BROWSE brDigita
     _START_FREEFORM
OPEN QUERY brDigita FOR EACH tt-digita,
                        FIRST tt-nota-fiscal OF tt-digita.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brDigita */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fpage0
/* Query rebuild information for FRAME fpage0
     _Options          = "SHARE-LOCK KEEP-EMPTY"
     _Query            is NOT OPENED
*/  /* FRAME fpage0 */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fPage7
/* Query rebuild information for FRAME fPage7
     _Query            is NOT OPENED
*/  /* FRAME fPage7 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wReport
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wReport wReport
ON END-ERROR OF wReport
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wReport wReport
ON WINDOW-CLOSE OF wReport
DO:
  /* This event will close the window and terminate the procedure.  */
  {report/logfin.i}  
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brDigita
&Scoped-define FRAME-NAME fPage5
&Scoped-define SELF-NAME brDigita
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brDigita wReport
ON ENTER OF brDigita IN FRAME fPage5
ANYWHERE
DO:
  apply 'tab' to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brDigita wReport
ON MOUSE-SELECT-CLICK OF brDigita IN FRAME fPage5
DO:
    FIND tt-nota-fiscal
        WHERE tt-nota-fiscal.cod-estabel = INPUT BROWSE brDigita tt-digita.cod-estabel
          AND tt-nota-fiscal.serie       = INPUT BROWSE brDigita tt-digita.serie
          AND tt-nota-fiscal.nr-nota-fis = INPUT BROWSE brDigita tt-digita.nr-nota-fis
        NO-LOCK NO-ERROR.
    IF  AVAIL tt-nota-fiscal THEN
        ASSIGN c-nat-entrada:SCREEN-VALUE IN FRAME fpage5 = tt-nota-fiscal.nat-entrada.
    ELSE
        ASSIGN c-nat-entrada:SCREEN-VALUE IN FRAME fpage5 = "".

    APPLY 'leave' TO c-nat-entrada IN FRAME fpage5.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-eliminar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-eliminar wReport
ON CHOOSE OF bt-eliminar IN FRAME fPage5 /* Eliminar */
DO:
    IF  brDigita:NUM-SELECTED-ROWS > 0 THEN DO ON ERROR UNDO, RETURN NO-APPLY:
        MESSAGE "Confirma exclus�o das Notas Fiscais selecionadas?"
                VIEW-AS ALERT-BOX QUESTION BUTTON YES-NO UPDATE l-confirma.
        IF  l-confirma THEN DO i-cont = 1 TO brDigita:NUM-SELECTED-ROWS IN FRAME fPage5:
            brDigita:FETCH-SELECTED-ROW(i-cont) IN FRAME fPage5.
            FIND tt-nota-fiscal
                WHERE tt-nota-fiscal.cod-estabel = tt-digita.cod-estabel
                  AND tt-nota-fiscal.serie       = tt-digita.serie
                  AND tt-nota-fiscal.nr-nota-fis = tt-digita.nr-nota-fis
                NO-LOCK NO-ERROR.
            IF  AVAIL tt-nota-fiscal THEN DO:
                FOR EACH tt-it-nota-fisc OF tt-nota-fiscal:
                    FOR EACH tt-fat-ser-lote OF tt-it-nota-fisc:
                        DELETE tt-fat-ser-lote.
                    END.
                    DELETE tt-it-nota-fisc.
                END.
                FOR EACH tt-fat-duplic 
                    WHERE tt-fat-duplic.cod-estabel = tt-nota-fiscal.cod-estabel
                      AND tt-fat-duplic.serie       = tt-nota-fiscal.serie
                      AND tt-fat-duplic.nr-fatura   = tt-nota-fiscal.nr-nota-fis:
                    DELETE tt-fat-duplic.
                END.
                DELETE tt-nota-fiscal.
            END.
            DELETE tt-digita.
            IF  brDigita:DELETE-SELECTED-ROWS() IN FRAME {&FRAME-NAME} THEN.
        END.
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btAtualizar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btAtualizar wReport
ON CHOOSE OF btAtualizar IN FRAME fPage5 /* Atualizar Recebimento */
DO:
    /* vars */
    DEFINE VARIABLE l-encontrou AS LOGICAL    NO-UNDO.
    DEFINE VARIABLE c-lista-sel AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-lista-sel-ag AS CHARACTER  NO-UNDO.
    DEFINE VARIABLE c-lista-nfs AS CHARACTER  NO-UNDO.
    DEFINE VARIABLE i-seq AS INTEGER    NO-UNDO.
    
    /* main */
    ASSIGN l-erro             = NO
           i-seq-tt-docum-est = 0.

    FOR EACH tt-erro:
        DELETE tt-erro.
    END.
    FOR EACH tt-docum-est:
        DELETE tt-docum-est.
    END.
    FOR EACH tt-item-doc-est:
        DELETE tt-item-doc-est.
    END.
    FOR EACH tt-dupli-apagar:
        DELETE tt-dupli-apagar.
    END.
    FOR EACH tt-dupli-imp:
        DELETE tt-dupli-imp.
    END.
    FOR EACH tt-unid-neg-nota:
        DELETE tt-unid-neg-nota.
    END.

    FIND emitente
        WHERE emitente.cod-emitente = INPUT FRAME fPage4 i-cod-emitente
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL emitente THEN DO:
        ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = "".
        MESSAGE "Cliente n�o cadastrado"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO i-cod-emitente IN FRAME fPage4.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        IF  emitente.identific = 1 THEN DO:
            ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = "".
            MESSAGE "Dever� ser informado um Fornecedor"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            APPLY 'entry' TO i-cod-emitente IN FRAME fPage4.
            RETURN NO-APPLY.
        END.
        ELSE
            ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = emitente.nome-abrev.
    END.

    FIND estabelec
        WHERE estabelec.cod-estabel = INPUT FRAME fPage4 c-cod-estab
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL estabelec THEN DO:
        ASSIGN c-nome-estab:SCREEN-VALUE IN FRAME fPage4 = "".
        MESSAGE "Estabelecimento n�o cadastrado"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO c-cod-estab IN FRAME fPage4.
        RETURN NO-APPLY.
    END.
    ELSE
        ASSIGN c-nome-estab:SCREEN-VALUE IN FRAME fPage4 = estabelec.nome.

    FIND tipo-rec-desp
        WHERE tipo-rec-desp.tp-codigo = INPUT FRAME fPage4 i-tp-codigo
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL tipo-rec-desp THEN DO:
        ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = "".
        MESSAGE "Tipo de Despesa n�o cadastrado"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO i-tp-codigo IN FRAME fPage4.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        IF  tipo-rec-desp.tipo <> 2 THEN DO:
            ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = "".
            MESSAGE "Dever� ser informado um Tipo de Despesa"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            APPLY 'entry' TO i-tp-codigo IN FRAME fPage4.
            RETURN NO-APPLY.
        END.
        ELSE
            ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = tipo-rec-desp.descricao.
    END.

    ASSIGN tt-param.cod-emitente = INPUT FRAME fPage4 i-cod-emitente
           tt-param.cod-estabel  = INPUT FRAME fPage4 c-cod-estab
           tt-param.tp-codigo    = INPUT FRAME fPage4 i-tp-codigo.

    /** ronil **/
    /**
    *  Colecionar os selecionados em docs receb tercieros e industriliz
    **/
/*     MESSAGE "ronil new"                    */
/*         VIEW-AS ALERT-BOX INFO BUTTONS OK. */
    EMPTY TEMP-TABLE tt-agregado.
    ASSIGN c-lista-sel = ""
           c-lista-sel-ag = "".

    DO i-cont = 1 TO brDigita:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
        brDigita:FETCH-SELECTED-ROW(i-cont) IN FRAME {&FRAME-NAME}.

        /**
        *  criar lista dos selecinados agregados
        **/
        IF c-lista-sel = "" THEN
            ASSIGN c-lista-sel = fnTextoCSNFS(INPUT tt-digita.cod-estabel,
                                              INPUT tt-digita.serie,
                                              INPUT tt-digita.nr-nota-fis).
        ELSE
            ASSIGN c-lista-sel = c-lista-sel + ";" + fnTextoCSNFS(INPUT tt-digita.cod-estabel,
                                                                  INPUT tt-digita.serie,
                                                                  INPUT tt-digita.nr-nota-fis).

        /**
        *  Verificar se a nota selecionada � de agregado
        **/
        FIND tt-nota-fiscal
            WHERE tt-nota-fiscal.cod-estabel = tt-digita.cod-estabel
              AND tt-nota-fiscal.serie       = tt-digita.serie
              AND tt-nota-fiscal.nr-nota-fis = tt-digita.nr-nota-fis
            NO-LOCK NO-ERROR.
        IF  AVAIL tt-nota-fiscal THEN 
        DO:
            FIND natur-oper
                WHERE natur-oper.nat-operacao = tt-nota-fiscal.nat-entrada
                NO-LOCK NO-ERROR.
            IF natur-oper.tipo-compra = 4 THEN /* material agregado */
            DO:
                /**
                *  criar lista dos selecinados agregados
                **/
                IF c-lista-sel-ag = "" THEN
                    ASSIGN c-lista-sel-ag = fnTextoCSNFS(INPUT tt-digita.cod-estabel,
                                                         INPUT tt-digita.serie,
                                                         INPUT tt-digita.nr-nota-fis).
                ELSE
                    ASSIGN c-lista-sel-ag = c-lista-sel-ag + ";" + fnTextoCSNFS(INPUT tt-digita.cod-estabel,
                                                                             INPUT tt-digita.serie,
                                                                             INPUT tt-digita.nr-nota-fis).
            END.
        END.
    END.

/*     MESSAGE c-lista-sel SKIP               */
/*             c-lista-sel-ag                 */
/*         VIEW-AS ALERT-BOX INFO BUTTONS OK. */

    /**
    *  Verificar se o rel agregados foram selecionados corretamente
    **/
    DO i-cont = 1 TO NUM-ENTRIES(c-lista-sel-ag,";").
        ASSIGN c-lista-nfs = ENTRY(i-cont, REPLACE(c-lista-sel-ag,'"',''), ";").

        FIND FIRST tt-nota-fiscal
            WHERE tt-nota-fiscal.cod-estabel = TRIM(ENTRY(1,c-lista-nfs))
              AND tt-nota-fiscal.serie       = TRIM(ENTRY(2,c-lista-nfs))
              AND tt-nota-fiscal.nr-nota-fis = TRIM(ENTRY(3,c-lista-nfs))
            NO-LOCK NO-ERROR.

        FOR EACH tt-it-nota-fisc OF tt-nota-fiscal
            NO-LOCK.

            /**
            *  buscar a nota de retorno de servi�o
            **/
            FIND FIRST b-tt-it-nota-fisc
                WHERE b-tt-it-nota-fisc.cod-estabel =  tt-nota-fiscal.cod-estabel
                  AND b-tt-it-nota-fisc.serie       =  tt-nota-fiscal.serie
                  AND b-tt-it-nota-fisc.nr-nota-fis >  tt-nota-fiscal.nr-nota-fis
                  AND b-tt-it-nota-fisc.cod-servico =  tt-it-nota-fisc.nr-ord-produ
                NO-LOCK NO-ERROR.
            IF AVAIL b-tt-it-nota-fisc THEN
            DO:
                IF LOOKUP(fnTextoCSNFS(INPUT b-tt-it-nota-fisc.cod-estabel,
                                       INPUT b-tt-it-nota-fisc.serie,
                                       INPUT b-tt-it-nota-fisc.nr-nota-fis), c-lista-sel, ";") > 0 THEN
                DO:
                    CREATE tt-agregado.
                    ASSIGN tt-agregado.cod-estabel  = tt-nota-fiscal.cod-estabel
                           tt-agregado.serie        = tt-nota-fiscal.serie
                           tt-agregado.nr-nota-fis  = tt-nota-fiscal.nr-nota-fis
                           tt-agregado.estab-comp   = b-tt-it-nota-fisc.cod-estabel
                           tt-agregado.serie-comp   = b-tt-it-nota-fisc.serie
                           tt-agregado.nr-nota-comp = b-tt-it-nota-fisc.nr-nota-fis
                           tt-agregado.nr-ord-produ = b-tt-it-nota-fisc.cod-servico.
                END.
                ELSE
                DO:
                    RUN pi-gerar-erro (INPUT 9999,
                                       INPUT 'Nota de Retorno Simb�lico sem sua respectiva nota de cobran�a de industrializa��o').
                END.
            END.
        END.
    END.

    /**
    *  Verificar se os selecionados possui ordem de producao por�m n�o foi gerado agregado
    **/
    DO i-cont = 1 TO NUM-ENTRIES(c-lista-sel,";").
        ASSIGN c-lista-nfs = ENTRY(i-cont, REPLACE(c-lista-sel,'"',''), ";").

        FIND FIRST tt-nota-fiscal
            WHERE tt-nota-fiscal.cod-estabel = TRIM(ENTRY(1,c-lista-nfs))
              AND tt-nota-fiscal.serie       = TRIM(ENTRY(2,c-lista-nfs))
              AND tt-nota-fiscal.nr-nota-fis = TRIM(ENTRY(3,c-lista-nfs))
            NO-LOCK NO-ERROR.
        FOR EACH tt-it-nota-fisc OF tt-nota-fiscal
            NO-LOCK.
            IF tt-it-nota-fisc.cod-servico <> 0 THEN
            DO:
                FIND FIRST tt-agregado
                    WHERE tt-agregado.cod-estabel  = tt-it-nota-fisc.cod-estabel
                      AND tt-agregado.serie-comp   = tt-it-nota-fisc.serie
                      AND tt-agregado.nr-nota-comp = tt-it-nota-fisc.nr-nota-fis
                    NO-LOCK NO-ERROR.
                IF NOT AVAIL tt-agregado THEN
                DO:
                    RUN pi-gerar-erro (INPUT 9999,
                                       INPUT "Nota de Cobran�a de Industrializa��o sem sua respectiva Nota de Retorno Simb�lico").
                END.
            END.
        END.
    END.

    /**
    *  Converter nota de agregado em movto-pend
    **/
    EMPTY TEMP-TABLE tt-movto-pend.
    FOR EACH tt-agregado NO-LOCK
        BREAK BY tt-agregado.cod-estabel
              BY tt-agregado.serie
              BY tt-agregado.nr-nota-fis
              BY tt-agregado.nr-ord-produ.

        IF FIRST-OF(tt-agregado.nr-ord-produ) THEN
            ASSIGN i-seq = 0.

        ASSIGN i-seq = i-seq + 10.

        IF LAST-OF(tt-agregado.nr-ord-produ) THEN
        DO:
            /**
            *  Buscar dados da nota retorno de terceiro
            **/
            FIND tt-nota-fiscal
                WHERE tt-nota-fiscal.cod-estabel = tt-agregado.cod-estabel
                  AND tt-nota-fiscal.serie       = tt-agregado.serie
                  AND tt-nota-fiscal.nr-nota-fis = tt-agregado.nr-nota-fis
                NO-LOCK NO-ERROR.
            IF AVAIL tt-nota-fiscal THEN
            DO:
                CREATE tt-movto-pend.
                ASSIGN tt-movto-pend.serie-docto  = tt-nota-fiscal.serie
                       tt-movto-pend.nro-docto    = tt-nota-fiscal.nr-nota-fis
                       tt-movto-pend.cod-emitente = emitente.cod-emitente /*tt-nota-fiscal.cod-emitente*/
                       tt-movto-pend.nat-operacao = tt-nota-fiscal.nat-entrada
                       tt-movto-pend.tipo         = 2
                       tt-movto-pend.sequencia    = i-seq
                       tt-movto-pend.nr-ord-produ = tt-agregado.nr-ord-produ.
    
                FIND estab-mat
                    WHERE estab-mat.cod-estabel = tt-nota-fiscal.cod-estabel
                    NO-LOCK NO-ERROR.
                IF AVAIL estab-mat THEN
                    ASSIGN tt-movto-pend.conta-contabil = estab-mat.conta-fornec.
    
                /**
                *  buscar dados da nota do retorno de terceiro
                **/
                FIND tt-nota-fiscal
                    WHERE tt-nota-fiscal.cod-estabel = tt-agregado.estab-comp
                      AND tt-nota-fiscal.serie       = tt-agregado.serie-comp
                      AND tt-nota-fiscal.nr-nota-fis = tt-agregado.nr-nota-comp
                    NO-LOCK NO-ERROR.
                IF AVAIL tt-nota-fiscal THEN
                    ASSIGN tt-movto-pend.nro-comp = tt-nota-fiscal.nr-nota-fis
                           tt-movto-pend.serie-comp = tt-nota-fiscal.serie
                           tt-movto-pend.nat-comp = tt-nota-fiscal.nat-entrada.
            END.
        END.
    END.

/*     FOR EACH tt-movto-pend NO-LOCK.             */
/*         MESSAGE tt-movto-pend.nro-docto SKIP    */
/*                 tt-movto-pend.nat-operacao SKIP */
/*                 tt-movto-pend.nro-comp SKIP     */
/*                 tt-movto-pend.nat-comp SKIP     */
/*             VIEW-AS ALERT-BOX INFO BUTTONS OK.  */
/*     END.                                        */

    /**
    *  Tratar erro
    **/
    IF CAN-FIND(tt-erro) THEN
    DO:
        RUN cdp/cd0666.p (INPUT TABLE tt-erro).
        RETURN.
    END.

    IF c-lista-sel-ag <> "" AND
        NOT CAN-FIND(FIRST tt-agregado) THEN 
    DO:
        RUN cdp/cd0666.p (INPUT TABLE tt-erro).
        RETURN.
    END.

    DO  i-cont = 1 TO brDigita:NUM-SELECTED-ROWS IN FRAME {&FRAME-NAME}:
        brDigita:FETCH-SELECTED-ROW(i-cont) IN FRAME {&FRAME-NAME}.

        FIND tt-nota-fiscal
            WHERE tt-nota-fiscal.cod-estabel = tt-digita.cod-estabel
              AND tt-nota-fiscal.serie       = tt-digita.serie
              AND tt-nota-fiscal.nr-nota-fis = tt-digita.nr-nota-fis
            NO-LOCK NO-ERROR.
        IF  AVAIL tt-nota-fiscal THEN 
        DO:
            IF  tt-nota-fiscal.nat-entrada = "" THEN DO:
                MESSAGE "Deve ser informada Natureza de Opera��o de Entrada" SKIP
                        "Est: " + tt-nota-fiscal.cod-estabel                 SKIP
                        "Ser: " + tt-nota-fiscal.serie                       SKIP
                        "Nota Fiscal: " + STRING(tt-nota-fiscal.nr-nota-fis,"9999999")
                        VIEW-AS ALERT-BOX ERROR BUTTONS OK.
                APPLY 'entry' TO brDigita IN FRAME fpage5.
                RETURN NO-APPLY.
            END.

            /*
            FIND nota-fiscal
                WHERE nota-fiscal.cod-estabel = tt-nota-fiscal.cod-estabel
                  AND nota-fiscal.serie       = tt-nota-fiscal.serie
                  AND nota-fiscal.nr-nota-fis = tt-nota-fiscal.nr-nota-fis
                NO-LOCK NO-ERROR.
            MESSAGE "nota-fiscal" AVAIL nota-fiscal SKIP
                    tt-digita.cod-estabel     
                    tt-digita.serie       
                    tt-digita.nr-nota-fis 
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            IF  AVAIL nota-fiscal THEN
            */
            RUN esp/esre001a.p (INPUT        ROWID(tt-nota-fiscal),
                                INPUT        TABLE tt-param,
                                INPUT-OUTPUT TABLE tt-docum-est,    
                                INPUT-OUTPUT TABLE tt-item-doc-est,
                                INPUT-OUTPUT TABLE tt-dupli-apagar,
                                INPUT-OUTPUT TABLE tt-dupli-imp,   
                                INPUT-OUTPUT TABLE tt-item-doc-est-aux,    
                                INPUT-OUTPUT TABLE tt-rat-lote, 
                                INPUT-OUTPUT TABLE tt-movto-pend,
                                INPUT-OUTPUT TABLE tt-erro,
                                OUTPUT l-erro).
        END.
        ELSE
            ASSIGN l-erro = YES.
    END.

    /*IF  NOT l-erro THEN DO:*/
        CREATE tt-versao-integr.        
        ASSIGN tt-versao-integr.registro              = 1
               tt-versao-integr.cod-versao-integracao = 004 NO-ERROR.
    
        RUN esp/esre001x.p (INPUT  TABLE tt-versao-integr,
                            INPUT  TABLE tt-docum-est,
                            INPUT  TABLE tt-item-doc-est,
                            INPUT  TABLE tt-dupli-apagar,
                            INPUT  TABLE tt-dupli-imp,
                            INPUT  TABLE tt-unid-neg-nota,
                            INPUT  TABLE tt-movto-pend,
                            OUTPUT TABLE tt-erro).
    /*END.*/

    IF  CAN-FIND(FIRST tt-erro) THEN
        RUN cdp/cd0666.w (INPUT TABLE tt-erro).
    ELSE DO:
        IF  NOT l-erro THEN
            RUN pi-relatorio.

        FOR EACH tt-item-doc-est-aux:
            FIND FIRST es-item-doc-est OF tt-item-doc-est-aux NO-ERROR.
            IF  AVAIL es-item-doc-est THEN 
                ASSIGN es-item-doc-est.nr-ord-ext  = tt-item-doc-est-aux.nr-ord-ext
                       es-item-doc-est.qt-ord-ext  = tt-item-doc-est-aux.qt-ord-ext         
                       es-item-doc-est.nat-oper-nf = tt-item-doc-est-aux.nat-oper-nf. 
        END.
    END.

    RUN pi-cria-tt-digita.
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fpage0
&Scoped-define SELF-NAME btCancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btCancel wReport
ON CHOOSE OF btCancel IN FRAME fpage0 /* Fechar */
DO:
    APPLY "CLOSE":U TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage7
&Scoped-define SELF-NAME btConfigImprDest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btConfigImprDest wReport
ON CHOOSE OF btConfigImprDest IN FRAME fPage7
DO:
   {report/imimp.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btDestinyFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btDestinyFile wReport
ON CHOOSE OF btDestinyFile IN FRAME fPage7
DO:
    {report/imarq.i cDestinyFile fPage7}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fpage0
&Scoped-define SELF-NAME btHelp2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btHelp2 wReport
ON CHOOSE OF btHelp2 IN FRAME fpage0 /* Ajuda */
DO:
    {include/ajuda.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage4
&Scoped-define SELF-NAME btInputFile
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btInputFile wReport
ON CHOOSE OF btInputFile IN FRAME fPage4
DO:
    {report/imarq.i cInputFile fPage4}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage5
&Scoped-define SELF-NAME btNatOper
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btNatOper wReport
ON CHOOSE OF btNatOper IN FRAME fPage5 /* Confirma Natureza de Operacao de Entrada */
DO:
    FIND natur-oper
        WHERE natur-oper.nat-operacao = INPUT FRAME fpage5 c-nat-entrada
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL natur-oper THEN DO:
        MESSAGE "Natureza de Opera��o inv�lida!"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO c-nat-entrada IN FRAME fpage5.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        IF  natur-oper.tipo <> 1 THEN DO:
            MESSAGE "Natureza de Opera��o deve ser tipo 'Entrada'"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            APPLY 'entry' TO c-nat-entrada IN FRAME fpage5.
            RETURN NO-APPLY.
        END.
        IF  natur-oper.transf THEN DO:
            MESSAGE "Natureza de Opera��o n�o pode ser de 'Transfer�ncia'"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            APPLY 'entry' TO c-nat-entrada IN FRAME fpage5.
            RETURN NO-APPLY.
        END.
    END.

    IF  brDigita:NUM-SELECTED-ROWS > 0 THEN DO ON ERROR UNDO, RETURN NO-APPLY:
        MESSAGE "Confirma assossiar a Natureza de Opera��o de Entrada informada para as Notas Fiscais selecionadas?"
                VIEW-AS ALERT-BOX QUESTION BUTTON YES-NO UPDATE l-confirma.
        IF  l-confirma THEN DO i-cont = 1 TO brDigita:NUM-SELECTED-ROWS IN FRAME fPage5:
            brDigita:FETCH-SELECTED-ROW(i-cont) IN FRAME fPage5.

            FIND tt-nota-fiscal
                WHERE tt-nota-fiscal.cod-estabel = tt-digita.cod-estabel
                  AND tt-nota-fiscal.serie       = tt-digita.serie
                  AND tt-nota-fiscal.nr-nota-fis = tt-digita.nr-nota-fis
                NO-LOCK NO-ERROR.
            IF  AVAIL tt-nota-fiscal THEN 
                ASSIGN tt-nota-fiscal.nat-entrada = c-nat-entrada:SCREEN-VALUE IN FRAME fPage5.
        END.
    END.
    RUN pi-cria-tt-digita.
    APPLY "choose" TO btSelecionar IN FRAME fPage5.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fpage0
&Scoped-define SELF-NAME btOK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOK wReport
ON CHOOSE OF btOK IN FRAME fpage0 /* Importar */
DO:
   do  on error undo, return no-apply:
       run piExecute.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage5
&Scoped-define SELF-NAME btOpen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btOpen wReport
ON CHOOSE OF btOpen IN FRAME fPage5 /* Atualizar */
RUN pi-cria-tt-digita.
APPLY "choose" TO btSelecionar IN FRAME fPage5.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btSelecionar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btSelecionar wReport
ON CHOOSE OF btSelecionar IN FRAME fPage5 /* Selecionar Todos */
DO:
    IF  brDigita:NUM-SELECTED-ROWS > 0 THEN DO ON ERROR UNDO, RETURN NO-APPLY:
        IF  brDigita:DESELECT-ROWS() IN FRAME fPage5 THEN.
    END.
    ELSE DO:
        IF  CAN-FIND(FIRST tt-digita) THEN
            IF  brDigita:SELECT-ALL() IN FRAME fPage5 THEN.
    END.
        
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage4
&Scoped-define SELF-NAME c-cod-estab
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-cod-estab wReport
ON F5 OF c-cod-estab IN FRAME fPage4 /* Estabelecimento */
DO:
    {include/zoomvar.i &prog-zoom="adzoom/z01ad107.w"
                       &campo=c-cod-estab
                       &campozoom=cod-estabel
                       &campo2=c-nome-estab
                       &campozoom2=nome
                       &FRAME=fPage4}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-cod-estab wReport
ON LEAVE OF c-cod-estab IN FRAME fPage4 /* Estabelecimento */
DO:
    FIND estabelec
        WHERE estabelec.cod-estabel = INPUT FRAME fPage4 c-cod-estab
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL estabelec THEN 
        ASSIGN c-nome-estab:SCREEN-VALUE IN FRAME fPage4 = "".
    ELSE
        ASSIGN c-nome-estab:SCREEN-VALUE IN FRAME fPage4 = estabelec.nome.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-cod-estab wReport
ON MOUSE-SELECT-DBLCLICK OF c-cod-estab IN FRAME fPage4 /* Estabelecimento */
DO:
    APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage5
&Scoped-define SELF-NAME c-nat-entrada
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-nat-entrada wReport
ON F5 OF c-nat-entrada IN FRAME fPage5 /* Natureza Operacao Entrada */
DO:
    {include/zoomvar.i &prog-zoom="inzoom/z01in245.w"
                       &campo=c-nat-entrada
                       &campozoom=nat-operacao
                       &campo2=c-desc-nat
                       &campozoom2=denominacao
                       &FRAME=fPage5}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-nat-entrada wReport
ON LEAVE OF c-nat-entrada IN FRAME fPage5 /* Natureza Operacao Entrada */
DO:
    FIND natur-oper
        WHERE natur-oper.nat-operacao = INPUT FRAME fPage5 c-nat-entrada
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL natur-oper THEN 
        ASSIGN c-desc-nat:SCREEN-VALUE IN FRAME fPage5 = "".
    ELSE
        ASSIGN c-desc-nat:SCREEN-VALUE IN FRAME fPage5 = natur-oper.denominacao.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-nat-entrada wReport
ON MOUSE-SELECT-DBLCLICK OF c-nat-entrada IN FRAME fPage5 /* Natureza Operacao Entrada */
DO:
    APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage4
&Scoped-define SELF-NAME i-cod-emitente
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL i-cod-emitente wReport
ON F5 OF i-cod-emitente IN FRAME fPage4 /* Emitente */
DO:
        
      {include/zoomvar.i &prog-zoom="adzoom/z02ad098.w"
                       &campo=i-cod-emitente
                       &campozoom=cod-emitente
                       &campo2=c-nome-abrev
                       &campozoom2=nome-abrev
                       &FRAME=fPage4}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL i-cod-emitente wReport
ON LEAVE OF i-cod-emitente IN FRAME fPage4 /* Emitente */
DO:
    FIND emitente
        WHERE emitente.cod-emitente = INPUT FRAME fPage4 i-cod-emitente
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL emitente THEN 
        ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = "".
    ELSE 
        ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = emitente.nome-abrev.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL i-cod-emitente wReport
ON MOUSE-SELECT-DBLCLICK OF i-cod-emitente IN FRAME fPage4 /* Emitente */
DO:
    APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME i-tp-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL i-tp-codigo wReport
ON F5 OF i-tp-codigo IN FRAME fPage4 /* Tipo Despesa */
DO:
    {include/zoomvar.i &prog-zoom="adzoom/z01ad259.w"
                       &campo=i-tp-codigo
                       &campozoom=tp-codigo
                       &campo2=c-desc-despesa
                       &campozoom2=descricao
                       &FRAME=fPage4}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL i-tp-codigo wReport
ON LEAVE OF i-tp-codigo IN FRAME fPage4 /* Tipo Despesa */
DO:
    FIND tipo-rec-desp
        WHERE tipo-rec-desp.tp-codigo = INPUT FRAME fPage4 i-tp-codigo
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL tipo-rec-desp THEN 
        ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = "".
    ELSE 
        ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = tipo-rec-desp.descricao.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL i-tp-codigo wReport
ON MOUSE-SELECT-DBLCLICK OF i-tp-codigo IN FRAME fPage4 /* Tipo Despesa */
DO:
    APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fPage7
&Scoped-define SELF-NAME rsDestiny
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsDestiny wReport
ON VALUE-CHANGED OF rsDestiny IN FRAME fPage7
DO:
do  with frame fPage7:
    case self:screen-value:
        when "1" then do:
            assign cDestinyFile:sensitive     = no
                   cDestinyFile:visible       = yes
                   btDestinyFile:visible      = no
                   btConfigImprDest:visible   = yes.
        end.
        when "2" then do:
            assign cDestinyFile:sensitive     = yes
                   cDestinyFile:visible       = yes
                   btDestinyFile:visible      = yes
                   btConfigImprDest:visible   = no.
        end.
        when "3" then do:
            assign cDestinyFile:sensitive     = no
                   cDestinyFile:visible       = no
                   btDestinyFile:visible      = no
                   btConfigImprDest:visible   = no.
        end.
    end case.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rsExecution
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rsExecution wReport
ON VALUE-CHANGED OF rsExecution IN FRAME fPage7
DO:
   {report/imrse.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME fpage0
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wReport 


/*--- L�gica para inicializa��o do programam ---*/

i-cod-emitente:LOAD-MOUSE-POINTER("image/lupa.cur":U) IN FRAME fPage4.
c-cod-estab:LOAD-MOUSE-POINTER("image/lupa.cur":U) IN FRAME fPage4.
i-tp-codigo:LOAD-MOUSE-POINTER("image/lupa.cur":U) IN FRAME fPage4.
c-nat-entrada:LOAD-MOUSE-POINTER("image/lupa.cur":U) IN FRAME fPage5.

{report/MainBlock.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE afterInitializeInterface wReport 
PROCEDURE afterInitializeInterface :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DISABLE c-nome-abrev
        c-nome-estab
        c-desc-despesa
        WITH FRAME fPage4.

ENABLE btAtualizar
       bt-eliminar 
       btNatOper 
       WITH FRAME fPage5.


FOR EACH tt-nota-fiscal:
    DELETE tt-nota-fiscal.
END.
FOR EACH tt-it-nota-fisc:
    DELETE tt-it-nota-fisc.
END.
FOR EACH tt-fat-ser-lote:
    DELETE tt-fat-ser-lote.
END.
FOR EACH tt-fat-duplic:
    DELETE tt-fat-duplic.
END.
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-cria-tt-digita wReport 
PROCEDURE pi-cria-tt-digita :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

FOR EACH tt-digita:
    DELETE tt-digita.
END. 


FOR EACH tt-nota-fiscal NO-LOCK:
    
    /*,
    FIRST emitente 
        WHERE emitente.cod-emitente = tt-nota-fiscal.cod-emitente:*/

    FIND tt-digita
        WHERE tt-digita.cod-estabel = tt-nota-fiscal.cod-estabel
          AND tt-digita.serie       = tt-nota-fiscal.serie
          AND tt-digita.nr-nota-fis = tt-nota-fiscal.nr-nota-fis
        NO-ERROR.
    IF  NOT AVAIL tt-digita THEN DO:
        CREATE tt-digita.
        BUFFER-COPY tt-nota-fiscal TO tt-digita.
        ASSIGN tt-digita.nome-abrev = tt-nota-fiscal.nome-abrev.
    END.
END.

OPEN QUERY brDigita FOR EACH tt-digita,
                        FIRST tt-nota-fiscal OF tt-digita.

APPLY 'entry' TO brDigita IN FRAME fpage5.

APPLY 'CHOOSE' TO btSelecionar IN FRAME fpage5.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-gerar-erro wReport 
PROCEDURE pi-gerar-erro :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEFINE INPUT PARAM pErro AS INTEGER NO-UNDO.
    DEFINE INPUT PARAM pMensagem AS CHAR NO-UNDO.

    DEFINE VARIABLE i-seq AS INTEGER    NO-UNDO.


    /* main */
    FIND LAST tt-erro NO-LOCK NO-ERROR.
    IF AVAIL tt-erro THEN
        ASSIGN i-seq = tt-erro.i-sequen + 10.
    ELSE
        ASSIGN i-seq = 10.

    CREATE tt-erro.
    ASSIGN tt-erro.i-sequen = i-seq
           tt-erro.cd-erro = pErro
           tt-erro.mensagem = pMensagem.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-relatorio wReport 
PROCEDURE pi-relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
{report/rpexa.i}

{report/imexb.i}

if  session:set-wait-state("GENERAL":U) then.

{report/imrun.i esp/esre001rpa.p}

{report/imexc.i}

if  session:set-wait-state("") then.
    
{report/imtrm.i tt-param.arq-destino tt-param.destino}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE piExecute wReport 
PROCEDURE piExecute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

define var r-tt-docum-est as rowid no-undo.

/*** Importacao/Exportacao ***/
do  on error undo, return error
    on stop  undo, return error:     

    {report/rpexa.i}

    FIND emitente
        WHERE emitente.cod-emitente = INPUT FRAME fPage4 i-cod-emitente
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL emitente THEN DO:
        ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = "".
        MESSAGE "Cliente n�o cadastrado"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO i-cod-emitente IN FRAME fPage4.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        IF  emitente.identific = 1 THEN DO:
            ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = "".
            MESSAGE "Dever� ser informado um Fornecedor"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            APPLY 'entry' TO i-cod-emitente IN FRAME fPage4.
            RETURN NO-APPLY.
        END.
        ELSE
            ASSIGN c-nome-abrev:SCREEN-VALUE IN FRAME fPage4 = emitente.nome-abrev.
    END.

    FIND estabelec
        WHERE estabelec.cod-estabel = INPUT FRAME fPage4 c-cod-estab
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL estabelec THEN DO:
        ASSIGN c-nome-estab:SCREEN-VALUE IN FRAME fPage4 = "".
        MESSAGE "Estabelecimento n�o cadastrado"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO c-cod-estab IN FRAME fPage4.
        RETURN NO-APPLY.
    END.
    ELSE
        ASSIGN c-nome-estab:SCREEN-VALUE IN FRAME fPage4 = estabelec.nome.

    FIND tipo-rec-desp
        WHERE tipo-rec-desp.tp-codigo = INPUT FRAME fPage4 i-tp-codigo
        NO-LOCK NO-ERROR.
    IF  NOT AVAIL tipo-rec-desp THEN DO:
        ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = "".
        MESSAGE "Tipo de Despesa n�o cadastrado"
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        APPLY 'entry' TO i-tp-codigo IN FRAME fPage4.
        RETURN NO-APPLY.
    END.
    ELSE DO:
        IF  tipo-rec-desp.tipo <> 2 THEN DO:
            ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = "".
            MESSAGE "Dever� ser informado um Tipo de Despesa"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            APPLY 'entry' TO i-tp-codigo IN FRAME fPage4.
            RETURN NO-APPLY.
        END.
        ELSE
            ASSIGN c-desc-despesa:SCREEN-VALUE IN FRAME fPage4 = tipo-rec-desp.descricao.
    END.

    if  input frame fPage7 rsDestiny = 2 and
        input frame fPage7 rsExecution = 1 then do:
        run utp/ut-vlarq.p (input input frame fPage7 cDestinyFile).
        if  return-value = "NOK":U then do:
            run utp/ut-msgs.p (input "SHOW":U,
                               input 73,
                               input "").
            apply "ENTRY":U to cDestinyFile in frame fPage7.                   
            return error.
        end.
    end.

    IF  input frame fPage4 cInputFile = "" THEN DO:
        MESSAGE "Arquivo inv�lido!"
                VIEW-AS ALERT-BOX ERROR BUTTONS OK.
        apply "ENTRY":U to cInputFile in frame fPage4.                   
        return error.
    END.
    
    assign file-info:file-name = input frame fPage4 cInputFile.
    if  file-info:pathname = ? and
        input frame fPage7 rsExecution = 1 then do:
        run utp/ut-msgs.p (input "SHOW":U,
                           input 326,
                           input cInputFile).                               
        apply "ENTRY":U to cInputFile in frame fPage4.                
        return error.
    end. 
            
    /* Coloque aqui as valida��es das outras p�ginas, lembrando que elas
       devem apresentar uma mensagem de erro cadastrada, posicionar na p�gina 
       com problemas e colocar o focus no campo com problemas             */    

         
    create tt-param.
    assign tt-param.usuario         = c-seg-usuario
           tt-param.destino         = input frame fPage7 rsDestiny
           tt-param.arq-entrada     = input frame fPage4 cInputFile
           tt-param.data-exec       = today
           tt-param.hora-exec       = time.

    if  tt-param.destino = 1 then
        assign tt-param.arq-destino = "".
    else
    if  tt-param.destino = 2 then 
        assign tt-param.arq-destino = input frame fPage7 cDestinyFile.
    else
        assign tt-param.arq-destino = session:temp-directory + c-programa-mg97 + ".tmp":U.

    /* Coloque aqui a l�gica de grava��o dos par�mtros e sele��o na temp-table
       tt-param */ 

    ASSIGN tt-param.cod-emitente = INPUT FRAME fPage4 i-cod-emitente
           tt-param.cod-estabel  = INPUT FRAME fPage4 c-cod-estab
           tt-param.tp-codigo    = INPUT FRAME fPage4 i-tp-codigo.
           
    /*
    FOR EACH tt-docum-est:
        DELETE tt-docum-est.
    END.

    DO  i-cont = 1 TO brDigita:NUM-SELECTED-ROWS IN FRAME fPage5:
        brDigita:FETCH-SELECTED-ROW(i-cont) IN FRAME fPage5.

        FIND tt-docum-est
            WHERE tt-docum-est.cod-emitente = tt-digita.cod-emitente
              AND tt-docum-est.serie-docto  = tt-digita.serie-docto
              AND tt-docum-est.nro-docto    = tt-digita.nro-docto
              AND tt-docum-est.nat-operacao = tt-digita.nat-operacao
            NO-ERROR.
        IF  NOT AVAIL tt-docum-est THEN DO:
            CREATE tt-docum-est.
            BUFFER-COPY tt-digita TO tt-docum-est.
        END.
    END.
    */

    {report/imexb.i}

    if  session:set-wait-state("GENERAL":U) then.

    {report/imrun.i esp/esre001rp.p}

    /*{report/imexc.i}

    if  session:set-wait-state("") then.
    
    {report/imtrm.i tt-param.arq-destino tt-param.destino}*/

    RUN pi-cria-tt-digita.
    
end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fnTextoCSNFS wReport 
FUNCTION fnTextoCSNFS RETURNS CHARACTER
  ( INPUT pEstab AS CHAR,
    INPUT pSerie AS CHAR,
    INPUT pNota  AS CHAR) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

    DEFINE VARIABLE c-ret AS CHARACTER  NO-UNDO.

    /* amin */
    ASSIGN c-ret = CHR(34) + 
                   pEstab + "," +
                   pSerie + "," + 
                   pNota + "," +
                   CHR(34).

  RETURN c-ret.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

