/* N�o foi feita a replica��o da l�gica, pois, tanto no layout 1.10 como no     */
/* layout novo 2.00, a l�gica da include abaixo � a mesma. Caso seja necess�rio */ 
/* alterar a l�gica da mesma apenas para o layout 2.00 da NF-e, solicitamos que */
/* o c�digo da include seja copiado para esse fonte, e a altera��o seja feita   */
/* somente nele.                                                                */

{adapters/xml/ep2/axsep006extradeclarations.i}

/* NOVAS DEFINICOES PARA O LAYOUT 2.00 DA NF-E */

DEFINE VARIABLE l-IPITributado         AS LOGICAL     NO-UNDO.
DEFINE VARIABLE cHoraContingencia      AS CHARACTER   NO-UNDO.
DEFINE VARIABLE cCodEstabel            AS CHARACTER   NO-UNDO.
DEFINE VARIABLE de-vlzfm               AS DECIMAL     NO-UNDO.
DEFINE VARIABLE de-icmzfm              AS DECIMAL     NO-UNDO.
DEFINE VARIABLE de-descto-icms         AS DECIMAL     NO-UNDO.
DEFINE VARIABLE de-vl-servico          AS DECIMAL     NO-UNDO.
DEFINE VARIABLE d-total-pis-serv       AS DECIMAL    NO-UNDO.
DEFINE VARIABLE d-total-cofins-serv    AS DECIMAL    NO-UNDO.

DEFINE BUFFER bf-it-nota-fisc-rat  FOR it-nota-fisc. /* Leitura it-nota-fisc para Rateios do Seguro e Outras Despesas  */



DEFINE TEMP-TABLE tt-rateio-seguro NO-UNDO
    FIELD nr-seq-fat LIKE it-nota-fisc.nr-seq-fat
    FIELD it-codigo  LIKE it-nota-fisc.it-codigo
    FIELD percentual AS DEC
    FIELD valor      AS DEC.

DEFINE TEMP-TABLE tt-rateio-vOutro NO-UNDO
    FIELD nr-seq-fat LIKE it-nota-fisc.nr-seq-fat
    FIELD it-codigo  LIKE it-nota-fisc.it-codigo
    FIELD percentual AS DEC
    FIELD valor      AS DEC.
