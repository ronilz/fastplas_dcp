/**************************************************************************
**
**   Include: BCAPI004.I - Definicao da temp-table Relacao de Programas com
**                         o Tipo da Transa��o
**
***************************************************************************/

def temp-table tt-prog-bc NO-UNDO
    field cod-versao-integracao     as integer
    field usuario                   as char     format "X(12)"
    field cod-prog-dtsul            as char     format "X(50)"
    field cd-trans                  as char     format "X(12)"
    field descricao                 as char     format "X(12)"
    field prog-api-atualizacao      as char     format "X(12)"
    field prog-altera               as char     format "X(12)"
    field prog-etiq                 as char     format "X(12)"
    field prog-import               as char     format "X(12)"
    field prog-criacao              as char     format "X(12)"
    field prog-leitura              as char     format "X(12)"
    field gera-etiq-total           as logical
    field imp-apos-trans            as logical
    field atualiza-on-line          as logical
    field erros-on-line             as logical
    field atualiz-etiq              as logical
    field baixa-reserva             as integer  format ">9"
    field codigo-rejei              as integer  format ">>9"
    field conta-aplicacao           as char     format "X(17)"
    field conta-refugo              as char     format "X(17)"
    field motivo-backflush          as integer  format ">9"
    field muda-status               as logical
    field nome-dir-etiq             as char     format "X(100)"
    field nr-trans                  as decimal  format ">>>>>>>>>9"
    field data                      as date
    field data-atualizacao          as date
    field detalhe                   as char     format "X(60)"
    field ep-codigo                 as integer  format ">>9"
    field estado-trans              as integer  format ">9"
    field etiq-impressa             as logical
    field hora-atualizacao          as char     format "X(08)"
    field horario                   as char     format "X(08)"
    field log-1                     as logical
    field opcao                     as integer  format "99"     initial 1.
