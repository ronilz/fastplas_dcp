/*******************************************************************************
** Programa...: ES0004RP
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..: 
*******************************************************************************/
/* Include de Controle de Vers�o */
{include/i-prgvrs.i ESDC013RP.P 1.00.00.000}
define buffer empresa for mgcad.empresa.

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD c-rg-ini         AS CHAR FORMAT "x(13)"
    FIELD c-rg-fim         AS CHAR FORMAT "x(13)".

DEFINE TEMP-TABLE tt-raw-digita
    FIELD raw-digita    AS RAW.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR h-acomp AS HANDLE NO-UNDO.

/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ESDC021RP"
       c-versao       = "1.00"
       c-revisao      = "1.00.000"
       c-empresa      = empresa.nome
       c-sistema      = "ESP"
       c-titulo-relat = c-titulo-relat.

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).

FIND FIRST dc-param NO-LOCK NO-ERROR.

PUT UNFORMATTED " Rg. item      Item             Item Pai         Dep Localiz.   PCtrl Retr Cor    Operador     Data       Hora       Aprovador   "  SKIP
                " ------------- ---------------- ---------------- --- ---------- ----- ---- ------ ------------ ---------- ---------- ------------" SKIP.


FOR EACH dc-aponta NO-LOCK
    
    WHERE dc-aponta.rg-item >= tt-param.c-rg-ini
      AND dc-aponta.rg-item <= tt-param.c-rg-fim,
    FIRST dc-rg-item NO-LOCK
    WHERE dc-rg-item.rg-item = dc-aponta.rg-item,
    FIRST dc-pto-controle NO-LOCK
    WHERE dc-pto-controle.cod-pto-controle = dc-aponta.cod-pto-controle
      BREAK by dc-aponta.rg-item.
      
    /*
    FIND FIRST dc-rg-item
         WHERE dc-rg-item.rg-item = dc-aponta.rg-item 
         NO-LOCK NO-ERROR.
         
    IF NOT AVAIL dc-rg-item THEN
        NEXT.
        
    FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = dc-aponta.cod-pto-controle
        NO-LOCK NO-ERROR.
                 
    IF NOT AVAIL dc-pto-controle THEN
        NEXT.
    */
      
    PUT UNFORMATTED " "
        string(dc-aponta.rg-item,"x(13)")
        AT 02
        STRING(dc-aponta.it-codigo,"x(16)")
        AT 16
        STRING(dc-aponta.it-codigo-pai,"x(16)")
        AT 33
        STRING(dc-pto-controle.cod-depos-dest,"x(03)")
        AT 50
        STRING(dc-pto-controle.cod-localiz-dest,"x(10)")
        AT 54
        STRING(dc-aponta.cod-pto-controle,"x(05)")
        AT 65
        string(dc-aponta.nr-retrabalho)
        AT 71
        STRING(dc-aponta.cod-cor)
        AT 76
        .
        
    if LAST-OF (dc-aponta.rg-item) THEN do:      
        FOR EACH dc-aponta-retrabalho
            WHERE dc-aponta-retrabalho.rg-item = dc-aponta.rg-item
            NO-LOCK.
            
            PUT UNFORMATTED " "
                STRING(dc-aponta-retrabalho.cod-usuario)
                AT 83
                STRING(dc-aponta-retrabalho.data,"99/99/9999")
                AT 96
                STRING(dc-aponta-retrabalho.hora)
                AT 107
                STRING(dc-aponta-retrabalho.cod-usuar-auth)
                AT 118 
                STRING(dc-aponta-retrabalho.cod-cor)
                AT 132
                SKIP.  
        END.
                        
    END.    
END.

RUN pi-finalizar IN h-acomp.
RETURN "OK":U.
