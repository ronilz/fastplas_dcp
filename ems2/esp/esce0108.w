&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgcad            PROGRESS
*/
&Scoped-define WINDOW-NAME w-livre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-livre 
/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/

define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESCE0108 2.06.00.000}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
DEFINE TEMP-TABLE tt-item-control NO-UNDO
    FIELD it-codigo   AS CHARACTER 
    FIELD msg-return  AS CHARACTER .

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE cContrItem       AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE h-acomp          AS HANDLE                      NO-UNDO.
DEFINE VARIABLE l-return-erro    AS LOGICAL                     NO-UNDO.
DEFINE VARIABLE cReturMsg        AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE de-medio-mat1    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-mat2    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-mat3    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-mob1    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-mob2    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-mob3    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-ggf1    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-ggf2    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE de-medio-ggf3    AS DECIMAL                     NO-UNDO.
DEFINE VARIABLE i-empresa        LIKE param-global.empresa-prin NO-UNDO.
DEFINE VARIABLE da-ult-per       LIKE param-estoq.ult-per-fech  NO-UNDO.
DEFINE VARIABLE da-ini-periodo   AS DATE                        NO-UNDO.    
DEFINE VARIABLE da-fim-periodo   AS DATE                        NO-UNDO.    
DEFINE VARIABLE i-ano-corrente   AS INTEGER                     NO-UNDO.    
DEFINE VARIABLE i-per-corrente   AS INTEGER                     NO-UNDO.    
DEFINE VARIABLE da-iniper-fech   AS DATE                        NO-UNDO.    
DEFINE VARIABLE da-fimper-fech   AS DATE                        NO-UNDO.    
DEFINE VARIABLE c-conta-contabil AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE c-desc-grupo     AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE c-desc-familia   AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE c-desc-tab       AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE c-depos-pad      AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE c-desc-dep       AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE c-serie          AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE i-docto          AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE v_msg_1          AS CHARACTER                   NO-UNDO.
DEFINE VARIABLE i-mo-fasb        AS INTEGER                     NO-UNDO.
DEFINE VARIABLE i-mo-cmi         AS INTEGER                     NO-UNDO.
DEFINE VARIABLE da-ult-fech-dia  AS DATE                        NO-UNDO.
DEFINE VARIABLE l-nota-ft        AS LOGICAL                     NO-UNDO.

def new shared var i-emp050      like conta-contab.ep-codigo.
def new shared var i-cta050      like conta-contab.ct-codigo.
def new shared var i-sbc050      like conta-contab.sc-codigo.
def new shared var i-red050      like conta-contab.reduzida.
def new shared var cg-tipo-z       as character.
def new shared var cg-sis-z        as character.

/* Begins -> Definition Temp-Table ------------------------------------*/
def temp-table tt-param
    field tipo       as integer 
    field medio-mat   as dec decimals 4 extent 3 format ">>>,>>>,>>9.9999"
    field medio-mob   as dec decimals 4 extent 3 format ">>>,>>>,>>9.9999"
    field medio-ggf    as dec decimals 4 extent 3 format ">>>,>>>,>>9.9999"
    field r-conta      as rowid 
    field da-inipa-x   like movto-estoq.dt-trans
    field depos-pad    like movto-estoq.cod-depos
    field serie1       like movto-estoq.serie-docto
    field docto1       like movto-estoq.nro-docto
    field it-codigo    like item.it-codigo
    field parametro    as char format "x(30)".

def temp-table tt-item-2 like item use-index codigo.
/* End -> Definition Temp-Table ------------------------------------*/
define buffer moeda   for mgcad.moeda.

/* Begins -> Definition Include ------------------------------------*/
{cep/ce1234.i}         /* Ajuste de Decimais - Chile */
{include/i_fnctrad.i}
{cdp/cdcfgdis.i}
{cdp/cdcfgmat.i}
{cdp/cd0666.i}      /* Definicao da temp-table de erros */
/* End    -> Definition Include ------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE w-livre
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-cad
&Scoped-define BROWSE-NAME br-itens-controle

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES item

/* Definitions for BROWSE br-itens-controle                             */
&Scoped-define FIELDS-IN-QUERY-br-itens-controle item.it-codigo item.desc-item /* item.tipo-contr */ fcTipContr(item.tipo-contr) @ cContrItem fcMsgAtu() @ cReturMsg   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-itens-controle   
&Scoped-define SELF-NAME br-itens-controle
&Scoped-define QUERY-STRING-br-itens-controle FOR EACH item                            WHERE item.ge-codigo >= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-ini                              AND item.ge-codigo <= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-fin                              AND ITEM.it-codigo >= INPUT FRAME {&FRAME-NAME} fi-it-codigo-ini                              AND ITEM.it-codigo <= INPUT FRAME {&FRAME-NAME} fi-it-codigo-fin NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-br-itens-controle OPEN QUERY {&SELF-NAME} FOR EACH item                            WHERE item.ge-codigo >= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-ini                              AND item.ge-codigo <= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-fin                              AND ITEM.it-codigo >= INPUT FRAME {&FRAME-NAME} fi-it-codigo-ini                              AND ITEM.it-codigo <= INPUT FRAME {&FRAME-NAME} fi-it-codigo-fin NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-br-itens-controle item
&Scoped-define FIRST-TABLE-IN-QUERY-br-itens-controle item


/* Definitions for FRAME f-cad                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f-cad ~
    ~{&OPEN-QUERY-br-itens-controle}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rt-button RECT-10 RECT-11 IMAGE-1 IMAGE-2 ~
IMAGE-3 IMAGE-4 fi-ge-codigo-ini fi-ge-codigo-fin fi-it-codigo-ini ~
fi-it-codigo-fin bt-filtro rd-tipo-contr fi-conta-contabil bt-alterar ~
br-itens-controle 
&Scoped-Define DISPLAYED-OBJECTS fi-ge-codigo-ini fi-ge-codigo-fin ~
fi-it-codigo-ini fi-it-codigo-fin rd-tipo-contr fi-conta-contabil c-titulo 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */
&Scoped-define List-2 fi-conta-contabil 
&Scoped-define List-3 fi-conta-contabil 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fcMsgAtu w-livre 
FUNCTION fcMsgAtu RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD fcTipContr w-livre 
FUNCTION fcTipContr RETURNS CHARACTER
  ( iTipControle AS INTEGER /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-livre AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU mi-programa 
       MENU-ITEM mi-consultas   LABEL "Co&nsultas"     ACCELERATOR "CTRL-L"
       MENU-ITEM mi-imprimir    LABEL "&Relat�rios"    ACCELERATOR "CTRL-P"
       RULE
       MENU-ITEM mi-sair        LABEL "&Sair"          ACCELERATOR "CTRL-X".

DEFINE SUB-MENU m_Ajuda 
       MENU-ITEM mi-conteudo    LABEL "&Conteudo"     
       MENU-ITEM mi-sobre       LABEL "&Sobre..."     .

DEFINE MENU m-livre MENUBAR
       SUB-MENU  mi-programa    LABEL "&Nome-do-Programa"
       SUB-MENU  m_Ajuda        LABEL "&Ajuda"        .


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_p-exihel AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-alterar 
     IMAGE-UP FILE "image\im-sav":U
     IMAGE-INSENSITIVE FILE "image\ii-sav":U
     LABEL "Alterar Tipo de Controle" 
     SIZE 4 BY 1 TOOLTIP "Alterar Tipo de Controle"
     FONT 4.

DEFINE BUTTON bt-filtro 
     IMAGE-UP FILE "image\im-sav":U
     IMAGE-INSENSITIVE FILE "image\ii-sav":U
     LABEL "Filtrar" 
     SIZE 4 BY 1 TOOLTIP "Filtrar"
     FONT 4.

DEFINE VARIABLE c-titulo AS CHARACTER FORMAT "X(32)":U 
     VIEW-AS FILL-IN 
     SIZE 41.43 BY .88 NO-UNDO.

DEFINE VARIABLE fi-conta-contabil AS CHARACTER FORMAT "X(17)":U 
     LABEL "Conta Aplica��o" 
     VIEW-AS FILL-IN 
     SIZE 20.72 BY .88 NO-UNDO.

DEFINE VARIABLE fi-ge-codigo-fin AS INTEGER FORMAT ">9" INITIAL 99 
     VIEW-AS FILL-IN 
     SIZE 3.14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-ge-codigo-ini AS INTEGER FORMAT ">9" INITIAL 0 
     LABEL "Grupo Estoque":R16 
     VIEW-AS FILL-IN 
     SIZE 3.14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-it-codigo-fin AS CHARACTER FORMAT "x(16)" INITIAL "ZZZZZZZZZZZZZZZZ" 
     VIEW-AS FILL-IN 
     SIZE 17.14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-it-codigo-ini AS CHARACTER FORMAT "x(16)" 
     LABEL "Item":R5 
     VIEW-AS FILL-IN 
     SIZE 17.14 BY .88 NO-UNDO.

DEFINE IMAGE IMAGE-1
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-2
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-3
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-4
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE VARIABLE rd-tipo-contr AS INTEGER INITIAL 2 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "F�sico", 1,
"Total", 2,
"Consignado", 3,
"D�bito Direto", 4
     SIZE 60 BY .67.

DEFINE RECTANGLE RECT-10
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89 BY 2.58.

DEFINE RECTANGLE RECT-11
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 68 BY 2.75.

DEFINE RECTANGLE rt-button
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 116 BY 1.46
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-itens-controle FOR 
      item SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-itens-controle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-itens-controle w-livre _FREEFORM
  QUERY br-itens-controle NO-LOCK DISPLAY
      item.it-codigo FORMAT "x(16)":U
      item.desc-item FORMAT "x(60)":U
/*       item.tipo-contr FORMAT ">9":U WIDTH 30.57 */
      fcTipContr(item.tipo-contr) @ cContrItem FORMAT "X(20)" LABEL "Tipo de Controle"
      fcMsgAtu() @ cReturMsg FORMAT "X(200)" LABEL "Retorno Processamento"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS SIZE 115 BY 14.04
         TITLE "Itens Controle de Estoque" ROW-HEIGHT-CHARS .67 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-cad
     fi-ge-codigo-ini AT ROW 3.33 COL 24.43 COLON-ALIGNED HELP
          "Grupo de estoque a que pertence o item" WIDGET-ID 2
     fi-ge-codigo-fin AT ROW 3.33 COL 43.14 COLON-ALIGNED HELP
          "Grupo de estoque a que pertence o item" NO-LABEL WIDGET-ID 38
     fi-it-codigo-ini AT ROW 4.25 COL 10.29 COLON-ALIGNED HELP
          "C�digo do Item" WIDGET-ID 24
     fi-it-codigo-fin AT ROW 4.25 COL 43.14 COLON-ALIGNED HELP
          "C�digo do Item" NO-LABEL WIDGET-ID 22
     bt-filtro AT ROW 4.25 COL 62.72 HELP
          "Filtrar Itens Por Grupo de Estoque" WIDGET-ID 4
     rd-tipo-contr AT ROW 6.38 COL 4.29 NO-LABEL WIDGET-ID 6
     fi-conta-contabil AT ROW 7.17 COL 16.57 COLON-ALIGNED WIDGET-ID 30
     c-titulo AT ROW 7.17 COL 37.57 COLON-ALIGNED NO-LABEL WIDGET-ID 32
     bt-alterar AT ROW 7.17 COL 81 HELP
          "Alterar Tipo de Controle" WIDGET-ID 16
     br-itens-controle AT ROW 8.5 COL 2 WIDGET-ID 200
     "Alterar Tipo de Controle Para" VIEW-AS TEXT
          SIZE 28 BY .67 AT ROW 5.54 COL 4.29 WIDGET-ID 12
     "Filtrar" VIEW-AS TEXT
          SIZE 8 BY .67 AT ROW 2.5 COL 4 WIDGET-ID 20
     rt-button AT ROW 1 COL 1
     RECT-10 AT ROW 5.79 COL 2 WIDGET-ID 14
     RECT-11 AT ROW 2.75 COL 2 WIDGET-ID 18
     IMAGE-1 AT ROW 4.25 COL 29.43 WIDGET-ID 26
     IMAGE-2 AT ROW 4.25 COL 42.29 WIDGET-ID 28
     IMAGE-3 AT ROW 3.33 COL 29.43 WIDGET-ID 34
     IMAGE-4 AT ROW 3.33 COL 42.29 WIDGET-ID 36
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 158.14 BY 22.17 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: w-livre
   Allow: Basic,Browse,DB-Fields,Smart,Window,Query
   Container Links: 
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-livre ASSIGN
         HIDDEN             = YES
         TITLE              = "Template Livre <Insira complemento>"
         HEIGHT             = 21.83
         WIDTH              = 116.43
         MAX-HEIGHT         = 23.04
         MAX-WIDTH          = 160.29
         VIRTUAL-HEIGHT     = 23.04
         VIRTUAL-WIDTH      = 160.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU m-livre:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-livre 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-livre.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-livre
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-cad
   FRAME-NAME L-To-R                                                    */
/* BROWSE-TAB br-itens-controle bt-alterar f-cad */
/* SETTINGS FOR FILL-IN c-titulo IN FRAME f-cad
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-conta-contabil IN FRAME f-cad
   2 3                                                                  */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-livre)
THEN w-livre:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-itens-controle
/* Query rebuild information for BROWSE br-itens-controle
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH item
                           WHERE item.ge-codigo >= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-ini
                             AND item.ge-codigo <= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-fin
                             AND ITEM.it-codigo >= INPUT FRAME {&FRAME-NAME} fi-it-codigo-ini
                             AND ITEM.it-codigo <= INPUT FRAME {&FRAME-NAME} fi-it-codigo-fin NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Where[1]         = "mgcad.item.ge-codigo = fi-ge-codigo"
     _Query            is OPENED
*/  /* BROWSE br-itens-controle */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-livre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-livre w-livre
ON END-ERROR OF w-livre /* Template Livre <Insira complemento> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-livre w-livre
ON WINDOW-CLOSE OF w-livre /* Template Livre <Insira complemento> */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-alterar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-alterar w-livre
ON CHOOSE OF bt-alterar IN FRAME f-cad /* Alterar Tipo de Controle */
DO:

    RUN utp/ut-msgs.p(INPUT "show",
                      INPUT 27100,
                      INPUT "Atualizar do Tipo de Controle?" + "~~" +
                            "Deseja Alterar o Tipo de Controle dos Itens com o Grupo de Estoque Informado?").
  IF RETURN-VALUE = "YES" THEN
  DO:
     EMPTY TEMP-TABLE tt-item-control.

     RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
     RUN pi-inicializar IN h-acomp (INPUT "Processando, Aguarde...").

     FOR EACH ITEM 
        WHERE ITEM.ge-codigo >= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-ini
          AND ITEM.ge-codigo <= INPUT FRAME {&FRAME-NAME} fi-ge-codigo-fin
          AND ITEM.it-codigo >= INPUT FRAME {&FRAME-NAME} fi-it-codigo-ini
          AND ITEM.it-codigo <= INPUT FRAME {&FRAME-NAME} fi-it-codigo-fin:

        RUN pi-acompanhar IN h-acomp(INPUT "Item: " + ITEM.it-codigo).

        ASSIGN l-return-erro = NO.
        RUN pi-processar.
     END.
     run pi-finalizar in h-acomp.

     RUN utp/ut-msgs.p(INPUT "show",
                       INPUT 15825,
                       INPUT "Atualiza��o Conclu�da!" + "~~" + "A atualiza��o dos itens foi conclu�da com sucesso. Verifique no Browser se houve algum erro no processamento.").     
  END.
  {&OPEN-BROWSERS-IN-QUERY-f-cad}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-filtro
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-filtro w-livre
ON CHOOSE OF bt-filtro IN FRAME f-cad /* Filtrar */
DO:
    EMPTY TEMP-TABLE tt-item-control.

    {&OPEN-BROWSERS-IN-QUERY-f-cad}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-conta-contabil
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-conta-contabil w-livre
ON F5 OF fi-conta-contabil IN FRAME f-cad /* Conta Aplica��o */
DO:
  find first param-global no-lock no-error.
  assign i-ep-codigo-usuario = i-empresa.

  assign i-emp050 = i-empresa
                    cg-sis-z = "CE"
                    cg-tipo-z = "1,2,3,4,5".
  assign l-implanta = yes.                  
  {include/zoomvar.i &prog-zoom="adzoom/z01ad049.w"
                     &campo=fi-conta-contabil
                     &campozoom=conta-contabil}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-conta-contabil w-livre
ON LEAVE OF fi-conta-contabil IN FRAME f-cad /* Conta Aplica��o */
DO:
  {include/leave.i &tabela=conta-contab
                    &atributo-ref=titulo
                    &variavel-ref=c-titulo
                    &where="conta-contab.conta-contabil = input frame {&frame-name} fi-conta-contabil and
                            conta-contab.ep-codigo = i-empresa"}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-conta-contabil w-livre
ON MOUSE-SELECT-DBLCLICK OF fi-conta-contabil IN FRAME f-cad /* Conta Aplica��o */
DO:
  apply "f5" to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-consultas
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-consultas w-livre
ON CHOOSE OF MENU-ITEM mi-consultas /* Consultas */
DO:
  RUN pi-consulta IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-conteudo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-conteudo w-livre
ON CHOOSE OF MENU-ITEM mi-conteudo /* Conteudo */
OR HELP OF FRAME {&FRAME-NAME}
DO:
  RUN pi-ajuda IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-imprimir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-imprimir w-livre
ON CHOOSE OF MENU-ITEM mi-imprimir /* Relat�rios */
DO:
  RUN pi-imprimir IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-programa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-programa w-livre
ON MENU-DROP OF MENU mi-programa /* Nome-do-Programa */
DO:
  run pi-disable-menu.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sair w-livre
ON CHOOSE OF MENU-ITEM mi-sair /* Sair */
DO:
  RUN pi-sair IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sobre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sobre w-livre
ON CHOOSE OF MENU-ITEM mi-sobre /* Sobre... */
DO:
  {include/sobre.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-itens-controle
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-livre 


/* ***************************  Main Block  *************************** */
  if fi-conta-contabil:load-mouse-pointer     ("image/lupa.cur") in frame {&FRAME-NAME} then.
/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-livre  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'panel/p-exihel.w':U ,
             INPUT  FRAME f-cad:HANDLE ,
             INPUT  'Edge-Pixels = 2,
                     SmartPanelType = NAV-ICON,
                     Right-to-Left = First-On-Left':U ,
             OUTPUT h_p-exihel ).
       RUN set-position IN h_p-exihel ( 1.13 , 100.57 ) NO-ERROR.
       /* Size in UIB:  ( 1.25 , 16.00 ) */

       /* Links to SmartPanel h_p-exihel. */
       RUN add-link IN adm-broker-hdl ( h_p-exihel , 'State':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-exihel ,
             fi-ge-codigo-ini:HANDLE IN FRAME f-cad , 'BEFORE':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-livre  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-livre  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-livre)
  THEN DELETE WIDGET w-livre.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-livre  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fi-ge-codigo-ini fi-ge-codigo-fin fi-it-codigo-ini fi-it-codigo-fin 
          rd-tipo-contr fi-conta-contabil c-titulo 
      WITH FRAME f-cad IN WINDOW w-livre.
  ENABLE rt-button RECT-10 RECT-11 IMAGE-1 IMAGE-2 IMAGE-3 IMAGE-4 
         fi-ge-codigo-ini fi-ge-codigo-fin fi-it-codigo-ini fi-it-codigo-fin 
         bt-filtro rd-tipo-contr fi-conta-contabil bt-alterar br-itens-controle 
      WITH FRAME f-cad IN WINDOW w-livre.
  {&OPEN-BROWSERS-IN-QUERY-f-cad}
  VIEW w-livre.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy w-livre 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-livre 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  
  RETURN.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize w-livre 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  run pi-before-initialize.

  {include/win-size.i}

  {utp/ut9000.i "ESCE0108" "2.06.00.000"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  find first param-estoq NO-LOCK NO-ERROR.  
  if not avail param-estoq then 
  do:
     run utp/ut-msgs.p (input "show",
                        input 1059,
                        input "").
  
     apply "close" to this-procedure.
     return "ADM ERROR".
  end.
  &if defined (bf_mat_fech_estab) &then
   if param-estoq.tp-fech = 1 then
      if param-estoq.mensal-ate <> param-estoq.ult-fech-dia then do:
          run utp/ut-msgs.p (input "show":U, 
                             input 2052, 
                             input string(param-estoq.ult-fech-dia) + "~~" + 
                                   string(param-estoq.mensal-ate)).
          apply "close" to this-procedure.
          return "ADM ERROR".
      end.
  &else
  if param-estoq.mensal-ate <> param-estoq.ult-fech-dia then do:
      run utp/ut-msgs.p (input "show":U, 
                         input 2052, 
                         input string(param-estoq.ult-fech-dia) + "~~" + 
                               string(param-estoq.mensal-ate)).
      apply "close" to this-procedure.
      return "ADM ERROR".
  end.
  &endif

  run pi-after-initialize.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-error w-livre 
PROCEDURE pi-error :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEFINE INPUT PARAMETER cReturnValue AS CHARACTER NO-UNDO.

    FIND FIRST tt-item-control WHERE 
               tt-item-control.it-codigo  = ITEM.it-codigo NO-ERROR.
    IF NOT AVAIL tt-item-control THEN
    DO:
        CREATE tt-item-control.
        ASSIGN tt-item-control.it-codigo  = ITEM.it-codigo.               
    END.

    ASSIGN tt-item-control.msg-return = tt-item-control.msg-return + cReturnValue
           l-return-erro = YES.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-processar w-livre 
PROCEDURE pi-processar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE de-val-unit AS DECIMAL     NO-UNDO.

  find first param-global no-lock no-error.
  find first param-estoq no-lock no-error.
  /* ------------------------------------ */
  assign i-empresa = param-global.empresa-prin.
  
  &if defined (bf_dis_consiste_conta) &then
    find estabelec where
         estabelec.cod-estabel = param-estoq.estabel-pad no-lock no-error.
    run cdp/cd9970.p (input rowid(estabelec),
                      output i-empresa).
  &endif
  /* ------------------------------------ */
  find first item-estab where
       item-estab.it-codigo   = item.it-codigo and
       item-estab.cod-estabel = item.cod-estabel no-error.
  &if defined (bf_mat_fech_estab) &then
   if param-estoq.tp-fech = 2 then do:
      find estab-mat
           where estab-mat.cod-estabel = item-estab.cod-estabel
           no-lock no-error.
      if avail estab-mat then
         assign da-ult-per = estab-mat.ult-per-fech.
   end.
   else
      assign da-ult-per = param-estoq.ult-per-fech.
  &else
     assign da-ult-per = param-estoq.ult-per-fech.
  &endif
  /* ------------------------------------ */
  run cdp/cdapi005.p (input  da-ult-per     ,
                      output da-ini-periodo ,
                      output da-fim-periodo ,
                      output i-per-corrente ,
                      output i-ano-corrente ,
                      output da-iniper-fech ,
                      output da-fimper-fech ).
  /* ------------------------------------ */
  if today > da-fim-periodo then
  do:
     run utp/ut-msgs.p (input "Msg",
                        input 18116,
                        input "").

     RUN pi-error("18116 - " + RETURN-VALUE).

  end.

  IF l-return-erro THEN NEXT.

  /* ------------------------------------ */
  assign c-conta-contabil = INPUT FRAME {&FRAME-NAME} fi-conta-contabil.
  find first grup-estoq  where grup-estoq.ge-codigo = item.ge-codigo no-lock no-error.
  assign c-desc-grupo   =  if avail grup-estoq then grup-estoq.descricao else "".
  
  find first familia     where familia.fm-codigo    = item.fm-codigo no-lock no-error.
  assign c-desc-familia = if avail familia then familia.descricao else "".
  
  find first tab-unidade where tab-unidade.un = item.un no-lock no-error.
  assign c-desc-tab     = if avail tab-unidade then tab-unidade.descricao else "".
  
  assign c-depos-pad = item.deposito-pad.

  find first deposito where deposito.cod-depos = item.deposito-pad no-lock no-error.
  assign c-desc-dep = if avail deposito then deposito.nome else "".
  
  FIND LAST movto-estoq where movto-estoq.it-codigo   = item.it-codigo
                          AND movto-estoq.tipo-trans  = 1
                          and movto-estoq.esp-docto = 21 no-lock no-error.
  IF NOT AVAIL movto-estoq THEN
  DO:
     FIND LAST recebimento WHERE recebimento.it-codigo = item.it-codigo NO-LOCK NO-ERROR.
     IF AVAIL recebimento THEN
     DO:         
        run cdp/cd0812.p (input 0, 
                          input 0,
                          input  recebimento.preco-unit,
                          input  recebimento.data-nota,
                          output de-val-unit).        

         assign c-serie       = recebimento.serie-nota  
                i-docto       = recebimento.numero-nota 
                de-medio-mat1 = de-val-unit
                de-medio-mat2 = 0
                de-medio-mat3 = 0
                de-medio-mob1 = 0
                de-medio-mob2 = 0
                de-medio-mob3 = 0
                de-medio-ggf1 = 0
                de-medio-ggf2 = 0
                de-medio-ggf3 = 0.                                                          
     END.
  END.
  ELSE
  DO:  
      assign c-serie       = movto-estoq.serie-docto 
             i-docto       = movto-estoq.nro-docto   
             de-medio-mat1 = movto-estoq.valor-mat-m[1]
             de-medio-mat2 = movto-estoq.valor-mat-m[2]
             de-medio-mat3 = movto-estoq.valor-mat-m[3]
             de-medio-mob1 = movto-estoq.valor-mob-m[1]
             de-medio-mob2 = movto-estoq.valor-mob-m[2]
             de-medio-mob3 = movto-estoq.valor-mob-m[3]
             de-medio-ggf1 = movto-estoq.valor-ggf-m[1]
             de-medio-ggf2 = movto-estoq.valor-ggf-m[2]
             de-medio-ggf3 = movto-estoq.valor-ggf-m[3]
             de-medio-mat1 = if de-medio-mat1 < 0 then 0 else de-medio-mat1
             de-medio-mat2 = if de-medio-mat2 < 0 then 0 else de-medio-mat2
             de-medio-mat3 = if de-medio-mat3 < 0 then 0 else de-medio-mat3
             de-medio-mob1 = if de-medio-mob1 < 0 then 0 else de-medio-mob1
             de-medio-mob2 = if de-medio-mob2 < 0 then 0 else de-medio-mob2
             de-medio-mob3 = if de-medio-mob3 < 0 then 0 else de-medio-mob3
             de-medio-ggf1 = if de-medio-ggf1 < 0 then 0 else de-medio-ggf1
             de-medio-ggf2 = if de-medio-ggf2 < 0 then 0 else de-medio-ggf2
             de-medio-ggf3 = if de-medio-ggf3 < 0 then 0 else de-medio-ggf3.        
  END.
  /* ------------------------------------ */
  EMPTY TEMP-TABLE tt-param.
  
  find first tt-param no-error.
  if not avail tt-param then
     create tt-param.

  assign l-rejeita-dec         = no.
  assign tt-param.medio-mat[1] = fn_vld_ajust_dec(de-medio-mat1,0).
  assign tt-param.medio-mat[2] = fn_vld_ajust_dec(de-medio-mat2,param-estoq.moeda1).
  assign tt-param.medio-mat[3] = fn_vld_ajust_dec(de-medio-mat3,param-estoq.moeda2).
  assign tt-param.medio-mob[1] = fn_vld_ajust_dec(de-medio-mob1,0).
  assign tt-param.medio-mob[2] = fn_vld_ajust_dec(de-medio-mob2,param-estoq.moeda1).
  assign tt-param.medio-mob[3] = fn_vld_ajust_dec(de-medio-mob3,param-estoq.moeda2).
  assign tt-param.medio-ggf[1] = fn_vld_ajust_dec(de-medio-ggf1,0).
  assign tt-param.medio-ggf[2] = fn_vld_ajust_dec(de-medio-ggf2,param-estoq.moeda1).
  assign tt-param.medio-ggf[3] = fn_vld_ajust_dec(de-medio-ggf3,param-estoq.moeda2).
  assign tt-param.depos-pad    = c-depos-pad
         tt-param.serie1       = "ACT"
         tt-param.docto1       = STRING(TODAY,"99999999")
         tt-param.parametro    = fcTipContr(input frame {&frame-name} rd-tipo-contr).
                  
  if l-rejeita-dec then do:
     run utp/ut-msgs.p (input "Msg",
                        input 18100,
                        input "").   

     RUN pi-error("18100 - " + RETURN-VALUE).

  end.

  IF l-return-erro THEN NEXT.

  EMPTY TEMP-TABLE tt-item-2.

  create tt-item-2.
  buffer-copy item to tt-item-2.
  /* ------------------- ********** ------------------------ */
  {utp/ut-liter.i Troca_de_controle_cont�bil_n�o_permitida!_Item_de_contrato. mce R}
  ASSIGN v_msg_1 = RETURN-VALUE.

  FIND FIRST param-contrat NO-LOCK WHERE
             param-contrat.it-codigo = item.it-codigo NO-ERROR.
  IF AVAIL param-contrat THEN DO:
      RUN utp/ut-msgs.p (INPUT "Msg",
                         INPUT 17006,
                         INPUT v_msg_1).  

      RUN pi-error("170061 - " + RETURN-VALUE).

  END.

  IF l-return-erro THEN NEXT.

  FOR EACH item-contrat NO-LOCK WHERE
           item-contrat.it-codigo = item.it-codigo and 
           item-contrat.ind-sit-item <> 3 :
      RUN utp/ut-msgs.p (INPUT "Msg",
                         INPUT 17006,
                         INPUT v_msg_1).  

      RUN pi-error("170062 - " + RETURN-VALUE).
  END.

  IF l-return-erro THEN NEXT.

  find first param-global no-lock no-error.
  if not avail param-global then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 16, 
                       input return-value).

      RUN pi-error("16 - " + RETURN-VALUE).

  END.   

  IF l-return-erro THEN NEXT.

  find first param-estoq no-lock no-error.
  if not avail param-estoq then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 1059, 
                       input RETURN-VALUE).

    RUN pi-error("1059 - " + RETURN-VALUE).
  END.

  IF l-return-erro THEN NEXT.

  if item.it-codigo = "" and 
     item.tipo-contr =  4 then do:
      {utp/ut-liter.i D�bito_direto}
      run utp/ut-msgs.p (input "Msg", 
                       input 32401 , 
                       input item.it-codigo + "~~" + return-value).
      RUN pi-error("32401 - " + RETURN-VALUE).

  END.

  IF l-return-erro THEN NEXT.
  
  find param-fasb 
       where param-fasb.ep-codigo = i-empresa no-lock no-error .

  assign i-mo-fasb = if avail param-fasb and param-fasb.moeda-fasb <> 0 then 
                       if param-fasb.moeda-fasb = param-estoq.moeda1  then 2
                         else if param-fasb.moeda-fasb = param-estoq.moeda2 then 3
                              else 0
                     else 0
         i-mo-cmi  = if avail param-fasb and param-fasb.moeda-cmi <> 0 then
                       if param-fasb.moeda-cmi = param-estoq.moeda1 then 2
                         else if param-fasb.moeda-cmi = param-estoq.moeda2
                                then 3
                                else 0
                     else 0. 
  for each item-estab use-index item
      where item-estab.it-codigo = item.it-codigo no-lock:
    &if defined (bf_mat_fech_estab) &then
      if param-estoq.tp-fech = 2 then do:
         find estab-mat 
              where estab-mat.cod-estabel = item-estab.cod-estabel
              no-lock no-error.
         if avail estab-mat then
            assign da-ult-fech-dia = estab-mat.ult-fech-dia.
      end.
      else
         assign da-ult-fech-dia = param-estoq.ult-fech-dia.
    &else
        assign da-ult-fech-dia = param-estoq.ult-fech-dia.
    &endif

    find first movto-estoq use-index item-data 
         where movto-estoq.it-codigo = item.it-codigo  and
               movto-estoq.dt-trans  > da-ult-fech-dia and
               movto-estoq.esp-docto = 25  no-lock no-error.
    if avail movto-estoq then do:
      run utp/ut-msgs.p (input "Msg":U, 
                         input 3242, 
                         input return-value).

      RUN pi-error("3242 - " + RETURN-VALUE).

    END.
    
    IF l-return-erro THEN NEXT.

/*     if item-estab.sald-ini-mat-m[1] < 0 or */
/*        item-estab.sald-ini-mat-m[2] < 0 or */
/*        item-estab.sald-ini-mat-m[3] < 0 or */
/*        item-estab.sald-ini-mob-m[1] < 0 or */
/*        item-estab.sald-ini-mob-m[2] < 0 or */
/*        item-estab.sald-ini-mob-m[3] < 0 then do: */
/*       run utp/ut-msgs.p (input "Msg":U, */
/*                          input 3244, */
/*                           input return-value). */
/*    */
/*       RUN pi-error("3244 - " + RETURN-VALUE). */
/*    */
/*     END. */
/*    */
/*     IF l-return-erro THEN NEXT. */
  
  END.

/********************************************
  for each saldo-estoq use-index item
      where saldo-estoq.it-codigo = item.it-codigo no-lock:
    if saldo-estoq.qtidade-ini < 0 then do:
      run utp/ut-msgs.p (input "Msg":U, 
                         input 3244, 
                         input return-value).

      RUN pi-error("3244 - " + RETURN-VALUE).

    END.
   
    IF l-return-erro THEN NEXT.

  end.   
  ******************************/

  {utp/ut-liter.i Verificando_Notas_n�o_Confirmadas_no_Faturamento... mce R}
  status input return-value.

  /* Verifica Notas nao Confirmadas no Faturamento */
  if param-global.modulo-ft then do:
    assign l-nota-ft = no.

    for each it-nota-fisc where it-nota-fisc.it-codigo = item.it-codigo and
             it-nota-fisc.dt-confirma = ? no-lock :
      find nota-fiscal where nota-fiscal.cod-estabel = it-nota-fisc.cod-estabel and
                             nota-fiscal.serie       = it-nota-fisc.serie       and
                             nota-fiscal.nr-nota-fis = it-nota-fisc.nr-nota-fis
                             no-lock no-error.
      if avail nota-fiscal then 
        if nota-fiscal.dt-cancela <> ? then next.
      else do:
        assign l-nota-ft = yes.
        leave.
      end.
    end.
    if l-nota-ft = no then 
      for each pre-fatur where pre-fatur.cod-sit-pre <> 3 no-lock:
        for each it-pre-fat where it-pre-fat.nr-embarque = pre-fatur.nr-embarque and
                                  it-pre-fat.nome-abrev  = pre-fatur.nome-abrev  and
                                  it-pre-fat.nr-pedcli   = pre-fatur.nr-pedcli   and
                                  it-pre-fat.it-codigo   = item.it-codigo        no-lock:
          assign l-nota-ft = yes.
          leave.
        end.
      end.
    if l-nota-ft then do:
      run utp/ut-msgs.p (input "Msg":U, 
                         input 25902, 
                         input return-value).

      RUN pi-error("25902 - " + RETURN-VALUE).

    END.
   
    IF l-return-erro THEN NEXT.

  END.

  /* Se tiver alguma transacao NFT com valores zerados */
  find first movto-estoq  where movto-estoq.it-codigo      = item.it-codigo and
                                movto-estoq.dt-trans      >= da-inipa-x     and
                                movto-estoq.esp-docto      = 23             and
                                movto-estoq.valor-mat-m[1] = 0              and
                                movto-estoq.valor-mob-m[1] = 0              and
                                movto-estoq.valor-ggf-m[1] = 0 no-lock no-error.
  if avail movto-estoq then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 3261, 
                       input return-value).

    RUN pi-error("3261 - " + RETURN-VALUE).

  END.  
   
  IF l-return-erro THEN NEXT.

  find conta-contab where conta-contab.ep-codigo      = i-empresa and
                          conta-contab.conta-contabil = c-conta-contabil
                          no-lock no-error.
  if not avail conta-contab then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 177, 
                       input return-value).

    RUN pi-error("177 - " + RETURN-VALUE).

  END.
   
  IF l-return-erro THEN NEXT. 

  if conta-contab.tipo = 6 then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 286, 
                       input return-value).
      RUN pi-error("286 - " + RETURN-VALUE).

 
  end.    

  IF l-return-erro THEN NEXT.

  if conta-contab.estado <> 3 then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 443, 
                       input return-value).
      RUN pi-error("443 - " + RETURN-VALUE).
  end.  

  IF l-return-erro THEN NEXT.

  if conta-contab.estoque = 0 then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 445, 
                       input return-value).
      RUN pi-error("445 - " + RETURN-VALUE).
  end.

  IF l-return-erro THEN NEXT.

  if conta-contab.estoque = 9 then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 1784, 
                       input return-value).
      RUN pi-error("1784 - " + RETURN-VALUE).
  end.   

  IF l-return-erro THEN NEXT.

  assign tt-param.r-conta     = rowid(conta-contab)
         tt-param.it-codigo   = item.it-codigo.

  find deposito where deposito.cod-depos = c-depos-pad 
                      no-lock no-error.
  if not avail deposito then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 530, 
                       input return-value).
      RUN pi-error("530 - " + RETURN-VALUE).
  end.   

  IF l-return-erro THEN NEXT.

  if deposito.ind-dep-cq = yes and item.contr-qualid then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 1210, 
                       input return-value).
      RUN pi-error("1210 - " + RETURN-VALUE).
  end.   

  IF l-return-erro THEN NEXT.

  if deposito.ind-dep-rej = yes and item.contr-qualid then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 1217, 
                       input return-value).
      RUN pi-error("1217 - " + RETURN-VALUE).
  end.

  IF l-return-erro THEN NEXT.

  for each item-estab where item-estab.it-codigo = item.it-codigo no-lock:
    find estabelec where estabelec.cod-estabel = item-estab.cod-estabel no-lock no-error.
    if not avail estabelec then 
      next.

    if estabelec.deposito-cq = c-depos-pad and item.contr-qualid then do:
      run utp/ut-msgs.p (input "Msg":U, 
                         input 1210, 
                         input return-value).

      RUN pi-error("1210 - " + RETURN-VALUE).
    end.    

    IF l-return-erro THEN NEXT.

    if estabelec.dep-rej-cq = c-depos-pad and item.contr-qualid then 
    do:
      run utp/ut-msgs.p (input "Msg":U, 
                         input 1217, 
                         input return-value).

      RUN pi-error("1217 - " + RETURN-VALUE).
    END.

    IF l-return-erro THEN NEXT.

  END. 
  
  if (item.tipo-contr = 1 and 
     INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2) or
     (item.tipo-contr = 3 and 
     INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2) or  
     (item.tipo-contr = 4 and 
     INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2) then do:

    if item.tipo-contr = 3 and INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2  then 
    do:
      {utp/ut-liter.i Assumir�_o_valor_informado_como_m�dio,_caso_o_m�dio_calculado_for_igual_a_0 mce R}
      status input return-value.
    end.    

    /************************************************/
/*     if tt-param.medio-mat[1] = 0 and          */
/*        tt-param.medio-mob[1] = 0 and          */
/*        tt-param.medio-ggf[1] = 0 then do:     */
/*       run utp/ut-msgs.p (input "Msg":U,       */
/*                          input 3263,          */
/*                          input "").           */
/*                                               */
/*       RUN pi-error("3263 - " + RETURN-VALUE). */
/*                                               */
/*     end.                                      */

    IF l-return-erro THEN NEXT.

    if item.tipo-contr = 3 AND INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2  then
      status input " ".
  end.   
  
  if item.tipo-contr = 1 AND INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2  then 
    find first movto-estoq
         where movto-estoq.it-codigo = item.it-codigo  and
               movto-estoq.dt-trans  < da-ult-fech-dia and
               movto-estoq.esp-docto = 29 /*"RFS"*/ no-lock no-error.
  if item.tipo-contr = 4 and INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2 or
     item.tipo-contr = 4 and INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 1 then 
    find first movto-estoq
         where movto-estoq.it-codigo = item.it-codigo  and
               movto-estoq.dt-trans  < da-ult-fech-dia and
               movto-estoq.esp-docto = 27 /*"RDD"*/ no-lock no-error.
  if item.tipo-contr = 3 and INPUT FRAME {&FRAME-NAME} rd-tipo-contr = 2  then 
    find first movto-estoq
         where movto-estoq.it-codigo = item.it-codigo  and
               movto-estoq.dt-trans  < da-ult-fech-dia and
               movto-estoq.esp-docto = 26 /*"RCS"*/ no-lock no-error.
  if avail movto-estoq then do:
    run utp/ut-msgs.p (input "Msg":U, 
                       input 3264, 
                       input "").
    RUN pi-error("3264 - " + RETURN-VALUE).

  end.  
  
  IF l-return-erro THEN NEXT.

  if  session:set-wait-state("GENERAL") then .

  run esp/esce0108a.p (input table tt-param,
                       output table tt-erro).

  if not can-find(first tt-erro) and not l-return-erro then 
  do:
      
      ASSIGN tt-item-2.tipo-contr = INPUT FRAME {&FRAME-NAME} rd-tipo-contr
             tt-item-2.conta-aplicacao = if tt-item-2.tipo-contr = 2 then "" else INPUT FRAME {&FRAME-NAME} fi-conta-contabil.  
             
      buffer-copy tt-item-2 using tipo-contr TO ITEM.
      
      if tt-item-2.tipo-contr = 2 then
         assign item.conta-aplicacao = "".
         
         
      RUN pi-error("OK").
             
      
  end.
  else
  do:
    for each tt-erro:
        RUN pi-error(string(tt-erro.cd-erro) + " - " + tt-erro.mensagem).
    end.  
  end.    
        

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-livre  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "item"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-livre 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     Manuseia trocas de estado dos SmartObjects
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fcMsgAtu w-livre 
FUNCTION fcMsgAtu RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE cReturn AS CHARACTER   NO-UNDO.
  FIND FIRST tt-item-control WHERE tt-item-control.it-codigo = ITEM.it-codigo NO-LOCK NO-ERROR.
  IF AVAIL tt-item-control THEN
     ASSIGN cReturn = tt-item-control.msg-return.


  RETURN cReturn.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION fcTipContr w-livre 
FUNCTION fcTipContr RETURNS CHARACTER
  ( iTipControle AS INTEGER /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE cDescItemControle AS CHARACTER   NO-UNDO.

  ASSIGN cDescItemControle = {ininc/i09in122.i 04 iTipControle}.  

  RETURN cDescItemControle. 

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



