/*******************************************************************************
** Programa...: ESDC030RP
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..: Listagem de Duplicidade Movimento Estoque
*******************************************************************************/
/* Include de Controle de Vers�o */
/* n�o esque�a */
//define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESDC030RP.P 2.06.00.001}

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG

    FIELD c-estabel        AS CHAR EXTENT 2
    FIELD c-depos          AS CHAR EXTENT 2
    FIELD dt-trans         AS DATE EXTENT 2
    FIELD c-item           AS CHAR EXTENT 2
    FIELD l-detalhe        AS LOGICAL
    FIELD l-excel          AS LOGICAL.
    
DEFINE TEMP-TABLE tt-raw-digita
    FIELD raw-digita    AS RAW.

DEFINE TEMP-TABLE tt-erro
    FIELD mensagem AS CHAR .

DEFINE TEMP-TABLE tt-imp
    FIELD nr-trans LIKE movto-estoq.nr-ord-produ
    FIELD cod-estabel LIKE movto-estoq.cod-estabel
    FIELD cod-depos LIKE movto-estoq.cod-depos
    FIELD dt-trans LIKE movto-estoq.dt-trans
    FIELD it-codigo LIKE movto-estoq.it-codigo
    FIELD nro-docto LIKE movto-estoq.nro-docto
    FIELD tipo-trans LIKE movto-estoq.tipo-trans
    FIELD esp-docto LIKE movto-estoq.esp-docto
    FIELD quantidade LIKE movto-estoq.quantidade
    FIELD nr-ord-produ LIKE movto-estoq.nr-ord-produ
    
    FIELD log-est AS LOG.

DEFINE BUFFER b-imp FOR tt-imp.

DEF NEW GLOBAL SHARED VAR v_cod_usuar_corren AS CHAR NO-UNDO.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR h-acomp       AS HANDLE NO-UNDO.
DEFINE NEW GLOBAL SHARED VARIABLE c-seg-usuario AS CHARACTER NO-UNDO.
DEFINE VARIABLE h-handle AS HANDLE      NO-UNDO.
DEFINE VARIABLE iCount AS INTEGER     NO-UNDO.
DEFINE VARIABLE c-lista AS CHARACTER   NO-UNDO.
DEFINE VARIABLE c-tipo AS CHARACTER   NO-UNDO.
DEFINE VARIABLE c-arquivo AS CHARACTER   NO-UNDO.

DEF VAR cont-lin AS INT NO-UNDO.

/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}
DEFINE STREAM str-excel.

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST mgcad.empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ESDC030RP"
       c-versao       = "2.06"
       c-revisao      = "00.001"
       c-empresa      = empresa.nome
       c-sistema      = "ESP"
       c-titulo-relat = "Movimento Estoque Duplicado (Analise)".

ASSIGN c-arquivo = "".
IF tt-param.l-excel THEN DO:

    RUN pi-retorna-diretorio 
         (tt-param.usuario,
         OUTPUT c-arquivo).

    ASSIGN c-arquivo = c-arquivo + "esdc030" + string(ETIME) + ".csv".

    OUTPUT STREAM str-excel TO VALUE(c-arquivo).

    PUT STREAM str-excel UNFORMATT
        "Estab;Depos;Esp;Tipo;Data;Item;Descricao;RG/RACK/Doc;Qtd;Ordem"
        SKIP.
END.

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).

DEF BUFFER b-tt-imp FOR tt-imp.

FOR EACH movto-estoq NO-LOCK USE-INDEX data-item
    WHERE movto-estoq.dt-trans >= tt-param.dt-trans[1]
      AND movto-estoq.dt-trans <= tt-param.dt-trans[2]
      AND movto-estoq.it-codigo >= tt-param.c-item[1]
      AND movto-estoq.it-codigo <= tt-param.c-item[2]
      AND (movto-estoq.esp-docto = 1 OR movto-estoq.esp-docto = 8).


    IF    movto-estoq.cod-estabel < tt-param.c-estabel[1]
       OR movto-estoq.cod-estabel > tt-param.c-estabel[2]  THEN
        NEXT.

    IF    movto-estoq.cod-depos < tt-param.c-depos[1]
       OR movto-estoq.cod-depos > tt-param.c-depos[2] THEN
        NEXT.

    RUN pi-acompanhar IN h-acomp (INPUT STRING(movto-estoq.dt-trans,"99/99/9999")).

    
    CREATE tt-imp.
    ASSIGN tt-imp.nr-trans = movto-estoq.nr-trans
           tt-imp.cod-depos = movto-estoq.cod-depos
           tt-imp.cod-estabel = movto-estoq.cod-estabel
           tt-imp.dt-trans = movto-estoq.dt-trans
           tt-imp.esp-docto = movto-estoq.esp-docto
           tt-imp.it-codigo = movto-estoq.it-codigo
           tt-imp.nro-docto = movto-estoq.nro-docto
           tt-imp.quantidade = movto-estoq.quantidade
           tt-imp.tipo-trans = movto-estoq.tipo-trans
           tt-imp.nr-ord-produ = movto-estoq.nr-ord-produ
           tt-imp.log-est      = FALSE.                         
END.

FOR EACH tt-imp :

    FIND FIRST b-tt-imp NO-LOCK
         WHERE b-tt-imp.cod-estabel = tt-imp.cod-estabel
           AND b-tt-imp.cod-depos   = tt-imp.cod-depos
           AND b-tt-imp.esp-docto   = (IF tt-imp.esp-docto  = 1 THEN 8 ELSE 1)
           AND b-tt-imp.tipo-trans  = (IF tt-imp.tipo-trans = 1 THEN 2 ELSE 1)
           AND b-tt-imp.dt-trans    = tt-imp.dt-trans
           AND b-tt-imp.it-codigo   = tt-imp.it-codigo
           AND b-tt-imp.nro-docto   = tt-imp.nro-docto
           NO-ERROR.
    IF AVAIL b-tt-imp THEN DO:    
        DELETE b-tt-imp.
        DELETE tt-imp.
    END.

        /*ASSIGN b-tt-imp.log-est = TRUE
               tt-imp.log-est   = TRUE.                */
    

END.

c-lista = "ACA;ACT;NU1;DD;DEV;DIV;DRM;EAC;EGF;BEM;NU2;NU3;NU4;ICM;INV;IPL;MOB;NC;NF;NFD;NFE;NFT;NU5;REF;RCS;RDD;REQ;RFS;RM;RRQ;STR;TRA;ZZZ;SOB;EDD;VAR;ROP".
c-tipo = "ENTRADA;SAIDA".

DISP "Est Dep                   Transa��o  Item             Descri��o                                                    Documento                      Qtde" SKIP
     "--- ---                   ---------- ---------------- ------------------------------------------------------------ ---------------- ------------------" SKIP
    WITH WIDTH 332 NO-BOX WITH FRAME f1 STREAM-IO NO-LABEL.



FOR EACH tt-imp NO-LOCK
    WHERE NOT tt-imp.log-est,
    FIRST ITEM NO-LOCK
    WHERE item.it-codigo = tt-imp.it-codigo
    BREAK BY tt-imp.cod-estabel
          BY tt-imp.cod-depos
          BY tt-imp.dt-trans      
          /*BY tt-imp.esp-docto
          BY tt-imp.tipo-trans*/
          BY tt-imp.it-codigo
          BY tt-imp.nro-docto          
          .

    IF FIRST-OF(tt-imp.nro-docto) THEN DO:
        iCount = 0.
    END.                    


    /*IF tt-imp.esp-docto = 1 THEN*/
        iCount = iCount + 1.

    /*IF tt-imp.esp-docto = 8 THEN
        iCount = iCount - 1.*/

    IF tt-param.l-detalhe THEN DO:
        DISP tt-imp.cod-estabel
             tt-imp.cod-depos
             entry(int(tt-imp.esp-docto),c-lista,";")
             entry(int(tt-imp.tipo-trans),c-tipo,";")
             tt-imp.dt-trans
             tt-imp.it-codigo
             item.desc-item
             tt-imp.nro-docto
             tt-imp.quantidade
             tt-imp.nr-ord-produ
            WITH WIDTH 332 NO-BOX WITH FRAME f1  NO-LABEL.
        DOWN WITH FRAME f1.
        IF tt-param.l-excel THEN
            RUN pi-excel.
    END.

    IF LAST-OF(tt-imp.nro-docto) THEN DO:
                                  
        IF iCount > 1 THEN DO:
            IF tt-param.l-detalhe THEN DO:
                DISP "Duplicidade: "
                     tt-imp.cod-estabel
                     tt-imp.cod-depos
                     entry(int(tt-imp.esp-docto),c-lista,";")
                     entry(int(tt-imp.tipo-trans),c-tipo,";")
                     tt-imp.dt-trans
                     tt-imp.it-codigo
                     item.desc-item
                     tt-imp.nro-docto
                     tt-imp.quantidade
                     tt-imp.nr-ord-produ
                    WITH WIDTH 332 NO-BOX WITH FRAME f1 NO-LABEL.
                DOWN WITH FRAME f1.
                IF tt-param.l-excel THEN
                    RUN pi-excel.
            END.
            ELSE DO:
                
                FOR EACH b-imp NO-LOCK
                    WHERE b-imp.cod-estabel = tt-imp.cod-estabel
                      AND b-imp.cod-depos = tt-imp.cod-depos
                      AND b-imp.esp-docto = tt-imp.esp-docto
                      AND b-imp.tipo-trans = tt-imp.tipo-trans
                      AND b-imp.dt-trans = tt-imp.dt-trans
                      AND b-imp.it-codigo = tt-imp.it-codigo
                      AND b-imp.nro-docto = tt-imp.nro-docto.

                    DISP b-imp.cod-estabel
                         b-imp.cod-depos
                         entry(int(b-imp.esp-docto),c-lista,";")
                         entry(int(b-imp.tipo-trans),c-tipo,";")
                         b-imp.dt-trans
                         b-imp.it-codigo
                         item.desc-item
                         b-imp.nro-docto
                         b-imp.quantidade
                         b-imp.nr-ord-produ
                        WITH WIDTH 332 NO-BOX.
                    IF tt-param.l-excel THEN
                        RUN pi-excel2.
                END.
            END.
        END.
    END.
END.

RUN pi-finalizar IN h-acomp.

DEFINE VARIABLE ch-excel  AS office.iface.excel.ExcelWrapper NO-UNDO.
DEFINE VARIABLE ch-book    AS office.iface.excel.Workbook NO-UNDO.
DEFINE VARIABLE ch-sheet       AS office.iface.excel.WorkSheet NO-UNDO.

IF tt-param.l-excel THEN DO:       

    OUTPUT STREAM str-excel CLOSE.
    /*OS-COMMAND SILENT START VALUE(c-arquivo).*/

    {office/office.i excel ch-excel}
    ch-book  = ch-excel:workbooks:OPEN(c-arquivo).

    ch-sheet = ch-book:worksheets(1).

    ch-sheet:range("A1:AZ1"):font:bold = YES.
    ch-sheet:columns("H:H"):NumberFormat = "0000000000000".
    ch-sheet:columns("J:J"):NumberFormat = "000,.000".
    ch-sheet:columns("A:AZ"):AutoFit().

    ch-excel:visible = true.

    DELETE object ch-book.
    DELETE object ch-excel.
    DELETE object ch-sheet.
END.

RETURN "OK":U.


PROCEDURE pi-excel.
    
    PUT STREAM str-excel UNFORMAT
        tt-imp.cod-estabel ";"
        tt-imp.cod-depos ";"
        entry(int(tt-imp.esp-docto),c-lista,";") ";"
        entry(int(tt-imp.tipo-trans),c-tipo,";") ";"
        tt-imp.dt-trans ";"
        tt-imp.it-codigo ";"
        item.desc-item ";"
        tt-imp.nro-docto ";"
        tt-imp.quantidade ";"
        tt-imp.nr-ord-produ ";"
        SKIP.

END PROCEDURE.

PROCEDURE pi-excel2.
    
    PUT STREAM str-excel UNFORMAT
        b-imp.cod-estabel ";"
        b-imp.cod-depos ";"
        entry(int(b-imp.esp-docto),c-lista,";") ";"
        entry(int(b-imp.tipo-trans),c-tipo,";") ";"
        b-imp.dt-trans ";"
        b-imp.it-codigo ";"
        item.desc-item ";"
        b-imp.nro-docto ";"
        b-imp.quantidade ";"
        b-imp.nr-ord-produ ";"
        SKIP.

END PROCEDURE.

{utils/retorna_diretorio_usuario.i}
