{utp/ut-glob.i}
{include/i-rpvar.i}                       

/* *** DEFINICOES DE PREPROCESSORS *** */

/* *** DEFINICOES DE VARIAVEIS GLOBAIS *** */

/* *** DEFINICOES DE VARIAVEIS LOCAIS *** */
DEFINE VARIABLE h-acomp AS HANDLE NO-UNDO.

def stream s-texto.

def var Plano                as int format ">>>>>9"                                      no-undo.
def var i-max-indice         as integer                                                  no-undo.
def var i-ind                as integer                                                  no-undo.
def var de-saldo             like pl-it-prod.quantidade format "->>>,>>9.9"              no-undo.
def var de-qtde              as decimal extent 1000                                      no-undo.
def var i-num-calc-plano     like it-periodo.num-calc-plano                              no-undo.
def var i-plano-inf          like it-periodo.num-calc-plano                              no-undo.


/* *** DEFINICOES DE BUFFERS *** */

/* *** DEFINICOES DE TEMP-TABLES *** */

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD plano            AS INTEGER
    FIELD wdepos           AS CHAR FORMAT "x(03)"
    FIELD westab           AS CHAR FORMAT "x(03)".

def temp-table tt-estoq                                                                                     no-undo
    field num-calc-plano                        like it-periodo.num-calc-plano
    field it-codigo                             like item.it-codigo
    field ano                                   like it-periodo.ano
    field mes                                   as integer
    field qtde                                  as decimal format ">>,>>9.9"
    field ge-codigo                             like item.ge-codigo
    index ch-prim is primary unique num-calc-plano it-codigo ano mes
    index ch-impr ge-codigo it-codigo ano mes.

def temp-table w-periodo                                                                                    no-undo
    field ano                                   as integer format "9999"
    field mes                                   as integer format "99"
    field indice                                as integer
    index chave is primary unique               ano mes.


DEFINE TEMP-TABLE tt-digita FIELD modelo AS CHARACTER.

DEFINE TEMP-TABLE tt-raw-digita FIELD raw-digita AS RAW.

/* *** DEFINICOES DE FORMS *** */

 
/* *** DEFINICAO DE PARAMETROS *** */

DEFINE INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEFINE INPUT PARAMETER TABLE FOR tt-raw-digita.

BLOCO_PRINCIPAL:
DO :
 
   
   CREATE tt-param. 
   RAW-TRANSFER raw-param TO tt-param.
   FIND FIRST tt-param NO-LOCK NO-ERROR.

   FOR EACH tt-raw-digita:
      CREATE tt-digita.
      RAW-TRANSFER raw-digita TO tt-digita.
   END.

   RUN pi_inicializacao.
     
   assign i-plano-inf      = tt-param.plano
          plano            = tt-param.plano * 1000
          i-max-indice     = 0.

   for each tt-estoq:
       delete tt-estoq.
   end.

   for each w-periodo:
       delete w-periodo.
   end.


   /*{include/i-rpout.i &tofile = tt-param.arquivo}*/
   
   for each it-periodo no-lock where
            it-periodo.num-calc-plano      >= plano + 1
       and  it-periodo.num-calc-plano      <= plano + 999:

       find tt-estoq where
            tt-estoq.num-calc-plano        = it-periodo.num-calc-plano 
       and  tt-estoq.it-codigo             = it-periodo.it-codigo 
       and  tt-estoq.ano                   = it-periodo.ano 
       and  tt-estoq.mes                   = month(it-periodo.data) no-error.

       RUN pi-acompanhar IN h-acomp ("Aguarde...").

       if not avail tt-estoq then
       do:
          find item of it-periodo no-lock no-error.
          create tt-estoq.
          assign 
              tt-estoq.num-calc-plano      = it-periodo.num-calc-plano
              tt-estoq.ano                 = it-periodo.ano
              tt-estoq.mes                 = month(it-periodo.data)
              tt-estoq.it-codigo           = it-periodo.it-codigo
              tt-estoq.ge-codigo           = item.ge-codigo.
       end.

       assign 
           tt-estoq.qtde    = tt-estoq.qtde + it-periodo.qt-res-plan
           i-num-calc-plano = if   i-num-calc-plano < it-periodo.num-calc-plano 
                              then it-periodo.num-calc-plano 
                              else i-num-calc-plano.

       find w-periodo where 
            w-periodo.ano                     = it-periodo.ano
       and  w-periodo.mes                     = month(it-periodo.data) no-error.

       if  not avail w-periodo then do:
           create w-periodo.
           assign w-periodo.ano               = it-periodo.ano
                  w-periodo.mes               = month(it-periodo.data).
       end.

   end.

   output stream s-texto to value(tt-param.arquivo).
   PUT STREAM s-texto "SEEBER FASTPLAS LTDA."  
                      TODAY FORMAT "99/99/9999" "   " string(TIME,"HH:MM:SS")  " hs"
                      SKIP "*** RELATORIO DE NECESSIDADE BRUTA / PLANO No. "  tt-param.plano "***"
                      SKIP(1).
   PUT STREAM s-texto UNFORMATTED
       "Codigo"        AT 01         "#" 
       "Descricao"              "#"
       "Un"                     "#"
       "Grupo Estoque"          "#"
       "Deposito"               "#"
       "Saldo Estoque"          .

   for each w-periodo by w-periodo.ano by w-periodo.mes:
       assign i-max-indice     = i-max-indice + 1
              w-periodo.indice = i-max-indice.
       put stream s-texto unformatted
            "#" (string(w-periodo.mes, "99") + "/" + string(w-periodo.ano, "9999")).

   end.

   put stream s-texto skip.

   for each tt-estoq where tt-estoq.num-calc-plano = i-num-calc-plano
       break by tt-estoq.ge-codigo 
             by tt-estoq.it-codigo
             by tt-estoq.ano
             by tt-estoq.mes:

       if first-of(tt-estoq.it-codigo) then do:

           ASSIGN de-saldo = 0
                  de-qtde  = 0.

           find item no-lock where item.it-codigo = tt-estoq.it-codigo no-error.
           /* conf. sol. Jos� Luiz em 01/02/11 - incluido deposito e estabelecimento no parametro */
          /* FOR EACH saldo-estoq WHERE
                    saldo-estoq.it-codigo = tt-estoq.it-codigo AND
                    saldo-estoq.cod-depos = item.deposito-pad no-lock: */
               FOR EACH saldo-estoq WHERE
                                 saldo-estoq.it-codigo = tt-estoq.it-codigo AND
                                 saldo-estoq.cod-depos = tt-param.wdepos AND
                                 saldo-estoq.cod-estabel = tt-param.westab no-lock: 

               ASSIGN de-saldo = de-saldo + (saldo-estoq.qtidade-atu - saldo-estoq.qt-alocada).

           END.


           put stream s-texto UNFORMATTED  
               tt-estoq.it-codigo  AT 01    "#"
               item.descricao-1             "#"
               item.un                      "#"
               tt-estoq.ge-codigo           "#"
               item.deposito-pad            "#"
               de-saldo                    .

       end.

       find w-periodo where 
            w-periodo.ano      = tt-estoq.ano
       and  w-periodo.mes      = tt-estoq.mes no-error.

       assign de-qtde [w-periodo.indice] = de-qtde [w-periodo.indice] + tt-estoq.qtde.

       if  last-of(tt-estoq.it-codigo) then do:
           do  i-ind = 1 to i-max-indice:
               put stream s-texto unformatted "#" de-qtde [i-ind].
           end.
           put stream s-texto skip.
       end.

   end.    

   output stream s-texto close.
   
   assign plano = i-plano-inf.

   RUN pi-finalizar IN h-acomp.
END.

PROCEDURE pi_inicializacao:

   RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
   RUN pi-inicializar IN h-acomp (INPUT "Impress�o").
   RUN pi-seta-tipo   IN h-acomp (INPUT 6).

END PROCEDURE.

  
