&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{utp/ut-glob.i}                    
                    
/* Parameters Definitions ---                                           */

DEF INPUT PARAM c-programa AS CHAR NO-UNDO.                                      
                                      
/* Local Variable Definitions ---                                       */

DEF VAR c-impressoras AS CHAR NO-UNDO.

DEF TEMP-TABLE dc-impressoras
    FIELD cod-impressora AS INTE FORMAT "999"
    FIELD descricao      AS CHAR FORMAT "x(40)"
    FIELD dispositivo    AS CHAR FORMAT "x(40)".

DEF TEMP-TABLE dc-prog-impres
    FIELD cod-programa   AS CHAR FORMAT "x(20)"
    FIELD cod-usuario    AS CHAR FORMAT "x(20)"
    FIELD cod-impressora AS INTE FORMAT "999".

CREATE dc-impressoras.
ASSIGN dc-impressoras.cod-impressora = 1
       dc-impressoras.descricao      = 'Impressora Zebra Sala de Reuniao'
       dc-impressoras.dispositivo    = 'Lpt1:'.

CREATE dc-impressoras.
ASSIGN dc-impressoras.cod-impressora = 1
       dc-impressoras.descricao      = 'Impressora Zebra Sala de Reuniao'
       dc-impressoras.dispositivo    = 'COM1:'.

CREATE dc-impressoras.
ASSIGN dc-impressoras.cod-impressora = 1
       dc-impressoras.descricao      = 'Impressora Zebra Expedi��o'
       dc-impressoras.dispositivo    = '\\fastsrv6\ZebraZ6M'.


CREATE dc-impressoras.
ASSIGN dc-impressoras.cod-impressora = 1
       dc-impressoras.descricao      = 'Impressora Zebra Expedi��o 2'
       dc-impressoras.dispositivo    = '\\fastsrv17\ExpZebraZ6M'.

CREATE dc-prog-impres.
ASSIGN dc-prog-impres.cod-programa   = 'cp0301'
       dc-prog-impres.cod-usuario    = 'super'
       dc-prog-impres.cod-impressora = 1.

CREATE dc-prog-impres.
ASSIGN dc-prog-impres.cod-programa   = 'cp0301'
       dc-prog-impres.cod-usuario    = 'datasul'
       dc-prog-impres.cod-impressora = 1.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-11 RECT-12 cb-impressoras Btn_OK ~
Btn_Cancel Btn_Imp 
&Scoped-Define DISPLAYED-OBJECTS c-usu c-prg cb-impressoras FILL-IN-1 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_Imp AUTO-END-KEY 
     LABEL "Cancel" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE cb-impressoras AS CHARACTER FORMAT "X(256)":U 
     LABEL "Impressora" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 44 BY 1
     FONT 1 NO-UNDO.

DEFINE VARIABLE c-prg AS CHARACTER FORMAT "X(256)":U 
     LABEL "Programa" 
     VIEW-AS FILL-IN 
     SIZE 23 BY .81
     FONT 1 NO-UNDO.

DEFINE VARIABLE c-usu AS CHARACTER FORMAT "X(256)":U 
     LABEL "Usu�rio" 
     VIEW-AS FILL-IN 
     SIZE 23 BY .81
     FONT 1 NO-UNDO.

DEFINE VARIABLE FILL-IN-1 AS CHARACTER FORMAT "X(256)":U INITIAL "ETQ001 iniciados com Zebra... aparecer�o aqui" 
     VIEW-AS FILL-IN 
     SIZE 66 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-11
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 67 BY 3.24.

DEFINE RECTANGLE RECT-12
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 67 BY 1.52.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     c-usu AT ROW 1.24 COL 12 COLON-ALIGNED
     c-prg AT ROW 2.14 COL 12 COLON-ALIGNED
     cb-impressoras AT ROW 3.05 COL 12 COLON-ALIGNED
     Btn_OK AT ROW 4.43 COL 1.6
     Btn_Cancel AT ROW 4.43 COL 16.8
     Btn_Imp AT ROW 4.43 COL 52 WIDGET-ID 2
     FILL-IN-1 AT ROW 5.76 COL 2 NO-LABEL WIDGET-ID 4
     RECT-11 AT ROW 1 COL 1
     RECT-12 AT ROW 4.24 COL 1
     SPACE(1.19) SKIP(1.23)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Escolha a Impressora"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN c-prg IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-usu IN FRAME Dialog-Frame
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN FILL-IN-1 IN FRAME Dialog-Frame
   NO-ENABLE ALIGN-L                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* Escolha a Impressora */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Cancel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Cancel Dialog-Frame
ON CHOOSE OF Btn_Cancel IN FRAME Dialog-Frame /* Cancel */
DO:
  RETURN 'nok'.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Imp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Imp Dialog-Frame
ON CHOOSE OF Btn_Imp IN FRAME Dialog-Frame /* Cancel */
DO:
   RUN esp\etq001.w.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* OK */
DO:
  RETURN cb-impressoras:SCREEN-VALUE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.

ASSIGN c-impressoras = ''.
/* FOR EACH dc-impressoras NO-LOCK: */
/*     IF  c-impressoras = '' THEN */
/*         ASSIGN c-impressoras = dc-impressoras.dispositivo */
/*                cb-impressoras:SCREEN-VALUE = c-impressoras. */
/*     ELSE */
/*         ASSIGN c-impressoras = c-impressoras + "," + dc-impressoras.dispositivo. */
/* END. */

FOR EACH es-etq-impressora NO-LOCK where es-etq-impressora.cod-impressora begins 'ZEBRA'
                                     and es-etq-impressora.ind-ativa:
    IF  c-impressoras = '' THEN
        ASSIGN c-impressoras = es-etq-impressora.end-impressora
               cb-impressoras:SCREEN-VALUE = c-impressoras.
    ELSE
        ASSIGN c-impressoras = c-impressoras + "," + es-etq-impressora.end-impressora.
END.

FIND FIRST dc-prog-impres NO-LOCK
    WHERE dc-prog-impres.cod-usuario  = c-seg-usuario
      AND dc-prog-impres.cod-programa = c-programa
    NO-ERROR.

IF  AVAIL dc-prog-impres THEN
    FIND FIRST dc-impressoras NO-LOCK
        WHERE dc-impressoras.cod-impressora = dc-prog-impres.cod-impressora
        NO-ERROR.

IF  AVAIL dc-prog-impres AND
    AVAIL dc-prog-impres THEN
    ASSIGN cb-impressoras:LIST-ITEMS = dc-impressoras.dispositivo.
ELSE
    ASSIGN cb-impressoras:LIST-ITEMS = c-impressoras.

ASSIGN c-usu = c-seg-usuario.
       c-prg = c-programa.
            
/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY c-usu c-prg cb-impressoras FILL-IN-1 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-11 RECT-12 cb-impressoras Btn_OK Btn_Cancel Btn_Imp 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

