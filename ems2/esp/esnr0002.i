/********************************************************************************
**  PROGRAMA: ESETQ1001rp
**  DATA....: 30 de julho de 2012
**  OBJETIVO: Relatório de Etiquetas 
**  AUTOR...: Fabricio Degaspari - Totvs Triah
******************************************************************************/
DEFINE TEMP-TABLE tt-param NO-UNDO
    FIELDS destino          AS   INTEGER
    FIELDS arquivo          AS   CHARACTER FORMAT "x(35)"
    FIELDS usuario          AS   CHARACTER FORMAT "x(12)"
    FIELDS data-exec        AS   DATE
    FIELDS hora-exec        AS   INTEGER
    FIELDS processa         AS   INTEGER
    FIELDS executa          AS   INTEGER
    FIELDS classifica       AS   INTEGER
    FIELDS cod-estabel-ini  LIKE estabelec.cod-estabel
    FIELDS cod-estabel-fim  LIKE estabelec.cod-estabel
    FIELDS it-codigo-ini    LIKE ITEM.it-codigo
    FIELDS it-codigo-fim    LIKE ITEM.it-codigo
    FIELDS dt-trans-ini     LIKE nota-fiscal.dt-emis-nota
    FIELDS dt-trans-fim     LIKE nota-fiscal.dt-emis-nota
    FIELDS cod-emitente-ini LIKE emitente.cod-emitente
    FIELDS cod-emitente-fim LIKE emitente.cod-emitente

    FIELDS nr-tabpre        AS   CHARACTER
    FIELDS nat-operacao     AS   CHARACTER
    FIELDS serie            AS   CHARACTER
    FIELDS ind-devolucao    AS   LOGICAL
    FIELDS ind-pecas        AS   LOGICAL
    FIELDS ind-gerar-nota   AS   LOGICAL
.

DEFINE TEMP-TABLE tt-digita NO-UNDO
    FIELDS seq-layout       AS INTEGER
    FIELDS cod-tipo-dado    AS CHARACTER
    FIELDS titulo-dado      AS CHARACTER
    FIELDS valor-dado       AS CHARACTER
.


DEFINE BUFFER b-tt-digita FOR tt-digita.

/* Transfer Definitions */
DEFINE TEMP-TABLE tt-raw-digita NO-UNDO
   FIELDS raw-digita     AS RAW
.
