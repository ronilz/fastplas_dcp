DEFINE TEMP-TABLE tt-param NO-UNDO
    FIELD destino               AS INTEGER
    FIELD arquivo               AS CHAR FORMAT "x(35)"
    FIELD usuario               AS CHAR FORMAT "x(12)"
    FIELD data-exec             AS DATE
    FIELD hora-exec             AS INTEGER
    FIELD cod-estabel-ini       LIKE nota-fiscal.cod-estabel
    FIELD cod-estabel-fim       LIKE nota-fiscal.cod-estabel
    FIELD serie-ini             LIKE nota-fiscal.serie
    FIELD serie-fim             LIKE nota-fiscal.serie
    FIELD nr-nota-fis-ini       LIKE nota-fiscal.nr-nota-fis
    FIELD nr-nota-fis-fim       LIKE nota-fiscal.nr-nota-fis
    FIELD dat-emis-ini          LIKE nota-fiscal.dt-emis-nota
    FIELD dat-emis-fim          LIKE nota-fiscal.dt-emis-nota
    FIELD c-arq-txt             AS CHARACTER
    FIELD c-destino             AS CHARACTER
    FIELD l-excel               AS LOGICAL
.
               
DEFINE TEMP-TABLE tt-digita NO-UNDO
    FIELD cod-repres        LIKE repres.cod-rep
    INDEX id IS PRIMARY 
            cod-repres 
.

DEFINE TEMP-TABLE tt-raw-digita
   FIELD raw-digita        AS RAW.
