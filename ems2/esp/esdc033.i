
/*------------------------------------------------------------------------
    File        : esdc033.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : TIB
    Created     : Wed Jul 27 12:49:43 BRT 2022
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE TEMP-TABLE tt-param NO-UNDO
    FIELD destino          AS INTEGER
    FIELD arquivo          AS CHARACTER FORMAT "x(35)"
    FIELD usuario          AS CHARACTER FORMAT "x(12)"
    FIELD data-exec        AS DATE
    FIELD hora-exec        AS INTEGER
    FIELD classifica       AS INTEGER
    FIELD desc-classifica  AS CHARACTER FORMAT "x(40)"
    FIELD modelo-rtf       AS CHARACTER FORMAT "x(35)"
    FIELD l-habilitaRtf    AS LOG
    FIELD cod-estab        AS CHARACTER EXTENT 2
    FIELD cod-depos        AS CHARACTER EXTENT 2
    FIELD it-codigo        AS CHARACTER EXTENT 2
    FIELD suspende         AS LOGICAL
    FIELD cod-pto-controle AS CHARACTER FORMAT "x(5)".

DEFINE TEMP-TABLE tt-digita NO-UNDO
    FIELD ordem   AS INTEGER   FORMAT ">>>>9"
    FIELD exemplo AS CHARACTER FORMAT "x(30)"
    INDEX id ordem.


/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
