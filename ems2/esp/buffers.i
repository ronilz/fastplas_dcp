/*------------------------------------------------------------------------------

    File        : buffers.i

    Purpose     : Defini��o de buffers para programas do 2.04 conectados no 5.05

    Author(s)   : BPM

    Created     : Dez/2005

------------------------------------------------------------------------------*/

 

DEFINE BUFFER prog_dtsul             FOR prog_dtsul.

DEFINE BUFFER empresa                FOR mgcad.empresa.

DEFINE BUFFER usuar_mestre           FOR usuar_mestre.

DEFINE BUFFER procedimento           FOR procedimento.

DEFINE BUFFER modul_dtsul            FOR modul_dtsul.

DEFINE BUFFER usuar_grp_usuar        FOR usuar_grp_usuar.

DEFINE BUFFER cidade                 FOR mgcad.cidade.

DEFINE BUFFER pais                   FOR mgcad.pais.

DEFINE BUFFER localizacao            FOR mgcad.localizacao.

DEFINE BUFFER layout_impres_padr     FOR layout_impres_padr.

DEFINE BUFFER configur_layout_impres FOR configur_layout_impres.

DEFINE BUFFER configur_tip_imprsor   FOR configur_tip_imprsor.

DEFINE BUFFER ped_exec               FOR ped_exec.

DEFINE BUFFER imprsor_usuar          FOR imprsor_usuar.

DEFINE BUFFER layout_impres          FOR layout_impres.

DEFINE BUFFER impressora             FOR impressora.

DEFINE BUFFER tip_imprsor            FOR tip_imprsor.

DEFINE BUFFER servid_exec_imprsor    FOR servid_exec_imprsor.

