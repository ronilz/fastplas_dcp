&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
/* Procedure Description
"Structured Procedure File Template.

Use this template to create a new Structured Procedure file to compile and run PROGRESS 4GL code. You edit structured procedure files using the AB's Section Editor."
*/
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : ativar-item.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : ronil
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .p file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
DEFINE NEW GLOBAL SHARED VAR v_cod_usuar_corren as CHAR NO-UNDO.
DEFINE VARIABLE c-dc-rg-item-ini AS CHARACTER  NO-UNDO.
DEFINE VARIABLE c-dc-rg-item-fim AS CHARACTER  NO-UNDO.

DEFINE BUFFER b-dc-movto-pto-controle FOR dc-movto-pto-controle.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

UPDATE c-dc-rg-item-ini FORMAT "x(13)" SKIP       c-dc-rg-item-fim FORMAT "x(13)"
    WITH CENTERED SIDE-LABEL.

OUTPUT to VALUE(SESSION:TEMP-DIRECTORY + "ativ.txt").
RUN executarAtivacao (INPUT c-dc-rg-item-ini, INPUT c-dc-rg-item-fim).
OUTPUT CLOSE.
DOS SILENT START notepad.exe VALUE(SESSION:TEMP-DIRECTORY + "ativ.txt").
    
/* **********************  Internal Procedures  *********************** */


&IF DEFINED(EXCLUDE-gerarErro) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gerarErro Procedure
PROCEDURE gerarErro :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    /* vars */
    
    /* param */
    DEFINE INPUT PARAM p-erro as CHAR NO-UNDO.
    
    /* main */
    PUT p-erro SKIP.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF



&IF DEFINED(EXCLUDE-gerarSucesso) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE gerarSucesso Procedure
PROCEDURE gerarSucesso :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    PUT "RG Item Ativado com sucesso: " dc-rg-item.rg-item SKIP.
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF



&IF DEFINED(EXCLUDE-executarAtivacao) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE executarAtivacao Procedure
PROCEDURE executarAtivacao :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    /* vars */
    DEFINE VARIABLE i-seq AS INTEGER    NO-UNDO.
    
    /* param */
    DEFINE INPUT PARAM p-rg-ini as CHAR NO-UNDO.
    DEFINE INPUT PARAM p-rg-fim as CHAR NO-UNDO.
    
    /* main */
    FOR EACH dc-rg-item
        WHERE dc-rg-item.rg-item >= p-rg-ini
          AND dc-rg-item.rg-item <= p-rg-fim.
          
        if CAN-FIND (FIRST dc-rack-itens
                     WHERE dc-rack-itens.rg-item = dc-rg-item.rg-item) THEN do:
            RUN gerarErro("RG Item se encontra Montado no rack:" + dc-rack-itens.nr-rack).                         
            NEXT.                
        END.                         
        
        if dc-rg-item.situacao <> 4 THEN do:
            RUN gerarErro ("Item n�o est� cancelado: " + dc-rg-item.rg-item).
            NEXT.
        END.
        
        FIND dc-pto-controle
            WHERE dc-pto-controle.cod-pto-controle = dc-rg-item.cod-pto-controle
            NO-LOCK NO-ERROR.
        if NOT AVAIL dc-pto-controle THEN do:
            RUN gerarErro ("Nao foi poss�vel encontrar o ponto de controle: " + dc-rg-item.cod-pto-controle).
            NEXT.
        END.                        
        
        ASSIGN dc-rg-item.situacao = 2
               dc-rg-item.cod-pto-controle = dc-pto-controle.cod-pto-controle
               dc-rg-item.tipo-pto = dc-pto-controle.tipo-pto
               dc-rg-item.local-pto = dc-pto-controle.local-pto
               dc-rg-item.cod-depos-orig = dc-pto-controle.cod-depos-dest
               dc-rg-item.cod-localiz-orig = dc-pto-controle.cod-localiz-dest.
               
        ASSIGN i-seq = NEXT-VALUE(seq-dc-movto-pto-controle).

        FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle
            WHERE b-dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
            NO-LOCK NO-ERROR.
        IF AVAIL b-dc-movto-pto-controle THEN
        DO:
            CREATE dc-movto-pto-controle.
            ASSIGN dc-movto-pto-controle.nr-seq = i-seq.
            BUFFER-COPY b-dc-movto-pto-controle EXCEPT nr-seq TO dc-movto-pto-controle.
            ASSIGN dc-movto-pto-controle.cod-depos-orig = dc-rg-item.cod-depos-orig
                   dc-movto-pto-controle.cod-localiz-orig = dc-rg-item.cod-localiz-orig
                   dc-movto-pto-controle.cod-depos-dest = dc-rg-item.cod-depos-orig
                   dc-movto-pto-controle.cod-localiz-dest = dc-rg-item.cod-localiz-orig
                   dc-movto-pto-controle.cod-pto-controle = "RATIV"
                   dc-movto-pto-controle.tipo-pto = dc-rg-item.tipo-pto
                   dc-movto-pto-controle.local-pto = dc-rg-item.local-pto
                   dc-movto-pto-controle.nr-seq-ant = b-dc-movto-pto-controle.nr-seq
                   dc-movto-pto-controle.cod-usuar = v_cod_usuar_corren
                   dc-movto-pto-controle.data = TODAY
                   dc-movto-pto-controle.hora = STRING(TIME,"hh:mm:ss").
        END.     
        
        RUN gerarSucesso.
                  
    END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF



