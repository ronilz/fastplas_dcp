/******************************************************************************
** Programa: espl001rp.p - Importa��o de op externas 
** Vers�o  : 2.04.00.001 
** Autor   : 
** Data    : 
******************************************************************************
** Vers�o  : 2.04.00.002 
** Autor   : rzo 
** Data    : exportar dados para analise

* Altera��o.: Triah - Ramon - 02/04/2012
*             - Alterado para permitir mais de uma lo-plames para o mesmo item no mesmo mes 
* Marca��o.:  - 02042012 

******************************************************************************/

/* include de controle de vers�o */
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ESPL001 2.06.00.001}

/* pr�processador para ativar ou n�o a sa�da para RTF */
&GLOBAL-define RTF no

/* pr�processador para setar o tamanho da p�gina */
&SCOPED-define pagesize 42   

/* defini��o das temp-tables para recebimento de par�metros */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field c-it-codigo-ini  as char
    field c-it-codigo-fim  as char
    field c-fm-codigo-ini  as char
    field c-fm-codigo-fim  as char
    field c-fam-codigo-ini  as char
    field c-fam-codigo-fim  as char
    field c-periodo-ini    as char
    field c-periodo-fim    as char
    field l-simulacao      as log
    FIELD cd-plano         AS INTEGER.

define temp-table tt-raw-digita no-undo
    field raw-digita	   as raw.

DEF TEMP-TABLE tt-pl-it-prod NO-UNDO LIKE pl-it-prod
    FIELD linha AS INT
    FIELD l-erro AS LOG INIT NO
    FIELD r-rowid AS ROWID
    index idx-linha is unique primary linha.

DEF BUFFER bf-tt-pl-it-prod FOR tt-pl-it-prod.

DEFINE TEMP-TABLE tt-erros
    FIELD tipo AS INTEGER
    FIELD campo AS CHAR
    FIELD descricao AS CHAR.

/* recebimento de par�metros */
define input parameter raw-param as raw no-undo.
define input parameter table for tt-raw-digita.

create tt-param.
raw-transfer raw-param to tt-param.

/* include padr�o para vari�veis de relat�rio  */
{include/i-rpvar.i}

/*includes do ems 204*/
{utp/ut-glob.i}
{cdp/cd0666.i}

/* defini��o de vari�veis  */
def var h-acomp        as handle no-undo.
DEFINE VARIABLE i-mes AS INTEGER    NO-UNDO.
DEFINE VARIABLE i-ano AS INTEGER    NO-UNDO.
DEFINE VARIABLE c-anomes AS CHARACTER  NO-UNDO.
DEFINE VARIABLE i-cont AS INTEGER    NO-UNDO.
DEFINE VARIABLE dt-termino AS DATE       NO-UNDO.
DEFINE VARIABLE i-per-inf AS INTEGER    NO-UNDO.
DEFINE VARIABLE i-peri-grav AS INTEGER    NO-UNDO.
DEFINE VARIABLE i-perf-grav AS INTEGER    NO-UNDO.
DEFINE VARIABLE i-linha AS INTEGER    NO-UNDO.

/*002*/
DEFINE VARIABLE l-exporta AS LOGICAL     NO-UNDO.
DEFINE STREAM str-exp.


/* defini��o de forms */
FORM tt-pl-it-prod.linha format "999999" LABEL "Linha"
     tt-pl-it-prod.cd-plano
     tt-pl-it-prod.cod-estabel
     tt-pl-it-prod.it-codigo
     tt-pl-it-prod.cod-refer
     tt-pl-it-prod.dt-termino
     tt-pl-it-prod.quantidade FORMAT "->>>>,>>9.9999"
     /*tt-pl-it-prod.nr-pedcli
     tt-pl-it-prod.nome-abrev
     tt-pl-it-prod.nr-sequencia*/
   with width 132 DOWN frame f-pl-it-prod stream-io.
              
form tt-erros.tipo at 16
     tt-erros.campo 
     tt-erros.descricao
     with width 132 down frame f-erros stream-io.

     
/* include padr�o para output de relat�rios */
{include/i-rpout.i &STREAM="stream str-rp"}

/* include com a defini��o da frame de cabe�alho e rodap� */
{include/i-rpcab.i &STREAM="str-rp"}

/* bloco principal do programa */
find empresa no-lock
    where empresa.ep-codigo = i-ep-codigo-usuario no-error.

assign c-programa 	  = "ESPL001RP"
       c-versao	      = "2.06"
       c-revisao	  = ".00.000"
       c-empresa      = empresa.nome
       c-sistema	  = "Produ��o"
       c-titulo-relat = if not tt-param.l-simula then "Importa��o de Itens do Plano Produ��o" else "SIMULA��O Itens Plano Produ��o".

VIEW STREAM str-rp FRAME f-cabec.

if not tt-param.l-simula then
    view stream str-rp frame f-cab-ordem.

VIEW STREAM str-rp FRAME f-rodape.


/* executando de forma persistente o utilit�rio de acompanhamento */
RUN utp/ut-acomp.p PERSISTENT SET h-acomp.

{utp/ut-liter.i Imprimindo *}
RUN pi-inicializar IN h-acomp (INPUT RETURN-VALUE).

/* corpo do relat�rio */
FIND FIRST pl-prod 
    WHERE pl-prod.cd-plano = tt-param.cd-plano 
    NO-LOCK NO-ERROR.
IF NOT AVAIL pl-prod THEN 
    RUN gerarErro (INPUT 1,
                   INPUT tt-param.cd-plano,
                   INPUT 'Plano n�o cadastrado').

/* FIND FIRST param-global                                   */
/*     NO-LOCK NO-ERROR.                                     */
/* IF NOT AVAIL param-global THEN                            */
/*     RUN gerarErro (INPUT 1,                               */
/*                    INPUT "Param. Global",                 */
/*                    INPUT 'Param. Global n�o dispon�vel'). */

/** 
*  ler registros
**/

/*
DEBUGGER:CLEAR().
DEBUGGER:INITIATE().
*/

/**002 **/
ASSIGN l-exporta = TRUE.
IF l-exporta THEN DO:
    OUTPUT STREAM str-exp TO VALUE(SESSION:TEMP-DIRE + "espl001rp.exp").
END.

ASSIGN i-linha = 0.
for each lo-plames 
    where lo-plames.anomes      >= tt-param.c-periodo-ini
      and lo-plames.anomes      <= tt-param.c-periodo-fim
      and lo-plames.it-codigo   >= tt-param.c-it-codigo-ini
      and lo-plames.it-codigo   <= tt-param.c-it-codigo-fim
      and lo-plames.l-item-prod = no
      /*AND lo-plames.pl-przir[2] > 0 /* ronil */*/,
    each item
    where item.it-codigo  = lo-plames.it-codigo
      and item.fm-codigo >= tt-param.c-fm-codigo-ini
      and item.fm-codigo <= tt-param.c-fm-codigo-fim
      AND ITEM.fm-cod-com >= tt-param.c-fam-codigo-ini
      AND ITEM.fm-cod-com <= tt-param.c-fam-codigo-fim
    no-lock:

    IF l-exporta THEN
        EXPORT STREAM str-exp lo-plames.

    run pi-acompanhar in h-acomp (input "Item do Plano: " + lo-plames.it-codigo).

    /**
    *  Cada registro possui plano para 3 meses
    **/
    ASSIGN c-anomes = lo-plames.anomes.
    DO i-cont = 2 TO 4.

        ASSIGN i-mes = INT(SUBSTRING(c-anomes,5,2))
               i-ano = INT(SUBSTRING(c-anomes,1,4)).
    
        IF i-mes >= 12 THEN
            ASSIGN i-mes = 01
                   i-ano = i-ano + 1.
        ELSE
            ASSIGN i-mes = i-mes + 1.
    
        ASSIGN dt-termino = DATE(i-mes, 01, i-ano) - 1.

        IF lo-plames.pl-przir[i-cont] > 0 THEN
        DO:

            FIND FIRST tt-pl-it-prod
                 WHERE tt-pl-it-prod.cd-plano   = pl-prod.cd-plano
                   AND tt-pl-it-prod.it-codigo  = lo-plames.it-codigo
                   AND tt-pl-it-prod.dt-termino = dt-termino
                   AND tt-pl-it-prod.r-rowid    = ROWID(lo-plames)  /*02042012*/
                       NO-LOCK NO-ERROR.
    
            IF NOT AVAIL tt-pl-it-prod THEN  
                CREATE tt-pl-it-prod.
     
            ASSIGN tt-pl-it-prod.cd-plano     = pl-prod.cd-plano
                   tt-pl-it-prod.cod-estabel  = pl-prod.cod-estabel
                   tt-pl-it-prod.it-codigo    = lo-plames.it-codigo
                   tt-pl-it-prod.cod-refer    = ITEM.cod-refer
                   tt-pl-it-prod.dt-termino   = dt-termino
                   tt-pl-it-prod.quantidade   = lo-plames.pl-przir[i-cont]
                   tt-pl-it-prod.nr-pedcli    = ''
                   tt-pl-it-prod.nome-abrev   = ''
                   tt-pl-it-prod.nr-sequencia = 0
                   tt-pl-it-prod.r-rowid      = ROWID(lo-plames)
                   i-linha                    = i-linha + 1
                   tt-pl-it-prod.linha        = i-linha.

            IF l-exporta THEN
                PUT STREAM str-exp tt-pl-it-prod.cd-plano tt-pl-it-prod.dt-termino.
        END.
        /**
        *  calcular novo ano-mes
        **/
        ASSIGN dt-termino = dt-termino + 1
               c-anomes = STRING(YEAR(dt-termino), "9999") + STRING(MONTH(dt-termino),"99").
    END.
end.

/**
*  validar registros
**/
FOR EACH tt-pl-it-prod.

    run pi-acompanhar in h-acomp (input "Validando Item do Plano: " + tt-pl-it-prod.it-codigo).

    /*Testa atributos obrigatorios conforme o layout de importacao*/
    IF tt-pl-it-prod.cd-plano    = ?   OR
       tt-pl-it-prod.it-codigo   = ""  OR
       tt-pl-it-prod.dt-termino  = ?   OR
       tt-pl-it-prod.quantidade  = ?  THEN
    DO:
        RUN gerarErro (INPUT 1,
                       INPUT '',
                       INPUT 'Informa��es obrigat�rias n�o foram informadas.').
    END.

    /*Valida Quantidade*/
    IF tt-pl-it-prod.quantidade < 0 THEN
    DO:
        RUN gerarErro (INPUT 1,
                       INPUT 'Quantidade',
                       INPUT 'Quantidade informada deve ser maior ou igual a zero.').
    END.

    /* Valida periodo do item dentro do periodo da plano */
    IF AVAIL pl-prod THEN
    DO:
        FIND FIRST periodo
             WHERE periodo.cd-tipo     = pl-prod.cd-tipo
             AND   periodo.dt-inicio  <= tt-pl-it-prod.dt-termino
             AND   periodo.dt-termino >= tt-pl-it-prod.dt-termino NO-LOCK NO-ERROR.
        IF l-exporta THEN
            PUT STREAM str-exp AVAIL periodo periodo.dt-inicio periodo.dt-termino.

        IF NOT AVAIL periodo THEN
        DO:
            RUN gerarErro (INPUT 1,
                       INPUT 'Periodo',
                       INPUT 'N�o h� periodo para a data t�rmino informada.').
        END.
        ELSE
        DO:
            assign i-per-inf   = int(string(YEAR(tt-pl-it-prod.dt-termino), "9999") + string(periodo.nr-periodo,"999"))
                   i-peri-grav = int(string(pl-prod.ano-per-ini,"9999") + string(pl-prod.nr-per-ini,"999"))
                   i-perf-grav = int(string(pl-prod.ano-per-fim,"9999") + string(pl-prod.nr-per-fim,"999")).

            if i-per-inf < i-peri-grav OR
               i-per-inf > i-perf-grav then
            do:
                RUN gerarErro (INPUT 1,
                               INPUT 'Periodo',
                               INPUT 'Per�odo do Item fora do intervalo do periodo do plano.').
            END.
        END.
    END.

    /*encontra a ped-venda para pegar o cod-gr-cli, caso tenha sido informado o nome-abrev e o nr-pedcli*/
/*     IF tt-pl-it-prod.nome-abrev <> "" AND tt-pl-it-prod.nr-pedcli <> "" THEN DO: */
/*         FOR FIRST ped-venda FIELDS(nome-abrev nr-pedcli cod-gr-cli)              */
/*             WHERE ped-venda.nome-abrev = tt-pl-it-prod.nome-abrev                */
/*               AND ped-venda.nr-pedcli  = tt-pl-it-prod.nr-pedcli NO-LOCK:        */
/*         END.                                                                     */
/*                                                                                  */
/*         IF AVAIL ped-venda THEN                                                  */
/*             ASSIGN tt-pl-it-prod.cod-gr-cli = ped-venda.cod-gr-cli.              */
/*     END.                                                                         */

    /* ALTERADO para considerar mais de uma lo-plames so vai dar mensagem de plano duplicado  02042012 */
    /*Valida se n�o est  sendo importado mais de um registro com a mesma chave*/
    FIND FIRST bf-tt-pl-it-prod
         WHERE bf-tt-pl-it-prod.cd-plano     = tt-pl-it-prod.cd-plano
           AND bf-tt-pl-it-prod.cod-estabel  = tt-pl-it-prod.cod-estabel
           AND bf-tt-pl-it-prod.it-codigo    = tt-pl-it-prod.it-codigo
           AND bf-tt-pl-it-prod.cod-refer    = tt-pl-it-prod.cod-refer
           AND bf-tt-pl-it-prod.ano          = periodo.ano        /*tt-pl-it-prod.ano*/
           AND bf-tt-pl-it-prod.periodo      = periodo.nr-periodo /*tt-pl-it-prod.periodo*/
           AND bf-tt-pl-it-prod.nome-abrev   = tt-pl-it-prod.nome-abrev
           AND bf-tt-pl-it-prod.nr-pedcli    = tt-pl-it-prod.nr-pedcli
           AND bf-tt-pl-it-prod.nr-sequencia = tt-pl-it-prod.nr-sequencia
           AND bf-tt-pl-it-prod.cod-gr-cli   = tt-pl-it-prod.cod-gr-cli
           AND ROWID(bf-tt-pl-it-prod)      <> ROWID(tt-pl-it-prod) NO-ERROR.

    IF AVAIL bf-tt-pl-it-prod THEN
    DO:
        RUN gerarErro (INPUT 2,   /*02042012*/ 
                       INPUT 'Item Plano',
                       INPUT 'Item Plano Duplicado:' + tt-pl-it-prod.it-codigo + 
                             " - " + STRING(tt-pl-it-prod.ano) + 
                             " - " + STRING(tt-pl-it-prod.periodo)         +
                             " Qt1 " + STRING(bf-tt-pl-it-prod.quantidade) +    /*02042012*/
                             " Qt2 " + STRING(tt-pl-it-prod.quantidade) )  .     /*02042012*/
     END.
 
   /*verifica erros tipo = 1 */ 
    IF CAN-FIND(tt-erros
                WHERE tt-erros.tipo = 1) THEN
        ASSIGN tt-pl-it-prod.l-erro = TRUE.
    ELSE
        /*Grava as demais informa��es da tabela*/
        ASSIGN tt-pl-it-prod.ano     = periodo.ano
               tt-pl-it-prod.periodo = periodo.nr-periodo.
END.

/**
*  Verificar se o simula n�o esta marcado
**/
IF NOT tt-param.l-simulacao THEN
DO:
    DO TRANSACTION ON ERROR UNDO, LEAVE:
        /****CRIA OS REGISTROS IMPORTADOS NA TABELA PL-IT-PROD*********/
        FOR EACH tt-pl-it-prod 
            WHERE tt-pl-it-prod.l-erro = NO .

            RUN pi-acompanhar in h-acomp (input "Efetivando Informacoes ":U + STRING(tt-pl-it-prod.it-codigo)).
            FIND pl-it-prod EXCLUSIVE-LOCK
                WHERE pl-it-prod.cd-plano     = tt-pl-it-prod.cd-plano
                AND   pl-it-prod.cod-estabel  = tt-pl-it-prod.cod-estabel
                AND   pl-it-prod.it-codigo    = tt-pl-it-prod.it-codigo
                AND   pl-it-prod.cod-refer    = tt-pl-it-prod.cod-refer
                AND   pl-it-prod.ano          = tt-pl-it-prod.ano
                AND   pl-it-prod.periodo      = tt-pl-it-prod.periodo
                AND   pl-it-prod.nome-abrev   = tt-pl-it-prod.nome-abrev
                AND   pl-it-prod.nr-pedcli    = tt-pl-it-prod.nr-pedcli
                AND   pl-it-prod.nr-sequencia = tt-pl-it-prod.nr-sequencia
                AND   pl-it-prod.cod-gr-cli   = tt-pl-it-prod.cod-gr-cli NO-ERROR.
    
            IF NOT AVAIL pl-it-prod THEN  
            DO:
                FIND lo-plames
                    WHERE ROWID(lo-plames) = tt-pl-it-prod.r-rowid NO-ERROR.

                CREATE pl-it-prod.
                BUFFER-COPY tt-pl-it-prod TO pl-it-prod.

                IF AVAIL lo-plames THEN
                    ASSIGN lo-plames.l-item-prod = YES.
            END.
            ELSE
            DO:
                FIND lo-plames
                    WHERE ROWID(lo-plames) = tt-pl-it-prod.r-rowid NO-ERROR.

               /* BUFFER-COPY tt-pl-it-prod TO pl-it-prod.    */     /*02042012*/ 

                ASSIGN pl-it-prod.quantidade = pl-it-prod.quantidade + tt-pl-it-prod.quantidade.  /*02042012*/ 

                IF AVAIL lo-plames THEN
                    ASSIGN lo-plames.l-item-prod = YES.
            END.
/*             ELSE                                                                  */
/*                 RUN gerarErro (INPUT 1,                                           */
/*                                INPUT 'Item Plano J� Importado',                   */
/*                                INPUT 'Registro sendo importado em duplicidade.'). */
        END.
    END.
END.



/*****MOSTRA NO RELATORIO AS INFORMACOES IMPORTADAS E RESPECTIVOS ERROS*****/
FOR EACH tt-pl-it-prod 
    BREAK BY tt-pl-it-prod.it-codigo:

    DISPLAY stream str-rp
            tt-pl-it-prod.linha
            tt-pl-it-prod.cd-plano
            tt-pl-it-prod.cod-estabel
            tt-pl-it-prod.it-codigo
            tt-pl-it-prod.cod-refer
            tt-pl-it-prod.dt-termino
            tt-pl-it-prod.quantidade
            /*tt-pl-it-prod.nr-pedcli - rzo 17/10/2007
             tt-pl-it-prod.nome-abrev
             tt-pl-it-prod.nr-sequencia*/
        WITH FRAME f-pl-it-prod.
    DOWN WITH FRAME f-pl-it-prod.

END.



FOR EACH tt-erros.

    DISPLAY STREAM str-rp
            tt-erros.tipo      FORMAT "999999"
            tt-erros.campo     FORMAT "x(15)"
            tt-erros.descricao FORMAT "x(60)"
            WITH FRAME f-erros.
    DOWN WITH FRAME f-erros.
END.



/*fechamento do output do relat�rio*/
IF l-exporta THEN
    OUTPUT STREAM str-exp CLOSE.
{include/i-rpclo.i &STREAM="stream str-rp"}
RUN pi-finalizar IN h-acomp.
RETURN "OK":U.


PROCEDURE gerarErro:
/******************************************************************************
 Procedure utilizada para criar uma tabela com os erros encontrados 
*******************************************************************************/
    DEF INPUT PARAMETER p-erro  AS INTEGER NO-UNDO  .
    DEF INPUT PARAMETER p-campo AS CHAR    NO-UNDO.
    DEF INPUT PARAMETER p-desc  AS CHAR    NO-UNDO.

    CREATE tt-erros.
    ASSIGN tt-erros.tipo      = p-erro
           tt-erros.campo     = p-campo
           tt-erros.descricao = p-desc.

END PROCEDURE .


