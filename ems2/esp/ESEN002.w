&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME w-relat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-relat 
/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
/*
**
** emiyahira - 13/08/2010 - revisao para 2.06
**
*/
{ESP\BUFFERS.I}

/*define buffer empresa for mgcad.empresa.*/
 

{include/i-prgvrs.i ESEN001 2.06.00.000}


/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Preprocessadores do Template de Relat�rio                            */
/* Obs: Retirar o valor do preprocessador para as p�ginas que n�o existirem  */

&GLOBAL-DEFINE PGSEL f-pg-sel
&GLOBAL-DEFINE PGCLA 
&GLOBAL-DEFINE PGPAR f-pg-par
&GLOBAL-DEFINE PGDIG 
&GLOBAL-DEFINE PGIMP f-pg-imp
  
/* Parameters Definitions ---                                           */
{utils/retorna_diretorio_usuario.i}
/* Temporary Table Definitions ---                                      */

define temp-table tt-param no-undo
    field destino           as integer
    field arquivo           as char format "x(35)"
    field usuario           as char format "x(12)"
    field data-exec         as date
    field hora-exec         as integer
    field classifica        as integer
    field desc-classifica   as char format "x(40)"
    field fm-codigo-ini   as char
    field fm-codigo-fim   as char
    field it-codigo-ini     as char
    field it-codigo-fim     as char
    field fm-cod-com-ini    as char
    field fm-cod-com-fim    as char
    field cod-estabel-ini   as char
    field cod-estabel-fim   as char
    field cd-planejado-ini  as char
    field cd-planejado-fim  as char
    FIELD nr-linha-ini      AS INT
    FIELD nr-linha-fim      AS INT
    field cb-classe-repro   as int
    field cb-contr-plan     as int
    field cb-demanda        as int
    field cb-div-ordem      as int
    field cb-emissao-ord    as int
    field cb-politica       as int
    field cb-reab-estoq     as int
    FIELD cb-ind-lista-mrp  AS INT
    field lote-economi      like item-uni-estab.lote-economi
    field lote-minimo       like item-uni-estab.lote-minimo 
    field lote-multipl      like item-uni-estab.lote-multipl
    field periodo-fixo      like item-uni-estab.periodo-fixo
    field quant-segur       like item-uni-estab.quant-segur 
    field res-cq-comp       like item-uni-estab.res-cq-comp 
    field res-for-comp      like item-uni-estab.res-for-comp
    field res-int-comp      like item-uni-estab.res-int-comp
    field tempo-segur       like item-uni-estab.tempo-segur 
    field tipo-est-seg      like item-uni-estab.tipo-est-seg
    FIELD tb-conv-tempo-seg AS  LOG
    field tg-simulacao      as log
    FIELD rs-importa        AS INT
    FIELD cd-planejado      LIKE lin-prod.cd-planejado 
    FIELD nr-linha          LIKE lin-prod.nr-linha
    FIELD arquivo-importacao AS CHAR
    FIELD arquivo-exportacao AS CHAR.
   

define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.

define buffer b-tt-digita for tt-digita.

/* Transfer Definitions */

def var raw-param        as raw no-undo.

def temp-table tt-raw-digita
   field raw-digita      as raw.
                    
/* Local Variable Definitions ---                                       */

def var l-ok               as logical no-undo.
def var c-arq-digita       as char    no-undo.
def var c-terminal         as char    no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE w-relat
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-pg-imp

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-7 RECT-9 rs-destino-2 rs-destino ~
bt-arquivo bt-config-impr c-arquivo rs-execucao 
&Scoped-Define DISPLAYED-OBJECTS rs-destino-2 rs-destino c-arquivo ~
rs-execucao 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */
&Scoped-define List-2 cb-politica cb-demanda lote-multipl lote-minimo ~
lote-economi periodo-fixo Tipo-est-seg quant-segur tempo-segur ~
tb-conv-tempo-seg cb-reab-estoq cb-classe-repro cb-emissao-ord ~
cb-contr-plan cb-div-ordem cb-ind-lista-mrp 
&Scoped-define List-3 cb-politica cb-demanda lote-multipl lote-minimo ~
lote-economi periodo-fixo Tipo-est-seg quant-segur tempo-segur ~
tb-conv-tempo-seg cb-reab-estoq cb-classe-repro cb-emissao-ord ~
cb-contr-plan cb-div-ordem 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-relat AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-arquivo 
     IMAGE-UP FILE "image\im-sea":U
     IMAGE-INSENSITIVE FILE "image\ii-sea":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE BUTTON bt-config-impr 
     IMAGE-UP FILE "image\im-cfprt":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE VARIABLE c-arquivo AS CHARACTER 
     VIEW-AS EDITOR MAX-CHARS 256
     SIZE 40 BY .88
     BGCOLOR 15  NO-UNDO.

DEFINE VARIABLE text-destino AS CHARACTER FORMAT "X(256)":U INITIAL " Destino" 
      VIEW-AS TEXT 
     SIZE 8.57 BY .63 NO-UNDO.

DEFINE VARIABLE text-modo AS CHARACTER FORMAT "X(256)":U INITIAL "Execu��o" 
      VIEW-AS TEXT 
     SIZE 10.86 BY .63 NO-UNDO.

DEFINE VARIABLE rs-destino AS INTEGER INITIAL 2 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Impressora", 1,
"Arquivo", 2,
"Terminal", 3
     SIZE .86 BY .21 NO-UNDO.

DEFINE VARIABLE rs-destino-2 AS INTEGER INITIAL 3 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Arquivo", 2,
"Terminal", 3
     SIZE 44 BY 1.08 NO-UNDO.

DEFINE VARIABLE rs-execucao AS INTEGER INITIAL 1 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "On-Line", 1,
"Batch", 2
     SIZE 27.72 BY .92 NO-UNDO.

DEFINE RECTANGLE RECT-7
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.29 BY 2.92.

DEFINE RECTANGLE RECT-9
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46.29 BY 1.75.

DEFINE VARIABLE cb-classe-repro AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Classe Reprograma��o" 
     VIEW-AS COMBO-BOX INNER-LINES 4
     LIST-ITEM-PAIRS "Antecipa/Prorroga",1,
                     "Prorroga",2,
                     "Antecipa",3,
                     "N�o Reprograma",4
     DROP-DOWN-LIST
     SIZE 18.43 BY 1 NO-UNDO.

DEFINE VARIABLE cb-contr-plan AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Controle Planejamento" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Produ��o",1,
                     "Manuten��o Industrial",2
     DROP-DOWN-LIST
     SIZE 18.29 BY 1 NO-UNDO.

DEFINE VARIABLE cb-demanda AS INTEGER FORMAT "->,>>>,>>9" INITIAL 1 
     LABEL "Demanda":R18 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Dependente",1,
                     "Independente",2
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cb-div-ordem AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Divis�o Ordens" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "N�o Divide",1,
                     "Lote M�ltiplo",2,
                     "Lote Econ�mico",3
     DROP-DOWN-LIST
     SIZE 18.43 BY 1 NO-UNDO.

DEFINE VARIABLE cb-emissao-ord AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Emiss�o Ordens" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Autom�tico",1,
                     "Manual",2
     DROP-DOWN-LIST
     SIZE 18.43 BY 1 NO-UNDO.

DEFINE VARIABLE cb-ind-lista-mrp AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Processo MRP" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Processo Principal",1,
                     "Todos os Processos",2
     DROP-DOWN-LIST
     SIZE 18.43 BY 1 NO-UNDO.

DEFINE VARIABLE cb-politica AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Politica" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "Per�odo Fixo",1,
                     "Lote Econ�mico",2,
                     "Ordem",3,
                     "N�vel Superior",4,
                     "Configurado",5,
                     "Composto",6,
                     "Ponto de Reposi��o",7
     DROP-DOWN-LIST
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE cb-reab-estoq AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 1 
     LABEL "Reabastecimento" 
     VIEW-AS COMBO-BOX INNER-LINES 2
     LIST-ITEM-PAIRS "Demanda",1,
                     "Quantidade",2
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cd-planejado AS CHARACTER FORMAT "x(12)" 
     LABEL "Planejador":R12 
     VIEW-AS FILL-IN 
     SIZE 13.57 BY .88.

DEFINE VARIABLE lote-economi AS DECIMAL FORMAT ">>>>,>>9.9999" INITIAL 1 
     LABEL "Lote Econ�mico":R17 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88.

DEFINE VARIABLE lote-minimo AS DECIMAL FORMAT ">>>>,>>9.9999" INITIAL 1 
     LABEL "Lote M�nimo":R14 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88.

DEFINE VARIABLE lote-multipl AS DECIMAL FORMAT ">>>>,>>9.9999" INITIAL 1 
     LABEL "Lote M�ltiplo":R16 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88.

DEFINE VARIABLE nr-linha AS INTEGER FORMAT ">>9" INITIAL 0 
     LABEL "Linha Produ��o":R17 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY .88.

DEFINE VARIABLE periodo-fixo AS INTEGER FORMAT ">>9" INITIAL 1 
     LABEL "Per�odo Fixo":R15 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY .88.

DEFINE VARIABLE quant-segur AS DECIMAL FORMAT ">>>>,>>9.9999" INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88.

DEFINE VARIABLE res-cq-comp AS INTEGER FORMAT ">>>9" INITIAL 0 
     LABEL "Ressupr CQ":R12 
     VIEW-AS FILL-IN 
     SIZE 5.57 BY .88.

DEFINE VARIABLE res-for-comp AS INTEGER FORMAT ">>>9" INITIAL 0 
     LABEL "Ressupr Fornec":R17 
     VIEW-AS FILL-IN 
     SIZE 5.57 BY .88.

DEFINE VARIABLE res-int-comp AS INTEGER FORMAT ">>>9" INITIAL 0 
     LABEL "Ressupr Compras":R18 
     VIEW-AS FILL-IN 
     SIZE 4 BY .79.

DEFINE VARIABLE tempo-segur AS INTEGER FORMAT ">>9" INITIAL 7 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY .88.

DEFINE VARIABLE Tipo-est-seg AS INTEGER INITIAL 1 
     VIEW-AS RADIO-SET VERTICAL
     RADIO-BUTTONS 
          "Quantidade", 1,
"Tempo", 2
     SIZE 11 BY 1.79.

DEFINE RECTANGLE RECT-33
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 31 BY 2.38.

DEFINE RECTANGLE RECT-34
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 29 BY 3.25.

DEFINE RECTANGLE rt-temp-qt-seg
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 31 BY 2.46.

DEFINE VARIABLE tb-conv-tempo-seg AS LOGICAL INITIAL no 
     LABEL "Converte Tempo Seg" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .75 NO-UNDO.

DEFINE BUTTON bt-arquivo-2 
     IMAGE-UP FILE "image\im-sea":U
     IMAGE-INSENSITIVE FILE "image\ii-sea":U
     LABEL "" 
     SIZE 4 BY 1.

DEFINE VARIABLE c-arquivo-importacao AS CHARACTER FORMAT "X(256)":U 
     LABEL "Arquivo Importa��o" 
     VIEW-AS FILL-IN 
     SIZE 43 BY .88 NO-UNDO.

DEFINE VARIABLE cd-planejado-fim AS CHARACTER FORMAT "x(12)" INITIAL "ZZZZZZZZZZZZ" 
     VIEW-AS FILL-IN 
     SIZE 13.57 BY .88.

DEFINE VARIABLE cd-planejado-ini AS CHARACTER FORMAT "x(12)" 
     LABEL "Planejador":R12 
     VIEW-AS FILL-IN 
     SIZE 13.57 BY .88.

DEFINE VARIABLE cod-estabel-fim AS CHARACTER FORMAT "x(3)" INITIAL "ZZZ" 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY .88.

DEFINE VARIABLE cod-estabel-ini AS CHARACTER FORMAT "x(3)" 
     LABEL "Estabelecimento Padr�o":R26 
     VIEW-AS FILL-IN 
     SIZE 4.57 BY .88.

DEFINE VARIABLE fm-cod-com-fim AS CHARACTER FORMAT "x(8)" INITIAL "ZZZZZZZZ" 
     VIEW-AS FILL-IN 
     SIZE 9.57 BY .88.

DEFINE VARIABLE fm-cod-com-ini AS CHARACTER FORMAT "x(8)" 
     LABEL "Fam�lia Comercial":R21 
     VIEW-AS FILL-IN 
     SIZE 9.57 BY .88.

DEFINE VARIABLE fm-codigo-fim AS CHARACTER FORMAT "x(8)" INITIAL "ZZZZZZZZ" 
     VIEW-AS FILL-IN 
     SIZE 9.57 BY .88.

DEFINE VARIABLE fm-codigo-ini AS CHARACTER FORMAT "X(8)":U 
     LABEL "Fam�lia Material" 
     VIEW-AS FILL-IN 
     SIZE 9.57 BY .88 NO-UNDO.

DEFINE VARIABLE it-codigo-fim AS CHARACTER FORMAT "x(16)" INITIAL "ZZZZZZZZZZZZZZZZZ" 
     VIEW-AS FILL-IN 
     SIZE 17.57 BY .88.

DEFINE VARIABLE it-codigo-ini AS CHARACTER FORMAT "x(16)" 
     LABEL "Item":R5 
     VIEW-AS FILL-IN 
     SIZE 17.57 BY .88.

DEFINE VARIABLE nr-linha-fim AS INTEGER FORMAT ">>9" INITIAL 999 
     VIEW-AS FILL-IN 
     SIZE 3.72 BY .88.

DEFINE VARIABLE nr-linha-ini AS INTEGER FORMAT ">>9" INITIAL 0 
     LABEL "Linha Produ��o":R17 
     VIEW-AS FILL-IN 
     SIZE 3.72 BY .88.

DEFINE IMAGE IMAGE-10
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-11
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-12
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-13
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-14
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-3
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-4
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-5
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-6
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-7
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-8
     FILENAME "image\im-las":U
     SIZE 3 BY .88.

DEFINE IMAGE IMAGE-9
     FILENAME "image\im-fir":U
     SIZE 3 BY .88.

DEFINE VARIABLE rs-importa AS INTEGER 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Atualiza de dados via Par�metros", 1,
"Atualiza de dados via Importa��o", 2
     SIZE 55.14 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-35
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 73 BY 11.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 7.25.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 1.42.

DEFINE RECTANGLE RECT-38
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 71 BY 1.25.

DEFINE VARIABLE tg-simulacao AS LOGICAL INITIAL yes 
     LABEL "Simula��o" 
     VIEW-AS TOGGLE-BOX
     SIZE 33 BY .83 NO-UNDO.

DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Fechar" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-executar 
     LABEL "Executar" 
     SIZE 10 BY 1.

DEFINE IMAGE im-pg-imp
     FILENAME "image\im-fldup":U
     SIZE 15.72 BY 1.25.

DEFINE IMAGE im-pg-par
     FILENAME "image\im-fldup":U
     SIZE 15.72 BY 1.25.

DEFINE IMAGE im-pg-sel
     FILENAME "image\im-fldup":U
     SIZE 15.72 BY 1.25.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 79 BY 1.42
     BGCOLOR 7 .

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 0    
     SIZE 78.72 BY .13
     BGCOLOR 7 .

DEFINE RECTANGLE rt-folder
     EDGE-PIXELS 1 GRAPHIC-EDGE  NO-FILL   
     SIZE 79 BY 12
     FGCOLOR 0 .

DEFINE RECTANGLE rt-folder-left
     EDGE-PIXELS 0    
     SIZE .43 BY 11.88
     BGCOLOR 15 .

DEFINE RECTANGLE rt-folder-right
     EDGE-PIXELS 0    
     SIZE .43 BY 11.88
     BGCOLOR 7 .

DEFINE RECTANGLE rt-folder-top
     EDGE-PIXELS 0    
     SIZE 78.72 BY .13
     BGCOLOR 15 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-pg-imp
     rs-destino-2 AT ROW 2.38 COL 3.29 HELP
          "Destino de Impress�o do Relat�rio" NO-LABEL WIDGET-ID 2
     rs-destino AT ROW 3.25 COL 3.29 HELP
          "Destino de Impress�o do Relat�rio" NO-LABEL
     bt-arquivo AT ROW 3.58 COL 43.29 HELP
          "Escolha do nome do arquivo"
     bt-config-impr AT ROW 3.58 COL 43.29 HELP
          "Configura��o da impressora"
     c-arquivo AT ROW 3.63 COL 3.29 HELP
          "Nome do arquivo de destino do relat�rio" NO-LABEL
     rs-execucao AT ROW 5.75 COL 3 HELP
          "Modo de Execu��o" NO-LABEL
     text-destino AT ROW 1.63 COL 3.86 NO-LABEL
     text-modo AT ROW 5 COL 1.29 COLON-ALIGNED NO-LABEL
     RECT-7 AT ROW 1.92 COL 2.14
     RECT-9 AT ROW 5.25 COL 2.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3.86 ROW 3
         SIZE 73.72 BY 10.92.

DEFINE FRAME f-relat
     bt-executar AT ROW 15.21 COL 3 HELP
          "Dispara a execu��o do relat�rio"
     bt-cancelar AT ROW 15.21 COL 14 HELP
          "Fechar"
     bt-ajuda AT ROW 15.21 COL 70 HELP
          "Ajuda"
     rt-folder-left AT ROW 2.54 COL 2.14
     rt-folder-right AT ROW 2.67 COL 80.43
     RECT-1 AT ROW 15 COL 2
     RECT-6 AT ROW 14.42 COL 2.14
     rt-folder-top AT ROW 2.54 COL 2.14
     rt-folder AT ROW 2.5 COL 2
     im-pg-imp AT ROW 1.5 COL 33.57
     im-pg-par AT ROW 1.5 COL 17.86
     im-pg-sel AT ROW 1.5 COL 2.14
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 81 BY 15.5
         DEFAULT-BUTTON bt-executar.

DEFINE FRAME f-pg-par
     cb-politica AT ROW 1.38 COL 15 COLON-ALIGNED WIDGET-ID 10
     cb-demanda AT ROW 2.38 COL 15 COLON-ALIGNED HELP
          "Tipo de Demanda do Item" WIDGET-ID 2
     lote-multipl AT ROW 3.38 COL 15 COLON-ALIGNED WIDGET-ID 24
     lote-minimo AT ROW 4.38 COL 15 COLON-ALIGNED WIDGET-ID 26
     lote-economi AT ROW 5.38 COL 15.14 COLON-ALIGNED WIDGET-ID 28
     periodo-fixo AT ROW 6.42 COL 15 COLON-ALIGNED WIDGET-ID 30
     res-cq-comp AT ROW 8.96 COL 15 COLON-ALIGNED WIDGET-ID 52
     res-for-comp AT ROW 9.96 COL 15 COLON-ALIGNED WIDGET-ID 54
     res-int-comp AT ROW 10.96 COL 15 COLON-ALIGNED WIDGET-ID 56
     Tipo-est-seg AT ROW 1.79 COL 43 NO-LABEL WIDGET-ID 20
     quant-segur AT ROW 1.79 COL 54 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     tempo-segur AT ROW 2.79 COL 54 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     tb-conv-tempo-seg AT ROW 4.46 COL 43 WIDGET-ID 38
     cb-reab-estoq AT ROW 5.29 COL 54 COLON-ALIGNED WIDGET-ID 34
     cb-classe-repro AT ROW 6.63 COL 52 COLON-ALIGNED WIDGET-ID 44
     cb-emissao-ord AT ROW 7.63 COL 52 COLON-ALIGNED WIDGET-ID 46
     cb-contr-plan AT ROW 8.63 COL 52 COLON-ALIGNED WIDGET-ID 48
     cb-div-ordem AT ROW 9.63 COL 52 COLON-ALIGNED WIDGET-ID 50
     cb-ind-lista-mrp AT ROW 7.5 COL 15 COLON-ALIGNED WIDGET-ID 62
     cd-planejado AT ROW 10.75 COL 39 COLON-ALIGNED WIDGET-ID 64
     nr-linha AT ROW 10.75 COL 66 COLON-ALIGNED HELP
          "Numero da Linha de Produ��o" WIDGET-ID 66
     " Itens Comprados" VIEW-AS TEXT
          SIZE 12.86 BY .54 AT ROW 8.33 COL 3.14 WIDGET-ID 60
     " Tipo Estoque Seguran�a" VIEW-AS TEXT
          SIZE 20 BY .63 AT ROW 1.13 COL 43 WIDGET-ID 42
          FONT 1
     " Estoque Seguran�a" VIEW-AS TEXT
          SIZE 15 BY .63 AT ROW 3.88 COL 43 WIDGET-ID 40
          FONT 1
     rt-temp-qt-seg AT ROW 1.38 COL 42 WIDGET-ID 16
     RECT-33 AT ROW 4.13 COL 42 WIDGET-ID 36
     RECT-34 AT ROW 8.71 COL 2 WIDGET-ID 58
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3.86 ROW 2.88
         SIZE 75 BY 11.38
         FONT 1 WIDGET-ID 100.

DEFINE FRAME f-pg-sel
     rs-importa AT ROW 1.71 COL 18.29 NO-LABEL WIDGET-ID 52
     it-codigo-ini AT ROW 3.21 COL 21.57 COLON-ALIGNED WIDGET-ID 8
     it-codigo-fim AT ROW 3.21 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 22
     fm-codigo-ini AT ROW 4.21 COL 21.57 COLON-ALIGNED WIDGET-ID 74
     fm-codigo-fim AT ROW 4.21 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     fm-cod-com-ini AT ROW 5.21 COL 21.57 COLON-ALIGNED WIDGET-ID 4
     fm-cod-com-fim AT ROW 5.21 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     cod-estabel-ini AT ROW 6.21 COL 21.57 COLON-ALIGNED WIDGET-ID 2
     cod-estabel-fim AT ROW 6.21 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 16
     cd-planejado-ini AT ROW 7.21 COL 21.57 COLON-ALIGNED WIDGET-ID 10
     cd-planejado-fim AT ROW 7.21 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     nr-linha-ini AT ROW 8.21 COL 21.57 COLON-ALIGNED WIDGET-ID 12
     nr-linha-fim AT ROW 8.21 COL 51 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     tg-simulacao AT ROW 9.21 COL 23.57 WIDGET-ID 50
     bt-arquivo-2 AT ROW 10.71 COL 67.57 HELP
          "Escolha do nome do arquivo" WIDGET-ID 58
     c-arquivo-importacao AT ROW 10.75 COL 22.57 COLON-ALIGNED WIDGET-ID 68
     "Tipo Atualiza��o:" VIEW-AS TEXT
          SIZE 12 BY .54 AT ROW 1.79 COL 6.29 WIDGET-ID 72
     IMAGE-3 AT ROW 3.25 COL 42.57 WIDGET-ID 26
     IMAGE-4 AT ROW 3.21 COL 48.72 WIDGET-ID 28
     IMAGE-5 AT ROW 4.25 COL 42.57 WIDGET-ID 30
     IMAGE-6 AT ROW 4.21 COL 48.72 WIDGET-ID 32
     IMAGE-7 AT ROW 5.25 COL 42.57 WIDGET-ID 34
     IMAGE-8 AT ROW 5.21 COL 48.72 WIDGET-ID 36
     IMAGE-9 AT ROW 6.25 COL 42.57 WIDGET-ID 38
     IMAGE-10 AT ROW 6.21 COL 48.72 WIDGET-ID 40
     IMAGE-11 AT ROW 7.25 COL 42.57 WIDGET-ID 42
     IMAGE-12 AT ROW 7.21 COL 48.72 WIDGET-ID 44
     IMAGE-13 AT ROW 8.25 COL 42.57 WIDGET-ID 46
     IMAGE-14 AT ROW 8.21 COL 48.72 WIDGET-ID 48
     RECT-35 AT ROW 1.17 COL 2.57 WIDGET-ID 62
     RECT-36 AT ROW 2.96 COL 3.57 WIDGET-ID 64
     RECT-37 AT ROW 10.46 COL 3.57 WIDGET-ID 66
     RECT-38 AT ROW 1.5 COL 3.72 WIDGET-ID 70
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3.86 ROW 3
         SIZE 75 BY 11.25
         FONT 1 WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: w-relat
   Allow: Basic,Browse,DB-Fields,Window,Query
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-relat ASSIGN
         HIDDEN             = YES
         TITLE              = "<Title>"
         HEIGHT             = 15.75
         WIDTH              = 81.14
         MAX-HEIGHT         = 22.33
         MAX-WIDTH          = 114.29
         VIRTUAL-HEIGHT     = 22.33
         VIRTUAL-WIDTH      = 114.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-relat 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-relat.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-relat
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME f-pg-par:FRAME = FRAME f-relat:HANDLE
       FRAME f-pg-sel:FRAME = FRAME f-relat:HANDLE.

/* SETTINGS FOR FRAME f-pg-imp
   FRAME-NAME                                                           */
/* SETTINGS FOR FILL-IN text-destino IN FRAME f-pg-imp
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
ASSIGN 
       text-destino:PRIVATE-DATA IN FRAME f-pg-imp     = 
                "Destino".

/* SETTINGS FOR FILL-IN text-modo IN FRAME f-pg-imp
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       text-modo:PRIVATE-DATA IN FRAME f-pg-imp     = 
                "Execu��o".

/* SETTINGS FOR FRAME f-pg-par
   Custom                                                               */
/* SETTINGS FOR COMBO-BOX cb-classe-repro IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR COMBO-BOX cb-contr-plan IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR COMBO-BOX cb-demanda IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR COMBO-BOX cb-div-ordem IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR COMBO-BOX cb-emissao-ord IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR COMBO-BOX cb-ind-lista-mrp IN FRAME f-pg-par
   2                                                                    */
ASSIGN 
       cb-ind-lista-mrp:HIDDEN IN FRAME f-pg-par           = TRUE.

/* SETTINGS FOR COMBO-BOX cb-politica IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR COMBO-BOX cb-reab-estoq IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FILL-IN lote-economi IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FILL-IN lote-minimo IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FILL-IN lote-multipl IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FILL-IN periodo-fixo IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FILL-IN quant-segur IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR TOGGLE-BOX tb-conv-tempo-seg IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FILL-IN tempo-segur IN FRAME f-pg-par
   NO-ENABLE 2 3                                                        */
/* SETTINGS FOR RADIO-SET Tipo-est-seg IN FRAME f-pg-par
   2 3                                                                  */
/* SETTINGS FOR FRAME f-pg-sel
                                                                        */
/* SETTINGS FOR BUTTON bt-arquivo-2 IN FRAME f-pg-sel
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-arquivo-importacao IN FRAME f-pg-sel
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME f-relat
                                                                        */
/* SETTINGS FOR RECTANGLE RECT-1 IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE RECT-6 IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder-left IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder-right IN FRAME f-relat
   NO-ENABLE                                                            */
/* SETTINGS FOR RECTANGLE rt-folder-top IN FRAME f-relat
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-relat)
THEN w-relat:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-pg-imp
/* Query rebuild information for FRAME f-pg-imp
     _Query            is NOT OPENED
*/  /* FRAME f-pg-imp */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-relat
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-relat w-relat
ON END-ERROR OF w-relat /* <Title> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
   RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-relat w-relat
ON WINDOW-CLOSE OF w-relat /* <Title> */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-ajuda
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ajuda w-relat
ON CHOOSE OF bt-ajuda IN FRAME f-relat /* Ajuda */
DO:
   {include/ajuda.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME bt-arquivo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arquivo w-relat
ON CHOOSE OF bt-arquivo IN FRAME f-pg-imp
DO:
    /*****************************************************************
    **
    ** I-RPARQ - Choose of bt-Arquivo no template de relat�rio
    **
    *****************************************************************/
    
        def var c-arq-conv  as char no-undo.
        assign c-arq-conv = replace(input frame f-pg-imp c-arquivo, "/", CHR(92)).
        SYSTEM-DIALOG GET-FILE c-arq-conv
           FILTERS "*.csv" "*.csv",
                   "*.*" "*.*"
           ASK-OVERWRITE 
           DEFAULT-EXTENSION "csv"
           INITIAL-DIR "spool" 
           SAVE-AS
           USE-FILENAME
           UPDATE l-ok.
    
    
    if  l-ok = yes then do:
        assign c-arquivo = replace(c-arq-conv, CHR(92), "/"). 
        display c-arquivo with frame f-pg-imp.
    end.
    
    /* i-rparq */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-sel
&Scoped-define SELF-NAME bt-arquivo-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-arquivo-2 w-relat
ON CHOOSE OF bt-arquivo-2 IN FRAME f-pg-sel
DO:
    /*****************************************************************
    **
    ** I-RPARQ - Choose of bt-Arquivo no template de relat�rio
    **
    *****************************************************************/
    
        def var c-arq-conv  as char no-undo.
        assign c-arq-conv = replace(input frame f-pg-sel c-arquivo-importacao, "/", CHR(92)).
        SYSTEM-DIALOG GET-FILE c-arq-conv
           FILTERS "*.csv" "*.csv",
                   "*.*" "*.*"
           ASK-OVERWRITE 
           DEFAULT-EXTENSION "csv"
           INITIAL-DIR "spool" 
           USE-FILENAME
           UPDATE l-ok.
    
    
    if  l-ok = yes then do:
        assign c-arquivo-importacao = replace(c-arq-conv, CHR(92), "/"). 
        display c-arquivo-importacao with frame f-pg-sel.
    end.
    
    /* i-rparq */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancelar w-relat
ON CHOOSE OF bt-cancelar IN FRAME f-relat /* Fechar */
DO:
   apply "close" to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME bt-config-impr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-config-impr w-relat
ON CHOOSE OF bt-config-impr IN FRAME f-pg-imp
DO:
   {include/i-rpimp.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME bt-executar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-executar w-relat
ON CHOOSE OF bt-executar IN FRAME f-relat /* Executar */
DO:
   do  on error undo, return no-apply:
       run pi-executar.
   end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-par
&Scoped-define SELF-NAME cb-ind-lista-mrp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cb-ind-lista-mrp w-relat
ON VALUE-CHANGED OF cb-ind-lista-mrp IN FRAME f-pg-par /* Processo MRP */
DO:
    run notify ('pi-atualiza,state-source').
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cb-politica
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cb-politica w-relat
ON VALUE-CHANGED OF cb-politica IN FRAME f-pg-par /* Politica */
DO:
    if  {ininc/i04in122.i 06 cb-politica:screen-value in frame f-pg-par} = 1 then do  with frame f-pg-par:
        enable  periodo-fixo .
    end.
    else do  with frame f-pg-par:
        disable periodo-fixo.
    end.        

    if  {ininc/i04in122.i 06 cb-politica:screen-value in frame f-pg-par} = 2 then do:
        enable lote-minimo 
               with frame f-pg-par.
    end.
    else do with frame f-pg-par:
        disable lote-economi.
    end.

    &IF DEFINED (bf_man_ponto_repos) &THEN  
        if  {ininc/i04in122.i 06 cb-politica:screen-value in frame f-pg-par} = 7 then 
            enable  c-pto-repos 
            lote-economi with frame f-pg-par .
        else  
        disable c-pto-repos  with frame f-pg-par.

     if  {ininc/i04in122.i 06 cb-politica:screen-value in frame f-pg-par} = 7 then  
        assign c-pto-repos:sensitive in frame f-pg-par = yes
               lote-economi:sensitive in frame f-pg-par = yes.
    else
        assign c-pto-repos:sensitive in frame f-pg-par = no.
    &ENDIF

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cd-planejado
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cd-planejado w-relat
ON F5 OF cd-planejado IN FRAME f-pg-par /* Planejador */
DO:
  {include/zoomvar.i &prog-zoom="inzoom/z01in321.w"
       &campo=cd-planejado
       &campozoom=cd-planejado}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cd-planejado w-relat
ON MOUSE-SELECT-DBLCLICK OF cd-planejado IN FRAME f-pg-par /* Planejador */
DO:
  APPLY 'f5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-relat
&Scoped-define SELF-NAME im-pg-imp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL im-pg-imp w-relat
ON MOUSE-SELECT-CLICK OF im-pg-imp IN FRAME f-relat
DO:
    run pi-troca-pagina.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME im-pg-par
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL im-pg-par w-relat
ON MOUSE-SELECT-CLICK OF im-pg-par IN FRAME f-relat
DO:
    run pi-troca-pagina.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME im-pg-sel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL im-pg-sel w-relat
ON MOUSE-SELECT-CLICK OF im-pg-sel IN FRAME f-relat
DO:
    run pi-troca-pagina.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-par
&Scoped-define SELF-NAME nr-linha
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL nr-linha w-relat
ON F5 OF nr-linha IN FRAME f-pg-par /* Linha Produ��o */
DO:
  {include/zoomvar.i &prog-zoom="inzoom/z01in186.w"
       &campo=nr-linha
       &campozoom=nr-linha}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL nr-linha w-relat
ON MOUSE-SELECT-DBLCLICK OF nr-linha IN FRAME f-pg-par /* Linha Produ��o */
DO:
  APPLY 'f5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&Scoped-define SELF-NAME rs-destino
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-destino w-relat
ON VALUE-CHANGED OF rs-destino IN FRAME f-pg-imp
DO:
do  with frame f-pg-imp:
    case self:screen-value:
        when "1" then do:
            assign c-arquivo:sensitive    = no
                   bt-arquivo:visible     = no
                   bt-config-impr:visible = yes.
        end.
        when "2" then do:
            assign c-arquivo:sensitive     = yes
                   bt-arquivo:visible      = yes
                   bt-config-impr:visible  = no.
        end.
        when "3" then do:
            assign c-arquivo:sensitive     = no
                   bt-arquivo:visible      = no
                   bt-config-impr:visible  = no.
        end.
    end case.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs-destino-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-destino-2 w-relat
ON VALUE-CHANGED OF rs-destino-2 IN FRAME f-pg-imp
DO:
do  with frame f-pg-imp:
/*     case self:screen-value:                      */
/*         when "1" then do:                        */
/*             assign c-arquivo:sensitive    = no   */
/*                    bt-arquivo:visible     = no   */
/*                    bt-config-impr:visible = yes. */
/*         end.                                     */
/*         when "2" then do:                        */
/*             assign c-arquivo:sensitive     = yes */
/*                    bt-arquivo:visible      = yes */
/*                    bt-config-impr:visible  = no. */
/*         end.                                     */
/*         when "3" then do:                        */
/*             assign c-arquivo:sensitive     = no  */
/*                    bt-arquivo:visible      = no  */
/*                    bt-config-impr:visible  = no. */
/*         end.                                     */
/*     end case.                                    */
    CASE rs-destino-2:SCREEN-VALUE IN FRAME f-pg-imp:
            WHEN "1" THEN DO:

                /* Alterado em 31/05/2005 - tech1139 - Altera��es FO 1152.814*/
                &IF "{&RTF}":U = "YES":U &THEN
                IF l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp <> "No" THEN DO:
                   ASSIGN l-rtf = YES
                          c-modelo-aux = c-modelo-rtf:SCREEN-VALUE IN FRAME f-pg-imp.
                END.
                &endif
                /* Alterado em 31/05/2005 - tech1139 - Altera��es FO 1152.814*/

                ASSIGN c-arquivo                                = c-imp-old
                       c-arquivo:SCREEN-VALUE IN FRAME f-pg-imp = c-imp-old
                       c-arquivo:VISIBLE IN FRAME f-pg-imp    = NO
                       bt-arquivo:VISIBLE IN FRAME f-pg-imp     = NO
                       bt-config-impr:VISIBLE IN FRAME f-pg-imp = NO
                /*Alterado 17/02/2005 - tech1007 - Realizado teste de preprocessador para
                verificar se o RTF est� ativo*/
                       &IF "{&RTF}":U = "YES":U &THEN
                       l-habilitaRtf:sensitive  = NO
                       l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp = "No"
                       l-habilitaRtf = NO
                       &endif
                /*Fim alteracao 17/02/2005*/
                       .
                if c-imp-old = "" then
                   run pi-impres-pad.
            END.
            WHEN "2" THEN DO:
                ASSIGN c-arquivo = IF rs-execucao = 1 
                                   THEN c-arq-old
                                   ELSE c-arq-old-batch
                       c-arquivo:SCREEN-VALUE IN FRAME f-pg-imp = c-arquivo
                       c-arquivo:SENSITIVE IN FRAME f-pg-imp    = YES
                       c-arquivo:VISIBLE IN FRAME f-pg-imp      = YES
                       bt-arquivo:VISIBLE IN FRAME f-pg-imp     = YES
                       bt-config-impr:VISIBLE IN FRAME f-pg-imp = NO
                       /*Alterado 17/02/2005 - tech1007 - Realizado teste de preprocessador para
                         verificar se o RTF est� ativo*/
                       &IF "{&RTF}":U = "YES":U &THEN
                       l-habilitaRtf:sensitive  = YES
                       &endif
                       /*Fim alteracao 17/02/2005*/
                       .

                /* Alterado em 31/05/2005 - tech1139 - Altera��es FO 1152.814*/
                &IF "{&RTF}":U = "YES":U &THEN
                IF l-rtf = YES THEN DO:
                   ASSIGN l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp = "Yes"
                          l-rtf = NO
                          c-modelo-rtf = c-modelo-aux
                          c-modelo-rtf:SCREEN-VALUE IN FRAME f-pg-imp = c-modelo-aux.
                END.
                &endif
                /* Alterado em 31/05/2005 - tech1139 - Altera��es FO 1152.814*/
                
            END.
            WHEN "3" THEN DO:
                ASSIGN c-arquivo                                = ""
                       c-arquivo:SCREEN-VALUE IN FRAME f-pg-imp = c-arquivo
                       c-arquivo:VISIBLE IN FRAME f-pg-imp      = NO
                       bt-arquivo:VISIBLE IN FRAME f-pg-imp     = NO                       
                       bt-config-impr:VISIBLE IN FRAME f-pg-imp = NO
                       /*Alterado 17/02/2005 - tech1007 - Realizado teste de preprocessador para
                         verificar se o RTF est� ativo*/
                       &IF "{&RTF}":U = "YES":U &THEN
                       l-habilitaRtf:sensitive  = YES
                       &endif
                       /*Fim alteracao 17/02/2005*/
                       .
                /*Alterado 15/02/2005 - tech1007 - Teste para funcionar corretamente no WebEnabler*/
                &IF "{&RTF}":U = "YES":U &THEN
                IF VALID-HANDLE(hWenController) THEN DO:
                    ASSIGN l-habilitaRtf:sensitive  = NO
                           l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp = "No"
                           l-habilitaRtf = NO.
                END.
                &endif
                /*Fim alteracao 15/02/2005*/                                        

                /* Alterado em 31/05/2005 - tech1139 - Altera��es FO 1152.814*/
                &IF "{&RTF}":U = "YES":U &THEN
                IF l-rtf = YES THEN DO:
                    ASSIGN l-habilitaRtf:SCREEN-VALUE IN FRAME f-pg-imp = "Yes"
                           l-rtf = NO
                           c-modelo-rtf = c-modelo-aux
                           c-modelo-rtf:SCREEN-VALUE IN FRAME f-pg-imp = c-modelo-aux.
                END.
                &endif
                /* Alterado em 31/05/2005 - tech1139 - Altera��es FO 1152.814*/

            END.
        END CASE.
end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME rs-execucao
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-execucao w-relat
ON VALUE-CHANGED OF rs-execucao IN FRAME f-pg-imp
DO:
   {include/i-rprse.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-sel
&Scoped-define SELF-NAME rs-importa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL rs-importa w-relat
ON VALUE-CHANGED OF rs-importa IN FRAME f-pg-sel
DO:
  IF INPUT FRAME f-pg-sel rs-importa = 1 THEN
  DO:
      ENABLE cd-planejado-fim cd-planejado-ini 
             fm-cod-com-fim fm-cod-com-ini 
             fm-codigo-fim fm-codigo-ini 
             nr-linha-fim nr-linha-ini
             cod-estabel-fim cod-estabel-ini 
             it-codigo-fim   it-codigo-ini 
             tg-simulacao    
          WITH FRAME f-pg-sel.
      DISABLE bt-arquivo-2 
              c-arquivo-importacao
              WITH FRAME f-pg-sel.

  END.
  ELSE DO:
      DISABLE cd-planejado-fim cd-planejado-ini 
             fm-cod-com-fim fm-cod-com-ini 
             fm-codigo-fim fm-codigo-ini 
             nr-linha-fim nr-linha-ini
             cod-estabel-fim cod-estabel-ini 
             it-codigo-fim   it-codigo-ini 
             tg-simulacao    
            WITH FRAME f-pg-sel.
      ENABLE bt-arquivo-2 
              c-arquivo-importacao
              WITH FRAME f-pg-sel.

  END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-par
&Scoped-define SELF-NAME Tipo-est-seg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tipo-est-seg w-relat
ON VALUE-CHANGED OF Tipo-est-seg IN FRAME f-pg-par
DO:
    if  input frame f-pg-par tipo-est-seg = 1 then do with frame f-pg-par:
        enable quant-segur.
        disable tempo-segur.
    end.
    else do with frame f-pg-par:
        enable tempo-segur.
        disable quant-segur.
    end.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME f-pg-imp
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-relat 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

{utp/ut9000.i "ESEN002" "2.06.00.000"}

/* inicializa��es do template de relat�rio */
{include/i-rpini.i}

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

{include/i-rplbl.i}
if cd-planejado:load-mouse-pointer ("image\lupa.cur") in frame f-pg-par then.
if nr-linha:load-mouse-pointer ("image\lupa.cur") in frame f-pg-par then.
/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO  ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
    ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

    RUN enable_UI.

    {include/i-rpmbl.i}
	
	RUN pi-retorna-diretorio 
		 (c-seg-usuario,
		 OUTPUT c-arq-old).

    ASSIGN c-arq-old = c-arq-old + "ESEN002.lst".
    
    ASSIGN rt-folder:HEIGHT IN FRAME f-relat = 12.00.
    IF  NOT THIS-PROCEDURE:PERSISTENT THEN
        WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-relat  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-relat  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-relat  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-relat)
  THEN DELETE WIDGET w-relat.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-relat  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE im-pg-imp im-pg-par im-pg-sel bt-executar bt-cancelar bt-ajuda 
      WITH FRAME f-relat IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-relat}
  DISPLAY cb-politica cb-demanda lote-multipl lote-minimo lote-economi 
          periodo-fixo res-cq-comp res-for-comp res-int-comp Tipo-est-seg 
          quant-segur tempo-segur tb-conv-tempo-seg cb-reab-estoq 
          cb-classe-repro cb-emissao-ord cb-contr-plan cb-div-ordem 
          cb-ind-lista-mrp cd-planejado nr-linha 
      WITH FRAME f-pg-par IN WINDOW w-relat.
  ENABLE cb-politica cb-demanda lote-multipl lote-minimo lote-economi 
         periodo-fixo res-cq-comp res-for-comp res-int-comp Tipo-est-seg 
         quant-segur tb-conv-tempo-seg cb-reab-estoq cb-classe-repro 
         cb-emissao-ord cb-contr-plan cb-div-ordem cb-ind-lista-mrp 
         rt-temp-qt-seg RECT-33 RECT-34 cd-planejado nr-linha 
      WITH FRAME f-pg-par IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-pg-par}
  DISPLAY rs-destino-2 rs-destino c-arquivo rs-execucao 
      WITH FRAME f-pg-imp IN WINDOW w-relat.
  ENABLE RECT-7 RECT-9 rs-destino-2 rs-destino bt-arquivo bt-config-impr 
         c-arquivo rs-execucao 
      WITH FRAME f-pg-imp IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-pg-imp}
  DISPLAY rs-importa it-codigo-ini it-codigo-fim fm-codigo-ini fm-codigo-fim 
          fm-cod-com-ini fm-cod-com-fim cod-estabel-ini cod-estabel-fim 
          cd-planejado-ini cd-planejado-fim nr-linha-ini nr-linha-fim 
          tg-simulacao c-arquivo-importacao 
      WITH FRAME f-pg-sel IN WINDOW w-relat.
  ENABLE IMAGE-3 IMAGE-4 IMAGE-5 IMAGE-6 IMAGE-7 IMAGE-8 IMAGE-9 IMAGE-10 
         IMAGE-11 IMAGE-12 IMAGE-13 IMAGE-14 RECT-35 RECT-36 RECT-37 RECT-38 
         rs-importa it-codigo-ini it-codigo-fim fm-codigo-ini fm-codigo-fim 
         fm-cod-com-ini fm-cod-com-fim cod-estabel-ini cod-estabel-fim 
         cd-planejado-ini cd-planejado-fim nr-linha-ini nr-linha-fim 
         tg-simulacao 
      WITH FRAME f-pg-sel IN WINDOW w-relat.
  {&OPEN-BROWSERS-IN-QUERY-f-pg-sel}
  VIEW w-relat.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-relat 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-executar w-relat 
PROCEDURE pi-executar :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define var r-tt-digita as rowid no-undo.
DEF VAR c-diretorio AS CHAR NO-UNDO.

DEF VAR chExcelApplication  AS office.iface.excel.ExcelWrapper NO-UNDO.
DEF VAR chWorkbook          AS office.iface.excel.Workbook NO-UNDO.
DEF VAR chWorkSheet         AS office.iface.excel.WorkSheet NO-UNDO.

do on error undo, return error on stop  undo, return error:
    {include/i-rpexa.i}
    
    if input frame f-pg-imp rs-destino = 2 and
       input frame f-pg-imp rs-execucao = 1 then do:
        run utp/ut-vlarq.p (input input frame f-pg-imp c-arquivo).
        
        if return-value = "NOK":U then do:
            run utp/ut-msgs.p (input "show", input 73, input "").
            
            apply "MOUSE-SELECT-CLICK":U to im-pg-imp in frame f-relat.
            apply "ENTRY":U to c-arquivo in frame f-pg-imp.
            return error.
        end.
    end.
    
    /* Coloque aqui as valida��es das outras p�ginas, lembrando que elas devem 
       apresentar uma mensagem de erro cadastrada, posicionar na p�gina com 
       problemas e colocar o focus no campo com problemas */
    if input frame f-pg-sel it-codigo-fim  <
       input frame f-pg-sel it-codigo-ini   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Item~~" + 
                           "Selecao inicial deve ser menor ou igual a selecao final").
        return "adm-error":U.
    END.

    if input frame f-pg-sel fm-codigo-fim  <
       input frame f-pg-sel fm-codigo-ini   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Familia Material~~" + 
                           "Selecao inicial deve ser menor ou igual a selecao final").
        return "adm-error":U.
    END.

    if input frame f-pg-sel fm-cod-com-fim  <
       input frame f-pg-sel fm-cod-com-ini   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Familia Comercial~~" + 
                           "Selecao inicial deve ser menor ou igual a selecao final").
        return "adm-error":U.
    end.
    if input frame f-pg-sel cod-estabel-fim  <
       input frame f-pg-sel cod-estabel-ini   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Estabelecimento~~" + 
                           "Selecao inicial deve ser menor ou igual a selecao final").
        return "adm-error":U.
    end.

    if input frame f-pg-sel cd-planejado-fim <
       input frame f-pg-sel cd-planejado-ini   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Planejador~~" + 
                           "Selecao inicial deve ser menor ou igual a selecao final").
        return "adm-error":U.
    end.
    
    if input frame f-pg-sel nr-linha-fim  <
       input frame f-pg-sel nr-linha-ini   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Linha Produ��o~~" + 
                           "Selecao inicial deve ser menor ou igual a selecao final").
        return "adm-error":U.
    END.
    
    if input frame f-pg-par cb-classe-repro = "" then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Classe Reprograma��o~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.
    if input frame f-pg-par cb-contr-plan = "" then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Controle Planejamento~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.

    if input frame f-pg-par cb-demanda = "" then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Demanda~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.
    if input frame f-pg-par cb-div-ordem = "" then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Divis�o Ordens~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.
    if input frame f-pg-par cb-emissao-ord = ""   then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Emiss�o Ordem~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.

    if input frame f-pg-par cb-politica = "" then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Politica~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.

    if input frame f-pg-par cb-reab-estoq = ""  then do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Reabastecimento~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.
    IF INPUT FRAME F-PG-PAR cb-ind-lista-mrp = "" THEN do:
        run utp/ut-msgs.p (input "show":U,
                           input 17006,
                           input "Processo MRP~~" + 
                           "Nenhuma op��o foi selecionada!").
        return "adm-error":U.
    end.
        

    
    
    /* Aqui s�o gravados os campos da temp-table que ser� passada como par�metro
       para o programa RP.P */
    
    create tt-param.
    assign tt-param.usuario         = c-seg-usuario
           tt-param.destino         = input frame f-pg-imp rs-destino
           tt-param.data-exec       = today
           tt-param.hora-exec       = time
           /*tt-param.classifica      = input frame f-pg-cla rs-classif
           tt-param.desc-classifica = entry((tt-param.classifica - 1) * 2 + 1, 
                                            rs-classif:radio-buttons in frame f-pg-cla)*/.
    
    if tt-param.destino = 1 
    then assign tt-param.arquivo = "".
    else if  tt-param.destino = 2 
         then assign tt-param.arquivo = input frame f-pg-imp c-arquivo.
         else assign tt-param.arquivo = session:temp-directory + "esen002.tmp".
		 
     RUN pi-retorna-diretorio 
          (tt-param.usuario,
          OUTPUT c-diretorio).

	if tt-param.destino = 3 then do:
			 
		ASSIGN
            tt-param.arquivo = c-diretorio + "esen002.tmp".
			 
	end.		 

    /* Coloque aqui a l�gica de grava��o dos demais campos que devem ser passados
       como par�metros para o programa RP.P, atrav�s da temp-table tt-param */
    ASSIGN tt-param.it-codigo-ini       = INPUT FRAME f-pg-SEL it-codigo-ini       
           tt-param.it-codigo-fim       = INPUT FRAME f-pg-SEL it-codigo-fim      
           tt-param.fm-codigo-ini       = INPUT FRAME f-pg-SEL fm-codigo-ini      
           tt-param.fm-codigo-fim       = INPUT FRAME f-pg-SEL fm-codigo-fim      
           tt-param.it-codigo-ini       = INPUT FRAME f-pg-SEL it-codigo-ini        
           tt-param.it-codigo-fim       = INPUT FRAME f-pg-SEL it-codigo-fim        
           tt-param.fm-cod-com-ini      = INPUT FRAME f-pg-SEL fm-cod-com-ini       
           tt-param.fm-cod-com-fim      = INPUT FRAME f-pg-SEL fm-cod-com-fim       
           tt-param.cod-estabel-ini     = INPUT FRAME f-pg-SEL cod-estabel-ini      
           tt-param.cod-estabel-fim     = INPUT FRAME f-pg-SEL cod-estabel-fim      
           tt-param.cd-planejado-ini    = INPUT FRAME f-pg-SEL cd-planejado-ini     
           tt-param.cd-planejado-fim    = INPUT FRAME f-pg-SEL cd-planejado-fim     
           tt-param.nr-linha-ini        = INPUT FRAME f-pg-SEL nr-linha-ini         
           tt-param.nr-linha-fim        = INPUT FRAME f-pg-SEL nr-linha-fim         
           tt-param.cb-classe-repro     = INPUT FRAME f-pg-PAR cb-classe-repro     
           tt-param.cb-contr-plan       = INPUT FRAME f-pg-PAR cb-contr-plan       
           tt-param.cb-demanda          = INPUT FRAME f-pg-PAR cb-demanda          
           tt-param.cb-div-ordem        = INPUT FRAME f-pg-PAR cb-div-ordem        
           tt-param.cb-emissao-ord      = INPUT FRAME f-pg-PAR cb-emissao-ord      
           tt-param.cb-politica         = INPUT FRAME f-pg-PAR cb-politica         
           tt-param.cb-reab-estoq       = INPUT FRAME f-pg-PAR cb-reab-estoq       
           tt-param.lote-economi        = INPUT FRAME f-pg-PAR lote-economi 
           tt-param.lote-minimo         = INPUT FRAME f-pg-PAR lote-minimo 
           tt-param.lote-multipl        = INPUT FRAME f-pg-PAR lote-multipl 
           tt-param.periodo-fixo        = INPUT FRAME f-pg-PAR periodo-fixo  
           tt-param.quant-segur         = INPUT FRAME f-pg-PAR quant-segur 
           tt-param.res-cq-comp         = INPUT FRAME f-pg-PAR res-cq-comp 
           tt-param.res-for-comp        = INPUT FRAME f-pg-PAR res-for-comp  
           tt-param.res-int-comp        = INPUT FRAME f-pg-PAR res-int-comp  
           tt-param.tempo-segur         = INPUT FRAME f-pg-PAR tempo-segur  
           tt-param.tipo-est-seg        = INPUT FRAME f-pg-PAR tipo-est-seg                 
           tt-param.tb-conv-tempo-seg   = INPUT FRAME f-pg-PAR tb-conv-tempo-seg   
           tt-param.tg-simulacao        = INPUT FRAME f-pg-SEL tg-simulacao 
           tt-param.cb-ind-lista-mrp    = INPUT FRAME f-pg-par cb-ind-lista-mrp
           tt-param.arquivo-importacao  = INPUT FRAME f-pg-SEL C-arquivo-importacao
           tt-param.rs-importa          = INPUT FRAME f-pg-SEL rs-importa
           tt-param.cd-planejado        = INPUT FRAME f-pg-PAR cd-planejado  
           tt-param.nr-linha            = INPUT FRAME f-pg-PAR nr-linha     
           tt-param.arquivo-exportacao  = c-diretorio + "esen002.csv"
           .       
    
    
    /* Executar do programa RP.P que ir� criar o relat�rio */
    {include/i-rpexb.i}
    
    SESSION:SET-WAIT-STATE("general":U).

    
    {include/i-rprun.i esp/esen002rp.p}
    
    {include/i-rpexc.i}
    
    SESSION:SET-WAIT-STATE("":U).
    
    DO:
        {office/office.i Excel chExcelApplication}

        IF tt-param.rs-importa = 1 THEN
            chWorkbook = chExcelApplication:Workbooks:OPEN(tt-param.arquivo-exportacao ).
        ELSE
            chWorkbook = chExcelApplication:Workbooks:OPEN(tt-param.arquivo-importacao ).

        chWorkSheet = chExcelApplication:Sheets:Item(1).
        chExcelApplication:visible = true.
        DELETE OBJECT chWorkSheet.
        DELETE OBJECT chWorkbook. /* Destroi objeto criado liberando a mem�ria */
        DELETE OBJECT chExcelApplication. /* Destroi objeto criado liberando a mem�ria */


    END.
/*     {include/i-rptrm.i} */
end.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-troca-pagina w-relat 
PROCEDURE pi-troca-pagina :
/*------------------------------------------------------------------------------
  Purpose: Gerencia a Troca de P�gina (folder)   
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

{include/i-rptrp.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-relat  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this w-relat, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-relat 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

