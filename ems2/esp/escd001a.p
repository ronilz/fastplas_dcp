/******************************************************************************
*      Programa .....: ESCD001A                                               *
*      Data .........: AGO/2014                                               *
*      Empresa ......: TOTVS                                                  *
*      Cliente ......: FASTPLAS                                               *
*      Objetivo .....: Eliminacao de itens                                    *
*******************************************************************************
*      VERSAO       DATA        RESPONSAVEL      MOTIVO                       *
*      2.06.00.000  AGO/2014    LEANDRO LABBATE  DESENVOLVIMENTO              *
******************************************************************************/

OUTPUT TO value(session:temp-directory + "log.txt").
OUTPUT close.

DEF VAR i-lidos AS INT NO-UNDO.
DEF VAR i-reg   AS INT NO-UNDO.
DEF VAR i-cont  AS INT NO-UNDO.

DEF VAR h-acomp AS HANDLE NO-UNDO.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
RUN pi-inicializar IN h-acomp (INPUT "Elimina Itens...").

DISABLE TRIGGERS FOR LOAD OF mgdbr.bmg-ord-mts.
DISABLE TRIGGERS FOR LOAD OF mgdbr.bmg-sdo-estoq-mts.
DISABLE TRIGGERS FOR LOAD OF mgdbr.ctrab-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.estab-solic-item.
DISABLE TRIGGERS FOR LOAD OF mgdbr.estrut-item-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.item-cenar.
DISABLE TRIGGERS FOR LOAD OF mgdbr.item-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.item-estab-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.op-ord-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.operac-engr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.ord-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.pdven-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.ped-alter-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.ped-alter-histor-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.pert-ord-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.proces-item-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.recur-sec-operac-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.rede-op-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.ref-it-estrut.
DISABLE TRIGGERS FOR LOAD OF mgdbr.refer-item-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.res-ord-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.rot-item-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.sdo-estoq-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.sdo-terc-dbr.
DISABLE TRIGGERS FOR LOAD OF movdbr.bmg-apont-causa.
DISABLE TRIGGERS FOR LOAD OF movdbr.bmg-estatis-regiao.
DISABLE TRIGGERS FOR LOAD OF movdbr.bmg-necessidades.
DISABLE TRIGGERS FOR LOAD OF movdbr.bmg-programacao.
DISABLE TRIGGERS FOR LOAD OF movdbr.demand-cenar.
DISABLE TRIGGERS FOR LOAD OF movdbr.ord-cenar.
DISABLE TRIGGERS FOR LOAD OF movdbr.ord-histor.
DISABLE TRIGGERS FOR LOAD OF movdbr.ped-demand-cenar.
DISABLE TRIGGERS FOR LOAD OF movdbr.programac-recebto-estab.
DISABLE TRIGGERS FOR LOAD OF movdbr.sdo-item-cenar.
DISABLE TRIGGERS FOR LOAD OF movdbr.solicit-produc-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.agrup-reservas.
DISABLE TRIGGERS FOR LOAD OF mgcad.al-pr-op.
DISABLE TRIGGERS FOR LOAD OF mgcad.al-res-oc.
DISABLE TRIGGERS FOR LOAD OF mgcad.al-res-op.
DISABLE TRIGGERS FOR LOAD OF mgcad.alt-comp-tar.
DISABLE TRIGGERS FOR LOAD OF mgcad.alt-reci.
DISABLE TRIGGERS FOR LOAD OF mgcad.alternativo.
DISABLE TRIGGERS FOR LOAD OF mgcad.aprov-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.area-produc-prefer.
DISABLE TRIGGERS FOR LOAD OF mgcad.base-mensal.
DISABLE TRIGGERS FOR LOAD OF mgcad.base-mensal1.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-etiqueta.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item-emit.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item-emit.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item-emit.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item-emit.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-item-emit.
DISABLE TRIGGERS FOR LOAD OF mgcad.bc-ext-saldo-estoq.
DISABLE TRIGGERS FOR LOAD OF mgcad.canal-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.cap-turno.
DISABLE TRIGGERS FOR LOAD OF mgcad.carga-maq.
DISABLE TRIGGERS FOR LOAD OF mgcad.carga-turno.
DISABLE TRIGGERS FOR LOAD OF mgcad.carga-turno-pl.
DISABLE TRIGGERS FOR LOAD OF mgcad.carga-x-ref.
DISABLE TRIGGERS FOR LOAD OF mgcad.cat83-item-natur-operac.
DISABLE TRIGGERS FOR LOAD OF mgcad.comissao.
DISABLE TRIGGERS FOR LOAD OF mgcad.comp-altern.
DISABLE TRIGGERS FOR LOAD OF mgcad.comp-equipto.
DISABLE TRIGGERS FOR LOAD OF mgcad.comp-solic.
DISABLE TRIGGERS FOR LOAD OF mgcad.componente-cex.
DISABLE TRIGGERS FOR LOAD OF mgcad.compr-fabric.
DISABLE TRIGGERS FOR LOAD OF mgcad.conf-restr.
DISABLE TRIGGERS FOR LOAD OF mgcad.conta-ft.
DISABLE TRIGGERS FOR LOAD OF mgcad.contab-ge.
DISABLE TRIGGERS FOR LOAD OF mgcad.conv-man-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.cota-item-reaj.
DISABLE TRIGGERS FOR LOAD OF mgcad.cotac-cond-pagto.
DISABLE TRIGGERS FOR LOAD OF mgcad.cotacao-inv.
DISABLE TRIGGERS FOR LOAD OF mgcad.cotacao-item-cex.
DISABLE TRIGGERS FOR LOAD OF mgcad.criter-orig.
DISABLE TRIGGERS FOR LOAD OF mgcad.crp-oper.
DISABLE TRIGGERS FOR LOAD OF mgcad.crp-oper-ant.
DISABLE TRIGGERS FOR LOAD OF mgcad.crp-ord-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.ctrab-operac.
DISABLE TRIGGERS FOR LOAD OF mgcad.ctrab-operac-aloc.
DISABLE TRIGGERS FOR LOAD OF mgcad.custo-data.
DISABLE TRIGGERS FOR LOAD OF mgcad.delta-x-ref.
DISABLE TRIGGERS FOR LOAD OF mgcad.depen-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.depen-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.desconto.
DISABLE TRIGGERS FOR LOAD OF mgcad.desenho-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.Desp-cotacao-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.desp-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.Desp-item-fornec.
DISABLE TRIGGERS FOR LOAD OF mgcad.Desp-item-fornec-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.diario-tmp.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.EB-embalagem.
DISABLE TRIGGERS FOR LOAD OF mgcad.EB-item-embal.
DISABLE TRIGGERS FOR LOAD OF mgcad.EB-rest-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.EB-rest-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.embalag.
DISABLE TRIGGERS FOR LOAD OF mgcad.eq-item-embal-embarq.
DISABLE TRIGGERS FOR LOAD OF mgcad.eq-item-embarq.
DISABLE TRIGGERS FOR LOAD OF mgcad.eq-item-grp-embarq.
DISABLE TRIGGERS FOR LOAD OF mgcad.est-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.est-prod-coml.
DISABLE TRIGGERS FOR LOAD OF mgcad.est-ref-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.estim-mat.
DISABLE TRIGGERS FOR LOAD OF mgcad.estr-conf.
DISABLE TRIGGERS FOR LOAD OF mgcad.estr-mod-cf.
DISABLE TRIGGERS FOR LOAD OF mgcad.estr-mod-for.
DISABLE TRIGGERS FOR LOAD OF mgcad.estr-mod-reg.
DISABLE TRIGGERS FOR LOAD OF mgcad.estr-modelo.
DISABLE TRIGGERS FOR LOAD OF mgcad.estrat-preco.
DISABLE TRIGGERS FOR LOAD OF mgcad.estrutura.
DISABLE TRIGGERS FOR LOAD OF mgcad.event-mod-contrat.
DISABLE TRIGGERS FOR LOAD OF mgcad.evento-inv.
DISABLE TRIGGERS FOR LOAD OF mgcad.exame-insp.
DISABLE TRIGGERS FOR LOAD OF mgcad.ext-ped-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.fat-graf.
DISABLE TRIGGERS FOR LOAD OF mgcad.ferr-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.ge-troca.
DISABLE TRIGGERS FOR LOAD OF mgcad.his-componente.
DISABLE TRIGGERS FOR LOAD OF mgcad.hist-preco.
DISABLE TRIGGERS FOR LOAD OF mgcad.histor-cust.
DISABLE TRIGGERS FOR LOAD OF mgcad.icms-it-uf.
DISABLE TRIGGERS FOR LOAD OF mgcad.impto-remito-fatur.
DISABLE TRIGGERS FOR LOAD OF mgcad.impto-remito-fatur-det.
DISABLE TRIGGERS FOR LOAD OF mgcad.inv-condpagto.
DISABLE TRIGGERS FOR LOAD OF mgcad.inv-log-exp.
DISABLE TRIGGERS FOR LOAD OF mgcad.inventario.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-carac-tec.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-carga-plan.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-comp-exame.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-contrato-cli.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-dep-fat.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-dep-remito.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-exame.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-markup.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-msg-carac.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-periodo.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-pre-emb.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-pre-fat.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-proc-siscomex.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-res-carac.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-rot-conf.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-ult-ent.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-ult-est.
DISABLE TRIGGERS FOR LOAD OF mgcad.item.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-abc.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-ap.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-caixa.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-cex.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-cli.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-cli-caixa.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-cli-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-contrat.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-contrat-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-cota.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-depos-roman.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-desenho.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-dist.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-doc-orig-nfe.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-docto-import.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-docto-orig-cte.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-estrut-draw.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-estrut-draw.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-estrut-draw.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-export.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-fornec.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-fornec-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-fornec-fabrican.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-invest.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-linha.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-man.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-man-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-mat.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-mat-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-re.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-ref-nfe.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-roteiro.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-tab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-uf.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-un-altern.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-uni-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-unid-venda.
DISABLE TRIGGERS FOR LOAD OF mgcad.jurisdic-aliq.
DISABLE TRIGGERS FOR LOAD OF mgcad.lin-texto.
DISABLE TRIGGERS FOR LOAD OF mgcad.lista-compon-for.
DISABLE TRIGGERS FOR LOAD OF mgcad.lista-compon-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.lista-compon-reg.
DISABLE TRIGGERS FOR LOAD OF mgcad.log-custo-pad.
DISABLE TRIGGERS FOR LOAD OF mgcad.log-ordem.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-comp.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-comp.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-compr.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-lin-tex.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-res-carac.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-texto.
DISABLE TRIGGERS FOR LOAD OF mgcad.lote-vend.
DISABLE TRIGGERS FOR LOAD OF mgcad.mat-comp.
DISABLE TRIGGERS FOR LOAD OF mgcad.matriz-rat-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.memorando-movto.
DISABLE TRIGGERS FOR LOAD OF mgcad.mi-pl-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.mi-pl-per.
DISABLE TRIGGERS FOR LOAD OF mgcad.mla-lista-aprov-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.mla-tipo-aprov-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-consumo.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-custo-data.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-lote.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-narrativa.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-pend-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.mnt-ref-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.movto-apr.
DISABLE TRIGGERS FOR LOAD OF mgcad.movto-ato.
DISABLE TRIGGERS FOR LOAD OF mgcad.movto-ato.
DISABLE TRIGGERS FOR LOAD OF mgcad.movto-ato.
DISABLE TRIGGERS FOR LOAD OF mgcad.movto-inv.
DISABLE TRIGGERS FOR LOAD OF mgcad.movto-nf.
DISABLE TRIGGERS FOR LOAD OF mgcad.multa-contrat.
DISABLE TRIGGERS FOR LOAD OF mgcad.narr-plano.
DISABLE TRIGGERS FOR LOAD OF mgcad.narrativa.
DISABLE TRIGGERS FOR LOAD OF mgcad.nbm-re.
DISABLE TRIGGERS FOR LOAD OF mgcad.nec-brutas.
DISABLE TRIGGERS FOR LOAD OF mgcad.nec-calc.
DISABLE TRIGGERS FOR LOAD OF mgcad.necessidade-oc.
DISABLE TRIGGERS FOR LOAD OF mgcad.op-ord-exam.
DISABLE TRIGGERS FOR LOAD OF mgcad.oper-conf.
DISABLE TRIGGERS FOR LOAD OF mgcad.oper-exam.
DISABLE TRIGGERS FOR LOAD OF mgcad.oper-ord-pa.
DISABLE TRIGGERS FOR LOAD OF mgcad.operacao.
DISABLE TRIGGERS FOR LOAD OF mgcad.ord-aber.
DISABLE TRIGGERS FOR LOAD OF mgcad.ord-ped.
DISABLE TRIGGERS FOR LOAD OF mgcad.ord-plan-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.ord-planej-pa.
DISABLE TRIGGERS FOR LOAD OF mgcad.ord-planej-sws.
DISABLE TRIGGERS FOR LOAD OF mgcad.ordem-man-mov.
DISABLE TRIGGERS FOR LOAD OF mgcad.ordem-reprog.
DISABLE TRIGGERS FOR LOAD OF mgcad.os-requis.
DISABLE TRIGGERS FOR LOAD OF mgcad.par-cota.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-contrat.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-cq.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-dp.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-dp.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-item-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-mi.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-config.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-config.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-curva.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-ent-desp.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-est-it.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-hist.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-planej-sws.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-pmp.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-saldo.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-sdo-terc.
DISABLE TRIGGERS FOR LOAD OF mgcad.pedido-drb-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.pend-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.pend-item-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.pert-crp.
DISABLE TRIGGERS FOR LOAD OF mgcad.pi-res-aber.
DISABLE TRIGGERS FOR LOAD OF mgcad.pl-carga-maq.
DISABLE TRIGGERS FOR LOAD OF mgcad.pl-ext-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.pl-it-calc.
DISABLE TRIGGERS FOR LOAD OF mgcad.pl-it-calc-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.pl-it-prod.
DISABLE TRIGGERS FOR LOAD OF mgcad.pl-saldo-estoq.
DISABLE TRIGGERS FOR LOAD OF mgcad.plano-aprov.
DISABLE TRIGGERS FOR LOAD OF mgcad.pm-al-it.
DISABLE TRIGGERS FOR LOAD OF mgcad.pm-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.pm-item-per.
DISABLE TRIGGERS FOR LOAD OF mgcad.pm-item-per-estab.
DISABLE TRIGGERS FOR LOAD OF mgcad.pm-repro.
DISABLE TRIGGERS FOR LOAD OF mgcad.pr-venda.
DISABLE TRIGGERS FOR LOAD OF mgcad.preco-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.preco-pad.
DISABLE TRIGGERS FOR LOAD OF mgcad.proc-container-emb.
DISABLE TRIGGERS FOR LOAD OF mgcad.proc-it-nota-desp.
DISABLE TRIGGERS FOR LOAD OF mgcad.proc-it-nota-fisc.
DISABLE TRIGGERS FOR LOAD OF mgcad.proc-lote-ped-ent.
DISABLE TRIGGERS FOR LOAD OF mgcad.proc-ped-ent.
DISABLE TRIGGERS FOR LOAD OF mgcad.proc-ped-ent-desp.
DISABLE TRIGGERS FOR LOAD OF mgcad.proces-acond-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.proces-embal-volum.
DISABLE TRIGGERS FOR LOAD OF mgcad.proces-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.proces-reg.
DISABLE TRIGGERS FOR LOAD OF mgcad.prod-composto.
DISABLE TRIGGERS FOR LOAD OF mgcad.prod-composto.
DISABLE TRIGGERS FOR LOAD OF mgcad.produto.
DISABLE TRIGGERS FOR LOAD OF mgcad.produto.
DISABLE TRIGGERS FOR LOAD OF mgcad.produto-coml.
DISABLE TRIGGERS FOR LOAD OF mgcad.prog-entr.
DISABLE TRIGGERS FOR LOAD OF mgcad.quota.
DISABLE TRIGGERS FOR LOAD OF mgcad.rec-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.recur-sec-operac.
DISABLE TRIGGERS FOR LOAD OF mgcad.recurso.
DISABLE TRIGGERS FOR LOAD OF mgcad.rede-pert.
DISABLE TRIGGERS FOR LOAD OF mgcad.ref-estrut.
DISABLE TRIGGERS FOR LOAD OF mgcad.ref-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.reg-export-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.repr-ped.
DISABLE TRIGGERS FOR LOAD OF mgcad.repr-prev.
DISABLE TRIGGERS FOR LOAD OF mgcad.repres-perc.
DISABLE TRIGGERS FOR LOAD OF mgcad.res-aber.
DISABLE TRIGGERS FOR LOAD OF mgcad.res-atp.
DISABLE TRIGGERS FOR LOAD OF mgcad.res-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.resum-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.ri-bem.
DISABLE TRIGGERS FOR LOAD OF mgcad.ri-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.rot-alt-paral.
DISABLE TRIGGERS FOR LOAD OF mgcad.rot-altern.
DISABLE TRIGGERS FOR LOAD OF mgcad.rot-it-conf.
DISABLE TRIGGERS FOR LOAD OF mgcad.rot-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.rot-item-paral.
DISABLE TRIGGERS FOR LOAD OF mgcad.rot-item-reg.
DISABLE TRIGGERS FOR LOAD OF mgcad.saldo-inv.
DISABLE TRIGGERS FOR LOAD OF mgcad.simul-ent.
DISABLE TRIGGERS FOR LOAD OF mgcad.simul-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.simulacao-preco.
DISABLE TRIGGERS FOR LOAD OF mgcad.sl-qt-per.
DISABLE TRIGGERS FOR LOAD OF mgcad.sl-vl-per.
DISABLE TRIGGERS FOR LOAD OF mgcad.tab-conver-veic.
DISABLE TRIGGERS FOR LOAD OF mgcad.tar-item-eq.
DISABLE TRIGGERS FOR LOAD OF mgcad.tar-item-eq-tag.
DISABLE TRIGGERS FOR LOAD OF mgcad.taref-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.tbpreco-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.tex-exame.
DISABLE TRIGGERS FOR LOAD OF mgcad.texto.
DISABLE TRIGGERS FOR LOAD OF mgcad.tipo-aprov-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.traduc-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.unid-neg-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.unid-negoc-depos.
DISABLE TRIGGERS FOR LOAD OF mgcad.valor-ult-ven.
DISABLE TRIGGERS FOR LOAD OF mgcad.wt-fat-ser-lote.
DISABLE TRIGGERS FOR LOAD OF mgcad.wt-it-docto.
DISABLE TRIGGERS FOR LOAD OF mgcad.wt-item-sfa.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-it-nota-fisc.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-item-dcr.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-pli-oc.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-sdo-insumo.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-ult-entr-dcr.
DISABLE TRIGGERS FOR LOAD OF mgmov.consu-aloc.
DISABLE TRIGGERS FOR LOAD OF mgmov.cot-rede-p.
DISABLE TRIGGERS FOR LOAD OF mgmov.cot-relat.
DISABLE TRIGGERS FOR LOAD OF mgmov.meta-produc-det.
DISABLE TRIGGERS FOR LOAD OF mgmov.mgc-apont-calib.
DISABLE TRIGGERS FOR LOAD OF mgmov.mnt-aloca-reserva.
DISABLE TRIGGERS FOR LOAD OF mgmov.mnt-oper-ord.
DISABLE TRIGGERS FOR LOAD OF mgmov.mnt-ordem-reprog.
DISABLE TRIGGERS FOR LOAD OF mgmov.mnt-reservas.
DISABLE TRIGGERS FOR LOAD OF mgmov.op-sfc.
DISABLE TRIGGERS FOR LOAD OF mgmov.pend-import.
DISABLE TRIGGERS FOR LOAD OF mgmov.promocao.
DISABLE TRIGGERS FOR LOAD OF mgmov.rep-prod.
DISABLE TRIGGERS FOR LOAD OF mgmov.rep-refugo-mat.
DISABLE TRIGGERS FOR LOAD OF mgmov.res-estab.
DISABLE TRIGGERS FOR LOAD OF mgmov.resum-item-aloc.
DISABLE TRIGGERS FOR LOAD OF mgmov.split-operac.
DISABLE TRIGGERS FOR LOAD OF mgmov.wt-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.wt-ped-item.
DISABLE TRIGGERS FOR LOAD OF movdis.calc-preco.
DISABLE TRIGGERS FOR LOAD OF movdis.ctr-saldo.
DISABLE TRIGGERS FOR LOAD OF movdis.devol-cli.
DISABLE TRIGGERS FOR LOAD OF movdis.eq-simul-carg-ped.
DISABLE TRIGGERS FOR LOAD OF movdis.eq-simul-ped.
DISABLE TRIGGERS FOR LOAD OF movdis.ext-it-nota.
DISABLE TRIGGERS FOR LOAD OF movdis.fat-estat.
DISABLE TRIGGERS FOR LOAD OF movdis.fat-ser-lote.
DISABLE TRIGGERS FOR LOAD OF movdis.his-ped-ent.
DISABLE TRIGGERS FOR LOAD OF movdis.it-doc-fisc.
DISABLE TRIGGERS FOR LOAD OF movdis.it-nota-doc.
DISABLE TRIGGERS FOR LOAD OF movdis.it-nota-fisc.
DISABLE TRIGGERS FOR LOAD OF movdis.it-nota-imp.
DISABLE TRIGGERS FOR LOAD OF movdis.it-pedido-imp.
DISABLE TRIGGERS FOR LOAD OF movdis.it-remito.
DISABLE TRIGGERS FOR LOAD OF movdis.item-embal.
DISABLE TRIGGERS FOR LOAD OF movdis.item-nf-ifrs.
DISABLE TRIGGERS FOR LOAD OF movdis.item-roman-faturam.
DISABLE TRIGGERS FOR LOAD OF movdis.movto-armazem.
DISABLE TRIGGERS FOR LOAD OF movdis.nar-it-nota.
DISABLE TRIGGERS FOR LOAD OF movdis.ped-ent.
DISABLE TRIGGERS FOR LOAD OF movdis.ped-item.
DISABLE TRIGGERS FOR LOAD OF movdis.prog-ent.
DISABLE TRIGGERS FOR LOAD OF movdis.prog-entreg-cham.
DISABLE TRIGGERS FOR LOAD OF movdis.prog-item.
DISABLE TRIGGERS FOR LOAD OF movdis.rateio-it-duplic.
DISABLE TRIGGERS FOR LOAD OF movdis.retroat.
DISABLE TRIGGERS FOR LOAD OF movdis.retroat.
DISABLE TRIGGERS FOR LOAD OF movdis.retroat-clien-item.
DISABLE TRIGGERS FOR LOAD OF movdis.retroat-pre-nf-item.
DISABLE TRIGGERS FOR LOAD OF movdis.ri-ext-it-nota-doc.
DISABLE TRIGGERS FOR LOAD OF movdis.unid-neg-fat.
DISABLE TRIGGERS FOR LOAD OF movdis.unid-neg-ped.
DISABLE TRIGGERS FOR LOAD OF movind.aloca-reserva.
DISABLE TRIGGERS FOR LOAD OF movind.componente.
DISABLE TRIGGERS FOR LOAD OF movind.consumo.
DISABLE TRIGGERS FOR LOAD OF movind.consumo-estab.
DISABLE TRIGGERS FOR LOAD OF movind.controle-preco.
DISABLE TRIGGERS FOR LOAD OF movind.cotacao-item.
DISABLE TRIGGERS FOR LOAD OF movind.devol-forn.
DISABLE TRIGGERS FOR LOAD OF movind.doc-pend-aprov.
DISABLE TRIGGERS FOR LOAD OF movind.estat-estab.
DISABLE TRIGGERS FOR LOAD OF movind.evento-ped.
DISABLE TRIGGERS FOR LOAD OF movind.exam-ficha.
DISABLE TRIGGERS FOR LOAD OF movind.ficha-cq.
DISABLE TRIGGERS FOR LOAD OF movind.hist-alter.
DISABLE TRIGGERS FOR LOAD OF movind.it-consumo.
DISABLE TRIGGERS FOR LOAD OF movind.it-doc-fisico.
DISABLE TRIGGERS FOR LOAD OF movind.it-prog-for.
DISABLE TRIGGERS FOR LOAD OF movind.it-requisicao.
DISABLE TRIGGERS FOR LOAD OF movind.it-romaneio.
DISABLE TRIGGERS FOR LOAD OF movind.item-doc-est.
DISABLE TRIGGERS FOR LOAD OF movind.item-estab.
DISABLE TRIGGERS FOR LOAD OF movind.item-nat-oper.
DISABLE TRIGGERS FOR LOAD OF movind.item-om.
DISABLE TRIGGERS FOR LOAD OF movind.medicao-contrat.
DISABLE TRIGGERS FOR LOAD OF movind.mla-doc-pend-aprov.
DISABLE TRIGGERS FOR LOAD OF movind.movto-coprodut.
DISABLE TRIGGERS FOR LOAD OF movind.movto-dir.
DISABLE TRIGGERS FOR LOAD OF movind.movto-estoq.
DISABLE TRIGGERS FOR LOAD OF movind.movto-ggf.
DISABLE TRIGGERS FOR LOAD OF movind.movto-mat.
DISABLE TRIGGERS FOR LOAD OF movind.movto-pend.
DISABLE TRIGGERS FOR LOAD OF movind.ocor-medio.
DISABLE TRIGGERS FOR LOAD OF movind.oper-ord.
DISABLE TRIGGERS FOR LOAD OF movind.ord-movto-period.
DISABLE TRIGGERS FOR LOAD OF movind.ord-prod.
DISABLE TRIGGERS FOR LOAD OF movind.ordem-compra.
DISABLE TRIGGERS FOR LOAD OF movind.pert-ordem.
DISABLE TRIGGERS FOR LOAD OF movind.pr-it-per.
DISABLE TRIGGERS FOR LOAD OF movind.prazo-compra.
DISABLE TRIGGERS FOR LOAD OF movind.prazo-it-prog.
DISABLE TRIGGERS FOR LOAD OF movind.rat-componente.
DISABLE TRIGGERS FOR LOAD OF movind.rat-lote.
DISABLE TRIGGERS FOR LOAD OF movind.rat-saldo-terc.
DISABLE TRIGGERS FOR LOAD OF movind.recebimento.
DISABLE TRIGGERS FOR LOAD OF movind.ref-ordem.
DISABLE TRIGGERS FOR LOAD OF movind.rep-oper.
DISABLE TRIGGERS FOR LOAD OF movind.req-altern.
DISABLE TRIGGERS FOR LOAD OF movind.req-ord.
DISABLE TRIGGERS FOR LOAD OF movind.req-sum.
DISABLE TRIGGERS FOR LOAD OF movind.res-fic-cq.
DISABLE TRIGGERS FOR LOAD OF movind.reservas.
DISABLE TRIGGERS FOR LOAD OF movind.saldo-custo.
DISABLE TRIGGERS FOR LOAD OF movind.saldo-estoq.
DISABLE TRIGGERS FOR LOAD OF movind.saldo-op-config.
DISABLE TRIGGERS FOR LOAD OF movind.saldo-terc.
DISABLE TRIGGERS FOR LOAD OF movind.sl-it-per.
DISABLE TRIGGERS FOR LOAD OF movind.sl-op-per.
DISABLE TRIGGERS FOR LOAD OF movind.sl-terc-per.
DISABLE TRIGGERS FOR LOAD OF movind.tex-ex-fic.
DISABLE TRIGGERS FOR LOAD OF movind.texto-follow-up.
DISABLE TRIGGERS FOR LOAD OF movind.unid-neg-requis.
DISABLE TRIGGERS FOR LOAD OF mgfas.ctx-pendencias.
DISABLE TRIGGERS FOR LOAD OF mgfas.ctx-pendencias.
DISABLE TRIGGERS FOR LOAD OF mgfas.ctx-pendencias.
DISABLE TRIGGERS FOR LOAD OF mgfas.ctx-reporte.
DISABLE TRIGGERS FOR LOAD OF mgfas.ctx-reporte.
DISABLE TRIGGERS FOR LOAD OF mgfas.ctx-reporte.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-aponta.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-aponta.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-aponta-retrabalho.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-aponta-retrabalho.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-cor.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-embarque-item.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-item-cor.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-item-cor.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-item-cor.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-juncao.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-juncao.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-juncao.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-movto-pto-controle.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-movto-pto-controle.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-reporte.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-reporte.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-reporte.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-rg-item.
DISABLE TRIGGERS FOR LOAD OF mgfas.dc-rg-item.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-etq-item.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-it-ordem.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-item-cli.
DISABLE TRIGGERS FOR LOAD OF mgfas.ES-ITEM-DOC-EST.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-nfr-item-movto.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-nfr-nota-item.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-nfr-peca.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-operacao.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-pedido-cli.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-pedido-vw.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-prog-entrega.
DISABLE TRIGGERS FOR LOAD OF mgfas.es-saldo-terc.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-cliprd.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-defocorre.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-detord.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-it-emb.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-it-nota.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-matplano.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-matsaldo.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-perc-crit.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-plames.
DISABLE TRIGGERS FOR LOAD OF mgfas.lo-plaprod.

DISABLE TRIGGERS FOR LOAD OF mgdbr.estrut-item-dbr.
DISABLE TRIGGERS FOR LOAD OF mgdbr.ref-it-estrut.
DISABLE TRIGGERS FOR LOAD OF mgcad.altern-lista-compon.
DISABLE TRIGGERS FOR LOAD OF mgcad.alternativo.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-altern.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-estrut.
DISABLE TRIGGERS FOR LOAD OF mgcad.especies.
DISABLE TRIGGERS FOR LOAD OF mgcad.estr-conf.
DISABLE TRIGGERS FOR LOAD OF mgcad.estrutura.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-estrut-draw.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-lista-compon.
DISABLE TRIGGERS FOR LOAD OF mgcad.ref-estrut.
DISABLE TRIGGERS FOR LOAD OF mgcad.ref-estrut-dp.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-compon-dcr.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-compon-dcr.
DISABLE TRIGGERS FOR LOAD OF mgmov.cot-estrut.
DISABLE TRIGGERS FOR LOAD OF movind.ord-prod.
DISABLE TRIGGERS FOR LOAD OF movind.prazo-compra.

DISABLE TRIGGERS FOR LOAD OF mgcad.dp-altern.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-estrut.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-nar-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-oper.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-proces-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.dp-rede-p.
DISABLE TRIGGERS FOR LOAD OF mgcad.ref-estrut-dp.
DISABLE TRIGGERS FOR LOAD OF mgcad.refer-item-desenv.
DISABLE TRIGGERS FOR LOAD OF mgmov.dp-relat.

DISABLE TRIGGERS FOR LOAD OF movdbr.bmg-apont-causa.
DISABLE TRIGGERS FOR LOAD OF movdbr.op-alter-confir.
DISABLE TRIGGERS FOR LOAD OF mgcad.decla-import-adic-itens.
DISABLE TRIGGERS FOR LOAD OF mgcad.decla-import-ord.
DISABLE TRIGGERS FOR LOAD OF mgcad.deduc-adic-itens.
DISABLE TRIGGERS FOR LOAD OF mgcad.despes-adic-itens.
DISABLE TRIGGERS FOR LOAD OF mgcad.docto-orig-cte.
DISABLE TRIGGERS FOR LOAD OF mgcad.docto-orig-nfse.
DISABLE TRIGGERS FOR LOAD OF mgcad.dwf-fator-conver.
DISABLE TRIGGERS FOR LOAD OF mgcad.dwf-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.dwf-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.dwf-item-compon.
DISABLE TRIGGERS FOR LOAD OF mgcad.dwf-item-compon.
DISABLE TRIGGERS FOR LOAD OF mgcad.dwf-item-produt-decla.
DISABLE TRIGGERS FOR LOAD OF mgcad.fab-medic-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.fab-medic-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.iss-cidad-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.it-pre-fat-serial.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-bonif-cliente.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-bonif-cliente.
DISABLE TRIGGERS FOR LOAD OF mgcad.Item-cubado-tf.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-docto-transf-depos.
DISABLE TRIGGERS FOR LOAD OF mgcad.item-fornec-fabrican-umd.
DISABLE TRIGGERS FOR LOAD OF mgcad.ncm-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.ncm-item-traduz.
DISABLE TRIGGERS FOR LOAD OF mgcad.nve-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.operacao.
DISABLE TRIGGERS FOR LOAD OF mgcad.ped-sdo-terc.
DISABLE TRIGGERS FOR LOAD OF mgcad.proces-devol-clien.
DISABLE TRIGGERS FOR LOAD OF mgcad.serial-faturam.
DISABLE TRIGGERS FOR LOAD OF mgcad.sfw-integr-ped-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.sit-tribut-relacto.
DISABLE TRIGGERS FOR LOAD OF mgcad.tab-conver-veic.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-aloca-saldo.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-box-indisponivel.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-box-movto.
DISABLE TRIGGERS FOR LOAD OF mgcad.Wm-box-movto-lote.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-box-preferencial.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-box-saida-ressup.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-box-saldo.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-conferencia-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-docto-itens.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-docto-itens-devol.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-docto-itens-ped.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-docto-itens-ped-etiqueta.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-etiqueta.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-inventario-acertos.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-inventario-ficha.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-inventario-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item-embalagem.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item-embalagem-etiq.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item-embalagem-local.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item-etiq.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item-picking.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-item-regra.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-packing-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-ref-item.
DISABLE TRIGGERS FOR LOAD OF mgcad.wm-saldo-estoque.
DISABLE TRIGGERS FOR LOAD OF mgcad.wms-item-embal-local-pack.
DISABLE TRIGGERS FOR LOAD OF mgcad.wms-item-estab-local.
DISABLE TRIGGERS FOR LOAD OF mgcad.wms-item-fornec-embal.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-estrut.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-estrut.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-apurac-cr-extmpreo.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-apurac-impto-docto.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-bomba-bico.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cf-resum-diario-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cf-vda-consm-det.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cf-vda-consm-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cf-vda-consm-item-entr.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cfe-detmnto.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cfe-detmnto-umd-produt.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cfe-resum-sat.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cfe-resum-umd-produt-sat.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-ciap-docto-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-concil-estoq-combust.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cupom-fisc-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cupom-fisc-mensal.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-detmnto-recta-regim-cx.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-item-arma.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-item-impto.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-item-medicto.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-item-outros-ajust.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-item-veic-novo.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-reg-export.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-docto-reg-export-indta.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-estrut-ord-produc.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-estrut-ord-produc.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-invent.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-invent-motiv.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-movimen-combust.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-movimen-combust-concil.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-movimen-combust-tanque.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-movto-estoq.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-ord-produc.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-outros-docto-operac.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-reduc-z-tot-parcial-item.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-val-agreg-munpio.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-vol-vendas-combust.
DISABLE TRIGGERS FOR LOAD OF mgmov.op-sfc.
DISABLE TRIGGERS FOR LOAD OF mgmov.split-operac.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-aloc-cust-icms-conjto.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-apurac-devol.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-apurac-icms-cust.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-apurac-icms-cust.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-export-indta-comprov.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-ficha-tec-produc.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-ficha-tec-unit-produc.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-gerac-cr-acum-icms.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-ident-item.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-invent-produt-elabor.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-rat-energ-ggf.
DISABLE TRIGGERS FOR LOAD OF movdis.cat83-sdo-icms-cust.
DISABLE TRIGGERS FOR LOAD OF movdis.desc-it-nota-fisc.
DISABLE TRIGGERS FOR LOAD OF movdis.desc-ped-item.
DISABLE TRIGGERS FOR LOAD OF movdis.estrut-item-fci.
DISABLE TRIGGERS FOR LOAD OF movdis.it-nota-fisc-serial.
DISABLE TRIGGERS FOR LOAD OF movdis.item-entr-st.
DISABLE TRIGGERS FOR LOAD OF movdis.item-nf-adc.
DISABLE TRIGGERS FOR LOAD OF movdis.item-nfs-st.
DISABLE TRIGGERS FOR LOAD OF movdis.nota-fisc-adc.
DISABLE TRIGGERS FOR LOAD OF movind.componente.
DISABLE TRIGGERS FOR LOAD OF movind.item-doc-est-troca.
DISABLE TRIGGERS FOR LOAD OF movind.item-docto-estoq-nfe-imp.
DISABLE TRIGGERS FOR LOAD OF movind.movto-coprodut.
DISABLE TRIGGERS FOR LOAD OF movind.oper-ord.
DISABLE TRIGGERS FOR LOAD OF movind.ord-prod.
DISABLE TRIGGERS FOR LOAD OF movind.rat-componente.
DISABLE TRIGGERS FOR LOAD OF movind.rma-it-dep.
DISABLE TRIGGERS FOR LOAD OF movind.rma-it-troca.
DISABLE TRIGGERS FOR LOAD OF movind.rma-item.

DISABLE TRIGGERS FOR LOAD OF mgcad.desconto.
DISABLE TRIGGERS FOR LOAD OF mgcad.est-prod-coml.
DISABLE TRIGGERS FOR LOAD OF mgcad.integr-dis.
DISABLE TRIGGERS FOR LOAD OF mgcad.integr-mat.
DISABLE TRIGGERS FOR LOAD OF mgcad.param-pa.
DISABLE TRIGGERS FOR LOAD OF mgcad.preco-pad.
DISABLE TRIGGERS FOR LOAD OF mgcad.prod-ordem.
DISABLE TRIGGERS FOR LOAD OF mgcad.produto-coml.
DISABLE TRIGGERS FOR LOAD OF mgcad.produto-pat.
DISABLE TRIGGERS FOR LOAD OF mgcad.time-connection.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-pli-destaq.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-pli-det.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-pli-ncm.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-pli-oc.
DISABLE TRIGGERS FOR LOAD OF mgcad.zfm-pli-orgao-anuente.
DISABLE TRIGGERS FOR LOAD OF mgmov.dwf-cta-ctbl-refer.
DISABLE TRIGGERS FOR LOAD OF mgmov.wt-item.


for each sanea-item where sanea-item.processado = no.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat sanea-item.it-codigo ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-ord-mts' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.bmg-ord-mts where bmg-ord-mts.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.bmg-ord-mts FIELDS() where bmg-ord-mts.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.bmg-ord-mts' + STRING(i-lidos)).
            delete bmg-ord-mts.
        end.
        release bmg-ord-mts.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-sdo-estoq-mts' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.bmg-sdo-estoq-mts FIELDS() use-index bmgsdstq-id where bmg-sdo-estoq-mts.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.bmg-sdo-estoq-mts' + STRING(i-lidos)).
            delete bmg-sdo-estoq-mts.
        end.
        release bmg-sdo-estoq-mts.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctrab-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.ctrab-dbr where ctrab-dbr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.ctrab-dbr FIELDS() where ctrab-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.ctrab-dbr' + STRING(i-lidos)).
            delete ctrab-dbr.
        end.
        release ctrab-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estab-solic-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.estab-solic-item FIELDS() use-index estbslct-it-prod where estab-solic-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.estab-solic-item' + STRING(i-lidos)).
            delete estab-solic-item.
        end.
        release estab-solic-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estrut-item-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.estrut-item-dbr FIELDS() use-index codigo where estrut-item-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.estrut-item-dbr' + STRING(i-lidos)).
            delete estrut-item-dbr.
        end.
        release estrut-item-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-cenar' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.item-cenar where item-cenar.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.item-cenar FIELDS() where item-cenar.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.item-cenar' + STRING(i-lidos)).
            delete item-cenar.
        end.
        release item-cenar.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.item-dbr FIELDS() use-index itemdbr-id where item-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.item-dbr' + STRING(i-lidos)).
            delete item-dbr.
        end.
        release item-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-estab-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.item-estab-dbr FIELDS() use-index itmstbdb-id where item-estab-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.item-estab-dbr' + STRING(i-lidos)).
            delete item-estab-dbr.
        end.
        release item-estab-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'op-ord-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.op-ord-dbr where op-ord-dbr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.op-ord-dbr FIELDS() where op-ord-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.op-ord-dbr' + STRING(i-lidos)).
            delete op-ord-dbr.
        end.
        release op-ord-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'operac-engr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.operac-engr FIELDS() use-index codigo where operac-engr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.operac-engr' + STRING(i-lidos)).
            delete operac-engr.
        end.
        release operac-engr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.ord-dbr FIELDS() use-index orddbr-dtinicio where ord-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.ord-dbr' + STRING(i-lidos)).
            delete ord-dbr.
        end.
        release ord-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pdven-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.pdven-dbr FIELDS() use-index pdvendbr-item where pdven-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.pdven-dbr' + STRING(i-lidos)).
            delete pdven-dbr.
        end.
        release pdven-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-alter-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.ped-alter-dbr where ped-alter-dbr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.ped-alter-dbr FIELDS() where ped-alter-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.ped-alter-dbr' + STRING(i-lidos)).
            delete ped-alter-dbr.
        end.
        release ped-alter-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-alter-histor-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.ped-alter-histor-dbr where ped-alter-histor-dbr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.ped-alter-histor-dbr FIELDS() where ped-alter-histor-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.ped-alter-histor-dbr' + STRING(i-lidos)).
            delete ped-alter-histor-dbr.
        end.
        release ped-alter-histor-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pert-ord-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.pert-ord-dbr where pert-ord-dbr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.pert-ord-dbr FIELDS() where pert-ord-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.pert-ord-dbr' + STRING(i-lidos)).
            delete pert-ord-dbr.
        end.
        release pert-ord-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proces-item-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.proces-item-dbr FIELDS() use-index it-estab where proces-item-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.proces-item-dbr' + STRING(i-lidos)).
            delete proces-item-dbr.
        end.
        release proces-item-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'recur-sec-operac-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.recur-sec-operac-dbr FIELDS() use-index rcrscpra-item where recur-sec-operac-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.recur-sec-operac-dbr' + STRING(i-lidos)).
            delete recur-sec-operac-dbr.
        end.
        release recur-sec-operac-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rede-op-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.rede-op-dbr FIELDS() use-index rdprcdbr-item-rot where rede-op-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.rede-op-dbr' + STRING(i-lidos)).
            delete rede-op-dbr.
        end.
        release rede-op-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-it-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.ref-it-estrut FIELDS() use-index codigo where ref-it-estrut.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.ref-it-estrut' + STRING(i-lidos)).
            delete ref-it-estrut.
        end.
        release ref-it-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'refer-item-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.refer-item-dbr FIELDS() use-index rfrtmdbr-id where refer-item-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.refer-item-dbr' + STRING(i-lidos)).
            delete refer-item-dbr.
        end.
        release refer-item-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'res-ord-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.res-ord-dbr FIELDS() use-index rsrvrddb-03 where res-ord-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.res-ord-dbr' + STRING(i-lidos)).
            delete res-ord-dbr.
        end.
        release res-ord-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-item-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.rot-item-dbr FIELDS() use-index rttmdbr-id where rot-item-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.rot-item-dbr' + STRING(i-lidos)).
            delete rot-item-dbr.
        end.
        release rot-item-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sdo-estoq-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.sdo-estoq-dbr FIELDS() use-index sdstqdbr-id where sdo-estoq-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.sdo-estoq-dbr' + STRING(i-lidos)).
            delete sdo-estoq-dbr.
        end.
        release sdo-estoq-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sdo-terc-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.sdo-terc-dbr FIELDS() use-index sdtrcdbr-item where sdo-terc-dbr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.sdo-terc-dbr' + STRING(i-lidos)).
            delete sdo-terc-dbr.
        end.
        release sdo-terc-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-apont-causa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.bmg-apont-causa where bmg-apont-causa.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.bmg-apont-causa FIELDS() where bmg-apont-causa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.bmg-apont-causa' + STRING(i-lidos)).
            delete bmg-apont-causa.
        end.
        release bmg-apont-causa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-estatis-regiao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.bmg-estatis-regiao where bmg-estatis-regiao.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.bmg-estatis-regiao FIELDS() where bmg-estatis-regiao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.bmg-estatis-regiao' + STRING(i-lidos)).
            delete bmg-estatis-regiao.
        end.
        release bmg-estatis-regiao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-necessidades' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.bmg-necessidades where bmg-necessidades.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.bmg-necessidades FIELDS() where bmg-necessidades.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.bmg-necessidades' + STRING(i-lidos)).
            delete bmg-necessidades.
        end.
        release bmg-necessidades.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-programacao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.bmg-programacao where bmg-programacao.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.bmg-programacao FIELDS() where bmg-programacao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.bmg-programacao' + STRING(i-lidos)).
            delete bmg-programacao.
        end.
        release bmg-programacao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'demand-cenar' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.demand-cenar where demand-cenar.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.demand-cenar FIELDS() where demand-cenar.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.demand-cenar' + STRING(i-lidos)).
            delete demand-cenar.
        end.
        release demand-cenar.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-cenar' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.ord-cenar where ord-cenar.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.ord-cenar FIELDS() where ord-cenar.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.ord-cenar' + STRING(i-lidos)).
            delete ord-cenar.
        end.
        release ord-cenar.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-histor' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.ord-histor FIELDS() use-index ordhstr-it-carreg where ord-histor.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.ord-histor' + STRING(i-lidos)).
            delete ord-histor.
        end.
        release ord-histor.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-demand-cenar' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.ped-demand-cenar where ped-demand-cenar.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.ped-demand-cenar FIELDS() where ped-demand-cenar.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.ped-demand-cenar' + STRING(i-lidos)).
            delete ped-demand-cenar.
        end.
        release ped-demand-cenar.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'programac-recebto-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.programac-recebto-estab where programac-recebto-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.programac-recebto-estab FIELDS() where programac-recebto-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.programac-recebto-estab' + STRING(i-lidos)).
            delete programac-recebto-estab.
        end.
        release programac-recebto-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sdo-item-cenar' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.sdo-item-cenar where sdo-item-cenar.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.sdo-item-cenar FIELDS() where sdo-item-cenar.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.sdo-item-cenar' + STRING(i-lidos)).
            delete sdo-item-cenar.
        end.
        release sdo-item-cenar.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'solicit-produc-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.solicit-produc-estab where solicit-produc-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.solicit-produc-estab FIELDS() where solicit-produc-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.solicit-produc-estab' + STRING(i-lidos)).
            delete solicit-produc-estab.
        end.
        release solicit-produc-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'agrup-reservas' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.agrup-reservas where agrup-reservas.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.agrup-reservas FIELDS() where agrup-reservas.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.agrup-reservas' + STRING(i-lidos)).
            delete agrup-reservas.
        end.
        release agrup-reservas.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'al-pr-op' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.al-pr-op FIELDS() use-index item where al-pr-op.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.al-pr-op' + STRING(i-lidos)).
            delete al-pr-op.
        end.
        release al-pr-op.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'al-res-oc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.al-res-oc where al-res-oc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.al-res-oc FIELDS() where al-res-oc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.al-res-oc' + STRING(i-lidos)).
            delete al-res-oc.
        end.
        release al-res-oc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'al-res-op' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.al-res-op where al-res-op.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.al-res-op FIELDS() where al-res-op.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.al-res-op' + STRING(i-lidos)).
            delete al-res-op.
        end.
        release al-res-op.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'alt-comp-tar' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.alt-comp-tar where alt-comp-tar.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.alt-comp-tar FIELDS() where alt-comp-tar.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.alt-comp-tar' + STRING(i-lidos)).
            delete alt-comp-tar.
        end.
        release alt-comp-tar.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'alt-reci' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.alt-reci FIELDS() use-index ordem where alt-reci.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.alt-reci' + STRING(i-lidos)).
            delete alt-reci.
        end.
        release alt-reci.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'alternativo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.alternativo FIELDS() use-index codigo where alternativo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.alternativo' + STRING(i-lidos)).
            delete alternativo.
        end.
        release alternativo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'aprov-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.aprov-item FIELDS() use-index aprov-item-1 where aprov-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.aprov-item' + STRING(i-lidos)).
            delete aprov-item.
        end.
        release aprov-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'area-produc-prefer' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.area-produc-prefer where area-produc-prefer.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.area-produc-prefer FIELDS() where area-produc-prefer.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.area-produc-prefer' + STRING(i-lidos)).
            delete area-produc-prefer.
        end.
        release area-produc-prefer.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'base-mensal' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.base-mensal where base-mensal.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.base-mensal FIELDS() where base-mensal.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.base-mensal' + STRING(i-lidos)).
            delete base-mensal.
        end.
        release base-mensal.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'base-mensal1' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.base-mensal1 where base-mensal1.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.base-mensal1 FIELDS() where base-mensal1.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.base-mensal1' + STRING(i-lidos)).
            delete base-mensal1.
        end.
        release base-mensal1.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-etiqueta' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-etiqueta FIELDS() use-index ch-it-codigo where bc-etiqueta.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-etiqueta' + STRING(i-lidos)).
            delete bc-etiqueta.
        end.
        release bc-etiqueta.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item FIELDS() use-index ch-codigo where bc-ext-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item' + STRING(i-lidos)).
            delete bc-ext-item.
        end.
        release bc-ext-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item FIELDS() use-index ch-it-codigo-ext-1 where bc-ext-item.it-codigo-ext-1 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item' + STRING(i-lidos)).
            delete bc-ext-item.
        end.
        release bc-ext-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item FIELDS() use-index ch-it-codigo-ext-2 where bc-ext-item.it-codigo-ext-2 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item' + STRING(i-lidos)).
            delete bc-ext-item.
        end.
        release bc-ext-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item FIELDS() use-index ch-it-codigo-ext-3 where bc-ext-item.it-codigo-ext-3 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item' + STRING(i-lidos)).
            delete bc-ext-item.
        end.
        release bc-ext-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item FIELDS() use-index ch-it-codigo-ext-4 where bc-ext-item.it-codigo-ext-4 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item' + STRING(i-lidos)).
            delete bc-ext-item.
        end.
        release bc-ext-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item-emit' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.bc-ext-item-emit where bc-ext-item-emit.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item-emit FIELDS() where bc-ext-item-emit.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item-emit' + STRING(i-lidos)).
            delete bc-ext-item-emit.
        end.
        release bc-ext-item-emit.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item-emit' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item-emit FIELDS() use-index ch-it-codigo-ext-1 where bc-ext-item-emit.it-codigo-ext-1 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item-emit' + STRING(i-lidos)).
            delete bc-ext-item-emit.
        end.
        release bc-ext-item-emit.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item-emit' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item-emit FIELDS() use-index ch-it-codigo-ext-2 where bc-ext-item-emit.it-codigo-ext-2 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item-emit' + STRING(i-lidos)).
            delete bc-ext-item-emit.
        end.
        release bc-ext-item-emit.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item-emit' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item-emit FIELDS() use-index ch-it-codigo-ext-3 where bc-ext-item-emit.it-codigo-ext-3 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item-emit' + STRING(i-lidos)).
            delete bc-ext-item-emit.
        end.
        release bc-ext-item-emit.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-item-emit' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-item-emit FIELDS() use-index ch-it-codigo-ext-4 where bc-ext-item-emit.it-codigo-ext-4 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-item-emit' + STRING(i-lidos)).
            delete bc-ext-item-emit.
        end.
        release bc-ext-item-emit.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bc-ext-saldo-estoq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.bc-ext-saldo-estoq where bc-ext-saldo-estoq.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.bc-ext-saldo-estoq FIELDS() where bc-ext-saldo-estoq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.bc-ext-saldo-estoq' + STRING(i-lidos)).
            delete bc-ext-saldo-estoq.
        end.
        release bc-ext-saldo-estoq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'canal-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.canal-item FIELDS() use-index cnltmvd-02 where canal-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.canal-item' + STRING(i-lidos)).
            delete canal-item.
        end.
        release canal-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cap-turno' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.cap-turno where cap-turno.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.cap-turno FIELDS() where cap-turno.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.cap-turno' + STRING(i-lidos)).
            delete cap-turno.
        end.
        release cap-turno.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'carga-maq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.carga-maq FIELDS() use-index item where carga-maq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.carga-maq' + STRING(i-lidos)).
            delete carga-maq.
        end.
        release carga-maq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'carga-turno' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.carga-turno where carga-turno.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.carga-turno FIELDS() where carga-turno.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.carga-turno' + STRING(i-lidos)).
            delete carga-turno.
        end.
        release carga-turno.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'carga-turno-pl' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.carga-turno-pl FIELDS() use-index sequencia where carga-turno-pl.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.carga-turno-pl' + STRING(i-lidos)).
            delete carga-turno-pl.
        end.
        release carga-turno-pl.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'carga-x-ref' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.carga-x-ref where carga-x-ref.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.carga-x-ref FIELDS() where carga-x-ref.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.carga-x-ref' + STRING(i-lidos)).
            delete carga-x-ref.
        end.
        release carga-x-ref.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-item-natur-operac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.cat83-item-natur-operac where cat83-item-natur-operac.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.cat83-item-natur-operac FIELDS() where cat83-item-natur-operac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.cat83-item-natur-operac' + STRING(i-lidos)).
            delete cat83-item-natur-operac.
        end.
        release cat83-item-natur-operac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'comissao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.comissao where comissao.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.comissao FIELDS() where comissao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.comissao' + STRING(i-lidos)).
            delete comissao.
        end.
        release comissao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'comp-altern' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.comp-altern where comp-altern.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.comp-altern FIELDS() where comp-altern.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.comp-altern' + STRING(i-lidos)).
            delete comp-altern.
        end.
        release comp-altern.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'comp-equipto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.comp-equipto FIELDS() use-index it-equipto where comp-equipto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.comp-equipto' + STRING(i-lidos)).
            delete comp-equipto.
        end.
        release comp-equipto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'comp-solic' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.comp-solic where comp-solic.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.comp-solic FIELDS() where comp-solic.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.comp-solic' + STRING(i-lidos)).
            delete comp-solic.
        end.
        release comp-solic.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'componente-cex' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.componente-cex where componente-cex.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.componente-cex FIELDS() where componente-cex.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.componente-cex' + STRING(i-lidos)).
            delete componente-cex.
        end.
        release componente-cex.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'compr-fabric' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.compr-fabric FIELDS() use-index codigo where compr-fabric.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.compr-fabric' + STRING(i-lidos)).
            delete compr-fabric.
        end.
        release compr-fabric.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'conf-restr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.conf-restr FIELDS() use-index ch-conf where conf-restr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.conf-restr' + STRING(i-lidos)).
            delete conf-restr.
        end.
        release conf-restr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'conta-ft' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.conta-ft FIELDS() use-index ch-item where conta-ft.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.conta-ft' + STRING(i-lidos)).
            delete conta-ft.
        end.
        release conta-ft.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'contab-ge' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.contab-ge FIELDS() use-index codigo where contab-ge.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.contab-ge' + STRING(i-lidos)).
            delete contab-ge.
        end.
        release contab-ge.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'conv-man-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.conv-man-item FIELDS() use-index codigo where conv-man-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.conv-man-item' + STRING(i-lidos)).
            delete conv-man-item.
        end.
        release conv-man-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cota-item-reaj' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.cota-item-reaj FIELDS() use-index item where cota-item-reaj.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.cota-item-reaj' + STRING(i-lidos)).
            delete cota-item-reaj.
        end.
        release cota-item-reaj.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cotac-cond-pagto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.cotac-cond-pagto where cotac-cond-pagto.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.cotac-cond-pagto FIELDS() where cotac-cond-pagto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.cotac-cond-pagto' + STRING(i-lidos)).
            delete cotac-cond-pagto.
        end.
        release cotac-cond-pagto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cotacao-inv' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.cotacao-inv where cotacao-inv.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.cotacao-inv FIELDS() where cotacao-inv.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.cotacao-inv' + STRING(i-lidos)).
            delete cotacao-inv.
        end.
        release cotacao-inv.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cotacao-item-cex' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.cotacao-item-cex where cotacao-item-cex.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.cotacao-item-cex FIELDS() where cotacao-item-cex.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.cotacao-item-cex' + STRING(i-lidos)).
            delete cotacao-item-cex.
        end.
        release cotacao-item-cex.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'criter-orig' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.criter-orig FIELDS() use-index crtrrg-02 where criter-orig.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.criter-orig' + STRING(i-lidos)).
            delete criter-orig.
        end.
        release criter-orig.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'crp-oper' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.crp-oper where crp-oper.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.crp-oper FIELDS() where crp-oper.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.crp-oper' + STRING(i-lidos)).
            delete crp-oper.
        end.
        release crp-oper.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'crp-oper-ant' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.crp-oper-ant where crp-oper-ant.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.crp-oper-ant FIELDS() where crp-oper-ant.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.crp-oper-ant' + STRING(i-lidos)).
            delete crp-oper-ant.
        end.
        release crp-oper-ant.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'crp-ord-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.crp-ord-prod where crp-ord-prod.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.crp-ord-prod FIELDS() where crp-ord-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.crp-ord-prod' + STRING(i-lidos)).
            delete crp-ord-prod.
        end.
        release crp-ord-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctrab-operac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ctrab-operac FIELDS() use-index op-ctrab where ctrab-operac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ctrab-operac' + STRING(i-lidos)).
            delete ctrab-operac.
        end.
        release ctrab-operac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctrab-operac-aloc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ctrab-operac-aloc FIELDS() use-index ctrbprcl-oper where ctrab-operac-aloc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ctrab-operac-aloc' + STRING(i-lidos)).
            delete ctrab-operac-aloc.
        end.
        release ctrab-operac-aloc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'custo-data' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.custo-data FIELDS() use-index id where custo-data.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.custo-data' + STRING(i-lidos)).
            delete custo-data.
        end.
        release custo-data.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'delta-x-ref' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.delta-x-ref where delta-x-ref.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.delta-x-ref FIELDS() where delta-x-ref.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.delta-x-ref' + STRING(i-lidos)).
            delete delta-x-ref.
        end.
        release delta-x-ref.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'depen-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.depen-prod FIELDS() use-index ch-item where depen-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.depen-prod' + STRING(i-lidos)).
            delete depen-prod.
        end.
        release depen-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'depen-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.depen-prod where depen-prod.it-codigo-dep = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.depen-prod FIELDS() where depen-prod.it-codigo-dep = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.depen-prod' + STRING(i-lidos)).
            delete depen-prod.
        end.
        release depen-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'desconto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.desconto where desconto.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.desconto FIELDS() where desconto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.desconto' + STRING(i-lidos)).
            delete desconto.
        end.
        release desconto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'desenho-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.desenho-item FIELDS() use-index item where desenho-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.desenho-item' + STRING(i-lidos)).
            delete desenho-item.
        end.
        release desenho-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'Desp-cotacao-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.Desp-cotacao-item where Desp-cotacao-item.It-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.Desp-cotacao-item FIELDS() where Desp-cotacao-item.It-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.Desp-cotacao-item' + STRING(i-lidos)).
            delete Desp-cotacao-item.
        end.
        release Desp-cotacao-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'desp-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.desp-item FIELDS() use-index codigo where desp-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.desp-item' + STRING(i-lidos)).
            delete desp-item.
        end.
        release desp-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'Desp-item-fornec' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.Desp-item-fornec FIELDS() use-index desp-item where Desp-item-fornec.It-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.Desp-item-fornec' + STRING(i-lidos)).
            delete Desp-item-fornec.
        end.
        release Desp-item-fornec.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'Desp-item-fornec-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.Desp-item-fornec-estab FIELDS() use-index Desp-item where Desp-item-fornec-estab.It-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.Desp-item-fornec-estab' + STRING(i-lidos)).
            delete Desp-item-fornec-estab.
        end.
        release Desp-item-fornec-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'diario-tmp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.diario-tmp where diario-tmp.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.diario-tmp FIELDS() where diario-tmp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.diario-tmp' + STRING(i-lidos)).
            delete diario-tmp.
        end.
        release diario-tmp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-item FIELDS() use-index it-codigo where dp-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-item' + STRING(i-lidos)).
            delete dp-item.
        end.
        release dp-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'EB-embalagem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.EB-embalagem FIELDS() use-index Item where EB-embalagem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.EB-embalagem' + STRING(i-lidos)).
            delete EB-embalagem.
        end.
        release EB-embalagem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'EB-item-embal' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.EB-item-embal where EB-item-embal.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.EB-item-embal FIELDS() where EB-item-embal.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.EB-item-embal' + STRING(i-lidos)).
            delete EB-item-embal.
        end.
        release EB-item-embal.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'EB-rest-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.EB-rest-item FIELDS() use-index restricao where EB-rest-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.EB-rest-item' + STRING(i-lidos)).
            delete EB-rest-item.
        end.
        release EB-rest-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'EB-rest-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.EB-rest-item where EB-rest-item.it-codigo-rest = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.EB-rest-item FIELDS() where EB-rest-item.it-codigo-rest = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.EB-rest-item' + STRING(i-lidos)).
            delete EB-rest-item.
        end.
        release EB-rest-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'embalag' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.embalag FIELDS() use-index ch-item where embalag.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.embalag' + STRING(i-lidos)).
            delete embalag.
        end.
        release embalag.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'eq-item-embal-embarq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.eq-item-embal-embarq FIELDS() use-index eqtmmblm-id where eq-item-embal-embarq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.eq-item-embal-embarq' + STRING(i-lidos)).
            delete eq-item-embal-embarq.
        end.
        release eq-item-embal-embarq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'eq-item-embarq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.eq-item-embarq FIELDS() use-index eqtmmbrq-id where eq-item-embarq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.eq-item-embarq' + STRING(i-lidos)).
            delete eq-item-embarq.
        end.
        release eq-item-embarq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'eq-item-grp-embarq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.eq-item-grp-embarq FIELDS() use-index eqtmgrpm-01 where eq-item-grp-embarq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.eq-item-grp-embarq' + STRING(i-lidos)).
            delete eq-item-grp-embarq.
        end.
        release eq-item-grp-embarq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'est-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.est-prod FIELDS() use-index onde-se-usa where est-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.est-prod' + STRING(i-lidos)).
            delete est-prod.
        end.
        release est-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'est-prod-coml' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.est-prod-coml FIELDS() use-index onde-se-usa where est-prod-coml.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.est-prod-coml' + STRING(i-lidos)).
            delete est-prod-coml.
        end.
        release est-prod-coml.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'est-ref-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.est-ref-prod where est-ref-prod.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.est-ref-prod FIELDS() where est-ref-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.est-ref-prod' + STRING(i-lidos)).
            delete est-ref-prod.
        end.
        release est-ref-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estim-mat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.estim-mat where estim-mat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estim-mat FIELDS() where estim-mat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estim-mat' + STRING(i-lidos)).
            delete estim-mat.
        end.
        release estim-mat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estr-conf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estr-conf FIELDS() use-index operacao where estr-conf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estr-conf' + STRING(i-lidos)).
            delete estr-conf.
        end.
        release estr-conf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estr-mod-cf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estr-mod-cf FIELDS() use-index item where estr-mod-cf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estr-mod-cf' + STRING(i-lidos)).
            delete estr-mod-cf.
        end.
        release estr-mod-cf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estr-mod-for' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.estr-mod-for where estr-mod-for.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estr-mod-for FIELDS() where estr-mod-for.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estr-mod-for' + STRING(i-lidos)).
            delete estr-mod-for.
        end.
        release estr-mod-for.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estr-mod-reg' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.estr-mod-reg where estr-mod-reg.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estr-mod-reg FIELDS() where estr-mod-reg.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estr-mod-reg' + STRING(i-lidos)).
            delete estr-mod-reg.
        end.
        release estr-mod-reg.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estr-modelo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estr-modelo FIELDS() use-index item where estr-modelo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estr-modelo' + STRING(i-lidos)).
            delete estr-modelo.
        end.
        release estr-modelo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estrat-preco' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estrat-preco FIELDS() use-index ch-codigo where estrat-preco.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estrat-preco' + STRING(i-lidos)).
            delete estrat-preco.
        end.
        release estrat-preco.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estrutura' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estrutura FIELDS() use-index codigo where estrutura.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estrutura' + STRING(i-lidos)).
            delete estrutura.
        end.
        release estrutura.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'event-mod-contrat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.event-mod-contrat where event-mod-contrat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.event-mod-contrat FIELDS() where event-mod-contrat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.event-mod-contrat' + STRING(i-lidos)).
            delete event-mod-contrat.
        end.
        release event-mod-contrat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'evento-inv' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.evento-inv where evento-inv.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.evento-inv FIELDS() where evento-inv.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.evento-inv' + STRING(i-lidos)).
            delete evento-inv.
        end.
        release evento-inv.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'exame-insp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.exame-insp FIELDS() use-index codigo where exame-insp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.exame-insp' + STRING(i-lidos)).
            delete exame-insp.
        end.
        release exame-insp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ext-ped-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ext-ped-item FIELDS() use-index ch-item where ext-ped-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ext-ped-item' + STRING(i-lidos)).
            delete ext-ped-item.
        end.
        release ext-ped-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'fat-graf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.fat-graf FIELDS() use-index ch-item where fat-graf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.fat-graf' + STRING(i-lidos)).
            delete fat-graf.
        end.
        release fat-graf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ferr-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ferr-prod FIELDS() use-index item where ferr-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ferr-prod' + STRING(i-lidos)).
            delete ferr-prod.
        end.
        release ferr-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ge-troca' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ge-troca FIELDS() use-index codigo where ge-troca.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ge-troca' + STRING(i-lidos)).
            delete ge-troca.
        end.
        release ge-troca.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'his-componente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.his-componente where his-componente.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.his-componente FIELDS() where his-componente.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.his-componente' + STRING(i-lidos)).
            delete his-componente.
        end.
        release his-componente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'hist-preco' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.hist-preco where hist-preco.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.hist-preco FIELDS() where hist-preco.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.hist-preco' + STRING(i-lidos)).
            delete hist-preco.
        end.
        release hist-preco.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'histor-cust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.histor-cust where histor-cust.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.histor-cust FIELDS() where histor-cust.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.histor-cust' + STRING(i-lidos)).
            delete histor-cust.
        end.
        release histor-cust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'icms-it-uf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.icms-it-uf FIELDS() use-index codigo where icms-it-uf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.icms-it-uf' + STRING(i-lidos)).
            delete icms-it-uf.
        end.
        release icms-it-uf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'impto-remito-fatur' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.impto-remito-fatur where impto-remito-fatur.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.impto-remito-fatur FIELDS() where impto-remito-fatur.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.impto-remito-fatur' + STRING(i-lidos)).
            delete impto-remito-fatur.
        end.
        release impto-remito-fatur.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'impto-remito-fatur-det' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.impto-remito-fatur-det where impto-remito-fatur-det.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.impto-remito-fatur-det FIELDS() where impto-remito-fatur-det.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.impto-remito-fatur-det' + STRING(i-lidos)).
            delete impto-remito-fatur-det.
        end.
        release impto-remito-fatur-det.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'inv-condpagto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.inv-condpagto where inv-condpagto.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.inv-condpagto FIELDS() where inv-condpagto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.inv-condpagto' + STRING(i-lidos)).
            delete inv-condpagto.
        end.
        release inv-condpagto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'inv-log-exp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.inv-log-exp where inv-log-exp.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.inv-log-exp FIELDS() where inv-log-exp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.inv-log-exp' + STRING(i-lidos)).
            delete inv-log-exp.
        end.
        release inv-log-exp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'inventario' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.inventario FIELDS() use-index item where inventario.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.inventario' + STRING(i-lidos)).
            delete inventario.
        end.
        release inventario.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-carac-tec' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-carac-tec FIELDS() use-index codigo where it-carac-tec.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-carac-tec' + STRING(i-lidos)).
            delete it-carac-tec.
        end.
        release it-carac-tec.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-carga-plan' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-carga-plan FIELDS() use-index carga where it-carga-plan.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-carga-plan' + STRING(i-lidos)).
            delete it-carga-plan.
        end.
        release it-carga-plan.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-comp-exame' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-comp-exame FIELDS() use-index codigo where it-comp-exame.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-comp-exame' + STRING(i-lidos)).
            delete it-comp-exame.
        end.
        release it-comp-exame.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-contrato-cli' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-contrato-cli FIELDS() use-index ch-item-contrato where it-contrato-cli.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-contrato-cli' + STRING(i-lidos)).
            delete it-contrato-cli.
        end.
        release it-contrato-cli.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-dep-fat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-dep-fat FIELDS() use-index ch-item where it-dep-fat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-dep-fat' + STRING(i-lidos)).
            delete it-dep-fat.
        end.
        release it-dep-fat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-dep-remito' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-dep-remito FIELDS() use-index ch-item where it-dep-remito.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-dep-remito' + STRING(i-lidos)).
            delete it-dep-remito.
        end.
        release it-dep-remito.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-exame' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-exame FIELDS() use-index codigo where it-exame.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-exame' + STRING(i-lidos)).
            delete it-exame.
        end.
        release it-exame.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-markup' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.it-markup where it-markup.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-markup FIELDS() where it-markup.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-markup' + STRING(i-lidos)).
            delete it-markup.
        end.
        release it-markup.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-msg-carac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-msg-carac FIELDS() use-index codigo where it-msg-carac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-msg-carac' + STRING(i-lidos)).
            delete it-msg-carac.
        end.
        release it-msg-carac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-periodo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.it-periodo where it-periodo.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-periodo FIELDS() where it-periodo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-periodo' + STRING(i-lidos)).
            delete it-periodo.
        end.
        release it-periodo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-pre-emb' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.it-pre-emb where it-pre-emb.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-pre-emb FIELDS() where it-pre-emb.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-pre-emb' + STRING(i-lidos)).
            delete it-pre-emb.
        end.
        release it-pre-emb.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-pre-fat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-pre-fat FIELDS() use-index item-fat where it-pre-fat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-pre-fat' + STRING(i-lidos)).
            delete it-pre-fat.
        end.
        release it-pre-fat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-proc-siscomex' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.it-proc-siscomex where it-proc-siscomex.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-proc-siscomex FIELDS() where it-proc-siscomex.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-proc-siscomex' + STRING(i-lidos)).
            delete it-proc-siscomex.
        end.
        release it-proc-siscomex.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-res-carac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-res-carac FIELDS() use-index codigo where it-res-carac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-res-carac' + STRING(i-lidos)).
            delete it-res-carac.
        end.
        release it-res-carac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-rot-conf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.it-rot-conf where it-rot-conf.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-rot-conf FIELDS() where it-rot-conf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-rot-conf' + STRING(i-lidos)).
            delete it-rot-conf.
        end.
        release it-rot-conf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-ult-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-ult-ent FIELDS() use-index ch-it-dt where it-ult-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-ult-ent' + STRING(i-lidos)).
            delete it-ult-ent.
        end.
        release it-ult-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-ult-est' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-ult-est FIELDS() use-index ch-it-dt where it-ult-est.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-ult-est' + STRING(i-lidos)).
            delete it-ult-est.
        end.
        release it-ult-est.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-abc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-abc where item-abc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-abc FIELDS() where item-abc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-abc' + STRING(i-lidos)).
            delete item-abc.
        end.
        release item-abc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-ap' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-ap where item-ap.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-ap FIELDS() where item-ap.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-ap' + STRING(i-lidos)).
            delete item-ap.
        end.
        release item-ap.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-caixa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-caixa FIELDS() use-index ch-item where item-caixa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-caixa' + STRING(i-lidos)).
            delete item-caixa.
        end.
        release item-caixa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-cex' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-cex FIELDS() use-index ch-item where item-cex.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-cex' + STRING(i-lidos)).
            delete item-cex.
        end.
        release item-cex.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-cli' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-cli FIELDS() use-index ch-item-cli where item-cli.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-cli' + STRING(i-lidos)).
            delete item-cli.
        end.
        release item-cli.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-cli-caixa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-cli-caixa FIELDS() use-index ch-item-cli where item-cli-caixa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-cli-caixa' + STRING(i-lidos)).
            delete item-cli-caixa.
        end.
        release item-cli-caixa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-cli-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-cli-estab FIELDS() use-index ch-item-cli--est where item-cli-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-cli-estab' + STRING(i-lidos)).
            delete item-cli-estab.
        end.
        release item-cli-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-contrat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-contrat FIELDS() use-index item-narrat where item-contrat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-contrat' + STRING(i-lidos)).
            delete item-contrat.
        end.
        release item-contrat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-contrat-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-contrat-estab where item-contrat-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-contrat-estab FIELDS() where item-contrat-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-contrat-estab' + STRING(i-lidos)).
            delete item-contrat-estab.
        end.
        release item-contrat-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-cota' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-cota FIELDS() use-index codigo where item-cota.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-cota' + STRING(i-lidos)).
            delete item-cota.
        end.
        release item-cota.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-depos-roman' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-depos-roman where item-depos-roman.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-depos-roman FIELDS() where item-depos-roman.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-depos-roman' + STRING(i-lidos)).
            delete item-depos-roman.
        end.
        release item-depos-roman.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-desenho' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-desenho FIELDS() use-index item where item-desenho.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-desenho' + STRING(i-lidos)).
            delete item-desenho.
        end.
        release item-desenho.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-dist' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-dist FIELDS() use-index item where item-dist.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-dist' + STRING(i-lidos)).
            delete item-dist.
        end.
        release item-dist.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-doc-orig-nfe' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-doc-orig-nfe where item-doc-orig-nfe.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-doc-orig-nfe FIELDS() where item-doc-orig-nfe.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-doc-orig-nfe' + STRING(i-lidos)).
            delete item-doc-orig-nfe.
        end.
        release item-doc-orig-nfe.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-docto-import' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-docto-import FIELDS() use-index itmdctmp-01 where item-docto-import.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-docto-import' + STRING(i-lidos)).
            delete item-docto-import.
        end.
        release item-docto-import.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-docto-orig-cte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-docto-orig-cte where item-docto-orig-cte.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-docto-orig-cte FIELDS() where item-docto-orig-cte.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-docto-orig-cte' + STRING(i-lidos)).
            delete item-docto-orig-cte.
        end.
        release item-docto-orig-cte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-estrut-draw' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-estrut-draw where item-estrut-draw.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-estrut-draw FIELDS() where item-estrut-draw.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-estrut-draw' + STRING(i-lidos)).
            delete item-estrut-draw.
        end.
        release item-estrut-draw.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-estrut-draw' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-estrut-draw where item-estrut-draw.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-estrut-draw FIELDS() where item-estrut-draw.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-estrut-draw' + STRING(i-lidos)).
            delete item-estrut-draw.
        end.
        release item-estrut-draw.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-estrut-draw' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-estrut-draw where item-estrut-draw.it-codigo-similar = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-estrut-draw FIELDS() where item-estrut-draw.it-codigo-similar = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-estrut-draw' + STRING(i-lidos)).
            delete item-estrut-draw.
        end.
        release item-estrut-draw.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-export' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-export where item-export.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-export FIELDS() where item-export.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-export' + STRING(i-lidos)).
            delete item-export.
        end.
        release item-export.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-fornec' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-fornec FIELDS() use-index onde-compra where item-fornec.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-fornec' + STRING(i-lidos)).
            delete item-fornec.
        end.
        release item-fornec.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-fornec-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-fornec-estab FIELDS() use-index item-estab where item-fornec-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-fornec-estab' + STRING(i-lidos)).
            delete item-fornec-estab.
        end.
        release item-fornec-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-fornec-fabrican' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-fornec-fabrican FIELDS() use-index itmfrncf-id where item-fornec-fabrican.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-fornec-fabrican' + STRING(i-lidos)).
            delete item-fornec-fabrican.
        end.
        release item-fornec-fabrican.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-invest' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-invest FIELDS() use-index item where item-invest.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-invest' + STRING(i-lidos)).
            delete item-invest.
        end.
        release item-invest.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-linha' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-linha FIELDS() use-index item where item-linha.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-linha' + STRING(i-lidos)).
            delete item-linha.
        end.
        release item-linha.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-man' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-man FIELDS() use-index codigo where item-man.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-man' + STRING(i-lidos)).
            delete item-man.
        end.
        release item-man.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-man-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-man-estab FIELDS() use-index item where item-man-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-man-estab' + STRING(i-lidos)).
            delete item-man-estab.
        end.
        release item-man-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-mat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-mat FIELDS() use-index Item where item-mat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-mat' + STRING(i-lidos)).
            delete item-mat.
        end.
        release item-mat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-mat-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-mat-estab FIELDS() use-index codigo where item-mat-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-mat-estab' + STRING(i-lidos)).
            delete item-mat-estab.
        end.
        release item-mat-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-re' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-re where item-re.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-re FIELDS() where item-re.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-re' + STRING(i-lidos)).
            delete item-re.
        end.
        release item-re.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-ref-nfe' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-ref-nfe where item-ref-nfe.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-ref-nfe FIELDS() where item-ref-nfe.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-ref-nfe' + STRING(i-lidos)).
            delete item-ref-nfe.
        end.
        release item-ref-nfe.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-roteiro' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-roteiro FIELDS() use-index codigo where item-roteiro.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-roteiro' + STRING(i-lidos)).
            delete item-roteiro.
        end.
        release item-roteiro.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-tab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-tab FIELDS() use-index item-tab where item-tab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-tab' + STRING(i-lidos)).
            delete item-tab.
        end.
        release item-tab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-uf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-uf FIELDS() use-index ch-item-uf where item-uf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-uf' + STRING(i-lidos)).
            delete item-uf.
        end.
        release item-uf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-un-altern' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-un-altern FIELDS() use-index id where item-un-altern.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-un-altern' + STRING(i-lidos)).
            delete item-un-altern.
        end.
        release item-un-altern.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-uni-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-uni-estab FIELDS() use-index codigo where item-uni-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-uni-estab' + STRING(i-lidos)).
            delete item-uni-estab.
        end.
        release item-uni-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-unid-venda' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-unid-venda FIELDS() use-index ch-item where item-unid-venda.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-unid-venda' + STRING(i-lidos)).
            delete item-unid-venda.
        end.
        release item-unid-venda.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'jurisdic-aliq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.jurisdic-aliq where jurisdic-aliq.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.jurisdic-aliq FIELDS() where jurisdic-aliq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.jurisdic-aliq' + STRING(i-lidos)).
            delete jurisdic-aliq.
        end.
        release jurisdic-aliq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lin-texto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lin-texto FIELDS() use-index codigo where lin-texto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lin-texto' + STRING(i-lidos)).
            delete lin-texto.
        end.
        release lin-texto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lista-compon-for' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lista-compon-for FIELDS() use-index id where lista-compon-for.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lista-compon-for' + STRING(i-lidos)).
            delete lista-compon-for.
        end.
        release lista-compon-for.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lista-compon-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lista-compon-item FIELDS() use-index id where lista-compon-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lista-compon-item' + STRING(i-lidos)).
            delete lista-compon-item.
        end.
        release lista-compon-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lista-compon-reg' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lista-compon-reg FIELDS() use-index id where lista-compon-reg.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lista-compon-reg' + STRING(i-lidos)).
            delete lista-compon-reg.
        end.
        release lista-compon-reg.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'log-custo-pad' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.log-custo-pad FIELDS() use-index log-item-estab where log-custo-pad.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.log-custo-pad' + STRING(i-lidos)).
            delete log-custo-pad.
        end.
        release log-custo-pad.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'log-ordem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.log-ordem where log-ordem.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.log-ordem FIELDS() where log-ordem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.log-ordem' + STRING(i-lidos)).
            delete log-ordem.
        end.
        release log-ordem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote FIELDS() use-index codigo where lote.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote' + STRING(i-lidos)).
            delete lote.
        end.
        release lote.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-comp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-comp FIELDS() use-index item where lote-comp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-comp' + STRING(i-lidos)).
            delete lote-comp.
        end.
        release lote-comp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-comp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-comp FIELDS() use-index item-comp where lote-comp.it-codigo-comp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-comp' + STRING(i-lidos)).
            delete lote-comp.
        end.
        release lote-comp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-compr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-compr FIELDS() use-index item where lote-compr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-compr' + STRING(i-lidos)).
            delete lote-compr.
        end.
        release lote-compr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-lin-tex' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-lin-tex FIELDS() use-index codigo where lote-lin-tex.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-lin-tex' + STRING(i-lidos)).
            delete lote-lin-tex.
        end.
        release lote-lin-tex.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-prod FIELDS() use-index item-refer where lote-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-prod' + STRING(i-lidos)).
            delete lote-prod.
        end.
        release lote-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-res-carac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-res-carac FIELDS() use-index codigo where lote-res-carac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-res-carac' + STRING(i-lidos)).
            delete lote-res-carac.
        end.
        release lote-res-carac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-texto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-texto FIELDS() use-index codigo where lote-texto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-texto' + STRING(i-lidos)).
            delete lote-texto.
        end.
        release lote-texto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lote-vend' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.lote-vend FIELDS() use-index item where lote-vend.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.lote-vend' + STRING(i-lidos)).
            delete lote-vend.
        end.
        release lote-vend.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mat-comp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mat-comp FIELDS() use-index item where mat-comp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mat-comp' + STRING(i-lidos)).
            delete mat-comp.
        end.
        release mat-comp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'matriz-rat-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.matriz-rat-item where matriz-rat-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.matriz-rat-item FIELDS() where matriz-rat-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.matriz-rat-item' + STRING(i-lidos)).
            delete matriz-rat-item.
        end.
        release matriz-rat-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'memorando-movto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.memorando-movto where memorando-movto.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.memorando-movto FIELDS() where memorando-movto.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.memorando-movto' + STRING(i-lidos)).
            delete memorando-movto.
        end.
        release memorando-movto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mi-pl-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mi-pl-item FIELDS() use-index codigo where mi-pl-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mi-pl-item' + STRING(i-lidos)).
            delete mi-pl-item.
        end.
        release mi-pl-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mi-pl-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mi-pl-per FIELDS() use-index codigo where mi-pl-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mi-pl-per' + STRING(i-lidos)).
            delete mi-pl-per.
        end.
        release mi-pl-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mla-lista-aprov-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.mla-lista-aprov-item where mla-lista-aprov-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mla-lista-aprov-item FIELDS() where mla-lista-aprov-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mla-lista-aprov-item' + STRING(i-lidos)).
            delete mla-lista-aprov-item.
        end.
        release mla-lista-aprov-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mla-tipo-aprov-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.mla-tipo-aprov-item where mla-tipo-aprov-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mla-tipo-aprov-item FIELDS() where mla-tipo-aprov-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mla-tipo-aprov-item' + STRING(i-lidos)).
            delete mla-tipo-aprov-item.
        end.
        release mla-tipo-aprov-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-consumo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-consumo FIELDS() use-index item-conta where mnt-consumo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-consumo' + STRING(i-lidos)).
            delete mnt-consumo.
        end.
        release mnt-consumo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-custo-data' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-custo-data FIELDS() use-index item where mnt-custo-data.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-custo-data' + STRING(i-lidos)).
            delete mnt-custo-data.
        end.
        release mnt-custo-data.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-item FIELDS() use-index codigo where mnt-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-item' + STRING(i-lidos)).
            delete mnt-item.
        end.
        release mnt-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-lote' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-lote FIELDS() use-index codigo where mnt-lote.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-lote' + STRING(i-lidos)).
            delete mnt-lote.
        end.
        release mnt-lote.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-narrativa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-narrativa FIELDS() use-index codigo where mnt-narrativa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-narrativa' + STRING(i-lidos)).
            delete mnt-narrativa.
        end.
        release mnt-narrativa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-pend-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-pend-item FIELDS() use-index item-modulo where mnt-pend-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-pend-item' + STRING(i-lidos)).
            delete mnt-pend-item.
        end.
        release mnt-pend-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-ref-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.mnt-ref-item FIELDS() use-index item where mnt-ref-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.mnt-ref-item' + STRING(i-lidos)).
            delete mnt-ref-item.
        end.
        release mnt-ref-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-apr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.movto-apr where movto-apr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.movto-apr FIELDS() where movto-apr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.movto-apr' + STRING(i-lidos)).
            delete movto-apr.
        end.
        release movto-apr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-ato' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.movto-ato where movto-ato.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.movto-ato FIELDS() where movto-ato.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.movto-ato' + STRING(i-lidos)).
            delete movto-ato.
        end.
        release movto-ato.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-ato' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.movto-ato where movto-ato.it-codigo-re = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.movto-ato FIELDS() where movto-ato.it-codigo-re = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.movto-ato' + STRING(i-lidos)).
            delete movto-ato.
        end.
        release movto-ato.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-ato' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.movto-ato where movto-ato.it-codigo-similar = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.movto-ato FIELDS() where movto-ato.it-codigo-similar = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.movto-ato' + STRING(i-lidos)).
            delete movto-ato.
        end.
        release movto-ato.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-inv' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.movto-inv where movto-inv.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.movto-inv FIELDS() where movto-inv.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.movto-inv' + STRING(i-lidos)).
            delete movto-inv.
        end.
        release movto-inv.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-nf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.movto-nf where movto-nf.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.movto-nf FIELDS() where movto-nf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.movto-nf' + STRING(i-lidos)).
            delete movto-nf.
        end.
        release movto-nf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'multa-contrat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.multa-contrat where multa-contrat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.multa-contrat FIELDS() where multa-contrat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.multa-contrat' + STRING(i-lidos)).
            delete multa-contrat.
        end.
        release multa-contrat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'narr-plano' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.narr-plano where narr-plano.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.narr-plano FIELDS() where narr-plano.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.narr-plano' + STRING(i-lidos)).
            delete narr-plano.
        end.
        release narr-plano.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'narrativa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.narrativa FIELDS() use-index codigo where narrativa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.narrativa' + STRING(i-lidos)).
            delete narrativa.
        end.
        release narrativa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'nbm-re' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.nbm-re FIELDS() use-index ch-item where nbm-re.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.nbm-re' + STRING(i-lidos)).
            delete nbm-re.
        end.
        release nbm-re.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'nec-brutas' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.nec-brutas FIELDS() use-index item where nec-brutas.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.nec-brutas' + STRING(i-lidos)).
            delete nec-brutas.
        end.
        release nec-brutas.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'nec-calc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.nec-calc FIELDS() use-index item where nec-calc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.nec-calc' + STRING(i-lidos)).
            delete nec-calc.
        end.
        release nec-calc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'necessidade-oc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.necessidade-oc FIELDS() use-index item where necessidade-oc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.necessidade-oc' + STRING(i-lidos)).
            delete necessidade-oc.
        end.
        release necessidade-oc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'op-ord-exam' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.op-ord-exam where op-ord-exam.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.op-ord-exam FIELDS() where op-ord-exam.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.op-ord-exam' + STRING(i-lidos)).
            delete op-ord-exam.
        end.
        release op-ord-exam.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'oper-conf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.oper-conf where oper-conf.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.oper-conf FIELDS() where oper-conf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.oper-conf' + STRING(i-lidos)).
            delete oper-conf.
        end.
        release oper-conf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'oper-exam' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.oper-exam FIELDS() use-index codigo where oper-exam.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.oper-exam' + STRING(i-lidos)).
            delete oper-exam.
        end.
        release oper-exam.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'oper-ord-pa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.oper-ord-pa where oper-ord-pa.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.oper-ord-pa FIELDS() where oper-ord-pa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.oper-ord-pa' + STRING(i-lidos)).
            delete oper-ord-pa.
        end.
        release oper-ord-pa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'operacao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.operacao FIELDS() use-index codigo where operacao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.operacao' + STRING(i-lidos)).
            delete operacao.
        end.
        release operacao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-aber' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ord-aber where ord-aber.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ord-aber FIELDS() where ord-aber.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ord-aber' + STRING(i-lidos)).
            delete ord-aber.
        end.
        release ord-aber.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ord-ped where ord-ped.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ord-ped FIELDS() where ord-ped.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ord-ped' + STRING(i-lidos)).
            delete ord-ped.
        end.
        release ord-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-plan-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ord-plan-item FIELDS() use-index item-taref where ord-plan-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ord-plan-item' + STRING(i-lidos)).
            delete ord-plan-item.
        end.
        release ord-plan-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-planej-pa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ord-planej-pa where ord-planej-pa.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ord-planej-pa FIELDS() where ord-planej-pa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ord-planej-pa' + STRING(i-lidos)).
            delete ord-planej-pa.
        end.
        release ord-planej-pa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-planej-sws' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ord-planej-sws FIELDS() use-index codigo where ord-planej-sws.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ord-planej-sws' + STRING(i-lidos)).
            delete ord-planej-sws.
        end.
        release ord-planej-sws.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ordem-man-mov' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ordem-man-mov FIELDS() use-index ordmntmv-08 where ordem-man-mov.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ordem-man-mov' + STRING(i-lidos)).
            delete ordem-man-mov.
        end.
        release ordem-man-mov.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ordem-reprog' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ordem-reprog where ordem-reprog.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ordem-reprog FIELDS() where ordem-reprog.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ordem-reprog' + STRING(i-lidos)).
            delete ordem-reprog.
        end.
        release ordem-reprog.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'os-requis' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.os-requis where os-requis.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.os-requis FIELDS() where os-requis.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.os-requis' + STRING(i-lidos)).
            delete os-requis.
        end.
        release os-requis.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'par-cota' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.par-cota where par-cota.it-codigo-padrao = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.par-cota FIELDS() where par-cota.it-codigo-padrao = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.par-cota' + STRING(i-lidos)).
            delete par-cota.
        end.
        release par-cota.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-contrat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.param-contrat where param-contrat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-contrat FIELDS() where param-contrat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-contrat' + STRING(i-lidos)).
            delete param-contrat.
        end.
        release param-contrat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-cq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.param-cq where param-cq.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-cq FIELDS() where param-cq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-cq' + STRING(i-lidos)).
            delete param-cq.
        end.
        release param-cq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-dp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.param-dp where param-dp.it-codigo-dp = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-dp FIELDS() where param-dp.it-codigo-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-dp' + STRING(i-lidos)).
            delete param-dp.
        end.
        release param-dp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-dp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.param-dp where param-dp.it-codigo-pend = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-dp FIELDS() where param-dp.it-codigo-pend = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-dp' + STRING(i-lidos)).
            delete param-dp.
        end.
        release param-dp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-item-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-item-estab FIELDS() use-index codigo where param-item-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-item-estab' + STRING(i-lidos)).
            delete param-item-estab.
        end.
        release param-item-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-mi' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.param-mi where param-mi.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-mi FIELDS() where param-mi.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-mi' + STRING(i-lidos)).
            delete param-mi.
        end.
        release param-mi.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-config' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-config where ped-config.it-codigo-filho = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-config FIELDS() where ped-config.it-codigo-filho = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-config' + STRING(i-lidos)).
            delete ped-config.
        end.
        release ped-config.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-config' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-config where ped-config.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-config FIELDS() where ped-config.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-config' + STRING(i-lidos)).
            delete ped-config.
        end.
        release ped-config.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-curva' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-curva where ped-curva.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-curva FIELDS() where ped-curva.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-curva' + STRING(i-lidos)).
            delete ped-curva.
        end.
        release ped-curva.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-ent-desp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-ent-desp FIELDS() use-index pdntrgds-01 where ped-ent-desp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-ent-desp' + STRING(i-lidos)).
            delete ped-ent-desp.
        end.
        release ped-ent-desp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-est-it' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-est-it where ped-est-it.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-est-it FIELDS() where ped-est-it.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-est-it' + STRING(i-lidos)).
            delete ped-est-it.
        end.
        release ped-est-it.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-hist' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-hist where ped-hist.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-hist FIELDS() where ped-hist.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-hist' + STRING(i-lidos)).
            delete ped-hist.
        end.
        release ped-hist.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-planej-sws' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-planej-sws FIELDS() use-index ch-item where ped-planej-sws.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-planej-sws' + STRING(i-lidos)).
            delete ped-planej-sws.
        end.
        release ped-planej-sws.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-pmp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-pmp FIELDS() use-index ch-item where ped-pmp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-pmp' + STRING(i-lidos)).
            delete ped-pmp.
        end.
        release ped-pmp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-saldo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-saldo FIELDS() use-index ch-item where ped-saldo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-saldo' + STRING(i-lidos)).
            delete ped-saldo.
        end.
        release ped-saldo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-sdo-terc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-sdo-terc where ped-sdo-terc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-sdo-terc FIELDS() where ped-sdo-terc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-sdo-terc' + STRING(i-lidos)).
            delete ped-sdo-terc.
        end.
        release ped-sdo-terc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pedido-drb-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pedido-drb-item where pedido-drb-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pedido-drb-item FIELDS() where pedido-drb-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pedido-drb-item' + STRING(i-lidos)).
            delete pedido-drb-item.
        end.
        release pedido-drb-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pend-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pend-item FIELDS() use-index item-modulo where pend-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pend-item' + STRING(i-lidos)).
            delete pend-item.
        end.
        release pend-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pend-item-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pend-item-estab FIELDS() use-index item where pend-item-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pend-item-estab' + STRING(i-lidos)).
            delete pend-item-estab.
        end.
        release pend-item-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pert-crp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pert-crp FIELDS() use-index codigo where pert-crp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pert-crp' + STRING(i-lidos)).
            delete pert-crp.
        end.
        release pert-crp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pi-res-aber' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pi-res-aber where pi-res-aber.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pi-res-aber FIELDS() where pi-res-aber.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pi-res-aber' + STRING(i-lidos)).
            delete pi-res-aber.
        end.
        release pi-res-aber.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pl-carga-maq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pl-carga-maq where pl-carga-maq.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pl-carga-maq FIELDS() where pl-carga-maq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pl-carga-maq' + STRING(i-lidos)).
            delete pl-carga-maq.
        end.
        release pl-carga-maq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pl-ext-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pl-ext-item where pl-ext-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pl-ext-item FIELDS() where pl-ext-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pl-ext-item' + STRING(i-lidos)).
            delete pl-ext-item.
        end.
        release pl-ext-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pl-it-calc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pl-it-calc where pl-it-calc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pl-it-calc FIELDS() where pl-it-calc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pl-it-calc' + STRING(i-lidos)).
            delete pl-it-calc.
        end.
        release pl-it-calc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pl-it-calc-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pl-it-calc-estab where pl-it-calc-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pl-it-calc-estab FIELDS() where pl-it-calc-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pl-it-calc-estab' + STRING(i-lidos)).
            delete pl-it-calc-estab.
        end.
        release pl-it-calc-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pl-it-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pl-it-prod where pl-it-prod.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pl-it-prod FIELDS() where pl-it-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pl-it-prod' + STRING(i-lidos)).
            delete pl-it-prod.
        end.
        release pl-it-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pl-saldo-estoq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pl-saldo-estoq FIELDS() use-index codigo where pl-saldo-estoq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pl-saldo-estoq' + STRING(i-lidos)).
            delete pl-saldo-estoq.
        end.
        release pl-saldo-estoq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'plano-aprov' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.plano-aprov where plano-aprov.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.plano-aprov FIELDS() where plano-aprov.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.plano-aprov' + STRING(i-lidos)).
            delete plano-aprov.
        end.
        release plano-aprov.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pm-al-it' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pm-al-it where pm-al-it.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pm-al-it FIELDS() where pm-al-it.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pm-al-it' + STRING(i-lidos)).
            delete pm-al-it.
        end.
        release pm-al-it.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pm-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pm-item FIELDS() use-index item where pm-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pm-item' + STRING(i-lidos)).
            delete pm-item.
        end.
        release pm-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pm-item-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pm-item-per where pm-item-per.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pm-item-per FIELDS() where pm-item-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pm-item-per' + STRING(i-lidos)).
            delete pm-item-per.
        end.
        release pm-item-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pm-item-per-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pm-item-per-estab where pm-item-per-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pm-item-per-estab FIELDS() where pm-item-per-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pm-item-per-estab' + STRING(i-lidos)).
            delete pm-item-per-estab.
        end.
        release pm-item-per-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pm-repro' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pm-repro FIELDS() use-index origem where pm-repro.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pm-repro' + STRING(i-lidos)).
            delete pm-repro.
        end.
        release pm-repro.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pr-venda' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.pr-venda where pr-venda.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.pr-venda FIELDS() where pr-venda.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.pr-venda' + STRING(i-lidos)).
            delete pr-venda.
        end.
        release pr-venda.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'preco-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.preco-item FIELDS() use-index ch-itemtab where preco-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.preco-item' + STRING(i-lidos)).
            delete preco-item.
        end.
        release preco-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'preco-pad' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.preco-pad where preco-pad.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.preco-pad FIELDS() where preco-pad.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.preco-pad' + STRING(i-lidos)).
            delete preco-pad.
        end.
        release preco-pad.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proc-container-emb' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proc-container-emb where proc-container-emb.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proc-container-emb FIELDS() where proc-container-emb.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proc-container-emb' + STRING(i-lidos)).
            delete proc-container-emb.
        end.
        release proc-container-emb.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proc-it-nota-desp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proc-it-nota-desp FIELDS() use-index prcstmna-1 where proc-it-nota-desp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proc-it-nota-desp' + STRING(i-lidos)).
            delete proc-it-nota-desp.
        end.
        release proc-it-nota-desp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proc-it-nota-fisc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proc-it-nota-fisc where proc-it-nota-fisc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proc-it-nota-fisc FIELDS() where proc-it-nota-fisc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proc-it-nota-fisc' + STRING(i-lidos)).
            delete proc-it-nota-fisc.
        end.
        release proc-it-nota-fisc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proc-lote-ped-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proc-lote-ped-ent where proc-lote-ped-ent.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proc-lote-ped-ent FIELDS() where proc-lote-ped-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proc-lote-ped-ent' + STRING(i-lidos)).
            delete proc-lote-ped-ent.
        end.
        release proc-lote-ped-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proc-ped-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proc-ped-ent where proc-ped-ent.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proc-ped-ent FIELDS() where proc-ped-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proc-ped-ent' + STRING(i-lidos)).
            delete proc-ped-ent.
        end.
        release proc-ped-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proc-ped-ent-desp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proc-ped-ent-desp FIELDS() use-index prcspdna-2 where proc-ped-ent-desp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proc-ped-ent-desp' + STRING(i-lidos)).
            delete proc-ped-ent-desp.
        end.
        release proc-ped-ent-desp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proces-acond-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proces-acond-item where proces-acond-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proces-acond-item FIELDS() where proces-acond-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proces-acond-item' + STRING(i-lidos)).
            delete proces-acond-item.
        end.
        release proces-acond-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proces-embal-volum' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proces-embal-volum where proces-embal-volum.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proces-embal-volum FIELDS() where proces-embal-volum.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proces-embal-volum' + STRING(i-lidos)).
            delete proces-embal-volum.
        end.
        release proces-embal-volum.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proces-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proces-item FIELDS() use-index id where proces-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proces-item' + STRING(i-lidos)).
            delete proces-item.
        end.
        release proces-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proces-reg' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proces-reg FIELDS() use-index id where proces-reg.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proces-reg' + STRING(i-lidos)).
            delete proces-reg.
        end.
        release proces-reg.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prod-composto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.prod-composto where prod-composto.it-codigo-filho = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.prod-composto FIELDS() where prod-composto.it-codigo-filho = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.prod-composto' + STRING(i-lidos)).
            delete prod-composto.
        end.
        release prod-composto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prod-composto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.prod-composto FIELDS() use-index ch-codigo where prod-composto.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.prod-composto' + STRING(i-lidos)).
            delete prod-composto.
        end.
        release prod-composto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'produto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.produto where produto.it-codigo-cs = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.produto FIELDS() where produto.it-codigo-cs = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.produto' + STRING(i-lidos)).
            delete produto.
        end.
        release produto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'produto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.produto where produto.it-codigo-ref = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.produto FIELDS() where produto.it-codigo-ref = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.produto' + STRING(i-lidos)).
            delete produto.
        end.
        release produto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'produto-coml' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.produto-coml where produto-coml.it-codigo-cs = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.produto-coml FIELDS() where produto-coml.it-codigo-cs = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.produto-coml' + STRING(i-lidos)).
            delete produto-coml.
        end.
        release produto-coml.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prog-entr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.prog-entr where prog-entr.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.prog-entr FIELDS() where prog-entr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.prog-entr' + STRING(i-lidos)).
            delete prog-entr.
        end.
        release prog-entr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'quota' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.quota FIELDS() use-index ch-data where quota.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.quota' + STRING(i-lidos)).
            delete quota.
        end.
        release quota.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rec-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rec-item FIELDS() use-index item-rec where rec-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rec-item' + STRING(i-lidos)).
            delete rec-item.
        end.
        release rec-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'recur-sec-operac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.recur-sec-operac FIELDS() use-index rcrscprc-item where recur-sec-operac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.recur-sec-operac' + STRING(i-lidos)).
            delete recur-sec-operac.
        end.
        release recur-sec-operac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'recurso' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.recurso FIELDS() use-index item where recurso.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.recurso' + STRING(i-lidos)).
            delete recurso.
        end.
        release recurso.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rede-pert' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rede-pert FIELDS() use-index rede where rede-pert.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rede-pert' + STRING(i-lidos)).
            delete rede-pert.
        end.
        release rede-pert.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ref-estrut FIELDS() use-index refer-it where ref-estrut.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ref-estrut' + STRING(i-lidos)).
            delete ref-estrut.
        end.
        release ref-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ref-item FIELDS() use-index item where ref-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ref-item' + STRING(i-lidos)).
            delete ref-item.
        end.
        release ref-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'reg-export-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.reg-export-item FIELDS() use-index rgxprttm-01 where reg-export-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.reg-export-item' + STRING(i-lidos)).
            delete reg-export-item.
        end.
        release reg-export-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'repr-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.repr-ped where repr-ped.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.repr-ped FIELDS() where repr-ped.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.repr-ped' + STRING(i-lidos)).
            delete repr-ped.
        end.
        release repr-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'repr-prev' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.repr-prev where repr-prev.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.repr-prev FIELDS() where repr-prev.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.repr-prev' + STRING(i-lidos)).
            delete repr-prev.
        end.
        release repr-prev.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'repres-perc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.repres-perc where repres-perc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.repres-perc FIELDS() where repres-perc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.repres-perc' + STRING(i-lidos)).
            delete repres-perc.
        end.
        release repres-perc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'res-aber' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.res-aber where res-aber.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.res-aber FIELDS() where res-aber.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.res-aber' + STRING(i-lidos)).
            delete res-aber.
        end.
        release res-aber.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'res-atp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.res-atp FIELDS() use-index item-data where res-atp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.res-atp' + STRING(i-lidos)).
            delete res-atp.
        end.
        release res-atp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'res-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.res-item FIELDS() use-index ch-item where res-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.res-item' + STRING(i-lidos)).
            delete res-item.
        end.
        release res-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'resum-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.resum-item FIELDS() use-index ch-cdrefer where resum-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.resum-item' + STRING(i-lidos)).
            delete resum-item.
        end.
        release resum-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ri-bem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ri-bem where ri-bem.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ri-bem FIELDS() where ri-bem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ri-bem' + STRING(i-lidos)).
            delete ri-bem.
        end.
        release ri-bem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ri-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ri-item FIELDS() use-index idx-item where ri-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ri-item' + STRING(i-lidos)).
            delete ri-item.
        end.
        release ri-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-alt-paral' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rot-alt-paral FIELDS() use-index codigo where rot-alt-paral.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rot-alt-paral' + STRING(i-lidos)).
            delete rot-alt-paral.
        end.
        release rot-alt-paral.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-altern' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rot-altern FIELDS() use-index codigo where rot-altern.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rot-altern' + STRING(i-lidos)).
            delete rot-altern.
        end.
        release rot-altern.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-it-conf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.rot-it-conf where rot-it-conf.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rot-it-conf FIELDS() where rot-it-conf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rot-it-conf' + STRING(i-lidos)).
            delete rot-it-conf.
        end.
        release rot-it-conf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rot-item FIELDS() use-index codigo where rot-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rot-item' + STRING(i-lidos)).
            delete rot-item.
        end.
        release rot-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-item-paral' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rot-item-paral FIELDS() use-index codigo where rot-item-paral.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rot-item-paral' + STRING(i-lidos)).
            delete rot-item-paral.
        end.
        release rot-item-paral.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rot-item-reg' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.rot-item-reg FIELDS() use-index id where rot-item-reg.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.rot-item-reg' + STRING(i-lidos)).
            delete rot-item-reg.
        end.
        release rot-item-reg.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'saldo-inv' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.saldo-inv where saldo-inv.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.saldo-inv FIELDS() where saldo-inv.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.saldo-inv' + STRING(i-lidos)).
            delete saldo-inv.
        end.
        release saldo-inv.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'simul-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.simul-ent FIELDS() use-index ch-item where simul-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.simul-ent' + STRING(i-lidos)).
            delete simul-ent.
        end.
        release simul-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'simul-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.simul-item where simul-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.simul-item FIELDS() where simul-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.simul-item' + STRING(i-lidos)).
            delete simul-item.
        end.
        release simul-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'simulacao-preco' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.simulacao-preco where simulacao-preco.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.simulacao-preco FIELDS() where simulacao-preco.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.simulacao-preco' + STRING(i-lidos)).
            delete simulacao-preco.
        end.
        release simulacao-preco.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sl-qt-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.sl-qt-per FIELDS() use-index item where sl-qt-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.sl-qt-per' + STRING(i-lidos)).
            delete sl-qt-per.
        end.
        release sl-qt-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sl-vl-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.sl-vl-per where sl-vl-per.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.sl-vl-per FIELDS() where sl-vl-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.sl-vl-per' + STRING(i-lidos)).
            delete sl-vl-per.
        end.
        release sl-vl-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tab-conver-veic' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tab-conver-veic FIELDS() use-index conv-id where tab-conver-veic.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tab-conver-veic' + STRING(i-lidos)).
            delete tab-conver-veic.
        end.
        release tab-conver-veic.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tar-item-eq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tar-item-eq FIELDS() use-index item-taref where tar-item-eq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tar-item-eq' + STRING(i-lidos)).
            delete tar-item-eq.
        end.
        release tar-item-eq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tar-item-eq-tag' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tar-item-eq-tag FIELDS() use-index item-taref where tar-item-eq-tag.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tar-item-eq-tag' + STRING(i-lidos)).
            delete tar-item-eq-tag.
        end.
        release tar-item-eq-tag.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'taref-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.taref-item FIELDS() use-index item-taref where taref-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.taref-item' + STRING(i-lidos)).
            delete taref-item.
        end.
        release taref-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tbpreco-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tbpreco-item FIELDS() use-index item where tbpreco-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tbpreco-item' + STRING(i-lidos)).
            delete tbpreco-item.
        end.
        release tbpreco-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tex-exame' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tex-exame FIELDS() use-index codigo where tex-exame.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tex-exame' + STRING(i-lidos)).
            delete tex-exame.
        end.
        release tex-exame.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'texto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.texto FIELDS() use-index codigo where texto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.texto' + STRING(i-lidos)).
            delete texto.
        end.
        release texto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tipo-aprov-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tipo-aprov-item FIELDS() use-index tipo-aprov-item-2 where tipo-aprov-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tipo-aprov-item' + STRING(i-lidos)).
            delete tipo-aprov-item.
        end.
        release tipo-aprov-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'traduc-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.traduc-item FIELDS() use-index pk-tr-item where traduc-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.traduc-item' + STRING(i-lidos)).
            delete traduc-item.
        end.
        release traduc-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'unid-neg-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.unid-neg-item FIELDS() use-index item-unid where unid-neg-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.unid-neg-item' + STRING(i-lidos)).
            delete unid-neg-item.
        end.
        release unid-neg-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'unid-negoc-depos' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.unid-negoc-depos FIELDS() use-index undngcdp-id where unid-negoc-depos.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.unid-negoc-depos' + STRING(i-lidos)).
            delete unid-negoc-depos.
        end.
        release unid-negoc-depos.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'valor-ult-ven' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.valor-ult-ven where valor-ult-ven.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.valor-ult-ven FIELDS() where valor-ult-ven.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.valor-ult-ven' + STRING(i-lidos)).
            delete valor-ult-ven.
        end.
        release valor-ult-ven.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wt-fat-ser-lote' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wt-fat-ser-lote where wt-fat-ser-lote.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wt-fat-ser-lote FIELDS() where wt-fat-ser-lote.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wt-fat-ser-lote' + STRING(i-lidos)).
            delete wt-fat-ser-lote.
        end.
        release wt-fat-ser-lote.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wt-it-docto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wt-it-docto where wt-it-docto.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wt-it-docto FIELDS() where wt-it-docto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wt-it-docto' + STRING(i-lidos)).
            delete wt-it-docto.
        end.
        release wt-it-docto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wt-item-sfa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wt-item-sfa where wt-item-sfa.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wt-item-sfa FIELDS() where wt-item-sfa.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wt-item-sfa' + STRING(i-lidos)).
            delete wt-item-sfa.
        end.
        release wt-item-sfa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-it-nota-fisc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-it-nota-fisc where zfm-it-nota-fisc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-it-nota-fisc FIELDS() where zfm-it-nota-fisc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-it-nota-fisc' + STRING(i-lidos)).
            delete zfm-it-nota-fisc.
        end.
        release zfm-it-nota-fisc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-item FIELDS() use-index zfmitem-id where zfm-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-item' + STRING(i-lidos)).
            delete zfm-item.
        end.
        release zfm-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-item-dcr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-item-dcr FIELDS() use-index zfmtmdcr-01 where zfm-item-dcr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-item-dcr' + STRING(i-lidos)).
            delete zfm-item-dcr.
        end.
        release zfm-item-dcr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-pli-oc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-pli-oc where zfm-pli-oc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-pli-oc FIELDS() where zfm-pli-oc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-pli-oc' + STRING(i-lidos)).
            delete zfm-pli-oc.
        end.
        release zfm-pli-oc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-sdo-insumo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-sdo-insumo FIELDS() use-index zfmsdnsm-id where zfm-sdo-insumo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-sdo-insumo' + STRING(i-lidos)).
            delete zfm-sdo-insumo.
        end.
        release zfm-sdo-insumo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-ult-entr-dcr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-ult-entr-dcr FIELDS() use-index zfmltntr-id where zfm-ult-entr-dcr.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-ult-entr-dcr' + STRING(i-lidos)).
            delete zfm-ult-entr-dcr.
        end.
        release zfm-ult-entr-dcr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'consu-aloc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.consu-aloc where consu-aloc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.consu-aloc FIELDS() where consu-aloc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.consu-aloc' + STRING(i-lidos)).
            delete consu-aloc.
        end.
        release consu-aloc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cot-rede-p' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.cot-rede-p where cot-rede-p.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.cot-rede-p FIELDS() where cot-rede-p.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.cot-rede-p' + STRING(i-lidos)).
            delete cot-rede-p.
        end.
        release cot-rede-p.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cot-relat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.cot-relat FIELDS() use-index id where cot-relat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.cot-relat' + STRING(i-lidos)).
            delete cot-relat.
        end.
        release cot-relat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'meta-produc-det' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.meta-produc-det FIELDS() use-index mtprdcdt-item where meta-produc-det.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.meta-produc-det' + STRING(i-lidos)).
            delete meta-produc-det.
        end.
        release meta-produc-det.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mgc-apont-calib' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.mgc-apont-calib where mgc-apont-calib.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.mgc-apont-calib FIELDS() where mgc-apont-calib.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.mgc-apont-calib' + STRING(i-lidos)).
            delete mgc-apont-calib.
        end.
        release mgc-apont-calib.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-aloca-reserva' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.mnt-aloca-reserva where mnt-aloca-reserva.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.mnt-aloca-reserva FIELDS() where mnt-aloca-reserva.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.mnt-aloca-reserva' + STRING(i-lidos)).
            delete mnt-aloca-reserva.
        end.
        release mnt-aloca-reserva.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-oper-ord' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.mnt-oper-ord FIELDS() use-index item where mnt-oper-ord.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.mnt-oper-ord' + STRING(i-lidos)).
            delete mnt-oper-ord.
        end.
        release mnt-oper-ord.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-ordem-reprog' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.mnt-ordem-reprog FIELDS() use-index codigo where mnt-ordem-reprog.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.mnt-ordem-reprog' + STRING(i-lidos)).
            delete mnt-ordem-reprog.
        end.
        release mnt-ordem-reprog.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mnt-reservas' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.mnt-reservas FIELDS() use-index data where mnt-reservas.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.mnt-reservas' + STRING(i-lidos)).
            delete mnt-reservas.
        end.
        release mnt-reservas.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'op-sfc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.op-sfc FIELDS() use-index ites where op-sfc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.op-sfc' + STRING(i-lidos)).
            delete op-sfc.
        end.
        release op-sfc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pend-import' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.pend-import where pend-import.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.pend-import FIELDS() where pend-import.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.pend-import' + STRING(i-lidos)).
            delete pend-import.
        end.
        release pend-import.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'promocao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.promocao FIELDS() use-index ch-codigo where promocao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.promocao' + STRING(i-lidos)).
            delete promocao.
        end.
        release promocao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rep-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.rep-prod FIELDS() use-index item-rep where rep-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.rep-prod' + STRING(i-lidos)).
            delete rep-prod.
        end.
        release rep-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rep-refugo-mat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.rep-refugo-mat where rep-refugo-mat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.rep-refugo-mat FIELDS() where rep-refugo-mat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.rep-refugo-mat' + STRING(i-lidos)).
            delete rep-refugo-mat.
        end.
        release rep-refugo-mat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'res-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.res-estab where res-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.res-estab FIELDS() where res-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.res-estab' + STRING(i-lidos)).
            delete res-estab.
        end.
        release res-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'resum-item-aloc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.resum-item-aloc where resum-item-aloc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.resum-item-aloc FIELDS() where resum-item-aloc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.resum-item-aloc' + STRING(i-lidos)).
            delete resum-item-aloc.
        end.
        release resum-item-aloc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'split-operac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.split-operac where split-operac.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.split-operac FIELDS() where split-operac.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.split-operac' + STRING(i-lidos)).
            delete split-operac.
        end.
        release split-operac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wt-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.wt-item FIELDS() use-index codigo where wt-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.wt-item' + STRING(i-lidos)).
            delete wt-item.
        end.
        release wt-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wt-ped-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.wt-ped-item where wt-ped-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.wt-ped-item FIELDS() where wt-ped-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.wt-ped-item' + STRING(i-lidos)).
            delete wt-ped-item.
        end.
        release wt-ped-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'calc-preco' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.calc-preco where calc-preco.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.calc-preco FIELDS() where calc-preco.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.calc-preco' + STRING(i-lidos)).
            delete calc-preco.
        end.
        release calc-preco.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctr-saldo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.ctr-saldo FIELDS() use-index ch-liberado where ctr-saldo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.ctr-saldo' + STRING(i-lidos)).
            delete ctr-saldo.
        end.
        release ctr-saldo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'devol-cli' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.devol-cli FIELDS() use-index ch-item where devol-cli.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.devol-cli' + STRING(i-lidos)).
            delete devol-cli.
        end.
        release devol-cli.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'eq-simul-carg-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.eq-simul-carg-ped FIELDS() use-index eqsmlcrb-01 where eq-simul-carg-ped.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.eq-simul-carg-ped' + STRING(i-lidos)).
            delete eq-simul-carg-ped.
        end.
        release eq-simul-carg-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'eq-simul-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.eq-simul-ped FIELDS() use-index eqsmlpd-01 where eq-simul-ped.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.eq-simul-ped' + STRING(i-lidos)).
            delete eq-simul-ped.
        end.
        release eq-simul-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ext-it-nota' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.ext-it-nota FIELDS() use-index ch-item where ext-it-nota.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.ext-it-nota' + STRING(i-lidos)).
            delete ext-it-nota.
        end.
        release ext-it-nota.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'fat-estat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.fat-estat FIELDS() use-index item where fat-estat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.fat-estat' + STRING(i-lidos)).
            delete fat-estat.
        end.
        release fat-estat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'fat-ser-lote' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.fat-ser-lote FIELDS() use-index item where fat-ser-lote.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.fat-ser-lote' + STRING(i-lidos)).
            delete fat-ser-lote.
        end.
        release fat-ser-lote.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'his-ped-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.his-ped-ent where his-ped-ent.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.his-ped-ent FIELDS() where his-ped-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.his-ped-ent' + STRING(i-lidos)).
            delete his-ped-ent.
        end.
        release his-ped-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-doc-fisc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-doc-fisc FIELDS() use-index ch-item where it-doc-fisc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-doc-fisc' + STRING(i-lidos)).
            delete it-doc-fisc.
        end.
        release it-doc-fisc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-nota-doc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.it-nota-doc where it-nota-doc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-nota-doc FIELDS() where it-nota-doc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-nota-doc' + STRING(i-lidos)).
            delete it-nota-doc.
        end.
        release it-nota-doc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-nota-fisc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-nota-fisc FIELDS() use-index ch-item-nota where it-nota-fisc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-nota-fisc' + STRING(i-lidos)).
            delete it-nota-fisc.
        end.
        release it-nota-fisc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-nota-imp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.it-nota-imp where it-nota-imp.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-nota-imp FIELDS() where it-nota-imp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-nota-imp' + STRING(i-lidos)).
            delete it-nota-imp.
        end.
        release it-nota-imp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-pedido-imp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-pedido-imp FIELDS() use-index impttmpd-02 where it-pedido-imp.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-pedido-imp' + STRING(i-lidos)).
            delete it-pedido-imp.
        end.
        release it-pedido-imp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-remito' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-remito FIELDS() use-index ch-codigo where it-remito.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-remito' + STRING(i-lidos)).
            delete it-remito.
        end.
        release it-remito.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-embal' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.item-embal where item-embal.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.item-embal FIELDS() where item-embal.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.item-embal' + STRING(i-lidos)).
            delete item-embal.
        end.
        release item-embal.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-nf-ifrs' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.item-nf-ifrs where item-nf-ifrs.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.item-nf-ifrs FIELDS() where item-nf-ifrs.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.item-nf-ifrs' + STRING(i-lidos)).
            delete item-nf-ifrs.
        end.
        release item-nf-ifrs.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-roman-faturam' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.item-roman-faturam where item-roman-faturam.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.item-roman-faturam FIELDS() where item-roman-faturam.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.item-roman-faturam' + STRING(i-lidos)).
            delete item-roman-faturam.
        end.
        release item-roman-faturam.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-armazem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.movto-armazem FIELDS() use-index mvtrmzm-01 where movto-armazem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.movto-armazem' + STRING(i-lidos)).
            delete movto-armazem.
        end.
        release movto-armazem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'nar-it-nota' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.nar-it-nota FIELDS() use-index ch-item where nar-it-nota.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.nar-it-nota' + STRING(i-lidos)).
            delete nar-it-nota.
        end.
        release nar-it-nota.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.ped-ent FIELDS() use-index planejamento where ped-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.ped-ent' + STRING(i-lidos)).
            delete ped-ent.
        end.
        release ped-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.ped-item FIELDS() use-index planejamento where ped-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.ped-item' + STRING(i-lidos)).
            delete ped-item.
        end.
        release ped-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prog-ent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.prog-ent where prog-ent.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.prog-ent FIELDS() where prog-ent.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.prog-ent' + STRING(i-lidos)).
            delete prog-ent.
        end.
        release prog-ent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prog-entreg-cham' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.prog-entreg-cham FIELDS() use-index prgntrga-02 where prog-entreg-cham.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.prog-entreg-cham' + STRING(i-lidos)).
            delete prog-entreg-cham.
        end.
        release prog-entreg-cham.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prog-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.prog-item where prog-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.prog-item FIELDS() where prog-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.prog-item' + STRING(i-lidos)).
            delete prog-item.
        end.
        release prog-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rateio-it-duplic' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.rateio-it-duplic where rateio-it-duplic.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.rateio-it-duplic FIELDS() where rateio-it-duplic.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.rateio-it-duplic' + STRING(i-lidos)).
            delete rateio-it-duplic.
        end.
        release rateio-it-duplic.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'retroat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.retroat where retroat.it-codigo-fim = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.retroat FIELDS() where retroat.it-codigo-fim = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.retroat' + STRING(i-lidos)).
            delete retroat.
        end.
        release retroat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'retroat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.retroat where retroat.it-codigo-inic = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.retroat FIELDS() where retroat.it-codigo-inic = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.retroat' + STRING(i-lidos)).
            delete retroat.
        end.
        release retroat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'retroat-clien-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.retroat-clien-item FIELDS() use-index rtrtclnt-04 where retroat-clien-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.retroat-clien-item' + STRING(i-lidos)).
            delete retroat-clien-item.
        end.
        release retroat-clien-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'retroat-pre-nf-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.retroat-pre-nf-item where retroat-pre-nf-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.retroat-pre-nf-item FIELDS() where retroat-pre-nf-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.retroat-pre-nf-item' + STRING(i-lidos)).
            delete retroat-pre-nf-item.
        end.
        release retroat-pre-nf-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ri-ext-it-nota-doc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.ri-ext-it-nota-doc where ri-ext-it-nota-doc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.ri-ext-it-nota-doc FIELDS() where ri-ext-it-nota-doc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.ri-ext-it-nota-doc' + STRING(i-lidos)).
            delete ri-ext-it-nota-doc.
        end.
        release ri-ext-it-nota-doc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'unid-neg-fat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.unid-neg-fat where unid-neg-fat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.unid-neg-fat FIELDS() where unid-neg-fat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.unid-neg-fat' + STRING(i-lidos)).
            delete unid-neg-fat.
        end.
        release unid-neg-fat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'unid-neg-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.unid-neg-ped where unid-neg-ped.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.unid-neg-ped FIELDS() where unid-neg-ped.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.unid-neg-ped' + STRING(i-lidos)).
            delete unid-neg-ped.
        end.
        release unid-neg-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'aloca-reserva' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.aloca-reserva where aloca-reserva.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.aloca-reserva FIELDS() where aloca-reserva.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.aloca-reserva' + STRING(i-lidos)).
            delete aloca-reserva.
        end.
        release aloca-reserva.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'componente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.componente FIELDS() use-index item where componente.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.componente' + STRING(i-lidos)).
            delete componente.
        end.
        release componente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'consumo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.consumo FIELDS() use-index item-periodo where consumo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.consumo' + STRING(i-lidos)).
            delete consumo.
        end.
        release consumo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'consumo-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.consumo-estab FIELDS() use-index item-estab where consumo-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.consumo-estab' + STRING(i-lidos)).
            delete consumo-estab.
        end.
        release consumo-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'controle-preco' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.controle-preco where controle-preco.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.controle-preco FIELDS() where controle-preco.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.controle-preco' + STRING(i-lidos)).
            delete controle-preco.
        end.
        release controle-preco.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cotacao-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.cotacao-item FIELDS() use-index item-data-ord where cotacao-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.cotacao-item' + STRING(i-lidos)).
            delete cotacao-item.
        end.
        release cotacao-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'devol-forn' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.devol-forn where devol-forn.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.devol-forn FIELDS() where devol-forn.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.devol-forn' + STRING(i-lidos)).
            delete devol-forn.
        end.
        release devol-forn.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'doc-pend-aprov' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.doc-pend-aprov where doc-pend-aprov.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.doc-pend-aprov FIELDS() where doc-pend-aprov.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.doc-pend-aprov' + STRING(i-lidos)).
            delete doc-pend-aprov.
        end.
        release doc-pend-aprov.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estat-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.estat-estab where estat-estab.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.estat-estab FIELDS() where estat-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.estat-estab' + STRING(i-lidos)).
            delete estat-estab.
        end.
        release estat-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'evento-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.evento-ped where evento-ped.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.evento-ped FIELDS() where evento-ped.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.evento-ped' + STRING(i-lidos)).
            delete evento-ped.
        end.
        release evento-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'exam-ficha' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.exam-ficha where exam-ficha.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.exam-ficha FIELDS() where exam-ficha.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.exam-ficha' + STRING(i-lidos)).
            delete exam-ficha.
        end.
        release exam-ficha.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ficha-cq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ficha-cq FIELDS() use-index item-fornec where ficha-cq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ficha-cq' + STRING(i-lidos)).
            delete ficha-cq.
        end.
        release ficha-cq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'hist-alter' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.hist-alter where hist-alter.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.hist-alter FIELDS() where hist-alter.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.hist-alter' + STRING(i-lidos)).
            delete hist-alter.
        end.
        release hist-alter.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-consumo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.it-consumo FIELDS() use-index item-estab where it-consumo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.it-consumo' + STRING(i-lidos)).
            delete it-consumo.
        end.
        release it-consumo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-doc-fisico' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.it-doc-fisico FIELDS() use-index item where it-doc-fisico.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.it-doc-fisico' + STRING(i-lidos)).
            delete it-doc-fisico.
        end.
        release it-doc-fisico.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-prog-for' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.it-prog-for where it-prog-for.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.it-prog-for FIELDS() where it-prog-for.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.it-prog-for' + STRING(i-lidos)).
            delete it-prog-for.
        end.
        release it-prog-for.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-requisicao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.it-requisicao FIELDS() use-index item-entreg where it-requisicao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.it-requisicao' + STRING(i-lidos)).
            delete it-requisicao.
        end.
        release it-requisicao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-romaneio' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.it-romaneio FIELDS() use-index it-codigo where it-romaneio.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.it-romaneio' + STRING(i-lidos)).
            delete it-romaneio.
        end.
        release it-romaneio.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-doc-est' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.item-doc-est FIELDS() use-index item where item-doc-est.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.item-doc-est' + STRING(i-lidos)).
            delete item-doc-est.
        end.
        release item-doc-est.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-estab' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.item-estab FIELDS() use-index item where item-estab.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.item-estab' + STRING(i-lidos)).
            delete item-estab.
        end.
        release item-estab.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-nat-oper' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.item-nat-oper FIELDS() use-index ch-codigo where item-nat-oper.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.item-nat-oper' + STRING(i-lidos)).
            delete item-nat-oper.
        end.
        release item-nat-oper.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-om' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.item-om FIELDS() use-index itemom-id where item-om.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.item-om' + STRING(i-lidos)).
            delete item-om.
        end.
        release item-om.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'medicao-contrat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.medicao-contrat where medicao-contrat.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.medicao-contrat FIELDS() where medicao-contrat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.medicao-contrat' + STRING(i-lidos)).
            delete medicao-contrat.
        end.
        release medicao-contrat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'mla-doc-pend-aprov' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.mla-doc-pend-aprov where mla-doc-pend-aprov.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.mla-doc-pend-aprov FIELDS() where mla-doc-pend-aprov.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.mla-doc-pend-aprov' + STRING(i-lidos)).
            delete mla-doc-pend-aprov.
        end.
        release mla-doc-pend-aprov.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-coprodut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.movto-coprodut where movto-coprodut.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-coprodut FIELDS() where movto-coprodut.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-coprodut' + STRING(i-lidos)).
            delete movto-coprodut.
        end.
        release movto-coprodut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-dir' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.movto-dir where movto-dir.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-dir FIELDS() where movto-dir.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-dir' + STRING(i-lidos)).
            delete movto-dir.
        end.
        release movto-dir.
    end.

    /*OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-estoq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-estoq FIELDS() use-index item-est-dep where movto-estoq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-estoq' + STRING(i-lidos)).
            delete movto-estoq.
        end.
        release movto-estoq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-ggf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.movto-ggf where movto-ggf.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-ggf FIELDS() where movto-ggf.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-ggf' + STRING(i-lidos)).
            delete movto-ggf.
        end.
        release movto-ggf.
    end.*/

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-mat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-mat FIELDS() use-index onde-usou where movto-mat.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-mat' + STRING(i-lidos)).
            delete movto-mat.
        end.
        release movto-mat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-pend' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.movto-pend where movto-pend.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-pend FIELDS() where movto-pend.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-pend' + STRING(i-lidos)).
            delete movto-pend.
        end.
        release movto-pend.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ocor-medio' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.ocor-medio where ocor-medio.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ocor-medio FIELDS() where ocor-medio.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ocor-medio' + STRING(i-lidos)).
            delete ocor-medio.
        end.
        release ocor-medio.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'oper-ord' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.oper-ord FIELDS() use-index item where oper-ord.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.oper-ord' + STRING(i-lidos)).
            delete oper-ord.
        end.
        release oper-ord.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-movto-period' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ord-movto-period FIELDS() use-index ordmvtpr-it-ord where ord-movto-period.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ord-movto-period' + STRING(i-lidos)).
            delete ord-movto-period.
        end.
        release ord-movto-period.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ord-prod FIELDS() use-index prod-repet where ord-prod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ord-prod' + STRING(i-lidos)).
            delete ord-prod.
        end.
        release ord-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ordem-compra' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ordem-compra FIELDS() use-index ord-servico where ordem-compra.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ordem-compra' + STRING(i-lidos)).
            delete ordem-compra.
        end.
        release ordem-compra.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pert-ordem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.pert-ordem where pert-ordem.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.pert-ordem FIELDS() where pert-ordem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.pert-ordem' + STRING(i-lidos)).
            delete pert-ordem.
        end.
        release pert-ordem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'pr-it-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.pr-it-per FIELDS() use-index codigo where pr-it-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.pr-it-per' + STRING(i-lidos)).
            delete pr-it-per.
        end.
        release pr-it-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prazo-compra' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.prazo-compra FIELDS() use-index planejamento where prazo-compra.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.prazo-compra' + STRING(i-lidos)).
            delete prazo-compra.
        end.
        release prazo-compra.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prazo-it-prog' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.prazo-it-prog where prazo-it-prog.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.prazo-it-prog FIELDS() where prazo-it-prog.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.prazo-it-prog' + STRING(i-lidos)).
            delete prazo-it-prog.
        end.
        release prazo-it-prog.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rat-componente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.rat-componente where rat-componente.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rat-componente FIELDS() where rat-componente.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rat-componente' + STRING(i-lidos)).
            delete rat-componente.
        end.
        release rat-componente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rat-lote' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.rat-lote where rat-lote.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rat-lote FIELDS() where rat-lote.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rat-lote' + STRING(i-lidos)).
            delete rat-lote.
        end.
        release rat-lote.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rat-saldo-terc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.rat-saldo-terc where rat-saldo-terc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rat-saldo-terc FIELDS() where rat-saldo-terc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rat-saldo-terc' + STRING(i-lidos)).
            delete rat-saldo-terc.
        end.
        release rat-saldo-terc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'recebimento' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.recebimento FIELDS() use-index item where recebimento.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.recebimento' + STRING(i-lidos)).
            delete recebimento.
        end.
        release recebimento.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-ordem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ref-ordem FIELDS() use-index item where ref-ordem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ref-ordem' + STRING(i-lidos)).
            delete ref-ordem.
        end.
        release ref-ordem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rep-oper' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.rep-oper where rep-oper.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rep-oper FIELDS() where rep-oper.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rep-oper' + STRING(i-lidos)).
            delete rep-oper.
        end.
        release rep-oper.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'req-altern' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.req-altern where req-altern.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.req-altern FIELDS() where req-altern.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.req-altern' + STRING(i-lidos)).
            delete req-altern.
        end.
        release req-altern.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'req-ord' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.req-ord where req-ord.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.req-ord FIELDS() where req-ord.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.req-ord' + STRING(i-lidos)).
            delete req-ord.
        end.
        release req-ord.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'req-sum' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.req-sum FIELDS() use-index item where req-sum.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.req-sum' + STRING(i-lidos)).
            delete req-sum.
        end.
        release req-sum.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'res-fic-cq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.res-fic-cq where res-fic-cq.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.res-fic-cq FIELDS() where res-fic-cq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.res-fic-cq' + STRING(i-lidos)).
            delete res-fic-cq.
        end.
        release res-fic-cq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'reservas' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.reservas FIELDS() use-index planejamento where reservas.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.reservas' + STRING(i-lidos)).
            delete reservas.
        end.
        release reservas.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'saldo-custo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.saldo-custo FIELDS() use-index ch-item where saldo-custo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.saldo-custo' + STRING(i-lidos)).
            delete saldo-custo.
        end.
        release saldo-custo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'saldo-estoq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.saldo-estoq FIELDS() use-index item-lote where saldo-estoq.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.saldo-estoq' + STRING(i-lidos)).
            delete saldo-estoq.
        end.
        release saldo-estoq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'saldo-op-config' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.saldo-op-config where saldo-op-config.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.saldo-op-config FIELDS() where saldo-op-config.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.saldo-op-config' + STRING(i-lidos)).
            delete saldo-op-config.
        end.
        release saldo-op-config.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'saldo-terc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.saldo-terc FIELDS() use-index item-est where saldo-terc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.saldo-terc' + STRING(i-lidos)).
            delete saldo-terc.
        end.
        release saldo-terc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sl-it-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.sl-it-per FIELDS() use-index item where sl-it-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.sl-it-per' + STRING(i-lidos)).
            delete sl-it-per.
        end.
        release sl-it-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sl-op-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.sl-op-per where sl-op-per.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.sl-op-per FIELDS() where sl-op-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.sl-op-per' + STRING(i-lidos)).
            delete sl-op-per.
        end.
        release sl-op-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sl-terc-per' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.sl-terc-per FIELDS() use-index item-emit where sl-terc-per.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.sl-terc-per' + STRING(i-lidos)).
            delete sl-terc-per.
        end.
        release sl-terc-per.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tex-ex-fic' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.tex-ex-fic where tex-ex-fic.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.tex-ex-fic FIELDS() where tex-ex-fic.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.tex-ex-fic' + STRING(i-lidos)).
            delete tex-ex-fic.
        end.
        release tex-ex-fic.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'texto-follow-up' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.texto-follow-up FIELDS() use-index txtfllwp-08 where texto-follow-up.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.texto-follow-up' + STRING(i-lidos)).
            delete texto-follow-up.
        end.
        release texto-follow-up.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'unid-neg-requis' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.unid-neg-requis where unid-neg-requis.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.unid-neg-requis FIELDS() where unid-neg-requis.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.unid-neg-requis' + STRING(i-lidos)).
            delete unid-neg-requis.
        end.
        release unid-neg-requis.
    end.

    /*OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctx-pendencias' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ctx-pendencias where ctx-pendencias.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ctx-pendencias FIELDS() where ctx-pendencias.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ctx-pendencias' + STRING(i-lidos)).
            delete ctx-pendencias.
        end.
        release ctx-pendencias.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctx-pendencias' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ctx-pendencias where ctx-pendencias.it-codigo-juncao = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ctx-pendencias FIELDS() where ctx-pendencias.it-codigo-juncao = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ctx-pendencias' + STRING(i-lidos)).
            delete ctx-pendencias.
        end.
        release ctx-pendencias.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctx-pendencias' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ctx-pendencias where ctx-pendencias.it-codigo-resolv = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ctx-pendencias FIELDS() where ctx-pendencias.it-codigo-resolv = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ctx-pendencias' + STRING(i-lidos)).
            delete ctx-pendencias.
        end.
        release ctx-pendencias.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctx-reporte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ctx-reporte where ctx-reporte.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ctx-reporte FIELDS() where ctx-reporte.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ctx-reporte' + STRING(i-lidos)).
            delete ctx-reporte.
        end.
        release ctx-reporte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctx-reporte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ctx-reporte where ctx-reporte.it-codigo-juncao = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ctx-reporte FIELDS() where ctx-reporte.it-codigo-juncao = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ctx-reporte' + STRING(i-lidos)).
            delete ctx-reporte.
        end.
        release ctx-reporte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ctx-reporte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ctx-reporte where ctx-reporte.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ctx-reporte FIELDS() where ctx-reporte.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ctx-reporte' + STRING(i-lidos)).
            delete ctx-reporte.
        end.
        release ctx-reporte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-aponta' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-aponta where dc-aponta.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-aponta FIELDS() where dc-aponta.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-aponta' + STRING(i-lidos)).
            delete dc-aponta.
        end.
        release dc-aponta.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-aponta' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-aponta where dc-aponta.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-aponta FIELDS() where dc-aponta.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-aponta' + STRING(i-lidos)).
            delete dc-aponta.
        end.
        release dc-aponta.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-aponta-retrabalho' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-aponta-retrabalho where dc-aponta-retrabalho.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-aponta-retrabalho FIELDS() where dc-aponta-retrabalho.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-aponta-retrabalho' + STRING(i-lidos)).
            delete dc-aponta-retrabalho.
        end.
        release dc-aponta-retrabalho.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-aponta-retrabalho' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-aponta-retrabalho where dc-aponta-retrabalho.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-aponta-retrabalho FIELDS() where dc-aponta-retrabalho.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-aponta-retrabalho' + STRING(i-lidos)).
            delete dc-aponta-retrabalho.
        end.
        release dc-aponta-retrabalho.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-cor' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-cor FIELDS() use-index idx_dc_cor where dc-cor.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-cor' + STRING(i-lidos)).
            delete dc-cor.
        end.
        release dc-cor.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-embarque-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-embarque-item where dc-embarque-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-embarque-item FIELDS() where dc-embarque-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-embarque-item' + STRING(i-lidos)).
            delete dc-embarque-item.
        end.
        release dc-embarque-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-item-cor' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-item-cor FIELDS() use-index pk-item-cor where dc-item-cor.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-item-cor' + STRING(i-lidos)).
            delete dc-item-cor.
        end.
        release dc-item-cor.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-item-cor' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-item-cor FIELDS() use-index idx-it-codigo-acab where dc-item-cor.it-codigo-acab = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-item-cor' + STRING(i-lidos)).
            delete dc-item-cor.
        end.
        release dc-item-cor.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-item-cor' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-item-cor where dc-item-cor.it-codigo-cor = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-item-cor FIELDS() where dc-item-cor.it-codigo-cor = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-item-cor' + STRING(i-lidos)).
            delete dc-item-cor.
        end.
        release dc-item-cor.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-juncao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-juncao FIELDS() use-index pk-dc-juncao where dc-juncao.it-codigo-1 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-juncao' + STRING(i-lidos)).
            delete dc-juncao.
        end.
        release dc-juncao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-juncao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-juncao where dc-juncao.it-codigo-2 = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-juncao FIELDS() where dc-juncao.it-codigo-2 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-juncao' + STRING(i-lidos)).
            delete dc-juncao.
        end.
        release dc-juncao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-juncao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-juncao where dc-juncao.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-juncao FIELDS() where dc-juncao.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-juncao' + STRING(i-lidos)).
            delete dc-juncao.
        end.
        release dc-juncao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-movto-pto-controle' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-movto-pto-controle FIELDS() use-index idx-item-local where dc-movto-pto-controle.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-movto-pto-controle' + STRING(i-lidos)).
            delete dc-movto-pto-controle.
        end.
        release dc-movto-pto-controle.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-movto-pto-controle' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-movto-pto-controle where dc-movto-pto-controle.it-codigo-ant = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-movto-pto-controle FIELDS() where dc-movto-pto-controle.it-codigo-ant = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-movto-pto-controle' + STRING(i-lidos)).
            delete dc-movto-pto-controle.
        end.
        release dc-movto-pto-controle.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-reporte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-reporte where dc-reporte.it-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-reporte FIELDS() where dc-reporte.it-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-reporte' + STRING(i-lidos)).
            delete dc-reporte.
        end.
        release dc-reporte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-reporte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-reporte where dc-reporte.it-codigo-semi-1 = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-reporte FIELDS() where dc-reporte.it-codigo-semi-1 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-reporte' + STRING(i-lidos)).
            delete dc-reporte.
        end.
        release dc-reporte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-reporte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-reporte where dc-reporte.it-codigo-semi-2 = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-reporte FIELDS() where dc-reporte.it-codigo-semi-2 = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-reporte' + STRING(i-lidos)).
            delete dc-reporte.
        end.
        release dc-reporte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-rg-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-rg-item FIELDS() use-index idx-item-local where dc-rg-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-rg-item' + STRING(i-lidos)).
            delete dc-rg-item.
        end.
        release dc-rg-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dc-rg-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.dc-rg-item where dc-rg-item.it-codigo-juncao = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.dc-rg-item FIELDS() where dc-rg-item.it-codigo-juncao = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.dc-rg-item' + STRING(i-lidos)).
            delete dc-rg-item.
        end.
        release dc-rg-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-etq-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-etq-item FIELDS() use-index codigo where es-etq-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-etq-item' + STRING(i-lidos)).
            delete es-etq-item.
        end.
        release es-etq-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-it-ordem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-it-ordem where es-it-ordem.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-it-ordem FIELDS() where es-it-ordem.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-it-ordem' + STRING(i-lidos)).
            delete es-it-ordem.
        end.
        release es-it-ordem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-item-cli' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-item-cli FIELDS() use-index ch-item-cli where es-item-cli.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-item-cli' + STRING(i-lidos)).
            delete es-item-cli.
        end.
        release es-item-cli.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ES-ITEM-DOC-EST' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.ES-ITEM-DOC-EST where ES-ITEM-DOC-EST.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.ES-ITEM-DOC-EST FIELDS() where ES-ITEM-DOC-EST.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.ES-ITEM-DOC-EST' + STRING(i-lidos)).
            delete ES-ITEM-DOC-EST.
        end.
        release ES-ITEM-DOC-EST.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-nfr-item-movto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-nfr-item-movto where es-nfr-item-movto.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-nfr-item-movto FIELDS() where es-nfr-item-movto.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-nfr-item-movto' + STRING(i-lidos)).
            delete es-nfr-item-movto.
        end.
        release es-nfr-item-movto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-nfr-nota-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-nfr-nota-item where es-nfr-nota-item.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-nfr-nota-item FIELDS() where es-nfr-nota-item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-nfr-nota-item' + STRING(i-lidos)).
            delete es-nfr-nota-item.
        end.
        release es-nfr-nota-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-nfr-peca' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-nfr-peca where es-nfr-peca.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-nfr-peca FIELDS() where es-nfr-peca.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-nfr-peca' + STRING(i-lidos)).
            delete es-nfr-peca.
        end.
        release es-nfr-peca.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-operacao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-operacao FIELDS() use-index codigo where es-operacao.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-operacao' + STRING(i-lidos)).
            delete es-operacao.
        end.
        release es-operacao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-pedido-cli' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-pedido-cli where es-pedido-cli.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-pedido-cli FIELDS() where es-pedido-cli.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-pedido-cli' + STRING(i-lidos)).
            delete es-pedido-cli.
        end.
        release es-pedido-cli.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-pedido-vw' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-pedido-vw where es-pedido-vw.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-pedido-vw FIELDS() where es-pedido-vw.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-pedido-vw' + STRING(i-lidos)).
            delete es-pedido-vw.
        end.
        release es-pedido-vw.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-prog-entrega' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-prog-entrega where es-prog-entrega.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-prog-entrega FIELDS() where es-prog-entrega.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-prog-entrega' + STRING(i-lidos)).
            delete es-prog-entrega.
        end.
        release es-prog-entrega.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'es-saldo-terc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.es-saldo-terc where es-saldo-terc.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.es-saldo-terc FIELDS() where es-saldo-terc.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.es-saldo-terc' + STRING(i-lidos)).
            delete es-saldo-terc.
        end.
        release es-saldo-terc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-cliprd' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-cliprd FIELDS() use-index ctrpro where lo-cliprd.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-cliprd' + STRING(i-lidos)).
            delete lo-cliprd.
        end.
        release lo-cliprd.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-defocorre' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-defocorre FIELDS() use-index ctrpro where lo-defocorre.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-defocorre' + STRING(i-lidos)).
            delete lo-defocorre.
        end.
        release lo-defocorre.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-detord' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-detord FIELDS() use-index idto where lo-detord.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-detord' + STRING(i-lidos)).
            delete lo-detord.
        end.
        release lo-detord.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-it-emb' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.lo-it-emb where lo-it-emb.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-it-emb FIELDS() where lo-it-emb.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-it-emb' + STRING(i-lidos)).
            delete lo-it-emb.
        end.
        release lo-it-emb.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-it-nota' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.lo-it-nota where lo-it-nota.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-it-nota FIELDS() where lo-it-nota.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-it-nota' + STRING(i-lidos)).
            delete lo-it-nota.
        end.
        release lo-it-nota.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-matplano' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.lo-matplano where lo-matplano.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-matplano FIELDS() where lo-matplano.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-matplano' + STRING(i-lidos)).
            delete lo-matplano.
        end.
        release lo-matplano.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-matsaldo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-matsaldo FIELDS() use-index ch-item where lo-matsaldo.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-matsaldo' + STRING(i-lidos)).
            delete lo-matsaldo.
        end.
        release lo-matsaldo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-perc-crit' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-perc-crit FIELDS() use-index item where lo-perc-crit.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-perc-crit' + STRING(i-lidos)).
            delete lo-perc-crit.
        end.
        release lo-perc-crit.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-plames' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgfas.lo-plames where lo-plames.it-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-plames FIELDS() where lo-plames.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-plames' + STRING(i-lidos)).
            delete lo-plames.
        end.
        release lo-plames.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'lo-plaprod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgfas.lo-plaprod FIELDS() use-index ctrpro where lo-plaprod.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgfas.lo-plaprod' + STRING(i-lidos)).
            delete lo-plaprod.
        end.
        release lo-plaprod.
    end.*/

    /*------------------------------ES-CODIGO------------------------------*/

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estrut-item-dbr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.estrut-item-dbr where estrut-item-dbr.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.estrut-item-dbr FIELDS() where estrut-item-dbr.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.estrut-item-dbr' + STRING(i-lidos)).
            delete estrut-item-dbr.
        end.
        release estrut-item-dbr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-it-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgdbr.ref-it-estrut where ref-it-estrut.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgdbr.ref-it-estrut FIELDS() where ref-it-estrut.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgdbr.ref-it-estrut' + STRING(i-lidos)).
            delete ref-it-estrut.
        end.
        release ref-it-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'altern-lista-compon' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.altern-lista-compon where altern-lista-compon.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.altern-lista-compon FIELDS() where altern-lista-compon.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.altern-lista-compon' + STRING(i-lidos)).
            delete altern-lista-compon.
        end.
        release altern-lista-compon.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'alternativo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.alternativo where alternativo.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.alternativo FIELDS() where alternativo.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.alternativo' + STRING(i-lidos)).
            delete alternativo.
        end.
        release alternativo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-altern' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.dp-altern where dp-altern.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-altern FIELDS() where dp-altern.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-altern' + STRING(i-lidos)).
            delete dp-altern.
        end.
        release dp-altern.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-estrut FIELDS() use-index onde-se-usa where dp-estrut.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-estrut' + STRING(i-lidos)).
            delete dp-estrut.
        end.
        release dp-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estr-conf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estr-conf FIELDS() use-index onde-se-usa where estr-conf.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estr-conf' + STRING(i-lidos)).
            delete estr-conf.
        end.
        release estr-conf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estrutura' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.estrutura FIELDS() use-index onde-se-usa where estrutura.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.estrutura' + STRING(i-lidos)).
            delete estrutura.
        end.
        release estrutura.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-estrut-draw' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.item-estrut-draw where item-estrut-draw.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-estrut-draw FIELDS() where item-estrut-draw.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-estrut-draw' + STRING(i-lidos)).
            delete item-estrut-draw.
        end.
        release item-estrut-draw.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-lista-compon' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-lista-compon FIELDS() use-index onde-se-usa where item-lista-compon.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-lista-compon' + STRING(i-lidos)).
            delete item-lista-compon.
        end.
        release item-lista-compon.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ref-estrut FIELDS() use-index refer-es where ref-estrut.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ref-estrut' + STRING(i-lidos)).
            delete ref-estrut.
        end.
        release ref-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-estrut-dp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ref-estrut-dp where ref-estrut-dp.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ref-estrut-dp FIELDS() where ref-estrut-dp.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ref-estrut-dp' + STRING(i-lidos)).
            delete ref-estrut-dp.
        end.
        release ref-estrut-dp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-compon-dcr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-compon-dcr where zfm-compon-dcr.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-compon-dcr FIELDS() where zfm-compon-dcr.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-compon-dcr' + STRING(i-lidos)).
            delete zfm-compon-dcr.
        end.
        release zfm-compon-dcr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-compon-dcr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-compon-dcr where zfm-compon-dcr.es-codigo-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-compon-dcr FIELDS() where zfm-compon-dcr.es-codigo-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-compon-dcr' + STRING(i-lidos)).
            delete zfm-compon-dcr.
        end.
        release zfm-compon-dcr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cot-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.cot-estrut FIELDS() use-index onse-se-usa where cot-estrut.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.cot-estrut' + STRING(i-lidos)).
            delete cot-estrut.
        end.
        release cot-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.ord-prod where ord-prod.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ord-prod FIELDS() where ord-prod.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ord-prod' + STRING(i-lidos)).
            delete ord-prod.
        end.
        release ord-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prazo-compra' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.prazo-compra where prazo-compra.es-codigo = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.prazo-compra FIELDS() where prazo-compra.es-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.prazo-compra' + STRING(i-lidos)).
            delete prazo-compra.
        end.
        release prazo-compra.
    end.

    /*------------------------------ITEM-DP------------------------------*/
    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-altern' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-altern FIELDS() use-index id where dp-altern.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-altern' + STRING(i-lidos)).
            delete dp-altern.
        end.
        release dp-altern.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-estrut FIELDS() use-index id where dp-estrut.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-estrut' + STRING(i-lidos)).
            delete dp-estrut.
        end.
        release dp-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-item FIELDS() use-index id where dp-item.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-item' + STRING(i-lidos)).
            delete dp-item.
        end.
        release dp-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-nar-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-nar-item FIELDS() use-index id where dp-nar-item.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-nar-item' + STRING(i-lidos)).
            delete dp-nar-item.
        end.
        release dp-nar-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-oper' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-oper FIELDS() use-index id where dp-oper.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-oper' + STRING(i-lidos)).
            delete dp-oper.
        end.
        release dp-oper.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-proces-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-proces-item FIELDS() use-index id where dp-proces-item.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-proces-item' + STRING(i-lidos)).
            delete dp-proces-item.
        end.
        release dp-proces-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-rede-p' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dp-rede-p FIELDS() use-index rede where dp-rede-p.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dp-rede-p' + STRING(i-lidos)).
            delete dp-rede-p.
        end.
        release dp-rede-p.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ref-estrut-dp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ref-estrut-dp where ref-estrut-dp.item-dp = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ref-estrut-dp FIELDS() where ref-estrut-dp.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ref-estrut-dp' + STRING(i-lidos)).
            delete ref-estrut-dp.
        end.
        release ref-estrut-dp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'refer-item-desenv' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.refer-item-desenv FIELDS() use-index rfrtmdsn-id where refer-item-desenv.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.refer-item-desenv' + STRING(i-lidos)).
            delete refer-item-desenv.
        end.
        release refer-item-desenv.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dp-relat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dp-relat FIELDS() use-index id where dp-relat.item-dp = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dp-relat' + STRING(i-lidos)).
            delete dp-relat.
        end.
        release dp-relat.
    end.


    /*------------------------------COD-ITEM------------------------------*/
    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'bmg-apont-causa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdbr.bmg-apont-causa where bmg-apont-causa.cod-item-causa = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.bmg-apont-causa FIELDS() where bmg-apont-causa.cod-item-causa = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.bmg-apont-causa' + STRING(i-lidos)).
            delete bmg-apont-causa.
        end.
        release bmg-apont-causa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'op-alter-confir' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdbr.op-alter-confir FIELDS() use-index opltrcnf-item where op-alter-confir.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdbr.op-alter-confir' + STRING(i-lidos)).
            delete op-alter-confir.
        end.
        release op-alter-confir.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'decla-import-adic-itens' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.decla-import-adic-itens where decla-import-adic-itens.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.decla-import-adic-itens FIELDS() where decla-import-adic-itens.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.decla-import-adic-itens' + STRING(i-lidos)).
            delete decla-import-adic-itens.
        end.
        release decla-import-adic-itens.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'decla-import-ord' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.decla-import-ord where decla-import-ord.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.decla-import-ord FIELDS() where decla-import-ord.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.decla-import-ord' + STRING(i-lidos)).
            delete decla-import-ord.
        end.
        release decla-import-ord.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'deduc-adic-itens' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.deduc-adic-itens where deduc-adic-itens.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.deduc-adic-itens FIELDS() where deduc-adic-itens.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.deduc-adic-itens' + STRING(i-lidos)).
            delete deduc-adic-itens.
        end.
        release deduc-adic-itens.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'despes-adic-itens' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.despes-adic-itens where despes-adic-itens.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.despes-adic-itens FIELDS() where despes-adic-itens.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.despes-adic-itens' + STRING(i-lidos)).
            delete despes-adic-itens.
        end.
        release despes-adic-itens.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'docto-orig-cte' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.docto-orig-cte where docto-orig-cte.cod-item-serv = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.docto-orig-cte FIELDS() where docto-orig-cte.cod-item-serv = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.docto-orig-cte' + STRING(i-lidos)).
            delete docto-orig-cte.
        end.
        release docto-orig-cte.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'docto-orig-nfse' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.docto-orig-nfse FIELDS() use-index dcto-nfse-it where docto-orig-nfse.cod-item-serv = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.docto-orig-nfse' + STRING(i-lidos)).
            delete docto-orig-nfse.
        end.
        release docto-orig-nfse.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-fator-conver' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dwf-fator-conver FIELDS() use-index dwfftrcn-id where dwf-fator-conver.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dwf-fator-conver' + STRING(i-lidos)).
            delete dwf-fator-conver.
        end.
        release dwf-fator-conver.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dwf-item FIELDS() use-index dwfitem-id where dwf-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dwf-item' + STRING(i-lidos)).
            delete dwf-item.
        end.
        release dwf-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.dwf-item where dwf-item.cod-item-ant = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dwf-item FIELDS() where dwf-item.cod-item-ant = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dwf-item' + STRING(i-lidos)).
            delete dwf-item.
        end.
        release dwf-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-item-compon' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dwf-item-compon FIELDS() use-index dwftmcmp-id where dwf-item-compon.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dwf-item-compon' + STRING(i-lidos)).
            delete dwf-item-compon.
        end.
        release dwf-item-compon.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-item-compon' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.dwf-item-compon where dwf-item-compon.cod-item-compon = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dwf-item-compon FIELDS() where dwf-item-compon.cod-item-compon = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dwf-item-compon' + STRING(i-lidos)).
            delete dwf-item-compon.
        end.
        release dwf-item-compon.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-item-produt-decla' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.dwf-item-produt-decla FIELDS() use-index dwftmprd-item where dwf-item-produt-decla.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.dwf-item-produt-decla' + STRING(i-lidos)).
            delete dwf-item-produt-decla.
        end.
        release dwf-item-produt-decla.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'fab-medic-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.fab-medic-item FIELDS() use-index fbmdctm-id where fab-medic-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.fab-medic-item' + STRING(i-lidos)).
            delete fab-medic-item.
        end.
        release fab-medic-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'fab-medic-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.fab-medic-item where fab-medic-item.cod-item-comerc = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.fab-medic-item FIELDS() where fab-medic-item.cod-item-comerc = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.fab-medic-item' + STRING(i-lidos)).
            delete fab-medic-item.
        end.
        release fab-medic-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'iss-cidad-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.iss-cidad-item where iss-cidad-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.iss-cidad-item FIELDS() where iss-cidad-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.iss-cidad-item' + STRING(i-lidos)).
            delete iss-cidad-item.
        end.
        release iss-cidad-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-pre-fat-serial' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.it-pre-fat-serial FIELDS() use-index itmprfta-02 where it-pre-fat-serial.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.it-pre-fat-serial' + STRING(i-lidos)).
            delete it-pre-fat-serial.
        end.
        release it-pre-fat-serial.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-bonif-cliente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-bonif-cliente FIELDS() use-index itmbnfcl-04 where item-bonif-cliente.cod-item-bonif = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-bonif-cliente' + STRING(i-lidos)).
            delete item-bonif-cliente.
        end.
        release item-bonif-cliente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-bonif-cliente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-bonif-cliente FIELDS() use-index itmbnfcl-03 where item-bonif-cliente.cod-item-venda = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-bonif-cliente' + STRING(i-lidos)).
            delete item-bonif-cliente.
        end.
        release item-bonif-cliente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'Item-cubado-tf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.Item-cubado-tf where Item-cubado-tf.Cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.Item-cubado-tf FIELDS() where Item-cubado-tf.Cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.Item-cubado-tf' + STRING(i-lidos)).
            delete Item-cubado-tf.
        end.
        release Item-cubado-tf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-docto-transf-depos' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-docto-transf-depos FIELDS() use-index itmdcttr-04 where item-docto-transf-depos.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-docto-transf-depos' + STRING(i-lidos)).
            delete item-docto-transf-depos.
        end.
        release item-docto-transf-depos.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-fornec-fabrican-umd' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item-fornec-fabrican-umd FIELDS() use-index itmfrnca-id where item-fornec-fabrican-umd.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item-fornec-fabrican-umd' + STRING(i-lidos)).
            delete item-fornec-fabrican-umd.
        end.
        release item-fornec-fabrican-umd.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ncm-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ncm-item where ncm-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ncm-item FIELDS() where ncm-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ncm-item' + STRING(i-lidos)).
            delete ncm-item.
        end.
        release ncm-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ncm-item-traduz' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ncm-item-traduz where ncm-item-traduz.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ncm-item-traduz FIELDS() where ncm-item-traduz.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ncm-item-traduz' + STRING(i-lidos)).
            delete ncm-item-traduz.
        end.
        release ncm-item-traduz.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'nve-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.nve-item FIELDS() use-index nveitem-id where nve-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.nve-item' + STRING(i-lidos)).
            delete nve-item.
        end.
        release nve-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'operacao' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.operacao FIELDS() use-index operacao-refugo where operacao.cod-item-refugo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.operacao' + STRING(i-lidos)).
            delete operacao.
        end.
        release operacao.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ped-sdo-terc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.ped-sdo-terc where ped-sdo-terc.cod-item-ped = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.ped-sdo-terc FIELDS() where ped-sdo-terc.cod-item-ped = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.ped-sdo-terc' + STRING(i-lidos)).
            delete ped-sdo-terc.
        end.
        release ped-sdo-terc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'proces-devol-clien' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.proces-devol-clien where proces-devol-clien.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.proces-devol-clien FIELDS() where proces-devol-clien.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.proces-devol-clien' + STRING(i-lidos)).
            delete proces-devol-clien.
        end.
        release proces-devol-clien.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'serial-faturam' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.serial-faturam FIELDS() use-index srlftrm-01 where serial-faturam.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.serial-faturam' + STRING(i-lidos)).
            delete serial-faturam.
        end.
        release serial-faturam.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sfw-integr-ped-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.sfw-integr-ped-item where sfw-integr-ped-item.cod-item-sfw = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.sfw-integr-ped-item FIELDS() where sfw-integr-ped-item.cod-item-sfw = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.sfw-integr-ped-item' + STRING(i-lidos)).
            delete sfw-integr-ped-item.
        end.
        release sfw-integr-ped-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'sit-tribut-relacto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.sit-tribut-relacto FIELDS() use-index sttrbtrl-item where sit-tribut-relacto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.sit-tribut-relacto' + STRING(i-lidos)).
            delete sit-tribut-relacto.
        end.
        release sit-tribut-relacto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'tab-conver-veic' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.tab-conver-veic where tab-conver-veic.cod-item-veic = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.tab-conver-veic FIELDS() where tab-conver-veic.cod-item-veic = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.tab-conver-veic' + STRING(i-lidos)).
            delete tab-conver-veic.
        end.
        release tab-conver-veic.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-aloca-saldo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-aloca-saldo where wm-aloca-saldo.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-aloca-saldo FIELDS() where wm-aloca-saldo.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-aloca-saldo' + STRING(i-lidos)).
            delete wm-aloca-saldo.
        end.
        release wm-aloca-saldo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-box-indisponivel' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-box-indisponivel where wm-box-indisponivel.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-box-indisponivel FIELDS() where wm-box-indisponivel.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-box-indisponivel' + STRING(i-lidos)).
            delete wm-box-indisponivel.
        end.
        release wm-box-indisponivel.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-box-movto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-box-movto where wm-box-movto.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-box-movto FIELDS() where wm-box-movto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-box-movto' + STRING(i-lidos)).
            delete wm-box-movto.
        end.
        release wm-box-movto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'Wm-box-movto-lote' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.Wm-box-movto-lote where Wm-box-movto-lote.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.Wm-box-movto-lote FIELDS() where Wm-box-movto-lote.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.Wm-box-movto-lote' + STRING(i-lidos)).
            delete Wm-box-movto-lote.
        end.
        release Wm-box-movto-lote.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-box-preferencial' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-box-preferencial where wm-box-preferencial.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-box-preferencial FIELDS() where wm-box-preferencial.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-box-preferencial' + STRING(i-lidos)).
            delete wm-box-preferencial.
        end.
        release wm-box-preferencial.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-box-saida-ressup' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-box-saida-ressup where wm-box-saida-ressup.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-box-saida-ressup FIELDS() where wm-box-saida-ressup.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-box-saida-ressup' + STRING(i-lidos)).
            delete wm-box-saida-ressup.
        end.
        release wm-box-saida-ressup.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-box-saldo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-box-saldo where wm-box-saldo.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-box-saldo FIELDS() where wm-box-saldo.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-box-saldo' + STRING(i-lidos)).
            delete wm-box-saldo.
        end.
        release wm-box-saldo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-conferencia-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-conferencia-item FIELDS() use-index wmscnfra-01 where wm-conferencia-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-conferencia-item' + STRING(i-lidos)).
            delete wm-conferencia-item.
        end.
        release wm-conferencia-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-docto-itens' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-docto-itens FIELDS() use-index wmsdcttm-05 where wm-docto-itens.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-docto-itens' + STRING(i-lidos)).
            delete wm-docto-itens.
        end.
        release wm-docto-itens.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-docto-itens-devol' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-docto-itens-devol where wm-docto-itens-devol.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-docto-itens-devol FIELDS() where wm-docto-itens-devol.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-docto-itens-devol' + STRING(i-lidos)).
            delete wm-docto-itens-devol.
        end.
        release wm-docto-itens-devol.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-docto-itens-ped' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-docto-itens-ped where wm-docto-itens-ped.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-docto-itens-ped FIELDS() where wm-docto-itens-ped.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-docto-itens-ped' + STRING(i-lidos)).
            delete wm-docto-itens-ped.
        end.
        release wm-docto-itens-ped.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-docto-itens-ped-etiqueta' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-docto-itens-ped-etiqueta where wm-docto-itens-ped-etiqueta.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-docto-itens-ped-etiqueta FIELDS() where wm-docto-itens-ped-etiqueta.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-docto-itens-ped-etiqueta' + STRING(i-lidos)).
            delete wm-docto-itens-ped-etiqueta.
        end.
        release wm-docto-itens-ped-etiqueta.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-etiqueta' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-etiqueta FIELDS() use-index wmsetiq-12 where wm-etiqueta.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-etiqueta' + STRING(i-lidos)).
            delete wm-etiqueta.
        end.
        release wm-etiqueta.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-inventario-acertos' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-inventario-acertos FIELDS() use-index wmsnvntc-03 where wm-inventario-acertos.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-inventario-acertos' + STRING(i-lidos)).
            delete wm-inventario-acertos.
        end.
        release wm-inventario-acertos.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-inventario-ficha' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-inventario-ficha where wm-inventario-ficha.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-inventario-ficha FIELDS() where wm-inventario-ficha.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-inventario-ficha' + STRING(i-lidos)).
            delete wm-inventario-ficha.
        end.
        release wm-inventario-ficha.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-inventario-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-inventario-item FIELDS() use-index wmsnvnta-02 where wm-inventario-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-inventario-item' + STRING(i-lidos)).
            delete wm-inventario-item.
        end.
        release wm-inventario-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item FIELDS() use-index idx-item1 where wm-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item' + STRING(i-lidos)).
            delete wm-item.
        end.
        release wm-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item-embalagem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item-embalagem FIELDS() use-index idx-wm-item-embalagem3 where wm-item-embalagem.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item-embalagem' + STRING(i-lidos)).
            delete wm-item-embalagem.
        end.
        release wm-item-embalagem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item-embalagem-etiq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item-embalagem-etiq FIELDS() use-index wmstmmbb-id where wm-item-embalagem-etiq.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item-embalagem-etiq' + STRING(i-lidos)).
            delete wm-item-embalagem-etiq.
        end.
        release wm-item-embalagem-etiq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item-embalagem-local' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item-embalagem-local FIELDS() use-index idx-wm-item-embalagem-local5 where wm-item-embalagem-local.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item-embalagem-local' + STRING(i-lidos)).
            delete wm-item-embalagem-local.
        end.
        release wm-item-embalagem-local.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item-etiq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item-etiq FIELDS() use-index wmstmtq-id where wm-item-etiq.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item-etiq' + STRING(i-lidos)).
            delete wm-item-etiq.
        end.
        release wm-item-etiq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item-picking' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-item-picking where wm-item-picking.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item-picking FIELDS() where wm-item-picking.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item-picking' + STRING(i-lidos)).
            delete wm-item-picking.
        end.
        release wm-item-picking.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-item-regra' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-item-regra FIELDS() use-index idx-item-regra1 where wm-item-regra.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-item-regra' + STRING(i-lidos)).
            delete wm-item-regra.
        end.
        release wm-item-regra.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-packing-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-packing-item FIELDS() use-index idx-wm-packing-item2 where wm-packing-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-packing-item' + STRING(i-lidos)).
            delete wm-packing-item.
        end.
        release wm-packing-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-ref-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-ref-item FIELDS() use-index idx-ref-item1 where wm-ref-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-ref-item' + STRING(i-lidos)).
            delete wm-ref-item.
        end.
        release wm-ref-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wm-saldo-estoque' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.wm-saldo-estoque where wm-saldo-estoque.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wm-saldo-estoque FIELDS() where wm-saldo-estoque.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wm-saldo-estoque' + STRING(i-lidos)).
            delete wm-saldo-estoque.
        end.
        release wm-saldo-estoque.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wms-item-embal-local-pack' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wms-item-embal-local-pack FIELDS() use-index idx-wm-item-embalagem-local3 where wms-item-embal-local-pack.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wms-item-embal-local-pack' + STRING(i-lidos)).
            delete wms-item-embal-local-pack.
        end.
        release wms-item-embal-local-pack.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wms-item-estab-local' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wms-item-estab-local FIELDS() use-index wmstmstb-item where wms-item-estab-local.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wms-item-estab-local' + STRING(i-lidos)).
            delete wms-item-estab-local.
        end.
        release wms-item-estab-local.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wms-item-fornec-embal' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.wms-item-fornec-embal FIELDS() use-index idx_wm_it_for_emb4 where wms-item-fornec-embal.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.wms-item-fornec-embal' + STRING(i-lidos)).
            delete wms-item-fornec-embal.
        end.
        release wms-item-fornec-embal.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-estrut where zfm-estrut.cod-item-filho = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-estrut FIELDS() where zfm-estrut.cod-item-filho = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-estrut' + STRING(i-lidos)).
            delete zfm-estrut.
        end.
        release zfm-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-estrut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-estrut FIELDS() use-index zfmstrt-id where zfm-estrut.cod-item-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-estrut' + STRING(i-lidos)).
            delete zfm-estrut.
        end.
        release zfm-estrut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-apurac-cr-extmpreo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-apurac-cr-extmpreo where dwf-apurac-cr-extmpreo.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-apurac-cr-extmpreo FIELDS() where dwf-apurac-cr-extmpreo.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-apurac-cr-extmpreo' + STRING(i-lidos)).
            delete dwf-apurac-cr-extmpreo.
        end.
        release dwf-apurac-cr-extmpreo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-apurac-impto-docto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-apurac-impto-docto where dwf-apurac-impto-docto.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-apurac-impto-docto FIELDS() where dwf-apurac-impto-docto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-apurac-impto-docto' + STRING(i-lidos)).
            delete dwf-apurac-impto-docto.
        end.
        release dwf-apurac-impto-docto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-bomba-bico' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-bomba-bico where dwf-bomba-bico.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-bomba-bico FIELDS() where dwf-bomba-bico.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-bomba-bico' + STRING(i-lidos)).
            delete dwf-bomba-bico.
        end.
        release dwf-bomba-bico.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cf-resum-diario-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cf-resum-diario-item where dwf-cf-resum-diario-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cf-resum-diario-item FIELDS() where dwf-cf-resum-diario-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cf-resum-diario-item' + STRING(i-lidos)).
            delete dwf-cf-resum-diario-item.
        end.
        release dwf-cf-resum-diario-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cf-vda-consm-det' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cf-vda-consm-det where dwf-cf-vda-consm-det.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cf-vda-consm-det FIELDS() where dwf-cf-vda-consm-det.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cf-vda-consm-det' + STRING(i-lidos)).
            delete dwf-cf-vda-consm-det.
        end.
        release dwf-cf-vda-consm-det.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cf-vda-consm-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cf-vda-consm-item where dwf-cf-vda-consm-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cf-vda-consm-item FIELDS() where dwf-cf-vda-consm-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cf-vda-consm-item' + STRING(i-lidos)).
            delete dwf-cf-vda-consm-item.
        end.
        release dwf-cf-vda-consm-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cf-vda-consm-item-entr' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cf-vda-consm-item-entr where dwf-cf-vda-consm-item-entr.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cf-vda-consm-item-entr FIELDS() where dwf-cf-vda-consm-item-entr.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cf-vda-consm-item-entr' + STRING(i-lidos)).
            delete dwf-cf-vda-consm-item-entr.
        end.
        release dwf-cf-vda-consm-item-entr.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cfe-detmnto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cfe-detmnto where dwf-cfe-detmnto.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cfe-detmnto FIELDS() where dwf-cfe-detmnto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cfe-detmnto' + STRING(i-lidos)).
            delete dwf-cfe-detmnto.
        end.
        release dwf-cfe-detmnto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cfe-detmnto-umd-produt' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cfe-detmnto-umd-produt where dwf-cfe-detmnto-umd-produt.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cfe-detmnto-umd-produt FIELDS() where dwf-cfe-detmnto-umd-produt.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cfe-detmnto-umd-produt' + STRING(i-lidos)).
            delete dwf-cfe-detmnto-umd-produt.
        end.
        release dwf-cfe-detmnto-umd-produt.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cfe-resum-sat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cfe-resum-sat where dwf-cfe-resum-sat.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cfe-resum-sat FIELDS() where dwf-cfe-resum-sat.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cfe-resum-sat' + STRING(i-lidos)).
            delete dwf-cfe-resum-sat.
        end.
        release dwf-cfe-resum-sat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cfe-resum-umd-produt-sat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cfe-resum-umd-produt-sat where dwf-cfe-resum-umd-produt-sat.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cfe-resum-umd-produt-sat FIELDS() where dwf-cfe-resum-umd-produt-sat.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cfe-resum-umd-produt-sat' + STRING(i-lidos)).
            delete dwf-cfe-resum-umd-produt-sat.
        end.
        release dwf-cfe-resum-umd-produt-sat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-ciap-docto-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-ciap-docto-item where dwf-ciap-docto-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-ciap-docto-item FIELDS() where dwf-ciap-docto-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-ciap-docto-item' + STRING(i-lidos)).
            delete dwf-ciap-docto-item.
        end.
        release dwf-ciap-docto-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-concil-estoq-combust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-concil-estoq-combust where dwf-concil-estoq-combust.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-concil-estoq-combust FIELDS() where dwf-concil-estoq-combust.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-concil-estoq-combust' + STRING(i-lidos)).
            delete dwf-concil-estoq-combust.
        end.
        release dwf-concil-estoq-combust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cupom-fisc-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cupom-fisc-item where dwf-cupom-fisc-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cupom-fisc-item FIELDS() where dwf-cupom-fisc-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cupom-fisc-item' + STRING(i-lidos)).
            delete dwf-cupom-fisc-item.
        end.
        release dwf-cupom-fisc-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cupom-fisc-mensal' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cupom-fisc-mensal where dwf-cupom-fisc-mensal.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cupom-fisc-mensal FIELDS() where dwf-cupom-fisc-mensal.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cupom-fisc-mensal' + STRING(i-lidos)).
            delete dwf-cupom-fisc-mensal.
        end.
        release dwf-cupom-fisc-mensal.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-detmnto-recta-regim-cx' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-detmnto-recta-regim-cx where dwf-detmnto-recta-regim-cx.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-detmnto-recta-regim-cx FIELDS() where dwf-detmnto-recta-regim-cx.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-detmnto-recta-regim-cx' + STRING(i-lidos)).
            delete dwf-detmnto-recta-regim-cx.
        end.
        release dwf-detmnto-recta-regim-cx.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-item where dwf-docto-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-item FIELDS() where dwf-docto-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-item' + STRING(i-lidos)).
            delete dwf-docto-item.
        end.
        release dwf-docto-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-item-arma' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-item-arma where dwf-docto-item-arma.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-item-arma FIELDS() where dwf-docto-item-arma.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-item-arma' + STRING(i-lidos)).
            delete dwf-docto-item-arma.
        end.
        release dwf-docto-item-arma.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-item-impto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-item-impto where dwf-docto-item-impto.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-item-impto FIELDS() where dwf-docto-item-impto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-item-impto' + STRING(i-lidos)).
            delete dwf-docto-item-impto.
        end.
        release dwf-docto-item-impto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-item-medicto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-item-medicto where dwf-docto-item-medicto.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-item-medicto FIELDS() where dwf-docto-item-medicto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-item-medicto' + STRING(i-lidos)).
            delete dwf-docto-item-medicto.
        end.
        release dwf-docto-item-medicto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-item-outros-ajust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-item-outros-ajust where dwf-docto-item-outros-ajust.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-item-outros-ajust FIELDS() where dwf-docto-item-outros-ajust.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-item-outros-ajust' + STRING(i-lidos)).
            delete dwf-docto-item-outros-ajust.
        end.
        release dwf-docto-item-outros-ajust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-item-veic-novo' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-item-veic-novo where dwf-docto-item-veic-novo.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-item-veic-novo FIELDS() where dwf-docto-item-veic-novo.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-item-veic-novo' + STRING(i-lidos)).
            delete dwf-docto-item-veic-novo.
        end.
        release dwf-docto-item-veic-novo.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-reg-export' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-reg-export where dwf-docto-reg-export.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-reg-export FIELDS() where dwf-docto-reg-export.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-reg-export' + STRING(i-lidos)).
            delete dwf-docto-reg-export.
        end.
        release dwf-docto-reg-export.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-docto-reg-export-indta' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-docto-reg-export-indta where dwf-docto-reg-export-indta.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-docto-reg-export-indta FIELDS() where dwf-docto-reg-export-indta.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-docto-reg-export-indta' + STRING(i-lidos)).
            delete dwf-docto-reg-export-indta.
        end.
        release dwf-docto-reg-export-indta.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-estrut-ord-produc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-estrut-ord-produc where dwf-estrut-ord-produc.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-estrut-ord-produc FIELDS() where dwf-estrut-ord-produc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-estrut-ord-produc' + STRING(i-lidos)).
            delete dwf-estrut-ord-produc.
        end.
        release dwf-estrut-ord-produc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-estrut-ord-produc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-estrut-ord-produc where dwf-estrut-ord-produc.cod-item-compon = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-estrut-ord-produc FIELDS() where dwf-estrut-ord-produc.cod-item-compon = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-estrut-ord-produc' + STRING(i-lidos)).
            delete dwf-estrut-ord-produc.
        end.
        release dwf-estrut-ord-produc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-invent' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-invent where dwf-invent.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-invent FIELDS() where dwf-invent.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-invent' + STRING(i-lidos)).
            delete dwf-invent.
        end.
        release dwf-invent.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-invent-motiv' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-invent-motiv where dwf-invent-motiv.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-invent-motiv FIELDS() where dwf-invent-motiv.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-invent-motiv' + STRING(i-lidos)).
            delete dwf-invent-motiv.
        end.
        release dwf-invent-motiv.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-movimen-combust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-movimen-combust where dwf-movimen-combust.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-movimen-combust FIELDS() where dwf-movimen-combust.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-movimen-combust' + STRING(i-lidos)).
            delete dwf-movimen-combust.
        end.
        release dwf-movimen-combust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-movimen-combust-concil' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-movimen-combust-concil where dwf-movimen-combust-concil.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-movimen-combust-concil FIELDS() where dwf-movimen-combust-concil.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-movimen-combust-concil' + STRING(i-lidos)).
            delete dwf-movimen-combust-concil.
        end.
        release dwf-movimen-combust-concil.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-movimen-combust-tanque' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-movimen-combust-tanque where dwf-movimen-combust-tanque.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-movimen-combust-tanque FIELDS() where dwf-movimen-combust-tanque.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-movimen-combust-tanque' + STRING(i-lidos)).
            delete dwf-movimen-combust-tanque.
        end.
        release dwf-movimen-combust-tanque.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-movto-estoq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-movto-estoq where dwf-movto-estoq.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-movto-estoq FIELDS() where dwf-movto-estoq.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-movto-estoq' + STRING(i-lidos)).
            delete dwf-movto-estoq.
        end.
        release dwf-movto-estoq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-ord-produc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-ord-produc where dwf-ord-produc.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-ord-produc FIELDS() where dwf-ord-produc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-ord-produc' + STRING(i-lidos)).
            delete dwf-ord-produc.
        end.
        release dwf-ord-produc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-outros-docto-operac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-outros-docto-operac where dwf-outros-docto-operac.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-outros-docto-operac FIELDS() where dwf-outros-docto-operac.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-outros-docto-operac' + STRING(i-lidos)).
            delete dwf-outros-docto-operac.
        end.
        release dwf-outros-docto-operac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-reduc-z-tot-parcial-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-reduc-z-tot-parcial-item where dwf-reduc-z-tot-parcial-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-reduc-z-tot-parcial-item FIELDS() where dwf-reduc-z-tot-parcial-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-reduc-z-tot-parcial-item' + STRING(i-lidos)).
            delete dwf-reduc-z-tot-parcial-item.
        end.
        release dwf-reduc-z-tot-parcial-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-val-agreg-munpio' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-val-agreg-munpio where dwf-val-agreg-munpio.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-val-agreg-munpio FIELDS() where dwf-val-agreg-munpio.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-val-agreg-munpio' + STRING(i-lidos)).
            delete dwf-val-agreg-munpio.
        end.
        release dwf-val-agreg-munpio.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-vol-vendas-combust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-vol-vendas-combust where dwf-vol-vendas-combust.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-vol-vendas-combust FIELDS() where dwf-vol-vendas-combust.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-vol-vendas-combust' + STRING(i-lidos)).
            delete dwf-vol-vendas-combust.
        end.
        release dwf-vol-vendas-combust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'op-sfc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.op-sfc where op-sfc.cod-item-aux-sfc = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.op-sfc FIELDS() where op-sfc.cod-item-aux-sfc = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.op-sfc' + STRING(i-lidos)).
            delete op-sfc.
        end.
        release op-sfc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'split-operac' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.split-operac where split-operac.cod-item-op = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.split-operac FIELDS() where split-operac.cod-item-op = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.split-operac' + STRING(i-lidos)).
            delete split-operac.
        end.
        release split-operac.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-aloc-cust-icms-conjto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-aloc-cust-icms-conjto where cat83-aloc-cust-icms-conjto.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-aloc-cust-icms-conjto FIELDS() where cat83-aloc-cust-icms-conjto.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-aloc-cust-icms-conjto' + STRING(i-lidos)).
            delete cat83-aloc-cust-icms-conjto.
        end.
        release cat83-aloc-cust-icms-conjto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-apurac-devol' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-apurac-devol where cat83-apurac-devol.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-apurac-devol FIELDS() where cat83-apurac-devol.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-apurac-devol' + STRING(i-lidos)).
            delete cat83-apurac-devol.
        end.
        release cat83-apurac-devol.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-apurac-icms-cust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-apurac-icms-cust FIELDS() use-index ct83prcc-07 where cat83-apurac-icms-cust.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-apurac-icms-cust' + STRING(i-lidos)).
            delete cat83-apurac-icms-cust.
        end.
        release cat83-apurac-icms-cust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-apurac-icms-cust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-apurac-icms-cust where cat83-apurac-icms-cust.cod-item-orig-dest = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-apurac-icms-cust FIELDS() where cat83-apurac-icms-cust.cod-item-orig-dest = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-apurac-icms-cust' + STRING(i-lidos)).
            delete cat83-apurac-icms-cust.
        end.
        release cat83-apurac-icms-cust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-export-indta-comprov' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-export-indta-comprov where cat83-export-indta-comprov.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-export-indta-comprov FIELDS() where cat83-export-indta-comprov.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-export-indta-comprov' + STRING(i-lidos)).
            delete cat83-export-indta-comprov.
        end.
        release cat83-export-indta-comprov.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-ficha-tec-produc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-ficha-tec-produc where cat83-ficha-tec-produc.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-ficha-tec-produc FIELDS() where cat83-ficha-tec-produc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-ficha-tec-produc' + STRING(i-lidos)).
            delete cat83-ficha-tec-produc.
        end.
        release cat83-ficha-tec-produc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-ficha-tec-unit-produc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-ficha-tec-unit-produc where cat83-ficha-tec-unit-produc.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-ficha-tec-unit-produc FIELDS() where cat83-ficha-tec-unit-produc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-ficha-tec-unit-produc' + STRING(i-lidos)).
            delete cat83-ficha-tec-unit-produc.
        end.
        release cat83-ficha-tec-unit-produc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-gerac-cr-acum-icms' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-gerac-cr-acum-icms FIELDS() use-index ct83grcc-04 where cat83-gerac-cr-acum-icms.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-gerac-cr-acum-icms' + STRING(i-lidos)).
            delete cat83-gerac-cr-acum-icms.
        end.
        release cat83-gerac-cr-acum-icms.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-ident-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-ident-item where cat83-ident-item.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-ident-item FIELDS() where cat83-ident-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-ident-item' + STRING(i-lidos)).
            delete cat83-ident-item.
        end.
        release cat83-ident-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-invent-produt-elabor' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-invent-produt-elabor where cat83-invent-produt-elabor.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-invent-produt-elabor FIELDS() where cat83-invent-produt-elabor.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-invent-produt-elabor' + STRING(i-lidos)).
            delete cat83-invent-produt-elabor.
        end.
        release cat83-invent-produt-elabor.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-rat-energ-ggf' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.cat83-rat-energ-ggf where cat83-rat-energ-ggf.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-rat-energ-ggf FIELDS() where cat83-rat-energ-ggf.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-rat-energ-ggf' + STRING(i-lidos)).
            delete cat83-rat-energ-ggf.
        end.
        release cat83-rat-energ-ggf.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'cat83-sdo-icms-cust' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.cat83-sdo-icms-cust FIELDS() use-index ct83sdcm-01 where cat83-sdo-icms-cust.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.cat83-sdo-icms-cust' + STRING(i-lidos)).
            delete cat83-sdo-icms-cust.
        end.
        release cat83-sdo-icms-cust.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'desc-it-nota-fisc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.desc-it-nota-fisc FIELDS() use-index dsctmnf-01 where desc-it-nota-fisc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.desc-it-nota-fisc' + STRING(i-lidos)).
            delete desc-it-nota-fisc.
        end.
        release desc-it-nota-fisc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'desc-ped-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.desc-ped-item FIELDS() use-index dscpdtm-01 where desc-ped-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.desc-ped-item' + STRING(i-lidos)).
            delete desc-ped-item.
        end.
        release desc-ped-item.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'estrut-item-fci' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.estrut-item-fci where estrut-item-fci.cod-item-fci = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.estrut-item-fci FIELDS() where estrut-item-fci.cod-item-fci = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.estrut-item-fci' + STRING(i-lidos)).
            delete estrut-item-fci.
        end.
        release estrut-item-fci.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'it-nota-fisc-serial' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.it-nota-fisc-serial where it-nota-fisc-serial.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.it-nota-fisc-serial FIELDS() where it-nota-fisc-serial.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.it-nota-fisc-serial' + STRING(i-lidos)).
            delete it-nota-fisc-serial.
        end.
        release it-nota-fisc-serial.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-entr-st' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.item-entr-st where item-entr-st.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.item-entr-st FIELDS() where item-entr-st.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.item-entr-st' + STRING(i-lidos)).
            delete item-entr-st.
        end.
        release item-entr-st.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-nf-adc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.item-nf-adc where item-nf-adc.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.item-nf-adc FIELDS() where item-nf-adc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.item-nf-adc' + STRING(i-lidos)).
            delete item-nf-adc.
        end.
        release item-nf-adc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-nfs-st' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.item-nfs-st FIELDS() use-index itmnfsst-item where item-nfs-st.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.item-nfs-st' + STRING(i-lidos)).
            delete item-nfs-st.
        end.
        release item-nfs-st.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'nota-fisc-adc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movdis.nota-fisc-adc where nota-fisc-adc.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movdis.nota-fisc-adc FIELDS() where nota-fisc-adc.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movdis.nota-fisc-adc' + STRING(i-lidos)).
            delete nota-fisc-adc.
        end.
        release nota-fisc-adc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'componente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.componente where componente.cod-item-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.componente FIELDS() where componente.cod-item-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.componente' + STRING(i-lidos)).
            delete componente.
        end.
        release componente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-doc-est-troca' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.item-doc-est-troca where item-doc-est-troca.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.item-doc-est-troca FIELDS() where item-doc-est-troca.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.item-doc-est-troca' + STRING(i-lidos)).
            delete item-doc-est-troca.
        end.
        release item-doc-est-troca.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item-docto-estoq-nfe-imp' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.item-docto-estoq-nfe-imp where item-docto-estoq-nfe-imp.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.item-docto-estoq-nfe-imp FIELDS() where item-docto-estoq-nfe-imp.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.item-docto-estoq-nfe-imp' + STRING(i-lidos)).
            delete item-docto-estoq-nfe-imp.
        end.
        release item-docto-estoq-nfe-imp.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'movto-coprodut' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.movto-coprodut FIELDS() use-index mvtcprdt-id where movto-coprodut.cod-item-coprodut = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.movto-coprodut' + STRING(i-lidos)).
            delete movto-coprodut.
        end.
        release movto-coprodut.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'oper-ord' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.oper-ord FIELDS() use-index oper-ord-refugo where oper-ord.cod-item-refugo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.oper-ord' + STRING(i-lidos)).
            delete oper-ord.
        end.
        release oper-ord.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'ord-prod' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.ord-prod FIELDS() use-index ord-produc-refugo where ord-prod.cod-item-refugo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.ord-prod' + STRING(i-lidos)).
            delete ord-prod.
        end.
        release ord-prod.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rat-componente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.rat-componente where rat-componente.cod-item-pai = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rat-componente FIELDS() where rat-componente.cod-item-pai = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rat-componente' + STRING(i-lidos)).
            delete rat-componente.
        end.
        release rat-componente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rma-it-dep' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rma-it-dep FIELDS() use-index item where rma-it-dep.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rma-it-dep' + STRING(i-lidos)).
            delete rma-it-dep.
        end.
        release rma-it-dep.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rma-it-troca' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from movind.rma-it-troca where rma-it-troca.cod-item = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rma-it-troca FIELDS() where rma-it-troca.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rma-it-troca' + STRING(i-lidos)).
            delete rma-it-troca.
        end.
        release rma-it-troca.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'rma-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each movind.rma-item FIELDS() use-index item where rma-item.cod-item = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'movind.rma-item' + STRING(i-lidos)).
            delete rma-item.
        end.
        release rma-item.
    end.

    /*------------------------------COD-PRODUTO------------------------------*/
    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'desconto' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.desconto where desconto.cod-produto-coml = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.desconto FIELDS() where desconto.cod-produto-coml = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.desconto' + STRING(i-lidos)).
            delete desconto.
        end.
        release desconto.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'est-prod-coml' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.est-prod-coml FIELDS() use-index codigo where est-prod-coml.cod-produto-coml = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.est-prod-coml' + STRING(i-lidos)).
            delete est-prod-coml.
        end.
        release est-prod-coml.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'integr-dis' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.integr-dis where integr-dis.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.integr-dis FIELDS() where integr-dis.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.integr-dis' + STRING(i-lidos)).
            delete integr-dis.
        end.
        release integr-dis.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'integr-mat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.integr-mat where integr-mat.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.integr-mat FIELDS() where integr-mat.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.integr-mat' + STRING(i-lidos)).
            delete integr-mat.
        end.
        release integr-mat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'param-pa' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.param-pa where param-pa.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.param-pa FIELDS() where param-pa.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.param-pa' + STRING(i-lidos)).
            delete param-pa.
        end.
        release param-pa.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'preco-pad' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.preco-pad where preco-pad.cod-produto-coml = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.preco-pad FIELDS() where preco-pad.cod-produto-coml = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.preco-pad' + STRING(i-lidos)).
            delete preco-pad.
        end.
        release preco-pad.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'prod-ordem' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.prod-ordem where prod-ordem.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.prod-ordem FIELDS() where prod-ordem.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.prod-ordem' + STRING(i-lidos)).
            delete prod-ordem.
        end.
        release prod-ordem.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'produto-coml' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.produto-coml FIELDS() use-index codigo where produto-coml.cod-produto-coml = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.produto-coml' + STRING(i-lidos)).
            delete produto-coml.
        end.
        release produto-coml.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'produto-pat' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.produto-pat where produto-pat.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.produto-pat FIELDS() where produto-pat.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.produto-pat' + STRING(i-lidos)).
            delete produto-pat.
        end.
        release produto-pat.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'time-connection' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.time-connection where time-connection.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.time-connection FIELDS() where time-connection.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.time-connection' + STRING(i-lidos)).
            delete time-connection.
        end.
        release time-connection.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-pli-destaq' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-pli-destaq where zfm-pli-destaq.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-pli-destaq FIELDS() where zfm-pli-destaq.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-pli-destaq' + STRING(i-lidos)).
            delete zfm-pli-destaq.
        end.
        release zfm-pli-destaq.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-pli-det' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-pli-det where zfm-pli-det.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-pli-det FIELDS() where zfm-pli-det.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-pli-det' + STRING(i-lidos)).
            delete zfm-pli-det.
        end.
        release zfm-pli-det.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-pli-ncm' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-pli-ncm where zfm-pli-ncm.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-pli-ncm FIELDS() where zfm-pli-ncm.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-pli-ncm' + STRING(i-lidos)).
            delete zfm-pli-ncm.
        end.
        release zfm-pli-ncm.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-pli-oc' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-pli-oc where zfm-pli-oc.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-pli-oc FIELDS() where zfm-pli-oc.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-pli-oc' + STRING(i-lidos)).
            delete zfm-pli-oc.
        end.
        release zfm-pli-oc.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'zfm-pli-orgao-anuente' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgcad.zfm-pli-orgao-anuente where zfm-pli-orgao-anuente.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.zfm-pli-orgao-anuente FIELDS() where zfm-pli-orgao-anuente.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.zfm-pli-orgao-anuente' + STRING(i-lidos)).
            delete zfm-pli-orgao-anuente.
        end.
        release zfm-pli-orgao-anuente.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'dwf-cta-ctbl-refer' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.dwf-cta-ctbl-refer where dwf-cta-ctbl-refer.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.dwf-cta-ctbl-refer FIELDS() where dwf-cta-ctbl-refer.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.dwf-cta-ctbl-refer' + STRING(i-lidos)).
            delete dwf-cta-ctbl-refer.
        end.
        release dwf-cta-ctbl-refer.
    end.

    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'wt-item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    select count (*) into i-cont from mgmov.wt-item where wt-item.cod-produto = sanea-item.it-codigo. 
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgmov.wt-item FIELDS() where wt-item.cod-produto = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgmov.wt-item' + STRING(i-lidos)).
            delete wt-item.
        end.
        release wt-item.
    end.


    /*------------------------------ITEM------------------------------*/
    OUTPUT TO value(session:temp-directory + "log.txt") append.
    put unformat 'item' ';' string(time,'HH:MM:SS') skip.
    OUTPUT close.
    assign i-cont = 1.
    if i-cont > 0 then do:
         ASSIGN i-lidos = 0.
        for each mgcad.item FIELDS() use-index codigo where item.it-codigo = sanea-item.it-codigo.
            ASSIGN i-lidos = i-lidos + 1.
            IF i-lidos MOD 1000 = 0 THEN
            RUN pi-acompanhar IN h-acomp (INPUT 'mgcad.item' + STRING(i-lidos)).
            delete item.
        end.
        release item.
    end.

    sanea-item.processado = yes.

END.

RUN pi-finalizar IN h-acomp.

