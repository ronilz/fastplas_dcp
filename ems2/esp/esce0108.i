/*****************************************************************
**
**   CE0108.I - GERA SALDO-ESTOQ PARA A MOVIMENTACAO
**
*****************************************************************/

find {1} use-index estabel-dep 
  where {1}.cod-estabel = {2}.cod-estabel
    and {1}.cod-depos   = {2}.cod-depos
    and {1}.cod-localiz = {2}.cod-localiz
    and {1}.lote        = {2}.lote
    and {1}.it-codigo   = {2}.it-codigo
    and {1}.cod-refer   = {2}.cod-refer no-lock no-error.

if not avail {1} then do:
   create {1}.
   assign {1}.cod-estabel = {2}.cod-estabel
          {1}.cod-depos   = {2}.cod-depos
          {1}.cod-localiz = {2}.cod-localiz
          {1}.lote        = {2}.lote
          {1}.it-codigo   = {2}.it-codigo
          {1}.cod-refer   = {2}.cod-refer.
end.

