/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESCP010RP 2.12.00.001}

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i ESCP010RP MCP}
&ENDIF

/* ***************************  Definitions  ************************** */
&global-define programa ESCP010RP

/* definicao de temp-tables */
DEFINE TEMP-TABLE tt-param NO-UNDO
    FIELD destino          AS INTEGER
    FIELD arquivo          AS CHARACTER FORMAT "x(35)"
    FIELD usuario          AS CHARACTER FORMAT "x(12)"
    FIELD data-exec        AS DATE
    FIELD hora-exec        AS INTEGER
    FIELD data-ini         LIKE es-abastec-lin-item.data
    FIELD data-fim         LIKE es-abastec-lin-item.data
    FIELD it-codigo-ini    LIKE es-abastec-lin-item.it-codigo
    FIELD it-codigo-fim    LIKE es-abastec-lin-item.it-codigo.

DEF TEMP-TABLE tt-raw-digita
    FIELD raw-digita AS RAW.

DEF TEMP-TABLE tt-item-pai NO-UNDO
    FIELD tt-it-pai     AS CHAR
    FIELD tt-nr-ord     AS INTEGER
    FIELD tt-dt-trans   AS DATE
    FIELD tt-quant      AS DEC
    FIELD tt-diff       AS INTEGER
    INDEX idx IS PRIMARY
        tt-it-pai tt-dt-trans.

DEF TEMP-TABLE tt-item-filho NO-UNDO
    FIELD tt-it-pai     AS CHAR
    FIELD tt-it-filho   AS CHAR
    FIELD tt-nr-ord     AS INTEGER
    FIELD tt-dt-trans   AS DATE
    FIELD tt-quant      AS DEC
    INDEX idx IS PRIMARY
        tt-it-pai tt-it-filho tt-dt-trans.

DEF TEMP-TABLE tt-ord-rep
    FIELD tt-nr-ord LIKE ord-prod.nr-ord-produ.

/* definicao de parametros */
DEF INPUT PARAM raw-param AS RAW NO-UNDO.
DEF INPUT PARAM TABLE FOR tt-raw-digita.

/* definicao de variaveis */
DEF VAR h-acomp             AS HANDLE               NO-UNDO.    
DEF VAR c-arquivo           AS CHAR                 NO-UNDO.
DEF VAR l-vazio             AS LOGICAL INIT TRUE    NO-UNDO.

/* definicao de streams */
DEF STREAM str-csv.
DEF BUFFER bfw-movto-estoq FOR movto-estoq.
    
/* main block */
CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

{include/i-rpvar.i}

/*IF i-num-ped-exec-rpw <> 0 THEN
    tt-param.arquivo = "ESAPB010_" + STRING(i-num-ped-exec-rpw) + ".xlsx".*/

RUN pi-retorna-diretorio 
     (tt-param.usuario,
     OUTPUT c-arquivo).

ASSIGN c-arquivo = REPLACE(c-arquivo,"/","\") + "\"
       c-arquivo = REPLACE(c-arquivo,"\\","\") + "escp010_" + STRING(TIME) + ".csv".

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.

RUN pi-inicializar IN h-acomp (INPUT "Processando...":U).

OUTPUT STREAM str-csv TO VALUE(c-arquivo) NO-CONVERT.

blk-main:
REPEAT ON ERROR UNDO blk-main, LEAVE blk-main
       ON STOP  UNDO blk-main, LEAVE blk-main:

    RUN pi-carregar-tt.

    LEAVE blk-main.

END. /* DO ON ERROR UNDO, LEAVE */

OUTPUT STREAM str-csv CLOSE.

RUN pi-excel.

RUN pi-finalizar IN h-acomp.

RETURN "OK".

/* definicao de procedures */
PROCEDURE pi-excel:

    DEF VAR ch_excel        AS office.iface.excel.ExcelWrapper   NO-UNDO.
    DEF VAR ch_workbook     AS office.iface.excel.Workbook   NO-UNDO.
    DEF VAR i-col           AS INT          NO-UNDO.
    DEF VAR c-range         AS CHAR         NO-UNDO.

    RUN pi-acompanhar IN h-acomp (INPUT "Criando planilha...").

    /* gera��o excel */
    {office/office.i Excel ch_excel}

    IF NOT VALID-OBJECT(ch_excel) THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "N�o foi possivel iniciar o Excel!~~N�o foi possivel iniciar o Excel!").

        RETURN "NOK".

    END. /* IF NOT VALID-OBJECT(ch_excel) */

    ASSIGN ch_excel:VISIBLE = FALSE
           ch_workbook      = ch_excel:WorkBooks:ADD(SEARCH(c-arquivo)).
    ch_excel:displayAlerts = NO.
    ch_excel:VISIBLE = TRUE.
    ch_excel:Cells:EntireColumn:AutoFit().
    ch_excel:Range("A1"):Select().
    DELETE OBJECT ch_workbook NO-ERROR.
    DELETE OBJECT ch_excel NO-ERROR.

END PROCEDURE. /* PROCEDURE pi-excel */

PROCEDURE pi-carregar-tt:

    EMPTY TEMP-TABLE tt-item-pai.
    EMPTY TEMP-TABLE tt-item-filho.

    /* verificar todas as ordens que foram reportada no periodo */
    FOR EACH movto-estoq NO-LOCK USE-INDEX estab-dep
        WHERE movto-estoq.cod-estabel = tt-param.it-codigo-ini
          AND movto-estoq.dt-trans >= tt-param.data-ini
          AND movto-estoq.dt-trans <= tt-param.data-fim
          AND movto-estoq.nr-ord-produ <> 0
          //teste apenas
          //AND movto-estoq.nr-ord-produ = 135962
        .

        FIND FIRST tt-ord-rep 
            WHERE tt-ord-rep.tt-nr-ord = movto-estoq.nr-ord-produ
            NO-ERROR.
        IF NOT AVAIL tt-ord-rep THEN DO:
            CREATE tt-ord-rep.
            ASSIGN tt-ord-rep.tt-nr-ord = movto-estoq.nr-ord-produ.
        END.
            
    END.

    FOR EACH tt-ord-rep NO-LOCK,
        FIRST ord-prod NO-LOCK
        WHERE ord-prod.nr-ord-produ = tt-ord-rep.tt-nr-ord.

        /** buscar reportes do acabado no per�odo, acumulando por dia **/
        FOR EACH movto-estoq NO-LOCK
            WHERE movto-estoq.it-codigo = ord-prod.it-codigo
              AND movto-estoq.cod-estabel = tt-param.it-codigo-ini
              AND movto-estoq.dt-trans >= tt-param.data-ini
              AND movto-estoq.dt-trans <= tt-param.data-fim
              AND movto-estoq.nr-ord-produ = ord-prod.nr-ord-produ
              // acabado somente
              AND movto-estoq.esp-docto = 1.

            RUN pi-acompanhar IN h-acomp (INPUT "Item/Data: " + STRING(movto-estoq.it-codigo) + "/" + STRING(movto-estoq.dt-trans)).

            FIND FIRST tt-item-pai
                WHERE tt-item-pai.tt-it-pai   = movto-estoq.it-codigo
                  AND tt-item-pai.tt-nr-ord   = movto-estoq.nr-ord-produ
                  AND tt-item-pai.tt-dt-trans = movto-estoq.dt-trans NO-ERROR.
            IF NOT AVAIL tt-item-pai THEN DO:
                CREATE tt-item-pai.
                ASSIGN tt-item-pai.tt-it-pai   = movto-estoq.it-codigo
                       tt-item-pai.tt-dt-trans = movto-estoq.dt-trans
                       tt-item-pai.tt-nr-ord   = movto-estoq.nr-ord-produ.
            END.
    
            IF movto-estoq.tipo-trans = 1 THEN
                tt-item-pai.tt-quant = tt-item-pai.tt-quant + movto-estoq.quantidade.
            ELSE
                tt-item-pai.tt-quant = tt-item-pai.tt-quant - movto-estoq.quantidade.

            //MESSAGE "1" movto-estoq.quantidade
            //    VIEW-AS ALERT-BOX INFO BUTTONS OK.

        END.

        /** buscar requisicoes das reservas, acumuladas por dia **/
        FOR EACH reservas OF ord-prod NO-LOCK,
            FIRST ITEM NO-LOCK
            WHERE ITEM.it-codigo = reservas.it-codigo 
              AND item.ge-codigo = 11
              // desconsiderar os 5
              AND NOT ITEM.it-codigo BEGINS "5".

            FOR EACH movto-estoq NO-LOCK
                WHERE movto-estoq.it-codigo = reservas.it-codigo
                  AND movto-estoq.cod-estabel = tt-param.it-codigo-ini
                  AND movto-estoq.dt-trans >= tt-param.data-ini
                  AND movto-estoq.dt-trans <= tt-param.data-fim
                  AND movto-estoq.nr-ord-produ = ord-prod.nr-ord-produ
                  // buscar somente requisicoes, desconsiderar o estornos
                  AND movto-estoq.esp-docto = 28
                .
    
                RUN pi-acompanhar IN h-acomp (INPUT "Item/Data: " + STRING(movto-estoq.it-codigo) + "/" + STRING(movto-estoq.dt-trans)).

                FIND FIRST tt-item-filho
                     WHERE tt-item-filho.tt-it-pai   = ord-prod.it-codigo 
                       AND tt-item-filho.tt-it-filho = reservas.it-codigo 
                       AND tt-item-filho.tt-nr-ord   = ord-prod.nr-ord-produ
                       AND tt-item-filho.tt-dt-trans = movto-estoq.dt-trans
                    NO-ERROR.

                IF NOT AVAIL tt-item-filho THEN DO:

                    CREATE tt-item-filho.
                    ASSIGN tt-item-filho.tt-it-pai   = ord-prod.it-codigo    
                           tt-item-filho.tt-it-filho = reservas.it-codigo
                           tt-item-filho.tt-dt-trans = movto-estoq.dt-trans
                           tt-item-filho.tt-nr-ord   = ord-prod.nr-ord-produ.
                END.

                IF movto-estoq.tipo-trans = 1 THEN
                    ASSIGN tt-item-filho.tt-quant = tt-item-filho.tt-quant + movto-estoq.quantidade.
                ELSE
                    ASSIGN tt-item-filho.tt-quant = tt-item-filho.tt-quant - movto-estoq.quantidade.

                //MESSAGE "2" movto-estoq.it-codigo movto-estoq.quantidade
                //    VIEW-AS ALERT-BOX INFO BUTTONS OK.


            END.

        END.

    END.

    /** checar se existe diferen�a nas quantidaades **/
    FOR EACH tt-item-filho,
        FIRST tt-item-pai 
        WHERE tt-item-pai.tt-nr-ord = tt-item-filho.tt-nr-ord
          AND tt-item-pai.tt-it-pai = tt-item-filho.tt-it-pai
          AND tt-item-pai.tt-dt-trans = tt-item-filho.tt-dt-trans.

        IF abs(tt-item-filho.tt-quant) <> abs(tt-item-pai.tt-quant) THEN
            ASSIGN tt-item-pai.tt-dif = 1.
    END.

    PUT STREAM str-csv UNFORMATTED
        "Item"	        ";"
        "Ordem Prod"    ";"
        "Data Trans"	";"
        "Quantidade"    ";" SKIP .

    FOR EACH tt-item-pai NO-LOCK
        WHERE tt-item-pai.tt-diff > 0.

        PUT STREAM str-csv UNFORMATTED
            "�" + tt-item-pai.tt-it-pai   ";" 
            tt-item-pai.tt-nr-ord ";"
            tt-item-pai.tt-dt-trans ";"
            tt-item-pai.tt-quant    ";" SKIP.
        
        FOR EACH tt-item-filho 
           WHERE tt-item-filho.tt-it-pai   = tt-item-pai.tt-it-pai
             AND tt-item-filho.tt-dt-trans = tt-item-pai.tt-dt-trans
             AND tt-item-filho.tt-nr-ord   = tt-item-pai.tt-nr-ord.

            RUN pi-acompanhar IN h-acomp (INPUT "Item/Data: " + STRING(tt-item-pai.tt-it-pai) + "/" + STRING(tt-item-pai.tt-dt-trans)).

            PUT STREAM str-csv UNFORMATTED
                "�" + tt-item-filho.tt-it-filho   ";"
                tt-item-filho.tt-nr-ord    ";"
                tt-item-filho.tt-dt-trans         ";"
                tt-item-filho.tt-quant            ";"
                SKIP.
        END. 
            
    END. 

END PROCEDURE. /* PROCEDURE pi-carregar-tt */

{utils/retorna_diretorio_usuario.i}
