&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESCP006B-V01 2.08.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i ESCP006B-V01 MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES es-abastec-lin-item
&Scoped-define FIRST-EXTERNAL-TABLE es-abastec-lin-item


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR es-abastec-lin-item.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS es-abastec-lin-item.it-codigo ~
es-abastec-lin-item.quantidade es-abastec-lin-item.qtd-peca-skid ~
es-abastec-lin-item.qtd-total-skid 
&Scoped-define ENABLED-TABLES es-abastec-lin-item
&Scoped-define FIRST-ENABLED-TABLE es-abastec-lin-item
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold 
&Scoped-Define DISPLAYED-FIELDS es-abastec-lin-item.nr-linha ~
es-abastec-lin-item.data es-abastec-lin-item.nr-seq ~
es-abastec-lin-item.it-codigo es-abastec-lin-item.quantidade ~
es-abastec-lin-item.qtd-peca-skid es-abastec-lin-item.qtd-total-skid 
&Scoped-define DISPLAYED-TABLES es-abastec-lin-item
&Scoped-define FIRST-DISPLAYED-TABLE es-abastec-lin-item
&Scoped-Define DISPLAYED-OBJECTS fi-desc-linha fi-desc-item fi-codigo-refer 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
it-codigo||y|mgfas.es-abastec-lin-item.it-codigo
nr-seq||y|mgfas.es-abastec-lin-item.nr-seq
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "it-codigo,nr-seq"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE fi-codigo-refer AS CHARACTER FORMAT "X(20)":U 
     LABEL "C�digo Cliente" 
     VIEW-AS FILL-IN 
     SIZE 24 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-item AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 43 BY .88 NO-UNDO.

DEFINE VARIABLE fi-desc-linha AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 55 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 80 BY 3.5.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 80 BY 5.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     es-abastec-lin-item.nr-linha AT ROW 1.25 COL 16 COLON-ALIGNED WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 4.57 BY .88
     fi-desc-linha AT ROW 1.25 COL 22 COLON-ALIGNED NO-LABEL WIDGET-ID 18
     es-abastec-lin-item.data AT ROW 2.25 COL 16 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 10 BY .88
     es-abastec-lin-item.nr-seq AT ROW 3.25 COL 16 COLON-ALIGNED WIDGET-ID 10
          VIEW-AS FILL-IN 
          SIZE 8 BY .88
     es-abastec-lin-item.it-codigo AT ROW 4.75 COL 16 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     fi-desc-item AT ROW 4.75 COL 34 COLON-ALIGNED NO-LABEL WIDGET-ID 20
     fi-codigo-refer AT ROW 5.75 COL 16 COLON-ALIGNED WIDGET-ID 24
     es-abastec-lin-item.quantidade AT ROW 6.75 COL 16 COLON-ALIGNED WIDGET-ID 16
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     es-abastec-lin-item.qtd-peca-skid AT ROW 7.75 COL 16 COLON-ALIGNED WIDGET-ID 12
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     es-abastec-lin-item.qtd-total-skid AT ROW 8.75 COL 16 COLON-ALIGNED WIDGET-ID 14
          VIEW-AS FILL-IN 
          SIZE 12.57 BY .88
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 4.5 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.es-abastec-lin-item
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 8.75
         WIDTH              = 80.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN es-abastec-lin-item.data IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-codigo-refer IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-item IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-desc-linha IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN es-abastec-lin-item.nr-linha IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN es-abastec-lin-item.nr-seq IN FRAME f-main
   NO-ENABLE                                                            */
ASSIGN 
       es-abastec-lin-item.qtd-peca-skid:READ-ONLY IN FRAME f-main        = TRUE.

ASSIGN 
       es-abastec-lin-item.qtd-total-skid:READ-ONLY IN FRAME f-main        = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME es-abastec-lin-item.it-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-item.it-codigo V-table-Win
ON F5 OF es-abastec-lin-item.it-codigo IN FRAME f-main /* Item */
DO:
    {include/zoomvar.i &prog-zoom="inzoom/z01in172.w"
                       &campo=SELF
                       &campozoom=it-codigo
                       &campo2=fi-desc-item
                       &campozoom2=desc-item
                       &campo3=fi-codigo-refer
                       &campozoom3=codigo-refer}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-item.it-codigo V-table-Win
ON LEAVE OF es-abastec-lin-item.it-codigo IN FRAME f-main /* Item */
DO:
  
    FOR FIRST ITEM FIELDS(desc-item codigo-refer) WHERE
        ITEM.it-codigo = SELF:SCREEN-VALUE NO-LOCK:
    END. /* FOR FIRST ITEM */

    IF AVAIL ITEM THEN
        ASSIGN fi-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME}    = ITEM.desc-item
               fi-codigo-refer:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.codigo-refer.
    ELSE
        ASSIGN fi-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME}    = ""
               fi-codigo-refer:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".

    RUN pi-calc-skid.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-item.it-codigo V-table-Win
ON MOUSE-SELECT-DBLCLICK OF es-abastec-lin-item.it-codigo IN FRAME f-main /* Item */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-item.it-codigo V-table-Win
ON RETURN OF es-abastec-lin-item.it-codigo IN FRAME f-main /* Item */
DO:
  
    APPLY "ENTRY" TO es-abastec-lin-item.quantidade IN FRAME {&FRAME-NAME}.

    RETURN NO-APPLY.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-abastec-lin-item.nr-linha
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-item.nr-linha V-table-Win
ON LEAVE OF es-abastec-lin-item.nr-linha IN FRAME f-main /* Linha Produ��o */
DO:
    FOR FIRST lin-prod FIELDS(descricao) WHERE
        lin-prod.nr-linha = INT(SELF:SCREEN-VALUE) NO-LOCK:
    END.

    fi-desc-linha:SCREEN-VALUE IN FRAME {&FRAME-NAME} = IF AVAIL lin-prod THEN lin-prod.descricao
                                                        ELSE "".
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME es-abastec-lin-item.quantidade
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL es-abastec-lin-item.quantidade V-table-Win
ON LEAVE OF es-abastec-lin-item.quantidade IN FRAME f-main /* Quantidade */
DO:
  
    RUN pi-calc-skid.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  IF es-abastec-lin-item.it-codigo:LOAD-MOUSE-POINTER("image/lupa.cur") IN FRAME {&FRAME-NAME} THEN.
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* No Foreign keys are accepted by this SmartObject. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "es-abastec-lin-item"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "es-abastec-lin-item"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

    find es-abastec-lin where rowid (es-abastec-lin) = v-row-parent no-lock no-error.
    if available es-abastec-lin then do:
        assign es-abastec-lin-item.nr-linha:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(es-abastec-lin.nr-linha)
               es-abastec-lin-item.data:SCREEN-VALUE IN FRAME {&FRAME-NAME}     = STRING(es-abastec-lin.data).
        APPLY "LEAVE" TO es-abastec-lin-item.nr-linha IN FRAME {&FRAME-NAME}.
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */

    RUN pi-validate.
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    IF es-abastec-lin-item.nr-seq = 0 THEN DO:
    
        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 27100,
                           INPUT "Deseja fechar a sequ�ncia?~~Fecha a sequ�ncia.").
    
        IF LOGICAL(RETURN-VALUE) THEN DO:
    
            RUN pi-fechar-sequencia (INPUT es-abastec-lin-item.nr-linha,
                                     INPUT es-abastec-lin-item.data).
    
        END. /* IF LOGICAL(RETURN-VALUE) */

    END. /* IF es-abastec-lin.nr-seq = 0 */

    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-create-record V-table-Win 
PROCEDURE local-create-record :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

    RUN dispatch IN THIS-PROCEDURE ( INPUT 'create-record':U ) .

    find es-abastec-lin where rowid (es-abastec-lin) = v-row-parent no-lock no-error.
    if available es-abastec-lin then do:
        assign es-abastec-lin-item.nr-linha = es-abastec-lin.nr-linha
               es-abastec-lin-item.data     = es-abastec-lin.data
               es-abastec-lin-item.hora-ini = STRING(TIME,"HH:MM").
    end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

    APPLY "LEAVE" TO es-abastec-lin-item.nr-linha IN FRAME {&FRAME-NAME}.
    FOR FIRST ITEM FIELDS(desc-item codigo-refer) WHERE
        ITEM.it-codigo = es-abastec-lin-item.it-codigo NO-LOCK:
    END.

    ASSIGN fi-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = IF AVAIL ITEM THEN ITEM.desc-item ELSE ""
           fi-codigo-refer:SCREEN-VALUE IN FRAME {&FRAME-NAME} = IF AVAIL ITEM THEN ITEM.codigo-refer ELSE "".


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-calc-skid V-table-Win 
PROCEDURE pi-calc-skid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEF VAR d-qtd-peca-skid     AS DEC  NO-UNDO.
    DEF VAR d-qtd-total-skid    AS DEC  NO-UNDO.
    DEF VAR d-quantidade        AS DEC  NO-UNDO.

    FOR FIRST es-item-skid WHERE
        es-item-skid.it-codigo = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.it-codigo NO-LOCK:
    END. /* FOR FIRST es-item-skid */

    IF AVAIL es-item-skid THEN DO:

        ASSIGN d-quantidade     = DEC(es-abastec-lin-item.quantidade:SCREEN-VALUE IN FRAME {&FRAME-NAME})
               d-qtd-peca-skid  = es-item-skid.qtd-peca-skid
               d-qtd-total-skid = d-quantidade / d-qtd-peca-skid.

        IF TRUNCATE(d-qtd-total-skid,0) < d-qtd-total-skid THEN
            d-qtd-total-skid = TRUNCATE(d-qtd-total-skid,0) + 1.

    END. /* IF AVAIL es-item-skid */

    ASSIGN es-abastec-lin-item.qtd-peca-skid:SCREEN-VALUE IN FRAME {&FRAME-NAME}  = STRING(d-qtd-peca-skid)
           es-abastec-lin-item.qtd-total-skid:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(d-qtd-total-skid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-fechar-sequencia V-table-Win 
PROCEDURE pi-fechar-sequencia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEF INPUT PARAM p-nr-linha  LIKE es-abastec-lin-item.nr-linha   NO-UNDO.
    DEF INPUT PARAM p-data      LIKE es-abastec-lin-item.data       NO-UNDO.

    DEF VAR i-nr-seq AS INT NO-UNDO.

    DEF BUFFER b-es-abastec-lin-item  FOR es-abastec-lin-item.
    DEF BUFFER b-es-abastec-lin-item2 FOR es-abastec-lin-item.

    FOR LAST b-es-abastec-lin-item WHERE
        b-es-abastec-lin-item.nr-linha = p-nr-linha AND
        b-es-abastec-lin-item.data     = p-data     NO-LOCK
        BY b-es-abastec-lin-item.nr-seq:
    END.

    i-nr-seq = IF AVAIL b-es-abastec-lin-item THEN b-es-abastec-lin-item.nr-seq + 1
               ELSE 1.

    es-abastec-lin-item.nr-seq = i-nr-seq.

    FOR EACH b-es-abastec-lin-item WHERE
        b-es-abastec-lin-item.nr-linha = p-nr-linha AND
        b-es-abastec-lin-item.data     = p-data     AND
        b-es-abastec-lin-item.nr-seq   = 0          NO-LOCK:

        FIND b-es-abastec-lin-item2 WHERE
            ROWID(b-es-abastec-lin-item2) = ROWID(b-es-abastec-lin-item) EXCLUSIVE-LOCK NO-ERROR.

        b-es-abastec-lin-item2.nr-seq = i-nr-seq.

        RELEASE b-es-abastec-lin-item2.

    END. /* FOR EACH b-es-abastec-lin-item */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

    IF adm-new-record THEN DO:

        DEF BUFFER b-es-abastec-lin-item FOR es-abastec-lin-item.

        IF CAN-FIND(FIRST b-es-abastec-lin-item WHERE
                    b-es-abastec-lin-item.nr-linha     = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.nr-linha      AND
                    b-es-abastec-lin-item.data         = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.data          AND
                    b-es-abastec-lin-item.nr-seq       = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.nr-seq        AND
                    b-es-abastec-lin-item.it-codigo    = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.it-codigo)    THEN DO:

            RUN utp/ut-msgs.p (INPUT "show",
                               INPUT 17006,
                               INPUT "Registro j� existe!~~J� existe um registro com os dados informados.").

            RETURN "ADM-ERROR".

        END. /* IF CAN-FIND(FIRST b-es-abastec-lin-item */

    END. /* IF adm-new-record */

    IF NOT CAN-FIND(FIRST ITEM WHERE
                    ITEM.it-codigo = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.it-codigo) THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "O Item informado n�o existe!~~Informe um Item v�lido.").

        RETURN "ADM-ERROR".

    END. /* IF NOT CAN-FIND(FIRST ITEM */

    FIND FIRST es-item-skid WHERE
        es-item-skid.it-codigo = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.it-codigo NO-LOCK NO-ERROR.

    IF NOT AVAIL es-item-skid          OR
       es-item-skid.qtd-peca-skid <= 0 THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "Quantidade de pe�as por skid inv�lida.~~Verifique os par�metros do item.").

        RETURN "ADM-ERROR".

    END. /* IF NOT AVAIL es-item-skid */

    IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.quantidade <= 0 THEN DO:

        RUN utp/ut-msgs.p (INPUT "show",
                           INPUT 17006,
                           INPUT "Quantidade inv�lida.~~Informe uma quantidade v�lida.").

        RETURN "ADM-ERROR".

    END. /* IF INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.quantidade <= 0 */

    FOR EACH es-abastec-lin-parada WHERE
        es-abastec-lin-parada.nr-linha     = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.nr-linha      AND
        es-abastec-lin-parada.data         = INPUT FRAME {&FRAME-NAME} es-abastec-lin-item.data          NO-LOCK:

        IF REPLACE(STRING(TIME,"HH:MM"),":","") >= es-abastec-lin-parada.hora-ini AND
           REPLACE(STRING(TIME,"HH:MM"),":","") <= es-abastec-lin-parada.hora-fim THEN DO:

            RUN utp/ut-msgs.p (INPUT "show",
                               INPUT 17006,
                               INPUT "Hor�rio inv�lido!.~~Existe uma parada informada neste hor�rio '" +
                                     STRING(TIME,"HH:MM") + "'.").

            RETURN "ADM-ERROR".

        END.

    END. /* FOR EACH es-abastec-lin-parada */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "it-codigo" "es-abastec-lin-item" "it-codigo"}
  {src/adm/template/sndkycas.i "nr-seq" "es-abastec-lin-item" "nr-seq"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "es-abastec-lin-item"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

