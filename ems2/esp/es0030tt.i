define temp-table tt-param
    field parametro               as logical /* Imprime parametros */
    field v_cdn_empres_usuar      like mgcad.empresa.ep-codigo
    field i-es-ini                like ordem-compra.cod-estabel
    field i-es-fim                like ordem-compra.cod-estabel
    field i-nr-ini                like ordem-compra.numero-ordem
    field i-nr-fim                like ordem-compra.numero-ordem
    field data-ini                like ordem-compra.dat-ordem
    field data-fim                like ordem-compra.dat-ordem
    field i-rs-tip-cla            as int
    field v_cod_grp_usuar         as char
    field v_num_opcao             as int  format "9"
    field destino                 as integer
    field arquivo                 as char
    field usuario                 as char
    field data-exec               as date format "99/99/9999"
    field hora-exec               as integer
    field v_cod_grp_usuar_lst     as char.
	

define  temp-table tt-digita
    field v_cdn_empres_usuar like param_empres_rh.cdn_empresa
    field v_cdn_estab        like ordem-compra.cod-estabel
    field v_cdn_funcionario  like funcionario.cdn_funcionario
    field fc-nome            like funcionario.nom_pessoa_fisic format "x(30)"
    field dv-matric          like funcionario.num_digito_verfdor_func
    index id is primary unique
          v_cdn_empres_usuar
          v_cdn_estab
          v_cdn_funcionario.
