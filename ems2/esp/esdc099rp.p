/*******************************************************************************
** Programa...: ESDC099RP
** Vers�o.....: 1.000
** Data.......: Julho de 2010
** Autor......: Unidade TOTVS TRIAH
** Descri��o..: Extrato por RG do Item
*******************************************************************************/

/* Include de Controle de Vers�o */
{include/i-prgvrs.i ESDC099RP.P 1.00.00.000}

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino               as integer
    field arquivo               as char format "x(35)"
    field usuario               as char format "x(12)"
    field data-exec             as date
    field hora-exec             as integer
    field classifica            as integer
    field desc-classifica       as char format "x(40)"
    FIELD c-dir-importacao      AS CHAR
    field ini-nr-rack           like dc-movto-pto-controle.nr-rack
    field ini-rg-item           like dc-movto-pto-controle.rg-item 
    field ini-it-codigo         like dc-movto-pto-controle.it-codigo
    field ini-data              like dc-movto-pto-controle.data
    field ini-cod-pto-controle  like dc-movto-pto-controle.cod-pto-controle
    field ini-cod-depos-orig    like dc-movto-pto-controle.cod-depos-orig 
    field ini-cod-localiz-orig  like dc-movto-pto-controle.cod-localiz-orig
    field fim-nr-rack           like dc-movto-pto-controle.nr-rack                   
    field fim-rg-item           like dc-movto-pto-controle.rg-item           
    field fim-it-codigo         like dc-movto-pto-controle.it-codigo         
    field fim-data              like dc-movto-pto-controle.data              
    field fim-cod-pto-controle  like dc-movto-pto-controle.cod-pto-controle  
    field fim-cod-depos-orig    like dc-movto-pto-controle.cod-depos-orig    
    field fim-cod-localiz-orig  like dc-movto-pto-controle.cod-localiz-orig.

define temp-table tt-digita no-undo
    field pto-controle           LIKE dc-pto-controle.cod-pto-controle 
    FIELD desc-pto-controle      LIKE dc-pto-controle.desc-pto-controle
    INDEX ch IS UNIQUE pto-controle.

DEFINE TEMP-TABLE tt-import NO-UNDO
     FIELD rg-item AS CHAR
     INDEX pk-rg-item
           rg-item.

DEFINE TEMP-TABLE tt-raw-digita
     FIELD raw-digita    AS RAW.

DEFINE TEMP-TABLE tt-pto-controle NO-UNDO
     field rg-item              like dc-movto-pto-controle.rg-item         
     field nr-rack              like dc-movto-pto-controle.nr-rack         
     field it-codigo            like dc-movto-pto-controle.it-codigo       
     field desc-item            like ITEM.desc-item                    
     field cod-pto-controle     like dc-movto-pto-controle.cod-pto-controle
     field tipo-pto             like dc-movto-pto-controle.tipo-pto        
     field situacao             AS CHAR                            
     field cod-depos-orig       like dc-movto-pto-controle.cod-depos-orig  
     field cod-localiz-orig     like dc-movto-pto-controle.cod-localiz-orig
     field cod-depos-dest       like dc-movto-pto-controle.cod-depos-dest  
     field cod-localiz-dest     like dc-movto-pto-controle.cod-localiz-dest
     field cod-usuario          like dc-movto-pto-controle.cod-usuario     
     field data                 like dc-movto-pto-controle.data            
     field hora                 like dc-movto-pto-controle.hora            
     field nr-seq               like dc-movto-pto-controle.nr-seq
     FIELD serie                LIKE dc-rg-item.serie
     FIELD nr-nota-fis          LIKE dc-rg-item.nr-nota-fis
     INDEX pk-rg-item
           rg-item.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

for each tt-raw-digita:
    create tt-digita.
    raw-transfer tt-raw-digita.raw-digita to tt-digita.
end.


/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR h-acomp AS HANDLE NO-UNDO.

/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i RG-Item *}
RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).

/* Bloco Principal do Programa ======================================== */

def var c-export as char.

c-export = session:temp-directory + "RG-item-" + string(time) + ".txt".
/* gera CSV */
OUTPUT TO value(c-export) NO-CONVERT.

ASSIGN
tt-param.ini-nr-rack = replace(tt-param.ini-nr-rack, "RC", "")
tt-param.fim-nr-rack = replace(tt-param.fim-nr-rack, "RC", "").


if tt-param.c-dir-importacao = "" then do:
       run pi-por-faixa.
end.
else do:                                  
       run pi-por-arquivo.                
end. /* else do tt-param.c-dir-importacao = "" */       


     /*
    PUT "Rg Item;Rack;Item;Descri��o;Ponto de Controle;"
        "Tipo do Ponto;Situacao;Desposito Original;"
        "Localiza��o Original;Deposito Destino;"
        "Localiza��o Destino;Usuario;Data;Hora;Sequencia" SKIP. */
    
    FOR EACH tt-pto-controle BREAK by tt-pto-controle.rg-item
                                   BY tt-pto-controle.nr-seq:
        
        /*
        PUT
        tt-pto-controle.rg-item           ";"
        tt-pto-controle.nr-rack           ";"
        tt-pto-controle.it-codigo         ";"
        tt-pto-controle.desc-item         ";"
        tt-pto-controle.cod-pto-controle  ";"
        tt-pto-controle.tipo-pto          ";"
        tt-pto-controle.situacao          ";"
        tt-pto-controle.cod-depos-orig    ";"
        tt-pto-controle.cod-localiz-orig  ";"
        tt-pto-controle.cod-depos-dest    ";"
        tt-pto-controle.cod-localiz-dest  ";"
        tt-pto-controle.cod-usuario       ";"
        tt-pto-controle.data              ";"
        tt-pto-controle.hora              ";"
        tt-pto-controle.nr-seq  format ">>>>>>>>>9" ";"
        SKIP.
        */
        
        disp
        tt-pto-controle.rg-item           
        tt-pto-controle.nr-rack           
        tt-pto-controle.it-codigo         
        tt-pto-controle.desc-item         
        tt-pto-controle.cod-pto-controle  
        tt-pto-controle.tipo-pto          
        tt-pto-controle.situacao          
        tt-pto-controle.cod-depos-orig    
        tt-pto-controle.cod-localiz-orig  
        tt-pto-controle.cod-depos-dest    
        tt-pto-controle.cod-localiz-dest  
        tt-pto-controle.cod-usuario       
        STRING(tt-pto-controle.data, "99/99/9999") FORMAT "x(10)" COLUMN-LABEL "Data" "   "             
        /* tt-pto-controle.hora              */
        tt-pto-controle.nr-seq  format ">>>>>>>>>9" 
        tt-pto-controle.serie FORMAT "x(05)"
        tt-pto-controle.nr-nota-fis FORMAT "x(07)"
        with scrollable.

        IF LAST-OF(tt-pto-controle.rg-item) THEN DO:
            PUT SKIP(2).
        END.
    
    END.      
    



OUTPUT CLOSE.
DOS SILENT START /* notepad.exe */ value(c-export).


RUN pi-finalizar IN h-acomp.
RETURN "OK":U.


/* procedures =============================== */

PROCEDURE pi-pto-controle:

DEF INPUT PARAM p-pto-controle AS CHAR.

FOR EACH tt-import where tt-import.rg-item >= tt-param.ini-rg-item 
                     AND tt-import.rg-item <= tt-param.fim-rg-item no-lock:

    FOR EACH dc-movto-pto-controle WHERE dc-movto-pto-controle.rg-item           = tt-import.rg-item
                                     AND dc-movto-pto-controle.nr-rack          >= tt-param.ini-nr-rack           
                                     AND dc-movto-pto-controle.nr-rack          <= tt-param.fim-nr-rack           
                                     AND dc-movto-pto-controle.it-codigo        >= tt-param.ini-it-codigo         
                                     AND dc-movto-pto-controle.it-codigo        <= tt-param.fim-it-codigo         
                                     AND dc-movto-pto-controle.data             >= tt-param.ini-data              
                                     AND dc-movto-pto-controle.data             <= tt-param.fim-data              
                                     AND dc-movto-pto-controle.cod-pto-controle  = p-pto-controle
                                     AND dc-movto-pto-controle.cod-depos-orig   >= tt-param.ini-cod-depos-orig    
                                     AND dc-movto-pto-controle.cod-depos-orig   <= tt-param.fim-cod-depos-orig    
                                     AND dc-movto-pto-controle.cod-localiz-dest >= tt-param.ini-cod-localiz-orig  
                                     AND dc-movto-pto-controle.cod-localiz-dest <= tt-param.fim-cod-localiz-orig  NO-LOCK: 
                                     
        if dc-movto-pto-controle.rg-item = "" then do: next. end.
    
        RUN pi-acompanhar IN h-acomp (INPUT "Rg: " + dc-movto-pto-controle.rg-item + "  Seq: " + string(dc-movto-pto-controle.nr-seq, "99999999999")).
       
        FIND FIRST tt-pto-controle WHERE tt-pto-controle.nr-seq  = dc-movto-pto-controle.nr-seq
                                     AND tt-pto-controle.rg-item = dc-movto-pto-controle.rg-item NO-LOCK NO-ERROR.
        IF NOT AVAIL tt-pto-controle THEN DO:
        
            FIND FIRST ITEM WHERE ITEM.it-codigo = dc-movto-pto-controle.it-codigo NO-LOCK NO-ERROR. 
            if not avail item then do: next. end.         

            FIND dc-rg-item NO-LOCK 
                WHERE dc-rg-item.rg-item = dc-movto-pto-controle.rg-item
                NO-ERROR.
        
             
            CREATE tt-pto-controle.
            ASSIGN tt-pto-controle.rg-item          = dc-movto-pto-controle.rg-item            
                   tt-pto-controle.nr-rack          = dc-movto-pto-controle.nr-rack            
                   tt-pto-controle.it-codigo        = dc-movto-pto-controle.it-codigo          
                   tt-pto-controle.desc-item        = ITEM.desc-item                           
                   tt-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle   
                   tt-pto-controle.tipo-pto         = dc-movto-pto-controle.tipo-pto           
                   tt-pto-controle.situacao         = ""                                      
                   tt-pto-controle.cod-depos-orig   = dc-movto-pto-controle.cod-depos-orig     
                   tt-pto-controle.cod-localiz-orig = dc-movto-pto-controle.cod-localiz-orig   
                   tt-pto-controle.cod-depos-dest   = dc-movto-pto-controle.cod-depos-dest     
                   tt-pto-controle.cod-localiz-dest = dc-movto-pto-controle.cod-localiz-dest   
                   tt-pto-controle.cod-usuario      = dc-movto-pto-controle.cod-usuario        
                   tt-pto-controle.data             = dc-movto-pto-controle.data               
                   tt-pto-controle.hora             = dc-movto-pto-controle.hora               
                   tt-pto-controle.nr-seq           = dc-movto-pto-controle.nr-seq
                   tt-pto-controle.serie            = dc-rg-item.serie
                   tt-pto-controle.nr-nota-fis      = dc-rg-item.nr-nota-fis
                   .

        END. /* not avail tt-pto-controle */
    
    END. /* for each dc-movto-pto-controle */
END. /* tt-import */

END PROCEDURE.


procedure pi-por-faixa:

    FOR EACH dc-movto-pto-controle WHERE dc-movto-pto-controle.rg-item >= tt-param.ini-rg-item    
                                          and dc-movto-pto-controle.rg-item <= tt-param.fim-rg-item 
                                          AND dc-movto-pto-controle.nr-rack >= tt-param.ini-nr-rack           
                                          AND dc-movto-pto-controle.nr-rack <= tt-param.fim-nr-rack
                                          AND dc-movto-pto-controle.data    >= tt-param.ini-data              
                                          AND dc-movto-pto-controle.data    <= tt-param.fim-data NO-LOCK
                                          break by dc-movto-pto-controle.rg-item
                                                by dc-movto-pto-controle.nr-seq: 

                         RUN pi-acompanhar IN h-acomp (INPUT "Rg: " + dc-movto-pto-controle.rg-item + "  Seq: " + string(dc-movto-pto-controle.nr-seq, "99999999999")).
                          
                         find first tt-import where tt-import.rg-item = dc-movto-pto-controle.rg-item no-lock no-error.
                         if not avail tt-import then do:
                             create tt-import.
                             assign tt-import.rg-item = dc-movto-pto-controle.rg-item .
                         end.    
              
          end. /* for each dc-movto-pto-controle */  
          
    FOR EACH tt-digita.
      RUN pi-pto-controle (INPUT tt-digita.pto-controle).      
    end. /* for each tt-digita */          

end. /* procedure pi-por-faixa */

procedure pi-por-arquivo:

   INPUT FROM VALUE(tt-param.c-dir-importacao) no-convert.
    REPEAT:
        create tt-import.
        IMPORT DELIMITER  ";"
    
        tt-import.rg-item.   
    end.
    
    FOR EACH tt-digita.
        RUN pi-pto-controle (INPUT tt-digita.pto-controle).    
    END.  
    
   

end procedure. /* procedure pi-por-aquivo */ 
