/******************************************************************************
*  Programa .....: escd026rp.p                                                *
*  Data .........: 02 de junho de 2008                                        *
*  Cliente ......: Fastplas                                                   *
*  Autor ........: Datasul WA                                                 *
*  Programador ..: Marli Cinira Garcia                                        *
*  Objetivo .....: Exporta��o e Elimina��o de dados das tabelas de Fastplas   *
******************************************************************************/

/* include de controle de vers�o */
{include/i-prgvrs.i ESDC026RP 2.04.00.000}

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    field arq-entrada      as char
    FIELD rg-item-ini      AS CHAR
    FIELD rg-item-fim      AS CHAR.
DEF TEMP-TABLE tt-raw-digita
    FIELD raw-digita   AS RAW.

/* recebimento de par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

{include/i-rpvar.i}

/* defini��o de vari�veis e streams */

define stream arqtabelas.
define stream arqtabelas1.
define stream arqtabelas2.
 
DEF STREAM str-rp.  

DEF VAR h-acomp       AS HANDLE                   NO-UNDO.
DEF VAR i-ct-es121    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es108    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es118    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es120    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es119    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es110    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es123    AS INTEGER  FORMAT ">>>>>9" NO-UNDO. 
DEF VAR i-ct-es116    AS INTEGER  FORMAT ">>>>>9" NO-UNDO. 
DEF VAR i-ct-es114    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.  
DEF VAR i-ct-es105    AS INTEGER  FORMAT ">>>>>9" NO-UNDO. 
DEF VAR i-ct-es107    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es106    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es109    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.
DEF VAR i-ct-es112    AS INTEGER  FORMAT ">>>>>9" NO-UNDO.

/* defini��o de frames do log */

/* include padr�o para output de log */
/* {include/i-rpout.i &STREAM="stream str-rp" &TOFILE=tt-param.arq-destino}  */
{include/i-rpout.i &STREAM="stream str-rp"}  

/* include com a defini��o da frame de cabe�alho e rodap� */
{include/i-rpcab.i &STREAM="str-rp"}

/* bloco principal do programa */
assign	c-programa     = "ESDC026RP"
        c-versao       = "2.04"
        c-revisao      = ".00.000"
        c-empresa      = "Empresa Fastplas"
        c-sistema      = 'Tabelas'
        c-titulo-relat = "Exporta��o e Elimina��o de Dados das Tabelas para Fastplas".

VIEW STREAM str-rp FRAME f-cabec.
VIEW STREAM str-rp FRAME f-rodape.

/* RUN pi-processa. */

/**
*  inicio do procedimento
**/

    RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
    RUN pi-inicializar IN h-acomp (INPUT "Eliminando as tabelas").

/*     DO TRANSACTION ON STOP UNDO, LEAVE     */
/*                      ON ERROR UNDO, LEAVE: */
                                                                           
        ASSIGN i-ct-es121 = 0                                                  
               i-ct-es108 = 0                                                   
               i-ct-es118 = 0                                                        
               i-ct-es120 = 0                                                  
               i-ct-es119 = 0                                                        
               i-ct-es123 = 0                                              
               i-ct-es116 = 0                                                        
               i-ct-es114 = 0                                                      
               i-ct-es105 = 0                                                       
               i-ct-es107 = 0                                                     
               i-ct-es106 = 0                                                        
               i-ct-es109 = 0                                                  
               i-ct-es112 = 0.                                                   
        
        /******************* < Eliminando Tabela dc-movto-pto-controle >*********************/
        
        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "ES121.d").

        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Movimento RG").

        FOR EACH dc-movto-pto-controle USE-INDEX idx_movto_pto_controle  
             WHERE dc-movto-pto-controle.rg-item >= tt-param.rg-item-ini
               AND dc-movto-pto-controle.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-movto-pto-controle.
             
            ASSIGN i-ct-es121 = i-ct-es121 + 1.

            DELETE dc-movto-pto-controle.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-movto-pto-controle: " i-ct-es121.         

        /******************* < Eliminando Tabela dc-rack-itens >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Itens do RACK").
         
        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "ES108.d").

        FOR EACH dc-rack-itens USE-INDEX idx-dc-rg-item
             WHERE dc-rack-itens.rg-item >= tt-param.rg-item-ini
               AND dc-rack-itens.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-rack-itens.
             
            ASSIGN i-ct-es108 = i-ct-es108 + 1.

            DELETE dc-rack-itens.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
         DISP STREAM str-rp
             "Quantidade de Registros Deletados da tabela dc-rack-itens: " i-ct-es108 WITH NO-LABEL. 
            
        /******************* < Eliminando Tabela dc-embarque-item >*********************/

        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Embarques do Item").
         
        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es118.d").

        FOR EACH dc-embarque-item USE-INDEX pk_embarque-item   
             WHERE dc-embarque-item.rg-item >= tt-param.rg-item-ini
               AND dc-embarque-item.rg-item <= tt-param.rg-item-fim 
             BREAK BY dc-embarque-item.nr-embarque 
                   BY dc-embarque-item.nr-rack: 

            IF FIRST-OF(dc-embarque-item.nr-embarque) THEN DO:

               /******************* < Eliminando Tabela dc-embarque >*********************/

               RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Racks do Embarque").

               OUTPUT STREAM arqtabelas1 to value(tt-param.arq-entrada + "es120.d").

               FOR EACH dc-embarque USE-INDEX pk_dc_embarque  
                  WHERE dc-embarque.nr-embarque = dc-embarque-item.nr-embarque:

                  EXPORT STREAM arqtabelas1 DELIMITER ";" dc-embarque.
             
                  ASSIGN i-ct-es120 = i-ct-es120 + 1.

                  DELETE dc-embarque.
               END.

               OUTPUT STREAM arqtabelas1 CLOSE.

               DISP STREAM str-rp 
                    "Quantidade de Registros Deletados da tabela dc-embarque: " i-ct-es120. 
            END.

            IF FIRST-OF(dc-embarque-item.nr-rack) THEN DO:  /* mcg ver */

               /******************* < Eliminando Tabela dc-embarque-rack >*********************/

               RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Embarque").

               OUTPUT STREAM arqtabelas2 to value(tt-param.arq-entrada + "es119.d").

               FOR EACH dc-embarque-rack USE-INDEX pk_dc_embarque_rack   
                  WHERE dc-embarque-rack.nr-embarque = dc-embarque-item.nr-embarque
                    AND dc-embarque-rack.nr-rack = dc-embarque-item.nr-rack: 

                  EXPORT STREAM arqtabelas2 DELIMITER ";" dc-embarque-rack.
             
                  ASSIGN i-ct-es119 = i-ct-es119 + 1.

                  DELETE dc-embarque-rack.
               END.

               OUTPUT STREAM arqtabelas2 CLOSE.

                DISP STREAM str-rp 
                    "Quantidade de Registros Deletados da tabela dc-embarque-rack: " i-ct-es119. 
            END.

            EXPORT STREAM arqtabelas DELIMITER ";" dc-embarque-item.
             
            ASSIGN i-ct-es118 = i-ct-es118 + 1.

         DELETE dc-embarque-item.
            
        END.
        OUTPUT STREAM arqtabelas CLOSE.     
        
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-embarque-item: " i-ct-es118.

        /******************* < Eliminando Tabela dc-reporte >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Reporte").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es110.d").

        FOR EACH dc-reporte USE-INDEX ch-rg-item-ord  
             WHERE dc-reporte.rg-item >= tt-param.rg-item-ini
               AND dc-reporte.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-reporte.
             
            ASSIGN i-ct-es110 = i-ct-es110 + 1.

            DELETE dc-reporte.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-reporte: " i-ct-es110. 

        /******************* < Eliminando Tabela dc-aponta-retrabalho >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Retrabalho").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es123.d").

        FOR EACH dc-aponta-retrabalho  
             WHERE dc-aponta-retrabalho.rg-item >= tt-param.rg-item-ini
               AND dc-aponta-retrabalho.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-aponta-retrabalho.
             
            ASSIGN i-ct-es123 = i-ct-es123 + 1.

            DELETE dc-aponta-retrabalho.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-aponta-retrabalho: " i-ct-es123. 

        /******************* < Eliminando Tabela dc-rg-item >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando RG Item").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es116.d").

        FOR EACH dc-rg-item   USE-INDEX pk_dc-rg-item  
             WHERE dc-rg-item.rg-item >= tt-param.rg-item-ini
               AND dc-rg-item.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-rg-item.
             
            ASSIGN i-ct-es116 = i-ct-es116 + 1.

            DELETE dc-rg-item.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-rg-item: " i-ct-es116. 
       
        /******************* < Eliminando Tabela dc-requisicao >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Requisicao").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es114.d").

        FOR EACH dc-requisicao use-index idx-rg-item
             WHERE dc-requisicao.rg-item >= tt-param.rg-item-ini
               AND dc-requisicao.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-requisicao.
             
            ASSIGN i-ct-es114 = i-ct-es114 + 1.

            DELETE dc-requisicao.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-requisicao: " i-ct-es114. 


        /******************* < Eliminando Tabela dc-aponta >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Apontamento Cor").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es105.d").

        FOR EACH dc-aponta  use-index idx-rg-item
             WHERE dc-aponta.rg-item >= tt-param.rg-item-ini
               AND dc-aponta.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-aponta.
             
            ASSIGN i-ct-es105 = i-ct-es105 + 1.

            DELETE dc-aponta .
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp 
             "Quantidade de Registros Deletados da tabela dc-aponta: " i-ct-es105. 

        /******************* < Eliminando Tabela dc-transfer-rg >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Transferencias").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es107.d").

        FOR EACH dc-transfer-rg USE-INDEX  pk_dc_transfer_rg   
             WHERE dc-transfer-rg.rg-item >= tt-param.rg-item-ini
               AND dc-transfer-rg.rg-item <= tt-param.rg-item-fim 
             BREAK BY dc-transfer-rg.nro-docto:   

            IF FIRST-OF(dc-transfer-rg.nro-docto) THEN DO:

               /******************* < Eliminando Tabela dc-transfer >*********************/

               RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Transferencias").

               OUTPUT STREAM arqtabelas1 to value(tt-param.arq-entrada + "es106.d").

               FIND dc-transfer USE-INDEX pk_dc_transfer  
                  WHERE dc-transfer.nro-docto = dc-transfer-rg.nro-docto NO-ERROR.

               if avail dc-transfer then do:
                  EXPORT STREAM arqtabelas1 DELIMITER ";" dc-transfer.             
                  ASSIGN i-ct-es106 = i-ct-es106 + 1.
                  DELETE dc-transfer.
               end.               
               
               OUTPUT STREAM arqtabelas1 CLOSE.
            
               DISP STREAM str-rp 
                    "Quantidade de Registros Deletados da tabela dc-transfer: " i-ct-es106. 
            END.

            EXPORT STREAM arqtabelas DELIMITER ";" dc-transfer-rg.
             
            ASSIGN i-ct-es107 = i-ct-es107 + 1.
         
         DELETE dc-transfer-rg.
           
        END.

        OUTPUT STREAM arqtabelas CLOSE.

        DISP STREAM str-rp  
             "Quantidade de Registros Deletados da tabela dc-transfer-rg: " i-ct-es107. 

        /******************* < Eliminando Tabela dc-log-rack-itens >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando Log Itens do RACK").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "es109.d").

        FOR EACH dc-log-rack-itens use-index idx-rg-item
             WHERE dc-log-rack-itens.rg-item >= tt-param.rg-item-ini
               AND dc-log-rack-itens.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-log-rack-itens.
             
            ASSIGN i-ct-es109 = i-ct-es109 + 1.

            DELETE dc-log-rack-itens.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp
             "Quantidade de Registros Deletados da tabela dc-log-rack-itens: " i-ct-es109. 

        /******************* < Eliminando Tabela dc-devol-requis >*********************/
    
        RUN pi-acompanhar IN h-acomp (INPUT "Eliminando es112").

        OUTPUT STREAM arqtabelas to value(tt-param.arq-entrada + "Devolucao Requisicao").

        FOR EACH dc-devol-requis  
             WHERE dc-devol-requis.rg-item >= tt-param.rg-item-ini
               AND dc-devol-requis.rg-item <= tt-param.rg-item-fim: 

            EXPORT STREAM arqtabelas DELIMITER ";" dc-devol-requis.
             
            ASSIGN i-ct-es112 = i-ct-es112 + 1.

            DELETE dc-devol-requis.
        END.
             
        OUTPUT STREAM arqtabelas CLOSE.
             
        DISP STREAM str-rp
             "Quantidade de Registros Deletados da tabela dc-devol-requis: " i-ct-es112. 

/*     END. */
    
/*     message                                     */
/*          "Fim da Elimina��o" view-as alert-box. */

/**
*  fim do procedimento
**/    

{include/i-rpclo.i &STREAM="stream str-rp"}

run pi-finalizar in h-acomp.
return "Ok":U.

