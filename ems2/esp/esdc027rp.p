/* include de controle de vers�o */
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i SP0014RP 1.00.00.000}

/* pr�processador para ativar ou n�o a sa�da para RTF */
&GLOBAL-DEFINE RTF YES

/* pr�processador para setar o tamanho da p�gina */
&SCOPED-DEFINE pagesize 42   
/* defini��o das temp-tables para recebimento de par�metros */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    FIELD c-depos          AS CHAR
    FIELD c-item-ini       AS CHAR 
    FIELD c-item-fim       AS CHAR.

DEFINE TEMP-TABLE tt-raw-digita NO-UNDO
    FIELD raw-digita	   AS RAW.

DEF TEMP-TABLE tt-ordem
    FIELD rg-item AS CHAR
    FIELD nr-rack AS CHAR
    FIELD data AS DATE
    FIELD hora AS CHAR
    FIELD it-codigo AS CHAR
    index idx-data data hora.

/* recebimento de par�metros */
DEFINE INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEFINE INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* include padr�o para vari�veis de relat�rio  */
{include/i-rpvar.i}

/* defini��o de vari�veis  */
DEFINE VARIABLE h-acomp   AS HANDLE     NO-UNDO.

DEFINE VARIABLE c-rg-item AS CHARACTER   NO-UNDO.
DEFINE VARIABLE c-nr-rack AS CHARACTER   NO-UNDO.
DEFINE VARIABLE dt-data  AS DATE        NO-UNDO.
DEFINE VARIABLE hr-hora AS CHARACTER   NO-UNDO.

/* defini��o de frames do relat�rio */
define frame f-cabec
"- Item --------- - RG Item ----- - Nr Rack ----- - Data --- - Hora --" skip
with page-top.
     
define frame f-rel 
     tt-ordem.it-codigo FORMAT "x(16)" 
     tt-ordem.rg-item FORMAT "x(15)" 
     tt-ordem.nr-rack FORMAT "x(15)" 
     tt-ordem.data FORMAT "99/99/9999" 
     tt-ordem.hora FORMAT "x(12)" 
    WITH WIDTH 132 stream-io no-label no-box.

/* include padr�o para output de relat�rios */
{include/i-rpout.i &STREAM="stream str-rp"}

/* include com a defini��o da frame de cabe�alho e rodap� */
{include/i-rpcab.i &STREAM="str-rp"}


/* bloco principal do programa */
ASSIGN   c-programa        = "ESDC027"
    	  c-versao	      = "2.04"
    	  c-revisao	      = ".00.002"
    	  c-empresa         = "Seeber Fastplas"
    	  c-sistema	      = "DCP"
    	  c-titulo-relat    = "Listagem Pe�as no " + tt-param.c-depos.


/* para n�o visualizar cabe�alho/rodap� em sa�da RTF */
IF tt-param.l-habilitaRTF <> YES THEN DO:
    VIEW STREAM str-rp FRAME f-cabec.
    VIEW STREAM str-rp FRAME f-rodape.
END.

/* executando de forma persistente o utilit�rio de acompanhamento */
RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}
RUN pi-inicializar IN h-acomp (INPUT RETURN-VALUE).

/* corpo do relat�rio */
FOR each dc-rg-item use-index idx-item-local
    WHERE dc-rg-item.it-codigo >= tt-param.c-item-ini
      AND dc-rg-item.it-codigo <= tt-param.c-item-fim
      AND dc-rg-item.cod-depos-orig = tt-param.c-depos
   /* AND dc-rg-item.cod-localiz-orig = "" */
      AND (dc-rg-item.tipo-pto = 1 OR
           dc-rg-item.tipo-pto = 2 OR      
           dc-rg-item.tipo-pto = 3)      
      AND dc-rg-item.situacao = 2
      NO-LOCK.
    
    assign c-rg-item = ""
           c-nr-rack = ""
           dt-data = ?
           hr-hora = "".
           
    for each dc-movto-pto-controle
        where dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
          and dc-movto-pto-controle.cod-depos-dest = dc-rg-item.cod-depos-orig
          and dc-movto-pto-controle.cod-localiz-dest = dc-rg-item.cod-localiz-orig
        no-lock.

        if dt-data = ? then
            assign dt-data = dc-movto-pto-controle.data
                   hr-hora = dc-movto-pto-controle.hora
                   c-rg-item = dc-rg-item.rg-item
                   c-nr-rack = dc-rg-item.nr-rack.

        if dt-data >= dc-movto-pto-controle.data THEN DO:
            IF dt-data = dc-movto-pto-controle.data THEN DO:
                IF hr-hora > dc-movto-pto-controle.hora THEN
                    assign dt-data = dc-movto-pto-controle.data
                           hr-hora = dc-movto-pto-controle.hora
                           c-rg-item = dc-rg-item.rg-item
                           c-nr-rack = dc-rg-item.nr-rack.
            END.
            ELSE
                assign dt-data = dc-movto-pto-controle.data
                       hr-hora = dc-movto-pto-controle.hora
                       c-rg-item = dc-rg-item.rg-item
                       c-nr-rack = dc-rg-item.nr-rack.
        END.
    end.
    
    
    
    if c-rg-item <> "" then do:
        run pi-acompanhar in h-acomp (input dc-rg-item.rg-item).
        CREATE tt-ordem.
        ASSIGN tt-ordem.rg-item = c-rg-item
               tt-ordem.nr-rack = IF c-nr-rack <> "" THEN  "RC" + c-nr-rack ELSE c-nr-rack 
               tt-ordem.data = dt-data
               tt-ordem.hora = hr-hora
               tt-ordem.it-codigo = dc-rg-item.it-codigo.
                       
    END.
end.

FOR EACH tt-ordem no-lock
    BY tt-ordem.it-codigo
    BY tt-ordem.data
    BY tt-ordem.hora.

    disp stream str-rp 
        tt-ordem.it-codigo 
         tt-ordem.rg-item 
         tt-ordem.nr-rack 
         tt-ordem.data 
         tt-ordem.hora 
        WITH FRAME f-rel .

END.



/*fechamento do output do relat�rio*/
{include/i-rpclo.i &STREAM="stream str-rp"}
RUN pi-finalizar IN h-acomp.
RETURN "OK":U.
