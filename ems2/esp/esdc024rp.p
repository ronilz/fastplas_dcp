/*******************************************************************************
** Programa...: ES0004RP
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..: 
*******************************************************************************/
/* Include de Controle de Vers�o */
{include/i-prgvrs.i ESDC024RP.P 1.00.00.000}
define buffer empresa     for mgcad.empresa.
define buffer localizacao for mgcad.localizacao.
DEFINE BUFFER estabelec              FOR mgadm.estabelec.
DEFINE BUFFER cidade                 FOR mgdis.cidade.   
DEFINE BUFFER pais                   FOR mguni.pais. 
define buffer unid-feder             for mgcad.unid-feder. 


/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD c-ini-rg         AS CHAR
    FIELD c-fim-rg         AS CHAR
    FIELD c-ini-it         AS CHAR
    FIELD c-fim-it         AS CHAR
    FIELD c-nr-rack-ini    AS CHAR
    FIELD c-nr-rack-fim    AS CHAR
    FIELD dt-per-ini       AS DATE
    FIELD dt-per-fim       AS DATE
    FIELD c-pto-ini        AS CHAR
    FIELD c-pto-fim        AS CHAR
    FIELD c-depos-ini      AS CHAR
    FIELD c-depos-fim      AS CHAR
    FIELD c-localiz-ini    AS CHAR
    FIELD c-localiz-fim    AS CHAR
    .

DEFINE TEMP-TABLE tt-raw-digita
    FIELD raw-digita    AS RAW.

DEFINE TEMP-TABLE tt-item-esp NO-UNDO LIKE ITEM.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR c-tipo  AS CHAR.
DEF VAR c-situacao  AS CHAR.
DEF VAR h-acomp AS HANDLE NO-UNDO.

/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ES0007RP"
       c-versao       = "1.00"
       c-revisao      = "1.00.000"
       c-empresa      = empresa.nome
       c-sistema      = "ESP"
       c-titulo-relat = c-titulo-relat.

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).
PUT "                                                                   --------------- Movimentos ---------------                                               " SKIP
    "RG Item       It. c�digo        Descri��o do item               Pto Contr  Dep Orig  Dep Dest Rack          Sit  Dep Atu  Usuario      Data       Hora      " SKIP
    "------------- ----------------  ------------------------------  ---------  --------  -------- ------------- ---- -------- ------------ ---------- ----------" SKIP.
/*   123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456 */
/*                1         2         3         4         5         6         7         8         9         0         1         2         3         4         5       */

FOR EACH dc-movto-pto-controle
    WHERE dc-movto-pto-controle.rg-item >= tt-param.c-ini-rg
      AND dc-movto-pto-controle.rg-item <= tt-param.c-fim-rg
      AND dc-movto-pto-controle.it-codigo >= tt-param.c-ini-it
      AND dc-movto-pto-controle.it-codigo <= tt-param.c-fim-it 
      AND dc-movto-pto-controle.nr-rack   >= substring(tt-param.c-nr-rack-ini,3,11)
      AND dc-movto-pto-controle.nr-rack   <= substring(tt-param.c-nr-rack-fim,3,11)
      AND dc-movto-pto-controle.cod-pto-controle >= tt-param.c-pto-ini
      AND dc-movto-pto-controle.cod-pto-controle <= tt-param.c-pto-fim
      AND dc-movto-pto-controle.cod-depos-orig >= tt-param.c-depos-ini
      AND dc-movto-pto-controle.cod-depos-dest <= tt-param.c-depos-fim
      AND dc-movto-pto-controle.cod-localiz-orig >= tt-param.c-localiz-ini
      AND dc-movto-pto-controle.cod-localiz-dest <= tt-param.c-localiz-fim
      AND dc-movto-pto-controle.data >= tt-param.dt-per-ini
      AND dc-movto-pto-controle.data <= tt-param.dt-per-fim
    NO-LOCK,
    FIRST dc-rg-item
    WHERE dc-rg-item.rg-item = dc-movto-pto-controle.rg-item
    NO-LOCK
    BREAK BY dc-movto-pto-controle.rg-item
          BY dc-movto-pto-controle.nr-seq 
          BY dc-movto-pto-controle.data
          BY dc-movto-pto-controle.hora.

    RUN pi-acompanhar IN h-acomp (INPUT dc-rg-item.rg-item).
    IF LAST-OF(dc-movto-pto-controle.rg-item) THEN DO:
    
        CASE dc-rg-item.tipo-pto.
            WHEN 1 THEN
                ASSIGN c-tipo = "Normal".
            WHEN 2 THEN
                ASSIGN c-tipo = "Jun��o".
            WHEN 3 THEN
                ASSIGN c-tipo = "Pintura".
            WHEN 4 THEN
                ASSIGN c-tipo = "Re-trabalho".
            WHEN 5 THEN
                ASSIGN c-tipo = "Expedi��o".
            WHEN 6 THEN
                ASSIGN c-tipo = "Sucata".
            WHEN 7 THEN
                ASSIGN c-tipo = "Impress�o".
        END CASE.
    
        CASE dc-rg-item.situacao.
            WHEN 1 THEN
                ASSIGN c-situacao = "Ger".
            WHEN 2 THEN
                ASSIGN c-situacao = "Ati".
            WHEN 3 THEN
                ASSIGN c-situacao = "Sus".
            WHEN 4 THEN
                ASSIGN c-situacao = "Can".
            WHEN 5 THEN
                ASSIGN c-situacao = "Jun".
            WHEN 6 THEN
                ASSIGN c-situacao = "Fat".
        END CASE.
                                              
        FIND ITEM WHERE ITEM.it-codigo = dc-movto-pto-controle.it-codigo NO-LOCK NO-ERROR.
            IF NOT AVAIL ITEM THEN
                NEXT.
    
        FIND dc-pto-controle
            WHERE dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle
            NO-LOCK NO-ERROR.
    
        PUT UNFORMATTED 
            dc-rg-item.rg-item AT 01
            dc-movto-pto-controle.it-codigo AT 15
            ITEM.desc-item FORMAT "X(30)" AT 33
            dc-movto-pto-controle.cod-pto-controle AT 65
            dc-movto-pto-controle.cod-depos-orig AT 76
            dc-movto-pto-controle.cod-depos-dest AT 86
            dc-movto-pto-controle.nr-rack AT 95
            c-situacao AT 109
            dc-rg-item.cod-depos-orig AT 114
            dc-movto-pto-controle.cod-usuario AT 123
            dc-movto-pto-controle.data FORMAT "99/99/9999" AT 136
            dc-movto-pto-controle.hora AT 147
            SKIP.
    END.
END.

RUN pi-finalizar IN h-acomp.
RETURN "OK":U.
