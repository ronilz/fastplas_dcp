&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME w-livre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-livre 
/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 

{include/i-prgvrs.i ETQ005 2.06.00.001}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEF TEMP-TABLE tt-pergunta NO-UNDO
    FIELD campo-etiqueta LIKE es-etq-layout.campo-etiqueta 
    FIELD desc-tipo-dado LIKE es-etq-tipo-dado.desc-tipo-dado
    FIELD c-valor        AS CHAR FORM 'x(60)'
    FIELD valida-data    AS INT
    FIELD fixo           AS LOG
    field cod-tipo-dado  like es-etq-tipo-dado.cod-tipo-dado.

DEF TEMP-TABLE tt-troca NO-UNDO LIKE tt-pergunta.

DEFINE VARIABLE hb-item         AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-estabelec    AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-item-mat     AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-item-fornec  AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-item-cli     AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-es-etq-item  AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-usuar        AS WIDGET-HANDLE    NO-UNDO.
DEFINE VARIABLE hb-mensagem     AS WIDGET-HANDLE    NO-UNDO.

DEFINE VARIABLE hb-es-emb-glt-klt AS WIDGET-HANDLE    NO-UNDO.

DEF STREAM str.

DEF VAR c-list      AS CHAR NO-UNDO.
DEF VAR c-list-etiq AS CHAR NO-UNDO.

DEF VAR c-aux AS CHAR EXTENT 2 NO-UNDO.
DEF VAR i-today AS INT NO-UNDO.

DEF VAR l-impres-user AS LOG INIT NO NO-UNDO.

DEF BUFFER b-tt-pergunta FOR tt-pergunta.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE w-livre
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-cad
&Scoped-define BROWSE-NAME br-fixo

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-pergunta tt-troca

/* Definitions for BROWSE br-fixo                                       */
&Scoped-define FIELDS-IN-QUERY-br-fixo tt-pergunta.desc-tipo-dado tt-pergunta.c-valor   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-fixo   
&Scoped-define SELF-NAME br-fixo
&Scoped-define QUERY-STRING-br-fixo FOR EACH tt-pergunta WHERE tt-pergunta.fixo = YES
&Scoped-define OPEN-QUERY-br-fixo OPEN QUERY {&SELF-NAME} FOR EACH tt-pergunta WHERE tt-pergunta.fixo = YES.
&Scoped-define TABLES-IN-QUERY-br-fixo tt-pergunta
&Scoped-define FIRST-TABLE-IN-QUERY-br-fixo tt-pergunta


/* Definitions for BROWSE br-pergunta                                   */
&Scoped-define FIELDS-IN-QUERY-br-pergunta tt-pergunta.desc-tipo-dado tt-pergunta.c-valor   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-pergunta tt-pergunta.c-valor   
&Scoped-define ENABLED-TABLES-IN-QUERY-br-pergunta tt-pergunta
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-pergunta tt-pergunta
&Scoped-define SELF-NAME br-pergunta
&Scoped-define QUERY-STRING-br-pergunta FOR EACH tt-pergunta WHERE tt-pergunta.fixo = NO
&Scoped-define OPEN-QUERY-br-pergunta OPEN QUERY {&SELF-NAME} FOR EACH tt-pergunta WHERE tt-pergunta.fixo = NO.
&Scoped-define TABLES-IN-QUERY-br-pergunta tt-pergunta
&Scoped-define FIRST-TABLE-IN-QUERY-br-pergunta tt-pergunta


/* Definitions for BROWSE br-troca                                      */
&Scoped-define FIELDS-IN-QUERY-br-troca tt-troca.desc-tipo-dado tt-troca.c-valor   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-troca   
&Scoped-define SELF-NAME br-troca
&Scoped-define QUERY-STRING-br-troca FOR EACH tt-troca
&Scoped-define OPEN-QUERY-br-troca OPEN QUERY {&SELF-NAME} FOR EACH tt-troca.
&Scoped-define TABLES-IN-QUERY-br-troca tt-troca
&Scoped-define FIRST-TABLE-IN-QUERY-br-troca tt-troca


/* Definitions for FRAME f-cad                                          */
&Scoped-define OPEN-BROWSERS-IN-QUERY-f-cad ~
    ~{&OPEN-QUERY-br-fixo}~
    ~{&OPEN-QUERY-br-pergunta}~
    ~{&OPEN-QUERY-br-troca}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cb-impressora i-num c-it-codigo cb-etiqueta ~
br-pergunta bt-impressao br-troca rt-button RECT-1 br-fixo 
&Scoped-Define DISPLAYED-OBJECTS cb-impressora i-num c-it-codigo ~
cb-etiqueta c-desc-item c-desc-etiqueta 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-livre AS WIDGET-HANDLE NO-UNDO.

/* Menu Definitions                                                     */
DEFINE SUB-MENU mi-programa 
       MENU-ITEM mi-consultas   LABEL "Co&nsultas"     ACCELERATOR "CTRL-L"
       MENU-ITEM mi-imprimir    LABEL "&Relat�rios"    ACCELERATOR "CTRL-P"
       RULE
       MENU-ITEM mi-sair        LABEL "&Sair"          ACCELERATOR "CTRL-X".

DEFINE SUB-MENU m_Ajuda 
       MENU-ITEM mi-conteudo    LABEL "&Conteudo"     
       MENU-ITEM mi-sobre       LABEL "&Sobre..."     .

DEFINE MENU m-livre MENUBAR
       SUB-MENU  mi-programa    LABEL "&Nome-do-Programa"
       SUB-MENU  m_Ajuda        LABEL "&Ajuda"        .


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_p-exihel AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-impressao 
     LABEL "Impress�o" 
     SIZE 15 BY 1.13.

DEFINE VARIABLE cb-etiqueta AS CHARACTER FORMAT "x(20)" 
     LABEL "Etiqueta" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 22 BY 1.

DEFINE VARIABLE cb-impressora AS CHARACTER FORMAT "x(20)" 
     LABEL "Impressora" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 22 BY 1.

DEFINE VARIABLE c-desc-etiqueta AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 45.14 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-item AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 51 BY .88 NO-UNDO.

DEFINE VARIABLE c-it-codigo AS CHARACTER FORMAT "X(16)":U 
     LABEL "Item" 
     VIEW-AS FILL-IN 
     SIZE 16 BY .88 NO-UNDO.

DEFINE VARIABLE i-num AS INTEGER FORMAT ">>9":U INITIAL 1 
     LABEL "N�m. C�pias Etiquetas" 
     VIEW-AS FILL-IN 
     SIZE 6 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 89.72 BY 17.75.

DEFINE RECTANGLE rt-button
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 89.72 BY 1.46
     BGCOLOR 7 .

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-fixo FOR 
      tt-pergunta SCROLLING.

DEFINE QUERY br-pergunta FOR 
      tt-pergunta SCROLLING.

DEFINE QUERY br-troca FOR 
      tt-troca SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-fixo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-fixo w-livre _FREEFORM
  QUERY br-fixo DISPLAY
      tt-pergunta.desc-tipo-dado  COLUMN-LABEL 'Campo' WIDTH 35
      tt-pergunta.c-valor         COLUMN-LABEL 'Valor'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 70 BY 4.29
         TITLE "Dados Fixos" FIT-LAST-COLUMN.

DEFINE BROWSE br-pergunta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-pergunta w-livre _FREEFORM
  QUERY br-pergunta DISPLAY
      tt-pergunta.desc-tipo-dado  COLUMN-LABEL 'Campo' WIDTH 35
      tt-pergunta.c-valor         COLUMN-LABEL 'Valor'
      ENABLE
      tt-pergunta.c-valor
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 70 BY 5.08
         TITLE "Pergunta" FIT-LAST-COLUMN.

DEFINE BROWSE br-troca
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-troca w-livre _FREEFORM
  QUERY br-troca DISPLAY
      tt-troca.desc-tipo-dado  COLUMN-LABEL 'Campo' WIDTH 35
      tt-troca.c-valor         COLUMN-LABEL 'Valor'
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 70 BY 4.29
         TITLE "Dados Sistema" FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-cad
     cb-impressora AT ROW 3 COL 10.72 COLON-ALIGNED WIDGET-ID 14
     i-num AT ROW 3 COL 72.29 COLON-ALIGNED WIDGET-ID 20
     c-it-codigo AT ROW 4.13 COL 10.72 COLON-ALIGNED WIDGET-ID 2
     cb-etiqueta AT ROW 5.13 COL 10.72 COLON-ALIGNED WIDGET-ID 16
     br-pergunta AT ROW 6.21 COL 12 WIDGET-ID 200
     bt-impressao AT ROW 1.17 COL 45 WIDGET-ID 12
     c-desc-item AT ROW 4.13 COL 27.29 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     c-desc-etiqueta AT ROW 5.17 COL 33.29 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     br-troca AT ROW 15.92 COL 12 WIDGET-ID 300
     br-fixo AT ROW 11.46 COL 12 WIDGET-ID 400
     rt-button AT ROW 1 COL 1
     RECT-1 AT ROW 2.67 COL 1 WIDGET-ID 4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 90 BY 19.42 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: w-livre
   Allow: Basic,Browse,DB-Fields,Smart,Window,Query
   Container Links: 
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-livre ASSIGN
         HIDDEN             = YES
         TITLE              = "Template Livre <Insira complemento>"
         HEIGHT             = 20.08
         WIDTH              = 90
         MAX-HEIGHT         = 20.08
         MAX-WIDTH          = 90
         VIRTUAL-HEIGHT     = 20.08
         VIRTUAL-WIDTH      = 90
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = yes
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

ASSIGN {&WINDOW-NAME}:MENUBAR    = MENU m-livre:HANDLE.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-livre 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-livre.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-livre
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-cad
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB br-pergunta cb-etiqueta f-cad */
/* BROWSE-TAB br-troca c-desc-etiqueta f-cad */
/* BROWSE-TAB br-fixo RECT-1 f-cad */
/* SETTINGS FOR FILL-IN c-desc-etiqueta IN FRAME f-cad
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-item IN FRAME f-cad
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-livre)
THEN w-livre:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-fixo
/* Query rebuild information for BROWSE br-fixo
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-pergunta WHERE tt-pergunta.fixo = YES.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-fixo */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-pergunta
/* Query rebuild information for BROWSE br-pergunta
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-pergunta WHERE tt-pergunta.fixo = NO.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-pergunta */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-troca
/* Query rebuild information for BROWSE br-troca
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-troca.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-troca */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-livre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-livre w-livre
ON END-ERROR OF w-livre /* Template Livre <Insira complemento> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-livre w-livre
ON WINDOW-CLOSE OF w-livre /* Template Livre <Insira complemento> */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-pergunta
&Scoped-define SELF-NAME br-pergunta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-pergunta w-livre
ON ROW-LEAVE OF br-pergunta IN FRAME f-cad /* Pergunta */
DO:
  
    DEF VAR d-dt-atual AS DATE NO-UNDO.
    DEF VAR i-dif-data AS INT  NO-UNDO.
    DEF VAR r-row      AS ROWID NO-UNDO.

    IF tt-pergunta.desc-tipo-dado = 'data' AND
       tt-pergunta.c-valor <> tt-pergunta.c-valor:SCREEN-VALUE IN BROWSE br-pergunta THEN DO:

       ASSIGN i-dif-data = DATE(tt-pergunta.c-valor) - DATE(tt-pergunta.c-valor:SCREEN-VALUE IN BROWSE br-pergunta)
              tt-pergunta.c-valor = tt-pergunta.c-valor:SCREEN-VALUE IN BROWSE br-pergunta
              d-dt-atual = DATE(tt-pergunta.c-valor:SCREEN-VALUE IN BROWSE br-pergunta) 
              r-row = ROWID(tt-pergunta) NO-ERROR.

       IF d-dt-atual <> ? THEN
       FOR EACH b-tt-pergunta.
           IF b-tt-pergunta.desc-tipo-dado = 'fifo' THEN
              ASSIGN b-tt-pergunta.c-valor = STRING(d-dt-atual - DATE('01/01/' + STRING(YEAR(d-dt-atual),'9999')) + 1) NO-ERROR.
           IF b-tt-pergunta.desc-tipo-dado = 'ano' THEN
              ASSIGN b-tt-pergunta.c-valor = STRING(YEAR(d-dt-atual),'9999') NO-ERROR.
           IF b-tt-pergunta.desc-tipo-dado = 'Data Primer' THEN
              ASSIGN b-tt-pergunta.c-valor = STRING(DATE(b-tt-pergunta.c-valor) - i-dif-data,'99/99/99') NO-ERROR.
           IF b-tt-pergunta.desc-tipo-dado = 'Data Usa' THEN
              ASSIGN b-tt-pergunta.c-valor = STRING(YEAR(d-dt-atual),'9999') + STRING(MONTH(d-dt-atual),'99') + STRING(DAY(d-dt-atual),'99') NO-ERROR.
       END.

       {&OPEN-QUERY-br-fixo}

       REPOSITION br-pergunta TO ROWID r-row.

    END.
        
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-impressao
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-impressao w-livre
ON CHOOSE OF bt-impressao IN FRAME f-cad /* Impress�o */
DO:
  
    DEF VAR c-arq   AS CHAR NO-UNDO.
    DEF VAR c-linha AS CHAR NO-UNDO.
    DEF VAR i-seq   AS INT NO-UNDO.
    DEF VAR i-cont  AS INT NO-UNDO.
    DEF VAR c-pri-linha AS CHAR INIT 'CT~~~~CD,~~CC^~~CT~~^XA~~TA000~~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR4,4~~SD15^JUS^LRN^CI0^XZ' NO-UNDO.
    def var iconta2 as int.
    def var c-valor2 as char format 'x(30)'.
    def var lseq as log.
    DEFINE VARIABLE cRack AS CHARACTER NO-UNDO.

    DEF VAR dt-valida-data AS DATE NO-UNDO.
    DEF VAR i-valida AS INT NO-UNDO.

    FOR EACH tt-pergunta WHERE
        tt-pergunta.valida-data > 0 NO-LOCK.
  
        IF tt-pergunta.valida-data = 1 THEN DO:
            ASSIGN dt-valida-data = ?.
            ASSIGN dt-valida-data = DATE(tt-pergunta.c-valor) NO-ERROR.
  
            IF dt-valida-data = ? THEN DO:
               MESSAGE 'Campo de data ' tt-pergunta.desc-tipo-dado ' com formato inv�lido'
                   VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
               RETURN NO-APPLY.
            END.
        END.
        IF tt-pergunta.valida-data = 2 THEN DO:
            ASSIGN i-valida = 0.
            ASSIGN i-valida = INT(tt-pergunta.c-valor) NO-ERROR.
  
            IF i-valida = 0 THEN DO:
               MESSAGE 'Campo numerico ' tt-pergunta.desc-tipo-dado ' nao pode ser zero'
                   VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  
               RETURN NO-APPLY.
            END.
        END.
    END.

    FIND es-etq-etiqueta WHERE 
         es-etq-etiqueta.cod-etiqueta = INPUT FRAME {&FRAME-NAME} cb-etiqueta NO-LOCK NO-ERROR.
    IF NOT AVAIL es-etq-etiqueta THEN RETURN.
    
    ASSIGN c-arq = SESSION:TEMP-DIR + 'etq_' + c-seg-usuario + '_' + STRING(TIME) + '.txt'.

    FIND es-etq-impressora WHERE
         es-etq-impressora.cod-impressora = INPUT FRAME {&FRAME-NAME} cb-impressora NO-LOCK NO-ERROR.

    IF AVAIL es-etq-impressora AND 
       es-etq-impressora.end-impressora = 'file' THEN
       ASSIGN c-arq = 'u:\etq_' + c-seg-usuario + '_' + STRING(TIME) + '.txt'.

    OUTPUT STREAM str TO VALUE(c-arq) NO-CONVERT.

    DO i-cont = 1 TO INPUT FRAME {&FRAME-NAME} i-num:

        INPUT FROM VALUE(es-etq-etiqueta.arq-layout) NO-CONVERT.

        ASSIGN i-seq = 0.
        assign lseq = false.
    
/* SMS */
/* CRIAR UMA VARIAVEL LOGICA, PARA toda vez colocar NO, para no sequenciador somar ou nao, caso passou 
   a segunda vez - esta sequenciando mais de 40 entre as etiquetas */    
    
        REPEAT:
            ASSIGN i-seq = i-seq + 1.
    
            IMPORT UNFORMAT c-linha.

            /*IF i-seq = 1 THEN
               PUT STREAM str UNFORMAT
                   SUBSTRING(c-pri-linha,1,16) SKIP
                   SUBSTRING(c-pri-linha,17,70) SKIP.
            ELSE DO:*/
               FOR EACH tt-troca.

                   if tt-troca.cod-tipo-dado = 'COD COMPL MBB' then do:
                      assign c-valor2 = tt-troca.c-valor.
                      do iconta2 = (length(c-valor2) + 1) to 21:
                          c-valor2 = c-valor2 + ' '.
                      end.
                      c-linha = REPLACE(c-linha,tt-troca.campo-etiqueta, c-valor2 ).
                   end. /* if tt-pergunta.campo-etiqueta = 'c-cod-compl' then do: */
                   else
                   IF tt-troca.cod-tipo-dado = 'SEQUENCIA MBB' then do: /* if tt-pergunta.campo-etiqueta = 'i-sequencia' then */
                        
                        c-linha = REPLACE(c-linha,tt-troca.campo-etiqueta, string(dec(tt-troca.c-valor),'999999999999')).

/* SMS */
                         for last es-num-chamado-MB.
                            if not lseq then
                                assign es-num-chamado-MB.nr-seq-MB = es-num-chamado-MB.nr-seq-MB + 1.
                            assign tt-troca.c-valor = string(es-num-chamado-MB.nr-seq-MB, '999999999') no-error.
                            assign lseq = true.
                         end. /* for last es-num-chamado-MB. */
                         RELEASE es-num-chamado-mb.
                         FOR LAST es-num-chamado-mb NO-LOCK. END.
                   end.
                   else
                   IF tt-troca.cod-tipo-dado = 'RACK' or
                      tt-troca.cod-tipo-dado = 'RACK SEQ2'  THEN DO: /* if tt-pergunta.campo-etiqueta = 'i-sequencia' then */
                    
                         FOR LAST es-etq-seq.
                         
                            IF NOT lseq THEN
                            DO:
                                
                                cRack = IF tt-troca.cod-tipo-dado = 'RACK' THEN STRING (dec(es-etq-seq.dc-seq-1) + 1, "99999999999")
                                        ELSE STRING (dec(es-etq-seq.dc-seq-2) + 1, "99999999999").
                                
                                FOR FIRST dc-rack no-lock
                                    WHERE dc-rack.nr-rack = cRack.
                                END.
                                IF NOT AVAILABLE dc-rack THEN 
                                DO:
                                    CREATE dc-rack.
                                    ASSIGN 
                                        dc-rack.nr-rack = cRack
                                        dc-rack.cod-pto-controle = ''
                                        dc-rack.data = today
                                        dc-rack.desc-rack = "Rack criado automaticamente pelo ETQ005"
                                        dc-rack.hora = string(time, "hh:mm:ss")
                                        dc-rack.situacao = 1
                                        dc-rack.tipo-pto = 1
                                        dc-rack.usuario = c-seg-usuario.
                               END.
                               
                               IF tt-troca.cod-tipo-dado = 'RACK' THEN
                                es-etq-seq.dc-seq-1 = cRack.
                               ELSE
                                es-etq-seq.dc-seq-2 = cRack.
                                
                            END.
                                
                            tt-troca.c-valor =  IF tt-troca.cod-tipo-dado = 'RACK' THEN 
                                                    STRING(es-etq-seq.dc-seq-1, '99999999999') 
                                                ELSE 
                                                    STRING(es-etq-seq.dc-seq-2, '99999999999') 
                                                NO-ERROR.
                                                
                            lseq = TRUE.
                            
                         END. /* for last es-num-chamado-MB. */
                         
                         RELEASE es-etq-seq.
                         FOR LAST es-etq-seq NO-LOCK. END.
                         
                         c-linha = REPLACE(c-linha, tt-troca.campo-etiqueta, STRING(dec(tt-troca.c-valor),'99999999999')).
                         
                   END.
                   else
                   IF tt-troca.cod-tipo-dado = 'RACK SEQ3' or
                      tt-troca.cod-tipo-dado = 'RACK SEQ4' THEN DO: /* if tt-pergunta.campo-etiqueta = 'i-sequencia' then */
                    
                         FOR LAST es-etq-seq.
                         
                            IF NOT lseq THEN
                            DO:
                                
                               IF tt-troca.cod-tipo-dado = 'RACK SEQ3' THEN
                                es-etq-seq.dc-seq-1 = STRING (dec(es-etq-seq.dc-seq-3) + 1, "99999999999").
                               ELSE
                                es-etq-seq.dc-seq-2 = STRING (dec(es-etq-seq.dc-seq-4) + 1, "99999999999").
                                
                            END.
                                
                            tt-troca.c-valor =  IF tt-troca.cod-tipo-dado = 'RACK SEQ3' THEN 
                                                    STRING(es-etq-seq.dc-seq-3, '99999999999') 
                                                ELSE 
                                                    STRING(es-etq-seq.dc-seq-4, '99999999999') 
                                                NO-ERROR.
                                                
                            lseq = TRUE.
                            
                         END. /* for last es-num-chamado-MB. */
                         
                         RELEASE es-etq-seq.
                         FOR LAST es-etq-seq NO-LOCK. END.
                         
                         c-linha = REPLACE(c-linha, tt-troca.campo-etiqueta, STRING(dec(tt-troca.c-valor),'99999999999')).
                         
                   END.
                   else
                       c-linha = REPLACE(c-linha,tt-troca.campo-etiqueta,tt-troca.c-valor).
               END. /* FOR EACH tt-troca. */

               FOR EACH tt-pergunta.
/*                    if INPUT FRAME {&FRAME-NAME} cb-etiqueta begins 'MBB-' then do: */

                      /* dt-usa i-sequencia 01 c-cod-compl da-qtd 000 d-ini da-fim */
                      IF tt-pergunta.cod-tipo-dado = 'PERGUNTA DATA USA MB' THEN /* if tt-pergunta.campo-etiqueta = 'dt-usa' then */
                         c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta, substr(tt-pergunta.c-valor,3,6)).
                      else
/*                       IF tt-pergunta.cod-tipo-dado = 'SEQUENCIA MBB' then /* if tt-pergunta.campo-etiqueta = 'i-sequencia' then */ */
/*                          c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta, string(dec(tt-pergunta.c-valor),'999999999999')). */
/*                       else */
                      IF tt-pergunta.cod-tipo-dado = 'PERGUNTA NUM MBB' THEN do: /* if tt-pergunta.campo-etiqueta = 'dz-qtdpcs' then */
                         if c-linha matches('*^FDdz-qtdpcs^FS') then /* GLT E KLT - somente quantidade Acima, a direita da etiqueta */
                            c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta, trim(string( int(tt-pergunta.c-valor),'>>>>9') )).
                         else c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta, string( dec(tt-pergunta.c-valor),'99999999999') ).
                      end.
                      else
                      IF tt-pergunta.cod-tipo-dado = 'PERGUNTA NUM2 MBB' THEN /* if tt-pergunta.campo-etiqueta = 'd-ini' then */
                         c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta, string( dec(substr(tt-pergunta.c-valor,1,2)),'99') ).
/*                       else */
/*                       if tt-pergunta.campo-etiqueta = 'da-fim' then */
/*                          c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta, string( dec(substr(tt-pergunta.c-valor,1,2)),'99') ). */
                      else do:
                          c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta,tt-pergunta.c-valor).
                      end.
/*                    end. */
/*                    else c-linha = REPLACE(c-linha,tt-pergunta.campo-etiqueta,tt-pergunta.c-valor). */

/* SMS */
                      IF tt-pergunta.cod-tipo-dado = 'SEQUENCIA MBB' then do:
  
                        for last es-num-chamado-MB.
                           assign es-num-chamado-MB.nr-seq-MB = es-num-chamado-MB.nr-seq-MB + 1.
                           assign tt-pergunta.c-valor = string(es-num-chamado-MB.nr-seq-MB, '999999999') no-error.
                        end. /* for last es-num-chamado-MB. */
                        RELEASE es-num-chamado-MB.
                        for last es-num-chamado-MB NO-LOCK. END.
  
                      end.
                      

               END.
               PUT STREAM str UNFORMAT c-linha SKIP.
            /*END.*/
    
        END.

    END. /* DO i-cont = 1 TO INPUT FRAME {&FRAME-NAME} i-num: */

    OUTPUT STREAM str CLOSE.

    FIND es-etq-impressora WHERE
         es-etq-impressora.cod-impressora = INPUT FRAME {&FRAME-NAME} cb-impressora NO-LOCK NO-ERROR.
    IF AVAIL es-etq-impressora AND 
       es-etq-impressora.end-impressora <> 'file' THEN DO:
       OS-COMMAND SILENT VALUE("TYPE " + c-arq + " > " + es-etq-impressora.end-impressora).
       OS-DELETE VALUE(REPLACE(c-arq,SESSION:TEMP-DIR,'u:')).
    END.
    ELSE DO:
       //OS-COMMAND SILENT VALUE("copy " + c-arq + " u:\").
    END.

    /**/

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-it-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-it-codigo w-livre
ON f5 OF c-it-codigo IN FRAME f-cad /* Item */
DO:
  
    {include/zoomvar.i &prog-zoom=inzoom/z01in172.w
                                   &campo=c-it-codigo
                                   &campozoom=it-codigo}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-it-codigo w-livre
ON LEAVE OF c-it-codigo IN FRAME f-cad /* Item */
DO:
  
    ASSIGN c-list-etiq = "".

    {&OPEN-QUERY-br-pergunta}
    {&OPEN-QUERY-br-fixo}
    {&OPEN-QUERY-br-troca}

    FIND ITEM WHERE 
         ITEM.it-codigo = INPUT FRAME {&FRAME-NAME} c-it-codigo NO-LOCK NO-ERROR.
    IF NOT AVAIL ITEM THEN DO: 
       RUN pi-limpa-tela.
       ASSIGN c-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
       MESSAGE "Item nao cadastrado"
           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
       RETURN.
    END.

    IF ITEM.cod-obsol > 1 THEN DO: 
       RUN pi-limpa-tela.
       ASSIGN c-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
       MESSAGE "Item Obsoleto"
           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
       RETURN.
    END.

    FIND item-mat WHERE item-mat.it-codigo = ITEM.it-codigo NO-LOCK NO-ERROR.
    IF NOT AVAIL item-mat THEN DO: 
       RUN pi-limpa-tela.
       MESSAGE "Item nao cadastrado"
           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
       RETURN.
    END.

    FIND estabelec WHERE estabelec.cod-estabel = ITEM.cod-estabel NO-LOCK NO-ERROR.
    IF NOT AVAIL estabelec THEN DO: 
       RUN pi-limpa-tela.
       MESSAGE "Estabelecimento nao cadastrado"
           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
       RETURN.
    END.

    FOR EACH es-etq-fam-comerc WHERE 
        es-etq-fam-comerc.fm-cod-com = ITEM.fm-cod-com NO-LOCK
        BY es-etq-fam-comerc.etiq-padrao DESC
        BY es-etq-fam-comerc.cod-etiqueta.

        IF INDEX(c-list-etiq, es-etq-fam-comerc.cod-etiqueta) = 0 THEN
            ASSIGN c-list-etiq = c-list-etiq + (IF c-list-etiq = "" THEN "" ELSE ",") +
                   es-etq-fam-comerc.cod-etiqueta + "," + es-etq-fam-comerc.cod-etiqueta.
    END.

    IF c-list-etiq = "" THEN DO:
       RUN pi-limpa-tela.
       MESSAGE "Item nao relacionado a nenhuma etiqueta"
           VIEW-AS ALERT-BOX ERROR BUTTONS OK.
       RETURN.
    END.
    ELSE
       ASSIGN cb-etiqueta:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = c-list-etiq
              cb-etiqueta:SCREEN-VALUE IN FRAME {&FRAME-NAME}    = ENTRY(1,c-list-etiq,",").


    /*FIND FIRST item-fornec WHERE item-fornec.it-codigo = ITEM.it-codigo NO-LOCK NO-ERROR.
    IF AVAIL item-fornec THEN DO:
       FIND es-etq-emit-etiqueta WHERE
            es-etq-emit-etiqueta.cod-emitente = item-fornec.cod-emitente NO-LOCK NO-ERROR.
       IF AVAIL es-etq-emit-etiqueta THEN DO:
          ASSIGN cb-etiqueta:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = es-etq-emit-etiqueta.cod-etiqueta + ',' + es-etq-emit-etiqueta.cod-etiqueta.
          ASSIGN cb-etiqueta:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es-etq-emit-etiqueta.cod-etiqueta.

          CREATE BUFFER hb-item-fornec FOR TABLE "item-fornec" NO-ERROR.
          hb-item-fornec:FIND-BY-ROWID( ROWID(item-fornec), NO-LOCK).

       END.
    END.

   FOR EACH item-cli WHERE 
       item-cli.it-codigo = ITEM.it-codigo NO-LOCK,
       FIRST es-etq-emit-etiqueta WHERE
             es-etq-emit-etiqueta.cod-emitente = item-cli.cod-emitente NO-LOCK,
       FIRST es-etq-etiqueta WHERE 
             es-etq-etiqueta.cod-etiqueta = es-etq-emit-etiqueta.cod-etiqueta NO-LOCK.

       ASSIGN cb-etiqueta:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = es-etq-emit-etiqueta.cod-etiqueta + ',' + es-etq-emit-etiqueta.cod-etiqueta.
       ASSIGN cb-etiqueta:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es-etq-emit-etiqueta.cod-etiqueta.
       
       CREATE BUFFER hb-item-cli FOR TABLE "item-cli" NO-ERROR.
       hb-item-cli:FIND-BY-ROWID( ROWID(item-cli), NO-LOCK).

       LEAVE.
   END.
   */
    ASSIGN c-desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.

    RUN pi-layout-etiqueta.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-it-codigo w-livre
ON MOUSE-SELECT-DBLCLICK OF c-it-codigo IN FRAME f-cad /* Item */
DO:
  APPLY 'f5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cb-etiqueta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cb-etiqueta w-livre
ON VALUE-CHANGED OF cb-etiqueta IN FRAME f-cad /* Etiqueta */
DO:
  
    FIND es-etq-etiqueta WHERE 
         es-etq-etiqueta.cod-etiqueta = INPUT FRAME {&FRAME-NAME} cb-etiqueta NO-LOCK NO-ERROR.
    IF NOT AVAIL es-etq-etiqueta THEN RETURN.

    ASSIGN c-desc-etiqueta:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es-etq-etiqueta.desc-etiqueta.

    RUN pi-layout-etiqueta.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-consultas
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-consultas w-livre
ON CHOOSE OF MENU-ITEM mi-consultas /* Consultas */
DO:
  RUN pi-consulta IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-conteudo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-conteudo w-livre
ON CHOOSE OF MENU-ITEM mi-conteudo /* Conteudo */
OR HELP OF FRAME {&FRAME-NAME}
DO:
  RUN pi-ajuda IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-imprimir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-imprimir w-livre
ON CHOOSE OF MENU-ITEM mi-imprimir /* Relat�rios */
DO:
  RUN pi-imprimir IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-programa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-programa w-livre
ON MENU-DROP OF MENU mi-programa /* Nome-do-Programa */
DO:
  run pi-disable-menu.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sair
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sair w-livre
ON CHOOSE OF MENU-ITEM mi-sair /* Sair */
DO:
  RUN pi-sair IN h_p-exihel.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME mi-sobre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL mi-sobre w-livre
ON CHOOSE OF MENU-ITEM mi-sobre /* Sobre... */
DO:
  {include/sobre.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-fixo
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-livre 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-livre  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'panel/p-exihel.w':U ,
             INPUT  FRAME f-cad:HANDLE ,
             INPUT  'Edge-Pixels = 2,
                     SmartPanelType = NAV-ICON,
                     Right-to-Left = First-On-Left':U ,
             OUTPUT h_p-exihel ).
       RUN set-position IN h_p-exihel ( 1.17 , 74.14 ) NO-ERROR.
       /* Size in UIB:  ( 1.25 , 16.00 ) */

       /* Links to SmartPanel h_p-exihel. */
       RUN add-link IN adm-broker-hdl ( h_p-exihel , 'State':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-exihel ,
             c-desc-etiqueta:HANDLE IN FRAME f-cad , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-livre  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-livre  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-livre)
  THEN DELETE WIDGET w-livre.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-livre  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cb-impressora i-num c-it-codigo cb-etiqueta c-desc-item 
          c-desc-etiqueta 
      WITH FRAME f-cad IN WINDOW w-livre.
  ENABLE cb-impressora i-num c-it-codigo cb-etiqueta br-pergunta bt-impressao 
         br-troca rt-button RECT-1 br-fixo 
      WITH FRAME f-cad IN WINDOW w-livre.
  {&OPEN-BROWSERS-IN-QUERY-f-cad}
  VIEW w-livre.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy w-livre 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-livre 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  
  RETURN.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize w-livre 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  run pi-before-initialize.

  {include/win-size.i}

  {utp/ut9000.i "ETQ005" "2.06.00.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  run pi-after-initialize.

  IF l-impres-user THEN
  FOR EACH imprsor_usuar WHERE 
      imprsor_usuar.cod_usuario = c-seg-usuario /*AND 
      imprsor_usuar.log_imprsor_princ */ NO-LOCK, 
      EACH es-etq-impressora WHERE 
           es-etq-impressora.cod-impressora = imprsor_usuar.nom_impressora AND
           es-etq-impressora.ind-ativa NO-LOCK:
        cb-impressora:ADD-LAST(es-etq-impressora.cod-impressora,es-etq-impressora.cod-impressora) IN FRAME {&FRAME-NAME} NO-ERROR.
  END.
  ELSE
  FOR EACH es-etq-impressora WHERE 
      es-etq-impressora.ind-ativa NO-LOCK:
      cb-impressora:ADD-LAST(es-etq-impressora.cod-impressora,es-etq-impressora.cod-impressora) IN FRAME {&FRAME-NAME} NO-ERROR.
  END.

  c-it-codigo:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-layout-etiqueta w-livre 
PROCEDURE pi-layout-etiqueta :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    def var iano as int format '9999'.
    def var iMES AS int format '99'.
    def var ccomposicao as char.

    def var W-valor1 as char.
    def var W-valor2 as char.
    def var iconta3  as int.
    
    FOR EACH tt-pergunta. DELETE tt-pergunta. END.
    FOR EACH tt-troca. DELETE tt-troca. END.

    CREATE BUFFER hb-item           FOR TABLE "item"            NO-ERROR.
    CREATE BUFFER hb-estabelec      FOR TABLE "estabelec"       NO-ERROR.
    CREATE BUFFER hb-item-mat       FOR TABLE "item-mat"        NO-ERROR.
    CREATE BUFFER hb-usuar          FOR TABLE "usuar_mestre"    NO-ERROR.
    CREATE BUFFER hb-mensagem       FOR TABLE "mensagem"        NO-ERROR.
    
    CREATE BUFFER hb-es-emb-glt-klt FOR TABLE "es-emb-glt-klt" NO-ERROR.
    
    FIND es-etq-item WHERE es-etq-item.it-codigo = ITEM.it-codigo NO-LOCK NO-ERROR.
    IF AVAIL es-etq-item THEN DO:
       CREATE BUFFER hb-es-etq-item FOR TABLE "es-etq-item" NO-ERROR.
       hb-es-etq-item:FIND-BY-ROWID( ROWID(es-etq-item), NO-LOCK).
       
    END.

    hb-item:FIND-BY-ROWID ( ROWID(ITEM), NO-LOCK).
    hb-item-mat:FIND-BY-ROWID( ROWID(item-mat), NO-LOCK).
    hb-estabelec:FIND-BY-ROWID( ROWID(estabelec), NO-LOCK).

    hb-es-emb-glt-klt:FIND-BY-ROWID( ROWID(item), NO-LOCK).
    find first es-emb-glt-klt where es-emb-glt-klt.it-codigo = item.it-codigo no-lock no-error.

    FOR EACH ped-item FIELDS(nome-abrev nr-pedcli) USE-INDEX planejamento WHERE
        ped-item.it-codigo = item.it-codigo AND
        ped-item.cod-sit-item < 3 NO-LOCK,
        FIRST ped-venda FIELDS(nat-operacao) OF ped-item NO-LOCK,
        FIRST natur-oper FIELDS(cod-mensagem) OF ped-venda NO-LOCK,
        FIRST mensagem OF natur-oper NO-LOCK.

        hb-mensagem:FIND-BY-ROWID( ROWID(mensagem), NO-LOCK).
        LEAVE.
    END.

    FIND usuar_mestre WHERE 
         usuar_mestre.cod_usuario = c-seg-usuario NO-LOCK NO-ERROR.
    IF AVAIL usuar_mestre THEN
       hb-usuar:FIND-BY-ROWID( ROWID(usuar_mestre), NO-LOCK).

    FIND es-etq-etiqueta WHERE 
         es-etq-etiqueta.cod-etiqueta = INPUT FRAME {&FRAME-NAME} cb-etiqueta NO-LOCK NO-ERROR.
    IF NOT AVAIL es-etq-etiqueta THEN RETURN.

    ASSIGN c-desc-etiqueta:SCREEN-VALUE IN FRAME {&FRAME-NAME} = es-etq-etiqueta.desc-etiqueta.

    FOR EACH es-etq-layout OF es-etq-etiqueta NO-LOCK,
        FIRST es-etq-tipo-dado OF es-etq-layout NO-LOCK.

        IF es-etq-tipo-dado.ind-tipo-dado = 1 THEN DO:
           CREATE tt-troca.
           ASSIGN tt-troca.campo-etiqueta = es-etq-layout.campo-etiqueta 
                  tt-troca.desc-tipo-dado = es-etq-tipo-dado.desc-tipo-dado.

           assign tt-troca.cod-tipo-dado = es-etq-tipo-dado.cod-tipo-dado.
           
           RUN pi-retorna-valor (INPUT  ENTRY(1, TRIM(es-etq-tipo-dado.valor-dado), ".")
                                ,INPUT  ENTRY(2, TRIM(es-etq-tipo-dado.valor-dado), ".")
                                ,OUTPUT tt-troca.c-valor).
           IF es-etq-tipo-dado.num-posicao > 0 THEN 
              ASSIGN tt-troca.c-valor = ENTRY(es-etq-tipo-dado.num-posicao, tt-troca.c-valor, es-etq-tipo-dado.char-separa) NO-ERROR.

        END.

        IF es-etq-tipo-dado.ind-tipo-dado = 2 THEN DO:
           CREATE tt-pergunta.
           ASSIGN tt-pergunta.campo-etiqueta = es-etq-layout.campo-etiqueta
                  tt-pergunta.desc-tipo-dado = es-etq-layout.char-1
                  tt-pergunta.c-valor        = IF es-etq-tipo-dado.valor-dado = 'today' THEN STRING(TODAY,'99/99/99') ELSE 
                   IF es-etq-tipo-dado.valor-dado = 'today-usa' THEN STRING(YEAR(TODAY),'9999') + STRING(MONTH(TODAY),'99') + STRING(DAY(TODAY),'99') ELSE
                   IF es-etq-tipo-dado.valor-dado = 'time' THEN STRING(TIME,'HH:MM:SS') ELSE
                   IF es-etq-tipo-dado.valor-dado = 'dias-ano' THEN STRING(TODAY - DATE('01/01/' + STRING(YEAR(TODAY),'9999')) + 1) ELSE 
                   IF es-etq-tipo-dado.valor-dado = 'year(today)' THEN STRING(YEAR(TODAY),'9999') ELSE
                   IF es-etq-tipo-dado.valor-dado = 'day(today)' THEN STRING(DAY(TODAY),'99') ELSE
                   IF es-etq-tipo-dado.valor-dado = 'month(today)' THEN STRING(MONTH(TODAY),'99') ELSE ''
                  tt-pergunta.valida-data    = es-etq-tipo-dado.int-1.

            assign tt-pergunta.cod-tipo-dado = es-etq-tipo-dado.cod-tipo-dado.

            if es-etq-tipo-dado.cod-tipo-dado = 'PERGUNTA DATA MBB' then do:
                assign tt-pergunta.c-valor = STRING(TODAY,'99/99/9999') no-error.
            end.
            
            ASSIGN c-aux = ''
                   i-today = 0.

            ASSIGN c-aux[1] = ENTRY(1,es-etq-tipo-dado.valor-dado,'+')
                   c-aux[2] = ENTRY(2,es-etq-tipo-dado.valor-dado,'+') 
                   i-today = INT(c-aux[2]) NO-ERROR.
    
            IF c-aux[1] = 'today' AND 
               i-today > 0 THEN
               ASSIGN tt-pergunta.c-valor = STRING(TODAY + i-today,'99/99/99').

            IF tt-pergunta.desc-tipo-dado = 'fifo' OR
               tt-pergunta.desc-tipo-dado = 'ano' OR
               tt-pergunta.desc-tipo-dado = 'Data Primer' OR 
               tt-pergunta.desc-tipo-dado = 'Data Usa' THEN
               ASSIGN tt-pergunta.fixo = YES.


            /* NOVO PADRAO PARA DESENVOLVIMENTO DAS ETIQUETAS "VOLVO PA" */
            if es-etq-etiqueta.cod-etiqueta = 'VOLVO PA' AND
               tt-pergunta.desc-tipo-dado = 'Data Usa' THEN DO:

                assign iano = int(substr(tt-pergunta.c-valor, 1, 4)).
                assign imes = int(substr(tt-pergunta.c-valor, 5, 2)).

                case iano:
                    when 2001 then assign ccomposicao = '1'.
                    when 2002 then assign ccomposicao = '2'.
                    when 2003 then assign ccomposicao = '3'.
                    when 2004 then assign ccomposicao = '4'.
                    when 2005 then assign ccomposicao = '5'.
                    when 2006 then assign ccomposicao = '6'.
                    when 2007 then assign ccomposicao = '7'.
                    when 2008 then assign ccomposicao = '8'.
                    when 2009 then assign ccomposicao = '9'.
                    when 2010 then assign ccomposicao = 'A'.
                    when 2011 then assign ccomposicao = 'B'.
                    when 2012 then assign ccomposicao = 'C'.
                    when 2013 then assign ccomposicao = 'D'.
                    when 2014 then assign ccomposicao = 'E'.
                    when 2015 then assign ccomposicao = 'F'.
                    when 2016 then assign ccomposicao = 'G'.
                    when 2017 then assign ccomposicao = 'H'.
                    when 2018 then assign ccomposicao = 'J'.
                    when 2019 then assign ccomposicao = 'K'.
                    when 2020 then assign ccomposicao = 'L'.
                    when 2021 then assign ccomposicao = 'M'.
                    when 2022 then assign ccomposicao = 'N'.
                    when 2023 then assign ccomposicao = 'P'.
                    when 2024 then assign ccomposicao = 'R'.
                    when 2025 then assign ccomposicao = 'S'.
                    when 2026 then assign ccomposicao = 'T'.
                    when 2027 then assign ccomposicao = 'V'.
                    when 2028 then assign ccomposicao = 'W'.
                    when 2029 then assign ccomposicao = 'X'.
                    when 2030 then assign ccomposicao = 'Y'.
                    when 2031 then assign ccomposicao = '1'.
                    when 2032 then assign ccomposicao = '2'.
                    when 2033 then assign ccomposicao = '3'.
                    when 2034 then assign ccomposicao = '4'.
                    when 2035 then assign ccomposicao = '5'.
                    when 2036 then assign ccomposicao = '6'.
                    when 2037 then assign ccomposicao = '7'.
                    when 2038 then assign ccomposicao = '8'.
                    when 2039 then assign ccomposicao = '9'.
                    otherwise assign ccomposicao = ''.
                end.
                case iMES:
                    when 01 then assign ccomposicao = ccomposicao + 'A'.
                    when 02 then assign ccomposicao = ccomposicao + 'B'.
                    when 03 then assign ccomposicao = ccomposicao + 'C'.
                    when 04 then assign ccomposicao = ccomposicao + 'D'.
                    when 05 then assign ccomposicao = ccomposicao + 'E'.
                    when 06 then assign ccomposicao = ccomposicao + 'F'.
                    when 07 then assign ccomposicao = ccomposicao + 'G'.
                    when 08 then assign ccomposicao = ccomposicao + 'H'.
                    when 09 then assign ccomposicao = ccomposicao + 'J'.
                    when 10 then assign ccomposicao = ccomposicao + 'K'.
                    when 11 then assign ccomposicao = ccomposicao + 'L'.
                    when 12 then assign ccomposicao = ccomposicao + 'M'.
                end.
                
                assign tt-pergunta.c-valor = ccomposicao.
            
            END. /* if es-etq-etiqueta.cod-etiqueta = 'VOLVO PA' AND */
            /* NOVO PADRAO PARA DESENVOLVIMENTO DAS ETIQUETAS "VOLVO PA" */


        END.

        /* TRATATIVA PARA AS ETIQUETAS DA MERCEDES */
        if es-etq-tipo-dado.cod-tipo-dado = 'COD COMPL MBB' then do:
            assign W-valor1 = entry(1, tt-troca.c-valor, ' ')
                   W-valor2 = replace(replace(tt-troca.c-valor, entry(1, tt-troca.c-valor, ' '), ''), ' ', '') no-error.
            if error-status:error then.
            else do:
                do iconta3 = (length(W-valor1) + length(W-valor2) + 1) to 21:
                    W-valor1 = W-valor1 + ' '.
                end.
            end.
            assign W-valor1 = W-valor1 + W-valor2.
            assign tt-troca.c-valor = W-valor1.
        end.
        
        /* TRATATIVA PARA RACKS */
        if es-etq-tipo-dado.cod-tipo-dado BEGINS 'RACK' then 
        DO:
            
            FOR FIRST es-etq-seq NO-LOCK.
            END.
            
            CASE es-etq-tipo-dado.cod-tipo-dado:
                
                WHEN "RACK" THEN 
                    tt-troca.c-valor = STRING(es-etq-seq.dc-seq-1, '99999999999'). 
                
                WHEN "RACK SEQ2" THEN 
                    tt-troca.c-valor = STRING(es-etq-seq.dc-seq-2, '99999999999').
                    
                WHEN "RACK SEQ3" THEN 
                    tt-troca.c-valor = STRING(es-etq-seq.dc-seq-3, '99999999999').
                    
                WHEN "RACK SEQ4" THEN 
                    tt-troca.c-valor = STRING(es-etq-seq.dc-seq-4, '99999999999').
                
            END CASE.
            
            
        END.
       
        
    END.

    DELETE OBJECT hb-item           NO-ERROR.
    DELETE OBJECT hb-estabelec      NO-ERROR.
    DELETE OBJECT hb-item-mat       NO-ERROR.
    DELETE OBJECT hb-mensagem       NO-ERROR.
    /*DELETE OBJECT hb-item-fornec    NO-ERROR.
    DELETE OBJECT hb-item-cli       NO-ERROR.*/
    DELETE OBJECT hb-es-etq-item    NO-ERROR.
    DELETE OBJECT hb-usuar          NO-ERROR.
    
    delete object hb-es-emb-glt-klt no-error.

    {&OPEN-QUERY-br-pergunta}
    {&OPEN-QUERY-br-fixo}
    {&OPEN-QUERY-br-troca}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-limpa-tela w-livre 
PROCEDURE pi-limpa-tela :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

    ASSIGN cb-etiqueta:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = ",".

    ASSIGN c-desc-etiqueta:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".

    FOR EACH tt-pergunta. DELETE tt-pergunta. END.
    FOR EACH tt-troca. DELETE tt-troca. END.

    {&OPEN-QUERY-br-pergunta}
    {&OPEN-QUERY-br-fixo}
    {&OPEN-QUERY-br-troca}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-retorna-valor w-livre 
PROCEDURE pi-retorna-valor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT  PARAMETER ip-tabela   AS CHARACTER    NO-UNDO.
DEFINE INPUT  PARAMETER ip-campo    AS CHARACTER    NO-UNDO.
DEFINE OUTPUT PARAMETER op-dado     AS CHARACTER    NO-UNDO.

    CASE ip-tabela:
    
        WHEN "item" THEN
            ASSIGN op-dado = hb-item:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "item-mat" THEN
            ASSIGN op-dado = hb-item-mat:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "item-fornec" THEN
            ASSIGN op-dado = hb-item-fornec:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "item-cli" THEN
            ASSIGN op-dado = hb-item-cli:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "estabelec" THEN
            ASSIGN op-dado = hb-estabelec:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "es-etq-item" THEN
            ASSIGN op-dado = hb-es-etq-item:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "usuar_mestre" THEN
            ASSIGN op-dado = hb-usuar:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        WHEN "mensagem" THEN do:
            if valid-handle(hb-mensagem) then
            ASSIGN op-dado = hb-mensagem:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
        end.
        WHEN "item-uni-estab" THEN do:
            for first item-uni-estab no-lock where item-uni-estab.it-codigo = item.it-codigo.
                ASSIGN op-dado = string(item-uni-estab.lote-economi) NO-ERROR.
            end.
        end.
        
        WHEN "es-emb-glt-klt" THEN do:
            ASSIGN op-dado = hb-es-emb-glt-klt:BUFFER-FIELD(ip-campo):BUFFER-VALUE NO-ERROR.
            if op-dado = ''  or 
               op-dado = '?' or 
               op-dado = ?   then do:
                if ip-campo = 'cod-local' then
                    ASSIGN op-dado = es-emb-glt-klt.cod-local /* hb-es-emb-glt-klt:BUFFER-FIELD(ip-campo):BUFFER-VALUE */ NO-ERROR.
                if ip-campo = 'embalag' then
                    ASSIGN op-dado = es-emb-glt-klt.embalag /* hb-es-emb-glt-klt:BUFFER-FIELD(ip-campo):BUFFER-VALUE */ NO-ERROR.
            end.
        end.
        when "es-num-chamado-MB" then do:
            for last es-num-chamado-MB. end.
            if not avail es-num-chamado-MB then do: create es-num-chamado-MB. end.
      
            for last es-num-chamado-MB.
               assign es-num-chamado-MB.nr-seq-MB = es-num-chamado-MB.nr-seq-MB + 1.
               assign op-dado = string(es-num-chamado-MB.nr-seq-MB, '999999999') no-error.
            end. /* for last es-num-chamado-MB. */
            RELEASE es-num-chamado-MB.
            for last es-num-chamado-MB NO-LOCK. END.
        end.

        OTHERWISE
            ASSIGN op-dado = "".
    END CASE.

    if op-dado = ? then
       ASSIGN op-dado = "".
    
    IF ip-campo BEGINS 'peso-' THEN
       ASSIGN op-dado = STRING(dec(op-dado),'>>>9.99').
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-livre  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-troca"}
  {src/adm/template/snd-list.i "tt-pergunta"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-livre 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     Manuseia trocas de estado dos SmartObjects
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

