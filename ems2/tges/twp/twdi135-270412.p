/*****************************************************************************
* Empresa  : DATASUL METROPOLITANA
* Cliente  : FASTPLAS
* Programa : TWDI135.P
* Descricao: TRIGGER DE WRITE DA TABELA NOTA-FISCAL
* Autor    : DATASUL METROPOLITANA
* Data     : NOVEMBRO/2003
* Versao   : 2.04.00.000 - Marcos Hoff - Versao inicial.
******************************************************************************/

/* *** DEFINICAO DE PARAMETROS *** */
DEFINE PARAMETER BUFFER b-nota-fiscal     FOR nota-fiscal.
DEFINE PARAMETER BUFFER b-old-nota-fiscal FOR nota-fiscal.

DEF BUFFER b1-nota-fiscal FOR nota-fiscal.


IF  AVAIL b-nota-fiscal THEN DO:
    
    FIND es-natur-oper
        WHERE es-natur-oper.nat-operacao = b-nota-fiscal.nat-operacao
        NO-LOCK NO-ERROR.
    IF  AVAIL es-natur-oper AND
        es-natur-oper.gera-retorno THEN DO:

        FIND FIRST b1-nota-fiscal
            WHERE b1-nota-fiscal.cod-estabel = b-nota-fiscal.cod-estabel
              AND b1-nota-fiscal.serie       = b-nota-fiscal.serie
              AND b1-nota-fiscal.nr-nota-fis = string(int(b-nota-fiscal.nr-nota-fis) - 1,"9999999")
            NO-LOCK NO-ERROR.
        IF  AVAIL b1-nota-fiscal THEN DO:
            IF  b1-nota-fiscal.nat-operacao = es-natur-oper.nat-oper-ret THEN
                ASSIGN b-nota-fiscal.observ-nota = b-nota-fiscal.observ-nota + " " + b1-nota-fiscal.nr-nota-fis.
        END.
    END.
END.
        




