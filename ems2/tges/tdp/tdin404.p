/*****************************************************************************
* Empresa  : DATASUL METROPOLITANA
* Cliente  : FASTPLAS
* Programa : tdin176.P
* Descricao: TRIGGER DE DELETE PARA TABELA DE EXTENSAO ES-saldo-terc
* Autor    : DATASUL METROPOLITANA
* Data     : OUTUBRO/2003
* Versao   : 2.04.00.000 - Marcos Hoff.
******************************************************************************/

/* *** DEFINICAO DE PARAMETROS *** */
DEFINE PARAMETER BUFFER b-saldo-terc FOR saldo-terc.

FIND es-saldo-terc OF b-saldo-terc EXCLUSIVE-LOCK NO-ERROR.
IF  AVAILABLE es-saldo-terc THEN DO:
    DELETE es-saldo-terc.
END.




