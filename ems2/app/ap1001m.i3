/***************************************************************************
***
***   app/ap1001m.i3 - Include usada na ap1001m.i2.
***
***************************************************************************/

if  tt-param.i-tipo-pag = 1 then
    assign i-tipo-pag = 2.
else
    if  tt-param.i-tipo-pag = 2 then
        assign i-tipo-pag = 1.
    else
        if  tt-param.i-tipo-pag = 3 then
            assign i-tipo-pag = 4.
        else
            if  tt-param.i-tipo-pag = 4 then
                assign i-tipo-pag = 5.
            else
                if  tt-param.i-tipo-pag = 83 then
                    assign i-tipo-pag = 4.
                else
                    if  tt-param.i-tipo-pag = 30
                    or  tt-param.i-tipo-pag = 31 then
                        assign i-tipo-pag = 6.

assign c-carteira      = tt-digita.ita-carteira
       l-aceite        = tt-digita.ita-aceite
       c-uso-banco-ita = tt-digita.ita-uso-banco.

find first emitente
     where emitente.cod-emitente = {1}.cod-fornec no-lock no-error.

if  avail emitente then
    assign i-natureza = if  emitente.natureza = 1 then 
                            2
                        else
                            if  emitente.natureza = 2 then
                                1
                            else 
                                 99.

assign i-hea-qua = if i-hea-qua = 0 then 1 else i-hea-qua.

/* Registro tipo 1 - Transacao pagto */

assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,07).
run pi-retira-caracter (input-output c-ctaCorren).
assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,7)),"9999999").

assign c-agen   = substr(cta-corren.agencia-extrato,3,04).
run pi-retira-caracter (input-output c-agen).
assign c-agen  = string(dec(substr(c-agen,1,4)),"9999").



put stream relat
    "1"                                                          at 001
    c-tipo-mov                                                   at 002
    "PG  "                                                       at 003
    "2"                                                          at 007
    substr(c-agen,1,4)                          format "9999"    at 008
    substr(cta-corrente.agencia-extrato,7,1)    format "9"       at 012
    substr(c-ctaCorren,1,7)                     format "9999999" at 013
    substr(cta-corrente.conta-corren-extr,11,1) format "9"       at 020
    "DUP"                                                        at 021
    string({5})                                 format "x(11)"   at 024.



if  {2} < today then
    put stream relat
    day(tt-param.da-pagto)                             format "99" at 039
    month(tt-param.da-pagto)                           format "99" at 041
    substr(string(year(tt-param.da-pagto),"9999"),3,2) format "99" at 043
    "0"                                                            at 045.
else    
    put stream relat
    day({2})                             format "99" at 039
    month({2})                           format "99" at 041
    substr(string(year({2}),"9999"),3,2) format "99" at 043
    "0"                                              at 045.

if  de-valor-pag = 0 then
    put stream relat
    {3} * 100 format "999999999999999" at 046.
else
    put stream relat
    de-valor-pag format "999999999999999" at 046.
put stream relat
    "398"                                       at 061
    "PG.FORNECEDOR                 "            at 064
    i-tipo-pag                       format "9" at 094.

    if  i-tipo-pag = 1 then
        put stream relat "1" at 095.
    else
        put stream relat "0" at 095.

/*assign de-hea-val = de-hea-val + {3}.*/

put stream relat
    "000000000000000000000000000000"                         at 096
    {1}.cod-fornec                   format "999999999"      at 126
    "0000"                           format "9999"           at 135
    i-natureza                       format "99"             at 139
    c-aux                            format "99999999999999"
                                                             at 141.

if  i-tipo-pag = 4 then do:
   put stream relat
        tt-digita.banco format "999" at 155.
end.
else
    put stream relat "000" format "999" at 155.

if  i-tipo-pag = 1 or i-tipo-pag = 2
or  i-tipo-pag = 4 or i-tipo-pag = 5 then do:
    assign c-ctaCorren  = substr(tt-digita.cta-corrente,1,10).
    run pi-retira-caracter (input-output c-ctaCorren).
    assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,10)),"9999999999").

    assign c-digito     = substr(tt-digita.cta-corrente,11,2).

    assign c-agen   = substr(tt-digita.agencia,2,5).
    run pi-retira-caracter (input-output c-agen).
    assign c-agen  = string(dec(substr(c-agen,1,5)),"99999").

    put stream relat 
        substr(c-agen,1,5)            format "99999"       at 158
        substr(tt-digita.agencia,7,1) format "9"           at 163
        substr(c-ctaCorren,1,10)      format "9999999999"  at 164.
    if length(trim(c-digito)) < 2 then 
       put stream relat  
           substr(c-digito,1,1)       FORMAT "9"           at 174.
    ELSE
      put stream relat  
          " "                         FORMAT "9"           at 174.       

    find first agencia
         where agencia.cod-banco = i-nr-banco
         and   agencia.agencia   = c-agencia  no-lock no-error.

    if  avail agencia then
        put stream relat
            agencia.nome format "x(30)" at 175.
end.

if  i-tipo-pag > 5 then do:
    put stream relat 
        "00000"      at 158
        "0000000000" at 164.
end.

put stream relat 
    "000000000" at 205.

if  i-tipo-pag = 1 or i-tipo-pag = 4
or  i-tipo-pag = 5 or i-tipo-pag = 6 then do:
    put stream relat
        c-nome     format "x(40)"    at 214
        c-endereco format "x(30)"    at 254
        c-bairro   format "x(15)"    at 299
        i-cep      format "99999999" at 314
        c-cidade   format "x(20)"    at 322
        c-estado   format "!!"       at 342.
end.

put stream relat 
    string({5})                          format "x(11)" at 344
    day({4})                             format "99"    at 355  
    month({4})                           format "99"    at 357
    substr(string(year({4}),"9999"),3,2) format "99"    at 359
    "000000000"                                         at 362.

if tt-param.i-tipo-pag = 3 then do:
    put stream relat
       "700" at 371.
end.
else do:
    if tt-param.i-tipo-pag = 83 then do:
        put stream relat
            "018" at 371.
    end.
    else do:
        put stream relat
            "000" at 371.
    end.
end.
put stream relat                     
    " "                                  at 374.

if length(trim(c-digito)) = 2 then 
   put stream relat  
       substr(c-digito,1,2)  Format "99" at 375.
else 
   put stream relat
    "  "                     Format "99" at 375.       

put stream relat
    "000000000000000000"                 at 377
    i-hea-qua + 1        format "999999" at 395.

/* Registro tipo 4 - codigo de barras */
if i-tipo-pag = 6 then do:
    assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,7).
    run pi-retira-caracter (input-output c-ctaCorren).
    assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,7)),"9999999").

    assign c-agen   = substr(cta-corrente.agencia-extrato,3,4).
    run pi-retira-caracter (input-output c-agen).
    assign c-agen  = string(dec(substr(c-agen,1,4)),"9999").
    put stream relat
        skip
        "4"                                                       at 001
        "I"                                                       at 002
        "PG   "                                                   at 003
        dec(substr(c-agen,1,4))                  format "9999"    at 008
        substr(cta-corrente.agencia-extrato,7,1) format "9"       at 012
        dec(substr(c-ctaCorren,1,7))             format "9999999" at 013
        substr(cta-corrente.conta-corren-extr,11,1)  format "9"   at 020.


     if  tt-digita.log-leitora = yes then
        put stream relat
            tt-digita.c-cod-barras format "x(44)" at 021.
    ELSE
        put stream relat
            tt-digita.c-cod-barras format "x(47)" at 065.

    put stream relat
        tt-digita.titulo-banco format "x(20)"  at 155
        i-hea-qua + 2          format "999999" at 395.

    assign i-hea-qua = i-hea-qua + 1.
end.

/* Fim de Include */
