/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
define buffer empresa for mgcad.empresa. 
define buffer portador for mgcad.portador.
define buffer banco for mgcad.banco.

{include/i-prgvrs.i AP1001M 2.00.00.078}  /*** 010078 ***/




/******************************************************************************
**
**  Programa: AP1001M.P
**  Objetivo: Layout - Banco Real
**
******************************************************************************/

/**********************************************************************************
**
**    Include : CDGFGFIN.i
**
**    Objetivo: Tratamento de Miniflexibiliza��o para a release 2.03
**    OBS: A include gerada no workspace MAN EMS 2.0 (POR) deve estar vazia)
***********************************************************************************/

/* Funcoes da Release 2.02 */

/* Conseguir expedir os objetos da WEB em OS da 2.00.*//*&glob BF_FIN_MOD_E_COLLECT          /* Fun��o Envio de Movtos via e-Collect */ PROJETO CANCELADO */

/* Funcoes da Release 2.03 */

/* Informar nr. de periodos na Consulta de Fluxo de Caixa Gerencial (cb1205)*//* Melhorias Contabiliza��o AN e PEF - Multiplanta *//* Tratar multa e abatimento na Liquida��o do T�tulo *//* Funcao generica, que n�o faz parte do Plano de Produto *//* Localizacao Argentina - Nota de Debito e Credito Propria *//* Cobran�a Centralizada. *//* Modulo de Transferencias Eletronicas Bancarias *//* Seguranca de Produto - Validacao das Sequencias no EMS 2 *//* Retorna Saldo da Previsao *//* Integracao Faturamento e Recebimento com APP/CRP *//* Provisao no Contas a Pagar - para ser liberado de forma especial *//* Tratamento para o Modulo RAC - Controle de Conta de Representantes *//* Controle de Cheques Contas a Receber - F.O. 620.611*/
/* Funcoes da Release 2.04 */

/* API Eliminacao da Tabela NF-VENDOR *//* Relatorio e Consulta de Moedas *//* Alteracao Antecipacao/PEF via Multiplanta *//* Check Inscri��o Estadual E.M.S. 2.0 *//* Informacoes Vendor via Multiplanta na Implantacao de Titulos *//* Layout Cobranca Escritural Banco Mercantil *//* Provis�o no Contas a Pagar *//* Rastreabilidade na Contabilidade *//* Melhorias Envio Pagto Escritural *//* Totaliza por dia Razao FASB,CMCAC e Valor Presente *//* Implantar ou Importar Titulos ou Previs�es sem Validar Portador e Moeda *//* Especifico - Verificar Antecipacoes na Baixa por Cheque Nominal *//* Tabela Progressiva de IR *//* Integracao Magnus I.01 X EMS 2.04 Financeiro *//* Modulo de C�mbio *//* Transacao Unica para o Multiplanta do Contas a Receber *//* Transacao Unica para o Multiplanta do Contas a Pagar *//* Isolamento Programas DCF e DCU (Cadastros de Integracoes Financeira) - Eng. EMS 5 *//* Encontro de Contas AP e CR*//* Cobranca Escritural Banco Santander *//* Funcao generica, para pequenas funcoes. Indica o codigo da release/dicionario *//* Especifico - Incluir o parametro Mantem numero do cheque na reimpress�o de cheques *//* Especifico - Autorizacao de documentos vinculados a um bordero em preparacao *//* Especifico - Considera ou nao o valor realizado das aplicacoes e emprestimos no fluxo de caixa *//* Especifico - Mostrar juros em um tipo de despesa diferente do valor principal *//* Especifico - Ativar/Desativar Conta Contabil por Faixa *//* Emitir Titulos em Aberto/Razao em CMCAC - CR *//* Emitir Titulos em Aberto/Razao em CMCAC - AP *//* Conciliacao Contabil CR em FASB/CMCAC e VP *//* Conciliacao Contabil AP em FASB/CMCAC e VP *//* Validar se o Movimento esta conciliado no Caixa e Bancos *//* Especifico - Configurar nome arquivo pagamento escritural *//* Diario do Patrimonio *//* Parte Fixa da Conta Contabil no Razao *//* Impressao Historico do Emitente *//* Especifico - Valor Minimo de Imposto de Renda *//* Bancos Historicos AP/CR/CB *//* Especifico - Ativa Desativa faixa de contas contabeis no Programa de Consolidacao*//* Especifico - Imprimir no fluxo resumido (operacional) apenas os totais di�rios. *//* Especifico - Inclui op��o de faixa de Subconta *//* Validacao Modulo Multiplanta Financeiro *//* Relatorio Fluxo Gerencial Previsto p/ Compras e Pedidos *//* Integracao EMS2 x Tesouraria do EMS5 *//* Vendor Citibank - Especifico Incorporado *//* C�mbio futuro versus fluxo de caixa e varia��o */ 

/*****************************************************************************
**
**  I-RPVAR.I - Variaveis para Impress�o do Cabecalho Padr�o (ex-CD9500.I)
**
*****************************************************************************/

/*************************************************
* i_dbvers.i - Include de vers�o de banco de dados   
**************************************************/

/* Preprocessadores que identificam os bancos do Produto EMS 5 */

/* Preprocessadores que identificam os bancos do Produto EMS 2 */
/*RAC Incorporado na 2.04*/

/* Preprocessadores que identificam os bancos do Produto HR */
/*Esta include est� sendo liberada vazia para o EMS 2
 para n�o ocorrer erros de compila��o*/
 


/* Fim */


/* Fim */
    

/* Fim */
    
.    
/* Fim */

 

define var c-empresa       as character format "x(40)"      no-undo.
define var c-titulo-relat  as character format "x(50)"      no-undo.
define var c-sistema       as character format "x(25)"      no-undo.
define var i-numper-x      as integer   format "ZZ"         no-undo.
define var da-iniper-x     as date      format "99/99/9999" no-undo.
define var da-fimper-x     as date      format "99/99/9999" no-undo.
define var c-rodape        as character                     no-undo.
define var v_num_count     as integer                       no-undo.
define var c-arq-control   as character                     no-undo.
define var i-page-size-rel as integer                       no-undo.
define var c-programa      as character format "x(08)"      no-undo.
define var c-versao        as character format "x(04)"      no-undo.
define var c-revisao       as character format "999"        no-undo.
define var c-impressora   as character                      no-undo.
define var c-layout       as character                      no-undo.

/*Defini��es inclu�das para corrigir problema de vari�veis j� definidas pois */
/*as vari�veis e temp-tables eram definidas na include --rpout.i que pode ser*/
/*executada mais de uma vez dentro do mesmo programa (FO 1.120.458) */
/*11/02/2005 - Ed�sio <tech14207>*/
/*-------------------------------------------------------------------------------------------*/
DEF VAR h-procextimpr                               AS HANDLE   NO-UNDO. 
DEF VAR i-num_lin_pag                               AS INT      NO-UNDO.    
DEF VAR c_process-impress                           AS CHAR     NO-UNDO.   
DEF VAR c-cod_pag_carac_conver                      AS CHAR     NO-UNDO.   

/*tech14207
FO 1663218
Inclu�das as defini��es das vari�veis e fun��es
*/

    /*tech868*/
    
        /*Alteracao 03/04/2008 - tech40260 - FO 1746516 -  Feito valida��o para verificar se a variavel h_pdf_controller j� foi definida 
                                                       anteriormente, evitando erro de duplicidade*/

        
            DEFINE VARIABLE h_pdf_controller     AS HANDLE NO-UNDO.
    
                
            DEFINE VARIABLE v_cod_temp_file_pdf  AS CHAR   NO-UNDO.
    
            DEFINE VARIABLE v_cod_relat          AS CHAR   NO-UNDO.
            DEFINE VARIABLE v_cod_file_config    AS CHAR   NO-UNDO.
    
            FUNCTION allowPrint RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION allowSelect RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION useStyle RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION usePDF RETURNS LOGICAL IN h_pdf_controller.
       
            FUNCTION getPrintFileName RETURNS CHARACTER IN h_pdf_controller.
       
            RUN btb/btb920aa.p PERSISTENT SET h_pdf_controller.
        
        /*Alteracao 03/04/2008 - tech40260 - FO 1746516 -  Feito valida��o para verificar se a variavel h_pdf_controller j� foi definida 
                                                       anteriormente, evitando erro de duplicidade*/
    /*tech868*/
    

/*tech14207*/
/*tech30713 - fo:1262674 - Defini��o de no-undo na temp-table*/
DEFINE TEMP-TABLE tt-configur_layout_impres_inicio NO-UNDO
    FIELD num_ord_funcao_imprsor    LIKE configur_layout_impres.num_ord_funcao_imprsor
    FIELD cod_funcao_imprsor        LIKE configur_layout_impres.cod_funcao_imprsor
    FIELD cod_opc_funcao_imprsor    LIKE configur_layout_impres.cod_opc_funcao_imprsor
    FIELD num_carac_configur        LIKE configur_tip_imprsor.num_carac_configur
    INDEX ordem num_ord_funcao_imprsor .

/*tech30713 - fo:1262674 - Defini��o de no-undo na temp-table*/
DEFINE TEMP-TABLE tt-configur_layout_impres_fim NO-UNDO
    FIELD num_ord_funcao_imprsor    LIKE configur_layout_impres.num_ord_funcao_imprsor
    FIELD cod_funcao_imprsor        LIKE configur_layout_impres.cod_funcao_imprsor
    FIELD cod_opc_funcao_imprsor    LIKE configur_layout_impres.cod_opc_funcao_imprsor
    FIELD num_carac_configur        LIKE configur_tip_imprsor.num_carac_configur
    INDEX ordem num_ord_funcao_imprsor .
/*-------------------------------------------------------------------------------------------*/

define buffer b_ped_exec_style for ped_exec.
define buffer b_servid_exec_style for servid_exec.

    define new shared stream str-rp.

 
 
/* i-rpvar.i */
/*Altera��o 20/07/2007 - tech1007 - Defini��o da vari�vel utilizada para impress�o em PDF*/
DEFINE VARIABLE v_output_file        AS CHAR   NO-UNDO.
 
/**************************************************************************
**
** I-FREEAC - Define function para transformar string's acentuadas em nao
**            acentuadas.
**
***************************************************************************/

FUNCTION fn-free-accent RETURNS char (INPUT p-string AS char ).
    define var c-free-accent as char case-sensitive no-undo.
    
    assign c-free-accent = p-string
           c-free-accent =  replace(c-free-accent, '�', 'A')
           c-free-accent =  replace(c-free-accent, '�', 'A')
           c-free-accent =  replace(c-free-accent, '�', 'A')
           c-free-accent =  replace(c-free-accent, '�', 'A')
           c-free-accent =  replace(c-free-accent, '�', 'A')
           c-free-accent =  replace(c-free-accent, '�', 'E')
           c-free-accent =  replace(c-free-accent, '�', 'E')
           c-free-accent =  replace(c-free-accent, '�', 'E')
           c-free-accent =  replace(c-free-accent, '�', 'E')
           c-free-accent =  replace(c-free-accent, '�', 'I')
           c-free-accent =  replace(c-free-accent, '�', 'I')
           c-free-accent =  replace(c-free-accent, '�', 'I')
           c-free-accent =  replace(c-free-accent, '�', 'I')
           c-free-accent =  replace(c-free-accent, '�', 'O')
           c-free-accent =  replace(c-free-accent, '�', 'O')
           c-free-accent =  replace(c-free-accent, '�', 'O')
           c-free-accent =  replace(c-free-accent, '�', 'O')
           c-free-accent =  replace(c-free-accent, '�', 'O')
           c-free-accent =  replace(c-free-accent, '�', 'U')
           c-free-accent =  replace(c-free-accent, '�', 'U')
           c-free-accent =  replace(c-free-accent, '�', 'U')
           c-free-accent =  replace(c-free-accent, '�', 'U')
           c-free-accent =  replace(c-free-accent, '�', 'Y')
           c-free-accent =  replace(c-free-accent, '�', 'Y')
           c-free-accent =  replace(c-free-accent, '�', 'C')
           c-free-accent =  replace(c-free-accent, '�', 'N')
           c-free-accent =  replace(c-free-accent, '�', 'a')
           c-free-accent =  replace(c-free-accent, '�', 'a')
           c-free-accent =  replace(c-free-accent, '�', 'a')
           c-free-accent =  replace(c-free-accent, '�', 'a')
           c-free-accent =  replace(c-free-accent, '�', 'a')
           c-free-accent =  replace(c-free-accent, '�', 'e')
           c-free-accent =  replace(c-free-accent, '�', 'e')
           c-free-accent =  replace(c-free-accent, '�', 'e')
           c-free-accent =  replace(c-free-accent, '�', 'e')
           c-free-accent =  replace(c-free-accent, '�', 'i')
           c-free-accent =  replace(c-free-accent, '�', 'i')
           c-free-accent =  replace(c-free-accent, '�', 'i')
           c-free-accent =  replace(c-free-accent, '�', 'i')
           c-free-accent =  replace(c-free-accent, '�', 'o')
           c-free-accent =  replace(c-free-accent, '�', 'o')
           c-free-accent =  replace(c-free-accent, '�', 'o')
           c-free-accent =  replace(c-free-accent, '�', 'o')
           c-free-accent =  replace(c-free-accent, '�', 'o')
           c-free-accent =  replace(c-free-accent, '�', 'u')
           c-free-accent =  replace(c-free-accent, '�', 'u')
           c-free-accent =  replace(c-free-accent, '�', 'u')
           c-free-accent =  replace(c-free-accent, '�', 'u')
           c-free-accent =  replace(c-free-accent, '�', 'y')
           c-free-accent =  replace(c-free-accent, '�', 'y')
           c-free-accent =  replace(c-free-accent, '�', 'c')
           c-free-accent =  replace(c-free-accent, '�', 'n')
           c-free-accent =  replace(c-free-accent, '�', 'a')
           c-free-accent =  replace(c-free-accent, '�', 'o')
           c-free-accent =  replace(c-free-accent, '&', 'E').
     
    return c-free-accent.
end function.
 

assign c-programa     = "AP1001M".
       c-titulo-relat = "Envio de Documentos ao Banco".
       c-sistema      = "Contas a Pagar".

/****************************************************************************
**
**  I-RPCAB.I - Form do Cabe�alho Padr�o e Rodap� (ex-CD9500.F)
**                              
** {&STREAM} - indica o nome da stream (opcional)
****************************************************************************/


    
        form header
            fill("-", 132) format "x(132)" skip
            c-empresa c-titulo-relat at 50
            "P�gina:":U at 120 page-number  at 128 format ">>>>9" skip
            fill("-", 112) format "x(110)" today format "99/99/9999"
            "-" string(time, "HH:MM:SS":U) skip(1)
            with stream-io width 132 no-labels no-box page-top frame f-cabec.
        
        form header
            fill("-", 132) format "x(132)" skip
            c-empresa c-titulo-relat at 50
            "P�gina:":U at 120 page-number  at 128 format ">>>>9" skip
            "Periodo:":U i-numper-x at 10 "-"
            da-iniper-x at 15 "a":U da-fimper-x
            fill("-", 74) format "x(72)" today format "99/99/9999"
            "-" string(time, "HH:MM:SS":U) skip(1)
            with stream-io width 132 no-labels no-box page-top frame f-cabper.
    


c-rodape = "DATASUL - ":U + c-sistema + " - " + c-prg-obj + " - V:":U + c-prg-vrs.
c-rodape = fill("-", 132 - length(c-rodape)) + c-rodape.

form header
    c-rodape format "x(132)"
    with stream-io width 132 no-labels no-box page-bottom frame f-rodape.

/* I-RPCAB.I */
 

/****************************************************************************
***
***  - app/ap1001.i - Defini��o temp-table's.
***
*****************************************************************************/
/*************************************************
* i_dbvers.i - Include de vers�o de banco de dados   
**************************************************/

/* Preprocessadores que identificam os bancos do Produto EMS 5 */

/* Preprocessadores que identificam os bancos do Produto EMS 2 */
/*RAC Incorporado na 2.04*/

/* Preprocessadores que identificam os bancos do Produto HR */
/*Esta include est� sendo liberada vazia para o EMS 2
 para n�o ocorrer erros de compila��o*/
 


/* Fim */


/* Fim */
    

/* Fim */
    
.    
/* Fim */

 
def temp-table tt-param no-undo
    field destino           as inte
    field arquivo           as char
    field usuario           as char
    field data-exec         as date
    field hora-exec         as inte
    field i-empresa         as inte
    field c-est-ini         as char
    field c-est-fim         as char
    field c-esp-ini         as char
    field c-esp-fim         as char
    field c-ser-ini         as char
    field c-ser-fim         as char
    field i-docto-ini       as char
    field i-docto-fim       as char
    field i-fornec-ini      as inte FORMAT "999999999"
    field i-fornec-fim      as inte FORMAT "999999999" 
    field da-vencimen-ini   as date
    field da-vencimen-fim   as date
    field da-inicial        as date
    field da-final          as date
    field i-bord-ini        as inte
    field i-bord-fim        as inte
    field i-portador        as inte
    field i-escolhe         as inte
    field i-sel-bordero     as inte
    field i-ocorrencia      as inte
    field i-tipo-pag        as inte
    field i-documento       as inte
    field l-estado          as logi
    field c-arquivo-remessa as char
    field i-op-banco        as inte
    field cta-corrente      as char format "XXXXXXXXXX-XX"
    field agencia           as char format "XXXXXX-X"
    field identific-emp     as char format "x(8)"
    field l-vid-rel         as logical
    field conta-compl       as inte
    field c-tipo-docum      as char format "x(01)"
    field i-finalidade      as inte format "99"
    field de-tipo-band      as dec  format "9999999999"
    field i-agencia         as inte format "9999999"
    field cod-versao-integ  as inte
    field i-conta-bos       as inte format "99999999"
    field i-agenc-bos       as inte format "9999999"
    field l-autoriza        as logical
    field i-sequencia       as integer
    field da-pagto          as date
    field i-convenio        as inte format "999999"
    field c-filial          as char 
    field d-conta-cosmos    as dec format "9999999999"
    FIELD l-incl-param-arq  as log.
        
def temp-table tt-raw-param no-undo
    field raw-param as raw.
        
def temp-table tt-tit-ap no-undo like tit-ap
    field row-mov-ap        as rowid
    field selecionado       as logical init no
    field c-trans           as char format "x(03)" label "TRA"
    field banco             like mov-ap.banco
    field agencia           like mov-ap.agencia
    field cta-corrente      like mov-ap.cta-corrente
    field c-cod-barras      as char format "x(50)"
    field ita-aceite        as logi 
    field ita-carteira      as char format "x(05)"
    field ita-uso-banco     as char format "x(10)"
    field ita-vl-abatim     as dec
    field ita-vl-multa      as dec
    field c-moeda           as char
    field log-leitora       as logical
    field r-rowid           as rowid
    field i-tipo-pagto      as integer
    INDEX codigo IS PRIMARY UNIQUE 
       row-mov-ap
    INDEX dt-trans 
       dt-transacao
    INDEX dt-venc  
       dt-vencimen
    INDEX saldo    
       valor-saldo
    INDEX abrev    
       nome-abrev
    INDEX docto    
       nr-docto
    INDEX fornec   
       cod-fornec.
    
def temp-table tt-erro-pag no-undo
    field i-cod-erro        as integer format "9999999"
    field c-desc-erro       as char format "x(70)"
    field c-arquivo-erro    as char format "x(100)".

def temp-table tt-digita no-undo like tt-tit-ap.

def buffer b-tt-digita for tt-digita.

def temp-table tt-raw-digita no-undo
    field raw-digita  as raw.

/* Fim de Include */
  /* defini��o das temp-tables */
/******************************************************************************
**
**  AP1001.i1
**  Objetivo: Definicao das variaveis shareds e new shareds, Versao do Prg.
**
******************************************************************************/

def   shared var l-param           as logical.
def   shared var l-resp1           as logical format "Escolhe/Todos".
def   shared var l-resp2           as logical format "Marca/Desmarca".
def   shared var l-resp3           as logical format "Bordero/Dt.trans".
def   shared var l-estado          as logical format "Considera/Nao Considera".
def   shared var l-gerar           as logical .
def   shared var l-alt-doc         as logical format "Sim/Nao".

def   shared var da-inicial      like mov-ap.dt-emissao  init today.
def   shared var da-final        like mov-ap.dt-emissao  init today.
def   shared var da-vencimen-ini like mov-ap.dt-vencimen init today.
def   shared var da-vencimen-fim like mov-ap.dt-vencimen init today.

def   shared var i-empresa    like mov-ap.ep-codigo  init 1.
def   shared var i-hea-qua    as integer format "99999".
def   shared var i-portador   like mov-ap.portador init 0.
def   shared var i-banco-def  as integer format ">>9"  no-undo.
def   shared var i-nr-banco   as integer format "999"  no-undo.
def   shared var i-agencia    as integer format "999999-x" no-undo.
def   shared var i-cep        like emitente.cep no-undo.
def   shared var i-cont       as integer format "999999" init 1 no-undo.
def   shared var i-con        as integer format "999999"     no-undo.
def   shared var i-movimento  as integer format "999999" init 1 no-undo.
def   shared var i-dia        as integer format "99"         no-undo.
def   shared var i-mes        as integer format "99"         no-undo.
def   shared var i-fornec-ini like mov-ap.cod-fornec format "zzzzzzzz9" init 0.
def   shared var i-fornec-fim like mov-ap.cod-fornec format "zzzzzzzz9" init 999999999.
def   shared var i-docto-ini  like mov-ap.nr-docto init 0.
def   shared var i-docto-fim  like mov-ap.nr-docto init 99999999.
def   shared var i-bord-ini   like mov-ap.nr-bordero.
def   shared var i-bord-fim   like mov-ap.nr-bordero  init 999999.
def   shared var i-ocorrencia as integer format "99" init 0  no-undo.
def   shared var i-central    as integer format "99999".
def   shared var i-tipo-pag   as integer format "99" label "Tipo Pgto".
def   shared var i-final-doc  as integer format "99" label "Finalidade DOC".
def   shared var i-tipo-pag-ant as integer format "99" no-undo.
def   shared var i-mar-qua    as integer format "99999".
def   shared var i-port-cod   like mov-ap.portador.
def   shared var i-op-banco   as integer.
def   shared var i-cod030     like empresa.ep-codigo.
def   shared var i-tipo-mov   as int format "9".
def   shared var i-num-reg    as int format "9999".
def   shared var i-digito     as int.
def   shared var i-sequencia  as int.
def   shared var i-natureza   as int format "9".
def   shared var i-cod-cedente as int format "99999999" no-undo.

def   shared var c-tipo-mov   as char    format "x(1)".
def   shared var c-tipo-pag   as char    format "x(02)" label "Tipo Pgto".
def   shared var c-oco        as character format "!!!".
def   shared var c-cc-forn    as character no-undo.
def   shared var agencia      as character format "xxxxxx-x".
def   shared var cta-corrente as char format "xxxxxxxxxx-xx".
def   shared var c-aux        as character format "x(18)"        no-undo.
def   shared var c-cgc        as character format "x"            no-undo.
def   shared var c-cgc-emp    as character format "x(14)"        no-undo.
def   shared var c-ser-ini  like mov-ap.serie   init "".
def   shared var c-ser-fim  like mov-ap.serie   init "ZZZZZ".
def   shared var c-esp-ini  like mov-ap.cod-esp init "".
def   shared var c-esp-fim  like mov-ap.cod-esp init "ZZ".
def   shared var c-est-ini  like mov-ap.cod-estab init "".
def   shared var c-est-fim  like mov-ap.cod-estab init "ZZZ".
def   shared var c-estado   like emitente.estado init ""     no-undo.
def   shared var c-ano        as character format "xx"       no-undo.
def   shared var c-nome-empresa  like empresa.nome.
def   shared var c-nome       as character format "x(37)"    no-undo.
def   shared var c-endereco   as character format "x(37)"    no-undo.
def   shared var c-bairro     as character format "x(30)"    no-undo.
def   shared var c-cidade     as character format "x(15)"    no-undo.
def   shared var c-traco      as character format "x" init "/" no-undo.
def   shared var c-arquivo-remessa as character format "x(20)".
def   shared var c-ext-arq    as character format "x(03)".
def   shared var c-arq-aux    as character format "x(12)".
def   shared var c-cod-mov    as character format "x(02)".
def   shared var c-agencia-che   like agencia.agencia.
def   shared var c-usu-sigma  as character format "x(05)"    no-undo.
def   shared var c-tipo-doc   as character format "!"        no-undo.
def   shared var c-transacao  as character format "xxx".
def   shared var c-msg        as character format "x(30)"
    extent 2 init [" "," "] no-undo.

def   shared var de-hea-val   as decimal format "99999999999.99".
def   shared var de-inscric   as dec     format "99999999999999" no-undo.
def   shared var de-desconto  like tit-ap.vl-desconto no-undo.
def   shared var de-juros     like tit-ap.vl-juros-dia no-undo.
def   shared var c-moeda      as char format "x(1)".
def   shared var i-ident-emp  as decimal format ">>>>>>>9" no-undo.

def   shared var r-mov-ap     as rowid.

/* Zoom */
def   shared var i-nr-doc      as integer format "999999" no-undo.
def   shared var i-cod-banco   like cta-corrente.cod-banco.
def   shared var i-banco       like cta-corrente.cod-banco.
def   shared var i-cod-agencia like cta-corrente.agencia.
def   shared var c-conta       as char format "999999999-xx".
def   shared var c-agencia     as char.
def   shared var c-port-abrev  like portador.nome-abrev.
def   shared var l-implanta    as logical.
def   shared var i-lote        as integer format "9999" no-undo.
def   shared var l-leitora     as logical format "Sim/Nao" 
        label "Possui Leitora" init yes no-undo.


def var de-valor     as decimal.
def var de-valor-ori like mov-ap.valor-mov.
def var de-valor-pag like mov-ap.valor-mov.
def var de-valor-oco like mov-ap.valor-mov.
def var de-mar-val   as decimal format "9999999999999.99".
def var de-tot-trans-b as decimal format ">,>>>,>>>,>>>,>>9.99" no-undo.
def var de-tot-geral-b as decimal format ">,>>>,>>>,>>>,>>9.99" no-undo.
def var de-tot-geral-l as decimal format ">,>>>,>>>,>>>,>>9.99" no-undo.
def var de-tot-inc   as decimal format ">,>>>,>>>,>>>,>>9.99" no-undo.
def var de-tot-canc  as decimal format ">,>>>,>>>,>>>,>>9.99" no-undo.
def var de-abatimen  like mov-ap.vl-desconto.
def var de-multa     like mov-ap.vl-desconto.
def var de-carteira  as dec format "999".

def var i-hora       as integer   format 99 init 00 no-undo.
def var i-min        as integer   format 99 init 00 no-undo.
def var i-seg        as integer   format 99 init 00 no-undo.
def var i-mar-val    as integer   format "99999".
def var i-cont-arq   as integer   format "9999".
def var i-cod-oco    as integer   format "999".
def var i-posicao    as integer.

def var c-nome-abrev like emitente.nome-abrev       no-undo.
def var c-horario    as character format "HH:MM:SS" no-undo.
def var c-instrucao  as character format "xx"       no-undo.
def var c-pagto      as character format "xx".
def var c-banco      as character format "x(16)"    no-undo.
def var c-observacao as character format "x(100)"   no-undo.
def var c-uso-banco  as character format "x(10)".
def var c-carteira   as character format "x(05)".
def var c-docto      as character format "x(19)".
def var c-uso-banco-ita as character format "x(10)".
def var c-dv-agcc    as character format "x" no-undo.
def var c-verifica-dvcc as char format "xx" no-undo.
def var c-cod-barras as character no-undo.

def var da-data       as date    format 99/99/99.
def var da-venci      as date    format 99/99/99.

def var l-controle    as logical.
def var l-avisa       as logical format "5/0".
def var l-aceite      as logical format "S/N".
def var l-antecipacao as logical no-undo.

def stream relat.

/* Banco Nacional, dados complementares */
def   shared temp-table w-docto
    field w-ap-banco    as integer format "999"
    field w-ap-agencia  as char    format "999999-x"
    field w-ap-conta    as char    format "xxxxxxxxxx-xx"
    field w-ap-recid    as rowid.

/* Banco Itau, dados complementares */
def   shared temp-table w-docto-ita
    field w-ita-banco     as integer format "999"
    field w-ita-agencia   as char    format "999999-x"
    field w-ita-conta     as char    format "xxxxxxxxxx-xx"
    field w-ita-nnum      as char    format "x(20)" 
    field w-ita-c1        as char    format "99999.99999"
    field w-ita-c2        as char    format "99999.999999"
    field w-ita-c3        as char    format "99999.999999"
    field w-ita-dvg       as int     format "9" 
    field w-ita-c5        as char    format "x(12)" 
    field w-ita-aceite    as log     format "S/N"
    field w-ita-carteira  as char  format "x(05)"
    field w-ita-uso-banco as char  format "x(10)"
    field w-ita-vl-abatim as dec
    field w-ita-vl-multa  as dec
    field w-ita-recid     as rowid.


/* Banco Safra, dados complementares */
def   shared temp-table w-docto-saf
    field w-saf-banco     as integer format "999"
    field w-saf-agencia   as char    format "999999-x"
    field w-saf-conta     as char    format "xxxxxxxxxx-xx"
    field w-saf-nnum      as char    format "x(20)" 
    field w-saf-c1        as char    format "99999.99999"
    field w-saf-c2        as char    format "99999.999999"
    field w-saf-c3        as char    format "99999.999999"
    field w-saf-dvg       as int     format "9" 
    field w-saf-c5        as char    format "x(14)" 
    field w-saf-aceite    as log     format "S/N"
    field w-saf-carteira  as char    format "x(05)"
    field w-saf-uso-banco as char    format "x(10)"
    field w-saf-vl-abatim as dec
    field w-saf-vl-multa  as dec
    field w-saf-recid     as rowid.

/* Banco Bradesco, dados complementares */
def   shared temp-table w-docto-brad
    field w-brad-banco     as integer format "999"
    field w-brad-agencia   as char    format "999999-x"
    field w-brad-conta     as char    format "xxxxxxxxxx-xx"
    field w-brad-nnum      as char    format "x(20)" 
    field w-brad-c1        as char    format "99999.99999"
    field w-brad-c2        as char    format "99999.999999"
    field w-brad-c3        as char    format "99999.999999"
    field w-brad-dvg       as int     format "9" 
    field w-brad-c5        as char    format "x(14)" 
    field w-brad-aceite    as log     format "S/N"
    field w-brad-carteira  as char    format "x(05)"
    field w-brad-uso-banco as char    format "x(10)"
    field w-brad-vl-abatim as dec
    field w-brad-vl-multa  as dec
    field w-brad-recid     as rowid.

/* Banco do Brasil, dados complementares */
def   shared temp-table w-docto-bra
    field w-bra-banco     as integer format "999"
    field w-bra-agencia   as char    format "999999-x"
    field w-bra-conta     as char    format "xxxxxxxxxx-xx"
    field w-bra-nnum      as char    format "x(20)" 
    field w-bra-c1        as char    format "99999.99999"
    field w-bra-c2        as char    format "99999.999999"
    field w-bra-c3        as char    format "99999.999999"
    field w-bra-dvg       as int     format "9" 
    field w-bra-c5        as char    format "x(14)" 
    field w-bra-aceite    as log     format "S/N"
    field w-bra-carteira  as char  format "x(05)"
    field w-bra-uso-banco as char  format "x(10)"
    field w-bra-vl-abatim as dec
    field w-bra-vl-multa  as dec
    field w-bra-recid     as rowid.
    
/* Banco Real, dados complementares */
def   shared workfile w-docto-real
    field w-real-banco      as integer format "999"
    field w-real-agencia    as char    format "999999-x"
    field w-real-conta      as char    format "xxxxxxxxx-xx"
    field w-real-nnum       as char    format "x(20)" 
    field w-real-c1         as char    format "99999.99999"
    field w-real-c2         as char    format "99999.999999"
    field w-real-c3         as char    format "99999.999999"
    field w-real-dvg        as int     format "9" 
    field w-real-c5         as char    format "x(12)" 
    field w-real-aceite     as log     format "S/N"
    field w-real-carteira   as char    format "x(05)"
    field w-real-uso-banco  as char    format "x(10)"
    field w-real-vl-abatim  as dec
    field w-real-vl-multa   as dec
    field w-real-cod-barras as char format "x(47)"
    field w-real-recid      as recid.

def   shared temp-table w-aux-movap
    field w-recid    as rowid
    field w-confirma as logical init no.

def   shared frame f-alt-doc.
form
    "Altera informacoes ?" l-alt-doc 
    with stream-io overlay no-labels row 19 column 1 frame f-alt-doc.

 

/*******************************************************************************
**
** AP1001D.I6 - Verifica se utiliza varios tipos de pagamento Bradesco
**
*******************************************************************************/


def var l-varios-pagtos as logical no-undo.



    if (can-find(funcao where funcao.cd-funcao = "spp-varios-pagto" 
    and funcao.ativo = yes ) ) then 
        assign l-varios-pagtos = yes. 

  /* Verifica se utiliza varios tipos de pagamento banco BRADESCO */


/* MINIFLEXIBILIZA��O *********/  
/*************************************************
* i_dbvers.i - Include de vers�o de banco de dados   
**************************************************/

/* Preprocessadores que identificam os bancos do Produto EMS 5 */

/* Preprocessadores que identificam os bancos do Produto EMS 2 */
/*RAC Incorporado na 2.04*/

/* Preprocessadores que identificam os bancos do Produto HR */
/*Esta include est� sendo liberada vazia para o EMS 2
 para n�o ocorrer erros de compila��o*/
 


/* Fim */


/* Fim */
    

/* Fim */
    
.    
/* Fim */

 
def var de-vl-saldo as dec no-undo.
/******************************/

def input param table for tt-digita.
def input param table for tt-param.
def var c-ctaCorren    as char no-undo.
def var c-agen         as char no-undo.
def var c-digito       as char format "x(2)" no-undo.
def var de-mul as decimal init 0 no-undo.
def var de-aba as decimal init 0 no-undo.
def var vl-tot-impto as decimal no-undo.
def var c-cta-corren-ext as char no-undo.
def var l-spp-escrit-oracle as logical no-undo.
find first tt-param no-error.

assign l-spp-escrit-oracle = no.
if  can-find(first funcao where funcao.cd-funcao = 'spp-escrit-ora') then
    assign l-spp-escrit-oracle = yes.

/*****************************************************************************
**
**  AP1001.i2
**  Objetivo: Definicao de frames e abertura dos Arquivos de Saida,Impressao.
**
******************************************************************************/


form
    "**----------------- Par�metros ----------------**" at  07
    "**-------------------------- Sele��o -------------------------**" at 63

    "Fornec Inicial:"                          at  64
    tt-param.i-fornec-ini  format "zzzzzzzz9"  at  80
    "Fornec Final:"                            at 101
    tt-param.i-fornec-fim  format "zzzzzzzz9"  at 115

    "Estab.Inicial:"                           at  65
    tt-param.c-est-ini                         at  80
    "Estab.Final:"                             at 102
    tt-param.c-est-fim                         at 115

    "Empresa:"                                 at  20
    tt-param.i-empresa                         at  41

    "Esp.Inicial:"                             at  67
    tt-param.c-esp-ini                         at  80
    "Esp.Final:"                               at 104
    tt-param.c-esp-fim                         at 115

    "Portador:"                                at  19
    tt-param.i-portador                        at  39

    "Serie Inicial:"                           at  65
    tt-param.c-ser-ini                         at  80
    "Serie Final:"                             at 102
    tt-param.c-ser-fim                         at 115

    "Lay-Out:"                                 at  20
    c-banco                                    at  29

    "Docto Inicial:"                           at  65
    tt-param.i-docto-ini                       at  80
    "Docto Final:"                             at 102
    tt-param.i-docto-fim                       at 115

    "Arquivo:"                                 at  20
    tt-param.c-arquivo-remessa format "x(29)"  at  31

    "Border� Inic:"                            at  66
    tt-param.i-bord-ini                        at  80
    "Border� Final"                            at 100
    tt-param.i-bord-fim                        at 115

    "Data Tr.Inic:"                            at  66
    tt-param.da-inicial  format "99/99/9999"   at  80
    "Data Tr.Final:"                           at 100
    tt-param.da-final    format "99/99/9999"   at 115

    "Venc.Inicial:"                            at  66
    tt-param.da-vencimen-ini format "99/99/9999" at  80
    "Venc.Final:"                              at 103
    tt-param.da-vencimen-fim format "99/99/9999" at 115
    skip(1)
    with stream-io width 132 no-labels no-box frame f-cabec1.
run utp/ut-trfrrp.p (input frame f-cabec1:handle).

form
    skip(1)
    "Data de Vencimento:" da-venci no-label skip
    skip(1)
    with stream-io width 132 no-labels no-box frame f-venci.
run utp/ut-trfrrp.p (input frame f-venci:handle).

form
    "Fornecedor"         at 01
    "Est"                at 22
    "Esp"                at 26
    "S�rie"              at 29
    "N�mero Docto/P"     at 35
    /*"Data Emis"          at 55*/
    "Data Venc"          at 55
    "Valor"              at 66    

    "Desc/Abat"          at 80
    "Juros/Multa"        at 91

    "Tp"                 at 100
    "Tra"                at 103
    "Bco"                at 107
    " Ag�ncia"           at 111
    "C.Corrente"         at 120
    skip
    "-------------------- --- -------- ------" at 01
    "------------- ---------- ------------- -" at 41
    "-------- --------- -- --- ---- ------- -" at 81
    "-----------" at 121 skip
    with stream-io width 132 no-labels no-box page-top frame f-cabec2.
run utp/ut-trfrrp.p (input frame f-cabec2:handle).    

form
    mov-ap.cod-fornec                      at 01
    mov-ap.nome-abrev  format "x(10)"      at 11
    mov-ap.cod-estabel                     at 22
    mov-ap.cod-esp                         at 26
    mov-ap.serie                           at 29
    c-docto                                at 35
    /*mov-ap.dt-emissao  format "99/99/9999" at 55*/
    da-data            format "99/99/9999" at 55
    de-valor           format ">>,>>>,>>9.99" at 66
    mov-ap.vl-desconto format ">>>,>>9.99"    at 79
    mov-ap.vl-juro-fasb format ">>>,>>9.99"   at 89
    i-tipo-pag         format "99"         at 100
    c-transacao        format "xxx"        at 103
    i-nr-banco                             at 107
    c-agencia          format "999999-x"   at 111
    c-cc-forn          format "xxxxxxxxxx-xx" at 120
    with stream-io width 132 down no-labels no-box no-attr frame f-detalhe.
run utp/ut-trfrrp.p (input frame f-detalhe:handle).    

form
    "Forneced"        at 01
    "Estab"           at 08
    "Esp"             at 14
    "Serie"           at 18
    "Numero/P"        at 27
    "Dt Emissao"      at 38
    "Dt Vencimen"     at 50
    "Dt Pagto"        at 62
    "Valor Movimento" at 77
    "Transacao"       at 96 skip
    "----------------------------------------" at 01
    "----------------------------------------" at 41
    "----------------------------------------" at 81
    "------------" at 121 skip
    with stream-io width 132 no-labels no-box page-top frame f-cab2.
run utp/ut-trfrrp.p (input frame f-cab2:handle).        

form
    skip(1)
    de-tot-geral-l label "Total do Movimento Liquido" colon 70
    de-tot-geral-b label "Total do Movimento Bruto  " colon 70
    with stream-io width 132 side-labels no-box no-attr frame f-tot-geral.
run utp/ut-trfrrp.p (input frame f-tot-geral:handle).            

form
    "Observacao:"
    c-observacao
    with stream-io width 132 side-labels no-label no-box no-attr frame f-observacao.
run utp/ut-trfrrp.p (input frame f-observacao:handle).                

form
    c-arquivo-remessa label "Arquivo Remessa"
    with stream-io overlay side-labels row 18 frame f-nome1.
run utp/ut-trfrrp.p (input frame f-nome1:handle).

find first portador
     where portador.ep-codigo    = i-empresa
     and   portador.cod-portador = i-portador
     no-lock no-error.
find first banco
     where banco.cod-banco  = portador.cod-banco
     no-lock no-error.

assign i-cont         = 1
       i-dia          = day(today)
       i-mes          = month(today)
       c-ano          = substr(string(year(today)),3,2)
       c-horario      = string(time,"HH:MM:SS")
       c-banco        = banco.nome-banco
       i-hora         = integer(substring(c-horario,1,2))
       i-min          = integer(substring(c-horario,4,2))
       i-seg          = integer(substring(c-horario,7,2))
       de-tot-geral-b = 0
       de-tot-geral-l = 0.

/**************************************************************************
**
** I-RPOUT - Define sa�da para impress�o do relat�rio - ex. cd9520.i
** Parametros: {&stream} = nome do stream de saida no formato "stream nome"
**             {&append} = append    
**             {&tofile} = nome da vari�vel ou campo com arquivo de destino
**             {&pagesize} = tamanho da pagina
***************************************************************************/

/*As defini��es foram transferidas para a include i-rpvar.i (FO 1.120.458) */
/*11/02/2005 - Ed�sio <tech14207>*/

 def new global shared var c-dir-spool-servid-exec as char no-undo.                     
 def new global shared var i-num-ped-exec-rpw as int no-undo.                           
                                                                                   
/*variaveis processador externo impress�o localiza��o*/


 /* procedimento necess�rio para permitir redirecionar saida para arquivo temporario no pdf sem aparecer na pagina de parametros */

    ASSIGN v_output_file = tt-param.arquivo.





/*tech14178 inicio defini��es PDF */

    
    /*tech868*/    
    /*tech868*/
        IF NUM-ENTRIES(tt-param.arquivo,"|":U) > 1  THEN DO:
            ASSIGN v_cod_relat = ENTRY(2,tt-param.arquivo,"|":U).
            ASSIGN v_cod_file_config = ENTRY(3,tt-param.arquivo,"|":U).
            ASSIGN tt-param.arquivo = ENTRY(1,tt-param.arquivo,"|":U).
    
            RUN pi_prepare_permissions IN h_pdf_controller(INPUT v_cod_relat).
            RUN pi_set_format IN h_pdf_controller(INPUT IF v_cod_relat <> "":U THEN "PDF":U ELSE "Texto":U).
            RUN pi_set_file_config IN h_pdf_controller(INPUT  v_cod_file_config).
            
        END.
        
        IF usePDF() AND tt-param.destino = 2 THEN DO:
            IF entry(num-entries(tt-param.arquivo,".":U),tt-param.arquivo,".":U) <> "pdf" THEN
                assign tt-param.arquivo = replace(tt-param.arquivo,".":U + entry(num-entries(tt-param.arquivo,".":U),tt-param.arquivo,".":U),".pdf":U).

        END.

        IF usePDF() AND tt-param.destino <> 1 THEN /*tech14178 muda o nome do arquivo para salvar temporario quando n�o � impressora*/
            ASSIGN v_output_file = tt-param.arquivo + ".pdt".



        
        
    /*tech868*/
    





    IF usePDF() AND tt-param.destino = 1  THEN /*pega arquivo tempor�rio randomico para ser usado como impressora */
        ASSIGN v_cod_temp_file_pdf = getPrintFileName().


/*tech14178 fim defini��es PDF */

 
    
/*29/12/2004 - tech1007 - Verfica se o arquivo informado tem extensao rtf, se tiver troca para .lst*/


if  tt-param.destino = 1 then do:
   
    if num-entries(tt-param.arquivo,":") = 2 then do:
   .                            
    
        assign c-impressora = substring(tt-param.arquivo,1,index(tt-param.arquivo,":") - 1).
        assign c-layout     = substring(tt-param.arquivo,index(tt-param.arquivo,":") + 1,length(tt-param.arquivo) - index(tt-param.arquivo,":")). 
    .                            

    find layout_impres no-lock
        where layout_impres.nom_impressora    = c-impressora
        and   layout_impres.cod_layout_impres = c-layout no-error.
    find imprsor_usuar no-lock
        where imprsor_usuar.nom_impressora = c-impressora
        and   imprsor_usuar.cod_usuario    = tt-param.usuario
        use-index imprsrsr_id no-error.
    find impressora  of imprsor_usuar no-lock no-error.
    find tip_imprsor of impressora    no-lock no-error.

    /*Alterado 26/04/2005 - tech1007 - Alterado para n�o ocasionar problemas na convers�o do mapa de caracteres*/
    IF AVAILABLE tip_imprsor THEN DO:
        ASSIGN c-cod_pag_carac_conver = tip_imprsor.cod_pag_carac_conver.
    END.
    IF AVAILABLE layout_impres THEN DO:
        ASSIGN i-num_lin_pag = layout_impres.num_lin_pag.
    END.
    /*Fim alteracao - tech1007*/

    
    if  i-num-ped-exec-rpw <> 0 then do:
        find b_ped_exec_style where b_ped_exec_style.num_ped_exec = i-num-ped-exec-rpw no-lock no-error. 
        find b_servid_exec_style of b_ped_exec_style no-lock no-error.
        find servid_exec_imprsor of b_servid_exec_style
          where servid_exec_imprsor.nom_impressora = imprsor_usuar.nom_impressora no-lock no-error.

        if  available b_servid_exec_style and b_servid_exec_style.ind_tip_fila_exec = 'UNIX'
        then do:
            /*tech14178 joga para arquivo a ser convertido para PDF */
                IF usePDF() THEN DO:
                    output  through value(v_cod_temp_file_pdf)
                            page-size value(layout_impres.num_lin_pag) convert target tip_imprsor.cod_pag_carac_conver.
                    RUN pi_set_print_device IN h_pdf_controller(INPUT servid_exec_imprsor.nom_disposit_so).
                END.
                ELSE
            
            output  through value(servid_exec_imprsor.nom_disposit_so)
                   page-size value(layout_impres.num_lin_pag) convert target tip_imprsor.cod_pag_carac_conver.
        end /* if */.
        else do:

            /*Alterado 26/04/2005 - tech1007 - As variaveis i-num_lin_pag e c-cod_pag_carac_conver estao sendo alteradas antes dos testes */
            ASSIGN 
                c-arq-control = servid_exec_imprsor.nom_disposit_so.
            /*Fim alteracao 26/04/2005*/

            IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN
                RUN pi_before_output IN h-procextimpr 
                    (INPUT c-impressora,
                     INPUT c-layout,
                     INPUT tt-param.usuario,
                     INPUT-OUTPUT c-arq-control,
                     INPUT-OUTPUT i-num_lin_pag,
                     INPUT-OUTPUT c-cod_pag_carac_conver).
            
            /*tech14178 joga para arquivo a ser convertido para PDF */
                IF usePDF() THEN DO:
                    output   to value(v_cod_temp_file_pdf)
                            page-size value(i-num_lin_pag) convert target c-cod_pag_carac_conver.
                    RUN pi_set_print_device IN h_pdf_controller(INPUT servid_exec_imprsor.nom_disposit_so).
                END.
                ELSE
            
                    output   to value(c-arq-control)
                           page-size value(i-num_lin_pag) convert target c-cod_pag_carac_conver.
        end /* else */.
    end.
    else do:

        /*Alterado 26/04/2005 - tech1007 - As variaveis i-num_lin_pag e c-cod_pag_carac_conver estao sendo alteradas antes dos testes */
        ASSIGN 
            c-arq-control = imprsor_usuar.nom_disposit_so.
        /*Fim alteracao 26/04/2005*/

        IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN
            RUN pi_before_output IN h-procextimpr 
                (INPUT c-impressora,
                 INPUT c-layout,
                 INPUT tt-param.usuario,
                 INPUT-OUTPUT c-arq-control,
                 INPUT-OUTPUT i-num_lin_pag,
                 INPUT-OUTPUT c-cod_pag_carac_conver).

        if i-num_lin_pag = 0 then do:
            /*tech14178 joga para arquivo a ser convertido para PDF */
                IF usePDF() THEN DO:
                /* sem salta p�gina */
                    output   
                            to value(v_cod_temp_file_pdf)
                            page-size 0
                            convert target c-cod_pag_carac_conver . 
                    RUN pi_set_print_device IN h_pdf_controller(INPUT imprsor_usuar.nom_disposit_so).
                END.
                ELSE
            
                /* sem salta p�gina */
                    output   
                            to value(c-arq-control)
                            page-size 0
                            convert target c-cod_pag_carac_conver . 
        end.
        else do:
            /*tech14178 joga para arquivo a ser convertido para PDF */
                IF usePDF() THEN DO:
                /* sem salta p�gina */
                    output   
                            to value(v_cod_temp_file_pdf)
                            paged page-size value(i-num_lin_pag) 
                            convert target c-cod_pag_carac_conver .
                    RUN pi_set_print_device IN h_pdf_controller(INPUT imprsor_usuar.nom_disposit_so).
                END.
                ELSE
            
                    /* com salta p�gina */
                    output  
                            to value(c-arq-control)
                            paged page-size value(i-num_lin_pag) 
                            convert target c-cod_pag_carac_conver .
        end.
    end.

    for each configur_layout_impres NO-LOCK 
        where configur_layout_impres.num_id_layout_impres = layout_impres.num_id_layout_impres
        by configur_layout_impres.num_ord_funcao_imprsor:

        find configur_tip_imprsor no-lock
            where configur_tip_imprsor.cod_tip_imprsor        = layout_impres.cod_tip_imprsor
            and   configur_tip_imprsor.cod_funcao_imprsor     = configur_layout_impres.cod_funcao_imprsor
            and   configur_tip_imprsor.cod_opc_funcao_imprsor = configur_layout_impres.cod_opc_funcao_imprsor
            use-index cnfgrtpm_id no-error.
        CREATE tt-configur_layout_impres_inicio.    
        BUFFER-COPY configur_tip_imprsor TO tt-configur_layout_impres_inicio
            ASSIGN tt-configur_layout_impres_inicio.num_ord_funcao_imprsor = configur_layout_impres.num_ord_funcao_imprsor.
    end.

    IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN
        RUN pi_after_output IN h-procextimpr (INPUT-OUTPUT TABLE tt-configur_layout_impres_inicio).

    FOR EACH tt-configur_layout_impres_inicio EXCLUSIVE-LOCK
        BY tt-configur_layout_impres_inicio.num_ord_funcao_imprsor :
        do v_num_count = 1 to extent(tt-configur_layout_impres_inicio.num_carac_configur):            
          case tt-configur_layout_impres_inicio.num_carac_configur[v_num_count]:
            when 0 then put  control null.
            when ? then leave.
            otherwise   put  control CODEPAGE-CONVERT(chr(tt-configur_layout_impres_inicio.num_carac_configur[v_num_count]),
                                                               session:cpinternal, 
                                                               c-cod_pag_carac_conver).
          end case.
        end.
        DELETE tt-configur_layout_impres_inicio.
    END.
  end.
  else do:
    
        assign c-impressora  = entry(1,tt-param.arquivo,":").
        assign c-layout      = entry(2,tt-param.arquivo,":"). 
        if num-entries(tt-param.arquivo,":") = 4 then
          assign c-arq-control = entry(3,tt-param.arquivo,":") + ":" + entry(4,tt-param.arquivo,":").
        else 
          assign c-arq-control = entry(3,tt-param.arquivo,":").
    .                            

    find layout_impres no-lock
        where layout_impres.nom_impressora    = c-impressora
        and   layout_impres.cod_layout_impres = c-layout no-error.
    find imprsor_usuar no-lock
        where imprsor_usuar.nom_impressora = c-impressora
        and   imprsor_usuar.cod_usuario    = tt-param.usuario
        use-index imprsrsr_id no-error.
    find impressora  of imprsor_usuar no-lock no-error.
    find tip_imprsor of impressora    no-lock no-error.

    /*Alterado 26/04/2005 - tech1007 - Alterado para n�o ocasionar problemas na convers�o do mapa de caracteres*/
    IF AVAILABLE tip_imprsor THEN DO:
        ASSIGN c-cod_pag_carac_conver = tip_imprsor.cod_pag_carac_conver.
    END.
    IF AVAILABLE layout_impres THEN DO:
        ASSIGN i-num_lin_pag = layout_impres.num_lin_pag.
    END.
    /*Fim alteracao - tech1007*/

    
    
    /*tech14178 adiciona extens�o PDT para que o arquivo a ser convertido n�o fique com o mesmo nome do arquivo final*/
        IF usePDF() THEN 
            ASSIGN c-arq-control = c-arq-control + ".pdt":U.
    

    


    if  i-num-ped-exec-rpw <> 0 then do:
        find b_ped_exec_style where b_ped_exec_style.num_ped_exec = i-num-ped-exec-rpw no-lock no-error. 
        find b_servid_exec_style of b_ped_exec_style no-lock no-error.
        find servid_exec_imprsor of b_servid_exec_style 
          where servid_exec_imprsor.nom_impressora = imprsor_usuar.nom_impressora no-lock no-error.

        if  available b_servid_exec_style and b_servid_exec_style.ind_tip_fila_exec = 'UNIX'
        then do:
            output  to value(c-dir-spool-servid-exec + "~/" + c-arq-control)
                   page-size value(layout_impres.num_lin_pag) convert target tip_imprsor.cod_pag_carac_conver.
        end /* if */.
        else do:
            output   to value(c-dir-spool-servid-exec + "~/" + c-arq-control)
                   page-size value(layout_impres.num_lin_pag) convert target tip_imprsor.cod_pag_carac_conver.
        end /* else */.
    end.
    else do:
        /*Alterado 26/04/2005 - tech1007 - Removido pois o assign est� sendo realizado antes dos testes
        ASSIGN 
            i-num_lin_pag = layout_impres.num_lin_pag
            c-cod_pag_carac_conver = tip_imprsor.cod_pag_carac_conver.
        Fim alteracao 26/04/2005*/    

        IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN
            RUN pi_before_output IN h-procextimpr 
                (INPUT c-impressora,
                 INPUT c-layout,
                 INPUT tt-param.usuario,
                 INPUT-OUTPUT c-arq-control,
                 INPUT-OUTPUT i-num_lin_pag,
                 INPUT-OUTPUT c-cod_pag_carac_conver).
        if i-num_lin_pag = 0 then do:
            /* sem salta p�gina */
            output   
                    to value(c-arq-control)
                    page-size 0
                    convert target c-cod_pag_carac_conver . 
        end.
        else do:
            /* com salta p�gina */
            output  
                    to value(c-arq-control)
                    paged page-size value(layout_impres.num_lin_pag) 
                    convert target tip_imprsor.cod_pag_carac_conver.
        end.
    end.
    
    /*tech14178 guarda o nome do arquivo a ser convertido para pdf  */
            if  i-num-ped-exec-rpw <> 0 THEN
                RUN pi_set_print_filename IN h_pdf_controller (INPUT c-dir-spool-servid-exec + "~/" + c-arq-control).
            ELSE
                RUN pi_set_print_filename IN h_pdf_controller (INPUT c-arq-control).
    


    for each configur_layout_impres NO-LOCK 
        where configur_layout_impres.num_id_layout_impres = layout_impres.num_id_layout_impres
        by configur_layout_impres.num_ord_funcao_imprsor:

        find configur_tip_imprsor no-lock
            where configur_tip_imprsor.cod_tip_imprsor        = layout_impres.cod_tip_imprsor
            and   configur_tip_imprsor.cod_funcao_imprsor     = configur_layout_impres.cod_funcao_imprsor
            and   configur_tip_imprsor.cod_opc_funcao_imprsor = configur_layout_impres.cod_opc_funcao_imprsor
            use-index cnfgrtpm_id no-error.
        CREATE tt-configur_layout_impres_inicio.    
        BUFFER-COPY configur_tip_imprsor TO tt-configur_layout_impres_inicio
            ASSIGN tt-configur_layout_impres_inicio.num_ord_funcao_imprsor = configur_layout_impres.num_ord_funcao_imprsor.
    end.

    IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN                
        RUN pi_after_output IN h-procextimpr (INPUT-OUTPUT TABLE tt-configur_layout_impres_inicio).

    FOR EACH tt-configur_layout_impres_inicio EXCLUSIVE-LOCK
        BY tt-configur_layout_impres_inicio.num_ord_funcao_imprsor :
        do v_num_count = 1 to extent(tt-configur_layout_impres_inicio.num_carac_configur):
          case tt-configur_layout_impres_inicio.num_carac_configur[v_num_count]:
            when 0 then put  control null.
            when ? then leave.
            otherwise   put  control CODEPAGE-CONVERT(chr(tt-configur_layout_impres_inicio.num_carac_configur[v_num_count]),
                                                               session:cpinternal, 
                                                               c-cod_pag_carac_conver).
                            
          end case.
        end.
        DELETE tt-configur_layout_impres_inicio.
    END.
  end.  
end.
else do:
    
        if  i-num-ped-exec-rpw <> 0 then do:
            
              /*Alterado 14/02/2005 - tech1007 - Alterado para que quando for gerar RTF em batch
                o tamanho da p�gina seja 42*/
              
                      output  
                             to value(c-dir-spool-servid-exec + "~/" + v_output_file) 
                             paged page-size 64
                             convert target "iso8859-1" .
              
              /*Fim alteracao 14/02/2005*/
            
        end.                             
        else do:
            /* Sa�da para RTF - tech981 20/10/2004 */
            
                
                        output  
                               to value(v_output_file) 
                               paged page-size 64 
                               convert target "iso8859-1" .                
                
            
        end.    
    
end.

/* i-rpout */
 



/****************************************************************************
*
* Objetivo: Verificar utiliza��o da configura��o de arquivo p/ Pagto Esc.
* Data    : 17/10/2000
*
*****************************************************************************/

def var l-arq-pag-escr as log no-undo.

assign l-arq-pag-escr = no.


    assign l-arq-pag-escr = yes.
.
 
def var i-barra          as int  no-undo.
def var i-erro           as int  no-undo.   
def var c-file           as char no-undo.
def var c-retorno-dir    as char no-undo.

    if  l-arq-pag-escr = yes then do:

        assign c-retorno-dir = "".
        block:
        do  i-barra = 1 to length(tt-param.c-arquivo-remessa):
            assign c-file = c-file + substr(tt-param.c-arquivo-remessa, i-barra, 1).
            if  substr(tt-param.c-arquivo-remessa, i-barra, 1) = "/" then do:
                os-create-dir value(c-file). 
                i-erro = OS-ERROR.
                IF  i-erro NE 0 THEN do:
                    run utp/ut-msgs.p (input "msg",
                                       input 17441,
                                       input string(i-erro)).

                    assign c-retorno-dir = return-value.
                    leave block.            
                end.   
            end.       
        end.       

        if  c-retorno-dir <> "" then do:
            disp c-retorno-dir @ c-observacao
                 with frame f-observacao.
            return.     
        end.
    end.        


view frame f-cabec.
view frame f-rodape.
/*
disp tt-param.i-empresa
     tt-param.i-portador
     c-banco
     tt-param.c-arquivo-remessa
     tt-param.c-esp-ini
     tt-param.c-esp-fim
     tt-param.c-est-ini
     tt-param.c-est-fim
     tt-param.da-inicial
     tt-param.da-final
     tt-param.da-vencimen-ini
     tt-param.da-vencimen-fim
     tt-param.i-docto-ini
     tt-param.i-docto-fim
     tt-param.i-fornec-ini
     tt-param.i-fornec-fim
     tt-param.c-ser-ini
     tt-param.c-ser-fim
     tt-param.i-bord-ini
     tt-param.i-bord-fim
     with frame f-cabec1.
*/     
disp with frame f-cabec2.

output stream relat to value(tt-param.c-arquivo-remessa) page-size 0.

/*--  AP1001.i2 --*/
 

/**************************************************************************
** AP1001M.I1
** Objetivo : Registros header do banco REAL
**************************************************************************/

find first empresa
    where empresa.ep-codigo = i-empresa no-lock no-error.

find first portador 
    where portador.ep-codigo    = i-empresa
    and   portador.cod-portador = i-portador no-lock no-error.

find banco
    where banco.cod-banco = portador.cod-banco no-lock no-error.

find first cta-corrente
    where cta-corrente.cod-banco    = portador.cod-banco
    and   cta-corrente.agencia      = portador.agencia
    and   cta-corrente.conta-corren = portador.conta-corren no-lock no-error.

assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,07).
run pi-retira-caracter (input-output c-ctaCorren).
assign c-ctaCorren  = string(int(substr(c-ctaCorren,1,7)),"9999999").

assign c-agen  = substr(cta-corren.agencia-extrato,3,04).
run pi-retira-caracter (input-output c-agen).
assign c-agen  = string(int(substr(c-agen,1,4)),"9999").

put stream relat
    "0"                                                         at 001
    "1"                                                         at 002
    "REMESSA"                                                   at 003
    "05"                                                        at 010
    "PG FORNECEDORES"                                           at 012
    substr(c-agen,1,4)                      format "9999"    at 027
    substr(cta-corren.agencia-extrato,7,1)     format "9"       at 031
    substr(c-ctaCorren,1,7)                    format "9999999" at 032
    substr(cta-corrente.conta-corren-extr,11,1)format "9"       at 039
    "      "                                                    at 040
    empresa.razao-social                format "x(30)"          at 047
    "356"                                                       at 077
    "ABN AMRO REAL"                                             at 080
    day(today)                               format "99"        at 095
    month(today)                             format "99"        at 097
    substr(string(year(today),"9999"),3,2)   format "99"        at 099
    day(today)                               format "99"        at 101
    month(today)                             format "99"        at 103
    substr(string(year(today),"9999"),3,2)   format "99"        at 105.

assign c-horario = string(time,"HH:MM:SS")
       i-hea-qua = 1.

put stream relat
    substr(c-horario,1,2)                    format "99"        at 107
    substr(c-horario,4,2)                    format "99"        at 109
    substr(c-horario,7,2)                    format "99"        at 111
    "000000"                                                    at 113
    "PG  "                                                      at 119
    "                             "                             at 123
    "01"                                                        at 153
    c-cgc-emp                   format "999999999999999"        at 155
    i-hea-qua                                format "999999"    at 395
    skip.

/*-- ap1001m.i1 --*/
  /* registro header */
/****************************************************************************
**
**   app/ap1001.i3 - Gerenciamento dos movimentos p/ envio ao banco.
**
****************************************************************************/

/*fut36887 - atividade 140138 - testa se o usu�rio tem o Banking Connect habilitado*/
/**************************************************************************
**  Include.: /include/getdefinedfunction.i
**  Objetivo: Verifica se existe fun��o de libera��o especial
**  Data....: 20/10/2003
***************************************************************************/

FUNCTION GetDefinedFunction RETURNS LOGICAL (INPUT p_cod AS character):

    DEF VAR v_log_retorno AS LOGICAL INITIAL NO NO-UNDO.
    def var v_cd_funcao   as char    format "x(16)" initial "" no-undo.

    assign v_cd_funcao = p_cod.
    
    IF  CAN-FIND (FIRST funcao NO-LOCK
        WHERE funcao.cd-funcao = v_cd_funcao
        AND   funcao.ativo     = yes) THEN
        ASSIGN v_log_retorno = YES.

    /************************************************************************************
**  Include.: /include/i-fnextvers.i
**  Objetivo: Imprime a fun��o no extrato de vers�o caso o mesmo esteja habilitado 
**  Data....: 20/10/2003
************************************************************************************/

if  c-arquivo-log <> "" and c-arquivo-log <> ? then do:
    output to value(c-arquivo-log) append.
    put v_cd_funcao at 1 v_log_retorno at 69 skip.
    output close.        
end.  
error-status:error = no.
 

    RETURN v_log_retorno.
END FUNCTION.
  /*** Verificar se a fun��o especial est� habilitada ***/

def var l-banking-connect as logical initial no no-undo.
DEF VAR v_hdl_reg_produt  AS HANDLE NO-UNDO.             /* Handle para o controle de registro - cdp/cd4210b.p */
DEF VAR l-registro-ok     AS LOGICAL INITIAL NO NO-UNDO. /* Variavel auxiliar para verifica��o do Registro */
DEF VAR d-limite          AS DATE NO-UNDO.               /* Data limite do registro */
DEF VAR c-empresa-banking AS CHAR NO-UNDO.               /* Empresa do registro encontrado */
DEF VAR l-integra-banking AS LOGICAL INITIAL NO NO-UNDO. /* Verifica se h� integra��o com Banking fut40695 */

/* Banking Connect - Verifica��o do Registro - Inicializando Handle */
IF NOT VALID-HANDLE(v_hdl_reg_produt)
        OR  v_hdl_reg_produt:TYPE       <> "procedure":U
        OR  v_hdl_reg_produt:FILE-NAME  <>  "cdp/cd4210b.p":U
       THEN run cdp/cd4210b.p PERSISTENT SET v_hdl_reg_produt.

/* Verifica se o usu�ri otem persmiss�o para usar o Banking Connect */

    /* Procura o registro */
    run find_registro in v_hdl_reg_produt (OUTPUT l-registro-ok, OUTPUT c-empresa-banking).

    /* Se encontrar verifica a data limite */
    IF l-registro-ok then do:
        RUN retorna_data_limite IN v_hdl_reg_produt (OUTPUT d-limite).
        
        /* Se a data Limite � valida */
        assign l-banking-connect = TODAY <= d-limite.

        /* Mensagem 30 dias antes do prazo do termino da licen�a */
        IF ((d-limite - 30) <= TODAY) and (today <= d-limite) then do:
            run utp/ut-msgs.p (input "show",
                               input 31436,
                               input STRING(d-limite - TODAY)).
        end.

        /* Mensagem de vencimento do Banking */
        if (today > d-limite) then do:
            run utp/ut-msgs.p (input "show",
                               input 31456,
                               input STRING(d-limite - TODAY)).
        end.
    end.
.


assign de-hea-val = 0
       i-hea-qua  = 0.

/*for each tt-digita:*/
/* Banking Connect "for each tt-digita:" */
for each tt-digita no-lock
break by tt-digita.i-tipo-pagto
      by tt-digita.dt-vencimen:
      
    assign da-venci = tt-digita.dt-vencimen.
    
    if first-of(tt-digita.dt-vencimen) then do:
        disp da-venci with frame f-venci.
    end.

    l-integra-banking = NO.
    
        l-integra-banking = (portador.log-4 = yes) and (l-banking-connect = yes).
    .
        
    if l-integra-banking then
        assign i-tipo-pag = tt-digita.i-tipo-pagto.
    else
    do:
        if l-varios-pagtos 
        and i-op-banco = 237 then do:
            assign i-tipo-pag = tt-digita.i-tipo-pagto.
        end.
        else do:
            assign i-tipo-pag = tt-param.i-tipo-pag.
        end.
    end.
    
    /* ---- Antecipacoes ---- */
    find first bordero-ap where rowid(bordero-ap) = tt-digita.row-mov-ap no-lock no-error.
    if  avail bordero-ap then do:
        assign l-antecipacao = yes.
    end.    
    else do:
    /*--- Documentos Normais ---*/
        find first mov-ap where rowid(mov-ap) = tt-digita.row-mov-ap no-lock no-error.
        if  avail mov-ap then do:
            assign l-antecipacao = no.
        end.
    end.
    
    if  avail mov-ap or avail bordero-ap then do:
        /****************************************************************************
***
***   app/ap1001.i14 - Esta include � usada na ap1001.i3.
***
****************************************************************************/

if  l-antecipacao = no then do:
    find tit-ap of mov-ap no-lock no-error.
    
    if avail tit-ap then do:
        
            assign de-abatimen = tit-ap.vl-abatimento-tot 
                   de-multa    = tit-ap.vl-multa-tot.
        
        
        
        
        
            assign de-vl-saldo = 0.
            find first bord-tit
                 where bord-tit.ep-codigo   = tit-ap.ep-codigo
                 and   bord-tit.cod-estabel = tit-ap.cod-estabel
                 and   bord-tit.cod-esp     = tit-ap.cod-esp
                 and   bord-tit.serie       = tit-ap.serie
                 and   bord-tit.cod-fornec  = tit-ap.cod-fornec
                 and   bord-tit.nr-docto    = tit-ap.nr-docto
                 and   bord-tit.parcela     = tit-ap.parcela
                 and   bord-tit.nr-bordero  = tit-ap.nr-bordero
                 and   bord-tit.portador    = tit-ap.portador 
                 no-lock no-error.
            
            if  avail bord-tit then 
                assign de-vl-saldo          = bord-tit.vl-saldo.
            else 
                assign de-vl-saldo = (if  tit-ap.vl-original = tit-ap.valor-saldo then
                                          tit-ap.vl-original
                                     else 
                                          tit-ap.valor-saldo).
            
            /****************************************************************************
***
***   app/ap1001.i15 - Rotina usada include ap1001.i14.
***
****************************************************************************/

/**********************************************************************************
**
**    Include : CDGFGFIN.i
**
**    Objetivo: Tratamento de Miniflexibiliza��o para a release 2.03
**    OBS: A include gerada no workspace MAN EMS 2.0 (POR) deve estar vazia)
***********************************************************************************/

/* Funcoes da Release 2.02 */

/* Conseguir expedir os objetos da WEB em OS da 2.00.*//*&glob BF_FIN_MOD_E_COLLECT          /* Fun��o Envio de Movtos via e-Collect */ PROJETO CANCELADO */

/* Funcoes da Release 2.03 */

/* Informar nr. de periodos na Consulta de Fluxo de Caixa Gerencial (cb1205)*//* Melhorias Contabiliza��o AN e PEF - Multiplanta *//* Tratar multa e abatimento na Liquida��o do T�tulo *//* Funcao generica, que n�o faz parte do Plano de Produto *//* Localizacao Argentina - Nota de Debito e Credito Propria *//* Cobran�a Centralizada. *//* Modulo de Transferencias Eletronicas Bancarias *//* Seguranca de Produto - Validacao das Sequencias no EMS 2 *//* Retorna Saldo da Previsao *//* Integracao Faturamento e Recebimento com APP/CRP *//* Provisao no Contas a Pagar - para ser liberado de forma especial *//* Tratamento para o Modulo RAC - Controle de Conta de Representantes *//* Controle de Cheques Contas a Receber - F.O. 620.611*/
/* Funcoes da Release 2.04 */

/* API Eliminacao da Tabela NF-VENDOR *//* Relatorio e Consulta de Moedas *//* Alteracao Antecipacao/PEF via Multiplanta *//* Check Inscri��o Estadual E.M.S. 2.0 *//* Informacoes Vendor via Multiplanta na Implantacao de Titulos *//* Layout Cobranca Escritural Banco Mercantil *//* Provis�o no Contas a Pagar *//* Rastreabilidade na Contabilidade *//* Melhorias Envio Pagto Escritural *//* Totaliza por dia Razao FASB,CMCAC e Valor Presente *//* Implantar ou Importar Titulos ou Previs�es sem Validar Portador e Moeda *//* Especifico - Verificar Antecipacoes na Baixa por Cheque Nominal *//* Tabela Progressiva de IR *//* Integracao Magnus I.01 X EMS 2.04 Financeiro *//* Modulo de C�mbio *//* Transacao Unica para o Multiplanta do Contas a Receber *//* Transacao Unica para o Multiplanta do Contas a Pagar *//* Isolamento Programas DCF e DCU (Cadastros de Integracoes Financeira) - Eng. EMS 5 *//* Encontro de Contas AP e CR*//* Cobranca Escritural Banco Santander *//* Funcao generica, para pequenas funcoes. Indica o codigo da release/dicionario *//* Especifico - Incluir o parametro Mantem numero do cheque na reimpress�o de cheques *//* Especifico - Autorizacao de documentos vinculados a um bordero em preparacao *//* Especifico - Considera ou nao o valor realizado das aplicacoes e emprestimos no fluxo de caixa *//* Especifico - Mostrar juros em um tipo de despesa diferente do valor principal *//* Especifico - Ativar/Desativar Conta Contabil por Faixa *//* Emitir Titulos em Aberto/Razao em CMCAC - CR *//* Emitir Titulos em Aberto/Razao em CMCAC - AP *//* Conciliacao Contabil CR em FASB/CMCAC e VP *//* Conciliacao Contabil AP em FASB/CMCAC e VP *//* Validar se o Movimento esta conciliado no Caixa e Bancos *//* Especifico - Configurar nome arquivo pagamento escritural *//* Diario do Patrimonio *//* Parte Fixa da Conta Contabil no Razao *//* Impressao Historico do Emitente *//* Especifico - Valor Minimo de Imposto de Renda *//* Bancos Historicos AP/CR/CB *//* Especifico - Ativa Desativa faixa de contas contabeis no Programa de Consolidacao*//* Especifico - Imprimir no fluxo resumido (operacional) apenas os totais di�rios. *//* Especifico - Inclui op��o de faixa de Subconta *//* Validacao Modulo Multiplanta Financeiro *//* Relatorio Fluxo Gerencial Previsto p/ Compras e Pedidos *//* Integracao EMS2 x Tesouraria do EMS5 *//* Vendor Citibank - Especifico Incorporado *//* C�mbio futuro versus fluxo de caixa e varia��o */ 

find first emitente 
     where emitente.cod-emitente = mov-ap.cod-fornec 
     and   emitente.identific   <> 1 no-lock no-error.

if  avail emitente then do:
    
    if  i-tipo-pag-ant = 5 then
        assign i-tipo-pag = int(emitente.tp-pagto).
    
    assign c-aux = "".
    
    /*-- Rotina para pegar somente os numeros do cgc
         quando o format for x(18) no emitente --*/
    
    do   i-con = 1 to length(emitente.cgc):
         assign c-cgc = substr(emitente.cgc,i-con,1).
         if  can-do("0,1,2,3,4,5,6,7,8,9",c-cgc) then
             assign c-aux = c-aux + c-cgc.
    end.
    
    assign de-inscric   = dec(c-aux)
           c-nome       = fn-free-accent(caps(emitente.nome-emit))
           c-endereco   = fn-free-accent(caps(emitente.endereco))
           c-bairro     = fn-free-accent(caps(emitente.bairro))
           c-cidade     = fn-free-accent(caps(emitente.cidade))
           c-estado     = fn-free-accent(caps(emitente.estado))
           i-cep        = emitente.cep.
end.

assign da-data      = tit-ap.dt-vencimen
       de-valor-ori = de-vl-saldo
       de-valor-oco = tit-ap.vl-desconto.


if  l-antecipacao = no then
    if  tit-ap.dt-desconto >= tt-param.da-pagto then
        assign de-valor-pag = de-valor-ori 
                            - tit-ap.vl-isr
                            - de-abatimen
                            + tit-ap.dec-2
                            + de-multa.
    else                        
        assign de-valor-pag = de-valor-ori 
                            - de-abatimen
                            + tit-ap.dec-2
                            + de-multa.
else
    assign de-valor-pag = de-valor-ori 
                        - tit-ap.vl-isr
                        - de-abatimen
                        + tit-ap.dec-2
                        + de-multa.
                        

if  mov-ap.transacao = 13 
or  mov-ap.transacao = 14 then 
    assign de-valor-oco = tit-ap.vl-desconto
           da-data      = mov-ap.dt-pagamento.
else 
    if  mov-ap.transacao = 15 then 
        assign da-data   = tit-ap.dt-vencimen.

if  tt-param.i-ocorrencia = 1 
or  tt-param.i-ocorrencia = 2 then do:
    /***************************************************************************
**
** AP1001.i8
** Objetivo : Atualizar os campos banco,ag�ncia e c.corrente para remessa.
**
**************************************************************************/

/* Forma de pagto */

if  i-op-banco = 1
or  i-op-banco = 041
or  i-op-banco = 275
or  i-op-banco = 356
or  i-op-banco = 237
or  i-op-banco = 341 
or  i-op-banco = 409
or  i-op-banco = 422
or  i-op-banco = 230
or  i-op-banco = 479
or  i-op-banco = 399
or  i-op-banco = 291
or  i-op-banco = 745 then do:
    assign i-nr-banco = tt-digita.banco
           c-agencia  = trim(tt-digita.agencia)
           c-cc-forn  = trim(tt-digita.cta-corrente).
end.

/* Fim de Include */
          /* atualiza i-nr-banco,c-agencia e c-cc-forn */
    /****************************************************************************
***  ap1001.i9
***  objetivo : Validacoes do banco, agencia e conta corrente.
****************************************************************************/

if   (i-tipo-pag = 1  and i-op-banco = 415) 
or   (i-tipo-pag = 1  and i-op-banco = 237)
or   (i-tipo-pag <> 4 and i-op-banco = 422)
or   (i-tipo-pag = 3  and i-op-banco = 341)
or   (i-tipo-pag = 3  and i-op-banco = 409) then 
    if (i-nr-banco = 0 or c-agencia = "" or c-cc-forn = "") then do:
       assign c-observacao = " Falta Banco, Agencia ou C.Corrente p/pagto.".
       /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp mov-ap.cod-fornec  @ mov-ap.cod-fornec
     mov-ap.cod-estabel @ mov-ap.cod-estabel
     mov-ap.cod-esp     @ mov-ap.cod-esp
     mov-ap.serie       @ mov-ap.serie
     mov-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     mov-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     mov-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     mov-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
       assign l-gerar = no.
       next.
    end.


if   (i-tipo-pag = 2  and i-op-banco = 415)
or   (i-tipo-pag = 1  and i-op-banco = 237)
or   (i-tipo-pag <> 4 and i-op-banco = 422)
or   (i-tipo-pag = 1  and i-op-banco = 341)
or   (i-tipo-pag = 1  and i-op-banco = 409) then
     if (c-agencia = "" or c-cc-forn = "") then do:
        assign c-observacao =
               " Falta Agencia ou C.Corrente p/pagto via C/Corrente".
        /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp mov-ap.cod-fornec  @ mov-ap.cod-fornec
     mov-ap.cod-estabel @ mov-ap.cod-estabel
     mov-ap.cod-esp     @ mov-ap.cod-esp
     mov-ap.serie       @ mov-ap.serie
     mov-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     mov-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     mov-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     mov-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
        assign l-gerar = no.
        next.
     end.

if   (i-tipo-pag = 4  and i-op-banco = 415) 
or   (i-tipo-pag = 1  and i-op-banco = 237)
or   (i-tipo-pag <> 4 and i-op-banco = 422)
or   (i-tipo-pag < 31 and i-op-banco = 341)
or   (i-tipo-pag = 7  and i-op-banco = 409) then do:
     if i-nr-banco = 0 then do:
        assign c-observacao =
               " Falta Banco p/pagto via Cobranca".
        /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp mov-ap.cod-fornec  @ mov-ap.cod-fornec
     mov-ap.cod-estabel @ mov-ap.cod-estabel
     mov-ap.cod-esp     @ mov-ap.cod-esp
     mov-ap.serie       @ mov-ap.serie
     mov-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     mov-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     mov-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     mov-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
        assign l-gerar = no.
        next.
     end.
     if  c-agencia = "" 
     and i-tipo-pag < 30 then do:
        assign c-observacao =
               " Falta Agencia p/pagto via Cobranca".
        /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp mov-ap.cod-fornec  @ mov-ap.cod-fornec
     mov-ap.cod-estabel @ mov-ap.cod-estabel
     mov-ap.cod-esp     @ mov-ap.cod-esp
     mov-ap.serie       @ mov-ap.serie
     mov-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     mov-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     mov-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     mov-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
        assign l-gerar = no.
        next.
     end.
end.

/* Codigo Febraban */

if  i-tipo-pag = 1 and i-op-banco = 341 and i-nr-banco <> 341 then do:
    assign c-observacao = "Credito em conta corrente nao pode ser "
                        + "realizado em banco diferente".
    /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp mov-ap.cod-fornec  @ mov-ap.cod-fornec
     mov-ap.cod-estabel @ mov-ap.cod-estabel
     mov-ap.cod-esp     @ mov-ap.cod-esp
     mov-ap.serie       @ mov-ap.serie
     mov-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     mov-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     mov-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     mov-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
    assign l-gerar = no.
    next.
end.

/* Fim da Include */
   /* montar agencia e cont.corrente conf.lay-out */
end.

/* Busca CEP no cadastro de agencia quando pagto for DOC */
find first agencia 
     where agencia.cod-banco = i-nr-banco 
     and   agencia.agencia   = c-agencia no-lock no-error.

if  avail agencia then
    assign i-cep = string(agencia.cep,"99999999").

assign i-dia          = day(da-data)
       i-mes          = month(da-data)
       c-ano          = substr(string(year(da-data)),3,2)
       de-valor       = de-valor-pag
       de-tot-geral-b = de-tot-geral-b + de-valor-ori
       de-tot-geral-l = de-tot-geral-l + de-valor-pag 
       de-valor-oco   = de-valor-oco * 100
       de-valor-pag   = de-valor-pag * 100.

/* Detalhe do relatorio */
assign c-transacao = /*****************************************************************
**
**      i01ad165.i  - define o indicador transacao
**
******************************************************************/

 
 


/* Implanta��o de documentos *//* Baixa de documentos *//* Pagto extra-fornecedor (d�bito) *//* Pagto extra-fornecedor (cr�dito) *//* Acerto de Valor *//* Substitui��o de Notas por Duplicatas *//* Utilizado na raz�o dos movimentos para gravar o saldo inicial do fornec, n�o � cont�bil*//* Ganhos e perdas FASB *//* Movimento CMCAC *//* Escritural - Cancelar t�tulo *//* Escritural - Sustar cancelamento t�tulo *//* Escritural - Cancelar/Sustar Protesto *//* Escritural - Alterar data de vencimento *//* Escritural - Alterar data de desconto *//* Escritural - Alterar outros dados *//* Nota de d�bito/cr�dito *//* Varia��o cambial a maior *//* Varia��o cambial a menor *//* Utilizado na Argentina para iva-liberado contabiliza��o */
/* Implantaci�n de documentos *//* Baja de documentos *//* Pago extra proveedor (d�bito) *//* Pago extra proveedor (cr�dito) *//* Ajuste de Valor *//* Substituci�n de Notas por Duplicatas *//* Utilizando en la raz�n de los Movimientos para gravar el saldo inicial del proveed, no es contable *//* Ganancias y P�rdidas FASB *//* Movimiento CMCAC *//* Escritural - Cancelar t�tulo *//* Escritural - Cancelaci�n t�tulo *//* Escritural - Cancelar/Sustar Protesta *//* Escritural - Alterar fecha vencimiento *//* Escritural - Alterar fecha descuento *//* Escritural - Alterar otros datos *//* Nota de d�bito/cr�dito *//* Variaci�n cambiar la mayor *//* Variaci�n cambiar la menor *//* Utilizado en la Argentina para IVA-liberado contabilizaci�n */
/* Document Entry *//* Invoice payment *//* Other payments (debit) *//* Other payments (credit) *//* Adjust values *//* Document Replacement by Invoices *//* Used in the transaction  *//* FASB profit/loss *//* FACCAC Trans *//* Special Chrages - Cancel document *//* Spec Chrg - Doc cancel suspension *//* Spec Chrg - Declaration Cancellation *//* Spec Chrg - Modify due date *//* Spec Chrg - Modify discount date *//* Spec Chrg - Modify other data *//* Debit/credit note *//* Greater Ex Rate Variance *//* Low Exchange Variation  *//* Used in Argentina for Accounting Tax-release  */

/***************************************************************************
**  ind11-50.i - define as funcoes de um indicador
**  Para indicadores de 11 a 50 items
**
**  Funcoes disponiveis
**  01: view-as combo-box
**  02: view-as radio-set
**  03: lista com os itens separados por virgula
**  04 n: retorna o item n da lista
**  05: retorna o numero de items da lista
**  06: retorna a posicao do item (numero)
**  07: valores para a propriedade Radio-Buttons de um Radio-Set
***************************************************************************/

/* verifica parametros ****************************************************/

/* &if "{&val21}" <> "" &then
    &message *** ({&file-name}): Para indicadores com mais de 20 este include deve ser alterado !
&endif */

/* &if lookup("{1}", "1,01,2,02,3,03,4,04,5,05,6,06,7,07") = 0 &then
    &message *** ({&file-name}): Parametro incorreto: {1} !
    &message *** Deveria ser: CB,RS, LISTA, NUM ou ITEM n !
&endif */
    

  


/* monta lista de items para LISTA, NUM, ITEM, IND ************************/

          
               
     
               
     
               
     
               
     
               
     
               
     
               
     
               
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     



/* funcao CB (01) *************************************************************/



/* funcao RS (02) *************************************************************/


/* funcao LISTA (03) **********************************************************/


/* funcao Num (05) ************************************************************/


/* funcao Item n (04) *********************************************************/    


     entry(mov-ap.transacao, "IMD,BND,PEF,PEC,AVA,SND,XYZ,GPF,CMCAC,CAN,SUS,CSP,DSV,DSD,ALT,NDC,VC+,VC-,IVA")



/* funcao IND string (06) ****************************************************/



/* valores da propriedade Radio-Buttons de um Radio-Set ***********************/




    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    
































/* fim */
 
/* Fim */


 
       c-docto     = mov-ap.nr-docto + "/" + mov-ap.parcela.

if  line-counter >= 62 then
    page.


    if  l-antecipacao = no then do:
        if  tit-ap.dt-desconto >= tt-param.da-pagto then
            assign de-mul = tit-ap.dec-2 + tit-ap.vl-multa-tot
                   de-aba = tit-ap.vl-desconto + tit-ap.vl-abatimento-tot.
        else           
            assign de-mul = tit-ap.dec-2 + tit-ap.vl-multa-tot
                   de-aba = tit-ap.vl-abatimento-tot.
    end.           
    else do:
        assign de-mul = tit-ap.dec-2
               de-aba = mov-ap.vl-desconto.
    end.           



disp mov-ap.cod-fornec  @ mov-ap.cod-fornec
     mov-ap.cod-estabel @ mov-ap.cod-estabel
     mov-ap.cod-esp     @ mov-ap.cod-esp
     mov-ap.serie       @ mov-ap.serie
     c-docto
     i-tipo-pag
     c-transacao
     da-data
     i-nr-banco
     de-valor
     de-aba @ mov-ap.vl-desconto 
     de-mul @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

if  c-agencia <> "" then
    disp c-agencia with frame f-detalhe.

if  c-cc-forn <> "" then
    disp c-cc-forn with frame f-detalhe.

down with frame f-detalhe.

/* Fim ap1001.i15 */

 
        
    end.        
end.
else do:
    /****************************************************************************
***
***   app/ap1001.i15 - Rotina usada include ap1001.i14.
***
****************************************************************************/

/**********************************************************************************
**
**    Include : CDGFGFIN.i
**
**    Objetivo: Tratamento de Miniflexibiliza��o para a release 2.03
**    OBS: A include gerada no workspace MAN EMS 2.0 (POR) deve estar vazia)
***********************************************************************************/

/* Funcoes da Release 2.02 */

/* Conseguir expedir os objetos da WEB em OS da 2.00.*//*&glob BF_FIN_MOD_E_COLLECT          /* Fun��o Envio de Movtos via e-Collect */ PROJETO CANCELADO */

/* Funcoes da Release 2.03 */

/* Informar nr. de periodos na Consulta de Fluxo de Caixa Gerencial (cb1205)*//* Melhorias Contabiliza��o AN e PEF - Multiplanta *//* Tratar multa e abatimento na Liquida��o do T�tulo *//* Funcao generica, que n�o faz parte do Plano de Produto *//* Localizacao Argentina - Nota de Debito e Credito Propria *//* Cobran�a Centralizada. *//* Modulo de Transferencias Eletronicas Bancarias *//* Seguranca de Produto - Validacao das Sequencias no EMS 2 *//* Retorna Saldo da Previsao *//* Integracao Faturamento e Recebimento com APP/CRP *//* Provisao no Contas a Pagar - para ser liberado de forma especial *//* Tratamento para o Modulo RAC - Controle de Conta de Representantes *//* Controle de Cheques Contas a Receber - F.O. 620.611*/
/* Funcoes da Release 2.04 */

/* API Eliminacao da Tabela NF-VENDOR *//* Relatorio e Consulta de Moedas *//* Alteracao Antecipacao/PEF via Multiplanta *//* Check Inscri��o Estadual E.M.S. 2.0 *//* Informacoes Vendor via Multiplanta na Implantacao de Titulos *//* Layout Cobranca Escritural Banco Mercantil *//* Provis�o no Contas a Pagar *//* Rastreabilidade na Contabilidade *//* Melhorias Envio Pagto Escritural *//* Totaliza por dia Razao FASB,CMCAC e Valor Presente *//* Implantar ou Importar Titulos ou Previs�es sem Validar Portador e Moeda *//* Especifico - Verificar Antecipacoes na Baixa por Cheque Nominal *//* Tabela Progressiva de IR *//* Integracao Magnus I.01 X EMS 2.04 Financeiro *//* Modulo de C�mbio *//* Transacao Unica para o Multiplanta do Contas a Receber *//* Transacao Unica para o Multiplanta do Contas a Pagar *//* Isolamento Programas DCF e DCU (Cadastros de Integracoes Financeira) - Eng. EMS 5 *//* Encontro de Contas AP e CR*//* Cobranca Escritural Banco Santander *//* Funcao generica, para pequenas funcoes. Indica o codigo da release/dicionario *//* Especifico - Incluir o parametro Mantem numero do cheque na reimpress�o de cheques *//* Especifico - Autorizacao de documentos vinculados a um bordero em preparacao *//* Especifico - Considera ou nao o valor realizado das aplicacoes e emprestimos no fluxo de caixa *//* Especifico - Mostrar juros em um tipo de despesa diferente do valor principal *//* Especifico - Ativar/Desativar Conta Contabil por Faixa *//* Emitir Titulos em Aberto/Razao em CMCAC - CR *//* Emitir Titulos em Aberto/Razao em CMCAC - AP *//* Conciliacao Contabil CR em FASB/CMCAC e VP *//* Conciliacao Contabil AP em FASB/CMCAC e VP *//* Validar se o Movimento esta conciliado no Caixa e Bancos *//* Especifico - Configurar nome arquivo pagamento escritural *//* Diario do Patrimonio *//* Parte Fixa da Conta Contabil no Razao *//* Impressao Historico do Emitente *//* Especifico - Valor Minimo de Imposto de Renda *//* Bancos Historicos AP/CR/CB *//* Especifico - Ativa Desativa faixa de contas contabeis no Programa de Consolidacao*//* Especifico - Imprimir no fluxo resumido (operacional) apenas os totais di�rios. *//* Especifico - Inclui op��o de faixa de Subconta *//* Validacao Modulo Multiplanta Financeiro *//* Relatorio Fluxo Gerencial Previsto p/ Compras e Pedidos *//* Integracao EMS2 x Tesouraria do EMS5 *//* Vendor Citibank - Especifico Incorporado *//* C�mbio futuro versus fluxo de caixa e varia��o */ 

find first emitente 
     where emitente.cod-emitente = bordero-ap.cod-fornec 
     and   emitente.identific   <> 1 no-lock no-error.

if  avail emitente then do:
    
    if  i-tipo-pag-ant = 5 then
        assign i-tipo-pag = int(emitente.tp-pagto).
    
    assign c-aux = "".
    
    /*-- Rotina para pegar somente os numeros do cgc
         quando o format for x(18) no emitente --*/
    
    do   i-con = 1 to length(emitente.cgc):
         assign c-cgc = substr(emitente.cgc,i-con,1).
         if  can-do("0,1,2,3,4,5,6,7,8,9",c-cgc) then
             assign c-aux = c-aux + c-cgc.
    end.
    
    assign de-inscric   = dec(c-aux)
           c-nome       = fn-free-accent(caps(emitente.nome-emit))
           c-endereco   = fn-free-accent(caps(emitente.endereco))
           c-bairro     = fn-free-accent(caps(emitente.bairro))
           c-cidade     = fn-free-accent(caps(emitente.cidade))
           c-estado     = fn-free-accent(caps(emitente.estado))
           i-cep        = emitente.cep.
end.

assign da-data      = bordero-ap.dt-vencto
       de-valor-ori = bordero-ap.vl-docto
       de-valor-oco = 0.


if  l-antecipacao = no then
    if  tit-ap.dt-desconto >= tt-param.da-pagto then
        assign de-valor-pag = de-valor-ori 
                            - 0
                            - 0
                            + 0
                            + 0.
    else                        
        assign de-valor-pag = de-valor-ori 
                            - 0
                            + 0
                            + 0.
else
    assign de-valor-pag = de-valor-ori 
                        - 0
                        - 0
                        + 0
                        + 0.
                        

if  bordero-ap.transacao = 13 
or  bordero-ap.transacao = 14 then 
    assign de-valor-oco = 0
           da-data      = bordero-ap.dt-transacao.
else 
    if  bordero-ap.transacao = 15 then 
        assign da-data   = bordero-ap.dt-vencto.

if  tt-param.i-ocorrencia = 1 
or  tt-param.i-ocorrencia = 2 then do:
    /***************************************************************************
**
** AP1001.i8
** Objetivo : Atualizar os campos banco,ag�ncia e c.corrente para remessa.
**
**************************************************************************/

/* Forma de pagto */

if  i-op-banco = 1
or  i-op-banco = 041
or  i-op-banco = 275
or  i-op-banco = 356
or  i-op-banco = 237
or  i-op-banco = 341 
or  i-op-banco = 409
or  i-op-banco = 422
or  i-op-banco = 230
or  i-op-banco = 479
or  i-op-banco = 399
or  i-op-banco = 291
or  i-op-banco = 745 then do:
    assign i-nr-banco = tt-digita.banco
           c-agencia  = trim(tt-digita.agencia)
           c-cc-forn  = trim(tt-digita.cta-corrente).
end.

/* Fim de Include */
          /* atualiza i-nr-banco,c-agencia e c-cc-forn */
    /****************************************************************************
***  ap1001.i9
***  objetivo : Validacoes do banco, agencia e conta corrente.
****************************************************************************/

if   (i-tipo-pag = 1  and i-op-banco = 415) 
or   (i-tipo-pag = 1  and i-op-banco = 237)
or   (i-tipo-pag <> 4 and i-op-banco = 422)
or   (i-tipo-pag = 3  and i-op-banco = 341)
or   (i-tipo-pag = 3  and i-op-banco = 409) then 
    if (i-nr-banco = 0 or c-agencia = "" or c-cc-forn = "") then do:
       assign c-observacao = " Falta Banco, Agencia ou C.Corrente p/pagto.".
       /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp bordero-ap.cod-fornec  @ mov-ap.cod-fornec
     bordero-ap.cod-estabel @ mov-ap.cod-estabel
     bordero-ap.cod-esp     @ mov-ap.cod-esp
     bordero-ap.serie       @ mov-ap.serie
     bordero-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     bordero-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     bordero-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     bordero-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
       assign l-gerar = no.
       next.
    end.


if   (i-tipo-pag = 2  and i-op-banco = 415)
or   (i-tipo-pag = 1  and i-op-banco = 237)
or   (i-tipo-pag <> 4 and i-op-banco = 422)
or   (i-tipo-pag = 1  and i-op-banco = 341)
or   (i-tipo-pag = 1  and i-op-banco = 409) then
     if (c-agencia = "" or c-cc-forn = "") then do:
        assign c-observacao =
               " Falta Agencia ou C.Corrente p/pagto via C/Corrente".
        /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp bordero-ap.cod-fornec  @ mov-ap.cod-fornec
     bordero-ap.cod-estabel @ mov-ap.cod-estabel
     bordero-ap.cod-esp     @ mov-ap.cod-esp
     bordero-ap.serie       @ mov-ap.serie
     bordero-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     bordero-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     bordero-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     bordero-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
        assign l-gerar = no.
        next.
     end.

if   (i-tipo-pag = 4  and i-op-banco = 415) 
or   (i-tipo-pag = 1  and i-op-banco = 237)
or   (i-tipo-pag <> 4 and i-op-banco = 422)
or   (i-tipo-pag < 31 and i-op-banco = 341)
or   (i-tipo-pag = 7  and i-op-banco = 409) then do:
     if i-nr-banco = 0 then do:
        assign c-observacao =
               " Falta Banco p/pagto via Cobranca".
        /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp bordero-ap.cod-fornec  @ mov-ap.cod-fornec
     bordero-ap.cod-estabel @ mov-ap.cod-estabel
     bordero-ap.cod-esp     @ mov-ap.cod-esp
     bordero-ap.serie       @ mov-ap.serie
     bordero-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     bordero-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     bordero-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     bordero-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
        assign l-gerar = no.
        next.
     end.
     if  c-agencia = "" 
     and i-tipo-pag < 30 then do:
        assign c-observacao =
               " Falta Agencia p/pagto via Cobranca".
        /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp bordero-ap.cod-fornec  @ mov-ap.cod-fornec
     bordero-ap.cod-estabel @ mov-ap.cod-estabel
     bordero-ap.cod-esp     @ mov-ap.cod-esp
     bordero-ap.serie       @ mov-ap.serie
     bordero-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     bordero-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     bordero-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     bordero-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
        assign l-gerar = no.
        next.
     end.
end.

/* Codigo Febraban */

if  i-tipo-pag = 1 and i-op-banco = 341 and i-nr-banco <> 341 then do:
    assign c-observacao = "Credito em conta corrente nao pode ser "
                        + "realizado em banco diferente".
    /*********************************************************************
***  ap1001.i5
***  Objetivo: Impressao da mensagem de observacao.
**********************************************************************/

disp bordero-ap.cod-fornec  @ mov-ap.cod-fornec
     bordero-ap.cod-estabel @ mov-ap.cod-estabel
     bordero-ap.cod-esp     @ mov-ap.cod-esp
     bordero-ap.serie       @ mov-ap.serie
     bordero-ap.nr-docto    @ mov-ap.nr-docto
     c-traco
     bordero-ap.parcela     @ mov-ap.parcela
     /*{2}             @ mov-ap.dt-emissao*/
     bordero-ap.transacao   @ mov-ap.transacao
     da-data
     i-tipo-pag
     c-agencia
     i-nr-banco
     c-cc-forn
     de-valor
     bordero-ap.vl-desconto @ mov-ap.vl-desconto
     with frame f-detalhe.

if  avail tit-ap then
    disp tit-ap.dec-2  @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

down with frame f-detalhe.

disp c-observacao
     with frame f-observacao.

down with frame f-observacao.

/* Fim da Include */
 
    assign l-gerar = no.
    next.
end.

/* Fim da Include */
   /* montar agencia e cont.corrente conf.lay-out */
end.

/* Busca CEP no cadastro de agencia quando pagto for DOC */
find first agencia 
     where agencia.cod-banco = i-nr-banco 
     and   agencia.agencia   = c-agencia no-lock no-error.

if  avail agencia then
    assign i-cep = string(agencia.cep,"99999999").

assign i-dia          = day(da-data)
       i-mes          = month(da-data)
       c-ano          = substr(string(year(da-data)),3,2)
       de-valor       = de-valor-pag
       de-tot-geral-b = de-tot-geral-b + de-valor-ori
       de-tot-geral-l = de-tot-geral-l + de-valor-pag 
       de-valor-oco   = de-valor-oco * 100
       de-valor-pag   = de-valor-pag * 100.

/* Detalhe do relatorio */
assign c-transacao = /*****************************************************************
**
**      i01ad165.i  - define o indicador transacao
**
******************************************************************/

 
 


/* Implanta��o de documentos *//* Baixa de documentos *//* Pagto extra-fornecedor (d�bito) *//* Pagto extra-fornecedor (cr�dito) *//* Acerto de Valor *//* Substitui��o de Notas por Duplicatas *//* Utilizado na raz�o dos movimentos para gravar o saldo inicial do fornec, n�o � cont�bil*//* Ganhos e perdas FASB *//* Movimento CMCAC *//* Escritural - Cancelar t�tulo *//* Escritural - Sustar cancelamento t�tulo *//* Escritural - Cancelar/Sustar Protesto *//* Escritural - Alterar data de vencimento *//* Escritural - Alterar data de desconto *//* Escritural - Alterar outros dados *//* Nota de d�bito/cr�dito *//* Varia��o cambial a maior *//* Varia��o cambial a menor *//* Utilizado na Argentina para iva-liberado contabiliza��o */
/* Implantaci�n de documentos *//* Baja de documentos *//* Pago extra proveedor (d�bito) *//* Pago extra proveedor (cr�dito) *//* Ajuste de Valor *//* Substituci�n de Notas por Duplicatas *//* Utilizando en la raz�n de los Movimientos para gravar el saldo inicial del proveed, no es contable *//* Ganancias y P�rdidas FASB *//* Movimiento CMCAC *//* Escritural - Cancelar t�tulo *//* Escritural - Cancelaci�n t�tulo *//* Escritural - Cancelar/Sustar Protesta *//* Escritural - Alterar fecha vencimiento *//* Escritural - Alterar fecha descuento *//* Escritural - Alterar otros datos *//* Nota de d�bito/cr�dito *//* Variaci�n cambiar la mayor *//* Variaci�n cambiar la menor *//* Utilizado en la Argentina para IVA-liberado contabilizaci�n */
/* Document Entry *//* Invoice payment *//* Other payments (debit) *//* Other payments (credit) *//* Adjust values *//* Document Replacement by Invoices *//* Used in the transaction  *//* FASB profit/loss *//* FACCAC Trans *//* Special Chrages - Cancel document *//* Spec Chrg - Doc cancel suspension *//* Spec Chrg - Declaration Cancellation *//* Spec Chrg - Modify due date *//* Spec Chrg - Modify discount date *//* Spec Chrg - Modify other data *//* Debit/credit note *//* Greater Ex Rate Variance *//* Low Exchange Variation  *//* Used in Argentina for Accounting Tax-release  */

/***************************************************************************
**  ind11-50.i - define as funcoes de um indicador
**  Para indicadores de 11 a 50 items
**
**  Funcoes disponiveis
**  01: view-as combo-box
**  02: view-as radio-set
**  03: lista com os itens separados por virgula
**  04 n: retorna o item n da lista
**  05: retorna o numero de items da lista
**  06: retorna a posicao do item (numero)
**  07: valores para a propriedade Radio-Buttons de um Radio-Set
***************************************************************************/

/* verifica parametros ****************************************************/

/* &if "{&val21}" <> "" &then
    &message *** ({&file-name}): Para indicadores com mais de 20 este include deve ser alterado !
&endif */

/* &if lookup("{1}", "1,01,2,02,3,03,4,04,5,05,6,06,7,07") = 0 &then
    &message *** ({&file-name}): Parametro incorreto: {1} !
    &message *** Deveria ser: CB,RS, LISTA, NUM ou ITEM n !
&endif */
    

  


/* monta lista de items para LISTA, NUM, ITEM, IND ************************/

          
               
     
               
     
               
     
               
     
               
     
               
     
               
     
               
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     



/* funcao CB (01) *************************************************************/



/* funcao RS (02) *************************************************************/


/* funcao LISTA (03) **********************************************************/


/* funcao Num (05) ************************************************************/


/* funcao Item n (04) *********************************************************/    


     entry(bordero-ap.transacao, "IMD,BND,PEF,PEC,AVA,SND,XYZ,GPF,CMCAC,CAN,SUS,CSP,DSV,DSD,ALT,NDC,VC+,VC-,IVA")



/* funcao IND string (06) ****************************************************/



/* valores da propriedade Radio-Buttons de um Radio-Set ***********************/




    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    
































/* fim */
 
/* Fim */


 
       c-docto     = bordero-ap.nr-docto + "/" + bordero-ap.parcela.

if  line-counter >= 62 then
    page.


    if  l-antecipacao = no then do:
        if  tit-ap.dt-desconto >= tt-param.da-pagto then
            assign de-mul = 0 + tit-ap.vl-multa-tot
                   de-aba = tit-ap.vl-desconto + tit-ap.vl-abatimento-tot.
        else           
            assign de-mul = 0 + tit-ap.vl-multa-tot
                   de-aba = tit-ap.vl-abatimento-tot.
    end.           
    else do:
        assign de-mul = 0
               de-aba = bordero-ap.vl-desconto.
    end.           



disp bordero-ap.cod-fornec  @ mov-ap.cod-fornec
     bordero-ap.cod-estabel @ mov-ap.cod-estabel
     bordero-ap.cod-esp     @ mov-ap.cod-esp
     bordero-ap.serie       @ mov-ap.serie
     c-docto
     i-tipo-pag
     c-transacao
     da-data
     i-nr-banco
     de-valor
     de-aba @ mov-ap.vl-desconto 
     de-mul @ mov-ap.vl-juro-fasb
     with frame f-detalhe.

if  avail emitente then
    disp emitente.nome-abrev @ mov-ap.nome-abrev with frame f-detalhe.

if  c-agencia <> "" then
    disp c-agencia with frame f-detalhe.

if  c-cc-forn <> "" then
    disp c-cc-forn with frame f-detalhe.

down with frame f-detalhe.

/* Fim ap1001.i15 */

 
end.

/*--- Registro Detalhe do Arquivo Remessa ---*/
if tt-param.i-ocorrencia = 1 
or tt-param.i-ocorrencia = 2 then do:

    assign vl-tot-impto = 0.
    if  l-antecipacao = no then do:
        for each imposto-pagto no-lock
           where imposto-pagto.ep-codigo   = tit-ap.ep-codigo  
           and   imposto-pagto.cod-estabel = tit-ap.cod-estabel
           and   imposto-pagto.cod-fornec  = tit-ap.cod-fornec 
           and   imposto-pagto.cod-esp     = tit-ap.cod-esp    
           and   imposto-pagto.serie       = tit-ap.serie      
           and   imposto-pagto.nr-docto    = tit-ap.nr-docto   
           and   imposto-pagto.parcela     = tit-ap.parcela:           
             assign vl-tot-impto = vl-tot-impto + imposto-pagto.vl-imposto.
        end.
    end.
    else do:
        for each imposto-pagto no-lock
           where imposto-pagto.ep-codigo   = bordero-ap.ep-codigo  
           and   imposto-pagto.cod-estabel = bordero-ap.cod-estabel
           and   imposto-pagto.cod-esp     = bordero-ap.cod-esp    
           and   imposto-pagto.serie       = bordero-ap.serie      
           and   imposto-pagto.nr-docto    = bordero-ap.nr-docto   
           and   imposto-pagto.parcela     = bordero-ap.parcela    
           and   imposto-pagto.cod-fornec  = bordero-ap.cod-fornec:      
             assign vl-tot-impto = vl-tot-impto + imposto-pagto.vl-imposto.
        end.
    end.    

   /***************************************************************************
***
***  ap1001m.i2 
***  Objetivo: Gerar detalhe para banco REAL(remessa).
***
***************************************************************************/

if  l-antecipacao = no then do:
            
    if  mov-ap.transacao = 15 then
        assign c-tipo-mov = "S".
            
    if  mov-ap.transacao = 10 
    or  mov-ap.transacao = 11 
    or  mov-ap.transacao = 12 then
        assign c-tipo-mov = "E".
        
    if  mov-ap.transacao = 1 
    or  mov-ap.transacao = 6 then
        assign c-tipo-mov = "I".
             
    assign de-desconto = tit-ap.vl-isr * 100
           de-juros    = tit-ap.dec-2  * 100.
        
    /***************************************************************************
***
***   app/ap1001m.i3 - Include usada na ap1001m.i2.
***
***************************************************************************/
assign i-tipo-pag-ant = i-tipo-pag.

if  tt-param.i-tipo-pag = 1 then
    assign i-tipo-pag = 2.
else
    if  tt-param.i-tipo-pag = 2 then
        assign i-tipo-pag = 1.
    else
        if  tt-param.i-tipo-pag = 3 then
            assign i-tipo-pag = 4.
        else
            if  tt-param.i-tipo-pag = 4 then
                assign i-tipo-pag = 5.
            else
                if  tt-param.i-tipo-pag = 83 then
                    assign i-tipo-pag = 4.
                else
                    if  tt-param.i-tipo-pag = 30
                    or  tt-param.i-tipo-pag = 31 then
                        assign i-tipo-pag = 6.

assign c-carteira      = tt-digita.ita-carteira
       l-aceite        = tt-digita.ita-aceite
       c-uso-banco-ita = tt-digita.ita-uso-banco.

find first emitente
     where emitente.cod-emitente = tit-ap.cod-fornec no-lock no-error.

if  avail emitente then
    assign i-natureza = if  emitente.natureza = 1 then 
                            2
                        else
                            if  emitente.natureza = 2 then
                                1
                            else 
                                 99.

assign i-hea-qua = if i-hea-qua = 0 then 1 else i-hea-qua.

/* Registro tipo 1 - Transacao pagto */

assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,07).
run pi-retira-caracter (input-output c-ctaCorren).
assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,7)),"9999999").

assign c-agen   = substr(cta-corren.agencia-extrato,3,04).
run pi-retira-caracter (input-output c-agen).
assign c-agen  = string(dec(substr(c-agen,1,4)),"9999").

if substr(cta-corrente.conta-corren-extr,11,1) = "" then
    assign c-cta-corren-ext = "0".
else
    assign c-cta-corren-ext = substr(cta-corrente.conta-corren-extr,11,1).

put stream relat
    "1"                                                          at 001
    c-tipo-mov                                                   at 002
    "PG  "                                                       at 003
    /* SEEBER FASTPLAS - VALERIA - 10/05/2010 AQUI */
    /* campo codigo de liberacao  - 1 liberado 2 - n�o liberado */
    /* alterado para atender solicita��o do financeiro para os 
    pagamentos irem como n�o liberados*/
    /*"1"*/
    "2"                                                          at 007
    substr(c-agen,1,4)                          format "9999"    at 008
    substr(cta-corrente.agencia-extrato,7,1)    format "9"       at 012
    substr(c-ctaCorren,1,7)                     format "9999999" at 013
    c-cta-corren-ext                            format "9"       at 020
    "DUP"                                                        at 021
    string(mov-ap.num-id-mov-ap)                                 format "x(11)"   at 024.



if  tit-ap.dt-vencimen < today then
    put stream relat
    day(tt-param.da-pagto)                             format "99" at 039
    month(tt-param.da-pagto)                           format "99" at 041
    substr(string(year(tt-param.da-pagto),"9999"),3,2) format "99" at 043
    "0"                                                            at 045.
else    
    put stream relat
    day(tit-ap.dt-vencimen)                             format "99" at 039
    month(tit-ap.dt-vencimen)                           format "99" at 041
    substr(string(year(tit-ap.dt-vencimen),"9999"),3,2) format "99" at 043
    "0"                                              at 045.

if  de-valor-pag = 0 then
    put stream relat
    (tit-ap.valor-saldo - vl-tot-impto) * 100 format "999999999999999" at 046.
else
    put stream relat
    de-valor-pag format "999999999999999" at 046.
put stream relat
    "398"                                       at 061
    "PG.FORNECEDOR                 "            at 064
    i-tipo-pag                       format "9" at 094.

    if  i-tipo-pag = 1 then
        put stream relat "1" at 095.
    else
        put stream relat "0" at 095.

/*assign de-hea-val = de-hea-val + {3}.*/

if  l-spp-escrit-oracle = no then do:
    put stream relat
        "000000000000000000000000000000"                         at 096
        tit-ap.cod-fornec                   format "999999999"      at 126
        "0000"                           format "9999"           at 135
        i-natureza                       format "99"             at 139
        c-aux                            format "99999999999999" at 141.
end.
else do:
    put stream relat
        "0000000000000000000000000"                                  at 096
        mov-ap.num-id-mov-ap                              format "999999999999999999" at 121
        i-natureza                       format "99"                 at 139
        c-aux                            format "99999999999999"     at 141.
end.

if  i-tipo-pag = 4 then do:
   put stream relat
        tt-digita.banco format "999" at 155.
end.
else
    put stream relat "000" format "999" at 155.

if  i-tipo-pag = 1 or i-tipo-pag = 2
or  i-tipo-pag = 4 or i-tipo-pag = 5 then do:
    assign c-ctaCorren  = substr(tt-digita.cta-corrente,1,10).
    run pi-retira-caracter (input-output c-ctaCorren).
    assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,10)),"9999999999").

    assign c-digito     = substr(tt-digita.cta-corrente,11,2).

    assign c-agen   = substr(tt-digita.agencia,2,5).
    run pi-retira-caracter (input-output c-agen).
    assign c-agen  = string(dec(substr(c-agen,1,5)),"99999").

    put stream relat 
        substr(c-agen,1,5)            format "99999"       at 158
        substr(tt-digita.agencia,7,1) format "9"           at 163
        substr(c-ctaCorren,1,10)      format "9999999999"  at 164.
    if length(trim(c-digito)) < 2 then 
       put stream relat  
           substr(c-digito,1,1)       FORMAT "9"           at 174.
    ELSE
      put stream relat  
          " "                         FORMAT "9"           at 174.       

    find first agencia
         where agencia.cod-banco = i-nr-banco
         and   agencia.agencia   = c-agencia  no-lock no-error.

    if  avail agencia then
        put stream relat
            agencia.nome format "x(30)" at 175.
end.

if  i-tipo-pag > 5 then do:
    put stream relat 
        "00000"      at 158
        "0000000000" at 164.
end.

put stream relat 
    "000000000" at 205.

if  i-tipo-pag = 1 or i-tipo-pag = 4
or  i-tipo-pag = 5 or i-tipo-pag = 6 then do:
    put stream relat
        c-nome     format "x(40)"    at 214
        c-endereco format "x(30)"    at 254
        c-bairro   format "x(15)"    at 299
        i-cep      format "99999999" at 314
        c-cidade   format "x(20)"    at 322
        c-estado   format "!!"       at 342.
end.

put stream relat 
    mov-ap.num-id-mov-ap                                  format "99999999999" at 344
    day(mov-ap.dt-transacao)                             format "99"          at 355  
    month(mov-ap.dt-transacao)                           format "99"          at 357
    substr(string(year(mov-ap.dt-transacao),"9999"),3,2) format "99"          at 359
    "000000000"                                               at 362.

if tt-param.i-tipo-pag = 3 then do:
    put stream relat
       "700" at 371.
end.
else do:
    if tt-param.i-tipo-pag = 83 then do:
        put stream relat
            "018" at 371.
    end.
    else do:
        put stream relat
            "000" at 371.
    end.
end.
put stream relat                     
    " "                                  at 374.

if length(trim(c-digito)) = 2 then 
   put stream relat  
       substr(c-digito,1,2)  Format "99" at 375.
else 
   put stream relat
    "  "                     Format "99" at 375.       

put stream relat
    "000000000000000000"                 at 377
    i-hea-qua + 1        format "999999" at 395.

/* Registro tipo 4 - codigo de barras */
if i-tipo-pag = 6 then do:
    assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,7).
    run pi-retira-caracter (input-output c-ctaCorren).
    assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,7)),"9999999").

    assign c-agen   = substr(cta-corrente.agencia-extrato,3,4).
    run pi-retira-caracter (input-output c-agen).
    assign c-agen  = string(dec(substr(c-agen,1,4)),"9999").
    put stream relat
        skip
        "4"                                                       at 001
        "I"                                                       at 002
        "PG   "                                                   at 003
        dec(substr(c-agen,1,4))                  format "9999"    at 008
        substr(cta-corrente.agencia-extrato,7,1) format "9"       at 012
        dec(substr(c-ctaCorren,1,7))             format "9999999" at 013
        c-cta-corren-ext                         format "9"       at 020.

     if  tt-digita.log-leitora = yes then
        put stream relat
            tt-digita.c-cod-barras format "x(44)" at 021.
    ELSE
        put stream relat
            tt-digita.c-cod-barras format "x(47)" at 065.

    put stream relat
        tt-digita.titulo-banco format "x(20)"  at 155
        i-hea-qua + 2          format "999999" at 395.

    assign i-hea-qua = i-hea-qua + 1.
end.

/* Fim de Include */
  /* Devido ao Banco real ter a informa��o de registro caracter foi
                                               necess�ria a inclus�o de mais um par�metro para a passagem de
                                               um campo n�merico */
                    
        
end.
else do:
    
     assign c-tipo-mov  = "I"
            de-desconto = 0
            de-juros    = 0.
            
    /***************************************************************************
***
***   app/ap1001m.i3 - Include usada na ap1001m.i2.
***
***************************************************************************/
assign i-tipo-pag-ant = i-tipo-pag.

if  tt-param.i-tipo-pag = 1 then
    assign i-tipo-pag = 2.
else
    if  tt-param.i-tipo-pag = 2 then
        assign i-tipo-pag = 1.
    else
        if  tt-param.i-tipo-pag = 3 then
            assign i-tipo-pag = 4.
        else
            if  tt-param.i-tipo-pag = 4 then
                assign i-tipo-pag = 5.
            else
                if  tt-param.i-tipo-pag = 83 then
                    assign i-tipo-pag = 4.
                else
                    if  tt-param.i-tipo-pag = 30
                    or  tt-param.i-tipo-pag = 31 then
                        assign i-tipo-pag = 6.

assign c-carteira      = tt-digita.ita-carteira
       l-aceite        = tt-digita.ita-aceite
       c-uso-banco-ita = tt-digita.ita-uso-banco.

find first emitente
     where emitente.cod-emitente = bordero-ap.cod-fornec no-lock no-error.

if  avail emitente then
    assign i-natureza = if  emitente.natureza = 1 then 
                            2
                        else
                            if  emitente.natureza = 2 then
                                1
                            else 
                                 99.

assign i-hea-qua = if i-hea-qua = 0 then 1 else i-hea-qua.

/* Registro tipo 1 - Transacao pagto */

assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,07).
run pi-retira-caracter (input-output c-ctaCorren).
assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,7)),"9999999").

assign c-agen   = substr(cta-corren.agencia-extrato,3,04).
run pi-retira-caracter (input-output c-agen).
assign c-agen  = string(dec(substr(c-agen,1,4)),"9999").

if substr(cta-corrente.conta-corren-extr,11,1) = "" then
    assign c-cta-corren-ext = "0".
else
    assign c-cta-corren-ext = substr(cta-corrente.conta-corren-extr,11,1).

put stream relat
    "1"                                                          at 001
    c-tipo-mov                                                   at 002
    "PG  "                                                       at 003
    "1"                                                          at 007
    substr(c-agen,1,4)                          format "9999"    at 008
    substr(cta-corrente.agencia-extrato,7,1)    format "9"       at 012
    substr(c-ctaCorren,1,7)                     format "9999999" at 013
    c-cta-corren-ext                            format "9"       at 020
    "DUP"                                                        at 021
    string(bordero-ap.char-2)                                 format "x(11)"   at 024.



if  bordero-ap.dt-vencto < today then
    put stream relat
    day(tt-param.da-pagto)                             format "99" at 039
    month(tt-param.da-pagto)                           format "99" at 041
    substr(string(year(tt-param.da-pagto),"9999"),3,2) format "99" at 043
    "0"                                                            at 045.
else    
    put stream relat
    day(bordero-ap.dt-vencto)                             format "99" at 039
    month(bordero-ap.dt-vencto)                           format "99" at 041
    substr(string(year(bordero-ap.dt-vencto),"9999"),3,2) format "99" at 043
    "0"                                              at 045.

if  de-valor-pag = 0 then
    put stream relat
    (bordero-ap.vl-docto - vl-tot-impto) * 100 format "999999999999999" at 046.
else
    put stream relat
    de-valor-pag format "999999999999999" at 046.
put stream relat
    "398"                                       at 061
    "PG.FORNECEDOR                 "            at 064
    i-tipo-pag                       format "9" at 094.

    if  i-tipo-pag = 1 then
        put stream relat "1" at 095.
    else
        put stream relat "0" at 095.

/*assign de-hea-val = de-hea-val + {3}.*/

if  l-spp-escrit-oracle = no then do:
    put stream relat
        "000000000000000000000000000000"                         at 096
        bordero-ap.cod-fornec                   format "999999999"      at 126
        "0000"                           format "9999"           at 135
        i-natureza                       format "99"             at 139
        c-aux                            format "99999999999999" at 141.
end.
else do:
    put stream relat
        "0000000000000000000000000"                                  at 096
        recid(bordero-ap)                              format "999999999999999999" at 121
        i-natureza                       format "99"                 at 139
        c-aux                            format "99999999999999"     at 141.
end.

if  i-tipo-pag = 4 then do:
   put stream relat
        tt-digita.banco format "999" at 155.
end.
else
    put stream relat "000" format "999" at 155.

if  i-tipo-pag = 1 or i-tipo-pag = 2
or  i-tipo-pag = 4 or i-tipo-pag = 5 then do:
    assign c-ctaCorren  = substr(tt-digita.cta-corrente,1,10).
    run pi-retira-caracter (input-output c-ctaCorren).
    assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,10)),"9999999999").

    assign c-digito     = substr(tt-digita.cta-corrente,11,2).

    assign c-agen   = substr(tt-digita.agencia,2,5).
    run pi-retira-caracter (input-output c-agen).
    assign c-agen  = string(dec(substr(c-agen,1,5)),"99999").

    put stream relat 
        substr(c-agen,1,5)            format "99999"       at 158
        substr(tt-digita.agencia,7,1) format "9"           at 163
        substr(c-ctaCorren,1,10)      format "9999999999"  at 164.
    if length(trim(c-digito)) < 2 then 
       put stream relat  
           substr(c-digito,1,1)       FORMAT "9"           at 174.
    ELSE
      put stream relat  
          " "                         FORMAT "9"           at 174.       

    find first agencia
         where agencia.cod-banco = i-nr-banco
         and   agencia.agencia   = c-agencia  no-lock no-error.

    if  avail agencia then
        put stream relat
            agencia.nome format "x(30)" at 175.
end.

if  i-tipo-pag > 5 then do:
    put stream relat 
        "00000"      at 158
        "0000000000" at 164.
end.

put stream relat 
    "000000000" at 205.

if  i-tipo-pag = 1 or i-tipo-pag = 4
or  i-tipo-pag = 5 or i-tipo-pag = 6 then do:
    put stream relat
        c-nome     format "x(40)"    at 214
        c-endereco format "x(30)"    at 254
        c-bairro   format "x(15)"    at 299
        i-cep      format "99999999" at 314
        c-cidade   format "x(20)"    at 322
        c-estado   format "!!"       at 342.
end.

put stream relat 
    recid(bordero-ap)                                  format "99999999999" at 344
    day(bordero-ap.dt-transacao)                             format "99"          at 355  
    month(bordero-ap.dt-transacao)                           format "99"          at 357
    substr(string(year(bordero-ap.dt-transacao),"9999"),3,2) format "99"          at 359
    "000000000"                                               at 362.

if tt-param.i-tipo-pag = 3 then do:
    put stream relat
       "700" at 371.
end.
else do:
    if tt-param.i-tipo-pag = 83 then do:
        put stream relat
            "018" at 371.
    end.
    else do:
        put stream relat
            "000" at 371.
    end.
end.
put stream relat                     
    " "                                  at 374.

if length(trim(c-digito)) = 2 then 
   put stream relat  
       substr(c-digito,1,2)  Format "99" at 375.
else 
   put stream relat
    "  "                     Format "99" at 375.       

put stream relat
    "000000000000000000"                 at 377
    i-hea-qua + 1        format "999999" at 395.

/* Registro tipo 4 - codigo de barras */
if i-tipo-pag = 6 then do:
    assign c-ctaCorren  = substr(cta-corrente.conta-corren-extr,4,7).
    run pi-retira-caracter (input-output c-ctaCorren).
    assign c-ctaCorren  = string(dec(substr(c-ctaCorren,1,7)),"9999999").

    assign c-agen   = substr(cta-corrente.agencia-extrato,3,4).
    run pi-retira-caracter (input-output c-agen).
    assign c-agen  = string(dec(substr(c-agen,1,4)),"9999").
    put stream relat
        skip
        "4"                                                       at 001
        "I"                                                       at 002
        "PG   "                                                   at 003
        dec(substr(c-agen,1,4))                  format "9999"    at 008
        substr(cta-corrente.agencia-extrato,7,1) format "9"       at 012
        dec(substr(c-ctaCorren,1,7))             format "9999999" at 013
        c-cta-corren-ext                         format "9"       at 020.

     if  tt-digita.log-leitora = yes then
        put stream relat
            tt-digita.c-cod-barras format "x(44)" at 021.
    ELSE
        put stream relat
            tt-digita.c-cod-barras format "x(47)" at 065.

    put stream relat
        tt-digita.titulo-banco format "x(20)"  at 155
        i-hea-qua + 2          format "999999" at 395.

    assign i-hea-qua = i-hea-qua + 1.
end.

/* Fim de Include */
  /* Devido ao Banco real ter a informa��o de registro caracter foi 
                                          necess�ria a inclus�o de mais um par�metro para a passagem de  
                                          um campo n�merico */                                           

    
end.            
    
/* Fim de Include */


 
end.
else do:
   /**************************************************************************
** ap1001.i7
** Objetivo : Definicao dos parametros especificos dos bancos.
**************************************************************************/

def  shared frame f-sel-bordero.
form
    "Bordero........:"
    i-bord-ini
    i-bord-fim
    with stream-io overlay row 18 column 35 no-labels title "Selecao"
    frame f-sel-bordero.

def  shared frame f-sel-data.
form
    "Data  Transacao:"
    da-inicial
    da-final
    with stream-io overlay row 18 column 35 no-labels title "Selecao"
    frame f-sel-data.

def  shared frame f-mensagem.
form
    c-msg[1] skip
    c-msg[2]
    with stream-io overlay row 17 no-labels title "MENSAGEM AOS CREDORES"
    frame f-mensagem.

def  shared frame f-banco-cob.
form
    i-banco-def label "Banco default p/Cobranca "
    help "Enter para assumir o banco do cadastro de fornecedores"
    with stream-io overlay row 18 column 25 side-labels frame f-banco-cob.

def  shared frame f-agencia-che.
form
    c-agencia-che label "Agencia emitente"
    help "Agencia emitente dos cheques administrativo"
    with stream-io overlay row 18 column 25 side-labels frame f-agencia-che.

def  shared frame f-parametros.
form
    "              Empresa:" i-empresa   skip
    "             Portador:" i-portador  skip
    "           Sequencial:" i-movimento help
    " Nr sequencial que identifica movtos na mesma data" skip
    "        Escolhe/Todos:" l-resp1     skip
    " Enter Marca/Desmarca:" l-resp2     skip
    " Sel.Bordero/Dt.trans:" l-resp3     skip
    "Estado diferente de 0:" l-estado    help            
    "Documentos com estado diferente de 0 (zero)"        skip
    "      Utiliza Leitora:" l-leitora  
    help "Informe se utiliza Leitora de Codigo de Barras" skip
    with stream-io overlay row 11 column 1 no-labels frame f-parametros.

def var c-ocorrencia as character format "x(35)" extent 2
             init ["PAG - Envio de Pagamento",
                   "OCO - Ocorrencia de Pagamento"].

def  shared frame f-ocor.
form
    c-ocorrencia[1] skip
    c-ocorrencia[2] skip
    with stream-io no-labels title "Ocorrencia" overlay row 17 frame f-ocor.

/* Nacional */
def var c-tipo-pag-nac as character format "x(30)" extent 5
             init ["01 - DOC",
                   "02 - Credito em Conta Corrente",
                   "03 - Cheque Administrativo",
                   "04 - Cobranca",
                   "05 - Informado no Fornecedor"].

def  shared frame f-tipo-pag-nac.
form
    c-tipo-pag-nac[1] skip
    c-tipo-pag-nac[2] skip
    c-tipo-pag-nac[3] skip
    c-tipo-pag-nac[4] skip
    c-tipo-pag-nac[5]
    with stream-io no-labels title "Forma de Pagamento"
         overlay row 14 frame f-tipo-pag-nac.

def var c-central    as character format "x(10)" extent 9
             init ["RJ - 06750",
                   "SP - 06751",
                   "MG - 06752",
                   "BA - 06753",
                   "PA - 06755",
                   "PE - 06756",
                   "PR - 06757",
                   "ES - 06423",
                   "CE - 07615"].

def  shared frame f-central-nac.
form
    c-central[1] skip
    c-central[2] skip
    c-central[3] skip
    c-central[4] skip
    c-central[5] skip
    c-central[6] skip
    c-central[7] skip
    c-central[8] skip
    c-central[9]
    with stream-io row 10 no-labels title "CENTRAL" overlay frame f-central-nac.

/* Itau */
def var c-tipo-pag-ita as character format "x(30)" extent 8
             init ["01 - Credito em Conta Corrente",
                   "02 - Cheque Pagamento",
                   "03 - Doc",
                   "04 - Ordem pagamento c/aviso",
                   "10 - Ordem pagamento",
                   "30 - Titulo banco Itau",
                   "31 - Titulo outro banco",
                   "43 - TED CIP"].

def  shared frame f-tipo-pag-ita.
form
    c-tipo-pag-ita[1] skip
    c-tipo-pag-ita[2] skip
    c-tipo-pag-ita[3] skip
    c-tipo-pag-ita[4] skip
    c-tipo-pag-ita[5] skip
    c-tipo-pag-ita[6] skip
    c-tipo-pag-ita[7] skip
    c-tipo-pag-ita[8] 
    with stream-io no-labels title "Forma de Pagamento"
         overlay row 12 frame f-tipo-pag-ita.


/* Bradesco */
def var c-tipo-pag-brad as character format "x(30)" extent 8
             init ["01 - Credito em Conta Corrente",
                   "02 - Cheque Pagamento",
                   "03 - Doc",
                   "05 - Credito em Conta Real Time",
                   "07 - TED CIP",
                   "08 - TED STR", 
                   "30 - Titulo banco Bradesco",
                   "31 - Titulo outro banco"].

def  shared frame f-tipo-pag-brad.
form
    c-tipo-pag-brad[1] skip
    c-tipo-pag-brad[2] skip
    c-tipo-pag-brad[3] skip
    c-tipo-pag-brad[4] skip
    c-tipo-pag-brad[5] skip
    c-tipo-pag-brad[6] skip
    c-tipo-pag-brad[7] skip
    c-tipo-pag-brad[8]     
    with stream-io no-labels title "Forma de Pagamento"
         overlay row 11 frame f-tipo-pag-brad.

/* Bradesco */
def var c-final-doc as character format "x(50)" extent 13
             init ["01 - Credito em Conta Corrente",
                   "02 - Pagamento de Aluguel/Condominio",
                   "03 - Pagamento de Duplicatas/Titulos",
                   "04 - Pagamento de Dividendos",
                   "05 - Pagamento de Mensalidade Escolar",
                   "06 - Pagamento de Salario",
                   "07 - Pagamento de Fornecedores/Honorarios",
                   "08 - Operacao de Cambio/Fundos/Bolsa de Valores",
                   "09 - Repasse de Arrecadacao/Pagamento de Tributos",
                   "10 - Transferencia Internacional em Reais",
                   "11 - Credito em Conta Poupanca",
                   "12 - Credito Judicial",
                   "20 - Outros"].

def  shared frame f-final-doc.
form
  c-final-doc[1] skip
  c-final-doc[2] skip
  c-final-doc[3] skip
  c-final-doc[4] skip
  c-final-doc[5] skip
  c-final-doc[6] skip
  c-final-doc[7] skip
  c-final-doc[8] skip
  c-final-doc[9] skip
  c-final-doc[10] skip
  c-final-doc[11] skip
  c-final-doc[12] skip
  c-final-doc[13] 
    with stream-io no-labels title "Finalidade do DOC"
         overlay row 6 frame f-final-doc.

/* Safra */
def var c-tipo-pag-saf as character format "x(38)" extent 4
             init ["01 - DOC - Documento de Credito",
                   "02 - CHQ - Cheque Administrativo",
                   "03 - CC  - Credito em Conta Corrente",
                   "04 - COB - Liquidacao de Cobranca"].

def  shared frame f-tipo-pag-saf.
form
    c-tipo-pag-saf[1] skip
    c-tipo-pag-saf[2] skip
    c-tipo-pag-saf[3] skip
    c-tipo-pag-saf[4] 
    with stream-io no-labels title "Forma de Pagamento"
         overlay row 15 frame f-tipo-pag-saf.

def  shared frame f-doc-brad.
form
   c-tipo-doc label "Tipo DOC"
   i-nr-doc   label "Nr.  DOC"
   with stream-io side-labels overlay row 18 frame f-doc-brad.

/* ap1001.i7 */
 
end.

assign de-hea-val = de-hea-val + de-valor-pag
       i-hea-qua  = i-hea-qua  + 1.

/* Fim ap1001.i14 */
 
    end.
end.


/***************************************************************************
** ap1001m.i5
** Objetivo: Gerar registro Trailer do Lote para o banco Real.
***************************************************************************/

/* Trailer */
put stream relat
    "9"                                                               at 001
    de-hea-val                               format "999999999999999" at 002
    i-hea-qua + 1                            format "999999"          at 395
    skip.

/*-- ap1001m.i5 --*/

  /* trailer de arquivo */

if  line-counter >= 62 then
    page.

disp de-tot-geral-l
     de-tot-geral-b with frame f-tot-geral.

if (portador.cod-febraban <> 745
and portador.cod-febraban <> 237
and portador.cod-febraban <> 001) 
or (portador.cod-febraban  = 237 
and portador.dec-2         = 1) 
or (portador.cod-febraban  = 001 
and tt-param.l-incl-param-arq) then 
    put stream relat unformatted chr(26).
else
    put stream relat unformatted skip.    

page.

hide frame f-cabec2.
disp tt-param.i-empresa
     tt-param.i-portador
     c-banco
     tt-param.c-arquivo-remessa
     tt-param.c-esp-ini
     tt-param.c-esp-fim
     tt-param.c-est-ini
     tt-param.c-est-fim
     tt-param.da-inicial
     tt-param.da-final
     tt-param.da-vencimen-ini
     tt-param.da-vencimen-fim
     tt-param.i-docto-ini
     tt-param.i-docto-fim
     tt-param.i-fornec-ini
     tt-param.i-fornec-fim
     tt-param.c-ser-ini
     tt-param.c-ser-fim
     tt-param.i-bord-ini
     tt-param.i-bord-fim
     with frame f-cabec1.

output stream relat close.

/**************************************************************************
**
** I-RPCLO - Define sa�da para impress�o do relat�rio - ex. cd9540.i
** Parametros: {&stream} = nome do stream de saida no formato "stream nome"
***************************************************************************/
IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN                
    RUN pi_before_close IN h-procextimpr (INPUT-OUTPUT TABLE tt-configur_layout_impres_fim).

FOR EACH tt-configur_layout_impres_fim EXCLUSIVE-LOCK
    BY tt-configur_layout_impres_fim.num_ord_funcao_imprsor :
    do v_num_count = 1 to extent(tt-configur_layout_impres_fim.num_carac_configur):
      case tt-configur_layout_impres_fim.num_carac_configur[v_num_count]:
        when 0 then put  control null.
        when ? then leave.
        OTHERWISE PUT  control CODEPAGE-CONVERT(chr(tt-configur_layout_impres_fim.num_carac_configur[v_num_count]),
                                                         session:cpinternal, 
                                                         c-cod_pag_carac_conver).
      end CASE.
    END.
    DELETE tt-configur_layout_impres_fim.
END.

/* N�o gerar p�gina em branco - tech14207 11/02/2005 */


output  close.

/* Sa�da para RTF - tech981 20/10/2004 */

/* fim: Sa�da para RTF */

/*tech14178 procedimentos de convers�o PDF */
/*tech868*/

    IF usePDF() THEN DO:
        IF tt-param.destino = 1 THEN DO:
            RUN pi_print IN h_pdf_controller.
        END.
        ELSE DO:
            /*tech868*/
                IF i-num-ped-exec-rpw <> 0 THEN
                    RUN pi_convert IN h_pdf_controller (INPUT c-dir-spool-servid-exec + "~/" + v_output_file, IF tt-param.destino = 3 THEN YES ELSE NO). /* indica se vai para terminal, pois regras de nomenclatura s�o diferentes */
                ELSE
                    RUN pi_convert IN h_pdf_controller (INPUT v_output_file, IF tt-param.destino = 3 THEN YES ELSE NO).
            /*tech868*/
        END.
    END.



IF VALID-HANDLE(h-procextimpr) AND h-procextimpr:FILE-NAME = c_process-impress THEN DO:
    RUN pi_after_close IN h-procextimpr (INPUT c-arq-control).
    DELETE PROCEDURE h-procextimpr NO-ERROR.
END.
/* i-rpout */
 

if valid-handle(v_hdl_reg_produt) then
   delete procedure(v_hdl_reg_produt).
/*-- Fim da Include --*/
 

assign i-tipo-pag = i-tipo-pag-ant.

procedure pi-retira-caracter:

  def var i as int no-undo.
  def var c-aux as char no-undo.

  define input-output parameter c-numero as char.

  assign c-aux = "".

  do i = 1 to length(c-numero):
      if substr(c-numero,i,1) < "0"
      or substr(c-numero,i,1) > "9" then
         next.
      assign c-aux = c-aux + substr(c-numero,i,1).
  end.

  assign c-numero = c-aux.
end procedure.


/* Fim de Programa */





