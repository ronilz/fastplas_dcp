&Scoped-define WINDOW-NAME C-Win

/*------------------------------------------------------------------------
File.............: es5904a.w
Description......: RELAT�RIO DE ACERTOS - CONTAS A RECEBER
Input Parameters : 
Output Parameters: 
Author...........: DATASUL S.A.
Created..........: 26/03/12 - 13:27 - super
OBS..............: Este fonte foi gerado pelo Data Viewer
------------------------------------------------------------------------*/
/* VSH - 16/04/12 */
                         
define variable c-prog-gerado as character no-undo initial "es5904A".
CREATE WIDGET-POOL. 

/* ***************************  Definitions  ************************** */

/* Preprocessadores do Template de Relat�rio                            */

&GLOBAL-DEFINE PGSEL f-pg-sel 
&GLOBAL-DEFINE PGPAR f-pg-par 
&GLOBAL-DEFINE PGIMP f-pg-imp 

/* Include Com as Vari�veis Globais */

def new global shared var c-seg-usuario as char format "x(12)" no-undo.
def new global shared var v_cod_usuar_corren as character format "x(12)" label "Usu�rio Corrente" column-label "Usu�rio Corrente" no-undo.
def new global shared var i-num-ped-exec-rpw  as integer no-undo.
def new global shared var v_cod_empres_usuar  as character format "x(3)"  label "Empresa" column-label "Empresa" no-undo. 
def new global shared var v_cod_dir_spool_servid_exec as CHARACTER format "x(8)":U no-undo.

def var v_cod_dwb_parameters as character format "x(8)" no-undo.
def var v_Cod_prog_chamador as character no-undo.
def var v_ind_tip_exec      as integer   no-undo.
def var v_rec_log           as recid     no-undo.
def var v_cod_private       as character no-undo.
def var i-ep-codigo-usuario as integer no-undo.
def var v_cdn_empres_usuar as integer no-undo.
def var v_cod_dwb_user as character format "x(12)" label "Usu�rio" column-label "Usu�rio" no-undo.
def var v_cod_dwb_order as character format "x(32)" label "Classifica��o" column-label "Classificador" no-undo.
def var v_nom_dwb_printer      like emsbas.dwb_rpt_param.nom_dwb_printer.
def var v_cod_dwb_print_layout like emsbas.dwb_rpt_param.cod_dwb_print_layout.
def var v_nom_dwb_print_file   as character format "x(100)"  label "Arquivo Impress�o"  column-label "Arq Impr"  no-undo.
def var v_dwb_run_mode as character no-undo.
def var v_cod_dwb_output as character no-undo.
def var v_cod_dwb_file_temp as character format "x(12)" no-undo.

def new shared stream str-rp.

/* Parameters Definitions ---                                           */ 


/* Temporary Table Definitions ---                                      */ 

/****************** Defini��o de Tabelas Tempor�rias do Relat�rio **********************/

define temp-table tt-raw-digita
    field raw-digita as raw.

define temp-table tt-param
    field destino              as integer
    field arquivo              as char
    field usuario              as char
    field data-exec            as date
    field hora-exec            as integer
    field parametro            as logical
    field formato              as integer
    field v_num_tip_aces_usuar as integer
    field ep-codigo            as integer
    field c-cod_empresa-ini like movfin.tit_acr.cod_empresa
    field c-cod_empresa-fim like movfin.tit_acr.cod_empresa
    field c-cod_estab-ini like movfin.tit_acr.cod_estab
    field c-cod_estab-fim like movfin.tit_acr.cod_estab
    field i-cdn_cliente-ini like movfin.tit_acr.cdn_cliente
    field i-cdn_cliente-fim like movfin.tit_acr.cdn_cliente
    field da-dat_emis_docto-ini like movfin.tit_acr.dat_emis_docto
    field da-dat_emis_docto-fim like movfin.tit_acr.dat_emis_docto
    FIELD c-nr-docto-ini like mov-tit.nr-docto  
    field c-nr-docto-fim like mov-tit.nr-docto 
    field da-dt-emissao-ini like mov-tit.dt-emissao  
    field da-dt-emissao-fim like mov-tit.dt-emissao.

/* Transfer Definitions */

def var raw-param        as raw no-undo.

/* Local Variable Definitions ---                                       */ 

def var l-ok                 as logical no-undo. 
def var c-arq-digita         as char    no-undo. 
def var c-terminal           as char    no-undo. 
def var v-cod-pg-mouse-selec as char    no-undo. 
def var v-cod-prog-i-rprun   as char    no-undo. 
def var c-impressora-old     as char    no-undo. 
def var c-arquivo-old        as char    no-undo. 
def var c-destino-old        as char    no-undo. 
def var i-cont               as int     no-undo. 
def var v-cod-prog-gerado    as char    no-undo. 
def var v-cod-extens-arq     as char    no-undo initial "lst". 

/****************** Defini�ao de Par�metros do Relat�rio *********************/ 

/****************** Defini�ao de Vari�veis de Sele��o do Relat�rio *********************/ 

def new shared var c-cod_empresa-ini like movfin.tit_acr.cod_empresa format "x(3)" initial "" no-undo.
def new shared var c-cod_empresa-fim like movfin.tit_acr.cod_empresa format "x(3)" initial "ZZZ" no-undo.
def new shared var c-cod_estab-ini like movfin.tit_acr.cod_estab format "x(3)" initial "" no-undo.
def new shared var c-cod_estab-fim like movfin.tit_acr.cod_estab format "x(3)" initial "ZZZ" no-undo.
def new shared var i-cdn_cliente-ini like movfin.tit_acr.cdn_cliente format ">>>,>>>,>>9" initial 0 no-undo.
def new shared var i-cdn_cliente-fim like movfin.tit_acr.cdn_cliente format ">>>,>>>,>>9" initial 999999999 no-undo.
def new shared var da-dat_emis_docto-ini like movfin.tit_acr.dat_emis_docto format "99/99/9999" initial TODAY no-undo.
def new shared var da-dat_emis_docto-fim like movfin.tit_acr.dat_emis_docto format "99/99/9999" initial TODAY no-undo.
def new shared var c-nr-docto-ini like mov-tit.nr-docto format "x(16)" initial "" no-undo.
def new shared var c-nr-docto-fim like mov-tit.nr-docto format "x(16)" initial "ZZZZZZZZZZZZZZZZ" no-undo.
def new shared var da-dt-emissao-ini like mov-tit.dt-emissao format "99/99/9999" initial "01/01/0001" no-undo.
def new shared var da-dt-emissao-fim like mov-tit.dt-emissao format "99/99/9999" initial "12/31/9999" no-undo.

/* ********************  Preprocessor Definitions  ******************** */ 

/* ***********************  Control Definitions  ********************** */ 

DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO. 


DEFINE BUTTON bt-arquivo
    IMAGE-UP FILE "imagem\gr-sea"
    IMAGE-INSENSITIVE FILE "imagem\gr-iisea"
    LABEL " "
    SIZE 4 BY 1.
    
DEFINE BUTTON bt-config-impr
    IMAGE-UP FILE "imagem\gr-cfprt"
    LABEL " "
    SIZE 4 BY 1.
    
DEFINE IMAGE IMAGE-1
    FILENAME "imagem\gr-fir"
    SIZE 3 BY .88.
    
DEFINE IMAGE IMAGE-2
    FILENAME "imagem\gr-las"
    SIZE 3 BY .88.
    
DEFINE IMAGE im-pg-imp
    FILENAME "imagem\gr-fldup"
    SIZE 15.72 BY 1.19.
    
DEFINE IMAGE im-pg-par
    FILENAME "imagem\gr-fldup"
    SIZE 15.72 BY 1.19.
    
DEFINE IMAGE im-pg-sel
    FILENAME "imagem\gr-fldup"
    SIZE 15.72 BY 1.19.

run grapi/gr5010.p.
if  return-value = 'rpw' then 
    return 'ok'.


DEFINE VARIABLE c-arquivo AS CHARACTER 
VIEW-AS EDITOR MAX-CHARS 256 
SIZE 40 BY 1.00 
BGCOLOR 15  font 2 NO-UNDO.

DEFINE VARIABLE text-destino AS CHARACTER FORMAT "X(256)" INITIAL "Destino"
VIEW-AS TEXT 
SIZE 8.57 BY .62 NO-UNDO. 

DEFINE VARIABLE text-modo AS CHARACTER FORMAT "X(256)" INITIAL "Execu��o"
VIEW-AS TEXT 
SIZE 10.86 BY .62 NO-UNDO.

DEFINE VARIABLE text-parametro AS CHARACTER FORMAT "X(256)" INITIAL "Par�metros de Impress�o"
VIEW-AS TEXT 
SIZE 24.72 BY .62 NO-UNDO.

DEFINE VARIABLE rs-destino AS INTEGER INITIAL 3 
VIEW-AS RADIO-SET HORIZONTAL
RADIO-BUTTONS 
"Impressora", 1,
"Arquivo", 2,
"Terminal", 3
SIZE 44 BY 1.08 NO-UNDO.
DEFINE VARIABLE rs-execucao AS INTEGER INITIAL 1 
VIEW-AS RADIO-SET HORIZONTAL 
RADIO-BUTTONS 
"On-Line", 1,
"Batch", 2
SIZE 27.72 BY .92 NO-UNDO.

DEFINE VARIABLE tb-parametro AS LOGICAL INITIAL no 
LABEL "Imprimir P�gina de Par�metros"
VIEW-AS TOGGLE-BOX 
SIZE 32 BY .83 
NO-UNDO.

DEFINE VARIABLE rs-formato AS INTEGER INITIAL 2 
VIEW-AS RADIO-SET HORIZONTAL 
RADIO-BUTTONS 
"80 colunas", 1,
"132 colunas", 2
SIZE 32 BY .92 NO-UNDO.

DEFINE RECTANGLE RECT-7 
EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
SIZE 46.29 BY 2.92.

DEFINE RECTANGLE RECT-9 
EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
SIZE 46.29 BY 1.69.

DEFINE RECTANGLE RECT-10 
EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
SIZE 46.29 BY 3.50.

DEFINE VARIABLE l-param-1 AS LOGICAL INITIAL no 
LABEL "Par�metro 1"
VIEW-AS TOGGLE-BOX 
SIZE 44 BY 1.08 NO-UNDO. 

DEFINE BUTTON bt-ajuda 
LABEL "Ajuda"
SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
LABEL "Fechar"
SIZE 10 BY 1.

DEFINE BUTTON bt-executar 
LABEL "Executar"
SIZE 10 BY 1.

DEFINE RECTANGLE RECT-1
EDGE-PIXELS 2 GRAPHIC-EDGE 
SIZE 79 BY 1.42 
BGCOLOR 7.

DEFINE RECTANGLE RECT-6
EDGE-PIXELS 0
SIZE 78.72 BY .12
BGCOLOR 7.

DEFINE RECTANGLE rt-folder
EDGE-PIXELS 1 GRAPHIC-EDGE  NO-FILL
SIZE 79 BY 11.38
FGCOLOR 0.

DEFINE RECTANGLE rt-folder-left
EDGE-PIXELS 0
SIZE .43 BY 11.19
BGCOLOR 15.

DEFINE RECTANGLE rt-folder-right
EDGE-PIXELS 0
SIZE .43 BY 11.15
BGCOLOR 7.

DEFINE RECTANGLE rt-folder-top
EDGE-PIXELS 0
SIZE 78.72 BY .12
BGCOLOR 15 .

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-relat
    bt-executar AT ROW 14.54 COL 3 HELP
"Dispara a execu��o do relat�rio"
     bt-cancelar AT ROW 14.54 COL 14 HELP
"Fechar"
     bt-ajuda AT ROW 14.54 COL 70 HELP
"Ajuda"
     im-pg-sel AT ROW 1.5 COL 2.14
     im-pg-par AT ROW 1.5 COL 17.86
     im-pg-imp AT ROW 1.5 COL 33.58
     "Data Viewer 3.00"AT ROW 1.8 COL 66.00
     rt-folder AT ROW 2.5 COL 2
     rt-folder-top AT ROW 2.54 COL 2.14
     rt-folder-left AT ROW 2.54 COL 2.14
     rt-folder-right AT ROW 2.65 COL 80.43
     RECT-6 AT ROW 13.73 COL 2.14
     RECT-1 AT ROW 14.31 COL 2
     WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY
     SIDE-LABELS NO-UNDERLINE THREE-D
     AT COL 1 ROW 1
     SIZE 81 BY 15
     DEFAULT-BUTTON bt-executar.

DEFINE FRAME f-pg-imp
    text-destino AT ROW 1.62 COL 3.86 NO-LABEL
    rs-destino AT ROW 2.38 COL 3.29 HELP
    "Destino de Impress�o do Relat�rio" NO-LABEL
    bt-arquivo AT ROW 3.58 COL 43.29 HELP
    "Escolha do nome do arquivo"
     bt-config-impr AT ROW 3.58 COL 43.29 HELP
    "Configura��o da impressora"
     c-arquivo AT ROW 3.56 COL 3.29 HELP
    "Nome do arquivo de destino do relat�rio" NO-LABEL
     text-modo AT ROW 5 COL 1.29 COLON-ALIGNED NO-LABEL
     rs-execucao AT ROW 5.77 COL 3 HELP
    "Modo de Execu��o" NO-LABEL
     tb-parametro AT ROW 7.92 COL 3.2
     rs-formato AT ROW 8.8 COL 3 HELP
    "Formato de Impress�o" NO-LABEL
     text-parametro AT ROW 7.17 COL 1.29 COLON-ALIGNED NO-LABEL
     RECT-7 AT ROW 1.92 COL 2.14
     RECT-9 AT ROW 5.31 COL 2.14
     RECT-10 AT ROW 7.46 COL 2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY
    SIDE-LABELS NO-UNDERLINE THREE-D
    AT COL 3 ROW 3
    SIZE 73.72 BY 10.

DEFINE FRAME f-pg-sel
   c-cod_empresa-ini label "Empresa"
     at row 1 col 17 colon-aligned
     view-as fill-in 
     size 5 by .88
     font 1

   c-cod_empresa-fim no-label
     at row 1 col 39 colon-aligned
     view-as fill-in 
     size 5 by .88
     font 1

   c-cod_estab-ini label "Estabelecimento"
     at row 2 col 17 colon-aligned
     view-as fill-in 
     size 5 by .88
     font 1

   c-cod_estab-fim no-label
     at row 2 col 39 colon-aligned
     view-as fill-in 
     size 5 by .88
     font 1

   i-cdn_cliente-ini label "Cliente"
     at row 3 col 17 colon-aligned
     view-as fill-in 
     size 13 by .88
     font 1

   i-cdn_cliente-fim no-label
     at row 3 col 39 colon-aligned
     view-as fill-in 
     size 13 by .88
     font 1

   da-dat_emis_docto-ini label "Data  Vencimento"
     at row 4 col 17 colon-aligned
     view-as fill-in 
     size 12 by .88
     font 1

   da-dat_emis_docto-fim no-label
     at row 4 col 39 colon-aligned
     view-as fill-in 
     size 12 by .88
     font 1

   da-dt-emissao-ini label "Data  Emiss�o"
     at row 5 col 17 colon-aligned
     view-as fill-in 
     size 12 by .88
     font 1

   da-dt-emissao-fim no-label
     at row 5 col 39 colon-aligned
     view-as fill-in 
     size 12 by .88
     font 1

   c-nr-docto-ini label "Documento"
        at row 6 col 17 colon-aligned
        view-as fill-in 
        size 12 by .88
        font 1
   
  c-nr-docto-fim no-label
        at row 6 col 39 colon-aligned
        view-as fill-in 
        size 12 by .88
        font 1

IMAGE-1 AT ROW 01.00 COL 32
   IMAGE-2 AT ROW 01.00 COL 37
   WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY
   SIDE-LABELS NO-UNDERLINE THREE-D
   AT COL 3 ROW 2.85
   SIZE 76.86 BY 10.62.

DEFINE RECTANGLE ret-par-fill
   EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
   SIZE  74.06 BY .4.

DEFINE FRAME f-pg-par

   WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY
   SIDE-LABELS NO-UNDERLINE THREE-D
   AT COL 3 ROW 3
   SIZE 75 BY 10.

/* ******** Acerto da posi��o dos labels e tamanho dos radio-set ******* */

DEFINE VARIABLE h-label AS WIDGET-HANDLE NO-UNDO.

/* *************************  Create Window  ************************** */

IF SESSION:DISPLAY-TYPE = "GUI" THEN
CREATE WINDOW C-Win ASSIGN
   HIDDEN             = YES
   TITLE              = "RELAT�RIO DE ACERTOS - CONTAS A RECEBER - es5904a"
   HEIGHT             = 15
   WIDTH              = 81.14
   MAX-HEIGHT         = 22.35
   MAX-WIDTH          = 114.29
   VIRTUAL-HEIGHT     = 22.35
   VIRTUAL-WIDTH      = 114.29
   RESIZE             = yes
   SCROLL-BARS        = no
   STATUS-AREA        = yes
   BGCOLOR            = ?
   FGCOLOR            = ?
   KEEP-FRAME-Z-ORDER = yes
   THREE-D            = yes
   MESSAGE-AREA       = no
   SENSITIVE          = yes.

ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

/* ***************  Runtime Attributes and UIB Settings  ************** */

ASSIGN FRAME f-pg-imp:FRAME = FRAME f-relat:HANDLE
       FRAME f-pg-par:FRAME = FRAME f-relat:HANDLE
       FRAME f-pg-sel:FRAME = FRAME f-relat:HANDLE.

IF SESSION:DISPLAY-TYPE = "GUI" AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* ************************* Included-Libraries *********************** */


def new shared var c-arq-old-batch as char no-undo.       
def new shared var v_cod_dwb_program as character format "x(32)" label "Programa" column-label "Programa" no-undo.

assign frame f-relat:visible  = no
       frame f-relat:font     = 1 
       frame f-pg-sel:visible = no
       frame f-pg-sel:font    = 1 
       frame f-pg-par:visible = no
       frame f-pg-par:font    = 1
       frame f-pg-imp:visible = no
       frame f-pg-imp:font    = 1.

ON  GO OF frame {&pgsel} ANYWHERE DO:
    if  self:type <> "editor"
        or (self:type = "editor"
        and keyfunction(lastkey) <> "RETURN") 
    then do:
        apply "choose" to bt-executar in frame f-relat.
    end.
END.

ON  GO OF frame {&pgpar} ANYWHERE DO:
    if  self:type <> "editor"
        or (self:type = "editor"
        and keyfunction(lastkey) <> "RETURN")
    then do:
        apply "choose" to bt-executar in frame f-relat.
    end.
END.

ON  GO OF frame {&pgimp} ANYWHERE DO:
    if  self:type <> "editor"
        or (self:type = "editor"
        and keyfunction(lastkey) <> "RETURN") 
    then do:
        apply "choose" to bt-executar in frame f-relat.
    end.
END.

/* ************************  Control Triggers  ************************ */

ON END-ERROR OF C-Win
OR ENDKEY OF C-Win ANYWHERE DO:
   RETURN NO-APPLY.
END.

ON WINDOW-CLOSE OF C-Win
DO:
   APPLY "CLOSE" TO THIS-PROCEDURE.
   RETURN NO-APPLY.
END.

ON ENDKEY OF FRAME f-relat DO:
  return no-apply.
END.

ON CHOOSE OF bt-ajuda IN FRAME f-relat
DO:

if  this-procedure:private-data = "HLP=00" or
    this-procedure:private-data = "" then do:
    message "N�o existe um arquivo de ajuda para este relat�rio!" skip
            "Entre em contato com o administrador do m�dulo para solicitar o mesmo!"
            view-as alert-box information title "Ajuda".
    return.
end.

run prgtec/men/men900za.py (input self:frame,
                            input this-procedure:handle).

END.

ON CHOOSE OF bt-arquivo IN FRAME f-pg-imp
DO:

run grapi/gr5007a.p (output c-arquivo,input v-cod-extens-arq).

disp c-arquivo with frame f-pg-imp.

END.

ON CHOOSE OF bt-cancelar IN FRAME f-relat
DO:
   apply "close" to this-procedure.
END.

ON CHOOSE OF bt-config-impr IN FRAME f-pg-imp
DO:


assign v_nom_dwb_printer      = ""
       v_cod_dwb_print_layout = ""
       v_nom_dwb_print_file   = "".

if  search("prgtec/btb/btb036zb.r") = ? and 
    search("prgtec/btb/btb036zb.p") = ? then do:
    message "Programa execut�vel n�o foi encontrado: prgtec/btb/btb036zb.p"
            view-as alert-box error buttons ok title "Erro".
    return.
end.
else
    run prgtec/btb/btb036zb.p (input-output v_nom_dwb_printer,
                               input-output v_cod_dwb_print_layout,
                               input-output v_nom_dwb_print_file) /*prg_fnc_layout_impres_imprsor*/.

assign c-arquivo = v_nom_dwb_printer + ":" + v_cod_dwb_print_layout.
ASSIGN c-arquivo = c-arquivo + (if v_nom_dwb_print_file <> "" 
                                then ":" + v_nom_dwb_print_file
                                else "").

disp c-arquivo with frame f-pg-imp.

END.

ON CHOOSE OF bt-executar IN FRAME f-relat
DO:
   do  on error undo, return no-apply:
        run pi-executar.
    end.
END.

ON MOUSE-SELECT-CLICK OF im-pg-imp IN FRAME f-relat
DO:
   run pi-troca-pagina.
END.

ON MOUSE-SELECT-CLICK OF im-pg-par IN FRAME f-relat
DO:
   run pi-troca-pagina.
END.

ON MOUSE-SELECT-CLICK OF im-pg-sel IN FRAME f-relat
DO:
   run pi-troca-pagina.
END.

ON VALUE-CHANGED OF rs-destino IN FRAME f-pg-imp
DO:
   do  with frame f-pg-imp:
       case self:screen-value:
          when "1" then do:
              if c-destino-old = "2" then assign c-arquivo-old = c-arquivo:screen-value.
              assign c-arquivo:sensitive    = no
                     c-destino-old          = "1"
                     c-arquivo:visible      = yes
                     c-arquivo:screen-value  = c-impressora-old
                     bt-arquivo:visible     = no
                     bt-config-impr:visible = yes.
            end.

            when "2" then do:
               if c-destino-old = "1" then assign c-impressora-old = c-arquivo:screen-value.
               if  input frame f-pg-imp rs-execucao = 2 /* Batch */ 
                   THEN assign c-arquivo-old = c-prog-gerado + "." + UPPER(v-cod-extens-arq).
                   ELSE assign c-arquivo-old = session:temp-directory + c-prog-gerado + "." + UPPER(v-cod-extens-arq).
               assign c-arquivo:sensitive     = yes
                      c-destino-old           = "2"
                      c-arquivo:visible       = yes
                      c-arquivo:screen-value  = c-arquivo-old
                      bt-arquivo:visible      = yes
                      bt-config-impr:visible  = no.
            end.

            when "3" then do:
               if c-destino-old = "2" then assign c-arquivo-old = c-arquivo:screen-value.
               if c-destino-old = "1" then assign c-impressora-old = c-arquivo:screen-value.
               assign c-arquivo:sensitive     = no
                      c-destino-old           = "3"
                      c-arquivo:visible       = no
                      bt-arquivo:visible      = no
                      bt-config-impr:visible  = no.
            end.
       end case.
   end.
END.

ON VALUE-CHANGED OF rs-execucao IN FRAME f-pg-imp
DO:

if  input frame f-pg-imp rs-execucao = 2 /* Batch */ then do:

    /* assign c-arquivo */

    if c-destino-old = "2" 
        THEN ASSIGN c-arq-old-batch  = c-arquivo.
    if c-destino-old = "1" 
        THEN ASSIGN c-impressora-old = c-arquivo.
    
    if  input frame f-pg-imp rs-destino = 2 THEN DO:  /* Arquivo */
       assign c-arquivo = c-prog-gerado + "." + upper(v-cod-extens-arq). 

       disp c-arquivo with frame f-pg-imp.
    END.
    if index(c-arquivo,"spool/") <> 0 then do:
         assign c-arquivo = replace(c-arquivo,"spool/","").
      disp c-arquivo with frame f-pg-imp.
    end.
    if  input frame f-pg-imp rs-destino = 3 then do:
        assign rs-destino:screen-value in frame f-pg-imp = "2".
        apply "value-changed" to rs-destino in frame f-pg-imp.
    end.
    if  rs-destino:disable(c-terminal) in frame f-pg-imp then.
end.
else do:  /* On-line */
    if  input frame f-pg-imp rs-destino = 1  /* Impressora */
       then assign c-arquivo = c-impressora-old.
       else c-arquivo = session:temp-directory + c-prog-gerado + "." + UPPER(v-cod-extens-arq).
    disp c-arquivo with frame f-pg-imp.
    if  rs-destino:enable(c-terminal) in frame f-pg-imp then.
end.

END.

/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW             = {&WINDOW-NAME}
   THIS-PROCEDURE:CURRENT-WINDOW  = {&WINDOW-NAME}.

assign v-cod-prog-gerado = "es5904a".


run grapi/gr5001.p (output v_cod_dwb_user).
assign v_cod_dwb_program = v-cod-prog-gerado
       c-seg-usuario     = v_cod_dwb_user.

run grapi/gr5002.p (input  v_cod_dwb_program,
                      output v_cod_private).

assign this-procedure:private-data = v_cod_private.

if  search("prgtec/btb/btb906za.r") = ? and search("prgtec/btb/btb906za.py") = ? then do:
    message "Programa execut�vel n�o foi encontrado: prgtec/btb/btb906za.py" view-as alert-box error buttons ok title "Erro".
    stop.
end.
else
    run prgtec/btb/btb906za.py.
if  v_cod_dwb_user = "" 
then
    assign v_cod_dwb_user = v_cod_usuar_corren.

if  c-seg-usuario = "" 
then
    assign c-seg-usuario = v_cod_usuar_corren.
    
if  search("prgtec/men/men901za.r") = ? and 
    search("prgtec/men/men901za.py") = ? then do:
    message "Programa execut�vel n�o foi encontrado: prgtec/men/men901za.py"
           view-as alert-box error buttons ok title "Erro".
    return.
end.
else
    run prgtec/men/men901za.py (Input v_cod_dwb_program).

if  return-value = "2014" then do:
    message "Programa a ser executado n�o � um programa v�lido Datasul !" view-as alert-box warning title "Aviso".
    return error.
end.
if  return-value = "2012" then do:
    message "Usu�rio sem permiss�o para acessar o programa." view-as alert-box warning title "Aviso".
    return error.
end.

run grapi/gr5003.p (input  v_cod_dwb_program,
                    input  v_cod_dwb_user,
                    output v_rec_log).


 /*include de inicializa��o do relat�rio */

 /*inicializa��es do template de relat�rio */

assign {&window-name}:virtual-width-chars  = {&window-name}:width-chars  
       {&window-name}:virtual-height-chars = {&window-name}:height-chars 
       {&window-name}:min-width-chars      = {&window-name}:width-chars  
       {&window-name}:max-width-chars      = {&window-name}:width-chars  
       {&window-name}:min-height-chars     = {&window-name}:height-chars 
       {&window-name}:max-height-chars     = {&window-name}:height-chars.
assign c-terminal = "Terminal".

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.


def var wh-label-sel     as widget-handle no-undo.
def var wh-label-par     as widget-handle no-undo.
def var wh-label-imp     as widget-handle no-undo.
def var wh-group         as widget-handle no-undo.
def var wh-child         as widget-handle no-undo.
def var c-list-folders   as char          no-undo.
def var i-current-folder as integer       no-undo.
def var i-new-folder     as integer       no-undo.
def var c-aux            as char no-undo.
def var i-aux            as integer no-undo.
def var c-arq-old        as char no-undo.
def var c-imp-old        as char no-undo.

ON  CLOSE OF this-procedure DO:
    run grapi/gr5004.p (input v_rec_log).
    find emsbas.log_exec_prog_dtsul where recid(emsbas.log_exec_prog_dtsul) = v_rec_log no-lock no-error.
    RUN disable_UI.
END.

find emsbas.usuar_mestre where emsbas.usuar_mestre.cod_usuario = c-seg-usuario no-lock no-error.
if avail emsbas.usuar_mestre then 
   assign c-arquivo = if length(emsbas.usuar_mestre.nom_subdir_spool) <> 0
                      then caps(replace(emsbas.usuar_mestre.nom_dir_spool, "~\", "~/") + "~/" + replace(usuar_mestre.nom_subdir_spool, "~\", "~/") + "~/" + v-cod-prog-gerado + "~." + v-cod-extens-arq)
                      else caps(replace(emsbas.usuar_mestre.nom_dir_spool, "~\", "~/") + "~/" + v-cod-prog-gerado + "~." + v-cod-extens-arq).
else 
    assign c-arquivo = caps("spool~/" + v-cod-prog-gerado + "~." + v-cod-extens-arq).

c-arq-old = c-arquivo.



&if "{&PGIMP}" <> "" &then
  ON "LEAVE" OF C-ARQUIVO IN FRAME F-PG-IMP do:
    assign c-arq-old = c-arquivo:screen-value.
  END.  

  ON "ENTER" OF C-ARQUIVO IN FRAME F-PG-IMP OR
     "RETURN" OF C-ARQUIVO IN FRAME F-PG-IMP OR
     "CTRL-ENTER" OF C-ARQUIVO IN FRAME F-PG-IMP OR
     "CTRL-J" OF C-ARQUIVO IN FRAME F-PG-IMP OR
     "CTRL-Z" OF C-ARQUIVO IN FRAME F-PG-IMP do:
    RETURN NO-APPLY.
  END.  

  ON "~\" OF C-ARQUIVO IN FRAME F-PG-IMP do:
    apply "~/" to C-ARQUIVO in frame F-PG-IMP.
    return no-apply.       
  end.

&endif



&IF "{&PGSEL}" <> "" &THEN 
    assign c-list-folders = c-list-folders + "im-pg-sel,".
&ENDIF
&IF "{&PGPAR}" <> "" &THEN 
    assign c-list-folders = c-list-folders + "im-pg-par,".
&ENDIF
&IF "{&PGIMP}" <> "" &THEN
    assign c-list-folders = c-list-folders + "im-pg-imp".
&ENDIF

if  substring(c-list-folders,length(c-list-folders)) = "," then 
    assign c-list-folders = substring(c-list-folders,1,length(c-list-folders) - 1 ).

on  CTRL-TAB,SHIFT-CTRL-TAB anywhere do:
    define variable h_handle  as handle no-undo.       
    define variable c_imagem  as character no-undo.
    define variable l_direita as logical no-undo.            

    l_direita = last-event:label = 'CTRL-TAB'.
        
    block1:
    repeat:        
        if  l_direita then do:
            if  i-current-folder = num-entries(c-list-folders) then
                i-current-folder = 1.
            else
                i-current-folder = i-current-folder + 1.
        end.
        else do:
            if  i-current-folder = 1 then
                i-current-folder = num-entries(c-list-folders).
            else
                i-current-folder = i-current-folder - 1.
        end.
    
        assign c_imagem = entry(i-current-folder,c-list-folders)
               h_handle = frame f-relat:first-child
               h_handle = h_handle:first-child.

        do  while valid-handle(h_handle):
            if  h_handle:type = 'image' and
                h_handle:name =  c_imagem then do:
                if  h_handle:sensitive = no then 
                    next block1.
                apply 'mouse-select-click' to h_handle.
                leave block1.
            end.
            h_handle = h_handle:next-sibling.
        end.
    end.
end.



procedure pi-first-child:
        
    define input parameter wh-entry-folder as widget-handle.
    
    assign wh-entry-folder = wh-entry-folder:first-child
           wh-entry-folder = wh-entry-folder:first-child.
    do  while(valid-handle(wh-entry-folder)):
        if  wh-entry-folder:sensitive = yes 
        and wh-entry-folder:type <> 'rectangle' 
        and wh-entry-folder:type <> 'image'
        and wh-entry-folder:type <> 'browse' then do:
            apply 'entry' to wh-entry-folder.
            leave.
        end.
        else
            assign wh-entry-folder = wh-entry-folder:next-sibling.    
    end.
    
end.


PROCEDURE WinExec EXTERNAL "kernel32.dll":
  DEF INPUT  PARAM prg_name                          AS CHARACTER.
  DEF INPUT  PARAM prg_style                         AS SHORT.
END PROCEDURE.



create text wh-label-sel
assign frame        = frame f-relat:handle
       format       = "x(07)"
       font         = 1
       screen-value = "Sele��o"
       width        = 8
       row          = 1.8
       col          = im-pg-sel:col in frame f-relat + 1.86
       visible      = yes
       triggers:
           on mouse-select-click
              apply "mouse-select-click" to im-pg-sel in frame f-relat.
       end triggers.

create text wh-label-par
assign frame        = frame f-relat:handle
       format       = "x(10)"
       font         = 1
       screen-value = "Par�metros"
       width        = 11
       row          = 1.8
       col          = im-pg-par:col in frame f-relat + 1.7
       visible      = yes
       triggers:
          on mouse-select-click
             apply "mouse-select-click" to im-pg-par in frame f-relat.
       end triggers.

create text wh-label-imp
assign frame        = frame f-relat:handle
       format       = "x(10)"
       font         = 1
       screen-value = "Impress�o"
       width        = 10
       row          = 1.8
       col          = im-pg-imp:col in frame f-relat + 1.7
       visible      = yes
       triggers:
          on mouse-select-click
            apply "mouse-select-click" to im-pg-imp in frame f-relat.
       end triggers.



  assign wh-label-imp:screen-value = "Impress�o".
PAUSE 0 BEFORE-HIDE.

MAIN-BLOCK:
DO  ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
    ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

    IF SESSION:SET-WAIT-STATE("":U) THEN.
    RUN enable_UI.

    ASSIGN text-destino:screen-value   IN FRAME f-pg-imp = "Destino".
    ASSIGN text-modo:screen-value      IN FRAME f-pg-imp = "Execu��o".
    ASSIGN text-parametro:screen-value IN FRAME f-pg-imp = "Par�metros de Impress�o".


    assign v-cod-pg-mouse-selec = "im-pg-sel".

    apply "value-changed" to rs-destino in frame f-pg-imp.

    if v-cod-pg-mouse-selec = "im-pg-sel"
    then
        apply "mouse-select-click" to im-pg-sel in frame f-relat.

    if v-cod-pg-mouse-selec = "im-pg-par"
    then
        apply "mouse-select-click" to im-pg-par in frame f-relat.

    if v-cod-pg-mouse-selec = "im-pg-imp"
    then
        apply "mouse-select-click" to im-pg-imp in frame f-relat.

     view c-win.
     apply "entry" to frame f-Relat.
     apply "entry" to c-win.

    rs-formato:disable(entry(1,rs-formato:radio-buttons in frame f-pg-imp)) in frame f-pg-imp.

    if  im-pg-sel:sensitive in frame f-relat = no then do:
             run pi-muda-cor-label-folder(input "Sele��o").

    end.

    if  im-pg-par:sensitive in frame f-relat = no then do:
        run pi-muda-cor-label-folder(input "Par�metros").

    end.

   IF  NOT THIS-PROCEDURE:PERSISTENT THEN
      WAIT-FOR CLOSE OF THIS-PROCEDURE.

END.
/* **********************  Internal Procedures  *********************** */

PROCEDURE adm-row-available :
   /* Define variables needed by this internal procedure.             */
  /* Process the newly available records (i.e. display fields, 
     open queries, and/or pass records on to any RECORD-TARGETS).    */
END PROCEDURE.

PROCEDURE local-initialize:
  /* Dispatch standard ADM method.  */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .
    rs-formato:disable(entry(1,rs-formato:radio-buttons in frame f-pg-imp)) in frame f-pg-imp.

END PROCEDURE.
PROCEDURE disable_UI :
   IF SESSION:DISPLAY-TYPE = "GUI" AND VALID-HANDLE(C-Win)
   THEN DELETE WIDGET C-Win.
   IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

PROCEDURE enable_UI :
   ENABLE bt-executar bt-cancelar bt-ajuda im-pg-imp im-pg-sel
   WITH FRAME f-relat IN WINDOW C-Win.
   
   DISPLAY  
   c-cod_empresa-ini c-cod_empresa-fim
   c-cod_estab-ini c-cod_estab-fim
   i-cdn_cliente-ini i-cdn_cliente-fim
   da-dat_emis_docto-ini da-dat_emis_docto-fim
   c-nr-docto-ini c-nr-docto-fim 
   da-dt-emissao-ini da-dt-emissao-fim
   WITH FRAME f-pg-sel IN WINDOW C-Win.

   ENABLE IMAGE-1 IMAGE-2  
   c-cod_empresa-ini c-cod_empresa-fim
   c-cod_estab-ini c-cod_estab-fim
   i-cdn_cliente-ini i-cdn_cliente-fim
   da-dat_emis_docto-ini da-dat_emis_docto-fim
   c-nr-docto-ini c-nr-docto-fim 
   da-dt-emissao-ini da-dt-emissao-fim
   WITH FRAME f-pg-sel IN WINDOW C-Win.
   
   DISPLAY rs-destino c-arquivo rs-execucao tb-parametro rs-formato
   WITH FRAME f-pg-imp IN WINDOW C-Win.
   ENABLE RECT-7 rs-destino bt-arquivo bt-config-impr c-arquivo RECT-9 rect-10 rs-execucao tb-parametro rs-formato
   WITH FRAME f-pg-imp IN WINDOW C-Win.
   
   DISPLAY 
   WITH FRAME f-pg-par IN WINDOW C-Win.

   ENABLE 
   WITH FRAME f-pg-par IN WINDOW C-Win.
   
   VIEW C-Win.
END PROCEDURE.

PROCEDURE local-exit :
   APPLY "CLOSE" TO THIS-PROCEDURE.
   RETURN.
END PROCEDURE.

PROCEDURE pi-executar :
   do  on error undo, return error
   on stop  undo, return error:


assign c-arquivo = c-arquivo:screen-value in frame f-pg-imp.

    /* Coloque aqui as valida��es das outras p�ginas, lembrando que elas
       devem apresentar uma mensagem de erro cadastrada, posicionar na p�gina
       com problemas e colocar o focus no campo com problemas             */ 

   if  v_cdn_empres_usuar <> ?
   then
       assign i-ep-codigo-usuario = v_cdn_empres_usuar.

   create tt-param.
   assign tt-param.usuario              = c-seg-usuario
          tt-param.destino              = input frame f-pg-imp rs-destino
          tt-param.data-exec            = today
          tt-param.hora-exec            = time
          tt-param.parametro            = if input frame f-pg-imp tb-parametro = "yes" then yes else no
          tt-param.formato              = if input frame f-pg-imp rs-formato   = "1" then 1 else 2
          tt-param.v_num_tip_aces_usuar = v_num_tip_aces_usuar
          tt-param.ep-codigo            = i-ep-codigo-usuario.
   if  tt-param.destino = 2 then
       assign tt-param.arquivo = input frame f-pg-imp c-arquivo.
   else
       assign tt-param.arquivo = session:temp-directory + "es5904a" + "." + v-cod-extens-arq.

    assign tt-param.c-cod_empresa-ini = input frame f-pg-sel c-cod_empresa-ini
           tt-param.c-cod_empresa-fim = input frame f-pg-sel c-cod_empresa-fim
           tt-param.c-cod_empresa-fim = input frame f-pg-sel c-cod_empresa-fim
           tt-param.c-cod_estab-ini = input frame f-pg-sel c-cod_estab-ini
           tt-param.c-cod_estab-fim = input frame f-pg-sel c-cod_estab-fim
           tt-param.i-cdn_cliente-ini = input frame f-pg-sel i-cdn_cliente-ini
           tt-param.i-cdn_cliente-fim = input frame f-pg-sel i-cdn_cliente-fim
           tt-param.da-dat_emis_docto-ini = input frame f-pg-sel da-dat_emis_docto-ini
           tt-param.da-dat_emis_docto-fim = input frame f-pg-sel da-dat_emis_docto-fim
           tt-param.c-nr-docto-ini        = input frame f-pg-sel c-nr-docto-ini 
           tt-param.c-nr-docto-fim        = input frame f-pg-sel c-nr-docto-fim 
           tt-param.da-dt-emissao-ini     = input frame f-pg-sel da-dt-emissao-ini 
           tt-param.da-dt-emissao-fim     = input frame f-pg-sel  da-dt-emissao-fim.


if  input frame f-pg-imp rs-destino <> 3 then do:
    if  c-arquivo:screen-value in frame f-pg-imp = "" then do:
        apply 'mouse-select-click' to im-pg-imp in frame f-relat.
        if  input frame f-pg-imp rs-destino = 1 then
            message "A impressora deve ser informada!" view-as alert-box error.
        if  input frame f-pg-imp rs-destino = 2 then
            message "O arquivo de sa�da deve ser informado!" view-as alert-box error title "Erro".
        apply 'entry' to c-arquivo in frame f-pg-imp.                   
        return error.
    end.
end.

assign  c-arquivo = c-arquivo:screen-value in frame f-pg-imp.

   if  session:set-wait-state("general") then.
    assign v-cod-prog-i-rprun = "ems5/es59041.p".


raw-transfer tt-param    to raw-param.

case int(rs-destino:screen-value in frame f-pg-imp):
    when 1 then v_cod_dwb_output          = "Impressora".
    when 2 then v_cod_dwb_output          = "Arquivo".
    when 3 then v_cod_dwb_output          = "Terminal".
end.

if  input frame f-pg-imp rs-execucao = 1 then
    assign v_dwb_run_mode = "On-Line".
else
    assign v_dwb_run_mode = "Batch".

run grapi/gr5012.p (input v_cod_dwb_program,
                    input v_cod_dwb_user,
                    input string(raw-param),
                    input v_cod_dwb_output,
                    input v_cod_dwb_order,
                    input v_dwb_run_mode,
                    input c-arquivo,
                    input v_nom_dwb_printer,
                    input v_cod_dwb_print_layout,
                    input v_nom_dwb_print_file).

run value(v-cod-prog-i-rprun) (input raw-param,
                               input table tt-raw-digita).

   if  session:set-wait-state("") then.
    def var c-key-value as char no-undo.

    if  tt-param.destino = 3 AND RETURN-VALUE <> "NOK" then do:

       IF v-cod-extens-arq = "lst" THEN DO:
          assign c-key-value = "Notepad.exe".
          run winexec (input c-key-value + chr(32) + tt-param.arquivo, input 1).
       END.
       IF v-cod-extens-arq = "pdf" THEN
          RUN OpenDocument(tt-param.arquivo).


    end.

 end.
END PROCEDURE.
PROCEDURE pi-troca-pagina:

   if  self:move-to-top()  in frame f-relat then.

   case self:name:

        when "im-pg-sel" then do with frame f-relat:
            view frame {&PGSEL}.
            if  im-pg-sel:load-image("imagem/gr-fldup") then.
                assign im-pg-sel:height = 1.20
                       im-pg-sel:row    = 1.50.
            hide frame {&PGPAR}.
            if  im-pg-par:load-image("imagem/gr-flddn") then.
            if  im-pg-par:move-to-bottom() then.
                assign im-pg-par:height = 1
                       im-pg-par:row    = 1.6.
            hide frame {&PGIMP}.
            if  im-pg-imp:load-image("imagem/gr-flddn") then.
            if  im-pg-imp:move-to-bottom() then.
                assign im-pg-imp:height = 1
                       im-pg-imp:row    = 1.6.
        end.

        when "im-pg-par" then do with frame f-relat:
            hide frame {&PGSEL}.
            if  im-pg-sel:load-image("imagem/gr-flddn") then.
            if  im-pg-sel:move-to-bottom() then.
            assign im-pg-sel:height = 1
                   im-pg-sel:row    = 1.6.
            view frame {&PGPAR}.
            if  im-pg-par:load-image("imagem/gr-fldup") then.
                assign im-pg-par:height = 1.20
                       im-pg-par:row    = 1.5.
            hide frame {&PGIMP}.
            if  im-pg-imp:load-image("imagem/gr-flddn") then.
            if  im-pg-imp:move-to-bottom() then.
                assign im-pg-imp:height = 1
                       im-pg-imp:row    = 1.6.
        end.

        when "im-pg-imp" then do with frame f-relat:
            hide frame {&PGSEL}.
            if  im-pg-sel:load-image("imagem/gr-flddn") then.
            if  im-pg-sel:move-to-bottom() then.
                assign im-pg-sel:height = 1
                       im-pg-sel:row    = 1.6.
            hide frame {&PGPAR}.
            if  im-pg-par:load-image("imagem/gr-flddn") then.
            if  im-pg-par:move-to-bottom() then.
                assign im-pg-par:height = 1
                       im-pg-par:row    = 1.6.
            view frame {&PGIMP}.
            if  im-pg-imp:load-image("imagem/gr-fldup") then.
            assign im-pg-imp:height = 1.20
                   im-pg-imp:row    = 1.5.
        end.

    end case.

    i-current-folder = lookup(self:name,c-list-folders).


END PROCEDURE.

PROCEDURE send-records :
    /* Define variables needed by this internal procedure.               */ 
    /* For each requested table, put it':Us ROWID in the output list.      */
    /* Deal with any unexpected table requests before closing.           */ 
END PROCEDURE.

PROCEDURE state-changed :
    DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
    DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
    run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

Procedure pi-muda-cor-label-folder:
   def input parameter p-cod-label as char  no-undo.
   def var wh-pai   as widget-handle.
   def var wh-filho as widget-handle.

    assign wh-pai = frame f-relat:handle
           wh-pai = wh-pai:first-child.
   do while wh-pai <> ?:
       do  while valid-handle(wh-pai):
           assign wh-filho = wh-pai:first-child.
           do  while valid-handle(wh-filho):
               if  wh-filho:type = "TEXT"
                   then
                       if  wh-filho:screen-value = p-cod-label
                       then
                           assign wh-filho:fgcolor = 7.
                       assign wh-filho = wh-filho:next-sibling.
           end.
           assign wh-pai = wh-pai:next-sibling.
       end.
   end.
END PROCEDURE.

PROCEDURE OpenDocument:

    def input param c-doc as char  no-undo.
    def var c-exec as char  no-undo.
    def var h-Inst as int  no-undo.

    assign c-exec = fill("x",255).
    run FindExecutableA (input c-doc,
                         input "",
                         input-output c-exec,
                         output h-inst).

    if h-inst >= 0 and h-inst <=32 then
      run ShellExecuteA (input 0,
                         input "open",
                         input "rundll32.exe",
                         input "shell32.dll,OpenAs_RunDLL " + c-doc,
                         input "",
                         input 1,
                         output h-inst).

    run ShellExecuteA (input 0,
                       input "open",
                       input c-doc,
                       input "",
                       input "",
                       input 1,
                       output h-inst).

    if h-inst < 0 or h-inst > 32 then return "OK".
    else return "NOK".

END PROCEDURE.

PROCEDURE FindExecutableA EXTERNAL "Shell32.dll" persistent:

    define input parameter lpFile as char  no-undo.
    define input parameter lpDirectory as char  no-undo.
    define input-output parameter lpResult as char  no-undo.
    define return parameter hInstance as long.

END.

PROCEDURE ShellExecuteA EXTERNAL "Shell32.dll" persistent:

    define input parameter hwnd as long.
    define input parameter lpOperation as char  no-undo.
    define input parameter lpFile as char  no-undo.
    define input parameter lpParameters as char  no-undo.
    define input parameter lpDirectory as char  no-undo.
    define input parameter nShowCmd as long.
    define return parameter hInstance as long.

END PROCEDURE.

