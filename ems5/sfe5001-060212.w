&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          movfin           PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.       */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE whistorico AS CHAR FORMAT "X(160)".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 RECT-4 wempresa-ini westab-ini ~
westab-fim wcliente-ini wcliente-fim wdt-emi-ini wdt-emi-fim wrel Btn_OK ~
Btn_Cancel 
&Scoped-Define DISPLAYED-OBJECTS wempresa-ini westab-ini westab-fim ~
wcliente-ini wcliente-fim wdt-emi-ini wdt-emi-fim wrel 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Cancel AUTO-END-KEY 
     LABEL "Fechar" 
     SIZE 15 BY 1
     BGCOLOR 8 FONT 1.

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "Executar" 
     SIZE 15 BY 1
     BGCOLOR 8 FONT 1.

DEFINE VARIABLE wcliente-fim LIKE tit_acr.cdn_cliente
     VIEW-AS FILL-IN 
     SIZE 13.72 BY .88
     FONT 1 NO-UNDO.

DEFINE VARIABLE wcliente-ini LIKE tit_acr.cdn_cliente
     VIEW-AS FILL-IN 
     SIZE 13.72 BY .88
     FONT 1 NO-UNDO.

DEFINE VARIABLE wdt-emi-fim LIKE tit_acr.dat_emis_docto
     VIEW-AS FILL-IN 
     SIZE 12.57 BY .88
     FONT 1 NO-UNDO.

DEFINE VARIABLE wdt-emi-ini LIKE tit_acr.dat_emis_docto
     VIEW-AS FILL-IN 
     SIZE 12.57 BY .88
     FONT 1 NO-UNDO.

DEFINE VARIABLE wempresa-ini LIKE tit_acr.cod_empresa
     VIEW-AS FILL-IN 
     SIZE 7.14 BY .79
     FONT 1 NO-UNDO.

DEFINE VARIABLE westab-fim LIKE tit_acr.cod_estab
     VIEW-AS FILL-IN 
     SIZE 7.14 BY .79
     FONT 1 NO-UNDO.

DEFINE VARIABLE westab-ini LIKE tit_acr.cod_estab
     VIEW-AS FILL-IN 
     SIZE 7.14 BY .79
     FONT 1 NO-UNDO.

DEFINE VARIABLE wrel AS CHARACTER FORMAT "X(50)":U 
     LABEL "Nome do arquivo" 
     VIEW-AS FILL-IN 
     SIZE 34 BY .88
     FONT 0 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 11.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 78 BY 1.5
     BGCOLOR 7 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     wempresa-ini AT ROW 5 COL 29 COLON-ALIGNED HELP
          "C�digo Empresa" WIDGET-ID 2
          FONT 1
     westab-ini AT ROW 6.25 COL 29 COLON-ALIGNED HELP
          "C�digo Estabelecimento" WIDGET-ID 10
          FONT 1
     westab-fim AT ROW 6.25 COL 46 COLON-ALIGNED HELP
          "C�digo Estabelecimento" NO-LABEL WIDGET-ID 12
          FONT 1
     wcliente-ini AT ROW 7.25 COL 29 COLON-ALIGNED HELP
          "C�digo Cliente" WIDGET-ID 16
          FONT 1
     wcliente-fim AT ROW 7.25 COL 46 COLON-ALIGNED HELP
          "C�digo Cliente" NO-LABEL WIDGET-ID 18
          FONT 1
     wdt-emi-ini AT ROW 8.5 COL 29 COLON-ALIGNED HELP
          "Data Emiss�o Documento" WIDGET-ID 22
          FONT 1
     wdt-emi-fim AT ROW 8.5 COL 46 COLON-ALIGNED HELP
          "Data Emiss�o Documento" NO-LABEL WIDGET-ID 24
          FONT 1
     wrel AT ROW 10.25 COL 29 COLON-ALIGNED WIDGET-ID 46
     Btn_OK AT ROW 14.75 COL 4
     Btn_Cancel AT ROW 14.75 COL 23
     "a" VIEW-AS TEXT
          SIZE 1 BY .67 AT ROW 8.75 COL 45 WIDGET-ID 26
          FONT 1
     "Dados extra�dos do EMS506" VIEW-AS TEXT
          SIZE 28 BY .67 AT ROW 2 COL 29 WIDGET-ID 28
     "a" VIEW-AS TEXT
          SIZE 2 BY .67 AT ROW 6.25 COL 45 WIDGET-ID 14
          FONT 1
     "a" VIEW-AS TEXT
          SIZE 2 BY .67 AT ROW 7.5 COL 45 WIDGET-ID 20
          FONT 1
     RECT-3 AT ROW 3.25 COL 2 WIDGET-ID 38
     RECT-4 AT ROW 14.5 COL 2 WIDGET-ID 44
     SPACE(1.28) SKIP(0.99)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "REL. T�TULOS EM ABERTO C.A RECEBER"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Cancel WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE
       FRAME Dialog-Frame:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN wcliente-fim IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.cdn_cliente EXP-SIZE                           */
/* SETTINGS FOR FILL-IN wcliente-ini IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.cdn_cliente EXP-SIZE                           */
/* SETTINGS FOR FILL-IN wdt-emi-fim IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.dat_emis_docto EXP-SIZE                        */
/* SETTINGS FOR FILL-IN wdt-emi-ini IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.dat_emis_docto EXP-SIZE                        */
/* SETTINGS FOR FILL-IN wempresa-ini IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.cod_empresa                                    */
/* SETTINGS FOR FILL-IN westab-fim IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.cod_estab EXP-SIZE                             */
/* SETTINGS FOR FILL-IN westab-ini IN FRAME Dialog-Frame
   LIKE = movfin.tit_acr.cod_estab EXP-SIZE                             */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON WINDOW-CLOSE OF FRAME Dialog-Frame /* REL. T�TULOS EM ABERTO C.A RECEBER */
DO:
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK Dialog-Frame
ON CHOOSE OF Btn_OK IN FRAME Dialog-Frame /* Executar */
DO:
     ASSIGN wempresa-ini westab-ini westab-fim wcliente-ini wcliente-fim
        wdt-emi-ini wdt-emi-fim.

    RUN relatorio.
  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
IF VALID-HANDLE(ACTIVE-WINDOW) AND FRAME {&FRAME-NAME}:PARENT eq ?
THEN FRAME {&FRAME-NAME}:PARENT = ACTIVE-WINDOW.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
   RUN INICIA.
   RUN enable_UI.
  WAIT-FOR GO OF FRAME {&FRAME-NAME}.
END.
RUN disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY wempresa-ini westab-ini westab-fim wcliente-ini wcliente-fim 
          wdt-emi-ini wdt-emi-fim wrel 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-3 RECT-4 wempresa-ini westab-ini westab-fim wcliente-ini 
         wcliente-fim wdt-emi-ini wdt-emi-fim wrel Btn_OK Btn_Cancel 
      WITH FRAME Dialog-Frame.
  VIEW FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE inicia Dialog-Frame 
PROCEDURE inicia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN   wempresa-ini = "1"
           westab-ini = " " 
           westab-fim = "ZZZ"
           wcliente-ini = 0
           wcliente-fim = 999999999
           wdt-emi-ini = TODAY
           wdt-emi-fim = TODAY
           wrel = "c:/spool/TIT" + string(TIME,"999999") + ".TXT".
  DISPLAY  wEmpresa-ini  westab-ini westab-fim wcliente-ini wcliente-fim
        wdt-emi-ini wdt-emi-fim wrel WITH FRAME    {&FRAME-NAME}.

  END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE relatorio Dialog-Frame 
PROCEDURE relatorio :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  
   OUTPUT TO VALUE(wrel) PAGED convert target "iso8859-1".   
   ASSIGN whistorico = "".
   DISABLE Btn_OK Btn_Cancel WITH FRAME {&FRAME-NAME}.
   FIND first ems5.empresa WHERE cod_empresa = wempresa-ini NO-LOCK NO-ERROR.

   FORM     tit_acr.cod_empresa cod_estab cdn_cliente cod_tit_acr tit_acr.nom_abrev cod_espec_docto
            dat_emis_docto dat_vencto_tit_acr val_origin_tit_acr dat_liquidac_tit_acr val_liq_tit_acr
   HEADER 
   ems5.empresa.nom_razao_social TODAY AT 100 "P�gina" AT 120 PAGE-NUMBER
   SKIP(2)
   "*** RELAT�RIO DE T�TULOS EM ABERTO - CONTAS A RECEBER - EMS506 **"
   
   SKIP(2)
            WITH FRAME f-rel WIDTH 300 DOWN STREAM-IO.

    FOR EACH tit_acr WHERE tit_acr.cod_empresa GE wempresa-ini
               /*      AND  cod_empresa LE wempresa-fim*/
                     AND  (cod_estab GE westab-ini
                     AND cod_estab LE westab-fim)
                     AND (cdn_cliente GE wcliente-ini
                     AND cdn_cliente LE wcliente-fim)
                     AND (dat_emis_docto GE wdt-emi-ini
                     AND dat_emis_docto LE wdt-emi-fim)
                    NO-LOCK  BREAK by cdn_cliente BY  dat_emis_docto :
     MESSAGE wcliente-ini wcliente-fim VIEW-AS ALERT-BOX.
      DISPLAY tit_acr.cod_empresa cod_estab cdn_cliente cod_tit_acr tit_acr.nom_abrev cod_espec_docto
            dat_emis_docto dat_vencto_tit_acr val_origin_tit_acr dat_liquidac_tit_acr val_liq_tit_acr
        WITH FRAME f-rel  STREAM-IO.
      FOR EACH histor_movto_tit_acr WHERE histor_movto_tit_acr.num_id_tit_acr = tit_acr.num_id_tit_acr 
                                    AND histor_movto_tit_acr.cod_estab = tit_acr.cod_estab NO-LOCK:
          
          ASSIGN whistorico = substring(string(des_text_histor,"x(2000)"),1,100).
          /*PUT SKIP "*** HIST�RICO DO T�TULO:" cod_tit_acr "***"
                  SKIP whistorico.*/
          DOWN WITH FRAME f-rel.
      END.
      /*FOR EACH histor_padr WHERE cod_histor_padr = tit_acr.cod_histor_padr NO-LOCK:
        DISPLAY SKIP(1) des_text_histor_padrao FORMAT "X(180)". 
      END. */
      END.
    OUTPUT CLOSE.
    MESSAGE "FIM DA EXECU��O" VIEW-AS ALERT-BOX.
    /*DOS SILENT OPEN notepad.exe  "c:/spool/tit.txt" .*/
END PROCEDURE.
ENABLE Btn_OK Btn_Cancel WITH FRAME {&FRAME-NAME}.    
DOS SILENT notepad.exe "c:/spool/tit.txt".

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

