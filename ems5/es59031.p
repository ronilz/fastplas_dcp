/*****************************************************************************
**       Programa: es59031.p
**       Data....: 09/05/12
**       Autor...: DATASUL S.A.
**       Objetivo: RELAT�RIO DE ACERTOS - CONTAS A RECEBER
**       Vers�o..: 1.00.000 - super
**       OBS.....: Este fonte foi gerado pelo Data Viewer 3.00
**       AUTOR   : VSH 
*******************************************************************************/
                                      
define variable c-prog-gerado as character no-undo initial "es59031".
/****************** Defini��o de Tabelas Tempor�rias do Relat�rio **********************/

define temp-table tt-raw-digita
    field raw-digita as raw.

define temp-table tt-param
    field destino              as integer
    field arquivo              as char
    field usuario              as char
    field data-exec            as date
    field hora-exec            as integer
    field parametro            as logical
    field formato              as integer
    field v_num_tip_aces_usuar as integer
    field ep-codigo            as integer
    field c-cod_empresa-ini like movfin.tit_acr.cod_empresa
    field c-cod_empresa-fim like movfin.tit_acr.cod_empresa
    field c-cod_estab-ini like movfin.tit_acr.cod_estab
    field c-cod_estab-fim like movfin.tit_acr.cod_estab
    field i-cdn_cliente-ini like movfin.tit_acr.cdn_cliente
    field i-cdn_cliente-fim like movfin.tit_acr.cdn_cliente
    field da-dat_emis_docto-ini like movfin.tit_acr.dat_emis_docto
    field da-dat_emis_docto-fim like movfin.tit_acr.dat_emis_docto
    FIELD c-nr-docto-ini like mov-tit.nr-docto  
    field c-nr-docto-fim like mov-tit.nr-docto. 
    /*
    field da-dt-emissao-ini like mov-tit.dt-emissao  
    field da-dt-emissao-fim like mov-tit.dt-emissao.*/


/****************** INCLUDE COM VARI�VEIS GLOBAIS *********************/

def new global shared var c-seg-usuario as char format "x(12)" no-undo.
def new global shared var v_cod_usuar_corren as character format "x(12)" label "Usu�rio Corrente" column-label "Usu�rio Corrente" no-undo.
def new global shared var i-num-ped-exec-rpw  as integer no-undo.
def new global shared var v_cod_empres_usuar  as character format "x(3)"  label "Empresa" column-label "Empresa" no-undo. 
def new global shared var v_cod_dir_spool_servid_exec as CHARACTER format "x(8)":U no-undo.

def var v_cod_dwb_parameters as character format "x(8)" no-undo.
def var v_Cod_prog_chamador as character no-undo.
def var v_ind_tip_exec      as integer   no-undo.
def var v_rec_log           as recid     no-undo.
def var v_cod_private       as character no-undo.
def var i-ep-codigo-usuario as integer no-undo.
def var v_cdn_empres_usuar as integer no-undo.
def var v_cod_dwb_user as character format "x(12)" label "Usu�rio" column-label "Usu�rio" no-undo.
def var v_cod_dwb_order as character format "x(32)" label "Classifica��o" column-label "Classificador" no-undo.
def var v_nom_dwb_printer      like emsbas.dwb_rpt_param.nom_dwb_printer.
def var v_cod_dwb_print_layout like emsbas.dwb_rpt_param.cod_dwb_print_layout.
def var v_nom_dwb_print_file   as character format "x(100)"  label "Arquivo Impress�o"  column-label "Arq Impr"  no-undo.
def var v_dwb_run_mode as character no-undo.
def var v_cod_dwb_output as character no-undo.
def var v_cod_dwb_file_temp as character format "x(12)" no-undo.

def new shared stream str-rp.

def new global shared var c-dir-spool-servid-exec as CHAR no-undo.
/****************** Defini�ao de Par�metros do Relat�rio *********************/ 

/****************** Defini�ao de Vari�veis de Sele��o do Relat�rio *********************/ 

def new shared var c-cod_empresa-ini like movfin.tit_acr.cod_empresa format "x(3)" initial "" no-undo.
def new shared var c-cod_empresa-fim like movfin.tit_acr.cod_empresa format "x(3)" initial "ZZZ" no-undo.
def new shared var c-cod_estab-ini like movfin.tit_acr.cod_estab format "x(3)" initial "" no-undo.
def new shared var c-cod_estab-fim like movfin.tit_acr.cod_estab format "x(3)" initial "ZZZ" no-undo.
def new shared var i-cdn_cliente-ini like movfin.tit_acr.cdn_cliente format ">>>,>>>,>>9" initial 0 no-undo.
def new shared var i-cdn_cliente-fim like movfin.tit_acr.cdn_cliente format ">>>,>>>,>>9" initial 999999999 no-undo.
def new shared var da-dat_emis_docto-ini like movfin.tit_acr.dat_emis_docto format "99/99/9999" initial "01/01/1800" no-undo.
def new shared var da-dat_emis_docto-fim like movfin.tit_acr.dat_emis_docto format "99/99/9999" initial "12/31/9999" no-undo.
def new shared var c-nr-docto-ini like mov-tit.nr-docto format "x(16)" initial "" no-undo.
def new shared var c-nr-docto-fim like mov-tit.nr-docto format "x(16)" initial "ZZZZZZZZZZZZZZZZ" no-undo.
/*
def new shared var da-dt-emissao-ini like mov-tit.dt-emissao format "99/99/9999" initial "01/01/0001" no-undo.
def new shared var da-dt-emissao-fim like mov-tit.dt-emissao format "99/99/9999" initial "12/31/9999" no-undo.
*/

/****************** Defini�ao de Vari�veis p/ Campos Virtuais do Relat�rio *******************/ 

/****************** Defini�ao de Vari�veis Campo Calculado do Relat�rio **********************/ 

/****************** Defini�ao de Vari�veis do Relat�rio N�o Pedidas em Tela ******************/ 

/****************** Defini�ao de Vari�veis de Total do Relat�rio *****************************/ 

/****************** Defini�ao de Vari�veis dos Calculos do Relat�rio *************************/ 

def input param raw-param as raw no-undo.
def input param table for tt-raw-digita.

/***************** Defini�ao de Vari�veis de Processamento do Relat�rio *********************/

def var h-acomp              as handle no-undo.
def var h-FunctionLibrary    as handle no-undo.
def var v-cod-destino-impres as char   no-undo.
def var v-num-reg-lidos      as int    no-undo.
def var v-num-point          as int    no-undo.
def var v-num-set            as int    no-undo.
def var v-num-linha          as int    no-undo.
def var v-cont-registro      as int    no-undo.
def var v-des-retorno        as char   no-undo.
def var v-des-local-layout   as char   no-undo.

/****************** Defini�ao de Forms do Relat�rio 132 Colunas ***************************************/ 

form /*movfin.tit_acr.cod_empresa column-label "Empresa" format "x(3)" at 001
     movfin.tit_acr.cod_estab column-label "Estab" format "x(3)" at 009
     movfin.tit_acr.cod_tit_acr column-label "T�tulo" format "x(10)" at 015
     movfin.tit_acr.cod_portador column-label "Portador" format "x(5)" at 026
     movfin.tit_acr.cdn_cliente column-label "Cliente" format ">>>,>>>,>>9" at 035
     movfin.tit_acr.nom_abrev column-label "Nome Abreviado" format "x(15)" at 047
     movfin.tit_acr.cod_espec_docto column-label "Esp�cie" format "x(3)" at 063
     movfin.tit_acr.cod_ser_docto column-label "S�rie" format "x(3)" at 071
     movfin.tit_acr.dat_emis_docto column-label "Dt Emiss�o" format "99/99/9999" at 077
     movfin.tit_acr.val_origin_tit_acr column-label "Vl Original T�tulo" format ">>>,>>>,>>9.99" at 088
     movfin.tit_acr.dat_transacao column-label "Dat Transac" format "99/99/9999" at 107
     movfin.tit_acr.val_sdo_tit_acr column-label "Saldo T�tulo" format ">>>,>>>,>>9.99" at 119 skip
     movfin.histor_movto_tit_acr.des_text_histor column-label "Hist�rico" format "x(60)" view-as fill-in  at 073 skip
     movfin.histor_movto_tit_acr.num_seq_histor_movto_acr column-label "Sequ�ncia" format ">>>>,>>9" at 073 */
     with down width 320 no-box stream-io frame f-relat-09-132.

create tt-param.
raw-transfer raw-param to tt-param.

def temp-table tt-editor no-undo
    field linha      as integer
    field conteudo   as character format "x(80)"
    index editor-id is primary unique linha.


define var c-empresa      as character format "x(40)" no-undo.
define var c-titulo-relat as character format "x(50)" no-undo.
define var c-sistema      as character format "x(25)" no-undo.
define var i-numper-x     as integer   format "ZZ"    no-undo.
define var da-iniper-x    as date format "99/99/9999" no-undo.
define var da-fimper-x    as date format "99/99/9999" no-undo.
define var c-rodape       as character                no-undo.
define var v_num_count    as integer                  no-undo.

define var c-programa     as character format "x(08)" no-undo.
define var c-versao       as character format "x(04)" no-undo.
define var c-revisao      as character format "999"   no-undo.

define var c-impressora   as char no-undo.
define var c-layout       as char no-undo.
def shared var v_cod_dwb_program as character format "x(32)" label "Programa" column-label "Programa" no-undo.

/* defini��o valeria */
DEFINE VARIABLE whistorico AS CHAR FORMAT "X(150)" COLUMN-LABEL "Hist�rico".
DEFINE VARIABLE wnat       LIKE nota-fiscal.nat-oper.
DEFINE VARIABLE wtrans     LIKE  movto_tit_acr.ind_trans_acr.

assign c-programa     = "es59031"
       c-versao       = "2.00"
       c-revisao      = ".00.000"
       c-titulo-relat = "RELAT�RIO DE ACERTOS - CONTAS A RECEBER"
       c-sistema      = "".


find first emsuni.empresa
    where  emsuni.empresa.cod_empresa = v_cod_empres_usuar
     no-lock no-error.
    
if  avail emsuni.empresa then
    assign c-empresa = emsuni.empresa.nom_razao_social.
else
    assign c-empresa = "".

if  tt-param.formato = 2 then do:


form header
    fill("-", 132) format "x(132)" skip
    c-empresa      at 1
    c-titulo-relat at 50
    "Pag:" at 123 page-number(str-rp) at 128 format ">>>>9" skip
    fill("-", 112) format "x(110)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabec.

form header
    fill("-", 132) format "x(132)" skip
    c-titulo-relat at 50
    "Pag:" at 123 page-number(str-rp) at 128 format ">>>>9" skip
    fill("-", 112) format "x(110)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabper.

form header
    fill("-", 178) format "x(177)"
    "EMS506 - PROCEDIMENTOS ESPECIAIS - ES5903a"
    with stream-io width 222 no-labels no-box page-bottom frame f-rodape.

end. /* tt-param.formato = 2 */




assign v_cod_prog_chamador = v_cod_dwb_program.

if  index(program-name(2),'gr5010') = 0 then do:

    run grapi/gr5005a.p (input tt-param.destino,
                         input v_cod_prog_chamador,
                         input tt-param.usuario,
                         input lc(tt-param.arquivo),
                         input no).

    if  return-value = "NOK" then
        return.

    if  return-value = "0" then
        return.
    
    if  return-value <> "NOK" and
        return-value <> "0"   and
        return-value <> "OK"  and
        return-value <> ""    then do:
        message "Criado pedido " return-value " para execu��o batch." view-as alert-box information title "Informa��o".
        return.
    end.
end.
else do:
    IF v_cod_dir_spool_servid_exec <> "" THEN DO:
       IF substring(v_cod_dir_spool_servid_exec,LENGTH(v_cod_dir_spool_servid_exec),1) <> "~/" OR
          substring(v_cod_dir_spool_servid_exec,LENGTH(v_cod_dir_spool_servid_exec),1) <> "~\" 
          THEN ASSIGN v_cod_dir_spool_servid_exec = v_cod_dir_spool_servid_exec + "~\".
    end.
    ASSIGN v_cod_dir_spool_servid_exec = replace(v_cod_dir_spool_servid_exec, "~\", "~/").
                                                                           

    ASSIGN tt-param.arquivo = v_cod_dir_spool_servid_exec + tt-param.arquivo.
    
    run grapi/gr5011a.p (input tt-param.destino,
                         input v_cod_prog_chamador,
                         input tt-param.usuario,
                         input lc(tt-param.arquivo),
                         input no). 
end.


assign i-ep-codigo-usuario = tt-param.ep-codigo
       v_cdn_empres_usuar  = i-ep-codigo-usuario
       c-cod_empresa-ini = tt-param.c-cod_empresa-ini
       c-cod_empresa-fim = tt-param.c-cod_empresa-fim
       c-cod_estab-ini = tt-param.c-cod_estab-ini
       c-cod_estab-fim = tt-param.c-cod_estab-fim
       i-cdn_cliente-ini = tt-param.i-cdn_cliente-ini
       i-cdn_cliente-fim = tt-param.i-cdn_cliente-fim
       da-dat_emis_docto-ini = tt-param.da-dat_emis_docto-ini
       da-dat_emis_docto-fim = tt-param.da-dat_emis_docto-fim
       c-nr-docto-ini         = tt-param.c-nr-docto-ini 
       c-nr-docto-fim         = tt-param.c-nr-docto-fim.
/*
       da-dt-emissao-ini      = tt-param.da-dt-emissao-ini
       da-dt-emissao-fim      = tt-param.da-dt-emissao-fim.*/

def var l-imprime as logical no-undo.

assign l-imprime = no.
if  tt-param.destino = 1 then
    assign v-cod-destino-impres = "Impressora".
else
    if  tt-param.destino = 2 then
        assign v-cod-destino-impres = "Arquivo".
    else
        assign v-cod-destino-impres = "Terminal".


run grapi/gr5008.p persistent set h-acomp.

run pi-inicializar in h-acomp(input "Acompanhamento Relat�rio").

assign v-num-reg-lidos = 0.

/* gr9020a.p */
ASSIGN wnat = " " .
for each movfin.tit_acr NO-LOCK
         where movfin.tit_acr.cdn_cliente    >= i-cdn_cliente-ini and 
               movfin.tit_acr.cdn_cliente    <= i-cdn_cliente-fim and
               movfin.tit_acr.cod_empresa    >= c-cod_empresa-ini and 
               movfin.tit_acr.cod_empresa    <= c-cod_empresa-fim and
               movfin.tit_acr.cod_estab      >= c-cod_estab-ini and 
               movfin.tit_acr.cod_estab      <= c-cod_estab-fim and
              (movfin.tit_acr.cod_tit_acr   GE c-nr-docto-ini           AND
               movfin.tit_acr.cod_tit_acr   LE c-nr-docto-fim)          AND 
               tit_acr.cod_espec_docto = "DU"
                /*    AND
               (movfin.tit_acr.dat_emis_docto GE da-dt-emissao-ini  AND
               movfin.tit_acr.dat_emis_docto LE da-dt-emissao-fim)  */
               BREAK by cdn_cliente BY  dat_transacao:

          /* natureza de operacao */
    FIND FIRST nota-fiscal WHERE nr-nota-fis = movfin.tit_acr.cod_tit_acr 
                           AND dt-emis-nota =   movfin.tit_acr.dat_emis_docto 
                           AND nota-fiscal.cod-emitente = movfin.tit_acr.cdn_cliente NO-LOCK NO-ERROR.
 
    
    IF AVAILABLE nota-fiscal THEN ASSIGN wnat = nota-fiscal.nat-operacao.

    FOR EACH movto_tit_acr OF tit_acr WHERE movto_tit_acr.ind_trans_acr BEGINS "Acerto Valor" 
                                      AND (movto_tit_acr.dat_transacao  >= da-dat_emis_docto-ini and 
                                           movto_tit_acr.dat_transacao  <= da-dat_emis_docto-fim)  
                                      NO-LOCK:
                                      /*
                                      AND   ind_trans_acr = "Acerto Valor a cr�dito"
                                      AND   ind_trans_acr = "Acerto Valor a menor" */

    /*  MESSAGE whistorico VIEW-AS ALERT-BOX. */
    assign v-num-reg-lidos = v-num-reg-lidos + 1.
    run pi-acompanhar in h-acomp("Data Transa��o: " + STRING(movto_tit_acr.dat_transacao)).

    /***  C�DIGO PARA SA�DA EM 132 COLUNAS ***/

    if  tt-param.formato = 2 then do:

        view stream str-rp frame f-cabec.
        view stream str-rp frame f-rodape.
        assign l-imprime = yes.
       
        display stream str-rp 
            movto_tit_acr.cod_empresa
            movto_tit_acr.cod_estab
            tit_acr.cod_ser_docto
            tit_acr.cod_espec_docto 
            tit_acr.cdn_cliente
            tit_acr.nom_abrev
            movfin.tit_acr.cod_tit_acr
            /* parcela */
            tit_acr.dat_emis_docto
            tit_acr.dat_vencto_tit_acr   
            tit_acr.val_origin_tit_acr           
            movto_tit_acr.dat_transacao COLUMN-LABEL "Dt.Trans.Mvto"  
            movto_tit_acr.ind_trans_acr 
            movto_tit_acr.cod_refer 
            tit_acr.des_obs_cobr                   
            movto_tit_acr.dat_cr_movto_tit_acr
            movto_tit_acr.val_movto_tit_acr
            movto_tit_acr.val_sdo_tit_acr 
            wnat
            tit_acr.cod_portador
            with stream-io frame f-relat-09-132.
                                                                                               
      /*  for each tt-editor:
                delete tt-editor.
            end.
            run pi-print-editor (input movfin.histor_movto_tit_acr.des_text_histor,
                                 input 100).
            for each tt-editor:
                if  tt-editor.linha = 1 then do:
                        disp stream str-rp tt-editor.conteudo @ movfin.histor_movto_tit_acr.des_text_histor with stream-io frame f-relat-09-132.
                    down stream str-rp with frame f-relat-09-132.
                end.
                else
                    put stream str-rp unformatted tt-editor.conteudo at 001.
            end.                                         */ 
            down stream str-rp with frame f-relat-09-132.
 
       end.
    END.
    end.
   

if  l-imprime = no then do:
    if  tt-param.formato = 2 then do:
        view stream str-rp frame f-cabec.
        view stream str-rp frame f-rodape.
    end.
    disp stream str-rp " " with stream-io frame f-nulo.
end.

if  tt-param.destino <> 1 then

    page stream str-rp.

else do:

    if   tt-param.parametro = yes then

         page stream str-rp.

end.

if  tt-param.parametro then do:


   disp stream str-rp "SELE��O" skip(01) with stream-io frame f-imp-sel.
   disp stream str-rp 
      c-cod_empresa-ini colon 17 "|< >|"   at 41 c-cod_empresa-fim no-label
      c-cod_estab-ini colon 17 "|< >|"   at 41 c-cod_estab-fim no-label
      i-cdn_cliente-ini colon 17 "|< >|"   at 41 i-cdn_cliente-fim no-label
      da-dat_emis_docto-ini colon 17 "|< >|"   at 41 da-dat_emis_docto-fim no-label
      da-dat_emis_docto-ini colon 17 "|< >|"   at 41 da-dat_emis_docto-fim no-label
      da-dat_emis_docto-ini colon 17 "|< >|"   at 41 da-dat_emis_docto-fim no-label
      da-dat_emis_docto-ini colon 17 "|< >|"   at 41 da-dat_emis_docto-fim no-label
      da-dat_emis_docto-ini colon 17 "|< >|"   at 41 da-dat_emis_docto-fim no-label
     with stream-io side-labels overlay row 032 frame f-imp-sel.

   put stream str-rp unformatted skip(1) "IMPRESS�O" skip(1).

   put stream str-rp unformatted skip "    " "Destino : " v-cod-destino-impres " - " tt-param.arquivo format "x(40)".
   put stream str-rp unformatted skip "    " "Execu��o: " if i-num-ped-exec-rpw = 0 then "On-Line" else "Batch".
   put stream str-rp unformatted skip "    " "Formato : " if tt-param.formato = 1 then "80 colunas" else "132 colunas".
   put stream str-rp unformatted skip "    " "Usu�rio : " tt-param.usuario.

end.

    output stream str-rp close.

procedure pi-print-editor:

    def input param c-editor    as char    no-undo.
    def input param i-len       as integer no-undo.

    def var i-linha  as integer no-undo.
    def var i-aux    as integer no-undo.
    def var c-aux    as char    no-undo.
    def var c-ret    as char    no-undo.

    for each tt-editor:
        delete tt-editor.
    end.

    assign c-ret = chr(255) + chr(255).

    do  while c-editor <> "":
        if  c-editor <> "" then do:
            assign i-aux = index(c-editor, chr(10)).
            if  i-aux > i-len or (i-aux = 0 and length(c-editor) > i-len) then
                assign i-aux = r-index(c-editor, " ", i-len + 1).
            if  i-aux = 0 then
                assign c-aux = substr(c-editor, 1, i-len)
                       c-editor = substr(c-editor, i-len + 1).
            else
                assign c-aux = substr(c-editor, 1, i-aux - 1)
                       c-editor = substr(c-editor, i-aux + 1).
            if  i-len = 0 then
                assign entry(1, c-ret, chr(255)) = c-aux.
            else do:
                assign i-linha = i-linha + 1.
                create tt-editor.
                assign tt-editor.linha    = i-linha
                       tt-editor.conteudo = c-aux.
            end.
        end.
        if  i-len = 0 then
            return c-ret.
    end.
    return c-ret.
end procedure.

IF VALID-HANDLE(h-acomp) THEN /*gr9030g*/
    RUN pi-finalizar IN h-acomp NO-ERROR.

return 'OK'.

/* fim do programa */
