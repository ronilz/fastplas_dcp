&Scoped-define WINDOW-NAME C-Win

/*------------------------------------------------------------------------
File.............: es5902.w
Description......: Tela que apresenta os relat�rios do Procedimento
Author...........: DATASUL S.A.
Created..........: 26/03/12 - 13:27 - super
OBS..............: Este fonte foi gerado pelo Data Viewer
------------------------------------------------------------------------*/

/* AUTOR:VSH - 16/04/12 - TITULOS BAIXADOS */

define variable c-prog-gerado as character no-undo initial "ES5902".

def new shared var v_cod_dwb_program as character format "x(32)" label "Programa" column-label "Programa" no-undo.


CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

def var c-impressora-old     as char    no-undo. 
def var c-programa         as char                          no-undo.              
def var c-arquivo          as char                          no-undo.              
def var c-arq-old          as char                          no-undo.              
def var c-lista            as char init "" no-undo.              
def var c-cod-proced       as char                          no-undo.              
def var c-prog             as char init "" no-undo.              
def var c-titulo           as char init "RELAT�RIO DE T�TULOS EM ABERTO - CONTAS A RECEBER" no-undo.        
def var v-cod-prog-gerado  as char init "es5902" no-undo.           

def var v-cod-extens-arq     as char    no-undo initial "lst". 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define FRAME-NAME DEFAULT-FRAME                                   

/* ***********************  Control Definitions  ********************** */

DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.                                

DEFINE BUTTON bt-ajuda 
     LABEL "Ajuda"
     SIZE 10 BY 1.

DEFINE BUTTON bt-cancelar AUTO-END-KEY 
     LABEL "Cancelar"
     SIZE 10 BY 1.

DEFINE BUTTON bt-ok 
     LABEL "Executar"
     SIZE 10 BY 1.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 49.72 BY 1.38
     BGCOLOR 7 .

DEFINE VARIABLE SELECT-1 AS CHARACTER 
     VIEW-AS SELECTION-LIST                               SINGLE                               SCROLLBAR-VERTICAL                               SCROLLBAR-HORIZONTAL 
     SIZE 49 BY 5.46 NO-UNDO.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     SELECT-1 AT ROW 1.08 COL 2 NO-LABEL
     bt-ok AT ROW 7.17 COL 2.57
     bt-cancelar AT ROW 7.17 COL 13.57
     bt-ajuda AT ROW 7.17 COL 40.43
     RECT-1 AT ROW 6.96 COL 1.57
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 51 BY 7.54.

/* *************************  Create Window  ************************** */

IF SESSION:DISPLAY-TYPE = "GUI" THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         HEIGHT             = 7.54
         WIDTH              = 51.14
         MAX-HEIGHT         = 16
         MAX-WIDTH          = 80
         VIRTUAL-HEIGHT     = 16
         VIRTUAL-WIDTH      = 80
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.

/* ***************  Runtime Attributes and UIB Settings  ************** */

IF SESSION:DISPLAY-TYPE = "GUI" AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* ************************* Included-Libraries *********************** */


def new global shared var c-seg-usuario as char format "x(12)" no-undo.
def new global shared var v_cod_usuar_corren as character format "x(12)" label "Usu�rio Corrente" column-label "Usu�rio Corrente" no-undo.
def new global shared var i-num-ped-exec-rpw  as integer no-undo.
def new global shared var v_cod_empres_usuar  as character format "x(3)"  label "Empresa" column-label "Empresa" no-undo. 
def new global shared var v_cod_dir_spool_servid_exec as CHARACTER format "x(8)":U no-undo.

def var v_cod_dwb_parameters as character format "x(8)" no-undo.
def var v_Cod_prog_chamador as character no-undo.
def var v_ind_tip_exec      as integer   no-undo.
def var v_rec_log           as recid     no-undo.
def var v_cod_private       as character no-undo.
def var i-ep-codigo-usuario as integer no-undo.
def var v_cdn_empres_usuar as integer no-undo.
def var v_cod_dwb_user as character format "x(12)" label "Usu�rio" column-label "Usu�rio" no-undo.
def var v_cod_dwb_order as character format "x(32)" label "Classifica��o" column-label "Classificador" no-undo.
def var v_nom_dwb_printer      like emsbas.dwb_rpt_param.nom_dwb_printer.
def var v_cod_dwb_print_layout like emsbas.dwb_rpt_param.cod_dwb_print_layout.
def var v_nom_dwb_print_file   as character format "x(100)"  label "Arquivo Impress�o"  column-label "Arq Impr"  no-undo.
def var v_dwb_run_mode as character no-undo.
def var v_cod_dwb_output as character no-undo.
def var v_cod_dwb_file_temp as character format "x(12)" no-undo.

def new shared stream str-rp.


/* ************************  Control Triggers  ************************ */

ON END-ERROR OF C-Win 
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

ON WINDOW-CLOSE OF C-Win
DO:
  APPLY "CLOSE" TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

ON CHOOSE OF bt-ajuda IN FRAME DEFAULT-FRAME
OR HELP OF FRAME {&FRAME-NAME}
DO:

if  this-procedure:private-data = "HLP=00" or
    this-procedure:private-data = "" then do:
    message "N�o existe um arquivo de ajuda para este relat�rio!" skip
            "Entre em contato com o administrador do m�dulo para solicitar o mesmo!"
            view-as alert-box information title "Ajuda".
    return.
end.

run prgtec/men/men900za.py (input self:frame,
                            input this-procedure:handle).

END.

ON CHOOSE OF bt-cancelar IN FRAME DEFAULT-FRAME
DO:
  apply "close" to this-procedure.
END.

ON CHOOSE OF bt-ok IN FRAME DEFAULT-FRAME
DO:
  if select-1:screen-value in frame {&frame-name} <> ? then do:
    if select-1:num-items in frame {&frame-name} <> 0 then
      run pi-selecao.
    RUN enable_UI.
  end.
END.

ON MOUSE-SELECT-DBLCLICK OF SELECT-1 IN FRAME DEFAULT-FRAME
DO:
    apply "choose" to bt-ok in frame {&frame-name}.
END.

/* ***************************  Main Block  *************************** */

ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME}
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

PAUSE 0 BEFORE-HIDE.

assign v-cod-prog-gerado = "es5902".

run grapi/gr5001.p (output v_cod_dwb_user).
assign v_cod_dwb_program = v-cod-prog-gerado
       c-seg-usuario     = v_cod_dwb_user.

run grapi/gr5002.p (input  v_cod_dwb_program,
                      output v_cod_private).

assign this-procedure:private-data = v_cod_private.

if  search("prgtec/btb/btb906za.r") = ? and search("prgtec/btb/btb906za.py") = ? then do:
    message "Programa execut�vel n�o foi encontrado: prgtec/btb/btb906za.py" view-as alert-box error buttons ok title "Erro".
    stop.
end.
else
    run prgtec/btb/btb906za.py.
if  v_cod_dwb_user = "" 
then
    assign v_cod_dwb_user = v_cod_usuar_corren.

if  c-seg-usuario = "" 
then
    assign c-seg-usuario = v_cod_usuar_corren.
    
if  search("prgtec/men/men901za.r") = ? and 
    search("prgtec/men/men901za.py") = ? then do:
    message "Programa execut�vel n�o foi encontrado: prgtec/men/men901za.py"
           view-as alert-box error buttons ok title "Erro".
    return.
end.
else
    run prgtec/men/men901za.py (Input v_cod_dwb_program).

if  return-value = "2014" then do:
    message "Programa a ser executado n�o � um programa v�lido Datasul !" view-as alert-box warning title "Aviso".
    return error.
end.
if  return-value = "2012" then do:
    message "Usu�rio sem permiss�o para acessar o programa." view-as alert-box warning title "Aviso".
    return error.
end.

run grapi/gr5003.p (input  v_cod_dwb_program,
                    input  v_cod_dwb_user,
                    output v_rec_log).


MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK 
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

        assign c-lista = "".

        assign c-prog = "es5902a.w".


run grapi/gr5009.p (input {&window-name}:handle,
                    input v-cod-prog-gerado,
                    input-output c-lista,
                    input-output c-prog).

  assign select-1:list-items in frame {&frame-name} = c-lista.

  if c-lista <> "" then
    assign select-1:screen-value in frame {&frame-name} = entry(1, select-1:list-items in frame {&frame-name}).

  RUN enable_UI.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* **********************  Internal Procedures  *********************** */

PROCEDURE disable_UI :
  IF SESSION:DISPLAY-TYPE = "GUI" AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

PROCEDURE enable_UI :
  DISPLAY SELECT-1 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE SELECT-1 /* bt-gerador */ RECT-1 bt-ok bt-cancelar bt-ajuda
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

PROCEDURE local-destroy :
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
END PROCEDURE.

PROCEDURE pi-selecao :
selecao:
do on error undo selecao, retry selecao:

  if select-1:num-items in frame {&frame-name}<> 0 then do:
    assign c-programa = entry(select-1:lookup(select-1:screen-value in frame {&FRAME-NAME}) in frame {&FRAME-NAME}, c-prog)
           c-programa = "ems5" + "/" + c-programa.


    assign c-programa = entry(select-1:lookup(select-1:screen-value in frame {&FRAME-NAME}) in frame {&FRAME-NAME}, c-prog).

    run value(c-programa).

  end.
end.

hide frame {&FRAME-NAME}.

END PROCEDURE.
