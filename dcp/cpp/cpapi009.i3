/*****************************************************************************************************
 **
 **    INCLUDE.: CPAPI009.I3
 **
 **    OBJETIVO: SOMAR A QUANTIDADE DOS COMPONENTES E CARREGAR AS TEMP-TABLES
 **                      TT-RESERVAS, TT-DEPOSITO E TT-IT-RESERVAS.
 **
 ******************************************************************************************************/
 
 procedure pi-carrega-tt-reservas.
 
    do on error undo, return error:

       for each tt-reservas:
           delete tt-reservas.
       end.            
    
       for each tt-ord-prod-aux,
           each reservas
           where reservas.nr-ord-prod = tt-ord-prod-aux.nr-ord-prod
             and reservas.estado      = 1 no-lock:
           create tt-reservas.
           buffer-copy reservas to tt-reservas
           assign tt-reservas.rw-reserva = rowid (reservas)
                  tt-reservas.tipo-ordem = tt-ord-prod-aux.tipo.
       end.
    end.

 end procedure.
