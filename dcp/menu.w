&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: 

  Description: from cntnrwin.w - ADM SmartWindow Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: New V9 Version - January 15, 1998
          
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

{src/adm2/widgetprto.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn1 Btn-16 Btn-31 Btn-2 Btn-17 Btn-32 Btn-3 ~
Btn-18 Btn-33 Btn-4 Btn-19 Btn-5 Btn-20 Btn-6 Btn-21 Btn-7 Btn-22 Btn-8 ~
Btn-23 Btn-9 Btn-24 Btn-10 Btn-25 Btn-11 Btn-26 Btn-12 Btn-27 Btn-13 Btn-28 ~
Btn-14 Btn-29 Btn-15 Btn-30 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn-10 AUTO-GO DEFAULT 
     LABEL "esdc010 - RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-11 AUTO-GO DEFAULT 
     LABEL "esdc011 - RG Ordem" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-12 AUTO-GO DEFAULT 
     LABEL "esdc012 - Gerar RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-13 AUTO-GO DEFAULT 
     LABEL "esdc013 - Extrato RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-14 AUTO-GO DEFAULT 
     LABEL "esdc014 - Etiqueta" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-15 AUTO-GO DEFAULT 
     LABEL "esdc015 - Movto RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-16 AUTO-GO DEFAULT 
     LABEL "esdc016 - Permissao" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-17 AUTO-GO DEFAULT 
     LABEL "esdc017 - Perm Pto Ctrl" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-18 AUTO-GO DEFAULT 
     LABEL "esdc018 - Etq. RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-19 AUTO-GO DEFAULT 
     LABEL "esdc019 - Baixa RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-2 AUTO-GO DEFAULT 
     LABEL "esdc002 - Jun��o" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-20 AUTO-GO DEFAULT 
     LABEL "esdc020 - Retrabalho" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-21 AUTO-GO DEFAULT 
     LABEL "esdc021 - Tempo Estoqu" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-22 AUTO-GO DEFAULT 
     LABEL "esdc022 - Rel Rack" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-23 AUTO-GO DEFAULT 
     LABEL "esdc023 - Cancel RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-24 AUTO-GO DEFAULT 
     LABEL "esdc024 - Rel Sit RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-25 AUTO-GO DEFAULT 
     LABEL "esdc025 - Cor Semi Acab" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-26 AUTO-GO DEFAULT 
     LABEL "esdc026 - Elimina Mov" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-27 AUTO-GO DEFAULT 
     LABEL "esdc027 - Rel Ordem RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-28 AUTO-GO DEFAULT 
     LABEL "esdc028 - Ativa RG" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-29 AUTO-GO DEFAULT 
     LABEL "esdc029 - Elim Movto" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-3 AUTO-GO DEFAULT 
     LABEL "esdc003 - Aprova��o" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-30 AUTO-GO DEFAULT 
     LABEL "esdc30 - Rel Dupl Estoq" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-31 AUTO-GO DEFAULT 
     LABEL "esdc31 - Monit. Transacao" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-32 AUTO-GO DEFAULT 
     LABEL "esdc32 - Elim Movto" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-33 AUTO-GO DEFAULT 
     LABEL "esdc70 - Criar Rack" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-4 AUTO-GO DEFAULT 
     LABEL "esdc004 - Cor" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-5 AUTO-GO DEFAULT 
     LABEL "esdc005 - Pto Contr" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-6 AUTO-GO DEFAULT 
     LABEL "esdc006 - Dep" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-7 AUTO-GO DEFAULT 
     LABEL "esdc007 - Local" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-8 AUTO-GO DEFAULT 
     LABEL "esdc008 - Programas" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn-9 AUTO-GO DEFAULT 
     LABEL "esdc009 - Permiss" 
     SIZE 21 BY 1
     BGCOLOR 8 .

DEFINE BUTTON Btn1 AUTO-GO DEFAULT 
     LABEL "esdc001 - Rack" 
     SIZE 21 BY 1
     BGCOLOR 8 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     Btn1 AT ROW 1.75 COL 3 WIDGET-ID 2
     Btn-16 AT ROW 1.75 COL 26 WIDGET-ID 32
     Btn-31 AT ROW 1.75 COL 49 WIDGET-ID 62
     Btn-2 AT ROW 2.75 COL 3 WIDGET-ID 4
     Btn-17 AT ROW 2.75 COL 26 WIDGET-ID 34
     Btn-32 AT ROW 2.75 COL 49 WIDGET-ID 64
     Btn-3 AT ROW 3.75 COL 3 WIDGET-ID 6
     Btn-18 AT ROW 3.75 COL 26 WIDGET-ID 36
     Btn-33 AT ROW 3.75 COL 49 WIDGET-ID 66
     Btn-4 AT ROW 4.75 COL 3 WIDGET-ID 8
     Btn-19 AT ROW 4.75 COL 26 WIDGET-ID 38
     Btn-5 AT ROW 5.75 COL 3 WIDGET-ID 10
     Btn-20 AT ROW 5.75 COL 26 WIDGET-ID 40
     Btn-6 AT ROW 6.75 COL 3 WIDGET-ID 12
     Btn-21 AT ROW 6.75 COL 26 WIDGET-ID 42
     Btn-7 AT ROW 7.75 COL 3 WIDGET-ID 14
     Btn-22 AT ROW 7.75 COL 26 WIDGET-ID 44
     Btn-8 AT ROW 8.75 COL 3 WIDGET-ID 16
     Btn-23 AT ROW 8.75 COL 26 WIDGET-ID 46
     Btn-9 AT ROW 9.75 COL 3 WIDGET-ID 18
     Btn-24 AT ROW 9.75 COL 26 WIDGET-ID 48
     Btn-10 AT ROW 10.75 COL 3 WIDGET-ID 20
     Btn-25 AT ROW 10.75 COL 26 WIDGET-ID 50
     Btn-11 AT ROW 11.75 COL 3 WIDGET-ID 22
     Btn-26 AT ROW 11.75 COL 26 WIDGET-ID 52
     Btn-12 AT ROW 12.75 COL 3 WIDGET-ID 24
     Btn-27 AT ROW 12.75 COL 26 WIDGET-ID 54
     Btn-13 AT ROW 13.75 COL 3 WIDGET-ID 26
     Btn-28 AT ROW 13.75 COL 26 WIDGET-ID 56
     Btn-14 AT ROW 14.75 COL 3 WIDGET-ID 28
     Btn-29 AT ROW 14.75 COL 26 WIDGET-ID 58
     Btn-15 AT ROW 15.75 COL 3 WIDGET-ID 30
     Btn-30 AT ROW 15.75 COL 26 WIDGET-ID 60
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 80 BY 17
         DEFAULT-BUTTON Btn1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "<insert SmartWindow title>"
         HEIGHT             = 17
         WIDTH              = 80
         MAX-HEIGHT         = 28.79
         MAX-WIDTH          = 146.14
         VIRTUAL-HEIGHT     = 28.79
         VIRTUAL-WIDTH      = 146.14
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* <insert SmartWindow title> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* <insert SmartWindow title> */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-10
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-10 wWin
ON CHOOSE OF Btn-10 IN FRAME fMain /* esdc010 - RG */
DO:
    run esp/esdc010.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-11
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-11 wWin
ON CHOOSE OF Btn-11 IN FRAME fMain /* esdc011 - RG Ordem */
DO:
    run esp/esdc011.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-12
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-12 wWin
ON CHOOSE OF Btn-12 IN FRAME fMain /* esdc012 - Gerar RG */
DO:
    run esp/esdc012.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-13
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-13 wWin
ON CHOOSE OF Btn-13 IN FRAME fMain /* esdc013 - Extrato RG */
DO:
    run esp/esdc013.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-14
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-14 wWin
ON CHOOSE OF Btn-14 IN FRAME fMain /* esdc014 - Etiqueta */
DO:
    run esp/esdc014.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-15
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-15 wWin
ON CHOOSE OF Btn-15 IN FRAME fMain /* esdc015 - Movto RG */
DO:
    run esp/esdc015.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-16
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-16 wWin
ON CHOOSE OF Btn-16 IN FRAME fMain /* esdc016 - Permissao */
DO:
    run esp/esdc016.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-17
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-17 wWin
ON CHOOSE OF Btn-17 IN FRAME fMain /* esdc017 - Perm Pto Ctrl */
DO:
    run esp/esdc017.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-18
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-18 wWin
ON CHOOSE OF Btn-18 IN FRAME fMain /* esdc018 - Etq. RG */
DO:
    run esp/esdc018.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-19
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-19 wWin
ON CHOOSE OF Btn-19 IN FRAME fMain /* esdc019 - Baixa RG */
DO:
    run esp/esdc019.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-2 wWin
ON CHOOSE OF Btn-2 IN FRAME fMain /* esdc002 - Jun��o */
DO:
    run esp/esdc002.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-20
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-20 wWin
ON CHOOSE OF Btn-20 IN FRAME fMain /* esdc020 - Retrabalho */
DO:
    run esp/esdc020.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-21
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-21 wWin
ON CHOOSE OF Btn-21 IN FRAME fMain /* esdc021 - Tempo Estoqu */
DO:
    run esp/esdc021.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-22
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-22 wWin
ON CHOOSE OF Btn-22 IN FRAME fMain /* esdc022 - Rel Rack */
DO:
    run esp/esdc022.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-23
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-23 wWin
ON CHOOSE OF Btn-23 IN FRAME fMain /* esdc023 - Cancel RG */
DO:
    run esp/esdc023.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-24
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-24 wWin
ON CHOOSE OF Btn-24 IN FRAME fMain /* esdc024 - Rel Sit RG */
DO:
    run esp/esdc024.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-25
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-25 wWin
ON CHOOSE OF Btn-25 IN FRAME fMain /* esdc025 - Cor Semi Acab */
DO:
    run esp/esdc025.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-26
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-26 wWin
ON CHOOSE OF Btn-26 IN FRAME fMain /* esdc026 - Elimina Mov */
DO:
    run esp/esdc026.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-27
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-27 wWin
ON CHOOSE OF Btn-27 IN FRAME fMain /* esdc027 - Rel Ordem RG */
DO:
    run esp/esdc027.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-28
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-28 wWin
ON CHOOSE OF Btn-28 IN FRAME fMain /* esdc028 - Ativa RG */
DO:
    run esp/esdc028.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-29
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-29 wWin
ON CHOOSE OF Btn-29 IN FRAME fMain /* esdc029 - Elim Movto */
DO:
    run esp/esdc029.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-3 wWin
ON CHOOSE OF Btn-3 IN FRAME fMain /* esdc003 - Aprova��o */
DO:
    run esp/esdc003.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-30
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-30 wWin
ON CHOOSE OF Btn-30 IN FRAME fMain /* esdc30 - Rel Dupl Estoq */
DO:
    run esp/esdc030.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-31
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-31 wWin
ON CHOOSE OF Btn-31 IN FRAME fMain /* esdc31 - Monit. Transacao */
DO:
    run esp/esdc031.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-32
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-32 wWin
ON CHOOSE OF Btn-32 IN FRAME fMain /* esdc32 - Elim Movto */
DO:
    run esp/esdc032.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-33
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-33 wWin
ON CHOOSE OF Btn-33 IN FRAME fMain /* esdc70 - Criar Rack */
DO:
    run esp/esdc033.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-4 wWin
ON CHOOSE OF Btn-4 IN FRAME fMain /* esdc004 - Cor */
DO:
    run esp/esdc004.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-5 wWin
ON CHOOSE OF Btn-5 IN FRAME fMain /* esdc005 - Pto Contr */
DO:
    run esp/esdc005.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-6
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-6 wWin
ON CHOOSE OF Btn-6 IN FRAME fMain /* esdc006 - Dep */
DO:
    run esp/esdc006.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-7 wWin
ON CHOOSE OF Btn-7 IN FRAME fMain /* esdc007 - Local */
DO:
    run esp/esdc007.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-8
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-8 wWin
ON CHOOSE OF Btn-8 IN FRAME fMain /* esdc008 - Programas */
DO:
    run esp/esdc008.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-9
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-9 wWin
ON CHOOSE OF Btn-9 IN FRAME fMain /* esdc009 - Permiss */
DO:
    run esp/esdc009.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn1 wWin
ON CHOOSE OF Btn1 IN FRAME fMain /* esdc001 - Rack */
DO:
    run esp/esdc001.w.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm2/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE Btn1 Btn-16 Btn-31 Btn-2 Btn-17 Btn-32 Btn-3 Btn-18 Btn-33 Btn-4 
         Btn-19 Btn-5 Btn-20 Btn-6 Btn-21 Btn-7 Btn-22 Btn-8 Btn-23 Btn-9 
         Btn-24 Btn-10 Btn-25 Btn-11 Btn-26 Btn-12 Btn-27 Btn-13 Btn-28 Btn-14 
         Btn-29 Btn-15 Btn-30 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

