/*******************************************************************************
** Programa...: ES0004RP
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..: 
*******************************************************************************/
/* Include de Controle de Vers�o */
{include/i-prgvrs.i ESDC028RP.P 1.00.00.001}
{include/ttdef1.i}
{include/ttdef2.i}
define buffer empresa     for mgcad.empresa.
define buffer localizacao for mgcad.localizacao.
DEFINE BUFFER estabelec              FOR mgadm.estabelec.
DEFINE BUFFER cidade                 FOR mgdis.cidade.   
DEFINE BUFFER pais                   FOR mguni.pais. 
define buffer unid-feder             for mgcad.unid-feder. 

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    FIELD rg-item-ini      AS CHAR
    FIELD rg-item-fim      AS CHAR
    FIELD pto-control      AS CHARACTER.
    
DEFINE TEMP-TABLE tt-raw-digita
    FIELD raw-digita    AS RAW.

DEFINE TEMP-TABLE tt-erro
    FIELD mensagem AS CHAR .

DEF NEW GLOBAL SHARED VAR v_cod_usuar_corren AS CHAR NO-UNDO.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR h-acomp       AS HANDLE NO-UNDO.
DEFINE NEW GLOBAL SHARED VARIABLE c-seg-usuario AS CHARACTER NO-UNDO.
DEFINE VARIABLE h-handle AS HANDLE      NO-UNDO.

/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ESDC028RP"
       c-versao       = "1.00"
       c-revisao      = "1.00.000"
       c-empresa      = empresa.nome
       c-sistema      = "ESP"
       c-titulo-relat = "Ativa��o por Faixa".

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).

FOR EACH dc-rg-item
    WHERE dc-rg-item.rg-item >= tt-param.rg-item-ini
      AND dc-rg-item.rg-item <= tt-param.rg-item-fim.

    RUN pi-acompanhar IN h-acomp (INPUT dc-rg-item.rg-item).
    
    FIND FIRST dc-rack-itens
        WHERE dc-rack-itens.rg-item = dc-rg-item.rg-item
        NO-LOCK NO-ERROR.
    IF AVAIL dc-rack-itens THEN
    DO:
        RUN pi-gerar-erro (INPUT "RG Item " + dc-rg-item.rg-item + " montado no rack " + dc-rack-itens.nr-rack).
        NEXT.
    END.

/*     IF dc-rg-item.situacao <> 4 THEN                                                  */
/*     DO:                                                                               */
/*         RUN pi-gerar-erro (INPUT 'RG Item n�o est� cancelado ' + dc-rg-item.rg-item). */
/*         NEXT.                                                                         */
/*     END.                                                                              */

    FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = tt-param.pto-control
        NO-LOCK NO-ERROR.
    IF NOT AVAILABLE dc-pto-controle THEN DO:
        RUN pi-gerar-erro (INPUT "Ponto de Controle n�o informado").
        NEXT.
    END.

    EMPTY TEMP-TABLE tt-erro-api.
    RUN esapi/esapi005.p PERSISTENT SET h-handle.
    RUN pi-ativa-rg IN h-handle (INPUT dc-pto-controle.cod-pto-controle,
                                 INPUT dc-rg-item.rg-item,
                                 OUTPUT TABLE tt-erro-api).
    DELETE PROCEDURE h-handle.

    IF CAN-FIND(FIRST tt-erro-api) THEN DO:
        FOR EACH tt-erro-api NO-LOCK.
            RUN pi-gerar-erro(INPUT "RG " + dc-rg-item.rg-item + " : " + tt-erro-api.mensagem).
        END.
    END.

END.

RUN pi-imprime.

RUN pi-finalizar IN h-acomp.
RETURN "OK":U.


PROCEDURE pi-gerar-erro.
    DEFINE INPUT PARAM p-mensagem AS CHAR NO-UNDO.

    CREATE tt-erro.
    ASSIGN tt-erro.mensagem = p-mensagem.
END.
       

PROCEDURE pi-imprime.
    IF CAN-FIND (FIRST tt-erro) THEN
        PUT SKIP(2)
            "*** ERROS: " AT 10 SKIP.
    FOR EACH tt-erro NO-LOCK.
        DISP tt-erro.mensagem FORMAT "x(50)" AT 10 SKIP.
    END.


END PROCEDURE.
