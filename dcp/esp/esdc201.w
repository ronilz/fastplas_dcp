&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC201 12.01.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE l-ok       AS LOGICAL   INITIAL TRUE NO-UNDO.
DEFINE VARIABLE c-erro     AS CHAR      NO-UNDO.

DEFINE VARIABLE cPonto     AS CHAR      NO-UNDO.
DEFINE VARIABLE cEntrada   AS CHAR      NO-UNDO.
DEFINE VARIABLE cLocal     AS CHAR      NO-UNDO.
 
DEFINE VARIABLE cComplemento      AS CHARACTER INITIAL '' NO-UNDO.

DEFINE VARIABLE vDeposito         AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE vLocalizacao      AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE vUtilizLocPadItem AS LOGICAL   INITIAL NO NO-UNDO.
DEFINE VARIABLE vAlteraLocaliz    AS LOGICAL   INITIAL NO NO-UNDO.

DEFINE INPUT PARAM cAction AS CHAR NO-UNDO.
DEF VAR c-pto-controle     AS CHAR NO-UNDO.
DEF VAR dep-entrada        AS CHAR NO-UNDO.
DEF VAR loc-entrada        AS CHAR NO-UNDO.
DEF VAR l-erro             AS LOG  NO-UNDO.

{esapi/esapi001.p}

DEF TEMP-TABLE tt-rg NO-UNDO
    FIELD codigo    AS INT 
    FIELD descricao AS CHAR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog
&Scoped-define BROWSE-NAME br-rg

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-rg

/* Definitions for BROWSE br-rg                                         */
&Scoped-define FIELDS-IN-QUERY-br-rg tt-rg.codigo tt-rg.descricao   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-rg   
&Scoped-define SELF-NAME br-rg
&Scoped-define QUERY-STRING-br-rg FOR EACH tt-rg NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-br-rg OPEN QUERY {&SELF-NAME}     FOR EACH tt-rg NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-br-rg tt-rg
&Scoped-define FIRST-TABLE-IN-QUERY-br-rg tt-rg


/* Definitions for FRAME F-Transferencia                                */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Transferencia ~
    ~{&OPEN-QUERY-br-rg}

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancTrans AUTO-GO 
     LABEL "Cancelar" 
     SIZE 20 BY 1.25.

DEFINE BUTTON bt-confTrans 
     LABEL "Confirmar" 
     SIZE 20 BY 1.25.

DEFINE VARIABLE fi-deposito AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep.Entrada" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE fi-descricao AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY .79 NO-UNDO.

DEFINE VARIABLE fi-local AS CHARACTER FORMAT "X(256)":U 
     LABEL "Local" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE fi-localEnt AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loc" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-pto-controle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pto. Controle" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .79 NO-UNDO.

DEFINE VARIABLE fi-rg AS CHARACTER FORMAT "X(256)":U 
     LABEL "RG / Rack" 
     VIEW-AS FILL-IN 
     SIZE 37 BY .79 NO-UNDO.

DEFINE VARIABLE fi-tipo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tipo" 
     VIEW-AS FILL-IN 
     SIZE 23 BY .79 NO-UNDO.

DEFINE VARIABLE fi-total AS CHARACTER FORMAT "X(256)":U 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 23.5.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 1.5.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-rg FOR 
      tt-rg SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-rg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-rg D-Dialog _FREEFORM
  QUERY br-rg NO-LOCK DISPLAY
      tt-rg.codigo    COLUMN-LABEL "C�digo"     WIDTH 10
      tt-rg.descricao COLUMN-LABEL "Descri��o"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 46 BY 15.83
         FONT 1 ROW-HEIGHT-CHARS .6 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     SPACE(50.02) SKIP(25.02)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Tranfer�ncia de Materiais" WIDGET-ID 100.

DEFINE FRAME F-Transferencia
     fi-pto-controle AT ROW 1.5 COL 10 COLON-ALIGNED WIDGET-ID 12
     fi-descricao AT ROW 1.5 COL 17 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     fi-local AT ROW 2.5 COL 10 COLON-ALIGNED WIDGET-ID 16
     fi-tipo AT ROW 2.5 COL 24 COLON-ALIGNED WIDGET-ID 18
     fi-deposito AT ROW 3.5 COL 10 COLON-ALIGNED WIDGET-ID 20
     fi-localEnt AT ROW 3.5 COL 30 COLON-ALIGNED WIDGET-ID 22
     fi-rg AT ROW 4.5 COL 10 COLON-ALIGNED WIDGET-ID 24
     br-rg AT ROW 5.5 COL 3 WIDGET-ID 600
     fi-total AT ROW 21.75 COL 21 COLON-ALIGNED WIDGET-ID 28
     bt-confTrans AT ROW 23.25 COL 5 WIDGET-ID 10
     bt-cancTrans AT ROW 23.25 COL 27 WIDGET-ID 4
     RECT-5 AT ROW 1.25 COL 2 WIDGET-ID 6
     RECT-6 AT ROW 21.5 COL 3 WIDGET-ID 26
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Transfer�ncia Entre Dep�sitos" WIDGET-ID 500.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* REPARENT FRAME */
ASSIGN FRAME F-Transferencia:FRAME = FRAME D-Dialog:HANDLE.

/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME F-Transferencia
                                                                        */
/* BROWSE-TAB br-rg fi-rg F-Transferencia */
/* SETTINGS FOR FILL-IN fi-deposito IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-descricao IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-local IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-localEnt IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-pto-controle IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-tipo IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-total IN FRAME F-Transferencia
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-rg
/* Query rebuild information for BROWSE br-rg
     _START_FREEFORM
OPEN QUERY {&SELF-NAME}
    FOR EACH tt-rg NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE br-rg */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Tranfer�ncia de Materiais */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F-Transferencia
&Scoped-define SELF-NAME bt-cancTrans
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancTrans D-Dialog
ON CHOOSE OF bt-cancTrans IN FRAME F-Transferencia /* Cancelar */
DO:
    APPLY 'CLOSE' TO THIS-PROCEDURE.
    RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME D-Dialog
&Scoped-define BROWSE-NAME br-rg
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

RUN esp/esdc200b.w (INPUT  cAction,
                    OUTPUT c-pto-controle,
                    OUTPUT l-erro).
IF  l-erro THEN DO:
    APPLY 'ENTRY' TO bt-cancTrans IN FRAME F-Transferencia.
    RETURN NO-APPLY.
END.
ELSE DO:
    RUN esp/esdc200c.w (INPUT cAction,
                        INPUT c-pto-controle,
                        OUTPUT dep-entrada,
                        OUTPUT loc-entrada,
                        OUTPUT l-erro).
    IF  l-erro THEN DO:
        APPLY 'ENTRY' TO bt-cancTrans IN FRAME F-Transferencia.
        RETURN NO-APPLY.
    END.
END.

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
  HIDE FRAME F-Transferencia.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
  DISPLAY fi-pto-controle fi-descricao fi-local fi-tipo fi-deposito fi-localEnt 
          fi-rg fi-total 
      WITH FRAME F-Transferencia.
  ENABLE RECT-5 RECT-6 fi-rg br-rg bt-confTrans bt-cancTrans 
      WITH FRAME F-Transferencia.
  {&OPEN-BROWSERS-IN-QUERY-F-Transferencia}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "ESDC201" "12.01.00.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  FIND FIRST dc-pto-controle NO-LOCK
      WHERE dc-pto-controle.cod-pto-controle = c-pto-controle
      NO-ERROR.

  ASSIGN fi-pto-controle = dc-pto-controle.cod-pto-controle
         fi-descricao    = dc-pto-controle.desc-pto-controle
         fi-local        = dc-pto-controle.local-pto
         fi-deposito     = dep-entrada
         fi-localEnt     = loc-entrada
         fi-total        = "0".

  CASE dc-pto-controle.tipo-pto:            
      WHEN 1 THEN ASSIGN fi-tipo = "Normal".
      WHEN 2 THEN ASSIGN fi-tipo = "Juncao".
      WHEN 3 THEN ASSIGN fi-tipo = "Pintura".
      WHEN 4 THEN ASSIGN fi-tipo = "Re-Trabalho".
      WHEN 5 THEN ASSIGN fi-tipo = "Expedicao".
      WHEN 6 THEN ASSIGN fi-tipo = "Sucata".
      WHEN 7 THEN ASSIGN fi-tipo = "Impressao".
  END.

  DISP fi-pto-controle
       fi-descricao 
       fi-local     
       fi-tipo
       fi-deposito  
       fi-localEnt  
       fi-total
      WITH FRAME F-Transferencia.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-rg"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

