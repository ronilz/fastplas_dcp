&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS W-Win 
/*********************************************************************
* Copyright (C) 2000 by Progress Software Corporation. All rights    *
* reserved. Prior versions of this work may contain portions         *
* contributed by participants of Possenet.                           *
*                                                                    *
*********************************************************************/
/*------------------------------------------------------------------------

  File: 

  Description: from cntnrwin.w - ADM SmartWindow Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: 
          
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress UIB.             */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE c-user  AS CHARACTER NO-UNDO.
DEFINE VARIABLE c-pass  AS CHARACTER NO-UNDO.
DEFINE VARIABLE l-ok    AS LOGICAL   INITIAL TRUE NO-UNDO.
DEFINE VARIABLE c-erro  AS CHAR      NO-UNDO.

{include/ttdef1.i}
{include/ttdef2.i}
{esapi/esapi001p.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Login

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-2 fi-identificacao bt-ok 
&Scoped-Define DISPLAYED-OBJECTS fi-identificacao 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR W-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ok AUTO-GO 
     LABEL "Sair" 
     SIZE 10 BY 1.

DEFINE VARIABLE fi-identificacao AS CHARACTER FORMAT "X(18)":U 
     LABEL "Identifica��o do Usu�rio" 
     VIEW-AS FILL-IN 
     SIZE 18 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 39 BY 2.25.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Login
     fi-identificacao AT ROW 6.75 COL 23 COLON-ALIGNED WIDGET-ID 2 AUTO-RETURN  PASSWORD-FIELD 
     bt-ok AT ROW 9.75 COL 20 WIDGET-ID 6
     RECT-2 AT ROW 6 COL 6 WIDGET-ID 4
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1.04
         SIZE 50 BY 25
         FONT 1
         TITLE "Login de Usu�rio" WIDGET-ID 200.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW W-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "Login de Usu�rio"
         HEIGHT             = 25
         WIDTH              = 50
         MAX-HEIGHT         = 50.04
         MAX-WIDTH          = 239.72
         VIRTUAL-HEIGHT     = 50.04
         VIRTUAL-WIDTH      = 239.72
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB W-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW W-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Login
   FRAME-NAME                                                           */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
THEN W-Win:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME W-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON END-ERROR OF W-Win /* Login de Usu�rio */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL W-Win W-Win
ON WINDOW-CLOSE OF W-Win /* Login de Usu�rio */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok W-Win
ON CHOOSE OF bt-ok IN FRAME F-Login /* Sair */
DO:
  apply "close":U to this-procedure.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-identificacao
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-identificacao W-Win
ON ANY-KEY OF fi-identificacao IN FRAME F-Login /* Identifica��o do Usu�rio */
DO:
    /*
    IF  LAST-EVENT:LABEL = "!" THEN DO:
        APPLY LAST-EVENT:LABEL TO SELF.
        RUN pi-leave.
        RETURN NO-APPLY.
    END.
    */
    ASSIGN fi-identificacao.
    IF  LENGTH(fi-identificacao) > 0 THEN
        IF  //SUBSTR(fi-identificacao,LENGTH(fi-identificacao),1) = "!" THEN 
        LAST-EVENT:LABEL = "!" THEN
            DO:
            RUN pi-leave.
        END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-identificacao W-Win
ON LEAVE OF fi-identificacao IN FRAME F-Login /* Identifica��o do Usu�rio */
DO:
    IF fi-identificacao:SCREEN-VALUE IN FRAME F-Login = "" THEN RETURN.
    ASSIGN fi-identificacao.

    EMPTY TEMP-TABLE tt-erro-api.
    ASSIGN l-ok = TRUE.

    IF  NUM-ENTRIES (fi-identificacao, '#') = 2 THEN DO:
        c-user = ENTRY (1, fi-identificacao, '#').
        c-pass =  TRIM ( ENTRY (2, fi-identificacao, '#') , '~!').      
        DO  ON ERROR UNDO, LEAVE:  
            RUN pi-valida-usuario (INPUT c-user, INPUT c-pass, OUTPUT TABLE tt-erro-api).
        END.
    END. 
    ELSE DO:
        IF  fi-identificacao <> ""  AND 
            fi-identificacao <> "!" THEN
            MESSAGE 'Autentica��o Inv�lida' 
                VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
                ASSIGN l-ok = FALSE.
        RETURN.
    END.
                        
    IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo = 1 NO-LOCK) THEN l-ok = FALSE.

    IF  l-ok THEN DO:
        ASSIGN fi-identificacao = "".
        DISP fi-identificacao WITH FRAME F-Login.
        RUN esp/esdc200a.w.
        DISP fi-identificacao WITH FRAME F-Login.
        RETURN NO-APPLY.
    END. 
    ELSE DO:
        ASSIGN c-erro = "".
        FOR EACH tt-erro-api NO-LOCK:
            ASSIGN c-erro = c-erro + string(tt-erro-api.tipo) + " - " + tt-erro-api.mensagem + CHR(10). 
        END.    
        MESSAGE c-erro 
            VIEW-AS ALERT-BOX INFORMATION BUTTONS OK TITLE "ERRO LOGIN".
        RETURN.
    END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK W-Win 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects W-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available W-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI W-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(W-Win)
  THEN DELETE WIDGET W-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI W-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fi-identificacao 
      WITH FRAME F-Login IN WINDOW W-Win.
  ENABLE RECT-2 fi-identificacao bt-ok 
      WITH FRAME F-Login IN WINDOW W-Win.
  {&OPEN-BROWSERS-IN-QUERY-F-Login}
  VIEW W-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit W-Win 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-leave W-Win 
PROCEDURE pi-leave :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
    IF fi-identificacao:SCREEN-VALUE IN FRAME F-Login = "" THEN RETURN.
    ASSIGN fi-identificacao.

    EMPTY TEMP-TABLE tt-erro-api.
    ASSIGN l-ok = TRUE.

    IF  NUM-ENTRIES (fi-identificacao, '#') = 2 THEN DO:
        c-user = ENTRY (1, fi-identificacao, '#').
        c-pass =  TRIM ( ENTRY (2, fi-identificacao, '#') , '~!').      
        DO  ON ERROR UNDO, LEAVE:  
            RUN pi-valida-usuario (INPUT c-user, INPUT c-pass, OUTPUT TABLE tt-erro-api).
        END.
    END. 
    ELSE DO:
        MESSAGE 'Autentica��o Inv�lida' 
            VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
            ASSIGN l-ok = FALSE.
        RETURN.
    END.
                        
    IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo = 1 NO-LOCK) THEN l-ok = FALSE.

    IF  l-ok THEN DO:
        ASSIGN fi-identificacao = "".
        DISP fi-identificacao WITH FRAME F-Login.
        RUN esp/esdc200a.w (INPUT c-user).
        ASSIGN fi-identificacao = "".
        DISP fi-identificacao WITH FRAME F-Login.
        RETURN NO-APPLY.
    END. 
    ELSE DO:
        ASSIGN c-erro = "".
        FOR EACH tt-erro-api NO-LOCK:
            ASSIGN c-erro = c-erro + string(tt-erro-api.tipo) + " - " + tt-erro-api.mensagem + CHR(10). 
        END.    
        MESSAGE c-erro 
            VIEW-AS ALERT-BOX INFORMATION BUTTONS OK TITLE "ERRO LOGIN".
        RETURN.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records W-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartWindow, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed W-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

