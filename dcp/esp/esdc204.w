&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME w-window
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-window 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC200D 12.01.00.003}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> <m�dulo>}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEFINE INPUT PARAMETER cAction AS CHAR NO-UNDO.
DEF NEW GLOBAL SHARED VAR c-usuar-sessao         AS CHAR NO-UNDO.
/* Local Variable Definitions ---                                       */

DEFINE VARIABLE c-user           AS CHARACTER               NO-UNDO.
DEFINE VARIABLE c-pass           AS CHARACTER               NO-UNDO.
DEFINE VARIABLE l-ok             AS LOGICAL   INITIAL TRUE  NO-UNDO.
DEFINE VARIABLE c-erro           AS CHAR                    NO-UNDO.
DEFINE VARIABLE l-erro           AS LOGICAL   INITIAL FALSE NO-UNDO.
DEFINE VARIABLE v-desc-rack      AS CHARACTER               NO-UNDO.
DEFINE VARIABLE c-ponto-controle AS CHARACTER               NO-UNDO.
DEFINE VARIABLE dep-entrada      AS CHARACTER               NO-UNDO.
DEFINE VARIABLE loc-entrada      AS CHARACTER               NO-UNDO.
DEFINE VARIABLE dep-saida        AS CHARACTER               NO-UNDO.
DEFINE VARIABLE loc-saida        AS CHARACTER               NO-UNDO.
DEFINE VARIABLE vCounter         AS INTEGER                 NO-UNDO.


{esapi\esapi004.p}


DEF TEMP-TABLE ttReporte LIKE tt-reporte.
DEF TEMP-TABLE ttReporteItem LIKE tt-reporte-item.

DEF TEMP-TABLE tt-digita LIKE tt-reporte-item
 FIELD r-rowid      AS ROWID
 FIELD descricao    AS CHAR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE JanelaDetalhe
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Reporte01
&Scoped-define BROWSE-NAME br-digita

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-codigo

/* Definitions for BROWSE br-digita                                     */
&Scoped-define FIELDS-IN-QUERY-br-digita tt-codigo.codigo tt-codigo.descricao tt-codigo.localiz   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-digita tt-codigo.localiz   
&Scoped-define ENABLED-TABLES-IN-QUERY-br-digita tt-codigo
&Scoped-define FIRST-ENABLED-TABLE-IN-QUERY-br-digita tt-codigo
&Scoped-define SELF-NAME br-digita
&Scoped-define QUERY-STRING-br-digita FOR EACH tt-codigo
&Scoped-define OPEN-QUERY-br-digita OPEN QUERY {&SELF-NAME} FOR EACH tt-codigo.
&Scoped-define TABLES-IN-QUERY-br-digita tt-codigo
&Scoped-define FIRST-TABLE-IN-QUERY-br-digita tt-codigo


/* Definitions for FRAME F-Reporte01                                    */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Reporte01 ~
    ~{&OPEN-QUERY-br-digita}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 c-rg-rack c-rg-rack-juncao br-digita ~
bt-ok bt-cancReporte 
&Scoped-Define DISPLAYED-OBJECTS c-pto-controle c-descricao-pto c-depos-sai ~
c-localiz-sai c-depos-entr c-localiz-entr c-rg-rack c-rg-rack-juncao 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-window AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancReporte 
     LABEL "Cancelar" 
     SIZE 20 BY 1.25.

DEFINE BUTTON bt-ok 
     LABEL "Ok" 
     SIZE 20 BY 1.25.

DEFINE VARIABLE c-depos-entr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep. Entrada" 
     VIEW-AS FILL-IN 
     SIZE 7.57 BY .79 NO-UNDO.

DEFINE VARIABLE c-depos-sai AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep. Sa�da" 
     VIEW-AS FILL-IN 
     SIZE 7.57 BY .79 NO-UNDO.

DEFINE VARIABLE c-descricao-pto AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS FILL-IN 
     SIZE 26.86 BY .79 NO-UNDO.

DEFINE VARIABLE c-localiz-entr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Localiz. Entrada" 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY .79 NO-UNDO.

DEFINE VARIABLE c-localiz-sai AS CHARACTER FORMAT "X(256)":U 
     LABEL "Localiz. Sa�da" 
     VIEW-AS FILL-IN 
     SIZE 12.14 BY .79 NO-UNDO.

DEFINE VARIABLE c-pto-controle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pto. Controle" 
     VIEW-AS FILL-IN 
     SIZE 7.57 BY .79 NO-UNDO.

DEFINE VARIABLE c-rg-rack AS CHARACTER FORMAT "X(256)":U 
     LABEL "RG / RACK" 
     VIEW-AS FILL-IN 
     SIZE 31.14 BY .79 NO-UNDO.

DEFINE VARIABLE c-rg-rack-juncao AS CHARACTER FORMAT "X(256)":U 
     LABEL "RG / RACK Jun��o" 
     VIEW-AS FILL-IN 
     SIZE 31.14 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 23.5.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-digita FOR 
      tt-codigo SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-digita
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-digita w-window _FREEFORM
  QUERY br-digita DISPLAY
      tt-codigo.codigo        
tt-codigo.descricao
tt-codigo.localiz 
 ENABLE
tt-codigo.localiz
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 44 BY 10.75
         FONT 1 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Reporte01
     c-pto-controle AT ROW 1.71 COL 10.72 COLON-ALIGNED WIDGET-ID 12
     c-descricao-pto AT ROW 1.71 COL 19.14 COLON-ALIGNED WIDGET-ID 14
     c-depos-sai AT ROW 2.58 COL 10.72 COLON-ALIGNED WIDGET-ID 16
     c-localiz-sai AT ROW 2.58 COL 33.86 COLON-ALIGNED WIDGET-ID 18
     c-depos-entr AT ROW 3.46 COL 10.72 COLON-ALIGNED WIDGET-ID 20
     c-localiz-entr AT ROW 3.46 COL 33.86 COLON-ALIGNED WIDGET-ID 22
     c-rg-rack AT ROW 4.54 COL 14.86 COLON-ALIGNED WIDGET-ID 24
     c-rg-rack-juncao AT ROW 5.46 COL 14.86 COLON-ALIGNED WIDGET-ID 26
     br-digita AT ROW 6.71 COL 4 WIDGET-ID 400
     bt-ok AT ROW 23.25 COL 5 WIDGET-ID 8
     bt-cancReporte AT ROW 23.25 COL 27 WIDGET-ID 10
     RECT-3 AT ROW 1.25 COL 2 WIDGET-ID 6
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Reporte de Produ��o" WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: JanelaDetalhe
   Allow: Basic,Browse,DB-Fields,Smart,Window,Query
   Container Links: 
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-window ASSIGN
         HIDDEN             = YES
         TITLE              = "Login de Usu�rio"
         HEIGHT             = 25
         WIDTH              = 50
         MAX-HEIGHT         = 49
         MAX-WIDTH          = 298.43
         VIRTUAL-HEIGHT     = 49
         VIRTUAL-WIDTH      = 298.43
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         FONT               = 1
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-window 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-window.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-window
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Reporte01
   FRAME-NAME                                                           */
/* BROWSE-TAB br-digita c-rg-rack-juncao F-Reporte01 */
/* SETTINGS FOR FILL-IN c-depos-entr IN FRAME F-Reporte01
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-depos-sai IN FRAME F-Reporte01
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-descricao-pto IN FRAME F-Reporte01
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-localiz-entr IN FRAME F-Reporte01
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-localiz-sai IN FRAME F-Reporte01
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-pto-controle IN FRAME F-Reporte01
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-window)
THEN w-window:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-digita
/* Query rebuild information for BROWSE br-digita
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-codigo.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE br-digita */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-window
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-window w-window
ON END-ERROR OF w-window /* Login de Usu�rio */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-window w-window
ON WINDOW-CLOSE OF w-window /* Login de Usu�rio */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancReporte
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancReporte w-window
ON CHOOSE OF bt-cancReporte IN FRAME F-Reporte01 /* Cancelar */
DO:
  APPLY 'close' TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok w-window
ON CHOOSE OF bt-ok IN FRAME F-Reporte01 /* Ok */
DO:
  /* valida��o para quantidade maxima de linhas por reporte */
  FIND FIRST dc-pto-controle WHERE dc-pto-controle.cod-pto-controle = c-pto-controle:SCREEN-VALUE IN FRAME F-Reporte01 NO-LOCK NO-ERROR.
   IF AVAIL dc-pto-controle AND dc-pto-controle.int-1 > 0 THEN
   DO:

   END.
  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-depos-entr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-depos-entr w-window
ON RETURN OF c-depos-entr IN FRAME F-Reporte01 /* Dep. Entrada */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-depos-sai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-depos-sai w-window
ON RETURN OF c-depos-sai IN FRAME F-Reporte01 /* Dep. Sa�da */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-descricao-pto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-descricao-pto w-window
ON RETURN OF c-descricao-pto IN FRAME F-Reporte01
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-localiz-entr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-localiz-entr w-window
ON RETURN OF c-localiz-entr IN FRAME F-Reporte01 /* Localiz. Entrada */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-localiz-sai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-localiz-sai w-window
ON RETURN OF c-localiz-sai IN FRAME F-Reporte01 /* Localiz. Sa�da */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-pto-controle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-pto-controle w-window
ON RETURN OF c-pto-controle IN FRAME F-Reporte01 /* Pto. Controle */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-rg-rack
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-rg-rack w-window
ON LEAVE OF c-rg-rack IN FRAME F-Reporte01 /* RG / RACK */
DO:
 IF TRIM(c-rg-rack:SCREEN-VALUE IN FRAME F-Reporte01) <> '' THEN
   RUN pi-reporte-a.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-rg-rack w-window
ON RETURN OF c-rg-rack IN FRAME F-Reporte01 /* RG / RACK */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-rg-rack-juncao
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-rg-rack-juncao w-window
ON LEAVE OF c-rg-rack-juncao IN FRAME F-Reporte01 /* RG / RACK Jun��o */
DO:
 IF TRIM(c-rg-rack-juncao:SCREEN-VALUE IN FRAME F-Reporte01) <> '' THEN
  RUN pi-reporte-a.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-rg-rack-juncao w-window
ON RETURN OF c-rg-rack-juncao IN FRAME F-Reporte01 /* RG / RACK Jun��o */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-digita
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-window 


/* ***************************  Main Block  *************************** */
 ASSIGN c-ponto-controle = ''.
 
   RUN esp/esdc200b.w (INPUT  cAction,
                       OUTPUT c-ponto-controle,
                       OUTPUT l-erro).
   IF  l-erro THEN DO:
       APPLY 'ENTRY' TO bt-cancReporte IN FRAME F-Reporte01.
       RETURN NO-APPLY.
   END.
   ELSE DO:
       ASSIGN c-pto-controle:SCREEN-VALUE IN FRAME F-Reporte01 = c-ponto-controle.
     
       /* Sele��o de deposito */
       RUN esp/esdc200c.w (INPUT cAction,
                           INPUT c-ponto-controle,
                           OUTPUT dep-entrada,
                           OUTPUT loc-entrada,
                           OUTPUT l-erro).
       IF  l-erro THEN DO:
           APPLY 'ENTRY' TO bt-cancReporte IN FRAME F-Reporte01.
           RETURN NO-APPLY.
       END. 
       ELSE DO:
           ASSIGN c-localiz-entr:SCREEN-VALUE IN FRAME F-Reporte01 = dep-entrada
                  c-depos-entr:SCREEN-VALUE IN FRAME F-Reporte01 = loc-entrada.
          
           RUN esp/esdc200f.w (INPUT cAction,
                               INPUT c-ponto-controle,
                               OUTPUT dep-saida,
                               OUTPUT loc-saida,
                               OUTPUT l-erro).
           IF  l-erro THEN DO:
               APPLY 'ENTRY' TO bt-cancReporte IN FRAME F-Reporte01.
               RETURN NO-APPLY.
           END. 
           ELSE DO:
               ASSIGN c-localiz-sai:SCREEN-VALUE IN FRAME F-Reporte01 = dep-saida
                      c-depos-sai:SCREEN-VALUE IN FRAME F-Reporte01 = loc-saida.
           END.
  
       END.
  
   END.
/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-window  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-window  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-window  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-window)
  THEN DELETE WIDGET w-window.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-window  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY c-pto-controle c-descricao-pto c-depos-sai c-localiz-sai c-depos-entr 
          c-localiz-entr c-rg-rack c-rg-rack-juncao 
      WITH FRAME F-Reporte01 IN WINDOW w-window.
  ENABLE RECT-3 c-rg-rack c-rg-rack-juncao br-digita bt-ok bt-cancReporte 
      WITH FRAME F-Reporte01 IN WINDOW w-window.
  {&OPEN-BROWSERS-IN-QUERY-F-Reporte01}
  VIEW w-window.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy w-window 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-window 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize w-window 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  {include/win-size.i}
  
  {utp/ut9000.i "ESDC204" "12.01.00.003"}

  SESSION:DATA-ENTRY-RETURN = YES.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  IF NOT(l-erro) THEN DO:
     ASSIGN c-pto-controle:SCREEN-VALUE IN FRAME F-Reporte01 = c-ponto-controle
            c-localiz-entr:SCREEN-VALUE IN FRAME F-Reporte01 = ENTRY(1,loc-entrada," -")
            c-depos-entr:SCREEN-VALUE IN FRAME F-Reporte01   = dep-entrada
            c-localiz-sai:SCREEN-VALUE IN FRAME F-Reporte01  = ENTRY(1,loc-saida," -")
            c-depos-sai:SCREEN-VALUE IN FRAME F-Reporte01    = dep-saida.

        FIND FIRST dc-pto-controle WHERE dc-pto-controle.cod-pto-controle = c-ponto-controle NO-LOCK NO-ERROR.
          IF AVAIL (dc-pto-controle) THEN
            ASSIGN c-descricao-pto:SCREEN-VALUE IN FRAME F-Reporte01 = dc-pto-controle.desc-pto-controle.

     END.

  /* Code placed here will execute AFTER standard behavior.    */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-reporte-a w-window 
PROCEDURE pi-reporte-a :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   EMPTY TEMP-TABLE tt-erro-api.
   DO ON ERROR UNDO, LEAVE:
    /* Monta TT Dados da Transa��o */    
    CREATE tt-reporte.
    ASSIGN tt-reporte.cod-pto-controle = c-pto-controle:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte.cod-depos-orig = c-depos-sai:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte.cod-localiz-orig = c-localiz-sai:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte.cod-depos-dest = c-depos-entr:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte.cod-localiz-dest = c-localiz-entr:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte.cod-usuario = c-usuar-sessao.
 
    /* Monta TT Itens */
    CREATE tt-reporte-item.
    ASSIGN tt-reporte-item.rg-item = c-rg-rack:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte-item.rg-item-juncao = c-rg-rack-juncao:SCREEN-VALUE IN FRAME F-Reporte01
           tt-reporte-item.cCodLocaliz = 'NOT_EXCHANGE'.
       
    FOR FIRST dc-work-localiz-temp WHERE dc-work-localiz-temp.id = c-usuar-sessao
          AND dc-work-localiz-temp.rg-item = tt-reporte-item.rg-item EXCLUSIVE-LOCK:
      
          ASSIGN tt-reporte-item.cCodLocaliz = dc-work-localiz-temp.cod-localiz.
          DELETE dc-work-localiz-temp. 
       
    END.         
      
    if tt-reporte-item.rg-item-juncao > '' then do:
    
       if (tt-reporte-item.rg-item + ',' + tt-reporte-item.rg-item-juncao) = /*GET-FIELD('itemAlt') regra para altera��o no browse */ '' then
          ASSIGN tt-reporte-item.cCodLocaliz =  ''. //GET-FIELD('NewLocaliz'). regra para altera��o no browse
          
    end.
    else do:
       if /*GET-FIELD('itemAlt') regra para altera��o no browse  */ '' = tt-reporte-item.rg-item then DO:
          ASSIGN tt-reporte-item.cCodLocaliz = ''.  //GET-FIELD('NewLocaliz'). regra para altera��o no browse
          
       end.
    end.   
 
    /* Valida Item */    
    RUN pi-valida-reporte (INPUT TABLE tt-reporte, 
                           INPUT TABLE tt-reporte-item, 
                           OUTPUT TABLE tt-codigo, 
                           OUTPUT TABLE tt-erro-api). 
                               
    FIND FIRST tt-codigo NO-LOCK NO-ERROR.
    
    if AVAILABLE tt-codigo THEN DO:
    
           for FIRST tt-reporte-item.
    
             if SUBSTRING(tt-reporte-item.rg-item, 1, 2) = 'RC' THEN DO:
          
                ASSIGN tt-codigo.descricao  = ''
                       tt-codigo.localiz    = (IF tt-reporte-item.cCodLocaliz = 'NOT_EXCHANGE' THEN '' 
                                               ELSE tt-reporte-item.cCodLocaliz).
                
                FOR FIRST dc-rack WHERE dc-rack.nr-rack = SUBSTRING(tt-reporte-item.rg-item, 3, 30) NO-LOCK,
                   FIRST dc-rack-itens of dc-rack no-lock,
                   first dc-rg-item where dc-rg-item.rg-item = dc-rack-itens.rg-item no-lock,
                   first item of dc-rg-item no-lock:                                 
                   ASSIGN tt-codigo.descricao  = item.desc-item.
                END.
                                                                                     
             END.
                
          END.
    
    END. 
         
    FOR EACH tt-erro-api NO-LOCK: 
     MESSAGE 'pi-reporte-a' SKIP
             'tt-erro-api' SKIP
             tt-erro-api.tipo SKIP
             tt-erro-api.mensagem
      VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.  
    END.             
   END.

   IF AVAIL tt-codigo THEN DO:   
     CREATE tt-digita.
     BUFFER-COPY tt-codigo TO tt-digita.
     {&OPEN-QUERY-{&BROWSE-NAME}}
   END.
   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-reporte-b w-window 
PROCEDURE pi-reporte-b :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

   EMPTY TEMP-TABLE tt-reporte.

   FOR EACH ttReporte NO-LOCK:
       CREATE tt-reporte.
       BUFFER-COPY ttReporte TO tt-reporte.
       RELEASE tt-reporte.
   END.

   /* Monta TT Itens */
   FOR EACH ttReporteItem NO-LOCK:
           CREATE tt-reporte-item.
       BUFFER-COPY ttReporteItem TO tt-reporte-item.
       RELEASE tt-reporte-item.                     
   END.

   FIND FIRST tt-reporte NO-LOCK NO-ERROR.
   FOR EACH dc-work-localiz-temp
     WHERE dc-work-localiz-temp.id = tt-reporte.cod-usuario:
     DELETE dc-work-localiz-temp.
   END.

   /* Realiza Reporte */        
   RUN pi-reporte (INPUT TABLE tt-reporte, 
                   INPUT TABLE tt-reporte-item, 
                   OUTPUT TABLE tt-erro-api). 

   FOR EACH tt-erro-api NO-LOCK: 
       MESSAGE tt-erro-api.tipo SKIP
               tt-erro-api.mensagem
        VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
   END. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-reporte-c w-window 
PROCEDURE pi-reporte-c :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   
      FOR EACH dc-work-localiz-temp WHERE dc-work-localiz-temp.id = c-usuar-sessao: 
        DELETE dc-work-localiz-temp. 
      END. 

      ASSIGN vCounter = 1.
      FOR EACH ttReporteItem:
          CREATE dc-work-localiz-temp. 
          ASSIGN dc-work-localiz-temp.id = c-usuar-sessao
                 dc-work-localiz-temp.seq = vCounter 
                 dc-work-localiz-temp.rg-item = ttReporteItem.rg-item
                 dc-work-localiz-temp.rg-item-juncao = ttReporteItem.rg-item-juncao
                 dc-work-localiz-temp.rg-item-pai = ttReporteItem.rg-item-pai
                 dc-work-localiz-temp.cod-localiz = ttReporteItem.cCodLocaliz. 
  
          FOR EACH ctx-reporte WHERE ctx-reporte.cod-usuario = c-usuar-sessao 
                                 AND ctx-reporte.rg-item-lido = dc-work-localiz-temp.rg-item: 
            DELETE ctx-reporte. 
          END.
          ASSIGN vCounter = vCounter + 1.
      END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-window  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-codigo"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-window 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

