&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC200C 12.01.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE l-ok       AS LOGICAL   INITIAL TRUE NO-UNDO.
DEFINE VARIABLE c-erro     AS CHAR      NO-UNDO.

DEFINE VARIABLE cPonto     AS CHAR      NO-UNDO.
DEFINE VARIABLE cEntrada   AS CHAR      NO-UNDO.
DEFINE VARIABLE cLocal     AS CHAR      NO-UNDO.
 
DEFINE VARIABLE cComplemento      AS CHARACTER INITIAL '' NO-UNDO.

DEFINE VARIABLE vDeposito         AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE vLocalizacao      AS CHARACTER INITIAL '' NO-UNDO.
DEFINE VARIABLE vUtilizLocPadItem AS LOGICAL   INITIAL NO NO-UNDO.
DEFINE VARIABLE vAlteraLocaliz    AS LOGICAL   INITIAL NO NO-UNDO.

DEFINE INPUT  PARAM cAction        AS CHAR NO-UNDO.
DEFINE INPUT  PARAM c-pto-controle AS CHAR NO-UNDO.
DEFINE OUTPUT PARAM dep-entrada    AS CHAR NO-UNDO.
DEFINE OUTPUT PARAM loc-entrada    AS CHAR NO-UNDO.
DEFINE OUTPUT PARAM l-erro         AS LOG  NO-UNDO.

{esapi/esapi001.p}

DEF TEMP-TABLE tt-rg NO-UNDO
    FIELD codigo    AS INT 
    FIELD descricao AS CHAR.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancEnt AUTO-GO 
     LABEL "Cancelar" 
     SIZE 20 BY 1.25.

DEFINE BUTTON bt-confEnt AUTO-GO 
     LABEL "Confirmar" 
     SIZE 20 BY 1.25.

DEFINE VARIABLE cbEntrada AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep�sito Entrada" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "descricao","codigo"
     DROP-DOWN-LIST
     SIZE 33 BY 1 NO-UNDO.

DEFINE VARIABLE cbLocal AS CHARACTER FORMAT "X(256)":U 
     LABEL "Local Entrada" 
     VIEW-AS COMBO-BOX INNER-LINES 20
     LIST-ITEM-PAIRS "descricao","codigo"
     DROP-DOWN-LIST
     SIZE 33 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 21.5.

DEFINE RECTANGLE RECT-8
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 1.75.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     SPACE(50.02) SKIP(25.02)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Sele��o Deposito/Local" WIDGET-ID 100.

DEFINE FRAME F-DepositoEnt
     cbEntrada AT ROW 4 COL 13 COLON-ALIGNED WIDGET-ID 12
     cbLocal AT ROW 6 COL 13 COLON-ALIGNED WIDGET-ID 14
     bt-confEnt AT ROW 23.25 COL 5 WIDGET-ID 10
     bt-cancEnt AT ROW 23.25 COL 27 WIDGET-ID 4
     RECT-4 AT ROW 1.25 COL 2 WIDGET-ID 6
     RECT-8 AT ROW 23 COL 2 WIDGET-ID 20
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Sele��o de Dep�sito / Local de Entrada" WIDGET-ID 400.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* REPARENT FRAME */
ASSIGN FRAME F-DepositoEnt:FRAME = FRAME D-Dialog:HANDLE.

/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME F-DepositoEnt
                                                                        */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Sele��o Deposito/Local */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  //APPLY "END-ERROR":U TO SELF.
  APPLY 'CLOSE' TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F-DepositoEnt
&Scoped-define SELF-NAME bt-cancEnt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancEnt D-Dialog
ON CHOOSE OF bt-cancEnt IN FRAME F-DepositoEnt /* Cancelar */
DO:
    ASSIGN l-erro = YES.
    APPLY 'CLOSE' TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-confEnt
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-confEnt D-Dialog
ON CHOOSE OF bt-confEnt IN FRAME F-DepositoEnt /* Confirmar */
DO:
    ASSIGN dep-entrada = cbEntrada:SCREEN-VALUE
           loc-entrada = cbLocal:SCREEN-VALUE.
    ASSIGN l-erro = NO.
    APPLY 'CLOSE' TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cbEntrada
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cbEntrada D-Dialog
ON VALUE-CHANGED OF cbEntrada IN FRAME F-DepositoEnt /* Dep�sito Entrada */
DO:
    
    ASSIGN vDeposito = cbEntrada:SCREEN-VALUE .
    EMPTY TEMP-TABLE tt-localizacao.
    ASSIGN cLocal   = "".

    RUN pi-localizacao (INPUT vDeposito, 
                        OUTPUT TABLE tt-localizacao).   
    
    FOR EACH tt-localizacao:
        IF  cLocal = "" THEN
            ASSIGN cLocal = tt-localizacao.cod-localiz + "," + tt-localizacao.descricao.
        ELSE
            ASSIGN cLocal = cLocal + "," + tt-localizacao.cod-localiz + "," + tt-localizacao.descricao.
    END.

    IF cLocal <> "" THEN
       ASSIGN cbLocal:LIST-ITEM-PAIRS IN FRAME F-DepositoEnt = cLocal.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME D-Dialog
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
  HIDE FRAME F-DepositoEnt.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
  DISPLAY cbEntrada cbLocal 
      WITH FRAME F-DepositoEnt.
  ENABLE RECT-4 RECT-8 cbEntrada cbLocal bt-confEnt bt-cancEnt 
      WITH FRAME F-DepositoEnt.
  {&OPEN-BROWSERS-IN-QUERY-F-DepositoEnt}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "ESDC200C" "12.01.00.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  FIND FIRST dc-pto-controle NO-LOCK
      WHERE dc-pto-controle.cod-pto-controle = c-pto-controle
      NO-ERROR.

  ASSIGN cComplemento = 'entrada'.

  IF cComplemento = 'saida' THEN 
      ASSIGN vDeposito    = dc-pto-controle.cod-depos-orig
             vLocalizacao = dc-pto-controle.cod-localiz-orig.
  ELSE
      ASSIGN vDeposito    = dc-pto-controle.cod-depos-dest
             vLocalizacao = dc-pto-controle.cod-localiz-dest.

  //    if GET-VALUE("DepPad") <> '' then 
  //       assign vDeposito = GET-VALUE("DepPad").

  ASSIGN cEntrada = ""
         cLocal   = "".

  EMPTY TEMP-TABLE tt-deposito.
  EMPTY TEMP-TABLE tt-localizacao.

  ASSIGN vUtilizLocPadItem = dc-pto-controle.utiliz-loc-pad-item
         vAlteraLocaliz    = dc-pto-controle.altera-localiz.

  /* Encontra Lista de Dep�sitos */
  RUN pi-deposito (OUTPUT TABLE tt-deposito).

  /* Encontra Localiza��es para o Dep�sito Padr�o */
  RUN pi-localizacao (INPUT vDeposito, 
                      OUTPUT TABLE tt-localizacao).       

  FOR EACH tt-deposito: 
      IF  cEntrada = "" THEN
          ASSIGN cEntrada = tt-deposito.nome + "," + tt-deposito.cod-depos.
      ELSE
          ASSIGN cEntrada = cEntrada + "," + tt-deposito.nome + "," + tt-deposito.cod-depos. 
  END.

  FOR EACH tt-localizacao:
      IF  cLocal = "" THEN
          ASSIGN cLocal = tt-localizacao.descricao + "," + tt-localizacao.cod-localiz.
      ELSE
          ASSIGN cLocal = cLocal + "," + tt-localizacao.descricao + "," + tt-localizacao.cod-localiz.
  END.

  IF cEntrada <> "" THEN
     ASSIGN cbEntrada:LIST-ITEM-PAIRS IN FRAME F-DepositoEnt = cEntrada.

  IF cLocal <> "" THEN
     ASSIGN cbLocal:LIST-ITEM-PAIRS IN FRAME F-DepositoEnt = cLocal.

  DISPLAY cbEntrada cbLocal 
      WITH FRAME F-DepositoEnt.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartDialog, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

