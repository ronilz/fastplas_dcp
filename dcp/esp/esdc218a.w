&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC218 12.01.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
{include/i-license-manager.i ESDC218 MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
{esapi/esapi001.p}
{cdp/cd0666.i}

DEFINE TEMP-TABLE tt-rg NO-UNDO
    FIELD codigo    AS CHARACTER FORMAT "x(13)" 
    FIELD cor       AS CHARACTER FORMAT "x(6)"
    FIELD descricao AS CHARACTER FORMAT "x(30)"
    FIELD l-imp     AS LOGICAL INITIAL FALSE
    FIELD seq       AS INTEGER.

DEFINE TEMP-TABLE tt-itens NO-UNDO
    FIELD it-codigo AS CHARACTER FORMAT "x(13)" 
    FIELD descricao AS CHARACTER FORMAT "x(30)"
    FIELD qtd       AS DECIMAL   FORMAT ">>>9"
    FIELD rg-item   LIKE dc-rg-item.rg-item.

DEFINE TEMP-TABLE tt-etiq
    FIELD c-valor AS CHARACTER.


/* Parameters Definitions ---                                       */
DEFINE INPUT PARAMETER pto-controle   AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER dep-entrada    AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER loc-entrada    AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER dep-saida      AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER loc-saida      AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER tp-cor         AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER tp-rack        AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER rack           AS CHARACTER NO-UNDO.
DEFINE INPUT PARAMETER c-impressora   AS CHARACTER NO-UNDO.

DEFINE INPUT PARAMETER TABLE FOR tt-rg.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog
&Scoped-define BROWSE-NAME br-rg

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-itens

/* Definitions for BROWSE br-rg                                         */
&Scoped-define FIELDS-IN-QUERY-br-rg tt-itens.it-codigo tt-itens.descricao tt-itens.qtd   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-rg   
&Scoped-define SELF-NAME br-rg
&Scoped-define QUERY-STRING-br-rg FOR EACH tt-itens NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-br-rg OPEN QUERY {&SELF-NAME}     FOR EACH tt-itens NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-br-rg tt-itens
&Scoped-define FIRST-TABLE-IN-QUERY-br-rg tt-itens


/* Definitions for FRAME F-amrp                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-amrp ~
    ~{&OPEN-QUERY-br-rg}

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancTrans AUTO-GO 
     LABEL "Sair" 
     SIZE 14 BY 1.25.

DEFINE BUTTON bt-etiqueta 
     LABEL "Etq Externa" 
     SIZE 14 BY 1.25.

DEFINE BUTTON bt-etiqueta-2 
     LABEL "Etq Todos" 
     SIZE 14 BY 1.25.

DEFINE VARIABLE fi-depEntrada AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep.Entrada" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE fi-depSaida AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep.Saida" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE fi-descricao AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY .79 NO-UNDO.

DEFINE VARIABLE fi-local AS CHARACTER FORMAT "X(256)":U 
     LABEL "Local" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE fi-localEnt AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loc" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-localSai AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loc" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-pto-controle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pto. Controle" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .79 NO-UNDO.

DEFINE VARIABLE fi-rack AS CHARACTER FORMAT "X(16)":U 
     LABEL "Rack" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-tipo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tipo" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-total AS CHARACTER FORMAT "X(256)":U 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE fi-tp-rc AS CHARACTER FORMAT "X(13)":U 
     LABEL "Tipo RC" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 23.5.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 1.5.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-rg FOR 
      tt-itens SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-rg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-rg D-Dialog _FREEFORM
  QUERY br-rg NO-LOCK DISPLAY
      tt-itens.it-codigo    COLUMN-LABEL "Item" FORMAT "x(13)"    WIDTH 13
    tt-itens.descricao       COLUMN-LABEL "Descri��o"     FORMAT "x(23)" WIDTH 23
    tt-itens.qtd COLUMN-LABEL "Qtd" FORMAT ">>>9" WIDTH 7
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS MULTIPLE SIZE 46 BY 14.83
         FONT 1 ROW-HEIGHT-CHARS 1.6 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     SPACE(50.26) SKIP(25.26)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Tranfer�ncia de Materiais" WIDGET-ID 100.

DEFINE FRAME F-amrp
     fi-pto-controle AT ROW 1.5 COL 10 COLON-ALIGNED WIDGET-ID 12
     fi-descricao AT ROW 1.5 COL 17 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     fi-local AT ROW 2.5 COL 10 COLON-ALIGNED WIDGET-ID 16
     fi-tipo AT ROW 2.5 COL 30 COLON-ALIGNED WIDGET-ID 18
     fi-depEntrada AT ROW 3.5 COL 10 COLON-ALIGNED WIDGET-ID 20
     fi-localEnt AT ROW 3.5 COL 30 COLON-ALIGNED WIDGET-ID 22
     fi-depSaida AT ROW 4.5 COL 10 COLON-ALIGNED WIDGET-ID 30
     fi-localSai AT ROW 4.5 COL 30 COLON-ALIGNED WIDGET-ID 32
     fi-tp-rc AT ROW 5.5 COL 10 COLON-ALIGNED WIDGET-ID 24 AUTO-RETURN 
     fi-rack AT ROW 5.5 COL 30 COLON-ALIGNED WIDGET-ID 34 AUTO-RETURN 
     br-rg AT ROW 6.75 COL 3 WIDGET-ID 600
     fi-total AT ROW 21.75 COL 21 COLON-ALIGNED WIDGET-ID 28
     bt-etiqueta-2 AT ROW 23.25 COL 4 WIDGET-ID 36
     bt-etiqueta AT ROW 23.25 COL 18.86 WIDGET-ID 10
     bt-cancTrans AT ROW 23.25 COL 34.86 WIDGET-ID 4
     RECT-5 AT ROW 1.25 COL 2 WIDGET-ID 6
     RECT-6 AT ROW 21.5 COL 3 WIDGET-ID 26
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Resumo e Impress�o" WIDGET-ID 500.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* REPARENT FRAME */
ASSIGN FRAME F-amrp:FRAME = FRAME D-Dialog:HANDLE.

/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME F-amrp
                                                                        */
/* BROWSE-TAB br-rg fi-rack F-amrp */
/* SETTINGS FOR FILL-IN fi-depEntrada IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-depSaida IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-descricao IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-local IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-localEnt IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-localSai IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-pto-controle IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-rack IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-tipo IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-total IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-tp-rc IN FRAME F-amrp
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-rg
/* Query rebuild information for BROWSE br-rg
     _START_FREEFORM
OPEN QUERY {&SELF-NAME}
    FOR EACH tt-itens NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE br-rg */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Tranfer�ncia de Materiais */
DO:  
        /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
        APPLY "END-ERROR":U TO SELF.
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F-amrp
&Scoped-define SELF-NAME bt-cancTrans
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancTrans D-Dialog
ON CHOOSE OF bt-cancTrans IN FRAME F-amrp /* Sair */
DO:
        APPLY 'CLOSE' TO THIS-PROCEDURE.
        RETURN.
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-etiqueta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-etiqueta D-Dialog
ON CHOOSE OF bt-etiqueta IN FRAME F-amrp /* Etq Externa */
DO:

        RUN pi-etiqueta.
        RETURN.
    
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-etiqueta-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-etiqueta-2 D-Dialog
ON CHOOSE OF bt-etiqueta-2 IN FRAME F-amrp /* Etq Todos */
DO:
        
        br-rg:SELECT-ALL ().
        RUN pi-etiqueta.
        RETURN.
    
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME D-Dialog
&Scoped-define BROWSE-NAME br-rg
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
  HIDE FRAME F-amrp.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
  DISPLAY fi-pto-controle fi-descricao fi-local fi-tipo fi-depEntrada 
          fi-localEnt fi-depSaida fi-localSai fi-tp-rc fi-rack fi-total 
      WITH FRAME F-amrp.
  ENABLE RECT-5 RECT-6 br-rg bt-etiqueta-2 bt-etiqueta bt-cancTrans 
      WITH FRAME F-amrp.
  {&OPEN-BROWSERS-IN-QUERY-F-amrp}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
          Purpose:     Override standard ADM method
          Notes:       
        ------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
    {include/i-logfin.i}

/* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
          Purpose:     Override standard ADM method
          Notes:       
        ------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */

    {utp/ut9000.i "ESDC218" "12.01.00.001"}

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

    /* Code placed here will execute AFTER standard behavior.    */
    SESSION:DATA-ENTRY-RETURN = TRUE.
  
    FIND FIRST dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = pto-controle
        NO-ERROR.

    ASSIGN 
        fi-pto-controle = dc-pto-controle.cod-pto-controle
        fi-descricao    = dc-pto-controle.desc-pto-controle
        fi-local        = dc-pto-controle.local-pto
        fi-tipo         = IF tp-cor = "1" THEN "Cor Unica" ELSE "Colorido"
        fi-depSaida     = dep-saida
        fi-localSai     = loc-saida
        fi-depEntrada   = dep-entrada
        fi-localEnt     = loc-entrada
        fi-rack         = rack
        fi-tp-rc        = IF tp-rack = "1" THEN "RACK" ELSE "AVULSO" 
        fi-total        = "0".

    DISPLAY 
        fi-pto-controle
        fi-descricao 
        fi-local
        fi-tipo
        fi-depSaida  
        fi-localSai  
        fi-depEntrada  
        fi-localEnt
        fi-rack
        fi-tp-rc 
        fi-total
        WITH FRAME f-amrp.
        
    RUN pi-calc-itens.
    RUN pi-total.        
    
    {&OPEN-BROWSERS-IN-QUERY-F-amrp}
                    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-calc-itens D-Dialog 
PROCEDURE pi-calc-itens :
/*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE c-nr-rack AS CHARACTER NO-UNDO.
    DEFINE VARIABLE de-qtd    AS DECIMAL   NO-UNDO.
    
    // main
    c-nr-rack = "".
    FOR EACH tt-rg,
        FIRST dc-rg-item FIELDS (rg-item it-codigo nr-rack) NO-LOCK
        WHERE dc-rg-item.rg-item = tt-rg.codigo
        BREAK BY dc-rg-item.it-codigo.
            
        IF FIRST-OF (dc-rg-item.it-codigo) THEN
        DO: 
            de-qtd = 0.
            IF c-nr-rack = "" THEN 
                c-nr-rack = dc-rg-item.nr-rack.
        END.
                
        de-qtd = de-qtd + 1.
            
        IF LAST-OF (dc-rg-item.it-codigo) THEN 
        DO:
                
            FOR FIRST ITEM FIELDS (it-codigo desc-item) NO-LOCK
                WHERE ITEM.it-codigo = dc-rg-item.it-codigo.
            END.
                
            CREATE tt-itens.
            ASSIGN 
                tt-itens.it-codigo = dc-rg-item.it-codigo
                tt-itens.descricao = ITEM.desc-item
                tt-itens.qtd       = de-qtd
                tt-itens.rg-item   = dc-rg-item.rg-item.
                       
        END.
                                                    
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-etiqueta D-Dialog 
PROCEDURE pi-etiqueta :
/*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    
    // vars
    DEFINE VARIABLE i-cont AS INTEGER NO-UNDO.
    DEFINE VARIABLE h-acomp AS HANDLE NO-UNDO.

    // main
    RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
    RUN pi-inicializar IN h-acomp ("Gerando impress�o...").
    
    FOR FIRST es-etq-impressora NO-LOCK
        WHERE es-etq-impressora.cod-impressora = c-impressora.
    END.
    IF AVAILABLE es-etq-impressora THEN 
    DO:
        
        DO i-cont = 1 TO br-rg:NUM-SELECTED-ROWS IN FRAME f-amrp.
        
            br-rg:FETCH-SELECTED-ROW (i-cont) IN FRAME f-amrp.
        
            RUN pi-acompanhar IN h-acomp ("Gerando RG: " + tt-itens.rg-item).
                    
            EMPTY TEMP-TABLE tt-etiq.
            CREATE tt-etiq.
            tt-etiq.c-valor = STRING(INT(tt-itens.rg-item),"9999999999999") + ";;" + tt-itens.it-codigo + ";". 
        
            RUN esp/esapi016.p (
                INPUT es-etq-impressora.end-impressora,
                INPUT 0,
                INPUT tt-itens.qtd,
                INPUT TABLE tt-etiq).
                    
        END.
            
    END.    
    
    RUN pi-finalizar IN h-acomp.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-total D-Dialog 
PROCEDURE pi-total :
/*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE i-cont AS INTEGER NO-UNDO.
    
    // main
    i-cont = 0.
    FOR EACH tt-itens.
        i-cont = i-cont + 1.
    END.
    fi-total:SCREEN-VALUE IN FRAME f-amrp = STRING (i-cont) .
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-itens"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
          Purpose:     
          Parameters:  <none>
          Notes:       
        -------------------------------------------------------------*/
    DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
    DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
    RUN pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

