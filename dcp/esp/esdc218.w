&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC218 12.01.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
{include/i-license-manager.i ESDC218 MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE c-pto-controle AS CHARACTER NO-UNDO.
DEFINE VARIABLE dep-entrada    AS CHARACTER NO-UNDO.
DEFINE VARIABLE loc-entrada    AS CHARACTER NO-UNDO.
DEFINE VARIABLE dep-saida      AS CHARACTER NO-UNDO.
DEFINE VARIABLE loc-saida      AS CHARACTER NO-UNDO.
DEFINE VARIABLE tp-cor         AS CHARACTER NO-UNDO.
DEFINE VARIABLE tp-rack        AS CHARACTER NO-UNDO.
DEFINE VARIABLE c-cor          AS CHARACTER NO-UNDO.
DEFINE VARIABLE i-rack         AS DECIMAL   NO-UNDO.
DEFINE VARIABLE c-impressora   AS CHARACTER NO-UNDO.
DEFINE VARIABLE c-auth         AS CHARACTER NO-UNDO.
DEFINE VARIABLE c-senha        AS CHARACTER NO-UNDO.

DEFINE VARIABLE l-erro         AS LOGICAL   NO-UNDO.

{esapi/esapi001.p}
{cdp/cd0666.i}

DEFINE TEMP-TABLE tt-rg NO-UNDO
    FIELD codigo    AS CHARACTER FORMAT "x(13)" 
    FIELD cor       AS CHARACTER FORMAT "x(6)"
    FIELD descricao AS CHARACTER FORMAT "x(30)"
    FIELD l-imp     AS LOGICAL INITIAL FALSE
    FIELD seq       AS INTEGER.

DEFINE TEMP-TABLE tt-etiq
    FIELD c-valor AS CHARACTER.
    
DEFINE TEMP-TABLE tt-erro-api-aux LIKE tt-erro-api.


/* Parameters Definitions ---                                       */
DEFINE INPUT PARAMETER cAction AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog
&Scoped-define BROWSE-NAME br-rg

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-rg

/* Definitions for BROWSE br-rg                                         */
&Scoped-define FIELDS-IN-QUERY-br-rg tt-rg.codigo tt-rg.cor tt-rg.descricao   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-rg   
&Scoped-define SELF-NAME br-rg
&Scoped-define QUERY-STRING-br-rg FOR EACH tt-rg  NO-LOCK BY tt-rg.seq DESCENDING
&Scoped-define OPEN-QUERY-br-rg OPEN QUERY {&SELF-NAME}     FOR EACH tt-rg  NO-LOCK BY tt-rg.seq DESCENDING.
&Scoped-define TABLES-IN-QUERY-br-rg tt-rg
&Scoped-define FIRST-TABLE-IN-QUERY-br-rg tt-rg


/* Definitions for FRAME F-amrp                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-amrp ~
    ~{&OPEN-QUERY-br-rg}

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD getSeq D-Dialog 
FUNCTION getSeq RETURNS INTEGER
  (  ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancTrans AUTO-GO 
     LABEL "Cancelar" 
     SIZE 14 BY 1.25.

DEFINE BUTTON bt-confTrans AUTO-GO 
     LABEL "Confirmar" 
     SIZE 14 BY 1.25.

DEFINE BUTTON bt-etiqueta 
     LABEL "Etiqueta RG" 
     SIZE 14 BY 1.25.

DEFINE VARIABLE fi-cor AS CHARACTER FORMAT "X(6)":U 
     LABEL "Cor" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .79 NO-UNDO.

DEFINE VARIABLE fi-depEntrada AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep.Entrada" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE fi-depSaida AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep.Saida" 
     VIEW-AS FILL-IN 
     SIZE 15 BY .79 NO-UNDO.

DEFINE VARIABLE fi-descricao AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY .79 NO-UNDO.

DEFINE VARIABLE fi-local AS CHARACTER FORMAT "X(256)":U 
     LABEL "Local" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE VARIABLE fi-localEnt AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loc" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-localSai AS CHARACTER FORMAT "X(256)":U 
     LABEL "Loc" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-pto-controle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pto. Controle" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .79 NO-UNDO.

DEFINE VARIABLE fi-rg AS CHARACTER FORMAT "X(13)":U 
     LABEL "RG" 
     VIEW-AS FILL-IN 
     SIZE 23 BY .79 NO-UNDO.

DEFINE VARIABLE fi-tipo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Tipo" 
     VIEW-AS FILL-IN 
     SIZE 17 BY .79 NO-UNDO.

DEFINE VARIABLE fi-total AS CHARACTER FORMAT "X(256)":U 
     LABEL "Total" 
     VIEW-AS FILL-IN 
     SIZE 8.86 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-5
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 23.5.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 46 BY 1.5.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-rg FOR 
      tt-rg SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-rg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-rg D-Dialog _FREEFORM
  QUERY br-rg NO-LOCK DISPLAY
      tt-rg.codigo    COLUMN-LABEL "RG Item" FORMAT "x(13)"    WIDTH 13
    tt-rg.cor       COLUMN-LABEL "Cor"     FORMAT "x(6)" WIDTH 7
    tt-rg.descricao COLUMN-LABEL "Descri��o" FORMAT "x(30)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 46 BY 14.83
         FONT 1 ROW-HEIGHT-CHARS 1.6 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     SPACE(50.00) SKIP(25.00)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Tranfer�ncia de Materiais" WIDGET-ID 100.

DEFINE FRAME F-amrp
     fi-pto-controle AT ROW 1.5 COL 10 COLON-ALIGNED WIDGET-ID 12
     fi-descricao AT ROW 1.5 COL 17 COLON-ALIGNED NO-LABEL WIDGET-ID 14
     fi-local AT ROW 2.5 COL 10 COLON-ALIGNED WIDGET-ID 16
     fi-tipo AT ROW 2.5 COL 30 COLON-ALIGNED WIDGET-ID 18
     fi-depEntrada AT ROW 3.5 COL 10 COLON-ALIGNED WIDGET-ID 20
     fi-localEnt AT ROW 3.5 COL 30 COLON-ALIGNED WIDGET-ID 22
     fi-depSaida AT ROW 4.5 COL 10 COLON-ALIGNED WIDGET-ID 30
     fi-localSai AT ROW 4.5 COL 30 COLON-ALIGNED WIDGET-ID 32
     fi-rg AT ROW 5.5 COL 10 COLON-ALIGNED WIDGET-ID 24 AUTO-RETURN 
     fi-cor AT ROW 5.5 COL 37.86 COLON-ALIGNED WIDGET-ID 34 AUTO-RETURN 
     br-rg AT ROW 6.5 COL 3 WIDGET-ID 600
     fi-total AT ROW 21.75 COL 21 COLON-ALIGNED WIDGET-ID 28
     bt-confTrans AT ROW 23.25 COL 3 WIDGET-ID 10
     bt-etiqueta AT ROW 23.25 COL 18.86 WIDGET-ID 36
     bt-cancTrans AT ROW 23.25 COL 34.86 WIDGET-ID 4
     RECT-5 AT ROW 1.25 COL 2 WIDGET-ID 6
     RECT-6 AT ROW 21.5 COL 3 WIDGET-ID 26
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Apontamento Montagem Reporte Pintura" WIDGET-ID 500.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* REPARENT FRAME */
ASSIGN FRAME F-amrp:FRAME = FRAME D-Dialog:HANDLE.

/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME F-amrp
                                                                        */
/* BROWSE-TAB br-rg fi-cor F-amrp */
/* SETTINGS FOR FILL-IN fi-cor IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-depEntrada IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-depSaida IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-descricao IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-local IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-localEnt IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-localSai IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-pto-controle IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-tipo IN FRAME F-amrp
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-total IN FRAME F-amrp
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-rg
/* Query rebuild information for BROWSE br-rg
     _START_FREEFORM
OPEN QUERY {&SELF-NAME}
    FOR EACH tt-rg  NO-LOCK BY tt-rg.seq DESCENDING.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE br-rg */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Tranfer�ncia de Materiais */
DO:  
        /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
        APPLY "END-ERROR":U TO SELF.
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-rg
&Scoped-define FRAME-NAME F-amrp
&Scoped-define SELF-NAME br-rg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rg D-Dialog
ON ENTRY OF br-rg IN FRAME F-amrp
DO:
        //APPLY "entry":U TO fi-rg.  
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rg D-Dialog
ON MOUSE-SELECT-DBLCLICK OF br-rg IN FRAME F-amrp
DO:
        RUN pi-delete.  
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-rg D-Dialog
ON ROW-DISPLAY OF br-rg IN FRAME F-amrp
DO:
    IF AVAILABLE tt-rg AND tt-rg.l-imp THEN 
        ASSIGN 
            tt-rg.codigo:BGCOLOR IN BROWSE br-rg = 14
            tt-rg.descricao:BGCOLOR IN BROWSE br-rg = 14
            tt-rg.cor:BGCOLOR IN BROWSE br-rg = 14. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-cancTrans
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancTrans D-Dialog
ON CHOOSE OF bt-cancTrans IN FRAME F-amrp /* Cancelar */
DO:
        APPLY 'CLOSE' TO THIS-PROCEDURE.
        RETURN.
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-confTrans
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-confTrans D-Dialog
ON CHOOSE OF bt-confTrans IN FRAME F-amrp /* Confirmar */
DO:
    
        RUN pi-confirma.                
        IF RETURN-VALUE = "NOK":U THEN
            RETURN NO-APPLY.
        ELSE
        DO:
            RUN esp/esdc218a.w (
                INPUT c-pto-controle,
                INPUT dep-entrada,
                INPUT loc-entrada,
                INPUT dep-saida,
                INPUT loc-saida,
                INPUT tp-cor,
                INPUT tp-rack,
                INPUT ("RC" + STRING (i-rack)),
                INPUT c-impressora,
                INPUT TABLE tt-rg).
        END.

        APPLY 'CLOSE' TO THIS-PROCEDURE.
        
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-etiqueta
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-etiqueta D-Dialog
ON CHOOSE OF bt-etiqueta IN FRAME F-amrp /* Etiqueta RG */
DO:

        RUN pi-etiqueta.
        RETURN.
    
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-cor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-cor D-Dialog
ON LEAVE OF fi-cor IN FRAME F-amrp /* Cor */
DO:

        IF fi-rg:SCREEN-VALUE <> "" THEN
        DO:
            RUN pi-add.
            
            fi-rg:SCREEN-VALUE IN FRAME f-amrp = "".
            SELF:SCREEN-VALUE IN FRAME f-amrp = "".
    
            APPLY "ENTRY":U TO fi-rg IN FRAME f-amrp.
            RETURN NO-APPLY.
            
        END.            
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-rg
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-rg D-Dialog
ON LEAVE OF fi-rg IN FRAME F-amrp /* RG */
DO:

        IF SELF:SCREEN-VALUE = "" THEN 
            NEXT.
             
        IF tp-cor = "1" THEN
        DO:
            RUN pi-add.
            
            fi-rg:SCREEN-VALUE IN FRAME f-amrp = "".
    
            IF tp-cor = "2" THEN 
                fi-cor:SCREEN-VALUE IN FRAME f-amrp = "".
            
            APPLY "ENTRY":U TO SELF IN FRAME f-amrp.
            RETURN NO-APPLY.
        END.
        
  
    END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME D-Dialog
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

// 200b - ponto de controle
// 200c - deposito de entrada
// 200f - deposito de saida
// 200g - tipo reporte
// 200h - cor
// 200p - impressora

RUN esp/esdc200b.w (
    INPUT  cAction,
    OUTPUT c-pto-controle,
    OUTPUT l-erro).
IF  l-erro THEN 
DO:
    APPLY 'ENTRY' TO bt-cancTrans IN FRAME f-amrp.
    RETURN NO-APPLY.
END.
ELSE 
DO:
    FOR FIRST dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = c-pto-controle.
    END.
    
    ASSIGN 
        dep-entrada = dc-pto-controle.cod-depos-dest
        loc-entrada = dc-pto-controle.cod-localiz-dest
        dep-saida   = dc-pto-controle.cod-depos-orig
        loc-saida   = dc-pto-controle.cod-localiz-orig.
        
    /*    RUN esp/esdc200c.w (                                  */
    /*        INPUT cAction,                                    */
    /*        INPUT c-pto-controle,                             */
    /*        OUTPUT dep-entrada,                               */
    /*        OUTPUT loc-entrada,                               */
    /*        OUTPUT l-erro).                                   */
    /*    IF  l-erro THEN                                       */
    /*    DO:                                                   */
    /*        APPLY 'ENTRY' TO bt-cancTrans IN FRAME f-amrp.    */
    /*        RETURN NO-APPLY.                                  */
    /*    END.                                                  */
    /*    ELSE                                                  */
    /*    DO:                                                   */
    /*        RUN esp/esdc200f.w (                              */
    /*            INPUT cAction,                                */
    /*            INPUT c-pto-controle,                         */
    /*            OUTPUT dep-saida,                             */
    /*            OUTPUT loc-saida,                             */
    /*            OUTPUT l-erro).                               */
    /*        IF  l-erro THEN                                   */
    /*        DO:                                               */
    /*            APPLY 'ENTRY' TO bt-cancTrans IN FRAME f-amrp.*/
    /*            RETURN NO-APPLY.                              */
    /*        END.                                              */
    /*        ELSE                                              */
    /*        DO:                                               */

    RUN esp/esdc200p.w (
        OUTPUT c-impressora,
        OUTPUT l-erro).
    IF  l-erro THEN 
    DO:
        APPLY 'ENTRY' TO bt-cancTrans IN FRAME f-amrp.
        RETURN NO-APPLY.
    END.
    
    RUN esp/esdc200g.w (
        INPUT cAction,
        INPUT c-pto-controle,
        OUTPUT tp-cor,
        OUTPUT tp-rack,
        OUTPUT l-erro).
    IF  l-erro THEN 
    DO:
        APPLY 'ENTRY' TO bt-cancTrans IN FRAME f-amrp.
        RETURN NO-APPLY.
    END.
            
    IF tp-cor = "1" THEN 
    DO:
        RUN esp/esdc200h.w (
            INPUT cAction,
            INPUT c-pto-controle,
            OUTPUT c-cor,
            OUTPUT l-erro).
        IF  l-erro THEN 
        DO:
            APPLY 'ENTRY' TO bt-cancTrans IN FRAME f-amrp.
            RETURN NO-APPLY.
        END.
                
    END.
        
END.

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
  HIDE FRAME F-amrp.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
  DISPLAY fi-pto-controle fi-descricao fi-local fi-tipo fi-depEntrada 
          fi-localEnt fi-depSaida fi-localSai fi-rg fi-cor fi-total 
      WITH FRAME F-amrp.
  ENABLE RECT-5 RECT-6 fi-rg br-rg bt-confTrans bt-etiqueta bt-cancTrans 
      WITH FRAME F-amrp.
  {&OPEN-BROWSERS-IN-QUERY-F-amrp}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
                      Purpose:     Override standard ADM method
                      Notes:       
                    ------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
    {include/i-logfin.i}

/* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
                      Purpose:     Override standard ADM method
                      Notes:       
                    ------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */

    {utp/ut9000.i "ESDC218" "12.01.00.001"}

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

    /* Code placed here will execute AFTER standard behavior.    */
    SESSION:DATA-ENTRY-RETURN = TRUE.
  
    FIND FIRST dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = c-pto-controle
        NO-ERROR.

    ASSIGN 
        fi-pto-controle = dc-pto-controle.cod-pto-controle
        fi-descricao    = dc-pto-controle.desc-pto-controle
        fi-local        = dc-pto-controle.local-pto
        fi-tipo         = IF tp-cor = "1" THEN "Cor Unica" ELSE "Colorido"
        fi-depSaida     = dep-saida
        fi-localSai     = loc-saida
        fi-depEntrada   = dep-entrada
        fi-localEnt     = loc-entrada
        fi-cor          = c-cor
        fi-total        = "0".

    DISPLAY 
        fi-pto-controle
        fi-descricao 
        fi-local
        fi-tipo
        fi-depSaida  
        fi-localSai  
        fi-depEntrada  
        fi-localEnt
        fi-cor  
        fi-total
        WITH FRAME f-amrp.
        
    IF tp-cor = "1" THEN 
        fi-cor:SENSITIVE = FALSE.
    ELSE
        fi-cor:SENSITIVE = TRUE.
            

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-add D-Dialog 
PROCEDURE pi-add :
/*------------------------------------------------------------------------------
    PROCEDURE pi-add :
    Purpose:
    Notes:
------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE h-api AS HANDLE NO-UNDO.
    DEFINE VARIABLE i-seq AS INTEGER NO-UNDO.

    // main
    EMPTY TEMP-TABLE tt-reporte.
    EMPTY TEMP-TABLE tt-reporte-item.
    EMPTY TEMP-TABLE tt-erro-api-aux.
    EMPTY TEMP-TABLE tt-erro-api.
    
    DO WITH FRAME f-amrp:
        
        FOR FIRST tt-rg NO-LOCK
            WHERE tt-rg.codigo = fi-rg:SCREEN-VALUE.
        END.
        IF AVAILABLE tt-rg THEN
        DO: 
            RUN utp/ut-msgs.p ("show", 17006, "Item j� existe").
            NEXT.
        END.             
            
        CREATE tt-reporte.
        ASSIGN 
            tt-reporte.cod-pto-controle = fi-pto-controle:SCREEN-VALUE 
            tt-reporte.cod-depos-orig   = fi-depSaida:SCREEN-VALUE 
            tt-reporte.cod-localiz-orig = fi-localSai:SCREEN-VALUE
            tt-reporte.cod-depos-dest   = fi-depEntrada:screen-value
            tt-reporte.cod-localiz-dest = fi-localEnt:SCREEN-VALUE
            tt-reporte.cod-usuario      = c-seg-usuario
            tt-reporte.rg-item          = fi-rg:SCREEN-VALUE .
        

        /* Monta TT Itens */
        CREATE tt-reporte-item.
        ASSIGN 
            tt-reporte-item.rg-item = fi-rg:SCREEN-VALUE 
            tt-reporte-item.cod-cor = fi-cor:SCREEN-VALUE.
            
    END.
    
    RUN esapi/esapi018.p PERSISTENT SET h-api.
    RUN pi-valida IN h-api (
        INPUT-OUTPUT TABLE tt-reporte,
        INPUT-OUTPUT TABLE tt-reporte-item,
        OUTPUT TABLE tt-erro-api-aux).
    DELETE PROCEDURE h-api.
    RUN pi-clone-erro.
    
    IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN 
        FOR EACH tt-erro-api.
            RUN utp/ut-msgs.p ('show', 17006, "Erro ao incluir RG: " + tt-erro-api.mensagem + ", " + STRING (tt-erro-api.tipo)).
            RETURN "NOK":U.
        END.
    ELSE
    DO:
        FIND FIRST tt-reporte-item NO-ERROR.
        
        i-seq = getSeq ().
        
        CREATE tt-rg.
        ASSIGN 
            tt-rg.seq       = i-seq
            tt-rg.codigo    = tt-reporte-item.rg-item
            tt-rg.cor       = tt-reporte-item.cod-cor
            tt-rg.descricao = tt-reporte-item.cCodLocaliz
            tt-rG.l-imp     = FALSE.

        // solicitado por alessandro em 11/08/23
        FOR FIRST dc-rg-item NO-LOCK
            WHERE dc-rg-item.rg-item = tt-reporte-item.rg-item,
            FIRST ITEM NO-LOCK
            WHERE ITEM.it-codigo = dc-rg-item.it-codigo.
        END. 
             
        FOR FIRST dc-cor NO-LOCK
            WHERE dc-cor.cod-cor = tt-rg.cor,
            FIRST dc-item-cor NO-LOCK
            WHERE dc-item-cor.it-codigo = ITEM.it-codigo
            AND dc-item-cor.it-codigo-cor = dc-cor.it-codigo.
        END.
        IF AVAILABLE dc-item-cor THEN 
        DO:
            FOR FIRST ITEM NO-LOCK
                WHERE ITEM.it-codigo = dc-item-cor.it-codigo-acab.
            END.
            IF AVAILABLE ITEM THEN 
                tt-rg.descricao = ITEM.desc-item.
        END.
        
        {&OPEN-BROWSERS-IN-QUERY-F-amrp}
                                            
        RUN pi-total.
        
        RETURN "OK":U.
                    
    END.        
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-clone-erro D-Dialog 
PROCEDURE pi-clone-erro :
/*------------------------------------------------------------------------------
                     Purpose:
                     Notes:
                    ------------------------------------------------------------------------------*/


    // main
    FOR EACH tt-erro-api-aux.
    
        CREATE tt-erro-api.
        BUFFER-COPY tt-erro-api-aux TO tt-erro-api.
        
        //RUN pi-debug (INPUT "pi-debug - tt-erro-api-aux: " + tt-erro-api.mensagem + " - " + STRING (tt-erro-api.tipo)). 
        
    END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-confirma D-Dialog 
PROCEDURE pi-confirma :
/*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE h-api   AS HANDLE  NO-UNDO.
    DEFINE VARIABLE l-rack  AS LOGICAL NO-UNDO.
    DEFINE VARIABLE l-auth  AS LOGICAL NO-UNDO.
    DEFINE VARIABLE h-acomp AS HANDLE  NO-UNDO.
    
       
    // main
    i-rack = 0.
    EMPTY TEMP-TABLE tt-erro-api.
    RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
    RUN pi-inicializar IN h-acomp ("Iniciando...").
    
    reporte:
    DO TRANSACTION ON ERROR UNDO, LEAVE.
    
        /* realizar apontamento */
        RUN pi-debug ("inicio pintura").
        FOR EACH tt-rg
            BREAK BY tt-rg.cor.
            
            IF FIRST-OF (tt-rg.cor) THEN 
            DO:
                EMPTY TEMP-TABLE tt-pintura.
                
                CREATE tt-pintura.
                ASSIGN 
                    tt-pintura.cod-usuario      = c-seg-usuario
                    tt-pintura.cod-pto-controle = c-pto-controle
                    tt-pintura.cod-usuar-auth   = c-auth
                    tt-pintura.cod-senha-auth   = c-senha
                    tt-pintura.cod-cor          = tt-rg.cor.
                    
                RUN pi-debug ("criando tt-pintura " + tt-pintura.cod-cor).
                
                EMPTY TEMP-TABLE tt-codigo.
                
            END.
            
            RUN pi-acompanhar IN h-acomp ("Pintura " + tt-rg.codigo).
            
            CREATE tt-codigo.
            tt-codigo.codigo = tt-rg.codigo.
            
            IF LAST-OF (tt-rg.cor) THEN 
            DO:
                RUN pi-debug(INPUT "rodar apontamento pintura inicio").
                
                RUN esapi/esapi009.p PERSISTENT SET h-api.
                RUN pi-apontamento IN h-api (
                    INPUT-OUTPUT TABLE tt-pintura,
                    INPUT TABLE tt-codigo,
                    OUTPUT TABLE tt-erro-api-aux).
                DELETE PROCEDURE h-api.
                RUN pi-debug(INPUT "rodar apontamento pintura fim").
                RUN pi-clone-erro.
                    
                IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN
                    UNDO, LEAVE reporte.
                
                /* se retornar autoriza��o, desfazer transa��o e gravar autentica��o, para solicitar aprovacao */
                l-auth = FALSE.
                FOR FIRST tt-pintura .
                END.
                
                IF AVAILABLE tt-pintura AND tt-pintura.l-auth THEN
                DO:
                    l-auth = TRUE.
                    UNDO, LEAVE reporte.
                END.
                    
            END.
        END.
        RUN pi-debug ("fim pintura").
        
        /**
        *  3o. gerar novo rack e ativar o mesmo
        **/
        i-rack = 0.
        l-rack = IF tp-rack = "1" THEN TRUE ELSE FALSE.
        RUN pi-debug ("inicio rack: " + STRING (l-rack)).
        
        IF l-rack THEN 
        DO:
            FOR LAST dc-rack NO-LOCK.
            END.
            i-rack = DECIMAL (dc-rack.nr-rack) + 1.
            RELEASE dc-rack.
            
            RUN pi-debug(INPUT "criando rack: " + STRING (i-rack)).
            
            DO WHILE NOT AVAILABLE dc-rack.
                
                FOR FIRST dc-rack NO-LOCK
                    WHERE dc-rack.nr-rack = STRING (i-rack).
                END.       
                IF NOT AVAILABLE dc-rack THEN 
                DO:
                    CREATE dc-rack.
                    ASSIGN 
                        dc-rack.nr-rack          = STRING (i-rack)
                        dc-rack.cod-pto-controle = ''
                        dc-rack.data             = TODAY
                        dc-rack.desc-rack        = "Rack criado automaticamente pelo FDCW015_001 - coletor"
                        dc-rack.hora             = STRING(TIME, "hh:mm:ss")
                        dc-rack.situacao         = 1
                        dc-rack.tipo-pto         = 1
                        dc-rack.usuario          = c-seg-usuario.
                END.
                ELSE 
                    i-rack = DECIMAL (dc-rack.nr-rack) + 1.
            END.
            RUN pi-debug ("fim rack: " + STRING (i-rack)).
            
            /**
            *  3o. montagem do rack
            **/ 
            RUN pi-debug ("inicio montagem rack").
            
            RUN esapi\esapi006.p PERSISTENT SET h-api.
            
            /* Monta tt-input */
            EMPTY TEMP-TABLE tt-rg-item.
            FOR EACH tt-rg.
                CREATE tt-rg-item.
                ASSIGN 
                    tt-rg-item.codigo = tt-rg.codigo.
            END. 
            
            
            /**
            *  buscar o ponto de controle para montagem do rack
            ***/
            FOR FIRST dc-pto-controle NO-LOCK
                WHERE dc-pto-controle.cod-pto-controle = c-pto-controle.
            END.
            
            FOR EACH tt-rg-item.
            
                RUN pi-acompanhar IN h-acomp ("Itens rack " + tt-rg-item.codigo).
                
                EMPTY TEMP-TABLE tt-monta-rack.
                
                CREATE tt-monta-rack.
                ASSIGN 
                    tt-monta-rack.cod-usuario      = c-seg-usuario 
                    tt-monta-rack.cod-pto-controle = c-pto-controle
                    tt-monta-rack.nr-rack          = "RC" + STRING (i-rack)
                    tt-monta-rack.rg-item          = tt-rg-item.codigo.
                   
                RUN pi-valida-entrada-rack IN h-api (
                    INPUT-OUTPUT TABLE tt-monta-rack,
                    OUTPUT TABLE tt-erro-api-aux).
                RUN pi-clone-erro.
                                   
                IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN 
                DO:
                    DELETE PROCEDURE h-api.
                    UNDO, LEAVE reporte.   
                END.
                
            END.
            
            /* se nao deu erro, validar o fifo 
            RUN pi-valida-fifo IN h-api (
                INPUT-OUTPUT TABLE tt-monta-rack,
                INPUT-OUTPUT TABLE tt-rg-item,
                OUTPUT TABLE tt-erro-api-aux).
            RUN pi-clone-erro.
                                        
            IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN
            DO:
                DELETE PROCEDURE h-api. 
                UNDO, LEAVE reporte.   
            END.         
            */
           
            /* se nao deu erro na valida rack e fifo, efetivar montagem rack */
            RUN pi-efetiva-monta-rack IN h-api (
                INPUT-OUTPUT TABLE tt-monta-rack,
                INPUT TABLE tt-rg-item,
                OUTPUT TABLE tt-erro-api-aux).
            DELETE PROCEDURE h-api NO-ERROR.
            RUN pi-clone-erro.
            
            IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN 
                UNDO, LEAVE reporte.
            RUN pi-debug ("fim montagem").
        
        END.  /* if l-rack */        
        
        
        /**
        *  se ja realizou a pintura com sucesso, fazer o reporte
        **/
        RUN pi-acompanhar IN h-acomp ("Reporte " + STRING (i-rack)).
        RUN pi-reporte (INPUT l-rack, INPUT i-rack).
        IF RETURN-VALUE = "NOK":U THEN 
            UNDO, LEAVE reporte.
            
    END.
    RUN pi-finalizar IN h-acomp.
    
    IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN 
    DO:
        EMPTY TEMP-TABLE tt-erro.
        
        FOR EACH tt-erro-api.
            CREATE tt-erro.
            ASSIGN 
                tt-erro.cd-erro  = IF tt-erro-api.tipo = 1 THEN 17006 ELSE 15825 
                tt-erro.mensagem = tt-erro-api.mensagem + ", tipo :" + STRING (tt-erro-api.tipo).
        END.
        
        //RUN cdp/cd0666.w (INPUT TABLE tt-erro).
        RUN esp/esdc200l.w (INPUT TABLE tt-erro).
        
        RETURN "NOK":U.            
    END.
    ELSE 
    DO:
        EMPTY TEMP-TABLE tt-erro.
        
        FOR EACH tt-erro-api.
            CREATE tt-erro.
            ASSIGN 
                tt-erro.cd-erro  = IF tt-erro-api.tipo = 1 THEN 17006 ELSE 15825
                tt-erro.mensagem = tt-erro-api.mensagem + ", tipo :" + STRING (tt-erro-api.tipo).
        END.
        
        //RUN cdp/cd0666.w (INPUT TABLE tt-erro).
        RUN esp/esdc200l.w (INPUT TABLE tt-erro).
        
        RETURN "OK":U.
    END. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-debug D-Dialog 
PROCEDURE pi-debug :
/*------------------------------------------------------------------------------
     Purpose:
    Notes:
------------------------------------------------------------------------------*/

    // param
    DEFINE INPUT PARAMETER p-texto AS CHARACTER NO-UNDO.
    
    // main
/*    OUTPUT TO VALUE (SESSION:TEMP-DIRECTORY + "debug.txt") APPEND.*/
/*    EXPORT p-texto.                                               */
/*    OUTPUT CLOSE.                                                 */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-delete D-Dialog 
PROCEDURE pi-delete :
/*------------------------------------------------------------------------------
                         Purpose:
                         Notes:
                        ------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE i-cont AS INTEGER NO-UNDO.
    
    // main
    RUN utp/ut-msgs.p ('show', 27100, 'Confirma exclus�o? ~~ Deseja excluir o RG item corrente').
    IF RETURN-VALUE = "yes" THEN 
    DO:
        
        DO i-cont = 1 TO br-rg:NUM-SELECTED-ROWS IN FRAME f-amrp.
            br-rg:FETCH-SELECTED-ROW (i-cont) IN FRAME f-amrp.
            DELETE tt-rg.
        END.
        
    END.        

    RUN pi-total.
    
    {&OPEN-BROWSERS-IN-QUERY-F-amrp}
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-etiqueta D-Dialog 
PROCEDURE pi-etiqueta :
/*------------------------------------------------------------------------------
     Purpose:
     Notes:
------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE h-acomp AS HANDLE NO-UNDO.
    DEFINE BUFFER b-item FOR ITEM.
    
    // main
    RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
    RUN pi-inicializar IN h-acomp ("Gerando impress�o...").
    
    FOR FIRST es-etq-impressora NO-LOCK
        WHERE es-etq-impressora.cod-impressora = c-impressora.
    END.
    IF AVAILABLE es-etq-impressora THEN 
    DO:
        br-rg:FETCH-SELECTED-ROW (1) IN FRAME f-amrp.
        
        IF AVAILABLE tt-rg THEN 
        DO:
            FOR FIRST dc-rg-item NO-LOCK
                WHERE dc-rg-item.rg-item =  tt-rg.codigo,
                FIRST ITEM NO-LOCK
                WHERE ITEM.it-codigo = dc-rg-item.it-codigo.
            END.
            IF AVAILABLE item THEN 
            DO:
                RUN pi-acompanhar IN h-acomp ("Gerando RG: " + dc-rg-item.rg-item).
                
                FOR FIRST dc-cor NO-LOCK
                    WHERE dc-cor.cod-cor = tt-rg.cor,
                    FIRST dc-item-cor NO-LOCK
                    WHERE dc-item-cor.it-codigo = ITEM.it-codigo
                    AND dc-item-cor.it-codigo-cor = dc-cor.it-codigo.
                END.
                IF NOT AVAILABLE dc-item-cor THEN 
                DO:
                    RUN utp/ut-msgs.p ('show', 17006, "Item cor n�o encontrado: " + ITEM.it-codigo + ' cor ' + c-cor).
                    RUN pi-finalizar IN h-acomp.
                    RETURN NO-APPLY.
                END.
                
                FOR FIRST b-item NO-LOCK
                    WHERE b-item.it-codigo = dc-item-cor.it-codigo-acab.
                END.
                
                EMPTY TEMP-TABLE tt-etiq.
                CREATE tt-etiq.
                tt-etiq.c-valor = STRING(INT(dc-rg-item.rg-item),"9999999999999") + ";" + b-ITEM.desc-item + ";" + b-item.it-codigo + ";" + b-item.codigo-refer.
                
                RUN esp/esapi017.p (
                    INPUT es-etq-impressora.end-impressora,
                    INPUT 0,
                    INPUT 0,
                    INPUT TABLE tt-etiq).
                    
                tt-rg.l-imp = TRUE.
                {&OPEN-BROWSERS-IN-QUERY-F-amrp}              
                      
                    
            END.
        END.
        ELSE
            RUN utp/ut-msgs.p ("show", 17006, "RG n�o selecionado").
            
    END.
    
    RUN pi-finalizar IN h-acomp.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-reporte D-Dialog 
PROCEDURE pi-reporte :
/*------------------------------------------------------------------------------
                     Purpose:
                     Notes:
                    ------------------------------------------------------------------------------*/


    /* vars */
    DEFINE VARIABLE c-item     AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-itens    AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont     AS INTEGER   NO-UNDO.
    DEFINE VARIABLE h-esapi013 AS HANDLE    NO-UNDO.
    DEFINE VARIABLE h-api      AS HANDLE    NO-UNDO.
    
    /* parama */
    DEFINE INPUT PARAMETER l-rack AS LOGICAL NO-UNDO.
    DEFINE INPUT PARAMETER p-rack AS DECIMAL NO-UNDO.
    
    /* main */
    /**
    *  2o. reporte de produ��o
    **/
    //RUN pi-debug ("inicio reporte").
    RUN esapi/esapi013 PERSISTENT SET h-api.
    
    EMPTY TEMP-TABLE tt-reporte.
    EMPTY TEMP-TABLE tt-reporte-item.
        
    CREATE tt-reporte.
    ASSIGN 
        tt-reporte.cod-pto-controle = c-pto-controle
        tt-reporte.cod-depos-orig   = dep-saida
        tt-reporte.cod-localiz-orig = loc-saida
        tt-reporte.cod-depos-dest   = dep-entrada
        tt-reporte.cod-localiz-dest = loc-entrada
        tt-reporte.cod-usuario      = c-seg-usuario
        tt-reporte.cod-senha        = c-senha.
    
    /**
    *  rodar a validacao do rg-item
    **/
    FIND dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle 
        NO-ERROR.
                   
    IF NOT VALID-HANDLE(h-esapi013) THEN 
    DO:
        RUN esapi/esapi013.p PERSISTENT SET h-esapi013.
        IF NOT VALID-HANDLE(h-esapi013) THEN 
        DO:
            RUN criarErro (INPUT "N�o foi poss�vel executar rotina de reporte.").
            RETURN "NOK":U.
        END.
    END.
       
    IF l-rack THEN 
    DO:
           
        EMPTY TEMP-TABLE tt-erro-api-aux.
        EMPTY TEMP-TABLE tt-reporte-item.
           
        CREATE tt-reporte-item.
        ASSIGN 
            tt-reporte-item.rg-item = "RC" + STRING (p-rack).
           //RUN pi-debug ("itens do reporte" + tt-reporte-item.rg-item).
           
        RUN incluiItem IN h-esapi013 (
            INPUT TABLE tt-reporte, 
            INPUT TABLE tt-reporte-item,
            OUTPUT TABLE tt-erro-api-aux).   
        RUN pi-clone-erro.
           
    END.
    ELSE
    DO:
           
        DO TRANS:
               
            FOR EACH tt-rg.
               
            
                EMPTY TEMP-TABLE tt-erro-api-aux.
                EMPTY TEMP-TABLE tt-reporte-item.
               
                CREATE tt-reporte-item.
                ASSIGN 
                    tt-reporte-item.cod-cor = tt-rg.cor
                    tt-reporte-item.rg-item = tt-rg.codigo.
                       
                RUN incluiItem IN h-esapi013 (
                    INPUT TABLE tt-reporte, 
                    INPUT TABLE tt-reporte-item,
                    OUTPUT TABLE tt-erro-api-aux).
                           
                RUN pi-clone-erro.
                IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN 
                    UNDO, LEAVE.
                    
            END.
            
        END.
                           
    END.
      
        
    /* se nao houver erros, realizar o reporte */
    IF NOT CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo > 0) THEN 
    DO:

        EMPTY TEMP-TABLE tt-erro-api-aux.
        RUN confirmaReporte IN h-esapi013 (INPUT TABLE tt-reporte, OUTPUT TABLE tt-erro-api-aux).

        RUN pi-clone-erro.              
                                    
    END.
    
    DELETE PROCEDURE h-esapi013.

    IF CAN-FIND(FIRST tt-erro-api
        WHERE tt-erro-api.tipo = 1) THEN 
        RETURN "NOK":U.
                
    //RUN pi-debug ("fim reporte").
    
    RETURN "OK":U.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-total D-Dialog 
PROCEDURE pi-total :
/*------------------------------------------------------------------------------
                         Purpose:
                         Notes:
                        ------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE i-cont AS INTEGER NO-UNDO.
    
    // main
    i-cont = 0.
    FOR EACH tt-rg.
        i-cont = i-cont + 1.
    END.
    fi-total:SCREEN-VALUE IN FRAME f-amrp = STRING (i-cont) .
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-rg"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
                          Purpose:     
                          Parameters:  <none>
                          Notes:       
                        -------------------------------------------------------------*/
    DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
    DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
    RUN pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION getSeq D-Dialog 
FUNCTION getSeq RETURNS INTEGER
  (  ):
/*------------------------------------------------------------------------------
 Purpose:
 Notes:
------------------------------------------------------------------------------*/

    DEFINE VARIABLE result AS INTEGER NO-UNDO.
    DEFINE BUFFER b-tt-rg FOR tt-rg.
    
    FOR EACH b-tt-rg.
        IF RESULT < b-tt-rg.seq THEN 
            RESULT = b-tt-rg.seq.
    END.
    RESULT = RESULT + 1.
    

    RETURN result.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

