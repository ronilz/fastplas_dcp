&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC217 12.01.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE l-ok       AS LOGICAL   INITIAL TRUE NO-UNDO.
DEFINE VARIABLE c-erro     AS CHAR      NO-UNDO.

DEFINE INPUT PARAM cAction AS CHAR NO-UNDO.

DEF NEW GLOBAL SHARED VAR c-usuar-sessao         AS CHAR NO-UNDO.

{esapi\esapi017.p}

DEFINE VARIABLE v-dt-saldo     AS DATE        NO-UNDO.
DEFINE VARIABLE v-nr-ficha     AS INTEGER     NO-UNDO.
DEFINE VARIABLE v-nr-contagem  AS DECIMAL     NO-UNDO.
DEFINE VARIABLE v-val-apurado  AS DECIMAL     NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancInv AUTO-GO 
     LABEL "Cancelar" 
     SIZE 20 BY 1.25.

DEFINE BUTTON bt-ok 
     LABEL "Confirmar" 
     SIZE 20 BY 1.25.

DEFINE VARIABLE c-cod-depos AS CHARACTER FORMAT "X(256)":U 
     LABEL "Dep." 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-cod-localiz AS CHARACTER FORMAT "X(256)":U 
     LABEL "Localiz" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-contagem AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Contagem" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-desc-item AS CHARACTER FORMAT "X(256)":U 
     LABEL "Descri��o" 
     VIEW-AS FILL-IN 
     SIZE 35.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-dt-saldo AS DATE FORMAT "99/99/9999":U 
     LABEL "Saldo" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-ficha AS CHARACTER FORMAT "X(256)":U 
     LABEL "Ficha" 
     VIEW-AS FILL-IN 
     SIZE 35.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-it-codigo AS CHARACTER FORMAT "X(256)":U 
     LABEL "Item" 
     VIEW-AS FILL-IN 
     SIZE 35.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-leitura AS CHARACTER FORMAT "X(256)":U 
     LABEL "Leitura" 
     VIEW-AS FILL-IN 
     SIZE 35.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-un AS CHARACTER FORMAT "X(256)":U 
     LABEL "UN" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY .79 NO-UNDO.

DEFINE VARIABLE c-val-apurado AS DECIMAL FORMAT "->,>>>,>>9.99":U INITIAL 0 
     LABEL "Quantidade" 
     VIEW-AS FILL-IN 
     SIZE 14.29 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 23.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     SPACE(50.07) SKIP(25.07)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Apontamento Inventario" WIDGET-ID 100.

DEFINE FRAME F-Apont-Inv
     c-leitura AT ROW 2.42 COL 9.43 COLON-ALIGNED WIDGET-ID 40
     c-ficha AT ROW 3.33 COL 9.43 COLON-ALIGNED WIDGET-ID 24
     c-it-codigo AT ROW 4.25 COL 9.43 COLON-ALIGNED WIDGET-ID 26
     c-desc-item AT ROW 5.17 COL 9.43 COLON-ALIGNED WIDGET-ID 28
     c-cod-depos AT ROW 6.08 COL 9.43 COLON-ALIGNED WIDGET-ID 30
     c-un AT ROW 6.08 COL 30.43 COLON-ALIGNED WIDGET-ID 32
     c-cod-localiz AT ROW 7 COL 9.43 COLON-ALIGNED WIDGET-ID 34
     c-dt-saldo AT ROW 7 COL 30.43 COLON-ALIGNED WIDGET-ID 42
     c-contagem AT ROW 7.92 COL 9.43 COLON-ALIGNED WIDGET-ID 36
     c-val-apurado AT ROW 8.83 COL 9.43 COLON-ALIGNED WIDGET-ID 38
     bt-ok AT ROW 23.25 COL 5 WIDGET-ID 8
     bt-cancInv AT ROW 23.25 COL 27 WIDGET-ID 12
     RECT-3 AT ROW 1.25 COL 2 WIDGET-ID 44
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Apontamento Inventario" WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* REPARENT FRAME */
ASSIGN FRAME F-Apont-Inv:FRAME = FRAME D-Dialog:HANDLE.

/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME F-Apont-Inv
                                                                        */
/* SETTINGS FOR FILL-IN c-cod-depos IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-cod-localiz IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-contagem IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-item IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-dt-saldo IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-ficha IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-it-codigo IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-un IN FRAME F-Apont-Inv
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Apontamento Inventario */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F-Apont-Inv
&Scoped-define SELF-NAME bt-cancInv
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancInv D-Dialog
ON CHOOSE OF bt-cancInv IN FRAME F-Apont-Inv /* Cancelar */
DO:
    //ASSIGN fi-identificacao = "".
    //DISP fi-identificacao WITH FRAME F-Login.
    //HIDE FRAME F-Menu01.
    //VIEW FRAME F-Login.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok D-Dialog
ON CHOOSE OF bt-ok IN FRAME F-Apont-Inv /* Confirmar */
DO:
  EMPTY TEMP-TABLE tt-erro-api.

  if trim(c-val-apurado:SCREEN-VALUE IN FRAME F-Apont-Inv) = "" then do: 
    CREATE tt-erro-api. 
    ASSIGN tt-erro-api.tipo = 1 
           tt-erro-api.mensagem = "Quantidade invalida". 
  END.
  else do: 
    ASSIGN v-dt-saldo = date(c-dt-saldo:SCREEN-VALUE IN FRAME F-Apont-Inv) 
           v-nr-ficha = int(c-ficha:SCREEN-VALUE IN FRAME F-Apont-Inv) 
           v-nr-contagem = INT(c-contagem:SCREEN-VALUE IN FRAME F-Apont-Inv) 
           v-val-apurado = dec(c-val-apurado).
           
    run pi-atualiza-ficha (INPUT c-usuar-sessao,
                           INPUT v-dt-saldo, 
                           INPUT v-nr-ficha,
                           INPUT v-nr-contagem,
                           INPUT v-val-apurado,
                           OUTPUT TABLE tt-erro-api).
  END.

  FOR EACH tt-erro-api NO-LOCK: 
     MESSAGE tt-erro-api.tipo SKIP
             tt-erro-api.mensagem
      VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
  END.                                         

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-cod-depos
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-cod-depos D-Dialog
ON RETURN OF c-cod-depos IN FRAME F-Apont-Inv /* Dep. */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-cod-localiz
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-cod-localiz D-Dialog
ON RETURN OF c-cod-localiz IN FRAME F-Apont-Inv /* Localiz */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-contagem
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-contagem D-Dialog
ON RETURN OF c-contagem IN FRAME F-Apont-Inv /* Contagem */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-desc-item
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-desc-item D-Dialog
ON RETURN OF c-desc-item IN FRAME F-Apont-Inv /* Descri��o */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-dt-saldo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-dt-saldo D-Dialog
ON RETURN OF c-dt-saldo IN FRAME F-Apont-Inv /* Saldo */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-ficha
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-ficha D-Dialog
ON RETURN OF c-ficha IN FRAME F-Apont-Inv /* Ficha */
DO:
  ASSIGN c-leitura.
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-it-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-it-codigo D-Dialog
ON RETURN OF c-it-codigo IN FRAME F-Apont-Inv /* Item */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-leitura
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-leitura D-Dialog
ON LEAVE OF c-leitura IN FRAME F-Apont-Inv /* Leitura */
DO:
  RUN Pi-valida-ficha ( INPUT c-leitura, OUTPUT TABLE tt-inventario, OUTPUT TABLE tt-erro-api).

  IF NOT CAN-FIND(FIRST tt-erro-api WHERE tt-erro-api.tipo = 1 NO-LOCK) THEN
      DO: 
         FIND FIRST tt-inventario NO-LOCK NO-ERROR. 
           IF AVAILABLE tt-inventario THEN DO: 
            ASSIGN c-leitura:SCREEN-VALUE IN FRAME F-Apont-Inv      = ''
                   c-dt-saldo:SCREEN-VALUE IN FRAME F-Apont-Inv     = STRING(tt-inventario.dt-saldo)
                   c-ficha:SCREEN-VALUE IN FRAME F-Apont-Inv        = string(tt-inventario.nr-ficha)
                   c-it-codigo:SCREEN-VALUE IN FRAME F-Apont-Inv    = tt-inventario.it-codigo
                   c-desc-item:SCREEN-VALUE IN FRAME F-Apont-Inv    = tt-inventario.desc-item
                   c-un:SCREEN-VALUE IN FRAME F-Apont-Inv           = tt-inventario.un
                   c-cod-depos:SCREEN-VALUE IN FRAME F-Apont-Inv    = tt-inventario.cod-depos
                   c-cod-localiz:SCREEN-VALUE IN FRAME F-Apont-Inv  = tt-inventario.cod-localiz
                   c-contagem:SCREEN-VALUE IN FRAME F-Apont-Inv     = string(tt-inventario.nr-contagem)
                   c-val-apurado:SCREEN-VALUE IN FRAME F-Apont-Inv  = tt-inventario.contagem.
           END. 
           ELSE DO:
            ASSIGN c-leitura:SCREEN-VALUE IN FRAME F-Apont-Inv      = ''
                   c-dt-saldo:SCREEN-VALUE IN FRAME F-Apont-Inv     = ''
                   c-ficha:SCREEN-VALUE IN FRAME F-Apont-Inv        = ''
                   c-it-codigo:SCREEN-VALUE IN FRAME F-Apont-Inv    = ''
                   c-desc-item:SCREEN-VALUE IN FRAME F-Apont-Inv    = ''
                   c-un:SCREEN-VALUE IN FRAME F-Apont-Inv           = ''
                   c-cod-depos:SCREEN-VALUE IN FRAME F-Apont-Inv    = ''
                   c-cod-localiz:SCREEN-VALUE IN FRAME F-Apont-Inv  = ''
                   c-contagem:SCREEN-VALUE IN FRAME F-Apont-Inv     = '0'
                   c-val-apurado:SCREEN-VALUE IN FRAME F-Apont-Inv  = '0'.

           END.
      END. 
      ELSE DO: 
        IF TRIM(c-leitura) <> '' THEN
          FOR EACH tt-erro-api NO-LOCK: 
             MESSAGE tt-erro-api.tipo SKIP
                     tt-erro-api.mensagem
              VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
          END. 
      END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-leitura D-Dialog
ON RETURN OF c-leitura IN FRAME F-Apont-Inv /* Leitura */
DO:
  ASSIGN c-leitura.
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-un
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-un D-Dialog
ON RETURN OF c-un IN FRAME F-Apont-Inv /* UN */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-val-apurado
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-val-apurado D-Dialog
ON RETURN OF c-val-apurado IN FRAME F-Apont-Inv /* Quantidade */
DO:
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME D-Dialog
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
  HIDE FRAME F-Apont-Inv.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
  DISPLAY c-leitura c-ficha c-it-codigo c-desc-item c-cod-depos c-un 
          c-cod-localiz c-dt-saldo c-contagem c-val-apurado 
      WITH FRAME F-Apont-Inv.
  ENABLE RECT-3 c-leitura c-val-apurado bt-ok bt-cancInv 
      WITH FRAME F-Apont-Inv.
  {&OPEN-BROWSERS-IN-QUERY-F-Apont-Inv}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "ESDC217" "12.01.00.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  VIEW FRAME F-Apont-Inv.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartDialog, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

