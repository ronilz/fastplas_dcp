&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME w-window
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS w-window 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC200A 12.01.00.003}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> <m�dulo>}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */

DEFINE INPUT PARAM c-user  AS CHARACTER NO-UNDO.

DEF NEW GLOBAL SHARED VAR c-usuar-sessao         AS CHAR NO-UNDO.

/*
DEFINE VARIABLE c-pass  AS CHARACTER NO-UNDO.
DEFINE VARIABLE l-ok    AS LOGICAL   INITIAL TRUE NO-UNDO.
DEFINE VARIABLE c-erro  AS CHAR      NO-UNDO.
*/
{include/ttdef1.i}
{include/ttdef2.i}
{esapi/esapi001p.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE JanelaDetalhe
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Menu01
&Scoped-define BROWSE-NAME br-menu01

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-menu

/* Definitions for BROWSE br-menu01                                     */
&Scoped-define FIELDS-IN-QUERY-br-menu01 tt-menu.descricao   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-menu01   
&Scoped-define SELF-NAME br-menu01
&Scoped-define QUERY-STRING-br-menu01 FOR EACH tt-menu NO-LOCK         WHERE tt-menu.ind-menu    = TRUE           AND tt-menu.cod-usuario = c-user         INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-br-menu01 OPEN QUERY {&SELF-NAME}     FOR EACH tt-menu NO-LOCK         WHERE tt-menu.ind-menu    = TRUE           AND tt-menu.cod-usuario = c-user         INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-br-menu01 tt-menu
&Scoped-define FIRST-TABLE-IN-QUERY-br-menu01 tt-menu


/* Definitions for FRAME F-Menu01                                       */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Menu01 ~
    ~{&OPEN-QUERY-br-menu01}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-3 br-menu01 bt-ok 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR w-window AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-ok AUTO-GO 
     LABEL "Logoff" 
     SIZE 10 BY 1.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 1.5.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-menu01 FOR 
      tt-menu SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-menu01
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-menu01 w-window _FREEFORM
  QUERY br-menu01 NO-LOCK DISPLAY
      tt-menu.descricao FORMAT "x(60)"
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-LABELS NO-ROW-MARKERS SEPARATORS SIZE 48 BY 21.83
         FONT 1 ROW-HEIGHT-CHARS .7 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Menu01
     br-menu01 AT ROW 1.25 COL 2 WIDGET-ID 500
     bt-ok AT ROW 23.5 COL 21 WIDGET-ID 4
     RECT-3 AT ROW 23.25 COL 2 WIDGET-ID 6
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Menu Principal"
         DEFAULT-BUTTON bt-ok WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: JanelaDetalhe
   Allow: Basic,Browse,DB-Fields,Smart,Window,Query
   Container Links: 
   Add Fields to: Neither
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW w-window ASSIGN
         HIDDEN             = YES
         TITLE              = "Login de Usu�rio"
         HEIGHT             = 25
         WIDTH              = 50
         MAX-HEIGHT         = 49
         MAX-WIDTH          = 298.43
         VIRTUAL-HEIGHT     = 49
         VIRTUAL-WIDTH      = 298.43
         MIN-BUTTON         = no
         MAX-BUTTON         = no
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         FONT               = 1
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB w-window 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/w-window.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW w-window
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Menu01
   FRAME-NAME                                                           */
/* BROWSE-TAB br-menu01 RECT-3 F-Menu01 */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-window)
THEN w-window:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-menu01
/* Query rebuild information for BROWSE br-menu01
     _START_FREEFORM
OPEN QUERY {&SELF-NAME}
    FOR EACH tt-menu NO-LOCK
        WHERE tt-menu.ind-menu    = TRUE
          AND tt-menu.cod-usuario = c-user
        INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION"
     _Query            is OPENED
*/  /* BROWSE br-menu01 */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME w-window
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-window w-window
ON END-ERROR OF w-window /* Login de Usu�rio */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL w-window w-window
ON WINDOW-CLOSE OF w-window /* Login de Usu�rio */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br-menu01
&Scoped-define SELF-NAME br-menu01
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-menu01 w-window
ON MOUSE-SELECT-CLICK OF br-menu01 IN FRAME F-Menu01
DO:
    IF  AVAIL tt-menu THEN DO:
        IF tt-menu.cod-programa = "fdcw002_001"  THEN RUN esp/esdc201.w (INPUT tt-menu.cod-programa). // Transferencia de Materias
        IF tt-menu.cod-programa = "fdcw003_001"  THEN RUN esp/esdc202.w (INPUT tt-menu.cod-programa). // Requisicao de Materias
        IF tt-menu.cod-programa = "fdcw003_002"  THEN RUN esp/esdc203.w (INPUT tt-menu.cod-programa). // Devolucao Requisicao Materiais
        IF tt-menu.cod-programa = "fdcw004_001"  THEN RUN esp/esdc204.w (INPUT tt-menu.cod-programa). // Reporte de Producao
        IF tt-menu.cod-programa = "fdcw004_002"  THEN RUN esp/esdc205.w (INPUT tt-menu.cod-programa). // Estorno Reporte de Producao
        IF tt-menu.cod-programa = "fdcw005_001"  THEN RUN esp/esdc206.w (INPUT tt-menu.cod-programa). // Ativacao de RG
        IF tt-menu.cod-programa = "fdcw006a_001" THEN RUN esp/esdc207.w (INPUT tt-menu.cod-programa). // Mnt Seq Rack
        IF tt-menu.cod-programa = "fdcw006_001"  THEN RUN esp/esdc208.w (INPUT tt-menu.cod-programa). // Montagem de Rack
        IF tt-menu.cod-programa = "fdcw006_002"  THEN RUN esp/esdc209.w (INPUT tt-menu.cod-programa). // Desmontagem de Rack
        IF tt-menu.cod-programa = "fdcw007_001"  THEN RUN esp/esdc210.w (INPUT tt-menu.cod-programa). // Separacao de Embarque
        IF tt-menu.cod-programa = "fdcw009_001"  THEN RUN esp/esdc211.w (INPUT tt-menu.cod-programa). // Apontamento de Pintura
        IF tt-menu.cod-programa = "fdcw010_001"  THEN RUN esp/esdc213.w (INPUT tt-menu.cod-programa). // Reporte por Item
        IF tt-menu.cod-programa = "fdcw011_001"  THEN RUN esp/esdc214.w (INPUT tt-menu.cod-programa). // Reporte por Sucata
        IF tt-menu.cod-programa = "fdcw012_001"  THEN RUN esp/esdc215.w (INPUT tt-menu.cod-programa). // Reporte por Refugo
        IF tt-menu.cod-programa = "fdcw013_001"  THEN RUN esp/esdc216.w (INPUT tt-menu.cod-programa). // Reporte de Producao
        IF tt-menu.cod-programa = "fdcw014_001"  THEN RUN esp/esdc217.w (INPUT tt-menu.cod-programa). // Apontamento Inventario
        IF tt-menu.cod-programa = "fdcw015_001"  THEN RUN esp/esdc218.w (INPUT tt-menu.cod-programa). // Reporte Montagem Apontamento
        IF tt-menu.cod-programa = "fdcw040_001"  THEN RUN esp/esdc219.w (INPUT tt-menu.cod-programa). // Separacao de Embarque Novo
        IF tt-menu.cod-programa = "fdcw020_001"  THEN RUN esp/esdc220.w (INPUT tt-menu.cod-programa). // Baixar Rack j� Faturado
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok w-window
ON CHOOSE OF bt-ok IN FRAME F-Menu01 /* Logoff */
DO:
    //ASSIGN fi-identificacao = "".
    //DISP fi-identificacao WITH FRAME F-Login.
    //HIDE FRAME F-Menu01.
    //VIEW FRAME F-Login.
    APPLY 'CLOSE' TO THIS-PROCEDURE.
    RETURN.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK w-window 


/* ***************************  Main Block  *************************** */
ASSIGN c-usuar-sessao = c-user.
/* Include custom  Main Block code for SmartWindows. */
{src/adm/template/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects w-window  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available w-window  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI w-window  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(w-window)
  THEN DELETE WIDGET w-window.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI w-window  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE RECT-3 br-menu01 bt-ok 
      WITH FRAME F-Menu01 IN WINDOW w-window.
  {&OPEN-BROWSERS-IN-QUERY-F-Menu01}
  VIEW w-window.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy w-window 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-exit w-window 
PROCEDURE local-exit :
/* -----------------------------------------------------------
  Purpose:  Starts an "exit" by APPLYing CLOSE event, which starts "destroy".
  Parameters:  <none>
  Notes:    If activated, should APPLY CLOSE, *not* dispatch adm-exit.   
-------------------------------------------------------------*/
   APPLY "CLOSE":U TO THIS-PROCEDURE.
   
   RETURN.
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize w-window 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  {include/win-size.i}
  
  {utp/ut9000.i "ESDC200A" "12.01.00.003"}

  SESSION:DATA-ENTRY-RETURN = YES.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  RUN pi-menu (INPUT c-user, 
               OUTPUT TABLE tt-menu).

  {&OPEN-BROWSERS-IN-QUERY-F-Menu01}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records w-window  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "tt-menu"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed w-window 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

