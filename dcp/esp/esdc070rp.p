/*****************************************************************************
* Empresa  : TOTVS TRIAH
* Cliente  : FASTPLAS 
* Programa : ESDC070RP
* Descricao: CRIA��O POR FAIXA RACK
* Data     : 12/JANEIRO/2014
* Versao   : 2.06.00.001
******************************************************************************/

/* DEFINICOES DE INCLUDES */
define buffer estabel for mgcad.estabel.
define buffer empresa for mgcad.empresa.

{include/i-prgvrs.i}
{utp/ut-glob.i}
{include/i-rpvar.i}  

/* *** DEFINICOES DE PREPROCESSORS *** */

/* *** DEFINICOES DE TEMP-TABLES *** */

define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"  LABEL  'Arquivo.......'
    field usuario          as char format "x(12)"  LABEL  'Usuario.......' 
    field data-exec        as date                  LABEL 'Execu��o......'
    field hora-exec        as integer
    FIELD deUltimoRack      AS DECIMAL              LABEL 'Ultimo Rack...'  FORMAT '99999999999'
    FIELD deProximoRack     AS DECIMAL              LABEL 'Proximo Rack..'  FORMAT '99999999999'
    FIELD iQtdeRack         AS INTEGER              LABEL 'Qtde Racks....'  FORMAT '>>>>>>>>>9'
    FIELD cExeRackIni       AS CHARACTER            LABEL 'Inicio........'  FORMAT 'x(256)'    
    FIELD cExeRackFim       AS CHARACTER            LABEL 'Termino.......' FORMAT 'x(256)'    
    FIELD iCodEmitente      AS INTEGER              LABEL 'Emitente......' FORMAT '>>>>>>>9'    
    .

define temp-table tt-digita no-undo
    field ordem            as integer   format ">>>>9"
    field exemplo          as character format "x(30)"
    index id ordem.

DEFINE TEMP-TABLE tt-raw-digita FIELD raw-digita AS RAW.

DEFINE TEMP-TABLE ttLogRack NO-UNDO
    FIELD cNrRack   LIKE dc-rack.nr-rack
    FIELD cMsg    AS CHARACTER FORMAT 'X(60)' LABEL 'Mensagem'

    INDEX idxRack IS PRIMARY 
    cNrRack
    .

/* *** DEFINICOES DE VARIAVEIS GLOBAIS *** */

/* *** DEFINICOES DE VARIAVEIS LOCAIS *** */

DEFINE VARIABLE h-acomp         AS HANDLE       NO-UNDO.
DEFINE VARIABLE cHora           AS CHARACTER    NO-UNDO.

/* *** DEFINICOES DE BUFFERS *** */

/* *** DEFINICAO DE PARAMETROS *** */

DEFINE INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEFINE INPUT PARAMETER TABLE FOR tt-raw-digita.

/* *** DEFINICOES DE FORMS *** */

FORM
    'I M P R E S S � O'     AT  01
    SKIP
    '-----------------'     AT 01
    SKIP(1)
    tt-param.arquivo        AT 10
    SKIP(1)
    tt-param.usuario        AT 10
    SKIP(1)
    tt-param.data-exec      AT 10
    SPACE(1)
    cHora NO-LABEL
    SKIP(2)

    'S E L E � � O'             AT 01
    SKIP
    '-------------'             AT 01
    SKIP(1)
    tt-param.deUltimoRack       AT 10
    SKIP(1)
    tt-param.deProximoRack      AT 10
    SKIP(1)
    tt-param.iQtdeRack          AT 10
    SKIP(1)
    tt-param.iCodEmitente       AT 10
    SKIP(1)
    tt-param.cExeRackIni        AT 10
    SKIP(1)
    tt-param.cExeRackFim        AT 10
    
    WITH SIDE-LABEL WIDTH 300 FRAME f-parametro STREAM-IO.

FIND FIRST param-global NO-LOCK NO-ERROR.

BLOCO_PRINCIPAL:
DO :

   RUN piCabecalhoRodape.
   RUN piInicializacao.
   
   CREATE tt-param. 
   RAW-TRANSFER raw-param TO tt-param.
   FIND FIRST tt-param NO-LOCK NO-ERROR.

   ASSIGN cHora = STRING(tt-param.hora-exec, 'HH:MM:SS').

   FOR EACH tt-raw-digita:
      CREATE tt-digita.
      RAW-TRANSFER raw-digita TO tt-digita.
   END.

   RUN piCreateRack.


   {include/i-rpout.i &tofile = tt-param.arquivo}

   VIEW 
       FRAME f-cabec
       FRAME f-rodape.

   FOR EACH ttLogRack.
       DISP ttLogRack WITH WIDTH 300 STREAM-IO  FRAME fLog DOWN.
   END.


   PAGE.

   DISPLAY
       tt-param.arquivo        
       tt-param.usuario        
       tt-param.data-exec 
       cHora
       tt-param.deUltimoRack
       tt-param.deProximoRack
       tt-param.iQtdeRack
       tt-param.iCodEmitente
       tt-param.cExeRackIni
       tt-param.cExeRackFim
       WITH FRAME f-parametro.
   
   {include/i-rpclo.i}
       
   RUN pi-finalizar IN h-acomp.
   
END.

PROCEDURE piCreateRack.

    DEFINE VARIABLE deNrAuxRack AS DECIMAL     NO-UNDO.
    DEFINE VARIABLE deNumeroRack AS DECIMAL     NO-UNDO.
    
    REPEAT deNrAuxRack = 1 TO tt-param.iQtdeRack:

        ASSIGN 
            deNumeroRack = tt-param.deProximoRack + (deNrAuxRack - 1)
            .

        CREATE ttLogRack.
        ASSIGN 
            ttLogRack.cNrRack   = STRING(deNumeroRack, '99999999999')
            ttLogRack.cMsg      = 'Numero j� cadastrado no sistema. Registro ignorado!!'
            .

        RUN pi-acompanhar IN h-acomp (INPUT 'Verificando Rack: ' + ttLogRack.cNrRack).

        
        FIND dc-rack
            WHERE dc-rack.nr-rack = ttLogRack.cNrRack
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-rack THEN
            NEXT.
        
        CREATE dc-rack.
        ASSIGN 
            dc-rack.cod-emitente        = tt-param.iCodEmitente
            dc-rack.cod-pto-controle    = ''
            dc-rack.data                = TODAY
            dc-rack.desc-rack           = 'Rack nr. ' + ttLogRack.cNrRack
            dc-rack.hora                = cHora
            dc-rack.local-pto           = ''
            dc-rack.nr-rack             = ttLogRack.cNrRack
            dc-rack.situacao            = 1
            dc-rack.tipo-pto            = 1
            dc-rack.usuario             = c-seg-usuario
                                        
            ttLogRack.cMsg              = 'Rack cadastrado com sucesso!'
            .
    END.

END PROCEDURE.

PROCEDURE piCabecalhoRodape:

    FIND FIRST param-global NO-LOCK NO-ERROR.
    IF AVAILABLE param-global THEN DO:
        FIND empresa WHERE empresa.ep-codigo = param-global.empresa-prin
                   NO-LOCK NO-ERROR.
      ASSIGN 
          c-empresa      = empresa.razao-social
          c-titulo-relat = "CRIA��O FAIXA RACK"
          c-sistema      = "Especifico"
          c-programa     = "ESDC070RP"
          c-versao       = "2.06"
          c-revisao      = "00.001".

      FORM HEADER 
             FILL("-", 132) FORMAT "X(132)" 
             SKIP
             c-empresa      AT 01 
             c-titulo-relat AT 50
             "Folha:"       AT 122 
             PAGE-NUMBER    AT 128 FORMAT ">>>>9" 
             SKIP
             FILL("-", 112) FORMAT "X(110)" 
             TODAY FORMAT "99/99/9999"
             "-" STRING(TIME, "HH:MM:SS") 
             SKIP(1)
             WITH WIDTH 300 NO-LABELS NO-BOX PAGE-TOP STREAM-IO FRAME f-cabec.

      ASSIGN c-rodape = "DATASUL - " + c-sistema 
                      + " - "        + c-programa 
                      + " - V:"      + c-versao 
                      + "."          + c-revisao
             c-rodape = FILL("-", 132 - LENGTH(c-rodape)) + c-rodape.

      FORM HEADER 
             c-rodape FORMAT "X(132)"
             WITH STREAM-IO WIDTH 300 NO-LABELS NO-BOX PAGE-BOTTOM FRAME f-rodape.

   END.
END PROCEDURE.

PROCEDURE piInicializacao:

   RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
   RUN pi-inicializar IN h-acomp (INPUT "Impress�o").
   RUN pi-seta-tipo   IN h-acomp (INPUT 6).

END PROCEDURE.

