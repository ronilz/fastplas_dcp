&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*:T*******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i ESDC20 12.01.00.001}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE l-ok       AS LOGICAL   INITIAL TRUE NO-UNDO.
DEFINE VARIABLE l-erro     AS LOGICAL   INITIAL FALSE NO-UNDO.
DEFINE VARIABLE c-erro     AS CHAR      NO-UNDO.
DEFINE VARIABLE c-ponto-controle AS CHARACTER   NO-UNDO.

DEFINE INPUT PARAM cAction AS CHAR NO-UNDO.

DEF NEW GLOBAL SHARED VAR c-usuar-sessao         AS CHAR NO-UNDO.

{include\ttdef2.i}
{esapi/esapi020i.i}
{esapi\esapi001p.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-cancBaixa AUTO-GO 
     LABEL "Cancelar" 
     SIZE 20 BY 1.25.

DEFINE BUTTON bt-ok 
     LABEL "Confirmar" 
     SIZE 20 BY 1.25.

DEFINE VARIABLE c-pto-controle AS CHARACTER FORMAT "X(256)":U 
     LABEL "Pto. Controle" 
     VIEW-AS FILL-IN 
     SIZE 32 BY .79 NO-UNDO.

DEFINE VARIABLE c-rack AS CHARACTER FORMAT "X(256)":U 
     LABEL "NR. Rack" 
     VIEW-AS FILL-IN 
     SIZE 32 BY .79 NO-UNDO.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 48 BY 23.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     SPACE(50.09) SKIP(25.09)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 1
         TITLE "Baixar Rack J� Faturado" WIDGET-ID 100.

DEFINE FRAME F-Baixa-Rack
     c-pto-controle AT ROW 3 COL 13 COLON-ALIGNED WIDGET-ID 46
     c-rack AT ROW 3.92 COL 13 COLON-ALIGNED WIDGET-ID 40
     bt-ok AT ROW 23.25 COL 5 WIDGET-ID 8
     bt-cancBaixa AT ROW 23.25 COL 27 WIDGET-ID 12
     RECT-3 AT ROW 1.25 COL 2 WIDGET-ID 44
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 50 BY 25
         FONT 1
         TITLE "Baixar Rack J� Faturado" WIDGET-ID 300.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{src/adm/method/containr.i}
{include/d-dialog.i}
{utp/ut-glob.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* REPARENT FRAME */
ASSIGN FRAME F-Baixa-Rack:FRAME = FRAME D-Dialog:HANDLE.

/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* SETTINGS FOR FRAME F-Baixa-Rack
                                                                        */
/* SETTINGS FOR FILL-IN c-pto-controle IN FRAME F-Baixa-Rack
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "SHARE-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Baixar Rack J� Faturado */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME F-Baixa-Rack
&Scoped-define SELF-NAME bt-cancBaixa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-cancBaixa D-Dialog
ON CHOOSE OF bt-cancBaixa IN FRAME F-Baixa-Rack /* Cancelar */
DO:
    //ASSIGN fi-identificacao = "".
    //DISP fi-identificacao WITH FRAME F-Login.
    //HIDE FRAME F-Menu01.
    //VIEW FRAME F-Login.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-ok
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-ok D-Dialog
ON CHOOSE OF bt-ok IN FRAME F-Baixa-Rack /* Confirmar */
DO:
  DO ON ERROR UNDO, LEAVE: 
      /* Instancia BOS */ 
      IF NOT VALID-HANDLE(hesapi020) THEN 
       RUN esapi/esapi020.p PERSISTENT SET hesapi020.
         CREATE ttParam. 
         ASSIGN ttParam.cRgItemIni = '' 
                ttParam.cRgItemFim = 'ZZZZZZZZZZZZZZZZZZZZZ' 
                ttParam.cNrRackIni = c-rack:SCREEN-VALUE IN FRAME F-Baixa-Rack
                ttParam.cNrRackFim = c-rack:SCREEN-VALUE IN FRAME F-Baixa-Rack 
                ttParam.cTipo = 'NR RACK'
                ttParam.cCodUsuario = c-usuar-sessao
                ttParam.cCodPtoControle = c-pto-controle:SCREEN-VALUE IN FRAME F-Baixa-Rack . 
         RUN pi-valida IN hesapi020 (INPUT TABLE ttParam, OUTPUT TABLE tt-erro-api). 
         if NOT TEMP-TABLE tt-erro-api:HAS-RECORDS THEN DO: 
          RUN pi-processa IN hesapi020 (INPUT TABLE ttParam, 
                                        OUTPUT TABLE tt-erro-api). 
          END. 
          IF VALID-HANDLE(hesapi020) THEN
            DELETE PROCEDURE hesapi020.
    END. 

    FOR EACH tt-erro-api NO-LOCK: 
        MESSAGE tt-erro-api.tipo SKIP
                tt-erro-api.mensagem
         VIEW-AS ALERT-BOX INFORMATION BUTTONS OK.
    END.                  

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-pto-controle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-pto-controle D-Dialog
ON RETURN OF c-pto-controle IN FRAME F-Baixa-Rack /* Pto. Controle */
DO:
  ASSIGN c-pto-controle.
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME c-rack
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL c-rack D-Dialog
ON RETURN OF c-rack IN FRAME F-Baixa-Rack /* NR. Rack */
DO:
  ASSIGN c-rack.
  APPLY "LEAVE" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME D-Dialog
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */
ASSIGN c-ponto-controle = ''.

RUN esp/esdc200b.w (INPUT  cAction,
                    OUTPUT c-ponto-controle,
                    OUTPUT l-erro).
IF  l-erro THEN DO:
    APPLY 'ENTRY' TO bt-cancBaixa IN FRAME F-Baixa-Rack.
    RETURN NO-APPLY.
END.
ELSE DO:
    ASSIGN c-pto-controle:SCREEN-VALUE IN FRAME F-Baixa-Rack = c-ponto-controle.
  
    /* Sele��o de deposito */
    /* RUN esp/esdc200c.w (INPUT cAction,
                        INPUT c-pto-controle,
                        OUTPUT dep-entrada,
                        OUTPUT loc-entrada,
                        OUTPUT l-erro).
    IF  l-erro THEN DO:
        APPLY 'ENTRY' TO bt-cancTrans IN FRAME F-Transferencia.
        RETURN NO-APPLY.
    END. */
END.

{src/adm/template/dialogmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
  HIDE FRAME F-Baixa-Rack.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
  DISPLAY c-pto-controle c-rack 
      WITH FRAME F-Baixa-Rack.
  ENABLE RECT-3 c-rack bt-ok bt-cancBaixa 
      WITH FRAME F-Baixa-Rack.
  {&OPEN-BROWSERS-IN-QUERY-F-Baixa-Rack}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-destroy D-Dialog 
PROCEDURE local-destroy :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'destroy':U ) .
  {include/i-logfin.i}

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  {utp/ut9000.i "ESDC220" "12.01.00.001"}

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  VIEW FRAME F-Baixa-Rack.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartDialog, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

