/*******************************************************************************
** Programa...: ES0004RP
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..:
*******************************************************************************/
/* Include de Controle de Vers�o */
{include/i-prgvrs.i ESDC013RP.P 1.00.00.000}
define buffer empresa     for mgcad.empresa.
define buffer localizacao for mgcad.localizacao.
DEFINE BUFFER estabelec              FOR mgadm.estabelec.
DEFINE BUFFER cidade                 FOR mgdis.cidade.   
DEFINE BUFFER pais                   FOR mguni.pais. 
define buffer unid-feder             for mgcad.unid-feder. 

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
   field destino          as integer
   field arquivo          as char format "x(35)"
   field usuario          as char format "x(12)"
   field data-exec        as date
   field hora-exec        as integer
   field classifica       as integer
   field desc-classifica  as char format "x(40)"
   FIELD nr-rack-ini      LIKE dc-rack.nr-rack
   FIELD nr-rack-fim      LIKE dc-rack.nr-rack
   FIELD cod-pto-controle LIKE dc-pto-controle.cod-pto-controle.

DEFINE TEMP-TABLE tt-raw-digita
   FIELD raw-digita    AS RAW.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Variveis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Variveis  */
{include\ttdef1.i}    
{include\ttdef2.i}  

DEF VAR h-acomp AS HANDLE NO-UNDO.
DEFINE VARIABLE h-api AS HANDLE     NO-UNDO.
DEFINE NEW GLOBAL SHARED VARIABLE c-seg-usuario AS CHARACTER  NO-UNDO.


/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ESDC021RP"
      c-versao       = "1.00"
      c-revisao      = "1.00.000"
      c-empresa      = empresa.nome
      c-sistema      = "ESP"
      c-titulo-relat = c-titulo-relat.

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).

FIND dc-pto-controle
    WHERE dc-pto-controle.cod-pto-controle = tt-param.cod-pto-controle
    NO-LOCK NO-ERROR.

PUT "RACK          Descri��o                      Pto. Cont. Usuario       Sit." SKIP
    "------------- ------------------------------ ---------- ------------- ----" SKIP.
    /*"12345678901234567890123456789012345678901234567890123456789012345678901234567890"*/
bloco:
DO TRANSACTION ON ERROR UNDO bloco, NEXT.
    FOR EACH dc-rack
        WHERE dc-rack.nr-rack >= tt-param.nr-rack-ini
          AND dc-rack.nr-rack <= tt-param.nr-rack-fim
          AND dc-rack.situacao = 1
        NO-LOCK.
    
        RUN pi-acompanhar IN h-acomp (INPUT "Rack: " + dc-rack.nr-rack).

        /**
        *  Desmontar o rack
        **/
        IF NOT VALID-HANDLE(h-api) THEN
            RUN esapi/esapi006.p PERSISTENT SET h-api.

        FOR EACH tt-monta-rack EXCLUSIVE-LOCK. DELETE tt-monta-rack. END.

        CREATE tt-monta-rack.
        ASSIGN tt-monta-rack.cod-usuario      = c-seg-usuario
               tt-monta-rack.cod-pto-controle = dc-pto-controle.cod-pto-controle
               tt-monta-rack.nr-rack          = "RC" + dc-rack.nr-rack
               tt-monta-rack.rg-item          = "".

        RUN pi-efetiva-desmonta-rack IN h-api (INPUT TABLE tt-monta-rack,
                                               INPUT TABLE tt-rg-item,
                                               OUTPUT TABLE tt-erro-api).
        DELETE PROCEDURE h-api.

        IF CAN-FIND(FIRST tt-erro-api
                    WHERE tt-erro-api.tipo = 1) THEN
        DO:
            PUT SKIP(2)
                "*** Erros Encontrados na Desmontagem ***" SKIP
                "----------------------------------------" SKIP(1).

            FOR EACH tt-erro-api NO-LOCK
                WHERE tt-erro-api.tipo = 1.

                PUT UNFORMATTED tt-erro-api.mensagem SKIP.
            END.
        END.
        ELSE
        DO:
            PUT dc-rack.nr-rack AT 01
                dc-rack.desc-rack AT 15 FORMAT "x(30)"
                tt-monta-rack.cod-pto-controle AT 46
                tt-monta-rack.cod-usuario AT 57
                " OK " AT 71
                SKIP.
        END.
    END.
END.

RUN pi-finalizar IN h-acomp.
RETURN "OK":U.
