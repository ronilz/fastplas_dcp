/*******************************************************************************
** Programa...: ESDC030RP
** Vers�o.....: 1.000
** Data.......: NOV/2006
** Autor......: Datasul WA
** Descri��o..: Listagem de Duplicidade Movimento Estoque
*******************************************************************************/
/* Include de Controle de Vers�o */
{include/i-prgvrs.i ESDC030RP.P 2.06.00.001}
define buffer empresa     for mgcad.empresa.
define buffer localizacao for mgcad.localizacao.
DEFINE BUFFER estabelec              FOR mgadm.estabelec.
DEFINE BUFFER cidade                 FOR mgdis.cidade.   
DEFINE BUFFER pais                   FOR mguni.pais. 
define buffer unid-feder             for mgcad.unid-feder. 

/* Defini��o da temp-table tt-param */
define temp-table tt-param no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG

    FIELD rg-item          AS CHAR EXTENT 2
    FIELD simula           AS LOGICAL
    FIELD excel            AS LOGICAL.
    
DEFINE TEMP-TABLE tt-raw-digita
    FIELD raw-digita    AS RAW.

define temp-table tt-param-1 no-undo
    field destino          as integer
    field arquivo          as char format "x(35)"
    field usuario          as char format "x(12)"
    field data-exec        as date
    field hora-exec        as integer
    field classifica       as integer
    field desc-classifica  as char format "x(40)"
    field modelo-rtf       as char format "x(35)"
    field l-habilitaRtf    as LOG
    field arq-entrada      as char
    FIELD rg-item-ini      AS CHAR
    FIELD rg-item-fim      AS CHAR.

DEFINE VARIABLE c-arquivo AS CHARACTER   NO-UNDO.
DEFINE VARIABLE h-acomp AS HANDLE      NO-UNDO.
DEFINE VARIABLE iCount AS INTEGER     NO-UNDO.
DEFINE NEW GLOBAL SHARED VARIABLE c-seg-usuario AS CHAR NO-UNDO.

/* Recebimento de Par�metros */
DEF INPUT PARAMETER raw-param AS RAW NO-UNDO.
DEF INPUT PARAMETER TABLE FOR tt-raw-digita.

CREATE tt-param.
RAW-TRANSFER raw-param TO tt-param.

/* Include Padr�o para Vari�veis de Relatorio  */
{include/i-rpvar.i}

/* Defini��o de Vari�veis  */
DEF VAR cont-lin AS INT NO-UNDO.

/* Include Padr�o Para Output de Relat�rios */
{include/i-rpout.i}
DEFINE STREAM str-excel.

/* Include com a Defini��o da Frame de Cabe�alho e Rodap� */
{include/i-rpcab.i}

/* Bloco Principal do Programa */
FIND FIRST empresa NO-LOCK NO-ERROR.

ASSIGN c-programa     = "ESDC032RP"
       c-versao       = "2.06"
       c-revisao      = "00.001"
       c-empresa      = empresa.nome
       c-sistema      = "ESP"
       c-titulo-relat = "RG Item sem Movimento".

ASSIGN c-arquivo = "".
IF tt-param.excel THEN DO:
    ASSIGN c-arquivo = SESSION:TEMP-DIR + "esdc032" + string(ETIME) + ".csv".

    OUTPUT STREAM str-excel TO VALUE(c-arquivo).

    PUT STREAM str-excel UNFORMATT
        "RG Item;Item;Rack;Pto Controle;Deposito;Sit"
        SKIP.
END.

VIEW FRAME f-cabec.
VIEW FRAME f-rodape.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
{utp/ut-liter.i Imprimindo *}

RUN pi-inicializar in h-acomp (INPUT RETURN-VALUE).
iCount = 0.
FOR EACH dc-rg-item NO-LOCK
    WHERE dc-rg-item.rg-item >= tt-param.rg-item[1]
      AND dc-rg-item.rg-item <= tt-param.rg-item[2].

    iCount = iCount + 1.
    IF (iCount MOD 1000) = 0 THEN 
        RUN pi-acompanhar IN h-acomp ("Registros Lidos: " + STRING(iCount) ).

    FIND FIRST dc-movto-pto-controle
        WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
        NO-LOCK NO-ERROR.
    IF NOT AVAIL dc-movto-pto-controle THEN DO:
        DISP 'RG Item Sem Movimento: ' dc-rg-item.rg-item.

        IF tt-param.excel THEN
            PUT STREAM str-excel UNFORMATT
                CHR(39) + dc-rg-item.rg-item + CHR(39) + ";"
                CHR(39) + dc-rg-item.it-codigo + CHR(39) + ";"
                CHR(39) + dc-rg-item.nr-rack + CHR(39)  + ";"
                dc-rg-item.cod-pto-controle + ";"
                dc-rg-item.cod-depos-orig + ";"
                dc-rg-item.situacao
                SKIP.

        IF NOT tt-param.simula THEN DO:
            EMPTY TEMP-TABLE tt-param-1.
            CREATE tt-param-1.
            ASSIGN tt-param-1.destino = 3
                   tt-param-1.arquivo = SESSION:TEMP-DIRECTORY + "esdc032.txt"
                   tt-param-1.usuario = c-seg-usuario
                   tt-param-1.data-exec = TODAY
                   tt-param-1.hora-exec = MTIME
                   tt-param-1.rg-item-ini = dc-rg-item.rg-item
                   tt-param-1.rg-item-fim = dc-rg-item.rg-item.

            RAW-TRANSFER tt-param-1 TO raw-param.
            RUN esp/esdc026rp.p (INPUT raw-param,
                                 INPUT TABLE tt-raw-digita).
        END.
    END.
END.

IF tt-param.excel THEN DO:
    OUTPUT STREAM str-excel CLOSE.
    OS-COMMAND SILENT START excel VALUE(c-arquivo).
END.
    


{include/i-rpclo.i &STREAM="stream str-rp"}

run pi-finalizar in h-acomp.
return "Ok":U.

