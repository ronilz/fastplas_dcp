{include\ttdef1.i}
{include\ttdef2.i}

DEF VAR h-api AS HANDLE NO-UNDO.
DEF VAR i-cont AS INTEGER.

RUN esapi/esapi005.p PERSISTENT SET h-api.

FOR EACH dc-rg-item
    WHERE dc-rg-item.situacao = 1
    NO-LOCK.

    SELECT COUNT(*) INTO i-cont FROM dc-movto-pto-controle
        WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item.

    IF i-cont > 1 THEN
    DO:
/*         DISP dc-rg-item.rg-item FORMAT "x(15)" */
/*              int(dc-rg-item.situacao)          */
/*          .                                     */

        
        RUN pi-ativa-rg IN h-api (INPUT "ATVEX",
                                  INPUT dc-rg-item.rg-item,
                                  OUTPUT TABLE tt-erro-api).


    END.
        

END.
