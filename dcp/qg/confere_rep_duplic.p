DEFINE VARIABLE i-cont AS INTEGER    NO-UNDO.
DEFINE VARIABLE h-acomp AS HANDLE     NO-UNDO.

RUN utp/ut-acomp.p PERSISTENT SET h-acomp.
RUN pi-inicializar IN h-acomp (INPUT "Iniciando").

FOR EACH dc-movto-pto-controle NO-LOCK
    WHERE dc-movto-pto-controle.cod-pto-controle BEGINS "REP"
    BREAK BY dc-movto-pto-controle.rg-item
          BY dc-movto-pto-controle.cod-pto-controle.
    

    IF FIRST-OF(dc-movto-pto-controle.rg-item) THEN
    DO:
        RUN pi-acompanhar IN h-acomp (INPUT dc-movto-pto-controle.rg-item).
        ASSIGN i-cont = 0.
    END.

    ASSIGN i-cont = i-cont + 1.

    IF LAST-OF(dc-movto-pto-controle.rg-item) THEN
    DO:
        IF i-cont > 1 THEN
            DISP dc-movto-pto-controle.rg-item
                 i-cont.
    END.
END.

RUN pi-finalizar IN h-acomp.
