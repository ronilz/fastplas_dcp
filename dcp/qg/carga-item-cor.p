

DEFINE VARIABLE c-item1 AS CHARACTER  NO-UNDO.
DEFINE VARIABLE c-item2 AS CHARACTER  NO-UNDO.
DEFINE VARIABLE c-item3 AS CHARACTER  NO-UNDO.
DEFINE VARIABLE c-input AS CHARACTER  NO-UNDO.

UPDATE c-input FORMAT "x(70)" WITH SIDE-LABEL.

INPUT FROM VALUE(c-input).
REPEAT :
    IMPORT DELIMITER ";" c-item1 c-item2 c-item3.

    FIND dc-item-cor
        WHERE dc-item-cor.it-codigo = c-item1
          AND dc-item-cor.it-codigo-cor = c-item2
        NO-ERROR.
    IF NOT AVAIL dc-item-cor THEN DO:
        CREATE dc-item-cor.
        ASSIGN dc-item-cor.it-codigo = c-item1
               dc-item-cor.it-codigo-cor = c-item2.
    END.
    ASSIGN dc-item-cor.it-codigo-acab = c-item3.
END.
INPUT CLOSE.
