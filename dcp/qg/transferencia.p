/******************************************************************************
*      Programa .....:                                                        *
*      Versao........: 2.04.00.000                                            *
*      Data .........: FEV/2007                                               *
*      Sistema ......: Materiais (Estoque)                                    *
*      Empresa ......: DATASUL WA                                             *
*      Cliente ......: FASTPLAS                                               *
*      Programador ..: Ellen Sousa                                            *
*      Objetivo .....: Transferencia de Estoque                               *
******************************************************************************/

{cep/ceapi001.i} 
{cdp/cd0666.i}     

Def New Global Shared Var c-seg-usuar As Char No-undo.
Find First param-global No-lock No-error.

Find Item "0.30.600" No-lock No-error.

/************** Saida ***************/
create tt-movto.
Assign tt-movto.cod-versao-integracao = 1
       tt-movto.cod-prog-orig         = "teste"
       tt-movto.conta-contabil        = "0000113000097"
       tt-movto.dt-trans              = Today
       tt-movto.dt-vali-lote          = 12/31/9999
       tt-movto.nro-docto             = "123Teste"
       tt-movto.serie-docto           = ""
       tt-movto.cod-depos             = "exp"
       tt-movto.cod-estabel           = "1"
       tt-movto.it-codigo             = Item.it-codigo
       tt-movto.cod-refer             = ""
       tt-movto.cod-localiz           = ""
       tt-movto.lote                  = "aa"
       tt-movto.quantidade            = 50
       tt-movto.un                    = Item.un
       tt-movto.tipo-trans            = 2        /* 1 = Entrada, 2 = Sa�da */
       tt-movto.esp-docto             = 33
       tt-movto.usuario               = c-seg-usuar.
            
find conta-contab No-lock
     Where conta-contab.conta-contabil  = tt-movto.conta-contabil 
       And conta-contab.ep-codigo       = param-global.empresa-prin no-error.
        
if avail conta-contab then
    assign tt-movto.ct-codigo = conta-contab.ct-codigo
           tt-movto.sc-codigo = conta-contab.sc-codigo
           tt-movto.ct-db     = conta-contab.ct-codigo
           tt-movto.sc-db     = conta-contab.sc-codigo
           tt-movto.conta-db  = tt-movto.conta-contabil.

run cep/ceapi001.p (input-output table tt-movto,
                    input-output table tt-erro,
                    input yes).
For Each tt-movto. Delete tt-movto. End.

For Each tt-erro.
    MESSAGE tt-erro.cd-erro Skip
        tt-erro.mensagem

        VIEW-AS ALERT-BOX INFO BUTTONS OK.
End.

Find Item "0.30.600" No-lock No-error.

/************** Entrada ***************/
create tt-movto.
assign tt-movto.cod-versao-integracao = 1
       tt-movto.cod-prog-orig  = "teste3"
       tt-movto.conta-contabil = "0000113000097"
       tt-movto.dt-trans       = Today
       tt-movto.dt-vali-lote   = 12/31/9999
       tt-movto.nro-docto      = "123Teste1"
       tt-movto.serie-docto    = ""
       tt-movto.cod-depos      = "alm"
       tt-movto.cod-estabel    = "1"
       tt-movto.it-codigo      =  Item.it-codigo
       tt-movto.cod-refer      =  ""
       tt-movto.cod-localiz    =  ""
       tt-movto.lote           =  "aaa"
       tt-movto.quantidade     =  50
       tt-movto.un             = Item.un
       tt-movto.tipo-trans     = 1    
       tt-movto.esp-docto      = 33
       tt-movto.usuario        = c-seg-usuar.

            
find conta-contab No-lock
     Where conta-contab.conta-contabil  = tt-movto.conta-contabil 
       and conta-contab.ep-codigo       = param-global.empresa-prin no-error.


if avail conta-contab then
    assign tt-movto.ct-codigo = conta-contab.ct-codigo
           tt-movto.sc-codigo = conta-contab.sc-codigo
           tt-movto.ct-db     = conta-contab.ct-codigo
           tt-movto.sc-db     = conta-contab.sc-codigo
           tt-movto.conta-db  = tt-movto.conta-contabil.

run cep/ceapi001.p (input-output table tt-movto,
                    input-output table tt-erro,
                    input yes).

For Each tt-erro.
    MESSAGE tt-erro.cd-erro Skip
        tt-erro.mensagem

        VIEW-AS ALERT-BOX INFO BUTTONS OK.
End.
