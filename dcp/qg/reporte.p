/******************************************************************************
*      Programa .....:                                                        *
*      Versao........: 2.04.00.000                                            *
*      Data .........: FEV/2007                                               *
*      Sistema ......: Produ��o                                               *
*      Empresa ......: DATASUL WA                                             *
*      Cliente ......: FASTPLAS                                               *
*      Programador ..: Luiz Tartaroti                                            *
*      Objetivo .....: Reporte de ordem de produ��o                           *
******************************************************************************/
{include/ttdef1.i}
{include/ttdef2.i}
{cpp/cpapi001.i}
{cdp/cd0666.i}
{cpp/cpapi018.i}  /* Defini��o das temp-tables sumariza��o  */



/********* Definicao Temp-Tables Reporte da Ordem  ********    
/************** Definicao Temp-Table de Erros  *************/    */
                   
Find ord-prod 269580 No-lock No-error.
    /*Where */

/* RUN pi-sumariza (INPUT 141633). */

DISP ord-prod.nr-req-sum.
If Avail ord-prod Then Do:

    Find Item Where Item.it-codigo = ord-prod.it-codigo No-lock No-error.

    CREATE tt-rep-prod.
    ASSIGN tt-rep-prod.tipo                  = 1
           tt-rep-prod.nr-ord-produ          = ord-prod.nr-ord-produ
           tt-rep-prod.it-codigo             = ITEM.it-codigo
           tt-rep-prod.data                  = TODAY
           tt-rep-prod.qt-reporte            = 1
           tt-rep-prod.nro-docto             = "RZO-Teste"
           tt-rep-prod.procura-saldos        = TRUE
           tt-rep-prod.carrega-reservas      = TRUE
           tt-rep-prod.requis-automatica     = TRUE
           tt-rep-prod.prog-seg              = "CP0301"
           tt-rep-prod.finaliza-ordem        = FALSE
           tt-rep-prod.finaliza-oper         = TRUE
           tt-rep-prod.reserva               = TRUE
           tt-rep-prod.cod-depos             = "PRO"
           tt-rep-prod.cod-localiz           = ""
           tt-rep-prod.cod-depos-sai         = "PRO"
           tt-rep-prod.cod-local-sai         = ""
           tt-rep-prod.sequencia             = 0
           tt-rep-prod.qt-refugo             = 0
           tt-rep-prod.lote-serie            = ""
           tt-rep-prod.ct-codigo             = ord-prod.ct-codigo
           tt-rep-prod.sc-codigo             = ord-prod.sc-codigo
           tt-rep-prod.un                    = ord-prod.un
           tt-rep-prod.cod-versao-integracao = 001
           tt-rep-prod.tentativas            = 10
           tt-rep-prod.time-out              = 90.

    MESSAGE "antes" VIEW-AS ALERT-BOX.
    
    RUN cpp/cpapi001.p (INPUT-OUTPUT TABLE tt-rep-prod,
                        INPUT        TABLE tt-refugo,
                        INPUT        TABLE tt-res-neg,
                        INPUT        TABLE tt-apont-mob,
                        INPUT-OUTPUT TABLE tt-erro,
                        INPUT        YES) NO-ERROR.

    MESSAGE "depios " SKIP
            CAN-FIND(FIRST tt-erro) SKIP
            RETURN-VALUE
        VIEW-AS ALERT-BOX.

    For Each tt-erro:
        Message  "erro: " tt-erro.mensagem
            View-as ALERT-BOX INFO BUTTONS OK.
    End.
End.





PROCEDURE pi-sumariza:

    DEFINE INPUT PARAMETER p-nr-ord LIKE ord-prod.nr-ord-produ.

    DEFINE VARIABLE h-cpapi018 AS HANDLE     NO-UNDO.
    
    IF NOT VALID-HANDLE(h-cpapi018) THEN
        run cpp/cpapi018.p persistent set h-cpapi018 (input        table tt-dados,
                                                      input        table tt-ord-prod-2,
                                                      input-output table tt-req-sum,
                                                      input-output table tt-erro,
                                                      input        yes).

    for each tt-dados: delete tt-dados. end.

    create tt-dados.
    assign tt-dados.requis-por-ordem      = TRUE
           tt-dados.cod-versao-integracao = 001
           tt-dados.estado                = 1 /* Inclus�o */
           tt-dados.c-estab-ini           = ""
           tt-dados.c-estab-fim           = "ZZZ"
           tt-dados.i-linha-ini           = 0
           tt-dados.i-linha-fim           = 999
           tt-dados.i-ordem-ini           = p-nr-ord
           tt-dados.i-ordem-fim           = p-nr-ord
           tt-dados.d-data-ini            = 01/01/0001
           tt-dados.d-data-fim            = 12/31/9999.
    
    run pi-processa-sumaris in h-cpapi018 (input        table tt-dados,
                                           input        table tt-ord-prod-2,
                                           input-output table tt-req-sum,
                                           input-output table tt-erro,
                                           input        no).

    FOR EACH tt-erro NO-LOCK:
        
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1 
               tt-erro-api.mensagem = tt-erro.mensagem.
    END.

    IF h-cpapi018:PERSISTENT THEN DELETE PROCEDURE h-cpapi018.

END PROCEDURE.
