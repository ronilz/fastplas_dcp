def temp-table tt-embarque no-undo like embarque use-index ch-emb
    field i-sequen as int
    field ind-oper as int. /* 1 - Inclus�o
                              2 - Altera��o
                              3 - Elimina��o */

def temp-table tt-ped-venda no-undo
    field i-sequen    as int
    field nr-embarque like embarque.nr-embarque
    field nome-abrev  like ped-venda.nome-abrev
    field nr-pedcli   like ped-venda.nr-pedcli
    field ind-oper    as int /* 1 - Alocar
                                2 - Desalocar */

    index ch-pedido is primary
        nome-abrev
        nr-pedcli.

def temp-table tt-ped-ent no-undo
    field nome-abrev   as char
    field nr-pedcli    as char
    field nr-sequencia as int
    field it-codigo    as char
    field cod-refer    as char
    field nr-entrega   as int
    field qt-a-alocar  as deci
    field i-sequen     as int
    field nr-embarque  as int
    field nr-proc-exp  as char  
    index ch-entrega is primary
        nome-abrev
        nr-pedcli
        nr-sequencia
        it-codigo
        cod-refer
        nr-entrega.
        
def temp-table tt-it-pre-fat no-undo
    field nr-embarque  as int
    field nr-resumo    as int
    field nome-abrev   as char
    field nr-pedcli    as char
    field nr-sequencia as int
    field it-codigo    as char
    field qt-a-alocar  as deci
    field i-sequen     as int
    field nr-entrega   as int
    index ch-it-pre-fat is primary
        nr-embarque
        nr-resumo
        nome-abrev
        nr-pedcli
        nr-sequencia
        it-codigo
        nr-entrega.

def temp-table tt-deposito no-undo
    field sequen as int
    field cod-depos like deposito.cod-depos
    index ch-aeq-dep is primary unique
        sequen.

def temp-table tt-it-narrativa no-undo
    field nome-abrev   as char
    field nr-pedcli    as char
    field nr-sequencia as int
    field it-codigo    as char
    field cod-refer    as char
    field nr-entrega   as int
    field nr-embarque  as int
    field nr-resumo    as int
    field narrativa    as char.

def temp-table tt-item-filho no-undo
    field nome-abrev   as char
    field nr-pedcli    as char
    field nr-sequencia as int
    field it-codigo    as char
    field cod-refer    as char
    field nr-entrega   as int
    field qt-a-alocar  as deci
    index ch-entrega is primary
        nome-abrev
        nr-pedcli
        nr-sequencia
        it-codigo
        cod-refer
        nr-entrega.

def temp-table tt-erro no-undo
    field i-sequen  as int             
    field cd-erro   as int
    field mensagem  as char
    field parametro as char
    index ch-seq is primary
        i-sequen.
