/*--------------------------------------------------------------------------------------
**  Programa....: esft4000a.p 
**  Objetivo....: Rotina para gerar NF de saida
**  Autor.......: Douglas Ferrari - Datasul WA
**  Data........: 22/04/2007
**F
**
*--------------------------------------------------------------------------------------*/
/* temp table recebida da aplicacao */
/** Temp para montar a NF */

{include/tt-edit.i}
{include/pi-edit.i}

{utp/ut-glob.i}

DEFINE TEMP-TABLE tt-item-seq
    FIELD nr-sequencia AS INTEGER
    FIELD qt-pedida AS DEC.


DEFINE TEMP-TABLE tt-item-seq-estoq NO-UNDO
    FIELD nr-sequencia AS INTEGER
    FIELD cod-depos   AS CHAR
    FIELD cod-localiz AS CHAR
    FIELD qt-pedida AS DEC
    .

/* Definicao da tabela temporaria tt-notas-geradas, include {dibo/bodi317ef.i1} */
def temp-table tt-notas-geradas no-undo
    field rw-nota-fiscal as   rowid
    field nr-nota        like nota-fiscal.nr-nota-fis
    field seq-wt-docto   like wt-docto.seq-wt-docto.

DEFINE INPUT PARAMETER c-nr-pedcli         AS CHAR NO-UNDO.      
DEFINE INPUT PARAMETER c-nome-abrev        AS CHAR NO-UNDO.      
DEFINE INPUT PARAMETER c-cod-estabel       AS CHAR NO-UNDO.      
DEFINE INPUT PARAMETER c-serie             AS CHAR NO-UNDO.      
DEFINE INPUT PARAMETER c-dt-emis-nota      AS DATE NO-UNDO.      
DEFINE INPUT PARAMETER c-nat-operacao      AS CHAR NO-UNDO.      
DEFINE INPUT PARAMETER c-cod-canal-vendax  AS INT NO-UNDO.
DEFINE INPUT PARAMETER TABLE FOR tt-item-seq.
DEFINE INPUT PARAMETER TABLE FOR tt-item-seq-estoq.
DEFINE INPUT PARAMETER c-nome-transp       AS CHAR NO-UNDO.
DEFINE INPUT PARAMETER c-motorista         AS CHAR NO-UNDO.
DEFINE INPUT PARAMETER c-placa             AS CHAR NO-UNDO.
DEFINE INPUT PARAMETER c-ufPlaca           AS CHAR NO-UNDO.
DEFINE OUTPUT PARAM    p-nr-nota-fis       AS CHAR NO-UNDO.
DEFINE OUTPUT PARAM    p-ok                AS LOGICAL NO-UNDO.    
DEFINE OUTPUT PARAM TABLE FOR tt-notas-geradas.

/*                                                             */
/* DEFINE OUTPUT PARAMETER P-nro-nota AS CHAR NO-UNDO.         */
/* DEFINE OUTPUT PARAMETER P-row-nota-fiscal AS ROWID NO-UNDO. */
/* DEFINE OUTPUT PARAMETER P-error    AS CHAR NO-UNDO.         */
/*                                                             */
/* Defini��o da vari�veis */
def var h-bodi317pr          as handle no-undo.
def var h-bodi317sd          as handle no-undo.
def var h-bodi317im1bra      as handle no-undo.
def var h-bodi317va          as handle no-undo.
def var h-bodi317in          as handle no-undo.
def var h-bodi317ef          as handle no-undo.
def var l-proc-ok-aux        as log    no-undo.
def var c-ultimo-metodo-exec as char   no-undo.
def var da-dt-emis-nota      as date   no-undo.
def var da-dt-base-dup       as date   no-undo.
def var da-dt-prvenc         as date   no-undo.
def var c-seg-usuario        as char   no-undo.
def var i-seq-wt-docto       as int    no-undo.
DEF VAR de-qtd               AS DEC    NO-UNDO.
DEF VAR c-cod-canal-venda    AS CHAR   NO-UNDO.

DEF VAR hshowmsg             AS HANDLE NO-UNDO.

DEF VAR i-seq-wt-it-docto like wt-docto.seq-wt-docto NO-UNDO.          
DEF VAR l-procedimento-ok as   log                   NO-UNDO.                             

DEF VAR l-delete AS LOG NO-UNDO.

/* Def temp-table de erros. Ela tb�m est� definida na include dbotterr.i */
def temp-table rowerrors no-undo
    field errorsequence    as int
    field errornumber      as int
    field errordescription as char
    field errorparameters  as char
    field errortype        as char
    field errorhelp        as char
    field errorsubtype     as char.


/* Defini��o de um buffer para tt-notas-geradas */
def buffer b-tt-notas-geradas for tt-notas-geradas.

DEFINE TEMP-TABLE tt-wt-it-docto NO-UNDO LIKE wt-it-docto
   field r-rowid as rowid.



FIND  ped-venda   WHERE
      ped-venda.nome-abrev = c-nome-abrev AND
      ped-venda.nr-pedcli  = c-nr-pedcli
      NO-LOCK NO-ERROR.


/* Informa��es do embarque para c�lculo */
assign 
       c-cod-estabel     = c-cod-estabel /*ped-venda.cod-estabel     */
       c-serie           = c-serie
       c-nome-abrev      = ped-venda.nome-abrev    
       c-nr-pedcli       = ped-venda.nr-pedcli
       da-dt-emis-nota   = c-dt-emis-nota
       c-nat-operacao    = c-nat-operacao
       c-cod-canal-venda = string(c-cod-canal-vendax)
       p-ok              = FALSE.


/* Inicializa��o das BOS para C�lculo */
run dibo/bodi317in.p persistent set h-bodi317in.
run inicializaBOS in h-bodi317in(output h-bodi317pr,
                                 output h-bodi317sd,     
                                 output h-bodi317im1bra,
                                 output h-bodi317va).

/* In�cio da transa��o */
repeat trans:
    /* Limpar a tabela de erros em todas as BOS */
    run emptyRowErrors        in h-bodi317in.

    /* Cria o registro WT-DOCTO para o pedido */
    
    run criaWtDocto in h-bodi317sd
            (input  c-seg-usuario,
             input  c-cod-estabel,
             input  c-serie,
             input  "1", 
             input  c-nome-abrev,
             input  c-nr-pedcli,
             input  1,    
             input  9999, 
             input  da-dt-emis-nota,
             input  0,  
             input  c-nat-operacao,
             input  c-cod-canal-venda,
             output i-seq-wt-docto,
             output l-proc-ok-aux).
     
     FIND FIRST dc-embarque-trans EXCLUSIVE-LOCK 
          WHERE dc-embarque-trans.nome-abrev = c-nome-abrev
          AND   dc-embarque-trans.nr-pedcli  = c-nr-pedcli NO-ERROR.
     IF NOT AVAIL dc-embarque-trans THEN
     DO:
         CREATE dc-embarque-trans.
         ASSIGN dc-embarque-trans.nome-abrev = c-nome-abrev
                dc-embarque-trans.nr-pedcli  = c-nr-pedcli. 
     END.
     ASSIGN dc-embarque-trans.motorista      = c-nome-transp 
            dc-embarque-trans.transportadora = c-motorista   
            dc-embarque-trans.placa          = c-placa       
            dc-embarque-trans.uf-placa       = c-ufPlaca.     
    
    /* Busca poss�veis erros que ocorreram nas valida��es */
    run devolveErrosbodi317sd in h-bodi317sd(output c-ultimo-metodo-exec,
                                             output table RowErrors).

    /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
    find first RowErrors no-lock no-error.
    
    if  avail RowErrors THEN DO:
        {method/ShowMessage.i1}              
        {method/ShowMessage.i2 &Modal="yes"} 

    END.

    
    /* Caso ocorreu problema nas valida��es, n�o continua o processo */
    if  not l-proc-ok-aux then
        undo, leave.

    /* Limpar a tabela de erros em todas as BOS */
    run emptyRowErrors        in h-bodi317in.

    /* Gera os itens para o pedido, com tela de acompanhamento */
/*     run inicializaAcompanhamento      in h-bodi317sd.                       */
/*     run geraWtItDoctoComItensDoPedido in h-bodi317sd(output l-proc-ok-aux). */
/*     run finalizaAcompanhamento        in h-bodi317sd.                       */

    
/*     FIND FIRST dc-embarque-item WHERE                         */
/*                dc-embarque-item.nome-abrev = c-nome-abrev AND */
/*                dc-embarque-item.nr-pedcli  = c-nr-pedcli      */
/*                NO-LOCK NO-ERROR.                              */
/*                                                               */
/*     IF NOT AVAIL dc-embarque-item THEN DO:                    */


    FOR FIRST ped-venda NO-LOCK WHERE
              ped-venda.nome-abrev = c-nome-abrev AND
              ped-venda.nr-pedcli  = c-nr-pedcli,
        EACH ped-item OF ped-venda NO-LOCK,
        EACH tt-item-seq 
        WHERE tt-item-seq.nr-sequencia = ped-item.nr-sequencia
        NO-LOCK: 

        FIND ped-ent OF ped-item NO-LOCK NO-ERROR.

        RUN criaWtItDocto IN h-bodi317sd
                (INPUT ROWID(ped-ent),
                 INPUT "ped-ent",
                 INPUT ped-item.nr-sequencia,
                 INPUT ped-item.it-codigo,
                 INPUT ped-item.cod-refer,
                 INPUT ped-item.nat-operacao,
                 OUTPUT i-seq-wt-it-docto,
                 OUTPUT l-procedimento-ok).

        /* Busca poss�veis erros que ocorreram nas valida��es */
        run devolveErrosbodi317sd         in h-bodi317sd(output c-ultimo-metodo-exec,
                                                         output table RowErrors).
    
        find first RowErrors no-lock no-error.
        
        if  avail RowErrors then
            for each RowErrors:
                /* <Mostra os Erros/Advert�ncias encontradas */
    
             {method/ShowMessage.i1}              
             {method/ShowMessage.i2 &Modal="yes"} 
    
            end.
    
        IF l-proc-ok-aux = NO THEN UNDO,LEAVE.
    
        /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
        find first RowErrors no-lock no-error.
        
        /* Caso ocorreu problema nas valida��es, n�o continua o processo */
        if  not l-proc-ok-aux then
            undo, leave.
    
        run gravaInfGeraisWtItDocto in h-bodi317sd 
                    (input i-seq-wt-docto,
                     input i-seq-wt-it-docto,
                     input tt-item-seq.qt-pedida,
                     input ped-item.vl-preori /*de-vl-preori-ped*/,
                     input ped-item.val-pct-desconto-tab-preco  /*de-val-pct-desconto-tab-preco*/,
                     input ped-item.per-des-item  /*de-per-des-item*/).



        run eliminaWtFatSerLoteAutomatico in h-bodi317sd.

        FOR EACH tt-item-seq-estoq
            WHERE tt-item-seq-estoq.nr-sequencia = tt-item-seq.nr-sequencia:

            

            /* informar o deposito padrao para o deposito de pao avariado */
            run criaAlteraWtFatSerLote in h-bodi317sd (input  yes, /* Inclus�o do registro */
                                                       input  i-seq-wt-docto,
                                                       input  i-seq-wt-it-docto,
                                                       input  ped-item.it-codigo,
                                                       input  tt-item-seq-estoq.cod-depos,  
                                                       input  tt-item-seq-estoq.cod-localiz,
                                                       input  "", /* es-retorno-troca.serie, */
                                                       input  tt-item-seq-estoq.qt-pedida,
                                                       input  0,
                                                       input  ?, /*tt-it-nota-fisc.dt-vali-lote, Verificar data de validade do lote */
                                                       output l-proc-ok-aux).
        END.



    END.

    /* Limpar a tabela de erros em todas as BOS */
    run emptyRowErrors        in h-bodi317in.

    /* Atende todos os itens do pedido, com tela de acompanhamento */
    run inicializaAcompanhamento in h-bodi317pr.
    run atendeTotalPed           in h-bodi317pr (input  i-seq-wt-docto,
                                                 output l-proc-ok-aux).
    run finalizaAcompanhamento   in h-bodi317pr.

    /* Busca poss�veis erros que ocorreram nas valida��es */
    run devolveErrosbodi317pr    in h-bodi317pr (output c-ultimo-metodo-exec,
                                                 output table RowErrors).

    /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
    find first RowErrors no-lock no-error.
    
    if  avail RowErrors then
        for each RowErrors:
            /* <Mostra os Erros/Advert�ncias encontradas */

         {method/ShowMessage.i1}              
         {method/ShowMessage.i2 &Modal="yes"} 

        end.
    
    /* Caso ocorreu problema nas valida��es, n�o continua o processo */
    if  not l-proc-ok-aux then
        undo, leave.

    /* Limpar a tabela de erros em todas as BOS */
    run emptyRowErrors        in h-bodi317in.

    /* Calcula o pedido, com acompanhamento */
    run inicializaAcompanhamento in h-bodi317pr.
    run confirmaCalculo          in h-bodi317pr(input  i-seq-wt-docto,
                                                output l-proc-ok-aux).
    run finalizaAcompanhamento   in h-bodi317pr.

    /* Busca poss�veis erros que ocorreram nas valida��es */
    run devolveErrosbodi317pr    in h-bodi317pr (output c-ultimo-metodo-exec,
                                                 output table RowErrors).

    /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
    find first RowErrors no-lock no-error.
    if  avail RowErrors then
        for each RowErrors:
            /* <Mostra os Erros/Advert�ncias encontradas */

         {method/ShowMessage.i1}              
         {method/ShowMessage.i2 &Modal="yes"} 

        end.

    
    /* Caso ocorreu problema nas valida��es, n�o continua o processo */
    if  not l-proc-ok-aux then
        undo, leave.
        
    FIND FIRST wt-docto EXCLUSIVE-LOCK
         WHERE wt-docto.seq-wt-docto = i-seq-wt-docto NO-ERROR.
    IF AVAIL wt-docto THEN
    DO:
        ASSIGN wt-docto.nome-transp = c-nome-transp
               wt-docto.placa       = c-placa      
               wt-docto.uf-placa    = c-ufPlaca.   
    END.
    RELEASE wt-docto.
    /* Efetiva os pedidos e cria a nota */
    run dibo/bodi317ef.p persistent set h-bodi317ef.
    run emptyRowErrors           in h-bodi317in.
    run inicializaAcompanhamento in h-bodi317ef.
    run setaHandlesBOS           in h-bodi317ef(h-bodi317pr,     
                                                h-bodi317sd, 
                                                h-bodi317im1bra, 
                                                h-bodi317va).
    run efetivaNota              in h-bodi317ef(input  i-seq-wt-docto,
                                                input  yes,
                                                output l-proc-ok-aux).
    run finalizaAcompanhamento   in h-bodi317ef.

    /* Busca poss�veis erros que ocorreram nas valida��es */
    run devolveErrosbodi317ef    in h-bodi317ef(output c-ultimo-metodo-exec,
                                                output table RowErrors).

    /* Pesquisa algum erro ou advert�ncia que tenha ocorrido */
    find first RowErrors
         where RowErrors.ErrorSubType = "ERROR":U no-error.


    find first RowErrors no-lock no-error.
    
    if  avail RowErrors then
        for each RowErrors:
            /* <Mostra os Erros/Advert�ncias encontradas */

         {method/ShowMessage.i1}              
         {method/ShowMessage.i2 &Modal="yes"} 

        end.

    
    /* Caso ocorreu problema nas valida��es, n�o continua o processo */
    if  not l-proc-ok-aux then do:
        delete procedure h-bodi317ef.
        undo, leave.
    end.

    /* Busca as notas fiscais geradas */
    run buscaTTNotasGeradas in h-bodi317ef(output l-proc-ok-aux,
                                           output table tt-notas-geradas).

    /* Elimina o handle do programa bodi317ef */
    delete procedure h-bodi317ef.
    
    leave.
end.
        
/* Finaliza��o das BOS utilizada no c�lculo */
run finalizaBOS in h-bodi317in.

/* Mostrar as notas geradas */
for first tt-notas-geradas no-lock:
    find last b-tt-notas-geradas no-error.
    for  first nota-fiscal 
        where rowid(nota-fiscal) = tt-notas-geradas.rw-nota-fiscal EXCLUSIVE-LOCK:
    end.
    bell.
    
    if  tt-notas-geradas.nr-nota = b-tt-notas-geradas.nr-nota then
        run utp/ut-msgs.p(input "show",
                          input 15263,
                          input string(tt-notas-geradas.nr-nota) + "~~" +
                                string(nota-fiscal.cod-estabel)  + "~~" +
                                string(nota-fiscal.serie)).
    else
        run utp/ut-msgs.p(input "show",
                          input 15264,
                          input string(tt-notas-geradas.nr-nota)   + "~~" +
                                string(b-tt-notas-geradas.nr-nota) + "~~" +
                                string(nota-fiscal.cod-estabel)    + "~~" +
                                string(nota-fiscal.serie)).
     /*ASSIGN   p-nro-nota = STRING(tt-notas-geradas.nr-nota).
     ASSIGN   P-row-nota-fiscal  =  ROWID(nota-fiscal).
       */

    ASSIGN p-ok = TRUE
           p-nr-nota-fis = tt-notas-geradas.nr-nota.

    if tt-notas-geradas.nr-nota = b-tt-notas-geradas.nr-nota then
        ASSIGN l-delete = YES.
end.

IF l-delete = YES THEN
   FOR EACH tt-notas-geradas.
       DELETE tt-notas-geradas.
   END.

RETURN  "OK".

/* Fim do programa que calcula um pedido */
 
