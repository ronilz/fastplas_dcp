&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i V99XX999 9.99.99.999}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.
DEF NEW GLOBAL SHARED VAR h-esft4002 AS HANDLE NO-UNDO.

DEF NEW GLOBAL SHARED VAR h-v01 AS HANDLE NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES dc-embarque
&Scoped-define FIRST-EXTERNAL-TABLE dc-embarque


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR dc-embarque.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS rt-key fi-dt-emis-nota fi-nat-operacao ~
fi-cod-canal-venda fi-cod-estabel fi-serie fi-pto-controle fi-nome-transp ~
fi-motorista fi-placa fi-ufPlaca 
&Scoped-Define DISPLAYED-FIELDS dc-embarque.nr-embarque ~
dc-embarque.data-embarque dc-embarque.hora-embarque dc-embarque.nome-abrev ~
dc-embarque.nr-pedcli 
&Scoped-define DISPLAYED-TABLES dc-embarque
&Scoped-define FIRST-DISPLAYED-TABLE dc-embarque
&Scoped-Define DISPLAYED-OBJECTS fi-dt-emis-nota fi-nat-operacao ~
fi-cod-canal-venda fi-cod-estabel fi-serie fi-pto-controle fi-nome-transp ~
c-nome-transp fi-motorista fi-placa fi-ufPlaca 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE c-nome-transp AS CHARACTER FORMAT "X(40)":U 
     VIEW-AS FILL-IN 
     SIZE 53.29 BY .88 NO-UNDO.

DEFINE VARIABLE fi-cod-canal-venda AS INTEGER FORMAT ">>9":U INITIAL 0 
     LABEL "Canal Venda" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .88 NO-UNDO.

DEFINE VARIABLE fi-cod-estabel AS CHARACTER FORMAT "X(3)":U 
     LABEL "Estabelecimento" 
     VIEW-AS FILL-IN 
     SIZE 5 BY .88 NO-UNDO.

DEFINE VARIABLE fi-dt-emis-nota AS DATE FORMAT "99/99/9999":U 
     LABEL "Dt Emiss�o" 
     VIEW-AS FILL-IN 
     SIZE 14 BY .88 NO-UNDO.

DEFINE VARIABLE fi-motorista AS CHARACTER FORMAT "x(25)" 
     LABEL "Motorista" 
     VIEW-AS FILL-IN 
     SIZE 26.14 BY .88.

DEFINE VARIABLE fi-nat-operacao AS CHARACTER FORMAT "X(6)":U 
     LABEL "Natureza Opera��o" 
     VIEW-AS FILL-IN 
     SIZE 9 BY .88 NO-UNDO.

DEFINE VARIABLE fi-nome-transp AS CHARACTER FORMAT "x(12)" 
     LABEL "Transportador" 
     VIEW-AS FILL-IN 
     SIZE 16.14 BY .88.

DEFINE VARIABLE fi-placa AS CHARACTER FORMAT "x(10)" 
     LABEL "Placa" 
     VIEW-AS FILL-IN 
     SIZE 14.86 BY .88.

DEFINE VARIABLE fi-pto-controle AS CHARACTER FORMAT "X(5)":U 
     LABEL "Cod. Pto Controle Desmonta RACK Faturamento" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .88 NO-UNDO.

DEFINE VARIABLE fi-serie AS CHARACTER FORMAT "X(5)":U 
     LABEL "S�rie" 
     VIEW-AS FILL-IN 
     SIZE 7 BY .88 NO-UNDO.

DEFINE VARIABLE fi-ufPlaca AS CHARACTER FORMAT "X(2)":U 
     LABEL "UF" 
     VIEW-AS FILL-IN 
     SIZE 4.29 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88 BY 6.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     dc-embarque.nr-embarque AT ROW 1.25 COL 16 COLON-ALIGNED WIDGET-ID 4
          VIEW-AS FILL-IN 
          SIZE 8 BY .88
     dc-embarque.data-embarque AT ROW 1.25 COL 24 COLON-ALIGNED NO-LABEL WIDGET-ID 20
          VIEW-AS FILL-IN 
          SIZE 12.57 BY .88
     dc-embarque.hora-embarque AT ROW 1.25 COL 37 COLON-ALIGNED NO-LABEL WIDGET-ID 22
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     fi-dt-emis-nota AT ROW 1.25 COL 72 COLON-ALIGNED WIDGET-ID 12
     dc-embarque.nome-abrev AT ROW 2.25 COL 16 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 16.14 BY .88
     fi-nat-operacao AT ROW 2.25 COL 72 COLON-ALIGNED WIDGET-ID 14
     dc-embarque.nr-pedcli AT ROW 3.25 COL 16 COLON-ALIGNED WIDGET-ID 6
          VIEW-AS FILL-IN 
          SIZE 16.14 BY .88
     fi-cod-canal-venda AT ROW 3.25 COL 72 COLON-ALIGNED WIDGET-ID 8
     fi-cod-estabel AT ROW 4.25 COL 16 COLON-ALIGNED WIDGET-ID 10
     fi-serie AT ROW 4.25 COL 30 COLON-ALIGNED WIDGET-ID 18
     fi-pto-controle AT ROW 4.25 COL 72 COLON-ALIGNED WIDGET-ID 16
     fi-nome-transp AT ROW 5.25 COL 16 COLON-ALIGNED WIDGET-ID 28
     c-nome-transp AT ROW 5.25 COL 32.72 COLON-ALIGNED NO-LABEL WIDGET-ID 24
     fi-motorista AT ROW 6.25 COL 16 COLON-ALIGNED WIDGET-ID 26
     fi-placa AT ROW 6.25 COL 51 COLON-ALIGNED WIDGET-ID 30
     fi-ufPlaca AT ROW 6.25 COL 73 COLON-ALIGNED WIDGET-ID 32
     rt-key AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 1 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.dc-embarque
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 6.5
         WIDTH              = 88.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN c-nome-transp IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-embarque.data-embarque IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-embarque.hora-embarque IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-embarque.nome-abrev IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-embarque.nr-embarque IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-embarque.nr-pedcli IN FRAME f-main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME fi-nome-transp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-nome-transp V-table-Win
ON F5 OF fi-nome-transp IN FRAME f-main /* Transportador */
DO:
  assign l-implanta = yes.
  {include/zoomvar.i &prog-zoom="adzoom/z01ad268.w"
                     &campo=fi-nome-transp
                     &campozoom=nome-abrev
                     &campo2=c-nome-transp
                     &campozoom2=nome}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-nome-transp V-table-Win
ON LEAVE OF fi-nome-transp IN FRAME f-main /* Transportador */
DO:
 {include/leave.i &tabela=transporte
                  &atributo-ref=nome
                  &variavel-ref=c-nome-transp
                  &where="transporte.nome-abrev = input frame {&frame-name} fi-nome-transp"}     

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-nome-transp V-table-Win
ON MOUSE-SELECT-DBLCLICK OF fi-nome-transp IN FRAME f-main /* Transportador */
DO:
  apply 'F5' to self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-placa
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-placa V-table-Win
ON F5 OF fi-placa IN FRAME f-main /* Placa */
DO:
  assign l-implanta = yes.
  {include/zoomvar.i &prog-zoom="dizoom/z02di00812.w"
                     &campo=fi-placa
                     &campozoom=placa}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-placa V-table-Win
ON MOUSE-SELECT-DBLCLICK OF fi-placa IN FRAME f-main /* Placa */
DO:
  APPLY 'F5' TO SELF. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-pto-controle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-pto-controle V-table-Win
ON F5 OF fi-pto-controle IN FRAME f-main /* Cod. Pto Controle Desmonta RACK Faturamento */
DO:
  {include/zoomvar.i &prog-zoom=eszoom/z01es117.w
                       &campo=fi-pto-controle
                       &campozoom=cod-pto-controle}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-pto-controle V-table-Win
ON MOUSE-SELECT-DBLCLICK OF fi-pto-controle IN FRAME f-main /* Cod. Pto Controle Desmonta RACK Faturamento */
DO:
  APPLY 'F5':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-serie
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-serie V-table-Win
ON LEAVE OF fi-serie IN FRAME f-main /* S�rie */
DO:
    FIND ser-estab
        WHERE ser-estab.cod-estabel = fi-cod-estabel:SCREEN-VALUE IN FRAME {&FRAME-NAME}
          AND ser-estab.serie = fi-serie:SCREEN-VALUE IN FRAME {&FRAME-NAME}
        NO-LOCK NO-ERROR.
    IF AVAIL ser-estab THEN
        ASSIGN fi-dt-emis-nota:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(ser-estab.dt-ult-fat,"99/99/9999").

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */
  if fi-nome-transp:LOAD-MOUSE-POINTER ("image/lupa.cur") in frame {&frame-name} then.
  if fi-placa:load-mouse-pointer       ("image/lupa.cur") in frame {&frame-name} then.
  h-v01 = THIS-PROCEDURE.

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "dc-embarque"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "dc-embarque"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /* Ponha na pi-validate todas as valida��es */
    /* N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    /* Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

  FIND ped-venda
      WHERE ped-venda.nome-abrev    = dc-embarque.nome-abrev
        AND ped-venda.nr-pedcli     = dc-embarque.nr-pedcli
      NO-LOCK NO-ERROR.

  IF NOT AVAILABLE ped-venda THEN
      RETURN 'OK'.

  IF VALID-HANDLE(h-esft4002) THEN
      RUN pi-atualiza-browse IN h-esft4002 (INPUT dc-embarque.nr-embarque) .

  /* Code placed here will execute AFTER standard behavior.    */

  IF AVAIL ped-venda THEN
      ASSIGN  fi-cod-estabel:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ped-venda.cod-estabel
              fi-nat-operacao:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ped-venda.nat-operacao
              fi-cod-canal-venda:SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(ped-venda.cod-canal-venda).

  FIND FIRST ser-estab WHERE
           ser-estab.cod-estabel = fi-cod-estabel:SCREEN-VALUE IN FRAME {&FRAME-NAME} 
           NO-LOCK NO-ERROR.
  IF AVAIL ser-estab THEN DO:

      ASSIGN fi-serie:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ser-estab.serie
             fi-dt-emis-nota:SCREEN-VALUE IN FRAME {&FRAME-NAME} = string(ser-estab.dt-ult-fat).
  END.
  FIND FIRST dc-embarque-trans NO-LOCK
       WHERE dc-embarque-trans.nome-abrev = ped-venda.nome-abrev
       AND   dc-embarque-trans.nr-pedcli  = ped-venda.nr-pedcli NO-ERROR.
  IF AVAIL dc-embarque-trans THEN
  DO:
      ASSIGN fi-motorista  :screen-value in frame {&frame-name} = dc-embarque-trans.motorista      
             fi-ufPlaca    :screen-value in frame {&frame-name} = dc-embarque-trans.placa
             fi-nome-transp:screen-value in frame {&frame-name} = dc-embarque-trans.transportadora
             fi-placa      :screen-value in frame {&frame-name} = dc-embarque-trans.uf-placa .
  END.
      
      

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available V-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-le-campos V-table-Win 
PROCEDURE pi-le-campos :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

DEFINE OUTPUT PARAMETER i-nr-embarque       AS INTEGER NO-UNDO.
DEFINE OUTPUT PARAMETER c-nr-pedcli         AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-nome-abrev        AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-cod-estabel       AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-serie             AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-dt-emis-nota      AS DATE NO-UNDO.
DEFINE OUTPUT PARAMETER c-nat-operacao      AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-cod-canal-venda   AS INTE NO-UNDO.
DEFINE OUTPUT PARAMETER c-pto-controle      AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-nome-transp       AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-motorista         AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-placa             AS CHAR NO-UNDO.
DEFINE OUTPUT PARAMETER c-ufPlaca           AS CHAR NO-UNDO.

ASSIGN  i-nr-embarque     = INPUT FRAME {&FRAME-NAME} dc-embarque.nr-embarque
        c-nr-pedcli       = dc-embarque.nr-pedcli:SCREEN-VALUE IN FRAME {&FRAME-NAME}
        c-nome-abrev      = dc-embarque.nome-abrev:SCREEN-VALUE IN FRAME {&FRAME-NAME}
        c-cod-estabel     = fi-cod-estabel:SCREEN-VALUE IN FRAME {&FRAME-NAME}       
        c-serie           = fi-serie:SCREEN-VALUE IN FRAME {&FRAME-NAME}       
        c-dt-emis-nota    = date(fi-dt-emis-nota:SCREEN-VALUE IN FRAME {&FRAME-NAME})       
        c-nat-operacao    = fi-nat-operacao:SCREEN-VALUE IN FRAME {&FRAME-NAME}      
        c-cod-canal-venda = int(fi-cod-canal-venda:SCREEN-VALUE IN FRAME {&FRAME-NAME})
        c-pto-controle    = fi-pto-controle:SCREEN-VALUE IN FRAME {&FRAME-NAME}
        c-nome-transp     = INPUT FRAME {&FRAME-NAME} fi-nome-transp
        c-motorista       = INPUT FRAME {&FRAME-NAME} fi-motorista 
        c-placa           = INPUT FRAME {&FRAME-NAME} fi-placa 
        c-ufPlaca         = INPUT FRAME {&FRAME-NAME} fi-ufPlaca
    .      

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-valida-transp V-table-Win 
PROCEDURE pi-valida-transp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE i-cont AS INTEGER     NO-UNDO.

FIND FIRST transporte  NO-LOCK
     WHERE transporte.nome-abrev = input frame {&frame-name} fi-nome-transp NO-ERROR.

IF NOT AVAIL transporte THEN
DO:
    RUN utp/ut-msgs.p(INPUT 'show',
                      INPUT 17006,
                      INPUT 'N�o foi Transportador informada.~~N�o foi encontrada ocorr�ncia em Transportador com a chave informada.  ').
    APPLY "entry":U TO fi-nome-transp IN FRAME {&FRAME-NAME}.
    return 'ADM-ERROR'.

END.
do  i-cont = 1 to length(trim(fi-placa:SCREEN-VALUE IN FRAME {&FRAME-NAME})):
    if  lookup(substring(fi-placa:SCREEN-VALUE IN FRAME {&FRAME-NAME},i-cont,1), "?") > 0 then do:        
        RUN utp/ut-msgs.p("show":U,4871, substring(fi-placa:SCREEN-VALUE IN FRAME {&FRAME-NAME},i-cont,1) ).        
        APPLY "entry":U TO fi-placa IN FRAME {&FRAME-NAME}.
        return 'ADM-ERROR'.
    end.
END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "dc-embarque"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

