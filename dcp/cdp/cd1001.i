/*****************************************************************************
**
**  CDP/CD1001.I - Include para defini��o das temp-tables
**                 tt_xml_input_1 e tt_log_erros.
**
*****************************************************************************/

/*--- temp-tables ---*/
def temp-table tt_xml_input_1 no-undo 
    field tt_cod_label      as char    format "x(20)" 
    field tt_des_conteudo   as char    format "x(40)" 
    field tt_num_seq_1_xml  as integer format ">>9"
    field tt_num_seq_2_xml  as integer format ">>9".

def temp-table tt_log_erros no-undo
    field ttv_num_seq                      as integer format ">>>,>>9" label "Seq��ncia" column-label "Seq"
    field ttv_num_cod_erro                 as integer format ">>>>,>>9" label "N�mero" column-label "N�mero"
    field ttv_des_erro                     as character format "x(50)" label "Inconsist�ncia" column-label "Inconsist�ncia"
    field ttv_des_ajuda                    as character format "x(50)" label "Ajuda" column-label "Ajuda".

/*--- vari�veis ---*/
def var v_sequencia as integer no-undo.
def var i-log-exec  as integer initial 1 no-undo.
