/********************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/

/******************************************************************************************************
**
** CD9970 - Consiste Conta Cont bil (Estabelecimento x Empresa)
**
** 02/01/2002 - Ivomar - duplicado para melhorar a performance evitando acessos ao param-global e estabelec
**            - recebe tabela.cod-estabel
*****************************************************************************************************/

if  param-global.ind-cons-conta = 2 then do:
    
    if not avail estabelec then
        find estabelec where
             estabelec.cod-estabel = {1} no-lock no-error.

    else 
        if estabelec.cod-estabel <> {1} then
            find estabelec where
                 estabelec.cod-estabel = {1} no-lock no-error.

    assign i-empresa = estabelec.ep-codigo when avail estabelec.
end.
else
    assign i-empresa = param-global.empresa-prin.
