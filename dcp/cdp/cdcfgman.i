/*** Esta include no ems 2.01 n�o dever� possuir nenhum c�digo, serve 
     apenas para desenvolvimento tratar o conceito de miniflexibiliza��o.
     Utilizado apenas para MANUFATURA. ***/

/*** RELEASE 2.02 ***/
&glob bf_man_sfc_lc          yes /* Fun��es ch�o f�brica e lista de componentes 2.02 - Logoplaste - Manufatura */

/*** RELEASE 2.03 ***/
&glob bf_man_sfc_magnus      yes /* Integra��o Magnus x SFC 2.03 - Klimmek - Manufatura */
&glob bf_man_linha_estab     yes /* Relacionamento Linha Produ��o x Estabelecimento     */
&glob bf_man_trans_assinc    yes /* Transa��o Reporte Ass�ncrono                        */
&glob bf_man_203             yes /* Altera��es Gerais EMS 2.03                          */

/*** RELEASE 2.04 ***/
&glob bf_man_204             yes /* Altera��es Gerais EMS 2.04                          */

/*** RELEASE 2.04A ***/
&glob bf_man_custeio_item    yes /* Altera��es p/ Foresight                             */

/*** RELEASE 2.05 ***/
&glob bf_man_sfc_rep_ascii   yes /* Importa��o de Transa��es via ASCII                  */
&glob bf_man_sfc_visoes_ger  yes /* 3 Novas Vis�es Gerenciais no SFC                    */
&glob bf_man_preco_config    yes /* Pre�o de Venda de Itens Configurados                */
&glob bf_man_ponto_repos     yes /* Ponto de Reposi��o no MRP                           */
&glob bf_man_mrp_override    yes /* Override de Ordens no MRP                           */
&glob bf_man_integra_aps     yes /* Integra��o do ERP com APS                           */
&glob bf_man_per_ppm         yes /* Tratamento do Fator de Concentra��o                 */
&glob bf_man_205             yes /* Altera��es Gerais EMS 2.05                          */
&glob bf_man_sfc_ref_oper    yes /* Tratamento do Refugo por Opera��o (no SFC)          */

/*** RELEASE 2.06 ***/
&glob bf_man_206             yes /* Altera��es Gerais EMS 2.06                          */
&glob bf_man_dbr             yes /* Altera��es DBR                                      */

/*** RELEASE 2.06B ***/
&glob bf_man_206b            yes /* Altera��es Gerais EMS 2.06B                         */

/*** RELEASE 2.07 ***/
&glob bf_man_207             yes /* Altera��es Gerais EMS 2.07                         */

/*** RELEASE 2.07A ***/
&glob bf_man_207a            yes /* Altera��es Gerais EMS 2.07A   */

/*** RELEASE 2.08 ***/
&glob bf_man_versao_ems              2.08 /* Utilizado para Teste de Release*/
&glob bf_lote_avancado_liberado      yes  /* Funcionalidade de Lote Avan�ado */


