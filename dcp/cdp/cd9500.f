/****************************************************************************
**
**    CD9500.F - Form do Cabecalho Padrao e Rodape
**
****************************************************************************/

form header
    fill("-", 132) format "x(132)" skip
    c-empresa c-titulo-relat at 50
    "Folha:" at 122 page-number  at 128 format ">>>>9" skip
    fill("-", 112) format "x(110)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabec.

form header
    fill("-", 132) format "x(132)" skip
    c-empresa c-titulo-relat at 50
    "Folha:" at 122 page-number  at 128 format ">>>>9" skip
    "Periodo:" i-numper-x at 10 "-"
    da-iniper-x at 15 "a" da-fimper-x
    fill("-", 74) format "x(72)" today format "99/99/9999"
    "-" string(time, "HH:MM:SS") skip(1)
    with stream-io width 132 no-labels no-box page-top frame f-cabper.

c-rodape = "DATASUL - " + c-sistema + " - " + c-programa + " - V:" + c-versao
         + "." + c-revisao.
c-rodape = fill("-", 132 - length(c-rodape)) + c-rodape.

form header
    c-rodape format "x(132)"
    with stream-io width 132 no-labels no-box page-bottom frame f-rodape.

/* CD9500.F */
