
/******************************************************************************
    Objetivo: include                             
    Programa: ttdf1.i        
    Data    : 08/03/2007
    Autor   : 
    Altercao: 
    Vers�o  : V.2.04.00.001
*******************************************************************************/

/*
DEF TEMP-TABLE tt-pto-controle NO-UNDO
    FIELD cod-pto-controle      AS CHAR FORMAT "x(5)"
    FIELD desc-pto-controle     AS CHAR FORMAT "x(50)"
    FIELD cod-depos-dest        AS CHAR FORMAT "x(3)"
    FIELD cod-depos-orig        AS CHAR FORMAT "x(3)"
    FIELD cod-localiz-dest      AS CHAR FORMAT "x(10)"
    FIELD cod-localiz-orig      AS CHAR FORMAT "x(10)".
*/
                                                       
DEF TEMP-TABLE tt-deposito     NO-UNDO
    FIELD cod-depos             AS CHAR FORMAT "x(3)"
    FIELD nome                  AS CHAR FORMAT "x(40)".

DEF TEMP-TABLE tt-codigo  NO-UNDO     
    FIELD codigo                AS CHAR FORMAT "x(16)"
    FIELD descricao             AS CHAR FORMAT "x(40)"
    FIELD localiz               AS CHAR FORMAT 'x(10)'
    .

DEF TEMP-TABLE tt-localizacao  NO-UNDO
    FIELD cod-localiz           AS CHAR FORMAT "x(10)"
    FIELD descricao             AS CHAR FORMAT "x(30)".

def temp-table tt-rg-item NO-UNDO
    FIELD codigo              AS CHAR FORMAT "X(16)".

DEF TEMP-TABLE tt-erro-api NO-UNDO
    FIELD tipo     AS INT  /* 0 - sucesso, 1 - erro, 2 - advertencia */
    FIELD mensagem AS CHAR.


DEF TEMP-TABLE tt-item 
    FIELD it-codigo AS CHAR FORMAT "x(16)"
    FIELD rg-item   AS CHAR FORMAT "X(13)"
    FIELD nr-rack   AS CHAR FORMAT "RC99999999999"
    FIELD nr-rack-a AS CHAR FORMAT "RC99999999999".

define temp-table tt-erros
    field cod-erro  as integer
    field desc-erro as character format "x(256)"
    field desc-arq  as character.
