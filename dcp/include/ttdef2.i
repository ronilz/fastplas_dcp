
/******************************************************************************
    Objetivo: include                             
    Programa: ttdf1.i        
    Data    : 08/03/2007
    Autor   : 
    Altercao: Adicionado NO-UNDO nas defini��es de temp-tables - Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    Vers�o  : V.2.04.00.001
*******************************************************************************/

DEF STREAM strlog.
DEF NEW GLOBAL SHARED VAR cLogFileName AS CHAR. 
DEF NEW GLOBAL SHARED VAR lGerarLog AS LOGICAL INIT TRUE.
DEF NEW GLOBAL SHARED VAR c-arquivo-log AS CHAR FORMAT "x(60)" NO-UNDO.

DEF TEMP-TABLE tt-requis NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD cod-usuario      AS CHAR
    FIELD cod-senha        AS CHAR
    FIELD ccusto           AS CHAR
    FIELD cod-pto-controle AS CHAR 
    FIELD cod-depos-dest   AS CHAR
    FIELD cod-localiz-dest AS CHAR.

DEF TEMP-TABLE tt-requis-lista NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD nro-docto        AS INT
    FIELD cod-pto-controle AS CHAR 
    FIELD ccusto           AS CHAR 
    FIELD rg-item          AS CHAR.

Def temp-table tt-reporte NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    Field cod-pto-controle   AS CHAR
    Field cod-depos-dest     AS CHAR
    Field cod-localiz-dest   AS CHAR
    FIELD cod-depos-orig     AS CHAR
    FIELD cod-localiz-orig   AS CHAR
    FIELD cCodLocaliz        AS CHAR
    Field rg-item            AS CHAR
    Field it-codigo          AS CHAR 
    Field desc-item          AS CHAR
    Field desc-pto-controle  AS CHAR
    Field nro-docto          AS INT
    Field nr-ord-produ       AS INT 
    Field local-pto          AS CHAR
    Field tipo-pto           AS INT
    FIELD cod-usuario        AS CHAR
    FIELD cod-senha          AS CHAR
    FIELD nr-ord-prod-est    AS INTEGER.

Def temp-table tt-reporte-item NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    Field rg-item            AS CHAR
    Field rg-item-juncao     AS CHAR
    FIELD rg-item-pai        AS CHAR
    Field cod-cor            AS CHAR
    FIELD cCodLocaliz        AS CHAR.

DEF TEMP-TABLE tt-cor NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD cod-cor   AS CHAR 
    FIELD desc-cor  AS CHAR 
    FIELD ind-ativo AS LOG
    FIELD it-codigo AS CHAR.

DEF TEMP-TABLE tt-pintura NO-UNDO
    FIELD cod-usuario      AS CHAR
    FIELD cod-pto-controle AS CHAR
    FIELD cod-cor          AS CHAR
    FIELD cod-usuar-auth   AS CHAR
    FIELD cod-senha-auth   AS CHAR
    FIELD l-auth           AS LOGICAL.

DEF TEMP-TABLE tt-monta-rack NO-UNDO
    FIELD cod-usuario      AS CHAR
    FIELD cod-pto-controle AS CHAR
    FIELD nr-rack          AS CHAR
    FIELD rg-item          AS CHAR
    FIELD desc-rg-item     AS CHARACTER
    FIELD cod-usuar-auth   AS CHAR
    FIELD cod-senha-auth   AS CHAR
    FIELD l-auth           AS LOGICAL.


Def temp-table tt-consulta NO-UNDO
    FIELD rg-item          AS CHAR 
    FIELD it-codigo        AS CHAR
    FIELD desc-item        AS CHAR
    FIELD nr-rack          AS CHAR
    FIELD cod-pto-controle AS CHAR
    FIELD local-pto        AS CHAR
    FIELD desc-local-pto   AS CHAR
    FIELD tipo-pto         AS INT
    FIELD ind-situacao     AS INT.

DEF temp-table tt-lista-movto NO-UNDO
    FIELD nr-seq           AS INT
    FIELD rg-item          AS CHAR
    FIELD it-codigo        AS CHAR
    FIELD desc-it-codigo   AS CHAR
    FIELD nr-rack          AS CHAR
    FIELD cod-pto-controle AS CHAR
    FIELD local-pto        AS CHAR
    FIELD tipo-pto         AS INT.

DEF temp-table tt-det-movto NO-UNDO
    FIELD nr-seq           AS INT
    FIELD rg-item          AS CHAR
    FIELD it-codigo        AS CHAR
    FIELD desc-it-codigo   AS CHAR
    FIELD nr-rack          AS CHAR
    FIELD cod-pto-controle AS CHAR
    FIELD local-pto        AS CHAR
    FIELD desc-local-pto   AS CHAR
    FIELD tipo-pto         AS INT
    FIELD cod-depos-orig   AS CHAR
    FIELD cod-localiz-orig AS CHAR
    FIELD cod-depos-dest   AS CHAR
    FIELD cod-localiz-dest AS CHAR.

DEFINE TEMP-TABLE tt-menu NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD cod-usuario  AS CHAR
    FIELD cod-programa AS CHAR
    FIELD descricao    AS CHAR
    FIELD ind-menu     AS LOGICAL.

DEFINE TEMP-TABLE tt-det-requis NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD nro-docto        AS CHAR
    FIELD nr-requisicao    AS INT
    FIELD rg-item          AS CHAR
    FIELD it-codigo        AS CHAR
    FIELD desc-item        AS CHAR
    FIELD cod-depos-dest   AS CHAR
    FIELD cod-localiz-dest AS CHAR
    FIELD cod-depos-orig   AS CHAR
    FIELD cod-localiz-orig AS CHAR
    FIELD cod-usuario      AS CHAR
    FIELD cod-senha        AS CHAR
    FIELD cod-pto-controle AS CHAR.

DEFINE TEMP-TABLE tt-busca-requis NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD char-1            AS CHAR 
    FIELD char-2            AS CHAR
    FIELD check-sum         AS CHAR
    FIELD cod-estabel       AS CHAR 
    FIELD ct-codigo         AS CHAR 
    FIELD data-1            AS DATE
    FIELD data-2            AS DATE
    FIELD dec-1             AS DEC
    FIELD dec-2             AS DEC
    FIELD dt-atend          AS DATE
    FIELD dt-devol          AS DATE
    FIELD dt-requisicao     AS DATE
    FIELD estado            AS INT
    FIELD impressa          AS INT
    FIELD int-1             AS INT
    FIELD int-2             AS INT 
    FIELD loc-entrega       AS CHAR
    FIELD log-1             AS LOG
    FIELD log-2             AS LOG
    FIELD narrativa         AS CHAR
    FIELD nome-abrev        AS CHAR
    FIELD nome-aprov        AS CHAR
    FIELD num-id-documento  AS INT
    FIELD sc-codigo         AS CHAR 
    FIELD situacao          AS INT
    FIELD tp-requis         AS INT.

DEF TEMP-TABLE tt-pto-controle NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021 
    LIKE dc-pto-controle 
    FIELD desc-tipo-pto AS CHAR

    INDEX idx001 IS PRIMARY
    cod-pto-controle.

/*
DEF TEMP-TABLE tt-pto-controle
    FIELD cod-pto-controle AS CHAR
    FIELD local-pto AS CHAR
    FIELD tipo-pto AS INTEGER
    FIELD desc-tipo-pto AS CHAR
    FIELD desc-pto-controle AS CHAR
    FIELD cod-depos-orig AS CHAR
    FIELD cod-localiz-orig AS CHAR
    FIELD cod-depos-dest AS CHAR
    FIELD cod-localiz-dest AS CHAR.
*/    

DEF TEMP-TABLE tt-rack NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD nr-rack AS CHAR
    FIELD desc-rack AS CHAR.

DEF TEMP-TABLE tt-rack-itens NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD nr-rack AS CHAR
    FIELD rg-item AS CHAR
    FIELD desc-rg-item AS CHAR
    FIELD it-codigo AS CHAR
    FIELD desc-item AS CHAR.

DEF TEMP-TABLE tt-pedido NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD cod-usuario      AS CHAR
    FIELD cod-senha        AS CHAR
    FIELD cod-usuar-auth   AS CHAR
    FIELD cod-senha-auth   AS CHAR
    FIELD nome-abrev       AS CHAR
    FIELD nr-pedcli        AS CHAR
    FIELD nome-emit        AS CHAR
    FIELD rg-item          AS CHAR
    FIELD descricao        AS CHAR
    FIELD cod-pto-controle AS CHAR.
    
DEFINE TEMP-TABLE tt-ctx-pendencia NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD it-codigo as CHAR
    FIELD desc-item as char
    FIELD localiz   AS CHAR
    FIELD r-rowid as ROWID
    FIELD rg-item as CHAR.
    
DEFINE TEMP-TABLE tt-lista-pint NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    FIELD it-codigo as CHAR
    FIELD it-codigo-cor as CHAR    FIELD rg-item as CHAR.    


DEFINE TEMP-TABLE ttDcEmbarque NO-UNDO LIKE dc-embarque
    FIELD cNrEmbarque AS CHARACTER
    FIELD cDataEmbarque AS CHARACTER.

Def temp-table tt-reporte-item2 NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    Field it-codigo          AS CHAR 
    Field desc-item          AS CHAR
    Field nr-ord-produ       AS INT
    Field cod-depos-dest     AS CHAR
    Field cod-localiz-dest   AS CHAR
    FIELD cod-depos-orig     AS CHAR
    FIELD cod-localiz-orig   AS CHAR
    Field cod-pto-controle   AS CHAR
    .

Def temp-table tt-reporte2 NO-UNDO // Lucio Zamarioli - TOTVS Ibirapuera 01/05/2021
    Field cod-pto-controle   AS CHAR
    Field cod-depos-dest     AS CHAR
    Field cod-localiz-dest   AS CHAR
    FIELD cod-depos-orig     AS CHAR
    FIELD cod-localiz-orig   AS CHAR
    Field it-codigo          AS CHAR 
    Field desc-item          AS CHAR
    Field desc-pto-controle  AS CHAR
    Field nr-ord-produ       AS INT 
    FIELD cod-usuario        AS CHAR
    FIELD cod-senha          AS CHAR
    FIELD quantidade         AS DEC
    FIELD qtde-refugada      AS DEC
    FIELD rg-rack            AS CHAR.

DEF TEMP-TABLE tt-inventario NO-UNDO
    FIELD nr-ficha      LIKE inventario.nr-ficha
    FIELD dt-saldo      LIKE inventario.dt-saldo
    FIELD it-codigo     LIKE inventario.it-codigo
    FIELD desc-item     AS CHAR
    FIELD un            AS CHAR
    FIELD cod-depos     LIKE inventario.cod-depos
    FIELD cod-localiz   LIKE inventario.cod-localiz
    FIELD nr-contagem   AS INT
    FIELD contagem      AS CHAR.
