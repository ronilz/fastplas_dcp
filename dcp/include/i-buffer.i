/*buffers*/

assign current-language = current-language no-error.

/*
DEF BUFFER prog_dtsul             FOR mguni.prog_dtsul.
def buffer transporte             for mguni.transporte.
def buffer empresa                for mguni.empresa.
def buffer usuar_mestre           for mguni.usuar_mestre.
def buffer procedimento           for mguni.procedimento.
def buffer modul_dtsul            for mguni.modul_dtsul.
def buffer layout_impres_padr     for mguni.layout_impres_padr.
def buffer imprsor_usuar          for mguni.imprsor_usuar.
def buffer layout_impres          for mguni.layout_impres.
def buffer param-global           for mguni.param-global. 
def buffer impressora             for mguni.impressora.
def buffer tip_imprsor            for mguni.tip_imprsor.
def buffer servid_exec_imprsor    for mguni.servid_exec_imprsor.
def buffer configur_layout_impres for mguni.configur_layout_impres.
def buffer configur_tip_imprsor   for mguni.configur_tip_imprsor.
DEF BUFFER ped_exec               FOR mguni.ped_exec.
DEF BUFFER tab_dic_dtsu           FOR mguni.tab_dic_dtsu.
DEF BUFFER tab_dic_dtsul          FOR mguni.tab_dic_dtsul.
DEF BUFFER localizacao            FOR mguni.localizacao.
DEF BUFFER ccusto                 FOR mguni.ccusto.
DEF BUFFER usuar_grp_usuar        FOR mguni.usuar_grp_usuar.
def buffer ITEM                   for mguni.ITEM.
def buffer estabelec              for mguni.estabelec.
def buffer repres                 for mguni.repres.
def buffer natur-oper             for mguni.natur-oper.  
def buffer nota-fiscal            for mguni.nota-fiscal.
def buffer emitente               for mguni.emitente.
def buffer item-cli               for mguni.item-cli.
*/



