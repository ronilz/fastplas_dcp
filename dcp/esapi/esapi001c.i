&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
/* Procedure Description
"Structured Include File Template.

Use this template to create a new Structured Include file to include PROGRESS 4GL code into another PROGRESS source file. You edit structured include files using the AB's Section Editor."
*/
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*------------------------------------------------------------------------
    File        : esapi001c.i
    Purpose     : Funcao que tem objetivo de tratar erros em fun��o de assign ou run.
Tratar erros ocorridos de lock tamb�m.

    Syntax      :

    Description : Tratamento de Erro	

    Author(s)   : ronil
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .i file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include Template
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
 



&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ***************************  Main Block  *************************** */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */


&IF DEFINED(EXCLUDE-verificarErro) = 0 &THEN

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE verificarErro Procedure
PROCEDURE verificarErro :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    /* vars */
    DEFINE VARIABLE i-cont AS INTEGER    NO-UNDO.
    DEFINE VARIABLE l-ok AS CHAR     NO-UNDO.
    
    /* param */
    
    /* main */
    ASSIGN l-ok = "OK".
    do i-cont = 1 to ERROR-STATUS:NUM-MESSAGES.
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo = 1
               tt-erro-api.mensagem = STRING(ERROR-STATUS:GET-NUMBER(i-cont)) + " - " + ERROR-STATUS:GET-MESSAGE(i-cont)
               l-ok = "NOK".
    END.
    
    RETURN l-ok.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ENDIF



