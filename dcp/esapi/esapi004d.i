/******************************************************************************
    Objetivo: Metodos                              
    Programa: ESAPI004d.i         
    Data    : 07/09/2011
    Autor   : 
    Altercao: 
    Vers�o  : V.2.04.00.001
    
    Documentacao Metodo:
    -> pi-marca-rg            - marcar rg em processo 
    -> pi-ver-sit-rg          - verificar situacao do rg
    -> pi-desmarca-rg         - liberar rg para processo
    
*******************************************************************************/

PROCEDURE pi-marca-rg.
    /* vars */
    DEFINE VARIABLE l-erro AS LOGICAL     NO-UNDO.

    /* main */
    IF AVAILABLE tt-reporte THEN
        FIND dc-pto-controle
            WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle
            NO-LOCK NO-ERROR.

    ASSIGN l-erro = FALSE.
    bloco:
    DO TRANSACTION ON ERROR UNDO bloco, NEXT.
        FOR EACH tt-reporte-item NO-LOCK.
            FIND FIRST ctx-reporte-proc
                WHERE ctx-reporte-proc.rg-item = tt-reporte-item.rg-item
                NO-LOCK NO-ERROR.
            IF AVAILABLE ctx-reporte-proc THEN DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo = 1
                       tt-erro-api.mensagem = "RG / RACK " + tt-reporte-item.rg-item + " ainda em processo."
                       l-erro = TRUE.

                UNDO bloco, LEAVE bloco.
            END.
            ELSE DO:
                CREATE ctx-reporte-proc.
                ASSIGN ctx-reporte-proc.rg-item = tt-reporte-item.rg-item
                       ctx-reporte-proc.cod-usuario = (IF AVAILABLE tt-reporte THEN tt-reporte.cod-usuario ELSE '')
                       ctx-reporte-proc.dt-trans = TODAY
                       ctx-reporte-proc.hr-trans = STRING(TIME,"HH:MM:SS").
            END.

            IF AVAILABLE dc-pto-controle AND
                dc-pto-controle.tipo-pto = 2 THEN DO:

                FIND FIRST ctx-reporte-proc
                    WHERE ctx-reporte-proc.rg-item = tt-reporte-item.rg-item-juncao
                    NO-LOCK NO-ERROR.
                IF AVAILABLE ctx-reporte-proc THEN DO:
                    CREATE tt-erro-api.
                    ASSIGN tt-erro-api.tipo = 1
                           tt-erro-api.mensagem = "RG / RACK " + tt-reporte-item.rg-item-juncao + " ainda em processo."
                           l-erro = TRUE.
    
                    UNDO bloco, LEAVE bloco.
                END.
                ELSE DO:
                    CREATE ctx-reporte-proc.
                    ASSIGN ctx-reporte-proc.rg-item = tt-reporte-item.rg-item-juncao
                           ctx-reporte-proc.cod-usuario = (IF AVAILABLE tt-reporte THEN tt-reporte.cod-usuario ELSE '')
                           ctx-reporte-proc.dt-trans = TODAY
                           ctx-reporte-proc.hr-trans = STRING(TIME,"HH:MM:SS").
                END.
            END.
        END.
    END.

    IF l-erro THEN
        RETURN "NOK":U.
    ELSE
        RETURN "OK":U.

END PROCEDURE.

PROCEDURE pi-ver-sit-rg.
    /* vars */

    /* main */
    IF AVAILABLE tt-reporte THEN
        FIND dc-pto-controle
            WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle
            NO-LOCK NO-ERROR.

    FOR EACH tt-reporte-item NO-LOCK.

        FIND FIRST ctx-reporte-proc
            WHERE ctx-reporte-proc.rg-item = tt-reporte-item.rg-item
            NO-LOCK NO-ERROR.
        IF AVAILABLE ctx-reporte-proc THEN DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo = 1
                   tt-erro-api.mensagem = "RG / RACK " + tt-reporte-item.rg-item + " em processo reporte/requisicao.".
            RETURN "NOK":U.
        END.

        IF AVAILABLE dc-pto-controle AND
           dc-pto-controle.tipo-pto = 2 THEN DO:

            FIND FIRST ctx-reporte-proc
                WHERE ctx-reporte-proc.rg-item = tt-reporte-item.rg-item-juncao
                NO-LOCK NO-ERROR.
            IF AVAILABLE ctx-reporte-proc THEN DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo = 1
                       tt-erro-api.mensagem = "RG / RACK " + tt-reporte-item.rg-item-juncao + " em processo reporte/requisicao.".
                RETURN "NOK":U.
            END.
        END.
    END.

    RETURN "OK":U.

END PROCEDURE.

PROCEDURE pi-desmarca-rg.
    /* vars */
    DEFINE VARIABLE l-erro AS LOGICAL     NO-UNDO.

    /* main */
    IF AVAILABLE tt-reporte THEN
        FIND dc-pto-controle
            WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle
            NO-LOCK NO-ERROR.

    ASSIGN l-erro = FALSE.
    bloco:
    DO TRANSACTION ON ERROR UNDO bloco, NEXT.
        FOR EACH tt-reporte-item NO-LOCK.
            FIND FIRST ctx-reporte-proc
                WHERE ctx-reporte-proc.rg-item = tt-reporte-item.rg-item
                NO-ERROR NO-WAIT.
    
            IF LOCKED(ctx-reporte-proc) THEN DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo = 1
                       tt-erro-api.mensagem = "RG / RACK " + tt-reporte-item.rg-item + " em processo de reporte.".
                l-erro = TRUE.

                UNDO bloco, LEAVE bloco.
            END.
            ELSE IF AVAILABLE ctx-reporte-proc THEN
                DELETE ctx-reporte-proc.

            IF AVAILABLE dc-pto-controle AND
                dc-pto-controle.tipo-pto = 2 THEN DO:
                FIND FIRST ctx-reporte-proc
                    WHERE ctx-reporte-proc.rg-item = tt-reporte-item.rg-item-juncao
                    NO-ERROR NO-WAIT.
        
                IF LOCKED(ctx-reporte-proc) THEN DO:
                    CREATE tt-erro-api.
                    ASSIGN tt-erro-api.tipo = 1
                           tt-erro-api.mensagem = "RG / RACK " + tt-reporte-item.rg-item-juncao + " em processo de reporte.".
                    l-erro = TRUE.
    
                    UNDO bloco, LEAVE bloco.
                END.
                ELSE IF AVAILABLE ctx-reporte-proc THEN
                    DELETE ctx-reporte-proc.
            END.
        END.
    END.

    IF l-erro THEN
        RETURN "NOK":U.
    ELSE
        RETURN "OK":U.

END PROCEDURE.
