
/*------------------------------------------------------------------------
    File        : esapi018.p
    Purpose     : 

    Syntax      :

    Description : Montagem, apontamento cor e reporte de producao

    Author(s)   : TIB
    Created     : Mon Mar 06 11:54:27 BRT 2023
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{esapi/esapi001.p}



/* ********************  Preprocessor Definitions  ******************** */

/* ************************  Function Prototypes ********************** */


FUNCTION fn-getErro RETURNS CHARACTER 
    (  ) FORWARD.


/* ***************************  Main Block  *************************** */



/* **********************  Internal Procedures  *********************** */

PROCEDURE pi-valida:
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    // vars
    DEFINE VARIABLE h-esapi009  AS HANDLE    NO-UNDO.
    DEFINE VARIABLE c-desc-cor  AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-desc-erro AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont      AS INTEGER   NO-UNDO.
    
    // param
    DEFINE INPUT-OUTPUT PARAMETER TABLE FOR tt-reporte. 
    DEFINE INPUT-OUTPUT PARAMETER TABLE FOR tt-reporte-item.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.
    
    // main
    FOR FIRST tt-reporte. 
    END.
    FOR FIRST tt-reporte-item. 
    END.
    
    // 1a - validar cor
    RUN esapi/esapi009.p PERSISTENT SET h-esapi009.
    RUN pi-valida-cor IN h-esapi009 (
        INPUT tt-reporte-item.cod-cor,
        OUTPUT c-desc-cor,
        OUTPUT TABLE tt-erro-api).
    DELETE PROCEDURE h-esapi009.
    
    IF CAN-FIND (FIRST tt-erro-api) THEN 
        UNDO, THROW NEW Progress.Lang.AppError ("Erro ao obter cor", 17006).
        
    // 2a. - validar reporte
    // nao da para usar o validador do reporte
    // pois como � pintura, precisa ter apontamento
    // e o apontamento da pintura deve ocorrer so no final
    // ===> pintura somente no final, senao correo risco de fiscar cancelando e apontantando novamente
    //      gerando distorc�es na apuracao de retrabalho
    
    FIND FIRST dc-param NO-LOCK NO-ERROR.
    FIND FIRST tt-reporte NO-LOCK NO-ERROR.
    FIND FIRST tt-reporte-item NO-LOCK NO-ERROR.

    FIND dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle 
        NO-ERROR.
    IF NOT AVAILABLE dc-pto-controle THEN
        UNDO, THROW NEW Progress.Lang.AppError ("Ponto de Controle " + tt-reporte.cod-pto-controle + " nao cadastrado!", 17006).
        
    IF dc-pto-controle.tipo-pto <> 3 THEN
        UNDO, THROW NEW Progress.Lang.AppError ("Tipo de Ponto de Controle deve ser Pintura (tipo = 3)", 17006).

    FIND mgind.localizacao NO-LOCK
        WHERE localizacao.cod-estabel = dc-param.cod-estabel
        AND localizacao.cod-depos   = tt-reporte.cod-depos-orig
        AND localizacao.cod-localiz = tt-reporte.cod-localiz-orig NO-ERROR.
    IF NOT AVAILABLE localizacao THEN
        UNDO, THROW NEW Progress.Lang.AppError ("Localizacao Origem " + tt-reporte.cod-localiz-dest + " nao cadastrada!", 17006).

    FIND deposito NO-LOCK
        WHERE deposito.cod-depos = tt-reporte.cod-depos-dest NO-ERROR.
    IF NOT AVAILABLE deposito THEN
        UNDO, THROW NEW Progress.Lang.AppError ("Deposito Destino " + tt-reporte.cod-depos-dest + " nao cadastrada!", 17006).

    FIND deposito NO-LOCK
        WHERE deposito.cod-depos = tt-reporte.cod-depos-orig NO-ERROR.
    IF NOT AVAILABLE deposito THEN
        UNDO, THROW NEW Progress.Lang.AppError ("Deposito Origem " + tt-reporte.cod-depos-dest + " nao cadastrada!", 17006).
        
    // validar rg item agora
    FOR FIRST dc-rg-item NO-LOCK
        WHERE dc-rg-item.rg-item = tt-reporte-item.rg-item.
    END.
        
    IF NOT AVAILABLE dc-rg-item THEN
        UNDO, THROW NEW Progress.Lang.AppError ("Rg Item " + tt-reporte-item.rg-item + " nao cadastrado!", 17006).
    
    IF dc-rg-item.situacao <> 2 THEN /*Ativo*/
        UNDO, THROW NEW Progress.Lang.AppError ("Rg Item " + dc-rg-item.rg-item + " nao tem situacao ATIVA!", 17006).

    IF dc-rg-item.cod-depos-orig <> tt-reporte.cod-depos-orig THEN
        UNDO, THROW NEW Progress.Lang.AppError ("RG Item " + dc-rg-item.rg-item + " nao se encontra no local do ponto de controle", 17006).
        
    
    // se validacao ok, ajustar descricao do rg
    FOR FIRST ITEM FIELDS (it-codigo desc-item cod-obsoleto) NO-LOCK 
        WHERE ITEM.it-codigo = dc-rg-item.it-codigo.
    END.
    IF NOT AVAILABLE ITEM THEN 
        UNDO, THROW NEW Progress.Lang.AppError ("Item n�o encontrado do RG Item " + dc-rg-item.rg-item, 17006).
    
    
    IF item.cod-obsoleto <> 1 THEN 
        UNDO, THROW NEW Progress.Lang.AppError ("Item do ERP obsoleto " + ITEM.it-codigo, 17006).
    
    // solicitado por alessandro em 11/08/23
    FOR FIRST dc-cor NO-LOCK
        WHERE dc-cor.cod-cor = tt-reporte-item.cod-cor,
        FIRST dc-item-cor NO-LOCK
        WHERE dc-item-cor.it-codigo = ITEM.it-codigo
        AND dc-item-cor.it-codigo-cor = dc-cor.it-codigo.
    END.
    IF NOT AVAILABLE dc-item-cor THEN 
        UNDO, THROW NEW Progress.Lang.AppError ("Item cor n�o encontrado " + ITEM.it-codigo + " - " + dc-cor.it-codigo + ", cor " + tt-reporte-item.cod-cor, 17006).
    
    FOR FIRST ITEM NO-LOCK
        WHERE ITEM.it-codigo = dc-item-cor.it-codigo-acab.
    END.
    IF NOT AVAILABLE ITEM THEN 
        UNDO, THROW NEW Progress.Lang.AppError ("Item acabado n�o encontrado " + dc-item-cor.it-codigo-acab, 17006).
    
    IF ITEM.cod-obsoleto > 1 THEN 
        UNDO, THROW NEW Progress.Lang.AppError ("Item acabado obsoleto " + dc-item-cor.it-codigo-acab,17006).
    
    tt-reporte-item.cCodLocaliz = ITEM.desc-item.        
    
    RETURN "OK".
        
    CATCH e AS Progress.Lang.Error:
        
        DO i-cont = 1 TO e:NumMessages.
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = e:GetMessage(i-cont).
        END.
        RETURN "NOK":U.    
        
    END CATCH.
    
END PROCEDURE.


/* ************************  Function Implementations ***************** */

FUNCTION fn-getErro RETURNS CHARACTER 
    (  ):
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/    

    DEFINE VARIABLE result AS CHARACTER NO-UNDO.
    
    // MAIN
    RESULT = "".
    FOR EACH tt-erro-api.
        IF RESULT = "" THEN
            RESULT = tt-erro-api.mensagem.
        ELSE
            RESULT = RESULT + ", " + tt-erro-api.mensagem. 
    END.
    
    RETURN result.
        
END FUNCTION.



