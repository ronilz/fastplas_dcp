/******************************************************************************
    Objetivo: Metodos                              
    Programa: ESAPI001         
    Data    : 12/03/2007
    Autor   : 
    Altercao: 
    Versao  : V.2.04.00.001
    
    Documentacao Metodo:
    -> pi-reporte             - Conjunto de m�todos que realizam as transa��es necess�rias para Reporte de Produ��o.
    -> 002 - adaptacao para EMS 206
    
*******************************************************************************/
DEFINE BUFFER estabel FOR mgcad.estabel.

/* Include */
{include\i-buffer.i} /* buffer EMS 2 x EMS 5 */
{include\ttdef1.i}
{include\ttdef2.i}
{cpp\cpapi001.i}   
{cpp\cpapi001.i1}   
{cpp\cpapi009.i}
{cdp/cd0666.i}
{cpp/cpapi018.i}  /* Definicao das temp-tables sumariza��o  */
{esapi\esapi001c.i}
{esapi\esapi004d.i}

// necess�rio para sessoes gui - 03/07/23
{utp/ut-glob.i}

DEFINE VARIABLE cLocalizPad AS CHARACTER NO-UNDO.


DEFINE TEMP-TABLE tt-dc-rg-item LIKE dc-rg-item
    FIELD r-rowid         AS ROWID
    FIELD nr-ord-prod-est AS INTEGER
    FIELD localiz-ent     LIKE ITEM.cod-localiz.
    
DEFINE TEMP-TABLE tt-dc-rg-item-mov 
    FIELD rg-item LIKE dc-rg-item.rg-item.    

DEFINE BUFFER b-ord-prod        FOR ord-prod.
DEFINE BUFFER b-dc-movto-rack   FOR dc-movto-rack.
DEFINE BUFFER b-tt-reporte-item FOR tt-reporte-item.
DEFINE BUFFER b-dc-rg-item      FOR dc-rg-item.
DEFINE BUFFER b-dc-reporte      FOR dc-reporte.
DEFINE BUFFER bfItem            FOR ITEM.
DEFINE BUFFER bfItemRG          FOR ITEM.
DEFINE BUFFER BFdc-rg-itemRG    FOR dc-rg-item.

PROCEDURE pi-reporte:
    DEFINE INPUT  PARAMETER TABLE FOR tt-reporte.
    DEFINE INPUT  PARAMETER TABLE FOR tt-reporte-item.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.
    
    DEFINE VARIABLE l-nok AS LOGICAL NO-UNDO.

    /* main */
    ASSIGN 
        lGerarLog = FALSE.
    IF lGerarLog THEN
    DO:
        MESSAGE "PI-REPORTE - start " ETIME(YES).
    END.
    ASSIGN 
        c-arquivo-log = "".
    /*     ASSIGN c-arquivo-log = "e:\datasul\especificos\dcp\logs\extrato.log". */
    
    IF lGerarLog THEN
        MESSAGE "Arquivo de log " c-arquivo-log .

    FIND FIRST tt-reporte NO-LOCK NO-ERROR.
    IF NOT AVAILABLE tt-reporte THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Reporte nao contem dados".
        RETURN "NOK".
    END.

    // analisar se a sess�o ja se encontra logada
    // api ser� executada em sessao gui
    // e webspeed, sendo webspeed necess�rio logar
    // e sess�o gui nao
    IF v_cdn_empres_usuar = "" THEN 
    DO: 
        ASSIGN 
            l-nok = FALSE.
        RUN btb/btapi910za.p (
            INPUT tt-reporte.cod-usuario,
            INPUT tt-reporte.cod-senha,
            OUTPUT TABLE tt-erros).

        IF RETURN-VALUE = 'nok' OR
            CAN-FIND(FIRST tt-erros) THEN
        DO:
            FIND FIRST tt-erros NO-LOCK NO-ERROR.
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "Erro Login EMS: ".

            IF AVAILABLE tt-erros THEN
                ASSIGN tt-erro-api.mensagem = STRING(tt-erros.cod-erro) + tt-erro-api.mensagem + ". " + tt-erros.desc-erro.

            RETURN "NOK".
        END.
    END.

    IF lGerarLog THEN
        MESSAGE "login " RETURN-VALUE " " tt-reporte.cod-usuario.
            
    FIND FIRST tt-reporte NO-LOCK NO-ERROR.
    IF AVAILABLE tt-reporte THEN 
    DO:
        /* verificar se o reporte j� foi iniciado por�m n�o finalizado
           caso n�o tenha sido iniciado, marcar como iniciado */
        RUN pi-ver-sit-rg.
        IF RETURN-VALUE = "OK" THEN 
        DO:
            RUN pi-marca-rg.
            IF RETURN-VALUE = "OK" THEN 
            DO:
                RUN pi-separa-reporte.
                RUN pi-desmarca-rg.
            END.
        END.
    END.
    ELSE
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "N�o existe registro na temp-table tt-reporte!".
    END.

    IF lGerarLog THEN
    DO:
        MESSAGE  "Fim pi-reporte " ETIME.
    END.

    RETURN "OK":U.

END PROCEDURE.



PROCEDURE pi-separa-reporte:
    /* vars */
    DEFINE VARIABLE de-qtd AS DECIMAL   NO-UNDO.
    DEFINE VARIABLE c-item AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont AS INTEGER   NO-UNDO.

    /*main*/
    IF lGerarLog THEN
        MESSAGE "pi-separa-reporte".

    FIND FIRST tt-reporte NO-LOCK NO-ERROR.
    FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle
        NO-LOCK NO-ERROR.

    /**
    *  Valida��o necess�ria 
    *  apenas para jun��o e tem que ser neste ponto
    **/
    IF dc-pto-controle.tipo-pto = 2 THEN
    DO:
        FOR EACH tt-reporte-item NO-LOCK.

            FIND FIRST b-tt-reporte-item
                WHERE b-tt-reporte-item.rg-item = tt-reporte-item.rg-item
                AND ROWID(b-tt-reporte-item) <> ROWID(tt-reporte-item)
                NO-LOCK NO-ERROR.

            IF NOT AVAILABLE b-tt-reporte-item THEN
                FIND FIRST b-tt-reporte-item
                    WHERE b-tt-reporte-item.rg-item = tt-reporte-item.rg-item-juncao
                    AND ROWID(b-tt-reporte-item) <> ROWID(tt-reporte-item)
                    NO-LOCK NO-ERROR.
            IF NOT AVAILABLE b-tt-reporte-item THEN
                FIND FIRST b-tt-reporte-item
                    WHERE b-tt-reporte-item.rg-item-juncao = tt-reporte-item.rg-item
                    AND ROWID(b-tt-reporte-item) <> ROWID(tt-reporte-item)    
                    NO-LOCK NO-ERROR.
            IF NOT AVAILABLE b-tt-reporte-item THEN
                FIND FIRST b-tt-reporte-item
                    WHERE b-tt-reporte-item.rg-item-juncao = tt-reporte-item.rg-item-juncao
                    AND ROWID(b-tt-reporte-item) <> ROWID(tt-reporte-item)
                    NO-LOCK NO-ERROR.
            
            IF AVAILABLE b-tt-reporte-item THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "RG Item duplicado: " + 
                                                tt-reporte-item.rg-item + " / " + 
                                                tt-reporte-item.rg-item-juncao.
                RETURN "NOK".
            END.
        END.
    END.

    bloco:
    DO TRANSACTION ON ERROR UNDO bloco, RETURN "NOK"
        ON STOP UNDO bloco, RETURN "LOCKWAIT": /* AJHJR - 23/02/2010 - tratamento de lock wait timeout */

        FOR EACH tt-reporte-item
            NO-LOCK.
            
            /**
            *  Carregar todos os rg do rack ou apenas o rg fornecido
            **/
            FOR EACH tt-dc-rg-item. 
                DELETE tt-dc-rg-item. 
            END.
            RUN pi-carga (INPUT tt-reporte-item.rg-item).
                        
            /**
            *  acertar o item pai 
            **/
            FOR EACH tt-dc-rg-item.

                IF dc-pto-controle.tipo-pto = 1 THEN 
                    ASSIGN tt-dc-rg-item.it-codigo-juncao = tt-dc-rg-item.it-codigo.
                ELSE IF dc-pto-controle.tipo-pto = 2 THEN 
                        ASSIGN tt-dc-rg-item.it-codigo-juncao = tt-reporte-item.rg-item-pai.
                    ELSE IF dc-pto-controle.tipo-pto = 3 OR
                            dc-pto-controle.tipo-pto = 8 THEN 
                        DO:
                            
                            IF NUM-ENTRIES(tt-reporte-item.rg-item-pai, ";") > 1 THEN 
                            DO: 
                                DO i-cont = 1 TO NUM-ENTRIES(tt-reporte-item.rg-item-pai, ";").
                                    ASSIGN 
                                        c-item = ENTRY(i-cont, tt-reporte-item.rg-item-pai, ";").
                                    IF LOOKUP(tt-dc-rg-item.rg-item, c-ITEM, ",") > 0 THEN 
                                        LEAVE.
                                END.
                                ASSIGN 
                                    tt-dc-rg-item.it-codigo-juncao = ENTRY(1, c-item, ",").
                            END.
                            ELSE 
                                ASSIGN tt-dc-rg-item.it-codigo-juncao = tt-reporte-item.rg-item-pai.
                        END.                            


                /* */

                FIND dc-rg-item
                    WHERE dc-rg-item.rg-item = tt-dc-rg-item.rg-item
                    NO-LOCK NO-ERROR.

                FIND ITEM
                    WHERE ITEM.it-codigo = dc-rg-item.it-codigo
                    NO-LOCK NO-ERROR.

                
                FIND bfItem
                    WHERE bfitem.it-codigo = tt-dc-rg-item.it-codigo-juncao
                    AND bfitem.it-codigo > ''
                    NO-LOCK NO-ERROR.
                IF NOT AVAILABLE bfItem THEN
                    FIND FIRST bfItem WHERE bfitem.it-codigo = replace(TRIM(ENTRY(1, tt-dc-rg-item.it-codigo-juncao, ",")), " ", "")
                        AND bfitem.it-codigo > '' NO-LOCK NO-ERROR.

                IF (SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" AND 
                    tt-reporte-item.cCodLocaliz = 'NOT_EXCHANGE' OR
                    tt-reporte-item.cCodLocaliz = '') THEN 
                DO:

                    ASSIGN 
                        cLocalizPad = (IF dc-pto-controle.utiliz-loc-pad-item AND AVAILABLE bfItem THEN 
                                                bfItem.cod-localiz
                                            ELSE IF dc-pto-controle.utiliz-loc-pad-item THEN
                                                ITEM.cod-localiz
                                            ELSE
                                                dc-pto-controle.cod-localiz-dest).
                
                END.
                ELSE 
                DO:
                    ASSIGN
                        cLocalizPad = tt-reporte-item.cCodLocaliz.
                END.
                
                ASSIGN 
                    tt-dc-rg-item.localiz-ent = cLocalizPad.

                IF lGerarLog THEN 
                    MESSAGE "Ajuste item pai V01 " 
                        tt-dc-rg-item.it-codigo-juncao
                        tt-dc-rg-item.rg-item
                        tt-dc-rg-item.it-codigo
                        tt-dc-rg-item.localiz-ent.

            END.
            
            FOR EACH tt-dc-rg-item NO-LOCK,
                FIRST dc-rg-item 
                WHERE dc-rg-item.rg-item = tt-dc-rg-item.rg-item
                NO-LOCK
                BREAK BY tt-dc-rg-item.nr-rack
                BY tt-dc-rg-item.it-codigo
                BY tt-dc-rg-item.it-codigo-juncao:
                    
                IF FIRST-OF(tt-dc-rg-item.it-codigo-juncao) THEN 
                DO:
                    ASSIGN 
                        de-qtd = 0.
                    
                    FOR EACH tt-dc-rg-item-mov. 
                        DELETE tt-dc-rg-item-mov. 
                    END.
                END.                           
                
                ASSIGN 
                    de-qtd = de-qtd + 1.
                
                CREATE tt-dc-rg-item-mov.
                ASSIGN 
                    tt-dc-rg-item-mov.rg-item = tt-dc-rg-item.rg-item.
                
                IF LAST-OF(tt-dc-rg-item.it-codigo-juncao) THEN 
                DO:
                    
                    IF dc-pto-controle.tipo-pto = 1 THEN 
                    DO:
                        RUN pi-gera-reporte(
                            INPUT tt-dc-rg-item.it-codigo-juncao /* item pai */,
                            INPUT dc-rg-item.it-codigo,
                            INPUT dc-rg-item.rg-item,
                            INPUT dc-rg-item.nr-rack,
                            INPUT de-qtd,
                            OUTPUT TABLE tt-erro-api).


                        IF RETURN-VALUE = "NOK" THEN
                            UNDO bloco, RETURN "NOK".
                    END.
                                            
                    IF dc-pto-controle.tipo-pto = 2 THEN 
                    DO:
                        RUN pi-gera-reporte(INPUT tt-dc-rg-item.it-codigo-juncao /* item pai */,
                            INPUT dc-rg-item.it-codigo,
                            INPUT dc-rg-item.rg-item,
                            INPUT "",
                            INPUT de-qtd,
                            OUTPUT TABLE tt-erro-api).
                        IF RETURN-VALUE = "NOK" THEN
                            UNDO bloco, RETURN "NOK".
                    END.
                                            
                    IF dc-pto-controle.tipo-pto = 3 OR 
                        dc-pto-controle.tipo-pto = 8 THEN 
                    DO:
                        RUN pi-gera-reporte(
                            INPUT tt-dc-rg-item.it-codigo-juncao /* item pai */,
                            INPUT dc-rg-item.it-codigo,
                            INPUT dc-rg-item.rg-item,
                            INPUT dc-rg-item.nr-rack,
                            INPUT de-qtd,
                            OUTPUT TABLE tt-erro-api).
                        IF RETURN-VALUE = "NOK" THEN
                            UNDO bloco, RETURN "NOK".
                    END.
                END.
            END.
        END.
        

        IF NOT CAN-FIND(FIRST tt-erro-api) THEN
        DO:
            /** retorno de sucesso **/
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 0
                tt-erro-api.mensagem = "Reporte Relizado com Sucesso! ".
        END.

        IF CAN-FIND(FIRST tt-erro-api
            WHERE tt-erro-api.tipo = 1) THEN
            UNDO bloco, RETURN "NOK".
    END.
    
    IF lGerarLog THEN
        MESSAGE "Fim pi-separa-reporte".
    RETURN  "OK":U.

END PROCEDURE.

/**
*  Procedure para validar informacoes inseridas no coletor
**/
PROCEDURE pi-valida-reporte:

    /* param */
    DEFINE INPUT  PARAMETER TABLE FOR tt-reporte.
    DEFINE INPUT  PARAMETER TABLE FOR tt-reporte-item.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-codigo.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /* vars */
    DEFINE VARIABLE c-item   AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-item-1 AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-item-2 AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont   AS INTEGER   NO-UNDO.

    /* main */
    ASSIGN 
        lGerarLog = FALSE.
    IF lGerarLog THEN
    DO:
        MESSAGE "pi-valida-reporte " ETIME.
    END.

    FIND FIRST dc-param NO-LOCK NO-ERROR.
    FIND FIRST tt-reporte NO-LOCK NO-ERROR.
    FIND FIRST tt-reporte-item NO-LOCK NO-ERROR.

    FIND dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle 
        NO-ERROR.
    IF NOT AVAILABLE dc-pto-controle THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Ponto de Controle " + tt-reporte.cod-pto-controle + " n�o cadastrado!".
        RETURN "NOK":U.
    END.
    
    IF dc-pto-controle.tipo-pto > 3 AND  
        dc-pto-controle.tipo-pto <> 8 THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Tipo de Ponto de Controle n�o pode ser diferente de Normal, Jun��o e Pintura".
        RETURN "NOK":U.
    END.


    /* TOTVS MPN
    FIND localizacao NO-LOCK
        WHERE localizacao.cod-estabel = dc-param.cod-estabel
          AND localizacao.cod-depos   = tt-reporte.cod-depos-dest
          AND localizacao.cod-localiz = tt-reporte.cod-localiz-dest NO-ERROR.
    IF NOT AVAIL localizacao THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1
               tt-erro-api.mensagem = "Localiza��o Destino " + tt-reporte.cod-localiz-dest + " n�o cadastrada!".
    END.
    */
        
    FIND mgcad.localizacao NO-LOCK
        WHERE localizacao.cod-estabel = dc-param.cod-estabel
        AND localizacao.cod-depos   = tt-reporte.cod-depos-orig
        AND localizacao.cod-localiz = tt-reporte.cod-localiz-orig NO-ERROR.
    IF NOT AVAILABLE localizacao THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Localiza��o Origem " + tt-reporte.cod-localiz-dest + " n�o cadastrada!".
    END.

    FIND deposito NO-LOCK
        WHERE deposito.cod-depos = tt-reporte.cod-depos-dest NO-ERROR.
    IF NOT AVAILABLE deposito THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Deposito Destino " + tt-reporte.cod-depos-dest + " n�o cadastrada!".
    END.

    FIND deposito NO-LOCK
        WHERE deposito.cod-depos = tt-reporte.cod-depos-orig NO-ERROR.
    IF NOT AVAILABLE deposito THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Deposito Origem " + tt-reporte.cod-depos-dest + " n�o cadastrada!".
    END.

    /**
    *  Verifica se o ponto de controle e juncao e o codigo de item e rack
    **/
    IF dc-pto-controle.tipo-pto = 2 AND 
        SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN 
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Tipo do ponto de controle n�o permite reporte por RACK.".           
    END.
    
    /**
    *  carga da tabela dos rgs
    **/
    IF AVAILABLE tt-reporte-item THEN
        RUN pi-carga (INPUT tt-reporte-item.rg-item).

    IF NOT CAN-FIND (FIRST tt-dc-rg-item) THEN 
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "N�o foi poss�vel ler o RG Item.".           
    END.
    
    FOR EACH tt-dc-rg-item NO-LOCK.
                
        /**
        *  Validar rg item juncao
        **/
        IF dc-pto-controle.tipo-pto = 2 THEN 
        DO:
            FIND dc-rg-item NO-LOCK
                WHERE dc-rg-item.rg-item = tt-reporte-item.rg-item-juncao
                NO-ERROR.
    
            IF AVAILABLE dc-rg-item THEN 
            DO:
                RUN pi-valida-rg-item (INPUT YES). /* validar rack */
                IF RETURN-VALUE = "NOK" THEN 
                    RETURN RETURN-VALUE.
                ELSE
                   
                    ASSIGN c-item-2 = dc-rg-item.it-codigo.
            END.
            ELSE
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Rg Item n�o encontrado: " + STRING(tt-reporte-item.rg-item,"RG99999999999").
                RETURN "NOK".
            END.
        END. 
        
        
        /**
        *  Validar rg item 
        **/
        IF lGerarLog THEN 
            MESSAGE "validando rg item".
        FIND dc-rg-item NO-LOCK
            WHERE dc-rg-item.rg-item = tt-dc-rg-item.rg-item 
            NO-ERROR.

        IF AVAILABLE dc-rg-item THEN 
        DO:
            
            IF dc-pto-controle.tipo-pto = 2 OR 
                dc-pto-controle.tipo-pto = 3 OR 
                dc-pto-controle.tipo-pto = 8 THEN
                RUN pi-valida-rg-item (INPUT YES).
            ELSE
                RUN pi-valida-rg-item (INPUT NO).

            IF RETURN-VALUE = "NOK" THEN 
                RETURN RETURN-VALUE.
            ELSE
                ASSIGN c-item-1 = dc-rg-item.it-codigo.
                
        END.
        ELSE
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "Rg Item n�o encontrado: " + STRING(tt-reporte-item.rg-item,"RG99999999999").
            RETURN "NOK".
        END.
        IF lGerarLog THEN 
            MESSAGE c-item-1. 
               
        IF dc-pto-controle.tipo-pto = 1 /*Normal*/ THEN 
        DO:
            /**
            *  criar dados de retorno da interface
            **/
            FIND ITEM
                WHERE ITEM.it-codigo = dc-rg-item.it-codigo
                NO-LOCK NO-ERROR.

            
            IF (SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" AND 
                tt-reporte-item.cCodLocaliz <> 'NOT_EXCHANGE' AND
                tt-reporte-item.cCodLocaliz <> '') THEN 
            DO:

                ASSIGN 
                    cLocalizPad = tt-reporte-item.cCodLocaliz.

            END.
            ELSE 
            DO:

                ASSIGN 
                    cLocalizPad = (IF tt-reporte-item.cCodLocaliz <> 'NOT_EXCHANGE' THEN
                                        tt-reporte-item.cCodLocaliz
                                    ELSE IF dc-pto-controle.utiliz-loc-pad-item THEN 
                                        ITEM.cod-localiz
                                    ELSE
                                        dc-pto-controle.cod-localiz-dest).
            END.
                                   
            CREATE tt-codigo.
            ASSIGN 
                tt-codigo.codigo    = dc-rg-item.it-codigo /* item pai */
                tt-codigo.descricao = item.desc-item
                tt-codigo.localiz   = cLocalizPad
                .

            /**
            *  Para otimizar performance, buscar a ordem de producao
            *  no momento da leitura do rg-item
            **/    
            IF dc-rg-item.nr-ord-prod = 0 AND 
                dc-rg-item.nr-prod-produ = 0 THEN
            DO:
                FIND FIRST param-cp NO-LOCK NO-ERROR.
                FIND LAST ord-prod NO-LOCK USE-INDEX item-emiss
                    WHERE ord-prod.nr-ord-prod >= param-cp.prim-ord-man
                    AND ord-prod.nr-ord-prod <= param-cp.ult-ord-man
                    AND ord-prod.it-codigo    = dc-rg-item.it-codigo
                    AND ord-prod.estado      <> 7 /*Finalizada*/
                    AND ord-prod.estado      <> 8 /*Terminada*/ 
                    AND ord-prod.qt-ordem    <> (ord-prod.qt-produzida + ord-prod.qt-refugada)
                    NO-ERROR.
        
                FIND CURRENT dc-rg-item EXCLUSIVE-LOCK NO-ERROR.
                IF AVAILABLE ord-prod THEN
                    ASSIGN dc-rg-item.nr-prod-produ = ord-prod.nr-ord-prod.
                FIND CURRENT dc-rg-item NO-LOCK NO-ERROR.
            END.
        END.

        IF dc-pto-controle.tipo-pto = 2 /*Jun��o*/ THEN 
        DO:
            RUN pi-valida-juncao (INPUT c-item-1,
                INPUT c-item-2,
                OUTPUT c-item).
            IF RETURN-VALUE = "NOK" THEN 
                RETURN RETURN-VALUE.

            FIND ITEM
                WHERE ITEM.it-codigo = c-item
                NO-LOCK NO-ERROR.


            IF (SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" AND 
                tt-reporte-item.cCodLocaliz <> 'NOT_EXCHANGE' AND
                tt-reporte-item.cCodLocaliz <> '') THEN 
            DO:

                ASSIGN 
                    cLocalizPad = tt-reporte-item.cCodLocaliz.

            END.
            ELSE 
            DO:

                ASSIGN 
                    cLocalizPad = (IF tt-reporte-item.cCodLocaliz <> 'NOT_EXCHANGE' THEN
                                        tt-reporte-item.cCodLocaliz
                                    ELSE IF dc-pto-controle.utiliz-loc-pad-item THEN 
                                        ITEM.cod-localiz
                                    ELSE
                                        dc-pto-controle.cod-localiz-dest).
            END.



            CREATE tt-codigo.
            ASSIGN 
                tt-codigo.codigo    = ITEM.it-codigo        /* item pai */
                tt-codigo.descricao = item.desc-item
                tt-codigo.localiz   = cLocalizPad
                .
        END.

        IF dc-pto-controle.tipo-pto = 3 OR 
            dc-pto-controle.tipo-pto = 8 /*Pintura*/ THEN 
        DO:
            IF AVAILABLE tt-reporte-item THEN
                IF lGerarLog THEN 
                    MESSAGE "tt-reporte-item " tt-reporte-item.rg-item 
                        tt-reporte-item.rg-item-pai
                        tt-reporte-item.rg-item-juncao
                        tt-reporte-item.cod-cor
                        tt-dc-rg-item.rg-item.
            /**
            *  Buscar o �ltimo apontamento da cor do item
            **/                                            
            FIND LAST dc-aponta
                WHERE dc-aponta.rg-item = tt-dc-rg-item.rg-item
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE dc-aponta THEN 
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "N�o encontrado apontamento cor " + tt-dc-rg-item.rg-item.
                RETURN "NOK".        
            END.
            FIND dc-cor
                WHERE dc-cor.cod-cor = dc-aponta.cod-cor
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE dc-cor THEN 
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "N�o encontrado a cor " + dc-aponta.cod-cor.
                RETURN "NOK".     
            END.
            
            IF dc-aponta.ind-situacao <> 1 AND 
                dc-aponta.ind-situacao <> 3 THEN 
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Pintura com situa��o Inv�lida: " + tt-dc-rg-item.rg-item.
                RETURN "NOK".              
            END.
                
            RUN pi-valida-pintura (
                INPUT dc-aponta.cod-cor,
                INPUT dc-rg-item.it-codigo,
                OUTPUT c-item).


            IF RETURN-VALUE = "OK" THEN 
            DO:
                IF dc-pto-controle.tipo-pto = 8 OR
                    dc-pto-controle.tipo-pto = 3 THEN 
                DO:
                    DO i-cont = 1 TO NUM-ENTRIES(c-item,","):
                        FIND ITEM 
                            WHERE item.it-codigo = ENTRY(i-cont,c-item,",")
                            NO-LOCK NO-ERROR.

                        IF (SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" AND 
                            tt-reporte-item.cCodLocaliz <> 'NOT_EXCHANGE' AND
                            tt-reporte-item.cCodLocaliz <> '') THEN 
                        DO:

                            ASSIGN 
                                cLocalizPad = tt-reporte-item.cCodLocaliz.
            
                        END.
                        ELSE 
                        DO:
            
                            ASSIGN 
                                cLocalizPad = (IF tt-reporte-item.cCodLocaliz <> 'NOT_EXCHANGE' THEN
                                                    tt-reporte-item.cCodLocaliz
                                                ELSE IF dc-pto-controle.utiliz-loc-pad-item THEN 
                                                    ITEM.cod-localiz
                                                ELSE
                                                    '').
                        END.
                            
                        CREATE tt-codigo.
                        ASSIGN 
                            tt-codigo.codigo    = item.it-codigo 
                            tt-codigo.descricao = tt-dc-rg-item.rg-item + "," + dc-cor.it-codigo
                            tt-codigo.localiz   = cLocalizPad
                            .
                    END.
                END.
            END.
            ELSE 
            DO:
            /* TOTVS MPN RETIRADO EM 25/07/2014 A PEDIDOS DO ALESSANDRO 
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1
                   tt-erro-api.mensagem = "RG Item n�o possui a cor: " + dc-rg-item.rg-item + 
                                           " / " + tt-reporte-item.cod-cor.
            RETURN "NOK".    
            */
            END.
        END.
    END.

    IF lGerarLog THEN
        MESSAGE "Fim pi-valida-reporte " ETIME.
        
    RETURN "OK":U.        
END PROCEDURE.



/*-------S� � executado essa procedure quando 
  o tipo de controle for igual a jun��o---------*/
PROCEDURE pi-valida-juncao:
    /* param */
    DEFINE INPUT PARAMETER p-item-1 AS CHARACTER NO-UNDO.
    DEFINE INPUT PARAMETER p-item-2 AS CHARACTER NO-UNDO.
    DEFINE OUTPUT PARAMETER p-it-codigo AS CHARACTER.

    /* vars */
    DEFINE VARIABLE c-item AS CHARACTER NO-UNDO.

    /* main */
    ASSIGN 
        lGerarLog = FALSE.
    IF lGerarLog THEN
        MESSAGE  "pi-valida-juncao" .

    FIND FIRST dc-juncao NO-LOCK
        WHERE dc-juncao.it-codigo-1 = TRIM(p-item-1)
        AND dc-juncao.it-codigo-2 = TRIM(p-item-2) 
        NO-ERROR.

    IF NOT AVAILABLE dc-juncao THEN
        FIND FIRST dc-juncao NO-LOCK
            WHERE dc-juncao.it-codigo-1 = TRIM(p-item-2)
            AND dc-juncao.it-codigo-2 = TRIM(p-item-1) 
            NO-ERROR.

    IF AVAILABLE dc-juncao THEN
    DO:
        /** validar ordem de insercao dos itens juncao */
        IF dc-juncao.ind-juncao = 1 THEN 
        DO:
            IF dc-juncao.it-codigo-1 <> p-item-1 THEN 
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Ordem de leitura incorreta (" + STRING(dc-juncao.ind-juncao) +
                                              ") " + dc-juncao.it-codigo-1 + 
                                              " lido ordem diferente: " + p-item-1.
                RETURN "NOK":U.
            END.
        END.
        ELSE IF dc-juncao.ind-juncao = 2 THEN 
            DO:
                IF dc-juncao.it-codigo-2 <> p-item-1 THEN 
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "Ordem de leitura incorreta (" + STRING(dc-juncao.ind-juncao) +
                                              ") " + dc-juncao.it-codigo-2 + 
                                              " lido ordem diferente: " + p-item-1.
                    RETURN "NOK":U.
                END.
            END.

        ASSIGN 
            p-it-codigo = dc-juncao.it-codigo-pai.
    END.
    ELSE 
    DO: 
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Juncao de semi-acabado " + p-item-1  + " com semi-acabado " + p-item-2 + " nao cadastrado!".
        RETURN "NOK":U.
    END.
    
END PROCEDURE.

/**
*  buscar o item pai atraves da estrutura do item 
**/
PROCEDURE pi-valida-pintura:
    /* param */
    DEFINE INPUT PARAMETER p-cor LIKE dc-cor.cod-cor.
    DEFINE INPUT PARAMETER p-item LIKE dc-rg-item.it-codigo.
    DEFINE OUTPUT PARAMETER p-item-cor AS CHARACTER.
    
    /* vars */
    DEFINE VARIABLE c-lista AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont AS INTEGER NO-UNDO INITIAL 1.
    DEFINE VARIABLE l-fdcw015 AS LOGICAL NO-UNDO INITIAL FALSE.
                                 
    /* main */
    ASSIGN 
        lGerarLog = FALSE.
    IF lGerarLog THEN
        MESSAGE  "pi-reporte-pintura " ETIME.

    IF lGerarLog THEN 
        MESSAGE  "codigo da cor " p-cor.
        
    /**
    *  Buscar o item atraves da estrutura
    **/
    FIND dc-cor 
        WHERE dc-cor.cod-cor = p-cor 
        NO-LOCK NO-ERROR.
    IF AVAILABLE dc-cor THEN
    DO: 
        i-cont = 1.
        IF lGerarLog THEN MESSAGE "===> antes do program-name " i-cont.
        REPEAT WHILE PROGRAM-NAME(i-cont) <> ?:
            
            IF lGerarLog THEN MESSAGE "===> program-name: " + PROGRAM-NAME (i-cont).
            IF PROGRAM-NAME(i-cont) MATCHES "*fdcw015*" THEN 
                l-fdcw015 = TRUE.
                
            i-cont = i-cont + 1.
        END.
        IF lGerarLog THEN MESSAGE "===> fim do program-name".
        
        IF l-fdcw015 THEN 
        DO:
            FOR FIRST dc-cor NO-LOCK
                WHERE dc-cor.cod-cor = p-cor,
                FIRST dc-item-cor NO-LOCK
                WHERE dc-item-cor.it-codigo = p-item
                  AND dc-item-cor.it-codigo-cor = dc-cor.it-codigo.
            END.
            IF AVAILABLE dc-item-cor THEN
                 p-item-cor = dc-item-cor.it-codigo-acab.
            ELSE
                p-item-cor = "".
        END.
        ELSE
            RUN esapi/esapi004c.p (INPUT dc-cor.it-codigo,
                INPUT p-item,
                OUTPUT p-item-cor).
    END.            
                               
    IF TRIM(p-item-cor) = "" THEN 
        RETURN  "NOK":U.
    ELSE
        RETURN "OK":U.
        
END PROCEDURE.


/*-------Essa procedure � procedure que gera o reporte 
    de produ��o, indiferentemente do tipo de controle---------*/              
PROCEDURE pi-gera-reporte:
    /* param */
    DEFINE INPUT PARAMETER p-item    LIKE ITEM.it-codigo NO-UNDO.
    DEFINE INPUT PARAMETER p-item-c  LIKE ITEM.it-codigo NO-UNDO.
    DEFINE INPUT PARAMETER p-rg-item LIKE dc-rg-item.rg-item NO-UNDO.
    DEFINE INPUT PARAMETER p-nr-rack LIKE dc-rg-item.nr-rack NO-UNDO.
    DEFINE INPUT PARAMETER p-qtd AS INTEGER NO-UNDO.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /* vars */
    DEFINE VARIABLE de-prod        LIKE ord-prod.qt-produzida.
    DEFINE VARIABLE i-seq-reporte  AS INTEGER   NO-UNDO.
    DEFINE VARIABLE i-seq-rack     AS INTEGER   NO-UNDO.
    DEFINE VARIABLE c-erro         AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-nr-ord-produ LIKE ord-prod.nr-ord-produ NO-UNDO.
    DEFINE VARIABLE l-ok           AS LOGICAL   NO-UNDO.

    /** 002 **/
    DEFINE VARIABLE h-handle       AS HANDLE    NO-UNDO.
    DEFINE VARIABLE c-texto        AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont         AS INTEGER   NO-UNDO.
    DEFINE VARIABLE c-retorno      AS CHARACTER NO-UNDO.


    /* main */
    IF lGerarLog THEN
        MESSAGE  "pi-gera-reporte" .

    FIND FIRST tt-reporte      NO-LOCK NO-ERROR.
    FIND FIRST param-cp NO-LOCK NO-ERROR.
    FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle
        NO-LOCK NO-ERROR.

    FIND dc-rg-item
        WHERE dc-rg-item.rg-item = p-rg-item NO-LOCK NO-ERROR.
    IF AVAILABLE dc-rg-item THEN
        ASSIGN i-nr-ord-produ = dc-rg-item.nr-prod-produ.

    ASSIGN 
        l-ok = FALSE.

    IF dc-rg-item.nr-ord-prod = 0 AND
        dc-rg-item.nr-prod-produ <> 0 AND 
        dc-rg-item.it-codigo = ENTRY(1, p-item, ",") THEN
    DO:

        FIND ord-prod
            WHERE ord-prod.nr-ord-prod = dc-rg-item.nr-prod-produ
            NO-LOCK NO-ERROR.

        IF (AVAILABLE ord-prod AND 
            ( (ord-prod.qt-ordem < (ord-prod.qt-produzida + ord-prod.qt-refugada + 1) ) OR
            ord-prod.estado >= 7)) OR
            NOT AVAILABLE ord-prod THEN
        DO:
            FIND LAST ord-prod NO-LOCK USE-INDEX item-emiss
                WHERE ord-prod.nr-ord-prod >= param-cp.prim-ord-man
                AND ord-prod.nr-ord-prod <= param-cp.ult-ord-man
                AND ord-prod.it-codigo    = dc-rg-item.it-codigo
                AND ord-prod.estado      <> 7 /*Finalizada*/
                AND ord-prod.estado      <> 8 /*Terminada*/ 
                AND ord-prod.qt-ordem    >= (ord-prod.qt-produzida + ord-prod.qt-refugada + p-qtd)
                NO-ERROR.
        END.
            
    END.
    ELSE
    DO:

        /**
        *  Verficar duplicidade de reporte
        **/
        FIND LAST dc-reporte 
            WHERE dc-reporte.rg-item = p-rg-item
            AND dc-reporte.it-codigo-pai = ENTRY(1, p-item, ",")
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-reporte AND
            dc-reporte.ind-tipo-reporte <> 3 THEN
        DO:
            CREATE tt-erro-api.             
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG Item j� reportado(1): " + p-rg-item + " / " + ENTRY(1, p-item, ",").
            RETURN "NOK".
        END.

        /**
        *  Se achar reporte diferente  de juncao
        **/
        FOR LAST dc-reporte NO-LOCK 
            WHERE dc-reporte.rg-item = p-rg-item.
        END.
            
        IF AVAILABLE dc-reporte AND
            dc-reporte.ind-tipo-reporte <> 3 AND
            dc-pto-controle.tipo-pto <> 2 AND /* rzo 17/10/2007 */
            dc-pto-controle.tipo-pto <> 3 AND 
            dc-pto-controle.tipo-pto <> 8 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG Item j� reportado(2): " + p-rg-item + " / " + ENTRY(1, p-item, ",").
            RETURN "NOK".
               
        END.


        FIND LAST ord-prod NO-LOCK USE-INDEX item-emiss
            WHERE ord-prod.nr-ord-prod >= param-cp.prim-ord-man
            AND ord-prod.nr-ord-prod <= param-cp.ult-ord-man
            AND ord-prod.it-codigo    = ENTRY(1, p-item, ",")
            AND ord-prod.estado      <> 7 /*Finalizada*/
            AND ord-prod.estado      <> 8 /*Terminada*/ 
            AND ord-prod.qt-ordem    >= (ord-prod.qt-produzida + ord-prod.qt-refugada + p-qtd)
            NO-ERROR.

        /**
        *  Verificar rack primeiro
        **/
        FIND FIRST dc-reporte
            WHERE dc-reporte.nr-rack = p-nr-rack
            AND dc-reporte.rg-item = p-rg-item
            AND dc-reporte.nr-ord-produ = ord-prod.nr-ord-produ
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-reporte AND
            dc-reporte.ind-tipo-reporte <> 3 AND 
            dc-reporte.ind-tipo-reporte <> 8 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RACK j� reportado:(3) RC" + p-nr-rack + " / " + p-rg-item.
            RETURN "NOK".
        END.

        /**
        *  Verificar rg item
        **/
        FIND LAST dc-reporte 
            WHERE dc-reporte.rg-item = p-rg-item
            AND dc-reporte.nr-ord-produ = ord-prod.nr-ord-produ
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-reporte AND
            dc-reporte.ind-tipo-reporte <> 3 AND 
            dc-reporte.ind-tipo-reporte <> 8 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG Item j� reportado:(4) " + p-rg-item + " / " + ENTRY(1, p-item, ",").
            RETURN "NOK".
        /*UNDO bloco, LEAVE bloco.*/
        END.
    END.
        
    IF AVAILABLE ord-prod THEN
    DO:
        IF dc-rg-item.nr-ord-prod = ord-prod.nr-ord-prod AND
            AVAILABLE dc-reporte AND 
            dc-reporte.ind-tipo-reporte <> 3 AND 
            dc-reporte.ind-tipo-reporte <> 8 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG Item j� reportado:(5) " + p-rg-item + " / " + ENTRY(1, p-item, ",").
            RETURN "NOK".
        END.
    
        ASSIGN 
            l-ok = TRUE.

        IF lGerarLog THEN
            MESSAGE  "OP " ord-prod.nr-ord-produ .

        FIND ITEM NO-LOCK 
            WHERE ITEM.it-codigo = ord-prod.it-codigo NO-ERROR.
    
        IF ord-prod.nr-req-sum = 0 THEN
            RUN pi-sumariza(INPUT ord-prod.nr-ord-produ).
    
        ASSIGN 
            de-prod = ord-prod.qt-produzida.

        IF lGerarLog THEN
            MESSAGE  "Fim Sumariza: "  de-prod " - " p-qtd .


        IF lGerarLog THEN
            MESSAGE 
                '* tt-reporte.cod-depos-dest   ' tt-reporte.cod-depos-dest  
                '* tt-reporte.cod-localiz-dest ' tt-reporte.cod-localiz-dest
                '* tt-reporte.cod-depos-orig   ' tt-reporte.cod-depos-orig  
                '* tt-reporte.cod-localiz-orig ' tt-reporte.cod-localiz-orig
                .

        EMPTY TEMP-TABLE tt-rep-prod.

        CREATE tt-rep-prod.
        ASSIGN 
            tt-rep-prod.tipo                  = 1
            tt-rep-prod.nr-ord-produ          = ord-prod.nr-ord-produ
            tt-rep-prod.it-codigo             = ITEM.it-codigo
            tt-rep-prod.data                  = TODAY
            tt-rep-prod.qt-reporte            = p-qtd
            tt-rep-prod.nro-docto             = IF p-nr-rack <> "" THEN ("RC" + p-nr-rack) ELSE p-rg-item 
            tt-rep-prod.procura-saldos        = TRUE
            tt-rep-prod.carrega-reservas      = TRUE
            tt-rep-prod.requis-automatica     = TRUE
            tt-rep-prod.prog-seg              = "CP0301"
            tt-rep-prod.finaliza-ordem        = FALSE
            tt-rep-prod.finaliza-oper         = TRUE
            tt-rep-prod.reserva               = TRUE
            tt-rep-prod.cod-depos             = tt-reporte.cod-depos-dest   /*ITEM.deposito-pad*/ 
            tt-rep-prod.cod-localiz           = tt-dc-rg-item.localiz-ent /*tt-reporte-item.cCodLocaliz */
            tt-rep-prod.cod-depos-sai         = tt-reporte.cod-depos-orig
            tt-rep-prod.cod-local-sai         = tt-reporte.cod-localiz-orig
            tt-rep-prod.sequencia             = 0
            tt-rep-prod.qt-refugo             = 0
            tt-rep-prod.lote-serie            = ord-prod.lote-serie
            /*tt-rep-prod.conta-aplicacao       = ord-prod.conta-ordem  */
            tt-rep-prod.ct-codigo             = ord-prod.ct-codigo
            tt-rep-prod.sc-codigo             = ord-prod.sc-codigo
            tt-rep-prod.un                    = ord-prod.un
            tt-rep-prod.cod-versao-integracao = 001
            tt-rep-prod.tentativas            = 3
            tt-rep-prod.time-out              = 1000.

        ASSIGN 
            tt-reporte.cod-localiz-dest = tt-dc-rg-item.localiz-ent.
                            
        RUN cpp/cpapi001.p (INPUT-OUTPUT TABLE tt-rep-prod,
            INPUT        TABLE tt-refugo,
            INPUT        TABLE tt-res-neg,
            INPUT        TABLE tt-apont-mob,
            INPUT-OUTPUT TABLE tt-erro,
            INPUT        YES) NO-ERROR.

        /*TESTE R*/        
        IF lGerarLog THEN
            MESSAGE "depois cpapi001 - Tem erro ="  
                CAN-FIND(FIRST tt-erro) SKIP
                RETURN-VALUE.  

                    
        /** rzo - 002 ** - INIBIDO EM TESTE
            
        IF NOT VALID-HANDLE(h-handle) THEN
            run cpp/cpapi001.p persistent set h-handle (input-output table tt-rep-prod,
                                                        input        table tt-refugo,
                                                        input        table tt-res-neg,
                                                        input        table tt-apont-mob,
                                                        input-output table tt-erro,
                                                        input        YES).

/*            run pi-recebe-tt-refugo in h-handle (input table tt-refugo). */
 
        /*TESTE R*/        
        IF lGerarLog THEN
            MESSAGE  " ANTES DA  pi-recebe-tt-rep-prod in h-handle" .  
         
         
        run pi-recebe-tt-rep-prod in h-handle (input table tt-rep-prod).
            
            
        /*TESTE R*/        
        IF lGerarLog THEN
            MESSAGE  " ANTES DA  pi-valida-rep-prod in h-handle" .  
            
        run pi-valida-rep-prod in h-handle (input  yes,
                                            input  ord-prod.nr-ord-prod,
                                            output c-erro,
                                            output c-texto).
        
        IF NUM-ENTRIES(c-erro) > 1  THEN DO:
            
            /*TESTE R*/        
            IF lGerarLog THEN
               MESSAGE  "Erros tt-erro "   .
  
            
            do i-cont = 1 to num-entries (c-erro):
                
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1
                       tt-erro-api.mensagem = (entry (i-cont, c-erro)) + " - " + entry (i-cont, c-texto).
                   
               /*TESTE R*/        
               IF lGerarLog THEN
                  MESSAGE  " ERRO " (entry (i-cont, c-erro))   " - "   entry (i-cont, c-texto)  .  
                       
                           
            end.
                  
            UNDO bloco, LEAVE bloco.
                
        END.
        ELSE DO:
            
           /* MESSAGE "antes do processo" 
                  VIEW-AS ALERT-BOX INFO BUTTONS OK. */
                    
           /*TESTE R*/        
           IF lGerarLog THEN
              MESSAGE  "ANTES DA pi-processa-reportes  "   .  
              
                    
            run pi-processa-reportes in h-handle (input-output table tt-rep-prod,
                                                  input        table tt-refugo,
                                                  input        table tt-res-neg,
                                                  input-output table tt-erro,
                                                  input yes,
                                                  input YES).
            assign c-retorno = return-value.
                
         /* MESSAGE "retorno " c-retorno
                VIEW-AS ALERT-BOX INFO BUTTONS OK.    */
                    
            /*TESTE R*/        
            IF lGerarLog THEN
                  MESSAGE  "DEPOIS DA pi-processa-reportes  "   .  
 
                                       
        END.
             
        INIBIDO EM TESTE */
            
            
            
  
        RUN verificarErro.
        IF RETURN-VALUE = "NOK" THEN 
            RETURN "NOK".
 
 
        IF lGerarLog THEN
            MESSAGE  "depois da api " RETURN-VALUE. 
    
        IF CAN-FIND(FIRST tt-erro) OR RETURN-VALUE = "NOK" THEN
        DO:
            FOR EACH tt-erro NO-LOCK:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = STRING(tt-erro.cd-erro) + " - " + tt-erro.mensagem.
                           
                /*TESTE R*/        
                IF lGerarLog THEN
                    MESSAGE  " ERRO TT-ERRO " STRING(tt-erro.cd-erro)   " - "   tt-erro.mensagem. .  
                           
                           
            END.
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "Erros encontrados na OP " + STRING(ord-prod.nr-ord-produ).
            RETURN "NOK".
        END.
        ELSE 
        DO:
            FIND FIRST tt-rep-prod
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE tt-rep-prod THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "OP n�o reportada!".
                RETURN "NOK".
            END.
    
            FIND b-ord-prod 
                WHERE b-ord-prod.nr-ord-produ = ord-prod.nr-ord-produ 
                NO-LOCK NO-ERROR.
    
            IF b-ord-prod.qt-produzida = de-prod THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "N�o foi poss�vel reportar a ordem de produ��o".
                RETURN "NOK".
            END.
    
            IF lGerarLog THEN 
                MESSAGE "Gerando Movimento " ENTRY(1, p-item, ",") p-item-c p-rg-item.
                
            IF p-nr-rack <> "" THEN 
            DO:
                FOR EACH dc-rack-itens NO-LOCK
                    WHERE dc-rack-itens.nr-rack = p-nr-rack,
                    FIRST b-dc-rg-item NO-LOCK
                    WHERE b-dc-rg-item.rg-item = dc-rack-itens.rg-item
                    AND b-dc-rg-item.it-codigo = p-item-c,
                    FIRST tt-dc-rg-item-mov NO-LOCK
                    WHERE tt-dc-rg-item-mov.rg-item = b-dc-rg-item.rg-item.
                        
                    IF lGerarLog THEN
                        MESSAGE "gerando movimento por rack " b-dc-rg-item.rg-item " do item " ENTRY(1, p-item, ",").
                
                    RUN pi-movimento-reporte (
                        INPUT ENTRY(1, p-item, ","),
                        INPUT p-item-c,
                        INPUT b-dc-rg-item.rg-item).
                        
                    IF RETURN-VALUE = "NOK" THEN 
                        RETURN "NOK".
                END.
            END.
            ELSE 
            DO:
                IF lGerarLog THEN
                    MESSAGE "gerando movimento por rg " p-rg-item " do item " ENTRY(1, p-item, ",").
                    
                RUN pi-movimento-reporte (
                    INPUT ENTRY(1, p-item, ","),
                    INPUT p-item-c,
                    INPUT dc-rg-item.rg-item).
                    
                IF RETURN-VALUE = "NOK" THEN
                    RETURN "NOK".
            END.
        END. /*Else*/
    END. /*FOR EACH ord-prod */
    ELSE
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "N�o encontrado OP em aberto para reporte para o item " + ENTRY(1, p-item, ",").
                   
    END.
    
    IF lGerarLog THEN 
        MESSAGE "Fim pi-gera-reporte".

    RETURN "OK":U.

END PROCEDURE.
                        



/*-------Procedure que efetua a sumariza��o da 
    ordem de produ��o se a mesma n�o estiver 
    sumarizada                          ---------*/
PROCEDURE pi-sumariza:
    /* param */
    DEFINE INPUT PARAMETER p-nr-ord LIKE ord-prod.nr-ord-produ.

    /* vars */
    DEFINE VARIABLE h-cpapi018 AS HANDLE NO-UNDO.
    
    /* main */
    IF lGerarLog THEN
        MESSAGE  "pi-sumariza" .

    IF NOT VALID-HANDLE(h-cpapi018) THEN
        RUN cpp/cpapi018.p PERSISTENT SET h-cpapi018 (INPUT        TABLE tt-dados,
            INPUT        TABLE tt-ord-prod-2,
            INPUT-OUTPUT TABLE tt-req-sum,
            INPUT-OUTPUT TABLE tt-erro,
            INPUT        YES) NO-ERROR.
    RUN verificarErro.
    IF RETURN-VALUE = "NOK" THEN 
        RETURN RETURN-VALUE.                                                              

    FOR EACH tt-dados: 
        DELETE tt-dados. 
    END.

    CREATE tt-dados.
    ASSIGN 
        tt-dados.requis-por-ordem      = TRUE
        tt-dados.cod-versao-integracao = 001
        tt-dados.estado                = 1 /* Inclus�o */
        tt-dados.c-estab-ini           = ""
        tt-dados.c-estab-fim           = "ZZZ"
        tt-dados.i-linha-ini           = 0
        tt-dados.i-linha-fim           = 999
        tt-dados.i-ordem-ini           = p-nr-ord
        tt-dados.i-ordem-fim           = p-nr-ord
        tt-dados.d-data-ini            = 01/01/0001
        tt-dados.d-data-fim            = 12/31/9999.
    
    RUN pi-processa-sumaris IN h-cpapi018 (INPUT        TABLE tt-dados,
        INPUT        TABLE tt-ord-prod-2,
        INPUT-OUTPUT TABLE tt-req-sum,
        INPUT-OUTPUT TABLE tt-erro,
        INPUT        NO)
        NO-ERROR.

    IF h-cpapi018:PERSISTENT THEN 
        DELETE PROCEDURE h-cpapi018.
    IF VALID-HANDLE(h-cpapi018) THEN
        DELETE PROCEDURE h-cpapi018.

    RUN verificarErro.                                           
    IF RETURN-VALUE = "NOK" THEN 
        RETURN RETURN-VALUE.                                                              

    FOR EACH tt-erro NO-LOCK:
        
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1 
            tt-erro-api.mensagem = tt-erro.mensagem.
    END.


END PROCEDURE.



PROCEDURE pi-estorna-reporte:
    /*------------Defini��o de Parametros Input/Ouput---------*/
    DEFINE INPUT PARAMETER TABLE FOR tt-reporte.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /*     DEFINE VARIABLE h-esapi004a AS HANDLE     NO-UNDO. */
    DEFINE VARIABLE h-esapi004b AS HANDLE NO-UNDO.

    DEFINE BUFFER b-dc-rg-item FOR dc-rg-item.
    DEFINE VARIABLE l-nok AS LOGICAL NO-UNDO.

    /* inicio */
    ASSIGN 
        lGerarLog = FALSE.
    IF lGerarLog THEN
        MESSAGE "pi-estorna-reporte  " ETIME(YES).

    FIND FIRST param-cp NO-LOCK NO-ERROR.
    FIND FIRST tt-reporte NO-LOCK NO-ERROR. 

    // analisar se a sess�o ja se encontra logada
    // api ser� executada em sessao gui
    // e webspeed, sendo webspeed necess�rio logar
    // e sess�o gui nao
    IF v_cdn_empres_usuar = "" THEN 
    DO:
        ASSIGN 
            l-nok = FALSE.
        RUN btb/btapi910za.p (INPUT tt-reporte.cod-usuario, 
            INPUT tt-reporte.cod-senha, 
            OUTPUT TABLE tt-erros).

        IF RETURN-VALUE = 'nok' OR
            CAN-FIND(FIRST tt-erros) THEN
        DO:
            FIND FIRST tt-erros NO-LOCK NO-ERROR.
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "Erro Login EMS: ".

            IF AVAILABLE tt-erros THEN
                ASSIGN tt-erro-api.mensagem = tt-erro-api.mensagem + ". " + tt-erros.desc-erro.

            RETURN "NOK".
        END.
    END.
    
    IF NOT VALID-HANDLE(h-esapi004b) THEN
        RUN esapi/esapi004b.p PERSISTENT SET h-esapi004b.

    bloco:
    DO TRANSACTION ON ERROR UNDO bloco, NEXT
        ON STOP UNDO bloco, RETURN "LOCKWAIT": /* AJHRJ - 25/02/2010 - tratamento de lock wait time out */

        FOR EACH tt-reporte NO-LOCK.
        
            RUN pi-carga (INPUT tt-reporte.rg-item).

            FOR EACH tt-dc-rg-item NO-LOCK,
                FIRST dc-rg-item NO-LOCK
                WHERE dc-rg-item.rg-item = tt-dc-rg-item.rg-item
                BREAK 
                BY tt-dc-rg-item.nr-rack
                BY tt-dc-rg-item.it-codigo.

                IF FIRST-OF(tt-dc-rg-item.it-codigo) OR
                    tt-dc-rg-item.nr-rack = "" THEN
                DO:
                    IF lGerarlog THEN
                        MESSAGE  
                            tt-dc-rg-item.rg-item  SKIP
                            tt-reporte.nr-ord-produ SKIP
                            tt-dc-rg-item.nr-ord-prod SKIP
                            tt-reporte.rg-item SKIP
                            tt-dc-rg-item.it-codigo SKIP
                            tt-reporte.nro-docto SKIP.
                    
                    /**
                    *   rzo: 01/12/2021
                    *   nao � necess�rio, o tt-reporte j� contem os dados de busca
                    **/ 
                    /*
                    RUN buscarUltimoReporteEstorno (INPUT tt-reporte.rg-item,
                        INPUT tt-dc-rg-item.rg-item,
                        INPUT tt-dc-rg-item.it-codigo).
                    IF NOT AVAILABLE dc-reporte THEN
                    DO:
                        CREATE tt-erro-api.
                        ASSIGN 
                            tt-erro-api.tipo     = 1 
                            tt-erro-api.mensagem = "Reporte Coletor n�o encontrado.".
                        UNDO bloco, RETURN "NOK".
                    END.
                    */
                    
                    FIND ord-prod
                        WHERE ord-prod.nr-ord-produ = tt-dc-rg-item.nr-ord-prod //tt-reporte.nr-ord-produ //dc-reporte.nr-ord-produ
                        NO-LOCK NO-ERROR.
                    IF NOT AVAILABLE ord-prod THEN 
                    DO:
                        CREATE tt-erro-api.
                        ASSIGN 
                            tt-erro-api.tipo     = 1 
                            tt-erro-api.mensagem = "Reporte Coletor/EMS n�o encontrado.".
                        UNDO bloco, RETURN "NOK".
                    END.
                
                    IF AVAILABLE ord-prod THEN
                    DO:
                        IF lGerarLog THEN
                            MESSAGE  ord-prod.nr-ord-produ ord-prod.estado.
        
                        /**
                        *  Verificar situacao da ordem antes do reporte
                        **/
                        IF ord-prod.estado > 6 THEN 
                        DO:
                            CREATE tt-erro-api.
                            ASSIGN 
                                tt-erro-api.tipo = 1.
                            tt-erro-api.mensagem = "Ordem se encontra terminada ou finalizada.".
                            UNDO bloco, RETURN "NOK".
                        END.
                        
                        /**
                        *  rzo : 01/12/2021
                        *  n�o � necess�rio busar o ultimo reporte, atividade j� foi realizada
                        **/
                        /*
                        FIND rep-prod 
                            WHERE rep-prod.nr-reporte = dc-reporte.nr-reporte
                            NO-LOCK NO-ERROR.
                        IF NOT AVAILABLE rep-prod THEN
                            FIND rep-prod 
                                WHERE rep-prod.nr-reporte = INT(tt-reporte.nro-docto)
                                NO-LOCK NO-ERROR.
                        */
                        
                        
                        // rzo 09/06/23
                        RUN buscarUltimoReporteOP2(INPUT tt-dc-rg-item.rg-item).
                                                
                        IF AVAILABLE dc-reporte THEN 
                            FIND rep-prod 
                            WHERE rep-prod.nr-reporte = dc-reporte.nr-reporte //INT(tt-reporte.nro-docto)
                            NO-LOCK NO-ERROR.
                        ELSE
                            FIND rep-prod 
                                WHERE rep-prod.nr-reporte = INT(tt-reporte.nro-docto)
                                NO-LOCK NO-ERROR.
                                
                        IF NOT AVAILABLE rep-prod THEN
                        DO:
                            CREATE tt-erro-api.
                            ASSIGN 
                                tt-erro-api.tipo     = 1 
                                tt-erro-api.mensagem = "Registro Reporte n�o encontrado.".
                            UNDO bloco, RETURN "NOK".
                        END.
        
                        IF AVAILABLE rep-prod THEN
                        DO:
                            IF lGerarlog THEN
                                MESSAGE  "Tipo : " dc-rg-item.tipo-pto " teste ronil - ord prod" rep-prod.nr-reporte.
        
                            /**/

                            FOR EACH tt-rep-prod1. 
                                DELETE tt-rep-prod1. 
                            END.

                            CREATE tt-rep-prod1.
                            ASSIGN 
                                tt-rep-prod1.cod-versao-integracao = 001
                                tt-rep-prod1.nr-reporte            = rep-prod.nr-reporte
                                tt-rep-prod1.data                  = TODAY.

                            RUN cpp/cpapi011.p (INPUT TABLE tt-rep-prod1,                     
                                INPUT-OUTPUT TABLE tt-erro,
                                INPUT TABLE tt-relatorio,
                                INPUT YES) .

                            IF lGerarLog THEN
                                MESSAGE "return value da api " RETURN-VALUE CAN-FIND(FIRST tt-erro).
                            
                            IF CAN-FIND(FIRST tt-erro) OR 
                                RETURN-VALUE <> "OK" THEN
                            DO:
                                FOR EACH tt-erro NO-LOCK:
                                    CREATE tt-erro-api.
                                    ASSIGN 
                                        tt-erro-api.tipo     = 1
                                        tt-erro-api.mensagem = tt-erro.mensagem.
                                END. /*FOR EACH tt-erro NO-LOCK:*/

                                IF CAN-FIND(FIRST tt-erro-api WHERE
                                    tt-erro-api.tipo = 1) THEN
                                    UNDO bloco, RETURN "NOK".

                            END. /*IF CAN-FIND(FIRST tt-erro) THEN*/

                            IF NOT CAN-FIND(FIRST tt-erro-api
                                WHERE tt-erro-api.tipo = 1) THEN
                            DO:
                                IF lGerarLog THEN
                                    MESSAGE  "Gerando Log ..." .
    
                                /**
                                *  rzo: alterar busca do dc-reporte / 01/12/2021
                                *  busar o dc-reporte atrav�s do numero do reporte
                                **/
                                FOR FIRST dc-reporte NO-LOCK
                                    WHERE dc-reporte.rg-item = dc-rg-item.rg-item
                                    AND dc-reporte.nr-reporte = rep-prod.nr-reporte.
                                END.
                                IF NOT AVAILABLE dc-reporte THEN 
                                DO:
                                    CREATE tt-erro-api.
                                    ASSIGN 
                                        tt-erro-api.tipo     = 1
                                        tt-erro-api.mensagem = "DC REPORTE n�o encontrado".
                                    UNDO bloco, RETURN "NOK".
                                END.
                                
                                FIND CURRENT dc-reporte NO-ERROR NO-WAIT.
                                IF LOCKED(dc-reporte) THEN 
                                DO: 
                                    CREATE tt-erro-api.
                                    ASSIGN 
                                        tt-erro-api.tipo     = 1
                                        tt-erro-api.mensagem = "DC REPORTE em uso".
                                    UNDO bloco, RETURN "NOK".
                                END.
                                
                                ASSIGN 
                                    dc-reporte.ind-tipo-reporte = 3.
                                FIND CURRENT dc-reporte NO-LOCK NO-ERROR.

                                IF lGerarLog THEN
                                    MESSAGE  dc-reporte.ind-tipo-reporte dc-rg-item.rg-item tt-reporte.cod-pto-controle.

                                /**
                                *  acertar os outros rgs do rc
                                **/
                                IF lGerarLog THEN
                                    MESSAGE "Acertando os outros rgs ...".

                                IF tt-dc-rg-item.nr-rack <> "" THEN 
                                DO:
                                    FOR EACH b-dc-rg-item
                                        WHERE b-dc-rg-item.nr-rack = tt-dc-rg-item.nr-rack
                                        AND b-dc-rg-item.it-codigo = tt-dc-rg-item.it-codigo
                                        NO-LOCK.
    
                                        FIND LAST b-dc-reporte
                                            WHERE b-dc-reporte.rg-item = b-dc-rg-item.rg-item
                                            AND b-dc-reporte.nr-reporte = rep-prod.nr-reporte
                                            NO-ERROR.
                                        IF AVAILABLE b-dc-reporte THEN 
                                        DO:
                                            ASSIGN 
                                                b-dc-reporte.ind-tipo-reporte = 3.
    
                                            IF lGerarLog THEN
                                                MESSAGE "Alterando status " b-dc-reporte.nr-reporte b-dc-reporte.ind-tipo-reporte.
                                        END.
                                    END.
                                END. 
                                
                                RUN pi-gera-estorno IN h-esapi004b (
                                    INPUT dc-rg-item.rg-item,
                                    INPUT tt-reporte.cod-pto-controle,
                                    INPUT rep-prod.nr-reporte,
                                    INPUT tt-reporte.cod-usuario).
                                
                                IF lGerarLog THEN 
                                    MESSAGE "Log de esotorno " RETURN-VALUE.

                                IF RETURN-VALUE = "NOK" THEN
                                    UNDO bloco, RETURN "NOK".
                                ELSE 
                                DO:
                                    CREATE tt-erro-api.
                                    ASSIGN 
                                        tt-erro-api.tipo     = 0
                                        tt-erro-api.mensagem = "RG Item estornado com sucesso!".
                                END.
                            END.
                        END. /*IF AVAIL rep-prod THEN*/
                    END. /*FOR EACH ord-prod NO-LOCK */
                END. /*IF AVAIL dc-rg-item THEN*/
            END.
        END.
    END.
    
    IF h-esapi004b:PERSISTENT THEN DELETE PROCEDURE h-esapi004b.

    IF NOT CAN-FIND(FIRST tt-erro-api
        WHERE tt-erro-api.tipo = 0) THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Erro estorno de OP.".
        RETURN "NOK".
    END.

    IF lGerarLog THEN
        MESSAGE "fim " ETIME.
END PROCEDURE.

PROCEDURE pi-busca-estorno-reporte:
    DEFINE INPUT PARAMETER p-rg-item LIKE dc-rg-item.rg-item.
    DEFINE INPUT PARAMETER p-cod-pto-controle LIKE dc-pto-controle.cod-pto-controle.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-reporte. 
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /* vars */
    DEFINE VARIABLE c-nr-ord-prod AS INTEGER NO-UNDO.
    DEFINE VARIABLE i-cont        AS INTEGER.
    DEFINE VARIABLE l-rachou      AS LOGICAL INITIAL FALSE NO-UNDO.

    /* main */
    ASSIGN 
        lGerarLog = FALSE.
    IF lGerarLog THEN
        MESSAGE  "pi-busca-estorno-reporte - atualziacao..." .

    FOR EACH tt-reporte. 
        DELETE tt-reporte. 
    END.

    FIND FIRST param-cp NO-LOCK NO-ERROR.

    FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = p-cod-pto-controle
        NO-LOCK NO-ERROR.

    /** 
    * verifica se tem reporte em processo ainda
    **/
    FIND FIRST ctx-reporte-proc
        WHERE ctx-reporte-proc.rg-item = p-rg-item
        NO-LOCK NO-ERROR.
    IF AVAILABLE ctx-reporte-proc THEN 
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "RG / RACK " + p-rg-item + " ainda em processo de reporte".
        RETURN "NOK".
    END.
    
    FOR EACH tt-dc-rg-item. 
        DELETE tt-dc-rg-item. 
    END.

    RUN pi-carga (INPUT p-rg-item).
    
    FOR EACH tt-dc-rg-item .

        IF lGerarLog THEN
            MESSAGE "Lista tt-dc-rg-item " tt-dc-rg-item.rg-item
                tt-dc-rg-item.nr-rack
                tt-dc-rg-item.nr-ord-prod.
            
        /*        IF dc-pto-controle.tipo-pto = 1 AND                      */
        /*            tt-dc-rg-item.nr-ord-prod = 0 THEN                   */
        /*            ASSIGN c-nr-ord-prod = tt-dc-rg-item.nr-prod-produ.  */
        /*        ELSE IF dc-pto-controle.tipo-pto = 2 AND                 */
        /*                tt-dc-rg-item.nr-ord-prod <> 0 THEN              */
        /*                ASSIGN c-nr-ord-prod = tt-dc-rg-item.nr-ord-prod.*/
        /*            ELSE                                                 */
        /*                ASSIGN c-nr-ord-prod = tt-dc-rg-item.nr-ord-prod.*/

        // rzo 03/12
        // tipo de reporte 3 a 8
        RUN buscarUltimoReporteOP2 (INPUT tt-dc-rg-item.rg-item).
        
        IF lGerarLog THEN 
        DO:
            IF NOT AVAILABLE dc-reporte THEN 
                MESSAGE "N�o encontrado dc reporte " SKIP.
            ELSE
                MESSAGE "reporte: " dc-reporte.nr-reporte.
        END. 
                            
        IF NOT AVAILABLE dc-reporte THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "N�o encontrado Reportes em aberto para esse RG: " + tt-dc-rg-item.rg-item.
            RETURN "NOK".
        END.
        
        IF AVAILABLE dc-reporte THEN 
            c-nr-ord-prod = dc-reporte.nr-ord-produ.
                        
            
        /*        IF c-nr-ord-prod = 0 THEN                                                         */
        /*        DO:                                                                               */
        /*            FOR LAST ord-prod NO-LOCK USE-INDEX item-emiss                                */
        /*                WHERE ord-prod.nr-ord-prod >= param-cp.prim-ord-man                       */
        /*                AND ord-prod.nr-ord-prod <= param-cp.ult-ord-man                          */
        /*                AND ord-prod.it-codigo    = tt-dc-rg-item.it-codigo                       */
        /*                AND ord-prod.estado      <> 7 /*Finalizada*/                              */
        /*                AND ord-prod.estado      <> 8 /*Terminada*/                               */
        /*                AND ord-prod.qt-ordem    <> (ord-prod.qt-produzida + ord-prod.qt-refugada)*/
        /*                /*AND ord-prod.nr-ord-prod  = tt-dc-rg-item.nr-prod-produ*/               */
        /*                BREAK BY ord-prod.data-alt.                                               */
        /*                                                                                          */
        /*                ASSIGN                                                                    */
        /*                    c-nr-ord-prod = ord-prod.nr-ord-prod.                                 */
        /*            END.                                                                          */
        /*        END.                                                                              */

        IF lGerarLog THEN 
            MESSAGE "OP " c-nr-ord-prod.

        FIND ord-prod
            WHERE ord-prod.nr-ord-prod = c-nr-ord-prod
            NO-LOCK NO-ERROR.
        IF AVAILABLE ord-prod THEN
        DO:
            IF tt-dc-rg-item.nr-rack <> "" AND 
                tt-dc-rg-item.nr-rack <> substring(p-rg-item,3,11) THEN 
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "RG Item se encontra em um Rack!" + tt-dc-rg-item.nr-rack.
                RETURN "NOK".
            END.

            /*            RUN buscarUltimoReporteOP (                                                                               */
            /*                INPUT ord-prod.nr-ord-prod,                                                                           */
            /*                INPUT tt-dc-rg-item.rg-item).                                                                         */
            /*                                                                                                                      */
            /*            IF lGerarLog THEN                                                                                         */
            /*            DO:                                                                                                       */
            /*                IF NOT AVAILABLE dc-reporte THEN                                                                      */
            /*                    MESSAGE "N�o encontrado dc reporte " SKIP.                                                        */
            /*                ELSE                                                                                                  */
            /*                    MESSAGE "reporte: " dc-reporte.nr-reporte.                                                        */
            /*            END.                                                                                                      */
            /*                                                                                                                      */
            /*            IF NOT AVAILABLE dc-reporte THEN                                                                          */
            /*            DO:                                                                                                       */
            /*                CREATE tt-erro-api.                                                                                   */
            /*                ASSIGN                                                                                                */
            /*                    tt-erro-api.tipo     = 1                                                                          */
            /*                    tt-erro-api.mensagem = "N�o encontrado Reportes em aberto para esse RG: " + tt-dc-rg-item.rg-item.*/
            /*                RETURN "NOK".                                                                                         */
            /*            END.                                                                                                      */
    
            /**
            *  Garantir que o estorno sera no mesmo rack de origem do item
            **/
            IF dc-reporte.nr-rack <> tt-dc-rg-item.nr-rack THEN 
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Rack do Reporte dif. Rack do RG Item atual ou item ja estornado: " + dc-reporte.nr-rack + "-" + tt-dc-rg-item.nr-rack.
                RETURN "NOK".
            END.
           
            FOR LAST ord-rep OF ord-prod NO-LOCK
                WHERE ord-rep.nr-reporte = dc-reporte.nr-reporte,
                FIRST rep-prod
                WHERE rep-prod.nr-reporte = ord-rep.nr-reporte
                AND rep-prod.nro-docto = p-rg-item
                NO-LOCK.

               
                FIND dc-pto-controle NO-LOCK
                    WHERE dc-pto-controle.cod-pto-controle = p-cod-pto-controle NO-ERROR.
                IF NOT AVAILABLE dc-pto-controle THEN
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "Ponto de Controle n�o encontrado!".
                    RETURN "NOK".
                END.

                /**
                *  situa��o
                **/
                IF tt-dc-rg-item.situacao <> 2 THEN
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "RG Item n�o est� ativo ou est� cancelado".
                    RETURN "NOK".
                END.
                
                IF lGerarLog THEN
                    MESSAGE tt-dc-rg-item.rg-item
                        tt-dc-rg-item.nr-ord-prod
                        tt-dc-rg-item.cod-depos-orig
                        dc-pto-controle.cod-depos-orig.

                /* totvs mpn
                IF tt-dc-rg-item.cod-depos-orig <> dc-pto-controle.cod-depos-orig OR
                   tt-dc-rg-item.cod-localiz-orig <> dc-pto-controle.cod-localiz-orig THEN
                DO:
                    CREATE tt-erro-api.
                    ASSIGN tt-erro-api.tipo = 1
                           tt-erro-api.mensagem = "RG Item n�o se encontra no local do ponto de controle.".
                    RETURN "NOK":U.
                END.
                */

                /** validar pecas no rack */
                IF tt-dc-rg-item.nr-rack <> "" THEN 
                DO:
                    ASSIGN 
                        i-cont = 0.
                    FOR EACH dc-rack-itens
                        WHERE dc-rack-itens.nr-rack = tt-dc-rg-item.nr-rack
                        NO-LOCK,
                        FIRST b-dc-rg-item
                        WHERE b-dc-rg-item.rg-item = dc-rack-itens.rg-item
                        AND b-dc-rg-item.it-codigo = tt-dc-rg-item.it-codigo
                        NO-LOCK.
   
                        ASSIGN 
                            i-cont = i-cont + 1.
                    END.

                    IF lGerarLog THEN
                        MESSAGE  "Ver. Quantidade " i-cont rep-prod.qt-reporte rep-prod.qt-estorno.
      
                    IF i-cont <> rep-prod.qt-reporte THEN 
                    DO:
                        CREATE tt-erro-api.
                        ASSIGN 
                            tt-erro-api.tipo     = 1
                            tt-erro-api.mensagem = "Qtd. do rack diferente da Qtd reportada: " + 
                                                        STRING(i-cont) + " / " + string(rep-prod.qt-reporte) + "!".
                        RETURN "NOK":U.
                    END.
                END.

                FIND ITEM NO-LOCK 
                    WHERE ITEM.it-codigo = tt-dc-rg-item.it-codigo NO-ERROR.
                
                CREATE tt-reporte.
                ASSIGN 
                    tt-reporte.cod-pto-controle  = dc-reporte.cod-pto-controle 
                    tt-reporte.desc-pto-controle = dc-pto-controle.desc-pto-controle 
                    tt-reporte.cod-depos-dest    = tt-dc-rg-item.cod-depos-orig /*dc-reporte.cod-depos-dest */
                    tt-reporte.cod-localiz-dest  = tt-dc-rg-item.cod-localiz-orig /*dc-reporte.cod-localiz-dest */
                    tt-reporte.rg-item           = tt-dc-rg-item.rg-item
                    tt-reporte.it-codigo         = tt-dc-rg-item.it-codigo
                    tt-reporte.desc-item         = IF AVAILABLE ITEM THEN ITEM.desc-item ELSE ""
                    tt-reporte.nro-docto         = dc-reporte.nr-reporte /*dc-reporte.nro-docto */
                    tt-reporte.nr-ord-produ      = ord-prod.nr-ord-produ
                    tt-reporte.local-pto         = IF AVAILABLE dc-pto-controle THEN dc-pto-controle.local-pto ELSE ""
                    tt-reporte.tipo-pto          = IF AVAILABLE dc-pto-controle THEN dc-pto-controle.tipo-pto  ELSE 1.

            END. /*IF AVAIL rep-prod*/
        END. /*FOR LAST ord-prod*/
    END.

    IF NOT CAN-FIND(FIRST tt-reporte) THEN
    DO: 
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "OP para estorno n�o encontrada".
    END.

END PROCEDURE.

PROCEDURE pi-valida-rg-item.
    /* param */
    DEFINE INPUT PARAMETER p-rack AS LOGICAL NO-UNDO.
    
    /* main */
    IF lGerarLog THEN
        MESSAGE  "pi-valida-rg-item" .

    IF NOT AVAILABLE dc-rg-item THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Rg Item " + tt-reporte-item.rg-item + " n�o cadastrado!".
        RETURN "NOK".

    END.
    
    IF dc-rg-item.situacao <> 2 THEN /*Ativo*/
    DO: 
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " n�o tem situa��o ATIVA!".
        RETURN "NOK".
    END.

    FIND dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = tt-reporte.cod-pto-controle NO-ERROR.
    IF NOT AVAILABLE dc-pto-controle THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Ponto de Controle " + tt-reporte.cod-pto-controle + " n�o cadastrado!".
        RETURN "NOK".
    END.
    ELSE IF dc-pto-controle.tipo-pto > 3 AND 
            dc-pto-controle.tipo-pto <> 8 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "Tipo de Ponto de Controle n�o pode ser diferente de Normal, Jun��o e Pintura".
            RETURN "NOK".
        END.

    
    IF dc-rg-item.cod-depos-orig <> tt-reporte.cod-depos-orig /* OR
       dc-rg-item.cod-localiz-orig <> tt-reporte.cod-localiz-orig */ THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "RG Item " + dc-rg-item.rg-item + " n�o se encontra no local do ponto de controle.".
        RETURN "NOK":U.
    END.
    
    IF p-rack THEN 
    DO:

        IF lGerarLog THEN MESSAGE 'totvs tt-reporte-item.rg-item REPLACE ' REPLACE(tt-reporte-item.rg-item, 'RC', '').


        FIND FIRST dc-rack-itens USE-INDEX idx-dc-rg-item
            WHERE dc-rack-itens.rg-item = dc-rg-item.rg-item
            AND dc-rack-itens.nr-rack <> replace(tt-reporte-item.rg-item, 'RC', '')
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-rack-itens THEN 
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG Item montado no rack " + dc-rack-itens.nr-rack.
            RETURN "NOK":U.
        END.
    END.

END.


PROCEDURE pi-movimento-reporte.
    /* param */
    DEFINE INPUT PARAMETER p-item LIKE ITEM.it-codigo NO-UNDO.
    DEFINE INPUT PARAMETER p-item-c LIKE ITEM.it-codigo NO-UNDO.
    DEFINE INPUT PARAMETER p-rg-item LIKE dc-rg-item.rg-item NO-UNDO.

    /* vars */
    DEFINE VARIABLE i-seq-movto AS INTEGER NO-UNDO.
    DEFINE BUFFER b-dc-movto-pto-controle FOR dc-movto-pto-controle.
    DEFINE BUFFER b-dc-rg-item            FOR dc-rg-item.
    DEFINE BUFFER b-item                  FOR ITEM.
    DEFINE VARIABLE i-seq-reporte AS INTEGER.  

    /* main */
    IF lGerarLog THEN
        MESSAGE  "pi-movimento-reporte inicio" .

    FIND dc-rg-item 
        WHERE dc-rg-item.rg-item = p-rg-item
        NO-ERROR NO-WAIT.
    IF LOCKED(dc-rg-item) THEN 
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1 
            tt-erro-api.mensagem = "RG Item se encontra em uso: " + p-rg-item.
        RETURN "NOK":U.                   
    END.        
    
    FIND ITEM 
        WHERE ITEM.it-codigo = dc-rg-item.it-codigo
        NO-LOCK NO-ERROR.
    FIND dc-cor
        WHERE dc-cor.cod-cor = tt-reporte-item.cod-cor
        NO-LOCK NO-ERROR.        

    IF tt-reporte-item.rg-item-juncao = "" THEN
    DO:
        IF lGerarLog THEN
            MESSAGE "Reporte normal ou pintura" tt-reporte-item.rg-item-juncao.
        
        /** 
        *  rzo 27062008
        **/
        ASSIGN 
            i-seq-reporte = NEXT-VALUE(seq-reporte).
            
        DO WHILE TRUE:
            FIND dc-reporte 
                WHERE dc-reporte.nro-docto = i-seq-reporte 
                NO-LOCK NO-ERROR.
            IF AVAILABLE dc-reporte THEN 
                ASSIGN i-seq-reporte = NEXT-VALUE(seq-reporte).
            ELSE
                LEAVE.
        END.
        
        IF lGerarLog THEN MESSAGE STRING (NOW) " - antes gravar dc-reporte: " p-item " - " p-item-c SKIP.
        
        CREATE dc-reporte.
        ASSIGN 
            dc-reporte.cod-depos-dest   = tt-reporte.cod-depos-dest
            dc-reporte.cod-localiz-dest = tt-reporte.cod-localiz-dest
            dc-reporte.cod-pto-controle = tt-reporte.cod-pto-controle
            dc-reporte.ind-tipo-reporte = IF SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN 1 /*rack*/ ELSE 2 /*rg*/
            dc-reporte.nr-rack          = IF SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN SUBSTRING(tt-reporte-item.rg-item,3,13) ELSE ""
            dc-reporte.nro-docto        = i-seq-reporte
            dc-reporte.cod-cor          = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
            dc-reporte.it-codigo-pai    = p-item
            dc-reporte.it-codigo-semi-1 = p-item-c         
            dc-reporte.it-codigo-semi-2 = tt-reporte-item.rg-item-juncao 
            dc-reporte.rg-item          = p-rg-item
            dc-reporte.nr-ord-produ     = ord-prod.nr-ord-produ
            dc-reporte.nr-reporte       = tt-rep-prod.nr-reporte
            dc-reporte.sequencia        = tt-rep-prod.sequencia
               NO-ERROR.
         
        IF lGerarLog THEN MESSAGE STRING (NOW) " - gravar dc-reporte: " dc-reporte.it-codigo-pai " - " dc-reporte.it-codigo-semi-1 SKIP.
        
        ASSIGN 
            i-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).
        
        FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle
            WHERE b-dc-movto-pto-controle.rg-item = p-rg-item
            NO-LOCK NO-ERROR.
        
        CREATE dc-movto-pto-controle.        
        ASSIGN 
            dc-movto-pto-controle.nr-seq               = i-seq-movto
            dc-movto-pto-controle.rg-item              = p-rg-item
            dc-movto-pto-controle.cod-pto-controle     = tt-reporte.cod-pto-controle
            dc-movto-pto-controle.cod-cor              = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
            dc-movto-pto-controle.cod-depos-dest       = tt-reporte.cod-depos-dest
            dc-movto-pto-controle.cod-depos-orig       = tt-reporte.cod-depos-orig
            dc-movto-pto-controle.cod-localiz-dest     = tt-reporte.cod-localiz-dest
            dc-movto-pto-controle.cod-localiz-orig     = tt-reporte.cod-localiz-orig
            dc-movto-pto-controle.cod-pto-controle-ant = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.cod-pto-controle ELSE ""
            dc-movto-pto-controle.nr-seq-ant           = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.nr-seq          ELSE 0
            dc-movto-pto-controle.it-codigo            = p-item
            dc-movto-pto-controle.it-codigo-ant        = p-item-c
            dc-movto-pto-controle.local-pto            = dc-pto-controle.local-pto
            dc-movto-pto-controle.nr-rack              = IF AVAILABLE dc-rg-item THEN dc-rg-item.nr-rack ELSE ""
            dc-movto-pto-controle.nro-docto            = dc-reporte.nro-docto
            dc-movto-pto-controle.tipo-pto             = dc-pto-controle.tipo-pto 
            dc-movto-pto-controle.cod-usuario          = tt-reporte.cod-usuario
            dc-movto-pto-controle.data                 = TODAY
            dc-movto-pto-controle.hora                 = STRING(TIME,"hh:mm:ss").
            
        RUN esapi/esapi011.p (INPUT ROWID(dc-pto-controle),
            INPUT dc-movto-pto-controle.nr-rack).
        
        /**
        *  Se pintura, n�o tem rack no log
        **/                                                                                                                                       
        ASSIGN 
            dc-rg-item.cod-pto-controle = tt-reporte.cod-pto-controle
            dc-rg-item.it-codigo        = p-item
            dc-rg-item.cod-depos-orig   = dc-reporte.cod-depos-dest
            dc-rg-item.cod-localiz-orig = dc-reporte.cod-localiz-dest
            dc-rg-item.nr-ord-prod      = ord-prod.nr-ord-produ 
            NO-ERROR.
        
        RUN verificarErro.
        IF RETURN-VALUE = "NOK" THEN 
            RETURN RETURN-VALUE.
    END.
    ELSE 
    DO:
        IF lGerarLog THEN
            MESSAGE "reporte juncao: " tt-reporte-item.rg-item-juncao.
        
        FIND b-dc-rg-item
            WHERE b-dc-rg-item.rg-item = tt-reporte-item.rg-item-juncao
            NO-ERROR NO-WAIT.
        IF LOCKED(b-dc-rg-item) THEN 
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "Rg item em uso: " + tt-reporte-item.rg-item-juncao.
            RETURN "NOK":U.
        END.                

        FIND b-item
            WHERE b-item.it-codigo = b-dc-rg-item.it-codigo
            NO-LOCK NO-ERROR.

        FIND dc-juncao
            WHERE dc-juncao.it-codigo-1 = ITEM.it-codigo
            AND dc-juncao.it-codigo-2 = b-item.it-codigo
            NO-LOCK NO-ERROR.

        IF AVAILABLE dc-juncao THEN
        DO:
            /** 
            *  rzo 27062008
            **/
            ASSIGN 
                i-seq-reporte = NEXT-VALUE(seq-reporte).
            DO WHILE TRUE:
                FIND dc-reporte 
                    WHERE dc-reporte.nro-docto = i-seq-reporte 
                    NO-LOCK NO-ERROR.
                IF AVAILABLE dc-reporte THEN 
                    ASSIGN i-seq-reporte = NEXT-VALUE(seq-reporte).
                ELSE
                    LEAVE.
            END.
            
            /**
            *  rzo - 27062008
            ***/ 
      
            CREATE dc-reporte.
            ASSIGN 
                dc-reporte.cod-depos-dest   = tt-reporte.cod-depos-dest
                dc-reporte.cod-localiz-dest = tt-reporte.cod-localiz-dest
                dc-reporte.cod-pto-controle = tt-reporte.cod-pto-controle
                dc-reporte.ind-tipo-reporte = IF SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN 1 /*rack*/ ELSE 2 /*rg*/
                dc-reporte.nr-rack          = IF SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN SUBSTRING(tt-reporte-item.rg-item,3,13) ELSE ""
                dc-reporte.nro-docto        = i-seq-reporte
                dc-reporte.cod-cor          = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
                dc-reporte.it-codigo-pai    = p-item
                dc-reporte.it-codigo-semi-1 = tt-reporte-item.rg-item        
                dc-reporte.it-codigo-semi-2 = tt-reporte-item.rg-item-juncao 
                dc-reporte.rg-item          = IF dc-juncao.ind-juncao = 1 THEN dc-rg-item.rg-item ELSE b-dc-rg-item.rg-item
                dc-reporte.nr-ord-produ     = ord-prod.nr-ord-produ
                dc-reporte.nr-reporte       = tt-rep-prod.nr-reporte
                dc-reporte.sequencia        = tt-rep-prod.sequencia
                   NO-ERROR.

            ASSIGN 
                i-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).
        
            FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle
                WHERE b-dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
                NO-LOCK NO-ERROR.
        
            CREATE dc-movto-pto-controle.        
            ASSIGN 
                dc-movto-pto-controle.nr-seq               = i-seq-movto
                dc-movto-pto-controle.rg-item              = dc-rg-item.rg-item
                dc-movto-pto-controle.cod-pto-controle     = tt-reporte.cod-pto-controle
                dc-movto-pto-controle.cod-cor              = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
                dc-movto-pto-controle.cod-depos-dest       = tt-reporte.cod-depos-dest
                dc-movto-pto-controle.cod-depos-orig       = tt-reporte.cod-depos-orig
                dc-movto-pto-controle.cod-localiz-dest     = tt-reporte.cod-localiz-dest
                dc-movto-pto-controle.cod-localiz-orig     = tt-reporte.cod-localiz-orig
                dc-movto-pto-controle.cod-pto-controle-ant = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.cod-pto-controle ELSE ""
                dc-movto-pto-controle.nr-seq-ant           = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.nr-seq          ELSE 0
                dc-movto-pto-controle.it-codigo            = IF dc-juncao.ind-juncao = 1 THEN p-item ELSE ITEM.it-codigo
                dc-movto-pto-controle.it-codigo-ant        = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.it-codigo ELSE ""
                dc-movto-pto-controle.local-pto            = dc-pto-controle.local-pto
                dc-movto-pto-controle.nr-rack              = ""
                dc-movto-pto-controle.nro-docto            = dc-reporte.nro-docto
                dc-movto-pto-controle.tipo-pto             = dc-pto-controle.tipo-pto 
                dc-movto-pto-controle.cod-usuario          = tt-reporte.cod-usuario
                dc-movto-pto-controle.data                 = TODAY
                dc-movto-pto-controle.hora                 = STRING(TIME,"hh:mm:ss").

            ASSIGN 
                i-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).

            FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle
                WHERE b-dc-movto-pto-controle.rg-item = b-dc-rg-item.rg-item
                NO-LOCK NO-ERROR.
        
            CREATE dc-movto-pto-controle.        
            ASSIGN 
                dc-movto-pto-controle.nr-seq               = i-seq-movto
                dc-movto-pto-controle.rg-item              = b-dc-rg-item.rg-item
                dc-movto-pto-controle.cod-pto-controle     = tt-reporte.cod-pto-controle
                dc-movto-pto-controle.cod-cor              = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
                dc-movto-pto-controle.cod-depos-dest       = tt-reporte.cod-depos-dest
                dc-movto-pto-controle.cod-depos-orig       = tt-reporte.cod-depos-orig
                dc-movto-pto-controle.cod-localiz-dest     = tt-reporte.cod-localiz-dest
                dc-movto-pto-controle.cod-localiz-orig     = tt-reporte.cod-localiz-orig
                dc-movto-pto-controle.cod-pto-controle-ant = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.cod-pto-controle ELSE ""
                dc-movto-pto-controle.nr-seq-ant           = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.nr-seq          ELSE 0
                dc-movto-pto-controle.it-codigo            = IF dc-juncao.ind-juncao = 2 THEN p-item ELSE b-item.it-codigo
                dc-movto-pto-controle.it-codigo-ant        = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.it-codigo ELSE ""
                dc-movto-pto-controle.local-pto            = dc-pto-controle.local-pto
                dc-movto-pto-controle.nr-rack              = ""
                dc-movto-pto-controle.nro-docto            = dc-reporte.nro-docto
                dc-movto-pto-controle.tipo-pto             = dc-pto-controle.tipo-pto 
                dc-movto-pto-controle.cod-usuario          = tt-reporte.cod-usuario
                dc-movto-pto-controle.data                 = TODAY
                dc-movto-pto-controle.hora                 = STRING(TIME,"hh:mm:ss").

            ASSIGN 
                dc-rg-item.cod-pto-controle   = tt-reporte.cod-pto-controle
                dc-rg-item.it-codigo          = IF dc-juncao.ind-juncao = 1 THEN p-item ELSE ITEM.it-codigo
                dc-rg-item.cod-depos-orig     = dc-reporte.cod-depos-dest
                dc-rg-item.cod-localiz-orig   = dc-reporte.cod-localiz-dest
                dc-rg-item.it-codigo-juncao   = b-dc-rg-item.rg-item
                dc-rg-item.nr-ord-prod        = ord-prod.nr-ord-prod

                b-dc-rg-item.cod-pto-controle = tt-reporte.cod-pto-controle
                b-dc-rg-item.it-codigo        = IF dc-juncao.ind-juncao = 2 THEN p-item ELSE b-ITEM.it-codigo
                b-dc-rg-item.cod-depos-orig   = dc-reporte.cod-depos-dest
                b-dc-rg-item.cod-localiz-orig = dc-reporte.cod-localiz-dest
                b-dc-rg-item.it-codigo-juncao = dc-rg-item.rg-item
                b-dc-rg-item.nr-ord-prod      = ord-prod.nr-ord-produ 
                   NO-ERROR.
            RUN verificarErro.
            IF RETURN-VALUE = "NOK" THEN 
                RETURN RETURN-VALUE.
    
            IF dc-juncao.ind-juncao = 1 THEN
                ASSIGN b-dc-rg-item.situacao = 4 /* juntado */ NO-ERROR.
            ELSE
                ASSIGN dc-rg-item.situacao = 4 /* juntado */ NO-ERROR.
            RUN verificarErro.
            IF RETURN-VALUE = "NOK" THEN 
                RETURN RETURN-VALUE.
                            
        END.
        IF NOT AVAILABLE dc-juncao THEN
        DO:
            FIND dc-juncao
                WHERE dc-juncao.it-codigo-1 = b-item.it-codigo
                AND dc-juncao.it-codigo-2 = ITEM.it-codigo
                NO-LOCK NO-ERROR.
            
            IF AVAILABLE dc-juncao THEN
            DO:
                /** 
                *  rzo 27062008
                **/
                ASSIGN 
                    i-seq-reporte = NEXT-VALUE(seq-reporte).
                DO WHILE TRUE:
                    FIND dc-reporte 
                        WHERE dc-reporte.nro-docto = i-seq-reporte 
                        NO-LOCK NO-ERROR.
                    IF AVAILABLE dc-reporte THEN 
                        ASSIGN i-seq-reporte = NEXT-VALUE(seq-reporte).
                    ELSE
                        LEAVE.
                END.
                
                /**
                *  rzo 27062008
                ** **/
          
                CREATE dc-reporte.
                ASSIGN 
                    dc-reporte.cod-depos-dest   = tt-reporte.cod-depos-dest
                    dc-reporte.cod-localiz-dest = tt-reporte.cod-localiz-dest
                    dc-reporte.cod-pto-controle = tt-reporte.cod-pto-controle
                    dc-reporte.ind-tipo-reporte = IF SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN 1 /*rack*/ ELSE 2 /*rg*/
                    dc-reporte.nr-rack          = IF SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" THEN SUBSTRING(tt-reporte-item.rg-item,3,13) ELSE ""
                    dc-reporte.nro-docto        = i-seq-reporte
                    dc-reporte.cod-cor          = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
                    dc-reporte.it-codigo-pai    = p-item
                    dc-reporte.it-codigo-semi-1 = tt-reporte-item.rg-item        
                    dc-reporte.it-codigo-semi-2 = tt-reporte-item.rg-item-juncao 
                    dc-reporte.rg-item          = IF dc-juncao.ind-juncao = 1 THEN b-dc-rg-item.rg-item ELSE dc-rg-item.rg-item
                    dc-reporte.nr-ord-produ     = ord-prod.nr-ord-produ
                    dc-reporte.nr-reporte       = tt-rep-prod.nr-reporte
                    dc-reporte.sequencia        = tt-rep-prod.sequencia
                       NO-ERROR.
    
                ASSIGN 
                    i-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).
            
                FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle
                    WHERE b-dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
                    NO-LOCK NO-ERROR.
            
                CREATE dc-movto-pto-controle.        
                ASSIGN 
                    dc-movto-pto-controle.nr-seq               = i-seq-movto
                    dc-movto-pto-controle.rg-item              = dc-rg-item.rg-item
                    dc-movto-pto-controle.cod-pto-controle     = tt-reporte.cod-pto-controle
                    dc-movto-pto-controle.cod-cor              = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
                    dc-movto-pto-controle.cod-depos-dest       = tt-reporte.cod-depos-dest
                    dc-movto-pto-controle.cod-depos-orig       = tt-reporte.cod-depos-orig
                    dc-movto-pto-controle.cod-localiz-dest     = tt-reporte.cod-localiz-dest
                    dc-movto-pto-controle.cod-localiz-orig     = tt-reporte.cod-localiz-orig
                    dc-movto-pto-controle.cod-pto-controle-ant = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.cod-pto-controle ELSE ""
                    dc-movto-pto-controle.nr-seq-ant           = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.nr-seq          ELSE 0
                    dc-movto-pto-controle.it-codigo            = IF dc-juncao.ind-juncao = 2 THEN p-item ELSE ITEM.it-codigo
                    dc-movto-pto-controle.it-codigo-ant        = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.it-codigo ELSE ""
                    dc-movto-pto-controle.local-pto            = dc-pto-controle.local-pto
                    dc-movto-pto-controle.nr-rack              = ""
                    dc-movto-pto-controle.nro-docto            = dc-reporte.nro-docto
                    dc-movto-pto-controle.tipo-pto             = dc-pto-controle.tipo-pto 
                    dc-movto-pto-controle.cod-usuario          = tt-reporte.cod-usuario
                    dc-movto-pto-controle.data                 = TODAY
                    dc-movto-pto-controle.hora                 = STRING(TIME,"hh:mm:ss").
    
                ASSIGN 
                    i-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).

                FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle
                    WHERE b-dc-movto-pto-controle.rg-item = b-dc-rg-item.rg-item
                    NO-LOCK NO-ERROR.
            
                CREATE dc-movto-pto-controle.        
                ASSIGN 
                    dc-movto-pto-controle.nr-seq               = i-seq-movto
                    dc-movto-pto-controle.rg-item              = b-dc-rg-item.rg-item
                    dc-movto-pto-controle.cod-pto-controle     = tt-reporte.cod-pto-controle
                    dc-movto-pto-controle.cod-cor              = IF AVAILABLE dc-cor THEN dc-cor.cod-cor ELSE ""
                    dc-movto-pto-controle.cod-depos-dest       = tt-reporte.cod-depos-dest
                    dc-movto-pto-controle.cod-depos-orig       = tt-reporte.cod-depos-orig
                    dc-movto-pto-controle.cod-localiz-dest     = tt-reporte.cod-localiz-dest
                    dc-movto-pto-controle.cod-localiz-orig     = tt-reporte.cod-localiz-orig
                    dc-movto-pto-controle.cod-pto-controle-ant = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.cod-pto-controle ELSE ""
                    dc-movto-pto-controle.nr-seq-ant           = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.nr-seq          ELSE 0
                    dc-movto-pto-controle.it-codigo            = IF dc-juncao.ind-juncao = 1 THEN p-item ELSE b-ITEM.it-codigo
                    dc-movto-pto-controle.it-codigo-ant        = IF AVAILABLE b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.it-codigo ELSE ""
                    dc-movto-pto-controle.local-pto            = dc-pto-controle.local-pto
                    dc-movto-pto-controle.nr-rack              = ""
                    dc-movto-pto-controle.nro-docto            = dc-reporte.nro-docto
                    dc-movto-pto-controle.tipo-pto             = dc-pto-controle.tipo-pto 
                    dc-movto-pto-controle.cod-usuario          = tt-reporte.cod-usuario
                    dc-movto-pto-controle.data                 = TODAY
                    dc-movto-pto-controle.hora                 = STRING(TIME,"hh:mm:ss").
    
                ASSIGN 
                    dc-rg-item.cod-pto-controle   = tt-reporte.cod-pto-controle
                    dc-rg-item.it-codigo          = IF dc-juncao.ind-juncao = 2 THEN p-item ELSE ITEM.it-codigo
                    dc-rg-item.cod-depos-orig     = dc-reporte.cod-depos-dest
                    dc-rg-item.cod-localiz-orig   = dc-reporte.cod-localiz-dest
                    dc-rg-item.it-codigo-juncao   = b-dc-rg-item.rg-item
                    dc-rg-item.nr-ord-prod        = ord-prod.nr-ord-prod
    
                    b-dc-rg-item.cod-pto-controle = tt-reporte.cod-pto-controle
                    b-dc-rg-item.it-codigo        = IF dc-juncao.ind-juncao = 1 THEN p-item ELSE b-ITEM.it-codigo
                    b-dc-rg-item.cod-depos-orig   = dc-reporte.cod-depos-dest
                    b-dc-rg-item.cod-localiz-orig = dc-reporte.cod-localiz-dest
                    b-dc-rg-item.it-codigo-juncao = dc-rg-item.rg-item
                    b-dc-rg-item.nr-ord-prod      = ord-prod.nr-ord-produ
                       NO-ERROR.
                RUN verificarErro.
                IF RETURN-VALUE = "NOK" THEN 
                    RETURN RETURN-VALUE.
                                           
                
                IF dc-juncao.ind-juncao = 1 THEN
                    ASSIGN dc-rg-item.situacao = 4 /* juntado */ NO-ERROR.
                ELSE
                    ASSIGN b-dc-rg-item.situacao = 4 /* juntado */ NO-ERROR.
                RUN verificarErro.
                IF RETURN-VALUE = "NOK" THEN 
                    RETURN RETURN-VALUE.
            END.
        END.
    END.
END PROCEDURE.


          
PROCEDURE pi-carga:
    
    DEFINE INPUT PARAMETER p-rg-item AS CHARACTER NO-UNDO.
    DEFINE VARIABLE cLocalizPad AS CHARACTER NO-UNDO.


    IF SUBSTRING(p-rg-item,1,2) = "RC" THEN
    DO:

        FOR EACH dc-rack-itens NO-LOCK 
            WHERE dc-rack-itens.nr-rack = SUBSTRING(p-rg-item,3,13):


            FIND dc-rg-item
                WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
                NO-LOCK NO-ERROR.


            IF AVAILABLE dc-rg-item THEN 
            DO:

                FIND ITEM
                    WHERE ITEM.it-codigo = dc-rg-item.it-codigo
                    NO-LOCK NO-ERROR.

                
                IF AVAILABLE tt-reporte-item THEN 
                DO:


                    FIND bfItem
                        WHERE bfitem.it-codigo = ENTRY(1, tt-reporte-item.rg-item-pai, ',')
                        AND bfitem.it-codigo > ''
                        NO-LOCK NO-ERROR.

                
                    IF (SUBSTRING(tt-reporte-item.rg-item,1,2) = "RC" AND 
                        tt-reporte-item.cCodLocaliz = 'NOT_EXCHANGE' OR
                        tt-reporte-item.cCodLocaliz = '') THEN 
                    DO:


                        ASSIGN 
                            cLocalizPad = (IF dc-pto-controle.utiliz-loc-pad-item AND AVAILABLE bfItem THEN 
                                                bfItem.cod-localiz
                                            ELSE IF dc-pto-controle.utiliz-loc-pad-item THEN
                                                ITEM.cod-localiz
                                            ELSE
                                                dc-pto-controle.cod-localiz-dest).
                
                    END.
                    ELSE 
                    DO:
                
                        /*                         ASSIGN */
                        /*                             cLocalizPad = tt-reporte-item.cCodLocaliz. */
                        /* SMS - Levantado com Alessandro, que a regra RC sera' a mesma para a RG */
                        ASSIGN 
                            cLocalizPad = (IF dc-pto-controle.utiliz-loc-pad-item AND AVAILABLE bfItem THEN 
                                                bfItem.cod-localiz
                                            ELSE IF dc-pto-controle.utiliz-loc-pad-item THEN
                                                ITEM.cod-localiz
                                            ELSE
                                                dc-pto-controle.cod-localiz-dest).
                    /* SMS - Levantado com Alessandro, que a regra RC sera' a mesma para a RG */
                        
                    END.
                END.

                CREATE tt-dc-rg-item.
                BUFFER-COPY dc-rg-item TO tt-dc-rg-item.
                ASSIGN 
                    tt-dc-rg-item.r-rowid     = ROWID(dc-rg-item)
                    tt-dc-rg-item.localiz-ent = cLocalizPad
                    .

            END.
        END.
    END.
    ELSE
    DO:
        /** 
        *  Verificar se o item se encontra em algum rack
        **/
        RELEASE dc-rack-itens.
        FIND FIRST dc-rack-itens USE-INDEX idx-dc-rg-item
            WHERE dc-rack-itens.rg-item = p-rg-item
            NO-LOCK NO-ERROR.


        IF AVAILABLE dc-rack-itens THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG Item se encontra montado no rack : " + STRING(dc-rack-itens.nr-rack,"RC99999999999").
            RETURN "NOK".
        END.

        FIND dc-rg-item NO-LOCK
            WHERE dc-rg-item.rg-item = p-rg-item NO-ERROR.

        IF AVAILABLE dc-rg-item THEN
        DO:
            CREATE tt-dc-rg-item.
            BUFFER-COPY dc-rg-item TO tt-dc-rg-item.
            ASSIGN 
                tt-dc-rg-item.r-rowid = ROWID(dc-rg-item).

            IF AVAILABLE tt-reporte-item THEN  
            DO:
                ASSIGN 
                    tt-dc-rg-item.localiz-ent = tt-reporte-item.cCodLocaliz.

            END.
        END.
    END.
END.


PROCEDURE buscarUltimoReporteEstorno.
    /* vars */
    DEFINE VARIABLE iNrDocto AS INTEGER NO-UNDO.
    DEFINE VARIABLE rRowid   AS ROWID   NO-UNDO.

    /* param */
    DEFINE INPUT PARAMETER pLinha AS CHARACTER.
    DEFINE INPUT PARAMETER pRgItem AS CHARACTER.
    DEFINE INPUT PARAMETER pItCodigo AS CHARACTER.

    /* main */
    ASSIGN 
        iNrDocto = 0.
    FOR EACH dc-reporte 
        WHERE dc-reporte.it-codigo-semi-1 = pLinha
        AND dc-reporte.rg-item = pRgItem
        AND dc-reporte.it-codigo-pai = pItCodigo
        NO-LOCK. 

        IF dc-reporte.nro-docto > iNrDocto THEN
            ASSIGN iNrDocto = dc-reporte.nro-docto
                rRowid   = ROWID(dc-reporte).
    END.

    FIND dc-reporte
        WHERE ROWID(dc-reporte) = rRowid
        NO-LOCK NO-ERROR.

END.

PROCEDURE buscarUltimoReporteOP.
    /* vars */
    DEFINE VARIABLE iNrDocto AS INTEGER NO-UNDO.
    DEFINE VARIABLE rRowid   AS ROWID   NO-UNDO.

    /* param */
    DEFINE INPUT PARAMETER pOP AS INTEGER.
    DEFINE INPUT PARAMETER pRgItem AS CHARACTER.

    /* main */
    IF lGerarLog THEN 
        MESSAGE "parametros: " pOP " - " pRgItem SKIP.
    
    ASSIGN 
        iNrDocto = 0.
    FOR EACH dc-reporte NO-LOCK 
        WHERE dc-reporte.rg-item = pRgItem
        AND dc-reporte.ind-tipo-reporte <> 3.

        IF dc-reporte.nro-docto > iNrDocto THEN
            ASSIGN iNrDocto = dc-reporte.nro-docto
                rRowid   = ROWID(dc-reporte).

    END.

    FIND dc-reporte
        WHERE ROWID(dc-reporte) = rRowid
        NO-LOCK NO-ERROR.
        
    IF lGerarLog THEN 
        MESSAGE "Avail reporte " AVAILABLE dc-reporte SKIP.        
    
END.



PROCEDURE buscarUltimoReporteOP2.
    /* vars */
    DEFINE VARIABLE iNrDocto AS INTEGER NO-UNDO.
    DEFINE VARIABLE rRowid   AS ROWID   NO-UNDO.

    /* param */
    DEFINE INPUT PARAMETER pRgItem AS CHARACTER.

    /* main */
    IF lGerarLog THEN 
        MESSAGE "parametros (2): " pRgItem SKIP.
    
    ASSIGN 
        iNrDocto = 0.
    FOR EACH dc-reporte NO-LOCK 
        WHERE dc-reporte.rg-item = pRgItem
        AND dc-reporte.ind-tipo-reporte <> 3.

        IF dc-reporte.nro-docto > iNrDocto THEN
            ASSIGN iNrDocto = dc-reporte.nro-docto
                rRowid   = ROWID(dc-reporte).

    END.

    FIND dc-reporte
        WHERE ROWID(dc-reporte) = rRowid
        NO-LOCK NO-ERROR.
        
    IF lGerarLog THEN 
        MESSAGE "Avail reporte " AVAILABLE dc-reporte SKIP.        
    
END.



