/******************************************************************************
    Objetivo: Metodos                              
    Programa: ESAPI001         
    Data    : 08/03/2007
    Autor   : 
    Altercao: 
    Vers�o  : V.2.04.00.001
    
    Documentacao Metodo:
    -> pi-pto-controle  :   - retorna todos os campos da tabela ponto de controle atrav�s da temp-table tt-pto-controle 
                            - Verificar nome dos campos na tabela dc-pto-controle
                        
    -> pi-desc-item-rack:   - retorna descricao do item ou descricao do rack.     -> pi-deposito        - retorna todos os campos da tabela deposito atrav�s da temp-table tt-deposito
                            - Verificar nome dos campos na tabela deposito
    -> pi-localizacao       - retorna todos os campos da tabela localizacao cujo deposito seja igual ao parametro passado
                            - Verificar nome dos campos na tabela localizacao
    -> pi-rack              - retorna descricao do rack
    -> pi-fifo-sac          - Verifica FIFO e SAC retornando solicitacao de senha se necessario
    -> pi-verifica-pedido   - Verifica Pedido Venda
    -> pi-verifica-item-ped - Verifica Item Pedido

    
    
*******************************************************************************/

/* variaveis locais */
DEFINE VARIABLE c-nro-docto-ant AS CHARACTER NO-UNDO.
DEFINE VARIABLE ihora-atual     AS INTEGER  NO-UNDO.
DEFINE VARIABLE chora-movto     AS CHARACTER NO-UNDO.
DEFINE VARIABLE itime-movto     AS INTEGER  NO-UNDO.
DEFINE VARIABLE i-horas         AS INTEGER  NO-UNDO.

/* Include */
{esapi\esapi001.i}

DEFINE BUFFER bf1-movto-estoq FOR movto-estoq.
DEFINE BUFFER bf2-movto-estoq FOR movto-estoq.
DEFINE BUFFER b-dc-rack-itens_esapi001 FOR dc-rack-itens.


/* temp-table */
DEFINE TEMP-TABLE tt-lista
    FIELD rg-item AS CHARACTER
    INDEX pk-tt-lista IS UNIQUE rg-item.

/** Logica do Programa **/
PROCEDURE pi-pto-controle:
    DEFINE INPUT PARAMETER p-cod-programa AS CHARACTER.
    DEFINE INPUT PARAMETER p-cod-usuario AS CHARACTER.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-pto-controle.

    FOR EACH dc-pto-usuario
        WHERE dc-pto-usuario.cod-programa = p-cod-programa
          AND dc-pto-usuario.cod-usuario = p-cod-usuario
          NO-LOCK,
        FIRST dc-pto-controle OF dc-pto-usuario
        NO-LOCK.

        CREATE tt-pto-controle.
        BUFFER-COPY dc-pto-controle TO tt-pto-controle.
        ASSIGN tt-pto-controle.desc-pto-controle = dc-pto-controle.cod-pto-controle + " - " +
                                                    dc-pto-controle.desc-pto-controle.
    END.

END PROCEDURE.

PROCEDURE pi-deposito:
    DEFINE OUTPUT PARAMETER TABLE FOR tt-deposito.
        
    FOR EACH deposito NO-LOCK
        WHERE deposito.cod-depos <> "ALM":
        CREATE tt-deposito.
        BUFFER-COPY deposito TO tt-deposito.
        ASSIGN tt-deposito.nome = tt-deposito.cod-depos + " - " +
                                   tt-deposito.nome.
    END.
END.

PROCEDURE pi-localizacao:
    DEFINE INPUT PARAMETER c-cod-depos AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-localizacao.

    FIND FIRST dc-param NO-LOCK NO-ERROR.

    IF NOT AVAILABLE dc-param THEN DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1 
               tt-erro-api.mensagem = "Parametro Data Collection nao Cadastrado.".
        RETURN.
    END.       

    FOR EACH mgcad.localizacao WHERE
             mgcad.localizacao.cod-estabel = dc-param.cod-estabel AND
             mgcad.localizacao.cod-depos = c-cod-depos               
             NO-LOCK:
        CREATE tt-localizacao.
        BUFFER-COPY localizacao TO tt-localizacao.
        ASSIGN tt-localizacao.descricao = tt-localizacao.cod-localiz + " - " +
                                          tt-localizacao.descricao.
        IF tt-localizacao.cod-localiz = "" THEN
            ASSIGN tt-localizacao.descricao = "".
    END.

END PROCEDURE.


PROCEDURE pi-fifo-sac:
    DEFINE INPUT  PARAMETER TABLE FOR tt-rg-item.
    DEFINE INPUT  PARAMETER c-cod-depos-de  AS CHARACTER    NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-localiz-de AS CHARACTER   NO-UNDO.
    DEFINE OUTPUT PARAMETER c-rg-item       AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER     c-rg-item-sac   AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER c-nr-rack       AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER l-autoriza-fifo AS LOG     NO-UNDO.
    DEFINE OUTPUT PARAMETER l-autoriza-sac  AS LOG     NO-UNDO.
    DEFINE OUTPUT PARAMETER l-autoriza-retr AS LOG     NO-UNDO.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    ASSIGN l-autoriza-fifo = NO
           l-autoriza-sac  = NO.
                 
    /**
    *  Criar lista de itens j� selecionados
    **/
    FOR EACH tt-lista. DELETE tt-lista. END.
    FOR EACH tt-rg-item NO-LOCK.
        IF SUBSTRING(tt-rg-item.codigo,1,2) = "RC" THEN
        DO:
            FOR EACH dc-rack-itens
                WHERE dc-rack-itens.nr-rack = SUBSTRING(tt-rg-item.codigo,3,11) 
                NO-LOCK.
                CREATE tt-lista.
                ASSIGN tt-lista.rg-item = dc-rack-itens.rg-item.
            END.
        END.
        ELSE
        DO:
            CREATE tt-lista.
            ASSIGN tt-lista.rg-item = tt-rg-item.codigo.
        END.
    END.

    /**
    *  Porcessar fifo
    **/
    FOR EACH tt-rg-item NO-LOCK.

        IF SUBSTRING(tt-rg-item.codigo,1,2) = "RC" THEN
        DO:
            FIND dc-rack NO-LOCK 
                WHERE dc-rack.nr-rack = SUBSTRING(tt-rg-item.codigo,3,11) NO-ERROR.
            IF NOT AVAILABLE dc-rack THEN 
            DO: 
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "Rack " + tt-rg-item.codigo + " nao Cadastrado.".
                RETURN.
            END.
            ELSE 
            DO: 
                FOR EACH dc-rack-itens NO-LOCK 
                    WHERE dc-rack-itens.nr-rack = dc-rack.nr-rack,
                    FIRST dc-rg-item NO-LOCK
                    WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item 
                    BY dc-rg-item.nr-seq:
                           
                    RUN pi-fifo-sac-2 (INPUT dc-rg-item.rg-item,
                                       INPUT c-cod-depos-de,
                                       INPUT c-cod-localiz-de,
                                       OUTPUT c-rg-item,
                                       OUTPUT l-autoriza-fifo,
                                       OUTPUT l-autoriza-sac,
                                       OUTPUT l-autoriza-retr,
                                       OUTPUT TABLE tt-erro-api).

                    FIND FIRST tt-lista
                        WHERE tt-lista.rg-item = c-rg-item
                        NO-LOCK NO-ERROR.
                    IF AVAILABLE tt-lista THEN
                        ASSIGN l-autoriza-fifo = FALSE.

                    IF l-autoriza-sac THEN
                        ASSIGN c-rg-item-sac = dc-rack-itens.rg-item.

                    IF l-autoriza-fifo THEN
                    DO:
                        FIND FIRST b-dc-rack-itens_esapi001
                            WHERE b-dc-rack-itens_esapi001.rg-item = c-rg-item
                            NO-LOCK NO-ERROR.
                        IF AVAILABLE b-dc-rack-itens_esapi001 THEN
                            ASSIGN c-nr-rack = STRING(b-dc-rack-itens_esapi001.nr-rack, "RC99999999999").
                    END.

                    IF l-autoriza-sac OR
                        l-autoriza-fifo THEN
                        LEAVE.
                END.
            END.
        END.
        ELSE 
        DO:
            FIND dc-rg-item NO-LOCK 
                WHERE dc-rg-item.rg-item = tt-rg-item.codigo NO-ERROR.
            IF AVAILABLE dc-rg-item THEN 
            DO:
                RUN pi-fifo-sac-2 (INPUT dc-rg-item.rg-item,
                                   INPUT  c-cod-depos-de,
                                   INPUT c-cod-localiz-de,
                                   OUTPUT c-rg-item,
                                   OUTPUT l-autoriza-fifo,
                                   OUTPUT l-autoriza-sac,
                                   OUTPUT l-autoriza-retr,
                                   OUTPUT TABLE tt-erro-api).

                FIND FIRST tt-lista
                    WHERE tt-lista.rg-item = c-rg-item
                    NO-LOCK NO-ERROR.
                IF AVAILABLE tt-lista THEN
                    ASSIGN l-autoriza-fifo = FALSE.

                IF l-autoriza-sac THEN
                    ASSIGN c-rg-item-sac = tt-rg-item.codigo.

                IF l-autoriza-fifo THEN
                DO:
                    FIND FIRST b-dc-rack-itens_esapi001
                        WHERE b-dc-rack-itens_esapi001.rg-item = c-rg-item
                        NO-LOCK NO-ERROR.
                    IF AVAILABLE b-dc-rack-itens_esapi001 THEN
                        ASSIGN c-nr-rack = STRING(b-dc-rack-itens_esapi001.nr-rack, "RC99999999999").
                END.
            END.
            ELSE 
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "RG Item " + tt-rg-item.codigo + " nao cadastrado.".
                RETURN.
            END.
        END.
    END.
END PROCEDURE.



PROCEDURE pi-fifo-sac-2:
    DEFINE INPUT  PARAMETER c-codigo        AS CHARACTER    NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-depos-de  AS CHARACTER    NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-localiz-de AS CHARACTER   NO-UNDO.
    DEFINE OUTPUT PARAMETER c-rg-item       AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER l-autoriza-fifo AS LOG     NO-UNDO.
    DEFINE OUTPUT PARAMETER l-autoriza-sac  AS LOG     NO-UNDO.
    DEFINE OUTPUT PARAMETER l-autoriza-retr AS LOG     NO-UNDO.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /* vars */
    DEFINE VARIABLE i-hora AS INTEGER NO-UNDO.
    /* main */

    FIND dc-param-depos NO-LOCK 
        WHERE dc-param-depos.cod-depos = c-cod-depos-de NO-ERROR.
    IF AVAILABLE dc-param-depos THEN 
    DO:
        IF dc-param-depos.ind-fifo THEN 
        DO:
            FIND dc-rg-item 
                WHERE dc-rg-item.rg-item = c-codigo NO-ERROR.
        
            IF NOT AVAILABLE dc-rg-item THEN NEXT.

            ASSIGN c-nro-docto-ant = "".
            
            FIND FIRST movto-estoq NO-LOCK
                WHERE movto-estoq.it-codigo = dc-rg-item.it-codigo 
                  AND movto-estoq.nro-docto = dc-rg-item.rg-item   
                  AND movto-estoq.cod-depos = c-cod-depos-de 
                  AND movto-estoq.cod-localiz = c-cod-localiz-de
                  AND movto-estoq.tipo-trans = 1 /* entrada*/ NO-ERROR.
            IF AVAILABLE movto-estoq THEN 
            DO:
                FOR EACH bf1-movto-estoq NO-LOCK 
                    WHERE bf1-movto-estoq.it-codigo  = movto-estoq.it-codigo 
                      AND bf1-movto-estoq.nro-docto   < movto-estoq.nro-docto 
                      AND bf1-movto-estoq.dt-trans   <= movto-estoq.dt-trans 
                      AND (bf1-movto-estoq.esp-docto  = 1   /* ACA */        
                       OR  bf1-movto-estoq.esp-docto  = 33  /*TRA */ )       
                      AND bf1-movto-estoq.cod-depos   = c-cod-depos-de    
                      AND bf1-movto-estoq.cod-localiz = c-cod-localiz-de
                      AND bf1-movto-estoq.tipo-trans  = 1   /* Entrada */ ,
                    FIRST dc-rg-item
                    WHERE dc-rg-item.rg-item = bf1-movto-estoq.nro-docto
                      AND dc-rg-item.cod-depos-orig = c-cod-depos-de
                      AND dc-rg-item.cod-localiz-orig = c-cod-localiz-de
                      AND dc-rg-item.situacao = 2 /* ativo */
                    NO-LOCK 
                    BY dc-rg-item.nr-seq:
    
                    FIND FIRST bf2-movto-estoq 
                        WHERE bf2-movto-estoq.it-codigo = bf1-movto-estoq.it-codigo
                          AND bf2-movto-estoq.nro-docto = bf1-movto-estoq.nro-docto
                          AND bf2-movto-estoq.dt-trans  >= bf1-movto-estoq.dt-trans
                          AND bf2-movto-estoq.hr-trans  >= bf1-movto-estoq.hr-trans
                          AND bf2-movto-estoq.tipo-trans = 2
                        NO-LOCK NO-ERROR.
                    IF NOT AVAILABLE bf2-movto-estoq THEN 
                    DO: 
                        ASSIGN c-nro-docto-ant = bf1-movto-estoq.nro-docto.
                        LEAVE.
                    END.
                END.    
            END.
    
            IF c-nro-docto-ant <> "" THEN 
            DO:
                ASSIGN l-autoriza-fifo = YES 
                       c-rg-item       = c-nro-docto-ant.
                /***** fora do fifo - pedir senha *****/
            END.
        END.
    
        IF dc-param-depos.ind-sac THEN 
        DO:
            FIND dc-rg-item 
                WHERE dc-rg-item.rg-item = c-codigo NO-ERROR.
        
            IF NOT AVAILABLE dc-rg-item THEN NEXT.

            IF AVAILABLE dc-rg-item AND
                dc-rg-item.situacao <> 2 THEN
                NEXT.

            ASSIGN l-autoriza-sac = NO.
    
            FIND FIRST movto-estoq NO-LOCK 
                WHERE movto-estoq.it-codigo = dc-rg-item.it-codigo 
                  AND movto-estoq.nro-docto = dc-rg-item.rg-item   
                  AND movto-estoq.cod-depos = c-cod-depos-de 
                  AND movto-estoq.cod-localiz = c-cod-localiz-de
                  AND movto-estoq.tipo-trans = 1 /* entrada*/ NO-ERROR.
            IF AVAILABLE movto-estoq THEN 
            DO:
                ASSIGN i-hora = ((TODAY - 2) - movto-estoq.dt-trans) * 24
                       i-hora = i-hora + (( TIME / 60 ) / 60 ).

/*                 ASSIGN ihora-atual = TIME                                                                                                            */
/*                        chora-movto = movto-estoq.hr-trans                                                                                            */
/*                        itime-movto = INT(SUBSTRING(chora-movto,1,2)) * 3600 + INT(SUBSTRING(chora-movto,4,2)) * 60 + INT(SUBSTRING(chora-movto,7,2)) */
/*                        i-horas     = (TODAY - movto-estoq.dt-trans) * 24 * 3600 - itime-movto + ihora-atual .                                        */
    
                /* Tempo de cura abaixo */
                IF (i-hora / 3600) < dc-param-depos.tempo-sac THEN 
                DO:
                    ASSIGN l-autoriza-sac = YES.
                    
                END.
            END.
        END.
    END.
END PROCEDURE.


PROCEDURE pi-desc-item-rack:
    DEFINE INPUT  PARAMETER c-codigo        AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER c-descricao     AS CHARACTER    NO-UNDO.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    IF SUBSTRING(c-codigo,1,2) = "RC" THEN 
    DO:
        FIND dc-rack NO-LOCK 
            WHERE dc-rack.nr-rack = SUBSTRING(c-codigo,3,13) NO-ERROR.
        IF NOT AVAILABLE dc-rack THEN 
        DO: 
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rack " + STRING(c-codigo,'RC99999999999') + " nao Cadastrado.".
            RETURN.
        END.

        IF dc-rack.situacao <> 1 THEN 
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1
                   tt-erro-api.mensagem = "Rack: " + c-codigo + " nao Ativo." .
            RETURN.
        END.

        ASSIGN c-descricao = dc-rack.desc-rack.
    END.
    ELSE DO:
        FIND dc-rg-item NO-LOCK 
            WHERE dc-rg-item.rg-item = c-codigo NO-ERROR.
        IF NOT AVAILABLE dc-rg-item THEN 
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + c-codigo + " nao Cadastrado.".
            RETURN.
        END.

        IF dc-rg-item.situacao <> 2 THEN 
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + c-codigo + " nao Ativo.".
            RETURN.
        END.

        FIND FIRST dc-rack-itens NO-LOCK
            WHERE dc-rack-itens.rg-item  = dc-rg-item.rg-item NO-ERROR.
        IF AVAILABLE dc-rack-itens THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1
                   tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " encontra-se dentro do Rack " + STRING(dc-rack-itens.nr-rack,"RC99999999999").
            RETURN "NOK".
        END.

        FIND ITEM WHERE
             ITEM.it-codigo = dc-rg-item.it-codigo NO-LOCK NO-ERROR.
        IF NOT AVAILABLE ITEM THEN 
        DO: 
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Item " + c-codigo + " nao Cadastrado.".
            RETURN.
        END.

        IF ITEM.cod-obsoleto <> 1 THEN 
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Item " + ITEM.it-codigo + " nao Ativo.".
            RETURN.
        END.

        ASSIGN c-descricao = ITEM.desc-item.
    END.
END PROCEDURE.

