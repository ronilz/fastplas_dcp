{include/ttdef1.i}

DEFINE TEMP-TABLE ttParam NO-UNDO
    FIELD cRgItemIni        AS CHARACTER
    FIELD cRgItemFim        AS CHARACTER
    FIELD cNrRackIni        AS CHARACTER
    FIELD cNrRackFim        AS CHARACTER
    FIELD cTipo             AS CHARACTER
    FIELD cCodUsuario       AS CHARACTER
    FIELD cCodPtoControle   AS CHARACTER
    .

DEFINE TEMP-TABLE ttPtoControle NO-UNDO LIKE dc-pto-controle.

DEFINE VARIABLE hesapi020 AS HANDLE NO-UNDO.
