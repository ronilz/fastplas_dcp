/******************************************************************************
    Objetivo: Metodos                              
    Programa: ESAPI001         
    Data    : 09/03/2007
    Autor   : 
    Altercao: 
    Vers�o  : V.2.04.00.001
    
    Documentacao Metodo:
    -> pi-requisicao          - 
    -> pi-devolve-requisicoes - 
    -> pi-estorna-requisicao  -
    -> pi-valida-rg-item      - 
    -> pi-devolve-ccusto      -
    
*******************************************************************************/

/* Include */
{include\ttdef1.i}
{include\ttdef2.i}
{esapi\esapi004d.i}

DEFINE NEW GLOBAL SHARED VARIABLE gr-nr-ord-produ AS CHARACTER NO-UNDO.

/** Logica do Programa **/
PROCEDURE pi-requisicao:
    /*-------Recebendo parametros-------*/
    DEFINE INPUT  PARAMETER TABLE FOR tt-requis.
    DEFINE INPUT  PARAMETER TABLE FOR tt-rg-item.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /*-------Definicao de Variaveis-------*/
    DEFINE VARIABLE h-handle AS HANDLE NO-UNDO.
    DEFINE VARIABLE l-nok    AS LOGICAL    NO-UNDO.

    /* main */
    ASSIGN lGerarLog = TRUE.

    IF NOT VALID-HANDLE(h-handle) THEN
        RUN esapi/esapi003a.p PERSISTENT SET h-handle.

    atualiza:
    DO TRANSACTION ON ERROR UNDO atualiza, NEXT:

        FIND FIRST tt-requis NO-LOCK NO-ERROR.
        IF NOT AVAILABLE tt-requis THEN
            UNDO atualiza, LEAVE atualiza.

        ASSIGN l-nok = FALSE.
        RUN btb/btapi910za.p (INPUT tt-requis.cod-usuario, 
                              INPUT tt-requis.cod-senha, 
                              OUTPUT table tt-erros).
        
        IF RETURN-VALUE = 'NOK':U THEN
        DO:
            FIND FIRST tt-erros NO-LOCK NO-ERROR.
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo = 1
                   tt-erro-api.mensagem = "Erro Login EMS: ".
        
            IF AVAILABLE tt-erros THEN
                ASSIGN tt-erro-api.mensagem = tt-erro-api.mensagem + ". " + tt-erros.desc-erro.
        
            UNDO atualiza, LEAVE atualiza.
        END.

        /* marcar rg ITEM */
        FIND FIRST dc-pto-controle
            WHERE dc-pto-controle.cod-pto-controle = tt-requis.cod-pto-controle
            NO-LOCK NO-ERROR.

        EMPTY TEMP-TABLE tt-reporte-item.
        FOR EACH tt-rg-item NO-LOCK.
            CREATE tt-reporte-item.
            ASSIGN tt-reporte-item.rg-item = tt-rg-item.codigo.
        END.
        
        RUN pi-ver-sit-rg.
        IF RETURN-VALUE = "OK" THEN DO:
            
            RUN pi-marca-rg.
            
            IF RETURN-VALUE = "OK" THEN DO:

                /* validar a todo momento a situa��o do rg item */
                FOR EACH tt-rg-item.
                    EMPTY TEMP-TABLE tt-codigo.
                    CREATE tt-codigo.
                    ASSIGN tt-codigo.codigo = tt-rg-item.codigo.

                    RUN pi-valida-rg-item (INPUT-OUTPUT TABLE tt-codigo, OUTPUT TABLE tt-erro-api).
                    IF CAN-FIND(FIRST tt-erro-api WHERE tt-erro-api.tipo = 1) THEN DO:
                        RUN pi-desmarca-rg.
                        UNDO atualiza, LEAVE atualiza.
                    END.
                END.
                

                RUN pi-inicializa-bo IN h-handle.
                RUN pi-cria-requisicao IN h-handle (INPUT TABLE tt-requis,
                                                    INPUT TABLE tt-rg-item,
                                                    OUTPUT TABLE tt-erro-api).
        
                RUN pi-finaliza-bo IN h-handle.
                IF h-handle:PERSISTENT THEN DELETE PROCEDURE h-handle.
        
                IF CAN-FIND(FIRST tt-erro-api WHERE tt-erro-api.tipo <> 0) THEN DO:
                    FIND FIRST tt-erro-api.
                    RUN pi-desmarca-rg.
                    UNDO atualiza, LEAVE atualiza.
                END.
                ELSE
                DO:
                    CREATE tt-erro-api.
                    ASSIGN tt-erro-api.tipo = 0
                           tt-erro-api.mensagem = "Requisicao Criada com Sucesso".
                    IF gr-nr-ord-produ <> "" THEN tt-erro-api.mensagem = tt-erro-api.mensagem + " OP: " + gr-nr-ord-produ.
                    ASSIGN gr-nr-ord-produ = "".
                    
                    RUN pi-desmarca-rg.
                    
                END.
            END.
        END.
        
    END. /*atualiza:*/
    
END PROCEDURE. /*pi-requisicao*/



PROCEDURE pi-devolve-requisicoes:

    /*-------Recebendo parametros-------*/
    DEFINE INPUT PARAMETER dt-ini AS DATE.
    DEFINE INPUT PARAMETER dt-fim AS DATE.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-requis-lista.

    FOR EACH dc-requisicao NO-LOCK
        WHERE dc-requisicao.data >= dt-ini
          AND dc-requisicao.data <= dt-fim
          AND NOT(dc-requisicao.ind-devolvido):

        CREATE tt-requis-lista.
        BUFFER-COPY dc-requisicao TO tt-requis-lista.
    END.
END PROCEDURE.

PROCEDURE pi-estorna-requisicao:

    /*-------Recebendo parametros-------*/
    DEFINE INPUT PARAMETER TABLE FOR tt-det-requis.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.


    /*-------Definicao de Variaveis-------*/
    DEFINE VARIABLE h-handle AS HANDLE NO-UNDO.
    DEFINE VARIABLE l-nok AS LOGICAL     NO-UNDO.

    /****************************************/
    ASSIGN lGerarLog = TRUE.
    IF lGerarLog THEN
        MESSAGE "pi-esetorna-requisicao".

    /** 
    *   Login
    **/
    FIND FIRST tt-det-requis NO-LOCK NO-ERROR.

    ASSIGN l-nok = FALSE.
    RUN btb/btapi910za.p (INPUT tt-det-requis.cod-usuario, 
                          INPUT tt-det-requis.cod-senha, 
                          OUTPUT table tt-erros).

    IF RETURN-VALUE = 'nok' OR
        CAN-FIND(FIRST tt-erros) THEN
    DO:
        FIND FIRST tt-erros NO-LOCK NO-ERROR.
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo = 1
               tt-erro-api.mensagem = "Erro Login EMS: ".

        IF AVAILABLE tt-erros THEN
            ASSIGN tt-erro-api.mensagem = STRING(tt-erros.cod-erro) + tt-erro-api.mensagem + ". " + tt-erros.desc-erro.

        RETURN "NOK".
    END.

    /* marcar rg ITEM */
/*     FIND FIRST tt-det-requis NO-LOCK NO-ERROR. */
/*     IF AVAIL tt-det-requis THEN                                                     */
/*         FIND FIRST dc-pto-controle                                                  */
/*             WHERE dc-pto-controle.cod-pto-controle = tt-det-requis.cod-pto-controle */
/*         NO-LOCK NO-ERROR.                                                           */

    EMPTY TEMP-TABLE tt-reporte-item.
    FOR EACH tt-det-requis NO-LOCK.
        CREATE tt-reporte-item.
        ASSIGN tt-reporte-item.rg-item = tt-det-requis.rg-item.
    END.
    IF CAN-FIND(FIRST tt-reporte-item) THEN 
        FIND FIRST tt-det-requis NO-LOCK NO-ERROR.
        IF AVAILABLE tt-det-requis THEN DO:
            CREATE tt-reporte.
            ASSIGN tt-reporte.cod-pto-controle = tt-det-requis.cod-pto-controle
                   tt-reporte.cod-usuario = tt-det-requis.cod-usuario.
        END.

    RUN pi-ver-sit-rg.
    IF RETURN-VALUE = "OK" THEN DO:
        RUN pi-marca-rg.
        IF RETURN-VALUE = "OK" THEN DO:
            /**
            *  processa estorno
            **/
            IF NOT VALID-HANDLE(h-handle) THEN
                RUN esapi/esapi003a.p PERSISTENT SET h-handle.
        
        
            RUN pi-valida-estorno (INPUT tt-det-requis.rg-item,
                                   OUTPUT TABLE tt-erro-api).
            IF lGerarLog THEN
                MESSAGE "pi-valida-estorno " + RETURN-VALUE.
        
            IF CAN-FIND(FIRST tt-erro-api
                        WHERE tt-erro-api.tipo <> 0) THEN DO:
                RUN pi-desmarca-rg.
                RETURN "NOK".
            END.
        
            RUN pi-inicializa-bo IN h-handle.
        
            RUN pi-estorna-requisicao IN h-handle(INPUT tt-det-requis.nr-requisicao,
                                                  INPUT tt-det-requis.rg-item,
                                                  INPUT tt-det-requis.cod-pto-controle,
                                                  INPUT tt-det-requis.cod-usuario,
                                                  OUTPUT TABLE tt-erro-api).
            IF lGerarLog THEN
                MESSAGE "pi-estorna-requis " + RETURN-VALUE.
        
            RUN pi-finaliza-bo IN h-handle.
            IF h-handle:PERSISTENT THEN DELETE PROCEDURE h-handle.
        
            IF CAN-FIND(FIRST tt-erro-api
                        WHERE tt-erro-api.tipo = 1) THEN DO:
                IF lGerarlog THEN DO:
                    FOR EACH tt-erro-api.
                        MESSAGE tt-erro-api.mensagem.
                    END.
                END.
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo  = 1
                       tt-erro-api.mensagem =  "N�o foi poss�vel devolver a requisi��o".
                RUN pi-desmarca-rg.
            END.
            ELSE DO:
                RUN pi-desmarca-rg.
            END.
        END.
    END.
            
/*     END. /*atualiza:*/ */

END PROCEDURE.




PROCEDURE pi-valida-rg-item:

    /*-------Recebendo parametros-------*/
    DEFINE INPUT-OUTPUT PARAMETER TABLE FOR tt-codigo.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.
        
/*     RUN pi-valida-rg-item (INPUT p-rg-item,      */
/*                            OUTPUT TABLE tt-erro-api) */
    /*** rq ***/
    FIND FIRST tt-codigo EXCLUSIVE-LOCK NO-ERROR.
    IF NOT AVAILABLE tt-codigo THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1 
               tt-erro-api.mensagem = "Nao foi passado dados para requisicao".

        RETURN "NOK".
    END.

    IF  tt-codigo.codigo BEGINS "RC" THEN DO:
        FOR EACH dc-rack-itens
            WHERE dc-rack-itens.nr-rack = SUBSTRING(tt-codigo.codigo,3,13) 
            NO-LOCK,
            FIRST dc-rg-item
            WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
            NO-LOCK,
            FIRST dc-rack
            WHERE dc-rack.nr-rack = dc-rack-itens.nr-rack
            NO-LOCK.

            FIND LAST dc-requisicao
                WHERE dc-requisicao.rg-item = dc-rg-item.rg-item
                NO-LOCK NO-ERROR.
            IF AVAILABLE dc-requisicao AND 
                NOT dc-requisicao.ind-devolvido THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "RG Item ja foi Requisitado.".
        
                RETURN "NOK".
            END.

            IF dc-rg-item.situacao <> 2 THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " nao esta ativo!".
        
                RETURN "NOK".
            END.

            RUN pi-valida.
            IF RETURN-VALUE = "NOK" THEN
                RETURN RETURN-VALUE.
            ELSE
                ASSIGN tt-codigo.descricao = STRING(dc-rack.desc-rack,"x(30)").
        END.
    END.
    ELSE DO:
        FIND dc-rg-item NO-LOCK
            WHERE dc-rg-item.rg-item = TRIM(tt-codigo.codigo) NO-ERROR.
        IF NOT AVAILABLE dc-rg-item THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + tt-codigo.codigo + " Nao Cadastrado!".
    
            RETURN "NOK".
        END.

        FIND LAST dc-requisicao
            WHERE dc-requisicao.rg-item = dc-rg-item.rg-item
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-requisicao AND 
            NOT dc-requisicao.ind-devolvido THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "RG Item ja foi Requisitado.".
    
            RETURN "NOK".
        END.

        IF dc-rg-item.situacao <> 2 THEN
        DO:
    
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " nao esta ativo!".
    
            RETURN "NOK".
    
        END.

        FIND FIRST dc-rack-itens NO-LOCK
            WHERE dc-rack-itens.rg-item = dc-rg-item.rg-item NO-ERROR.
        IF AVAILABLE dc-rack-itens THEN
        DO:
    
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " encontra-se no Rack" + string(dc-rack-itens.nr-rack,"RC99999999999").
    
            RETURN "NOK".
    
        END.

        RUN pi-valida.
        IF RETURN-VALUE = "NOK" THEN
            RETURN RETURN-VALUE.
        ELSE
            ASSIGN tt-codigo.descricao = ITEM.desc-item.
    END.

    RETURN "OK":U.
END PROCEDURE.
    

PROCEDURE pi-valida-estorno:

    /*-------Recebendo parametros-------*/
    DEFINE INPUT PARAMETER p-rg-item AS CHARACTER.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

/*     RUN pi-valida-rg-item (INPUT p-rg-item,      */
/*                            OUTPUT TABLE tt-erro-api) */

    IF p-rg-item BEGINS "RC" THEN DO:
        FOR EACH dc-rack-itens
            WHERE dc-rack-itens.nr-rack = SUBSTRING(p-rg-item ,3,13) 
            NO-LOCK,
            FIRST dc-rg-item
            WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
            NO-LOCK,
            FIRST dc-rack
            WHERE dc-rack.nr-rack = dc-rack-itens.nr-rack
            NO-LOCK.

            FIND LAST dc-requisicao
                WHERE dc-requisicao.rg-item = dc-rg-item.rg-item
                NO-LOCK NO-ERROR.
            IF AVAILABLE dc-requisicao AND 
                dc-requisicao.ind-devolvido THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "RG Item ja foi Devolvido.".
        
                RETURN "NOK".
            END.

            IF dc-rg-item.situacao <> 4 THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " esta ativo!".
        
                RETURN "NOK".
            END.            

            RUN pi-valida.
            IF RETURN-VALUE = "NOK" THEN
                RETURN RETURN-VALUE.
        END.
    END.
    ELSE DO:
        FIND dc-rg-item NO-LOCK
            WHERE dc-rg-item.rg-item = p-rg-item NO-ERROR.
        IF NOT AVAILABLE dc-rg-item THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + tt-codigo.codigo + " Nao Cadastrado!".
    
            RETURN "NOK".
        
        END.

        FIND LAST dc-requisicao
            WHERE dc-requisicao.rg-item = dc-rg-item.rg-item
            NO-LOCK NO-ERROR.
        IF AVAILABLE dc-requisicao AND 
            dc-requisicao.ind-devolvido THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "RG Item ja foi Devolvido.".
    
            RETURN "NOK".
        END.

        IF dc-rg-item.situacao <> 4 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " esta ativo!".
    
            RETURN "NOK".
        END.

        FIND FIRST dc-rack-itens NO-LOCK
            WHERE dc-rack-itens.rg-item = dc-rg-item.rg-item NO-ERROR.
        IF AVAILABLE dc-rack-itens THEN
        DO:
    
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1 
                   tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " encontra-se no Rack" + string(dc-rack-itens.nr-rack,"RC99999999999").
    
            RETURN "NOK".
    
        END.
        
        RUN pi-valida.
        IF RETURN-VALUE = "NOK" THEN
            RETURN RETURN-VALUE.
    END.

    RETURN "OK":U.

END PROCEDURE.


/**
*  validacoes que sao comum
**/
PROCEDURE pi-valida.

    FIND ITEM NO-LOCK 
        WHERE ITEM.it-codigo = dc-rg-item.it-codigo NO-ERROR.
    IF NOT AVAILABLE ITEM THEN
    DO:

        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1 
               tt-erro-api.mensagem = "Item do Rg Item " + dc-rg-item.rg-item + " Nao Cadastrado!".

        RETURN "NOK".

    END.

    IF ITEM.cod-obsoleto > 1 THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1 
               tt-erro-api.mensagem = "Item obsoleto, nao pode ser movimentado".

        RETURN "NOK".
    END.

    /*Falta ainda verificar se o item e acabado ou semi acabado*/
    IF dc-rg-item.nr-ord-prod = 0 /*OR 
        dc-rg-item.nr-prod-produ = 0*/ AND 
        NOT CAN-FIND(FIRST dc-movto-pto-controle
                     WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item 
                       AND dc-movto-pto-controle.cod-pto-controle BEGINS "RE") THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo = 1
               tt-erro-api.mensagem = "RG Item nao reportado: " + dc-rg-item.rg-item.
        RETURN "NOK".
    END.
END.

PROCEDURE pi-devolve-ccusto:

    /*-------Recebendo parametros-------*/
    DEFINE INPUT PARAMETER  p-cod-usuario LIKE usuar-mater.cod-usuario.
    DEFINE OUTPUT PARAMETER p-ccusto      LIKE dc-requisicao.ccusto.
    DEFINE OUTPUT PARAMETER p-desc-ccusto AS CHARACTER.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    FIND usuar-mater NO-LOCK
        WHERE usuar-mater.cod-usuario = p-cod-usuario NO-ERROR.
    IF AVAILABLE usuar-mater THEN
    DO:
        FIND centro-custo NO-LOCK
            WHERE centro-custo.cc-codigo = usuar-mater.sc-codigo NO-ERROR.
        IF AVAILABLE centro-custo THEN
        DO:

            ASSIGN p-ccusto      = centro-custo.cc-codigo
                   p-desc-ccusto = centro-custo.descricao.

        END.
        ELSE
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo = 1
                   tt-erro-api.mensagem = "Centro de Custo nao encontrado".
        END.

        /*
        FIND usuar-ccusto NO-LOCK
                 WHERE usuar-ccusto.sc-codigo   = usuar-mater.sc-codigo
                   AND usuar-ccusto.cod-usuario = usuar-mater.cod-usuario NO-ERROR.
        IF AVAIL usuar-ccusto THEN
        DO:

                FIND centro-custo NO-LOCK
                    WHERE centro-custo.cc-codigo = usuar-ccusto.sc-codigo NO-ERROR.
                IF AVAIL centro-custo THEN
                DO:

                    ASSIGN p-ccusto      = centro-custo.cc-codigo
                           p-desc-ccusto = centro-custo.descricao.

                END.
                ELSE
                DO:
                    CREATE tt-erro-api.
                    ASSIGN tt-erro-api.tipo     = 1 
                           tt-erro-api.mensagem = "Centro de Custo " + usuar-ccusto.sc-codigo + " nao cadastrado!".

                    RETURN "NOK".

                END.
            END.
            ELSE
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1 
                       tt-erro-api.mensagem = "Usuario " + p-cod-usuario + " nao cadastrado!".
        
                RETURN "NOK".
                
            END.
        END.
        ELSE
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1
                   tt-erro-api.mensagem = "Usuario " + p-cod-usuario + " nao cadastrado!".

            RETURN "NOK".

        END.
        */
    END.
    ELSE
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo = 1
               tt-erro-api.mensagem = "Usuario nao cadastrado nos parametros materiais".
    END.

END PROCEDURE.



    
PROCEDURE pi-busca-estorno:
    DEFINE INPUT PARAMETER p-rg-item AS CHARACTER FORMAT "x(15)".
    DEFINE INPUT PARAMETER p-cod-pto-controle LIKE dc-pto-controle.cod-pto-controle.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-det-requis.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /* main */
    ASSIGN lGerarLog = FALSE.
    IF lGerarLog THEN
        MESSAGE "pi-busca-estorno".
    RUN pi-valida-estorno(INPUT p-rg-item,
                          OUTPUT TABLE tt-erro-api).
    IF RETURN-VALUE = "NOK" THEN
        RETURN RETURN-VALUE.

    FIND dc-pto-controle 
        WHERE dc-pto-controle.cod-pto-controle = p-cod-pto-controle
        NO-LOCK NO-ERROR.

    IF p-rg-item BEGINS "RC" THEN DO:
        FOR FIRST dc-rack-itens
            WHERE dc-rack-itens.nr-rack = SUBSTRING(p-rg-item,3,13)
            NO-LOCK,
            FIRST dc-rg-item
            WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
            NO-LOCK.

            FOR LAST it-requisicao NO-LOCK                                               
                WHERE it-requisicao.it-codigo = dc-rg-item.it-codigo
                  AND it-requisicao.situacao = 2 /*Fechada*/,
                FIRST requisicao OF it-requisicao NO-LOCK,
                FIRST ITEM
                WHERE ITEM.it-codigo = it-requisicao.it-codigo NO-LOCK :

                CREATE tt-det-requis.
                ASSIGN tt-det-requis.nr-requisicao   = requisicao.nr-requisicao
                       tt-det-requis.rg-item         = dc-rg-item.rg-item
                       tt-det-requis.it-codigo       = it-requisicao.it-codigo
                       tt-det-requis.desc-item       = ITEM.desc-item
                       tt-det-requis.cod-depos-dest  = dc-pto-controle.cod-depos-dest
                       tt-det-requis.cod-localiz-dest = (IF dc-pto-controle.utiliz-loc-pad-item THEN ITEM.cod-localiz ELSE dc-pto-controle.cod-localiz-dest)
                       tt-det-requis.cod-depos-orig   = dc-rg-item.cod-depos-orig
                       tt-det-requis.cod-localiz-orig = dc-rg-item.cod-localiz-orig.
            END.
        END.
    END.
    ELSE DO:
        FOR FIRST dc-rg-item
            WHERE dc-rg-item.rg-item = p-rg-item
            NO-LOCK.

            FOR LAST it-requisicao NO-LOCK                                               
                WHERE it-requisicao.it-codigo = dc-rg-item.it-codigo
                  AND it-requisicao.situacao = 2 /*Fechada*/,
                FIRST requisicao OF it-requisicao NO-LOCK,
                FIRST ITEM
                WHERE ITEM.it-codigo = it-requisicao.it-codigo NO-LOCK :

                CREATE tt-det-requis.
                ASSIGN tt-det-requis.nr-requisicao   = requisicao.nr-requisicao
                       tt-det-requis.rg-item         = dc-rg-item.rg-item
                       tt-det-requis.it-codigo       = it-requisicao.it-codigo
                       tt-det-requis.desc-item       = ITEM.desc-item
                       tt-det-requis.cod-depos-dest  = dc-pto-controle.cod-depos-dest
                       tt-det-requis.cod-localiz-dest = (IF dc-pto-controle.utiliz-loc-pad-item THEN ITEM.cod-localiz ELSE dc-pto-controle.cod-localiz-dest)
                       tt-det-requis.cod-depos-orig   = dc-rg-item.cod-depos-orig
                       tt-det-requis.cod-localiz-orig = dc-rg-item.cod-localiz-orig
                    .
            END.
        END.
    END.
        

/*     IF AVAIL dc-rg-item THEN                                                         */
/*     DO:                                                                              */
/*         RUN pi-valida-estorno (INPUT p-rg-item,                                      */
/*                                OUTPUT TABLE tt-erro-api).                            */
/*         IF CAN-FIND(FIRST tt-erro-api                                                */
/*                     WHERE tt-erro-api.tipo <> 0) THEN                                */
/*             RETURN "NOK".                                                            */
/*                                                                                      */
/*         FOR LAST it-requisicao NO-LOCK                                               */
/*             WHERE it-requisicao.it-codigo = dc-rg-item.it-codigo                     */
/*               AND it-requisicao.situacao = 2 /*Fechada*/,                            */
/*             FIRST requisicao OF it-requisicao NO-LOCK,                               */
/*             FIRST ITEM                                                               */
/*             WHERE ITEM.it-codigo = it-requisicao.it-codigo NO-LOCK :                 */
/*                                                                                      */
/*             CREATE tt-det-requis.                                                    */
/*             ASSIGN /*tt-det-requis.nro-docto       = requisicao.nro-docto*/          */
/*                    tt-det-requis.nr-requisicao   = requisicao.nr-requisicao          */
/*                    tt-det-requis.rg-item         = dc-rg-item.rg-item                */
/*                    tt-det-requis.it-codigo       = it-requisicao.it-codigo           */
/*                    tt-det-requis.desc-item       = ITEM.desc-item                    */
/*                    tt-det-requis.cod-depos-dest  = dc-pto-controle.cod-depos-dest    */
/*                    tt-det-requis.cod-localiz-dest = dc-pto-controle.cod-localiz-dest */
/*                    tt-det-requis.cod-depos-orig   = dc-pto-controle.cod-depos-orig   */
/*                    tt-det-requis.cod-localiz-orig = dc-pto-controle.cod-localiz-orig */
/*                    /*tt-det-requis.cod-usuario      = "asdf"                         */
/*                    tt-det-requis.cod-senha        = "asdf"*/.                        */
/*         END.                                                                         */
/*     END.                                                                             */
/*     ELSE                                                                             */
/*     DO:                                                                              */
/*         CREATE tt-erro-api.                                                          */
/*         ASSIGN tt-erro-api.tipo     = 1                                              */
/*                tt-erro-api.mensagem = "Rg Item " + p-rg-item + " n�o cadastrado!".   */
/*         RETURN "NOK".                                                                */
/*     END.                                                                             */

    RETURN "OK".

END PROCEDURE.

