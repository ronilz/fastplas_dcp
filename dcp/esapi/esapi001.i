/******************************************************************************
    Objetivo: Metodos                              
    Programa: ESAPI001.i       
    Data    : 12/03/2007
    Autor   : 
    Altercao: 
    Vers�o  : V.2.04.00.001
    
    Documentacao Metodo:
    -> pi-valida-acesso  - 
    -> pi-valida-usuario - 
    
*******************************************************************************/

PROCEDURE pi-valida-acesso:

    /*-------Recebendo parametros-------*/
    DEF INPUT PARAM cod-usuario AS CHAR.
    DEF INPUT PARAM cod-programa AS CHAR.
    DEF OUTPUT PARAMETER TABLE FOR tt-erro-api.
    
    FIND usuar-mater 
        WHERE usuar-mater.cod-usuario = cod-usuario 
        NO-LOCK NO-ERROR.
    
    IF NOT AVAIL usuar-mater THEN DO:
       CREATE tt-erro-api.
       ASSIGN tt-erro-api.tipo = 1
              tt-erro-api.mensagem = "Usuario inexistente.".
    END.
    
    FIND dc-programa WHERE dc-programa.cod-programa = cod-programa NO-LOCK NO-ERROR.
    
    IF NOT AVAIL dc-programa THEN DO:
       CREATE tt-erro-api.
       ASSIGN tt-erro-api.tipo = 1
              tt-erro-api.mensagem = "Programa inexistente.".
    END.
    
    FIND dc-acesso WHERE dc-acesso.cod-usuario = cod-usuario
                     AND dc-acesso.cod-programa = cod-programa NO-LOCK NO-ERROR.
    
    IF (NOT can-find(FIRST tt-erro-api)) AND (NOT AVAIL dc-acesso) THEN DO:
       CREATE tt-erro-api.
       ASSIGN tt-erro-api.tipo = 1
              tt-erro-api.mensagem = "Usuario '" + cod-usuario + "' nao tem permissao de acesso ao programa '" + cod-programa + "'".
    END.
    
    IF NOT can-find(FIRST tt-erro-api) THEN DO:
       CREATE tt-erro-api.
       ASSIGN tt-erro-api.tipo = 0.
    END.                                       

END PROCEDURE.

PROCEDURE pi-valida-usuario.

    /*-------Recebendo parametros-------*/
    DEF INPUT PARAM cod-usuario like usuar_mestre.cod_usuario.
    DEF INPUT PARAM cod-senha   like usuar_mestre.cod_senha.
    DEF OUTPUT PARAMETER TABLE FOR tt-erro-api.

    DEFINE VARIABLE i-seq AS INTEGER    NO-UNDO.
      
    RUN btb/btapi910za.p (input cod-usuario, input cod-senha, output table tt-erros).          
       
    if RETURN-VALUE = 'nok' THEN
    DO:                                                                                        
        FIND FIRST tt-erros NO-LOCK NO-ERROR.                                                  
        CREATE tt-erro-api.                                                                    
        ASSIGN tt-erro-api.tipo = 1                                                            
               tt-erro-api.mensagem = "Usuario sem permissao ou ja logado em outra sessao".
                                                                                               
        IF AVAIL tt-erros THEN                                                                 
            ASSIGN tt-erro-api.mensagem = tt-erro-api.mensagem + ". " + tt-erros.desc-erro.    
                                                                                               
        RETURN "NOK".
    END.  
    
    
    FIND FIRST tt-erros NO-LOCK NO-ERROR.
    if avail tt-erros then
    do:                                                  
        CREATE tt-erro-api.                                                                    
        ASSIGN tt-erro-api.tipo = 1                                                            
               tt-erro-api.mensagem = tt-erro-api.mensagem + ". " + tt-erros.desc-erro.
               
        RETURN "NOK".       
    end.   
    
    find first tt-erro-api no-lock no-error.
    if avail tt-erro-api then
       return "nok".
                
    /**
    *  Analisando a sequence dos movimentos
    **/
    i-seq = CURRENT-VALUE(seq-dc-movto-pto-controle).

    FIND LAST dc-movto-pto-controle NO-LOCK NO-ERROR.
    IF AVAILABLE dc-movto-pto-controle AND
        dc-movto-pto-controle.nr-seq > i-seq THEN
        ASSIGN CURRENT-VALUE(seq-dc-movto-pto-controle) = dc-movto-pto-controle.nr-seq + 5.
        
    i-seq = CURRENT-VALUE(seq-reporte).

    FIND LAST dc-reporte NO-LOCK NO-ERROR.
    IF AVAILABLE dc-reporte AND dc-reporte.nro-docto > i-seq THEN
        ASSIGN CURRENT-VALUE(seq-reporte) = dc-reporte.nro-docto + 5.

END PROCEDURE.


PROCEDURE pi-devolve-ponto-controle.
    DEF INPUT PARAM p-cod-pto-controle AS CHAR.
    DEF OUTPUT PARAM TABLE FOR tt-pto-controle.
    DEF OUTPUT PARAMETER TABLE FOR tt-erro-api.

    FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = p-cod-pto-controle
        NO-LOCK NO-ERROR.
    IF NOT AVAIL dc-pto-controle THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo = 1
               tt-erro-api.mensagem = "Codigo do Tipo Controle nao encontrado".
        RETURN "NOK":U.
    END.

    CREATE tt-pto-controle.
    ASSIGN tt-pto-controle.cod-pto-controle  = dc-pto-controle.cod-pto-controle
           tt-pto-controle.tipo-pto          = dc-pto-controle.tipo-pto
           tt-pto-controle.local-pto         = dc-pto-controle.local-pto
           tt-pto-controle.desc-pto-controle = dc-pto-controle.desc-pto-controle
           tt-pto-controle.cod-depos-orig    = dc-pto-controle.cod-depos-orig  
           tt-pto-controle.cod-localiz-orig  = dc-pto-controle.cod-localiz-orig
           tt-pto-controle.cod-depos-dest    = dc-pto-controle.cod-depos-dest  
           tt-pto-controle.cod-localiz-dest  = dc-pto-controle.cod-localiz-dest.

    CASE dc-pto-controle.tipo-pto:            
        WHEN 1 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Normal".
        WHEN 2 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Juncao".
        WHEN 3 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Pintura".
        WHEN 4 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Re-Trabalho".
        WHEN 5 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Expedicao".
        WHEN 6 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Sucata".
        WHEN 7 THEN ASSIGN tt-pto-controle.desc-tipo-pto = "Impressao".
    END.

    RETURN "OK":U.

END PROCEDURE.




PROCEDURE pi-menu:

    DEFINE INPUT PARAMETER p-usuario AS CHARACTER.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-menu.

    FOR EACH dc-acesso NO-LOCK
        WHERE dc-acesso.cod-usuario = p-usuario:

        FIND dc-programa NO-LOCK
            WHERE dc-programa.cod-programa = dc-acesso.cod-programa NO-ERROR.

        CREATE tt-menu.
        BUFFER-COPY dc-acesso TO tt-menu.
        ASSIGN tt-menu.descricao = IF AVAIL dc-programa THEN dc-programa.descricao ELSE "".

        IF dc-programa.MENU THEN
            ASSIGN tt-menu.ind-menu = YES.
        ELSE
            ASSIGN tt-menu.ind-menu = NO.
    END.
END PROCEDURE.

