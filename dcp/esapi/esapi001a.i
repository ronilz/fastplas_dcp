/***
*    Include de logout do ERP
****/

/************************* Variable Definition Begin ************************
def new global shared var v2_cod_usuar_corren
    as character
    format "x(12)"
    label "Usu�rio Corrente"
    column-label "Usu�rio Corrente"
    no-undo.
def new global shared var v2_cod_empres_usuar
    as character
    format "x(3)"
    label "Empresa"
    column-label "Empresa"
    no-undo.
def new global shared var v2_cod_estab_usuar
    as character
    format "x(3)"
    label "Estabelecimento"
    column-label "Estab"
    no-undo.
def new global shared var v2_cod_idiom_usuar
    as character
    format "x(8)"
    label "Idioma"
    column-label "Idioma"
    no-undo.
def new global shared var v2_cod_pais_empres_usuar
    as character
    format "x(3)"
    label "Pa�s Empresa Usu�rio"
    column-label "Pa�s"
    no-undo.
def new global shared var v2_cod_usuar_corren_criptog
    as character
    format "x(16)"
    no-undo.
def new global shared var v5_cod_usuar_corren
    as character
    format "x(12)"
    label "Usu�rio Corrente"
    column-label "Usu�rio Corrente"
    no-undo.
def new global shared var v5_cod_empres_usuar
    as character
    format "x(3)"
    label "Empresa"
    column-label "Empresa"
    no-undo.
def new global shared var v5_cod_estab_usuar
    as character
    format "x(3)"
    label "Estabelecimento"
    column-label "Estab"
    no-undo.
def new global shared var v5_cod_idiom_usuar
    as character
    format "x(8)"
    label "Idioma"
    column-label "Idioma"
    no-undo.
def new global shared var v5_cod_pais_empres_usuar
    as character
    format "x(3)"
    label "Pa�s Empresa Usu�rio"
    column-label "Pa�s"
    no-undo.
def new global shared var v5_cod_usuar_corren_criptog
    as character
    format "x(16)"
    no-undo.

def new global shared var v_cod_aplicat_dtsul_corren
    as character
    format "x(3)"
    no-undo.
def new global shared var v_cod_ccusto_corren
    as character
    format "x(11)"
    label "Centro Custo"
    column-label "Centro Custo"
    no-undo.
def new global shared var v_cod_dwb_user
    as character
    format "x(12)"
    label "Usu�rio"
    column-label "Usu�rio"
    no-undo.
def new global shared var v_cod_empres_usuar
    as character
    format "x(3)"
    label "Empresa"
    column-label "Empresa"
    no-undo.
def new global shared var v_cod_estab_usuar
    as character
    format "x(3)"
    label "Estabelecimento"
    column-label "Estab"
    no-undo.
def new global shared var v_cod_funcao_negoc_empres
    as character
    format "x(50)"
    no-undo.
def new global shared var v_cod_grp_usuar_lst
    as character
    format "x(3)"
    label "Grupo Usu�rios"
    column-label "Grupo"
    no-undo.
def new global shared var v_cod_idiom_usuar
    as character
    format "x(8)"
    label "Idioma"
    column-label "Idioma"
    no-undo.
def new global shared var v_cod_modul_dtsul_corren
    as character
    format "x(3)"
    label "M�dulo Corrente"
    column-label "M�dulo Corrente"
    no-undo.
def new global shared var v_cod_modul_dtsul_empres
    as character
    format "x(100)"
    no-undo.
def new global shared var v_cod_pais_empres_usuar
    as character
    format "x(3)"
    label "Pa�s Empresa Usu�rio"
    column-label "Pa�s"
    no-undo.
def new global shared var v_cod_plano_ccusto_corren
    as character
    format "x(8)"
    label "Plano CCusto"
    column-label "Plano CCusto"
    no-undo.
def new global shared var v_cod_unid_negoc_usuar
    as character
    format "x(3)"
    view-as combo-box
    list-items ""
    inner-lines 5
    bgcolor 15 font 2
    label "Unidade Neg�cio"
    column-label "Unid Neg�cio"
    no-undo.
def new global shared var v_cod_usuar_corren
    as character
    format "x(12)"
    label "Usu�rio Corrente"
    column-label "Usu�rio Corrente"
    no-undo.
def new global shared var v_cod_usuar_corren_criptog
    as character
    format "x(16)"
    no-undo.
def var v_des_logout
    as character
    format "x(40)"
    no-undo.
def var v_nom_title_aux
    as character
    format "x(60)"
    no-undo.
def new global shared var v_num_ped_exec_corren
    as integer
    format ">>>>9"
    no-undo.
def var v_rec_log
    as recid
    format ">>>>>>9"
    no-undo.
define new global shared var l-autconlist as logical no-undo.
def new global shared var c-usuar-time-out as char no-undo.    
{utp/ut-glob.i}    

***********************************************************************************************/
/* PROCEDURE logoutColetor. */

    DEFINE VARIABLE l-ok AS LOGICAL    NO-UNDO.
    DEFINE VARIABLE i-cont AS INTEGER    NO-UNDO.

    ASSIGN l-ok = FALSE.
    DO  i-cont = 1 TO NUM-DBS.
        RUN esapi/esapi001a1.p (INPUT LDBNAME(i-cont),
                                OUTPUT l-ok).
    END.

    /**
    *  nao esta tratando erros
    **/
/*     IF NOT l-ok THEN */
/*     DO:              */
/*                      */
/*     END.             */

    assign v_cod_usuar_corren           = ""
           v_cod_empres_usuar           = ""
           v_cod_estab_usuar            = ""
           v_cod_idiom_usuar            = ""
           v_cod_modul_dtsul_corren     = ""
           v_cod_unid_negoc_usuar       = ""
           v_des_logout                 = ""
           c-seg-usuario                = ""
           v2_cod_usuar_corren          = v_cod_usuar_corren
           v2_cod_empres_usuar          = v_cod_empres_usuar
           v2_cod_estab_usuar           = v_cod_estab_usuar
           v2_cod_idiom_usuar           = v_cod_idiom_usuar
           v2_cod_pais_empres_usuar     = v_cod_pais_empres_usuar
           v2_cod_usuar_corren_criptog  = v_cod_usuar_corren_criptog
           v5_cod_usuar_corren          = v_cod_usuar_corren
           v5_cod_empres_usuar          = v_cod_empres_usuar
           v5_cod_estab_usuar           = v_cod_estab_usuar
           v5_cod_idiom_usuar           = v_cod_idiom_usuar
           v5_cod_pais_empres_usuar     = v_cod_pais_empres_usuar
           v5_cod_usuar_corren_criptog  = v_cod_usuar_corren_criptog
           c-usuar-time-out             = "".

/* END PROCEDURE. */
