/******************************************************************************
    Objetivo: Metodos                              
    Programa: ESAPI003         
    Data    : 10/03/2007
    Autor   : 
    Altercao: 
    Vers�o  : V.2.04.00.001
    
    Documentacao Metodo:
    -> pi-ativa-rg          - M�todo que altera a situa��o do RG e gera hist�rico de movimenta��o
*******************************************************************************/
{include\ttdef1.i}
{include\ttdef2.i}
{esapi/esapi001c.i}
    
PROCEDURE pi-ativa-rg:
    /*---------Parametros Input/Output----------*/
    DEFINE INPUT PARAMETER p-controle LIKE dc-pto-controle.cod-pto-controle.
    DEFINE INPUT PARAMETER p-rg-item  LIKE dc-rg-item.rg-item.
    DEFINE OUTPUT PARAM TABLE FOR tt-erro-api.


    /*---------Defini��o de variaveis----------*/
    DEFINE VARIABLE i-seq-movto AS INTEGER    NO-UNDO.
    DEFINE BUFFER b-dc-movto-pto-controle FOR dc-movto-pto-controle.


    /*---------Logica Principal----------*/
    FIND dc-rg-item 
        WHERE dc-rg-item.rg-item = p-rg-item 
        NO-ERROR NO-WAIT.
    if LOCKED(dc-rg-item) THEN do:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo = 1
               tt-erro-api.mensagem = "RG Item em uso: " + p-rg-item.
        RETURN "NOK":U.                   
    END.
    
    IF AVAIL dc-rg-item THEN
    DO:
        IF dc-rg-item.situacao <> 1 THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 1
                   tt-erro-api.mensagem = "RG Item j� est� ativado".
            RETURN "NOK".
        END.

        FIND dc-pto-controle 
            WHERE dc-pto-controle.cod-pto-controle = p-controle 
            NO-LOCK NO-ERROR.
        IF AVAIL dc-pto-controle THEN
        DO:
            IF dc-pto-controle.tipo-pto <> 1 THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo     = 1
                       tt-erro-api.mensagem = "Tipo de Controle Invalido.".
                RETURN "NOK".
            END.

            /*
            IF dc-rg-item.cod-depos-orig <> dc-pto-controle.cod-depos-orig OR
                dc-rg-item.cod-localiz-orig <> dc-pto-controle.cod-localiz-orig THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo = 1
                       tt-erro-api.mensagem = "RG Item n�o se encontra no local do ponto de controle.".
                RETURN "NOK":U.
            END.
            */

/*             FIND FIRST dc-cor NO-LOCK WHERE dc-cor.it-codigo = dc-rg-item.it-codigo NO-ERROR. */

            ASSIGN dc-rg-item.situacao          = 2
                   dc-rg-item.cod-pto-controle  = dc-pto-controle.cod-pto-controle
                   dc-rg-item.local-pto         = dc-pto-controle.local-pto
                   dc-rg-item.tipo-pto          = dc-pto-controle.tipo-pto
                   dc-rg-item.cod-depos-orig    = dc-pto-controle.cod-depos-dest   
                   dc-rg-item.cod-localiz-orig  = dc-pto-controle.cod-localiz-dest.

            ASSIGN i-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).
            FIND LAST b-dc-movto-pto-controle USE-INDEX idx_movto_pto_controle NO-LOCK 
                WHERE b-dc-movto-pto-controle.rg-item = dc-rg-item.rg-item NO-ERROR.

            CREATE dc-movto-pto-controle.
            ASSIGN dc-movto-pto-controle.cod-cor              = IF AVAIL dc-cor THEN dc-cor.cod-cor ELSE ""
                   dc-movto-pto-controle.cod-depos-dest       = dc-rg-item.cod-depos-orig  
                   dc-movto-pto-controle.cod-depos-orig       = dc-rg-item.cod-depos-orig
                   dc-movto-pto-controle.cod-localiz-dest     = dc-rg-item.cod-localiz-orig  
                   dc-movto-pto-controle.cod-localiz-orig     = dc-rg-item.cod-localiz-orig
                   dc-movto-pto-controle.cod-pto-controle     = dc-pto-controle.cod-pto-controle
                   dc-movto-pto-controle.cod-pto-controle-ant = IF AVAIL b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.cod-pto-controle ELSE ""
                   dc-movto-pto-controle.it-codigo            = dc-rg-item.it-codigo
                   dc-movto-pto-controle.it-codigo-ant        = ""
                   dc-movto-pto-controle.local-pto            = dc-pto-controle.local-pto 
                   dc-movto-pto-controle.nr-rack              = ""
                   dc-movto-pto-controle.nr-seq               = i-seq-movto
                   dc-movto-pto-controle.nr-seq-ant           = IF AVAIL b-dc-movto-pto-controle THEN b-dc-movto-pto-controle.nr-seq ELSE 0
                   dc-movto-pto-controle.nro-docto            = 0 /*Verificar Sequencia*/
                   dc-movto-pto-controle.rg-item              = dc-rg-item.rg-item
                   dc-movto-pto-controle.tipo-pto             = dc-pto-controle.tipo-pto
                   dc-movto-pto-controle.cod-usuario          = "usuario"
                   dc-movto-pto-controle.data                 = TODAY
                   dc-movto-pto-controle.hora                 = STRING(TIME,"HH:MM:SS").

            CREATE tt-erro-api.
            ASSIGN tt-erro-api.tipo     = 0
                   tt-erro-api.mensagem = "Ativa��o Realiz. com Sucesso".

            RETURN "OK".
        END.    
        ELSE 
        DO:
           CREATE tt-erro-api.
           ASSIGN tt-erro-api.tipo     = 1
                  tt-erro-api.mensagem = "Ponto de Controle " + p-controle + " n�o cadastrado!".
        END.
    END.
    ELSE 
    DO:
       CREATE tt-erro-api.
       ASSIGN tt-erro-api.tipo     = 1
              tt-erro-api.mensagem = "Rg item " + p-rg-item + " n�o cadastrado!".
    END.

END PROCEDURE.


PROCEDURE pi-devolve-rg:
    DEFINE INPUT PARAMETER p-rg-item AS CHAR.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-consulta.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.


    /* main*/
    FIND dc-rg-item 
        WHERE dc-rg-item.rg-item = p-rg-item 
        NO-LOCK NO-ERROR.
        
    IF AVAIL dc-rg-item THEN
    DO:
        FIND ITEM NO-LOCK
            WHERE ITEM.it-codigo = dc-rg-item.it-codigo NO-ERROR.

        FIND dc-local-pto NO-LOCK
            WHERE dc-local-pto.local-pto  = dc-rg-item.local-pto NO-ERROR.

        CREATE tt-consulta.
        ASSIGN tt-consulta.rg-item           = dc-rg-item.rg-item
               tt-consulta.it-codigo         = dc-rg-item.it-codigo
               tt-consulta.desc-item         = IF AVAIL ITEM THEN ITEM.desc-item ELSE ""
               tt-consulta.nr-rack           = dc-rg-item.nr-rack
               tt-consulta.cod-pto-controle  = dc-rg-item.cod-pto-controle
               tt-consulta.local-pto         = dc-rg-item.local-pto
               tt-consulta.desc-local-pto    = IF AVAIL dc-local-pto THEN dc-local-pto.desc-local ELSE ""
               tt-consulta.tipo-pto          = dc-rg-item.tipo-pto
               tt-consulta.ind-situacao      = dc-rg-item.situacao.
    END.
    ELSE 
    DO:
        CREATE tt-erro-api.
        ASSIGN tt-erro-api.tipo     = 1
               tt-erro-api.mensagem = "Rg Item " + p-rg-item + " n�o cadastrado!".
    END.
END PROCEDURE.
