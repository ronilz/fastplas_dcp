    /* param */
    DEF INPUT PARAM r-pto-controle AS ROWID NO-UNDO.
    DEF INPUT PARAM c-nr-rack AS CHAR NO-UNDO.
    
    /* vars */
    DEF VAR i-seq-movto AS INTEGER NO-UNDO.

    /* main */
    FIND dc-pto-controle
        WHERE ROWID(dc-pto-controle) = r-pto-controle
        NO-LOCK NO-ERROR.
    IF NOT AVAIL dc-pto-controle THEN
        RETURN "NOK":U.

    FIND dc-rack
        WHERE dc-rack.nr-rack = c-nr-rack
        NO-ERROR NO-WAIT.
    if LOCKED (dc-rack) THEN do:
        RETURN "NOK":U.
    END.          
    
    IF NOT AVAIL dc-rack THEN
        RETURN "NOK":U.

    ASSIGN dc-rack.cod-pto-controle = dc-pto-controle.cod-pto-controle
           dc-rack.local-pto = dc-pto-controle.local-pto
           dc-rack.tipo-pto = dc-pto-controle.tipo-pto NO-ERROR.

    ASSIGN i-seq-movto = NEXT-VALUE(seq-dc-movto-rack).
    
    CREATE dc-movto-rack.
    ASSIGN dc-movto-rack.tipo-pto                   = dc-pto-controle.tipo-pto
           dc-movto-rack.nro-docto                  = i-seq-movto
           dc-movto-rack.nr-seq                     = i-seq-movto
           dc-movto-rack.nr-rack                    = c-nr-rack
           dc-movto-rack.local-pto                  = dc-pto-controle.local-pto
           dc-movto-rack.cod-pto-controle-ant       = ""
           dc-movto-rack.cod-pto-controle           = dc-pto-controle.cod-pto-controle.

    RETURN "OK":U.
