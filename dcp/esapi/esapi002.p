/******************************************************************************
    Objetivo: Transferencia de lotes
    Programa: ESAPI001
    Data    : 07/03/2007
    Autor   : Douglas Ferrari - Datasul WA
    Altercao:
    Vers�o  : V.2.04.00.001
    Versao  : V.2.04.00.002 - adaptacao 206
*******************************************************************************/
/* variaveis locais */

/* Include */
/*{cep/ceapi001.i}*/
{cep/ceapi001k.i}
{include/ttdef1.i}
{include/ttdef2.i}
{cdp/cd0666.i}
{esapi\esapi001c.i}
MESSAGE "ESAPI002".
    
DEFINE TEMP-TABLE tt-rg-fifo
    FIELD rg-item   LIKE dc-rg-item.rg-item
    FIELD it-codigo LIKE ITEM.it-codigo
    FIELD nr-rack   LIKE dc-rack.nr-rack
    FIELD nr-seq    AS INTEGER
    FIELD data      AS DATE
    FIELD hora      AS CHARACTER
    INDEX idx-tt-rg-fifo IS UNIQUE rg-item nr-rack.

DEFINE TEMP-TABLE tt-item-fifo
    FIELD it-codigo LIKE ITEM.it-codigo
    FIELD nr-seq    AS INTEGER
    INDEX idx-tt-item-fifo IS UNIQUE it-codigo.

DEFINE TEMP-TABLE tt-ordem-rg-fifo
    FIELD rg-item   LIKE dc-rg-item.rg-item
    FIELD nr-seq    AS INTEGER
    FIELD nr-rack   LIKE dc-rack.nr-rack
    FIELD it-codigo LIKE ITEM.it-codigo.

DEFINE TEMP-TABLE tt-rg-item-2 LIKE tt-rg-item.
DEFINE TEMP-TABLE tt-rg-item-3 LIKE tt-rg-item.

DEFINE TEMP-TABLE tt-qtd-rack
    FIELD nr-rack   AS CHARACTER
    FIELD it-codigo AS CHARACTER
    FIELD qtd       AS DECIMAL.

DEFINE BUFFER b-dc-rack-itens          FOR dc-rack-itens.
DEFINE BUFFER b-dc-rg-item             FOR dc-rg-item.

DEFINE BUFFER b1-dc-movto-pto-controle FOR dc-movto-pto-controle.
DEFINE BUFFER b-dc-movto-rack          FOR dc-movto-rack.
DEFINE BUFFER b1-dc-movto-rack         FOR dc-movto-rack.
DEFINE BUFFER b-dc-transfer            FOR dc-transfer.
DEFINE VARIABLE de-qtd         AS DECIMAL NO-UNDO.
DEFINE VARIABLE i-nr-seq-movto AS INTEGER NO-UNDO.
DEFINE VARIABLE c-tipo-permissao AS CHARACTER NO-UNDO.

/* temp-table */
PROCEDURE pi-transfer-depos:
    /* param */
    DEFINE INPUT  PARAMETER c-cod-pto-controle AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-depos-de     AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-depos-para   AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-localiz-de   AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-cod-localiz-para AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-usuario          AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-usuar-aprov      AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER c-senha-aprov      AS CHARACTER                      NO-UNDO.
    DEFINE INPUT  PARAMETER TABLE FOR tt-rg-item.
    DEFINE OUTPUT PARAMETER l-senha            AS LOG                       NO-UNDO.
    DEFINE OUTPUT PARAMETER TABLE FOR tt-erro-api.

    /* varas */
    DEFINE VARIABLE h-handle            AS HANDLE    NO-UNDO.
    DEFINE VARIABLE i-seq-movto         AS INTEGER   NO-UNDO.
    DEFINE VARIABLE c-rg-item           AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-rg-item-sac       AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-nr-rack           AS CHARACTER NO-UNDO.
    DEFINE VARIABLE l-autoriza-fifo     AS LOG       NO-UNDO.
    DEFINE VARIABLE l-autoriza-sac      AS LOG       NO-UNDO.
    DEFINE VARIABLE l-autoriza-retr     AS LOG       NO-UNDO.
    DEFINE VARIABLE l-erro              AS LOG       NO-UNDO.
    DEFINE VARIABLE c-fifo              AS CHARACTER NO-UNDO.
    DEFINE VARIABLE h-api               AS HANDLE    NO-UNDO.
    DEFINE VARIABLE l-desmonta          AS LOGICAL   NO-UNDO INIT FALSE.
    DEFINE VARIABLE c-cod-pto-ativ      AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-nro-docto         AS CHARACTER NO-UNDO.
    DEFINE VARIABLE d-dt-transf         AS DATE      NO-UNDO.

    DEFINE VARIABLE d-qtd-antes-transf  AS DECIMAL   NO-UNDO.
    DEFINE VARIABLE d-qtd-depois-transf AS DECIMAL   NO-UNDO.

    /** rzo - 206  - 002 **/
    DEFINE VARIABLE i-ult-movto         AS INTEGER   NO-UNDO.
    DEFINE VARIABLE h-ceapi001k         AS HANDLE    NO-UNDO.

    /*** rzo = 30/05/2019 ****/
    DEFINE VARIABLE lReporte            AS LOGICAL   NO-UNDO.


    /* main */
    ASSIGN 
        lGerarLog = TRUE.

    IF lGerarLog THEN
        MESSAGE "pi-transfere " ETIME.

    FIND FIRST param-estoq NO-LOCK NO-ERROR.
    FOR EACH tt-erro-api.  
        DELETE tt-erro-api.  
    END.

    ASSIGN 
        l-senha = NO.

    IF c-usuar-aprov = "" AND
        c-senha-aprov = "" THEN
    DO:

        ASSIGN 
            c-fifo = "".
        RUN pi-fifo (/*INPUT c-cod-pto-controle,*/
            INPUT c-cod-depos-de,
            INPUT c-cod-localiz-de,
            INPUT TABLE tt-rg-item,
            OUTPUT c-fifo).
        IF c-fifo <> "" THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 1
                tt-erro-api.mensagem = "RG fora do FIFO " + c-fifo
                l-erro               = YES
                l-senha              = YES.
        END.
        ELSE
        DO:

            IF lGerarLog THEN
                MESSAGE "TOTVS 20 " ETIME.

            RUN pi-sac (INPUT c-cod-depos-de,
                INPUT c-cod-localiz-de,
                INPUT TABLE tt-rg-item,
                OUTPUT c-fifo).

            IF c-fifo <> "" THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "RG Item abaixo do tempo de cura: " + c-fifo
                    l-erro               = YES
                    l-senha              = YES.
            END.
        END.
    END.
    ELSE
    DO:

        IF lGerarLog THEN
            MESSAGE "TOTVS 30 " ETIME.

        IF NOT VALID-HANDLE(h-handle) THEN
            RUN esapi\esapi001.p PERSISTENT SET h-handle.

        RUN pi-valida-usuario IN h-handle (INPUT c-usuar-aprov,
            INPUT c-senha-aprov,
            OUTPUT TABLE tt-erro-api).

        IF VALID-HANDLE(h-handle) THEN DELETE PROCEDURE h-handle.

        IF CAN-FIND(FIRST tt-erro-api) THEN
        DO:
            ASSIGN 
                l-senha = TRUE.
            RETURN "NOK":U.
        END.
        ELSE
        DO:
            IF lGerarLog THEN 
                MESSAGE "tipo de permissao " c-tipo-permissao.
                
            FIND dc-usuar-mater NO-LOCK
                WHERE dc-usuar-mater.cod-usuario = c-usuar-aprov 
                  NO-ERROR.
            IF NOT AVAILABLE dc-usuar-mater THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Usuario nao possui permissao para aprovar!"
                    l-senha              = TRUE.
                RETURN "NOK":U.
            END.
            ELSE 
            DO:
            
                if c-tipo-permissao = "FIFO" AND NOT dc-usuar-mater.ind-fifo then 
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "Usuario nao possui permissao para aprovar FIFO!"
                        l-senha              = TRUE.
                    RETURN "NOK":U.
                end. 
                
                if c-tipo-permissao = "SAC" AND NOT dc-usuar-mater.ind-sac then 
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "Usuario nao possui permissao para aprovar SAC!"
                        l-senha              = TRUE.
                    RETURN "NOK":U.
                end. 
                
            end.
        END.
    END.

    IF l-senha THEN
    DO:
        IF c-fifo MATCHES ("*fifo*") THEN 
            return "FIFO".
        ELSE 
            RETURN "SAC".
    END.


    /** Logica do Programa **/
    FOR EACH tt-item.   
        DELETE tt-item.   
    END.
    FOR EACH tt-movto.  
        DELETE tt-movto.  
    END.

    ASSIGN 
        l-erro = NO.

    FIND FIRST dc-param NO-LOCK NO-ERROR.
    IF NOT AVAILABLE dc-param THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Parametro Data Collection nao Cadastrado.".
        l-erro = YES.
        RETURN "NOK".
    END.

    FIND usuar-mater NO-LOCK
        WHERE usuar-mater.cod-usuario = c-usuario NO-ERROR.
    IF NOT AVAILABLE usuar-mater THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Usuario: " + c-usuario + " nao Cadastrado.".
        l-erro = YES.
        RETURN "NOK".
    END.

    FIND dc-pto-controle NO-LOCK
        WHERE dc-pto-controle.cod-pto-controle = c-cod-pto-controle NO-ERROR.
    IF NOT AVAILABLE dc-pto-controle THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Ponto de Controle: " + c-cod-pto-controle + " nao Cadastrado."
            l-erro               = YES.
        RETURN "NOK".
    END.

    IF dc-pto-controle.tipo-pto <> 1 THEN
    DO:
        CREATE tt-erro-api.
        ASSIGN 
            tt-erro-api.tipo     = 1
            tt-erro-api.mensagem = "Tipo Peca Deve ser Normal"
            l-erro               = YES.
        RETURN "NOK".
    END.
                    
    IF dc-pto-controle.cod-pto-controle-ativ <> "" THEN
        ASSIGN l-desmonta     = TRUE
            c-cod-pto-ativ = dc-pto-controle.cod-pto-controle-ativ.
    ELSE
        ASSIGN l-desmonta     = FALSE
            c-cod-pto-ativ = "".

    /**
    *  Buscar dados para transferencia
    **/

    FOR EACH tt-rg-item NO-LOCK:

        IF SUBSTRING(tt-rg-item.codigo,1,2) = "RC" THEN
        DO:
            FIND dc-rack WHERE
                dc-rack.nr-rack = SUBSTRING(tt-rg-item.codigo,3,11)
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE dc-rack THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Rack: " + tt-rg-item.codigo + " nao Cadastrado."
                    l-erro               = YES.
                RETURN "NOK".
            END.

            IF dc-rack.situacao <> 1 THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Rack: " + tt-rg-item.codigo + " nao Ativo."
                    l-erro               = YES.
                RETURN "NOK".
            END.

            FOR EACH dc-rack-itens NO-LOCK
                WHERE dc-rack-itens.nr-rack = substring(tt-rg-item.codigo,3,11)
                BREAK BY dc-rack-itens.nr-rack:

                FIND dc-rg-item NO-LOCK
                    WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item NO-ERROR.

                IF AVAILABLE dc-rg-item THEN
                DO:
                    /**** rzo = 30/05/2019 ***/
                    lReporte = FALSE.
                    FOR EACH dc-movto-pto-controle NO-LOCK
                        WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item,
                        FIRST dc-pto-controle NO-LOCK
                        WHERE dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle.

                        IF dc-pto-controle.desc-pto-controle BEGINS "REP" THEN
                            lReporte = TRUE.
                    END.

                    IF  /*NOT CAN-FIND(FIRST dc-movto-pto-controle
                                         WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
                                           AND dc-movto-pto-controle.cod-pto-controle BEGINS "REP")*/
                        NOT lReporte THEN
                    DO:
                        IF (dc-rg-item.nr-ord-prod = 0 OR dc-rg-item.nr-prod-produ = 0) THEN
                        DO:                                                            
                            CREATE tt-erro-api.
                            ASSIGN 
                                tt-erro-api.tipo     = 1
                                tt-erro-api.mensagem = "RG Item nao reportado: " + dc-rg-item.rg-item +
                                                              " no RACK " + STRING(dc-rg-item.nr-rack,"RC99999999999").
                            RETURN "NOK".
                        END.
                    END.                                           
                    /*****************************************************************/

                    CREATE tt-item.
                    ASSIGN 
                        tt-item.it-codigo = dc-rg-item.it-codigo
                        tt-item.rg-item   = dc-rg-item.rg-item
                        tt-item.nr-rack   = dc-rack-itens.nr-rack
                        tt-item.nr-rack-a = tt-item.nr-rack.
                        
                END.
            END.
        END.
        ELSE
        DO:
            FIND dc-rg-item NO-LOCK
                WHERE dc-rg-item.rg-item = tt-rg-item.codigo NO-ERROR.
            IF NOT AVAILABLE dc-rg-item THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "RG Item: " + tt-rg-item.codigo + " nao Cadastrado."
                    l-erro               = YES.
                RETURN "NOK".
            END.

            IF dc-rg-item.situacao <> 2 THEN /*Ativo*/
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " nao tem situacao igual ATIVO!".
                RETURN "NOK".
            END.
                                              
            /*********** rzo = 30/05/2019 ***************/
            lReporte = FALSE.
            FOR EACH dc-movto-pto-controle NO-LOCK
                WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item,
                FIRST dc-pto-controle NO-LOCK
                WHERE dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle.

                IF dc-pto-controle.desc-pto-controle BEGINS "REP" THEN
                    lReporte = TRUE.
            END.
            IF dc-rg-item.nr-ord-prod = 0 OR
                dc-rg-item.nr-prod-produ = 0 AND
                NOT lReporte /*NOT CAN-FIND(FIRST dc-movto-pto-controle
                                 WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
                                   AND dc-movto-pto-controle.cod-pto-controle BEGINS "REP")*/ THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "RG Item nao reportado: " + dc-rg-item.rg-item.
                RETURN "NOK".
            END.
            /*******************************************/
                

            FIND FIRST dc-rack-itens NO-LOCK
                WHERE dc-rack-itens.rg-item  = dc-rg-item.rg-item NO-ERROR.
            IF AVAILABLE dc-rack-itens THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN 
                    tt-erro-api.tipo     = 1
                    tt-erro-api.mensagem = "Rg Item " + dc-rg-item.rg-item + " encontra-se dentro do Rack " + STRING(dc-rack-itens.nr-rack,"RC99999999999").
                RETURN "NOK".
            END.

            /*
            IF dc-rg-item.cod-depos-orig   <> c-cod-depos-de OR
               dc-rg-item.cod-localiz-orig <> c-cod-localiz-de THEN
            DO:
                CREATE tt-erro-api.
                ASSIGN tt-erro-api.tipo = 1
                       tt-erro-api.mensagem = "RG Item nao se encontra no local do ponto de controle: " + dc-rg-item.rg-item +
                                              " no RACK " + STRING(dc-rg-item.nr-rack,"RC99999999999").
                RETURN "NOK".
            END.
            */

            CREATE tt-item.
            ASSIGN 
                tt-item.it-codigo = dc-rg-item.it-codigo
                tt-item.rg-item   = dc-rg-item.rg-item
                tt-item.nr-rack   = ""
                tt-item.nr-rack-a = "".
        END.
    END.

    IF l-erro THEN
        RETURN "NOK".

    bloco:
    DO TRANSACTION ON ERROR UNDO bloco, RETURN "NOK"
        ON STOP UNDO bloco, RETURN "LOCKWAIT": /* AJHJR - 23/02/2010 - tratamento de lock wait timeout */

        /**
        *   Bo de Transferencia
        **/
        FIND dc-pto-controle NO-LOCK
            WHERE dc-pto-controle.cod-pto-controle = c-cod-pto-controle NO-ERROR.
            
        FOR EACH tt-item NO-LOCK
            BREAK BY tt-item.nr-rack
            BY tt-item.it-codigo.

            IF ( FIRST-OF(tt-item.it-codigo)  AND
                tt-item.nr-rack <> "" ) OR
                tt-item.nr-rack = "" THEN
            DO:
                ASSIGN 
                    i-seq-movto = 1.

                FIND ITEM NO-LOCK
                    WHERE ITEM.it-codigo = tt-item.it-codigo NO-ERROR.
                IF NOT AVAILABLE ITEM THEN
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "Item: " + tt-item.it-codigo + " nao Cadastrado."
                        l-erro               = YES.
                    UNDO bloco, RETURN "NOK".
                END.

                /**
                *  Buscar qtd pecas do rack
                **/
                IF tt-item.nr-rack <> "" THEN
                DO:
                    ASSIGN 
                        de-qtd = 0.
                    FOR EACH dc-rack-itens NO-LOCK
                        WHERE dc-rack-itens.nr-rack = tt-item.nr-rack,
                        FIRST dc-rg-item
                        WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
                        AND dc-rg-item.it-codigo = tt-item.it-codigo NO-LOCK.
                        ASSIGN 
                            de-qtd = de-qtd + 1.
                    END.
                    IF de-qtd = 0 THEN
                        ASSIGN de-qtd = 1.
                END.
                ELSE
                    ASSIGN de-qtd = 1.

                IF lGerarLog THEN
                    MESSAGE "pi-transfere2 - ponto controle - item "  tt-item.rg-item  ' qtd '  de-qtd .

                FOR EACH tt-movto. 
                    DELETE tt-movto. 
                END.

                IF lGerarLog THEN
                    MESSAGE tt-item.it-codigo tt-item.rg-item tt-item.nr-rack de-qtd.

                ASSIGN 
                    c-nro-docto = IF tt-item.nr-rack <> "" THEN ("RC" + tt-item.nr-rack) ELSE tt-item.rg-item
                    d-dt-transf = TODAY. 

                /** 002 - unidade de negocio **/
                FIND item-uni-estab
                    WHERE item-uni-estab.it-codigo = ITEM.it-codigo
                    AND item-uni-estab.cod-estabel = dc-param.cod-estabel
                    NO-LOCK NO-ERROR.
                    
                FIND dc-rg-item
                    WHERE dc-rg-item.rg-item = tt-item.rg-item
                    NO-LOCK NO-ERROR.             

                ASSIGN 
                    c-cod-depos-de = (IF AVAILABLE dc-rg-item AND dc-rg-item.cod-depos-orig <> '' THEN dc-rg-item.cod-depos-orig ELSE c-cod-depos-de)
                    c-cod-localiz-de = (IF AVAILABLE dc-rg-item AND dc-rg-item.cod-localiz-orig <> '' THEN dc-rg-item.cod-localiz-orig ELSE c-cod-localiz-de)
                    c-cod-localiz-para = IF dc-pto-controle.utiliz-loc-pad-item THEN item-uni-estab.cod-localiz ELSE c-cod-localiz-para.
                
                /*saida estoque*/
                CREATE tt-movto.
                ASSIGN 
                    tt-movto.Cod-versao-integracao = 1
                    tt-movto.cod-depos             = c-cod-depos-de 
                    tt-movto.Cod-localiz           = c-cod-localiz-de 
                    tt-movto.Cod-estabel           = dc-param.cod-estabel
                    /* tt-movto.conta-contabil        = param-estoq.conta-transf   */

                    tt-movto.ct-codigo             = param-estoq.ct-tr-transf
                    /*  tt-movto.sc-codigo          = SUBSTRING(param-estoq.conta-transf,9,5)*/

                    tt-movto.Dt-trans              = d-dt-transf
                    tt-movto.Esp-docto             = 33 /* TRA */
                    tt-movto.It-codigo             = tt-item.it-codigo
                    tt-movto.Tipo-trans            = 2 /* Saida */
                    tt-movto.Un                    = ITEM.un
                    tt-movto.Usuario               = c-usuario
                    tt-movto.nro-docto             = c-nro-docto
                    tt-movto.quantidade            = de-qtd
                    tt-movto.cod-unid-negoc        = IF AVAILABLE item-uni-estab THEN 
                                                            item-uni-estab.cod-unid-negoc
                                                        ELSE 
                                                            "".
                                                            
                IF lGerarLog THEN 
                    MESSAGE 'origem ' tt-movto.Cod-localiz " - " tt-movto.cod-depos SKIP.

                /*Entrada*/
                CREATE tt-movto.
                ASSIGN 
                    tt-movto.Cod-versao-integracao = 1
                    tt-movto.cod-depos             = c-cod-depos-para
                    tt-movto.cod-localiz           = c-cod-localiz-para
                    tt-movto.Cod-estabel           = dc-param.cod-estabel
                    /* tt-movto.conta-contabil        = param-estoq.conta-transf */
                      
                      

                    tt-movto.ct-codigo             = param-estoq.ct-tr-transf
                    tt-movto.sc-codigo             = param-estoq.sc-tr-transf
                    /*tt-movto.sc-codigo             = SUBSTRING(param-estoq.conta-transf,9,5)*/
                    tt-movto.Dt-trans              = d-dt-transf
                    tt-movto.Esp-docto             = 33 /* TRA */
                    tt-movto.It-codigo             = tt-item.it-codigo
                    tt-movto.Tipo-trans            = 1 /* Entrada */
                    tt-movto.Un                    = ITEM.un
                    tt-movto.Usuario               = c-usuario
                    tt-movto.nro-docto             = c-nro-docto
                    tt-movto.quantidade            = de-qtd
                    tt-movto.cod-unid-negoc        = IF AVAILABLE item-uni-estab THEN 
                                                              item-uni-estab.cod-unid-negoc
                                                          ELSE 
                                                              "".
                
                IF lGerarLog THEN 
                    MESSAGE 'destino ' tt-movto.Cod-localiz " - " tt-movto.cod-depos " - " dc-pto-controle.cod-pto-controle " - " dc-pto-controle.utiliz-loc-pad-item SKIP.
                    
                /**
                *  RZO - Verificar antes se a pl-it-calc nao est� travada 
                *        antes de realziar a transferencia
                *        02/02/2010
                **/

                IF lGerarLog THEN
                    MESSAGE "ANTES esapi014".
                
                RUN esapi/esapi014.p (INPUT tt-item.it-codigo, INPUT 0).
                
                IF lGerarLog THEN
                    MESSAGE "DEPOIS esapi014 " RETURN-VALUE.

                IF RETURN-VALUE <> "OK":U THEN 
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "Erro: MRP em uso, nao e possivel realizar a Transferencia".
                    l-erro = YES.
                    UNDO bloco, RETURN "NOK".
                END.

                IF lGerarLog THEN MESSAGE "Antes da CEAPI001k " ETIME .

                IF NOT VALID-HANDLE(h-ceapi001K) THEN
                    RUN cep/ceapi001k.p PERSISTENT SET h-ceapi001k.
        
                RUN pi-execute IN h-ceapi001k (INPUT-OUTPUT table tt-movto,
                    INPUT-OUTPUT table tt-erro,
                    INPUT YES). 
                                                
                DELETE PROCEDURE h-ceapi001k.

                ASSIGN 
                    h-ceapi001k = ?.

                IF lGerarLog THEN MESSAGE "Depois da CEAPI001K " RETURN-VALUE " - " ETIME .
		    
                IF CAN-FIND(FIRST tt-erro) OR RETURN-VALUE = "NOK" THEN
                DO:
            
                    FOR EACH tt-erro NO-LOCK:
    
                        CREATE tt-erro-api.
                        ASSIGN 
                            tt-erro-api.tipo     = 1
                            tt-erro-api.mensagem = tt-erro.mensagem
                            l-erro               = YES.
                    END.
                    UNDO bloco, RETURN "NOK".
                END.


            END.


            IF NOT CAN-FIND(FIRST tt-erro-api
                WHERE tt-erro-api.tipo = 1) THEN
            DO:
                FIND dc-rg-item  
                    WHERE dc-rg-item.rg-item = tt-item.rg-item
                    NO-ERROR NO-WAIT.
                IF LOCKED(dc-rg-item) OR ( NOT AVAILABLE dc-rg-item ) THEN
                DO:
                    CREATE tt-erro-api.
                    ASSIGN 
                        tt-erro-api.tipo     = 1
                        tt-erro-api.mensagem = "RG em uso: " + tt-item.rg-item.
                    UNDO bloco, RETURN "NOK".
                END.

                /**
                *  Criar a dc-transfer
                **/

                FIND LAST b-dc-transfer NO-LOCK NO-ERROR.

                FIND tt-movto
                    WHERE tt-movto.Tipo-trans            = 2 /* Saida */
                    NO-LOCK NO-ERROR.

                CREATE dc-transfer.
                ASSIGN 
                    dc-transfer.nro-docto        = IF AVAILABLE b-dc-transfer THEN b-dc-transfer.nro-docto + 1 ELSE 1
                    dc-transfer.cod-depos-dest   = c-cod-depos-para
                    dc-transfer.cod-depos-orig   = c-cod-depos-de
                    dc-transfer.cod-localiz-dest = c-cod-localiz-para
                    dc-transfer.cod-localiz-orig = c-cod-localiz-de
                    dc-transfer.cod-pto-controle = c-cod-pto-controle
                    dc-transfer.rg-item          = tt-item.rg-item
                    dc-transfer.nr-rack          = tt-item.nr-rack
                    dc-transfer.ind-tipo-transf  = 1.

                /**
                *  Gerar movimento historico
                **/
                ASSIGN 
                    i-nr-seq-movto = NEXT-VALUE(seq-dc-movto-pto-controle).

                FIND LAST b1-dc-movto-pto-controle NO-LOCK USE-INDEX idx_movto_pto_controle
                    WHERE b1-dc-movto-pto-controle.rg-item = dc-rg-item.rg-item NO-ERROR.

                CREATE dc-movto-pto-controle.
                ASSIGN 
                    dc-movto-pto-controle.cod-cor              = IF AVAILABLE b1-dc-movto-pto-controle THEN b1-dc-movto-pto-controle.cod-cor ELSE ""
                    dc-movto-pto-controle.cod-depos-dest       = c-cod-depos-para
                    dc-movto-pto-controle.cod-depos-orig       = c-cod-depos-de
                    dc-movto-pto-controle.cod-localiz-dest     = c-cod-localiz-para
                    dc-movto-pto-controle.cod-localiz-orig     = c-cod-localiz-de
                    dc-movto-pto-controle.cod-pto-controle     = c-cod-pto-controle
                    dc-movto-pto-controle.cod-pto-controle-ant = IF AVAILABLE b1-dc-movto-pto-controle THEN b1-dc-movto-pto-controle.cod-pto-controle-ant ELSE ""
                    dc-movto-pto-controle.it-codigo            = tt-item.it-codigo
                    dc-movto-pto-controle.it-codigo-ant        = IF AVAILABLE b1-dc-movto-pto-controle THEN b1-dc-movto-pto-controle.it-codigo-ant ELSE ""
                    dc-movto-pto-controle.local-pto            = IF AVAILABLE dc-pto-controle THEN dc-pto-controle.local-pto ELSE ""
                    dc-movto-pto-controle.nr-rack              = tt-item.nr-rack
                    dc-movto-pto-controle.nr-seq               = i-nr-seq-movto
                    dc-movto-pto-controle.nr-seq-ant           = IF AVAILABLE b1-dc-movto-pto-controle THEN b1-dc-movto-pto-controle.nr-seq ELSE 0
                    dc-movto-pto-controle.nro-docto            = i-seq-movto
                    dc-movto-pto-controle.rg-item              = tt-item.rg-item
                    dc-movto-pto-controle.tipo-pto             = IF AVAILABLE dc-pto-controle THEN dc-pto-controle.tipo-pto ELSE 0
                    dc-movto-pto-controle.cod-usuario          = c-usuario
                    dc-movto-pto-controle.data                 = TODAY
                    dc-movto-pto-controle.hora                 = STRING(TIME,"hh:mm:ss").

                ASSIGN 
                    dc-rg-item.cod-pto-controle = c-cod-pto-controle
                    dc-rg-item.tipo-pto         = dc-pto-controle.tipo-pto
                    dc-rg-item.local-pto        = dc-pto-controle.local-pto
                    dc-rg-item.cod-depos-orig   = c-cod-depos-para
                    dc-rg-item.cod-localiz-orig = c-cod-localiz-para.

                RELEASE dc-rg-item.
            END.

            IF l-erro THEN
                UNDO bloco, RETURN "NOK".
        END.

        /**
        *  Aplicar a desmontagem, quando houver
        **/
        IF l-desmonta THEN
        DO:
            FOR EACH tt-rg-item NO-LOCK.
                IF tt-rg-item.codigo BEGINS "RC" THEN
                DO:
                    /**
                    *  Transferir as pe�as para a tt-rg-item antes do desmonta
                    **/
                    IF NOT VALID-HANDLE(h-api) THEN
                        RUN esapi/esapi006.p PERSISTENT SET h-api.

                    FOR EACH tt-monta-rack . 
                        DELETE tt-monta-rack. 
                    END.
                    FOR EACH tt-rg-item-2 . 
                        DELETE tt-rg-item-2. 
                    END.

                    CREATE tt-monta-rack.
                    ASSIGN 
                        tt-monta-rack.cod-usuario      = c-usuario
                        tt-monta-rack.cod-pto-controle = c-cod-pto-ativ
                        tt-monta-rack.nr-rack          = tt-rg-item.codigo
                        tt-monta-rack.rg-item          = "".

                    RUN pi-efetiva-desmonta-rack IN h-api (INPUT TABLE tt-monta-rack,
                        INPUT TABLE tt-rg-item-2,
                        OUTPUT TABLE tt-erro-api).

                    IF VALID-HANDLE(h-api) THEN
                        DELETE PROCEDURE h-api.

                    IF CAN-FIND(FIRST tt-erro-api
                        WHERE tt-erro-api.tipo = 1) THEN
                        UNDO bloco, RETURN "NOK".
                END.
            END.
        END.

        IF NOT CAN-FIND(FIRST tt-erro-api
            WHERE tt-erro-api.tipo = 1) THEN
        DO:
            CREATE tt-erro-api.
            ASSIGN 
                tt-erro-api.tipo     = 0
                tt-erro-api.mensagem = "Transferencia Realizada com Sucesso.".
            RETURN "OK".
        END.
    /*
    IF h-handle:PERSISTEN THEN DELETE PROCEDURE h-handle.
    */
    END.

    IF lGerarLog THEN
        MESSAGE "fim transferencia " ETIME.
END PROCEDURE.

/**
*  Fifo alterado para buscar apenas pelo ultimo rg-item
**/
PROCEDURE pi-fifo.
    DEFINE INPUT PARAMETER p-cod-depos AS CHARACTER.
    DEFINE INPUT PARAMETER p-cod-localiz AS CHARACTER.
    DEFINE INPUT PARAMETER TABLE FOR tt-rg-item.
    DEFINE OUTPUT PARAMETER p-fifo AS CHARACTER.

    /* vars */
    DEFINE VARIABLE c-it-codigo AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-rg-item   AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-nr-rack   AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-rg-item-1 AS CHARACTER NO-UNDO.
    DEFINE VARIABLE c-nr-rack-1 AS CHARACTER NO-UNDO.
    DEFINE VARIABLE dt-data     AS DATE      NO-UNDO.
    DEFINE VARIABLE hr-hora     AS CHARACTER NO-UNDO.

    /* main */
    IF lGerarLog THEN
        MESSAGE "pi-fifo " ETIME.

    ASSIGN 
        p-fifo = "".

    /*
    FIND dc-param-depos NO-LOCK
        WHERE dc-param-depos.cod-depos = p-cod-depos NO-ERROR.

    IF NOT AVAIL dc-param-depos OR
        (AVAIL dc-param-depos AND NOT dc-param-depos.ind-fifo) THEN
        RETURN.
        */

    IF lGerarLog THEN
        MESSAGE "FIFO inicio " ETIME.

    FOR EACH tt-rg-item NO-LOCK.
        IF tt-rg-item.codigo BEGINS "RC" THEN
        DO:
            FIND LAST dc-rack-itens
                WHERE dc-rack-itens.nr-rack = SUBSTRING(tt-rg-item.codigo,3,11)
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE dc-rack-itens THEN
                RETURN.

            FIND dc-rg-item
                WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE dc-rg-item  THEN
                RETURN.
        END.
        ELSE 
        DO:
            FIND dc-rg-item
                WHERE dc-rg-item.rg-item = tt-rg-item.codigo
                NO-LOCK NO-ERROR.
            IF NOT AVAILABLE dc-rg-item THEN
                RETURN.
        END.

        ASSIGN
            p-cod-depos   = dc-rg-item.cod-depos-orig
            p-cod-localiz = dc-rg-item.cod-localiz-orig
            .

        /*IF lGerarLog THEN*/
        MESSAGE "TOTVS 222 " ETIME SKIP
            'dc-rg-item.rg-item ' dc-rg-item.rg-item SKIP
            'p-cod-depos ' p-cod-depos SKIP 
            'p-cod-localiz ' p-cod-localiz SKIP
            .
        
        FIND dc-param-depos NO-LOCK
            WHERE dc-param-depos.cod-depos = p-cod-depos NO-ERROR.
    
        IF NOT AVAILABLE dc-param-depos OR
            (AVAILABLE dc-param-depos AND NOT dc-param-depos.ind-fifo) THEN
            RETURN.

        IF lGerarLog THEN
            MESSAGE "TOTVS 3 " ETIME dc-rg-item.it-codigo.

        ASSIGN 
            c-it-codigo = dc-rg-item.it-codigo
            c-rg-item-1 = dc-rg-item.rg-item
            c-nr-rack-1 = dc-rg-item.nr-rack.

        /* /*ASSIGN dt-data = ?. */
/*         FOR each dc-rg-item */
/*             WHERE dc-rg-item.it-codigo = c-it-codigo */
/*               AND dc-rg-item.cod-depos-orig = p-cod-depos */
/*               /*AND dc-rg-item.cod-localiz-orig = p-cod-localiz*/ */
/*               and dc-rg-item.cod-pto-controle begimd 'RE' */
/*               AND (dc-rg-item.tipo-pto = 1 OR */
/*                    dc-rg-item.tipo-pto = 3) */
/*               AND dc-rg-item.situacao = 2 */
/*             NO-LOCK. */
/*    */
/*             for each dc-movto-pto-controle USE-INDEX idx-rg-item-local */
/*                 where dc-movto-pto-controle.rg-item = dc-rg-item.rg-item */
/*                   and dc-movto-pto-controle.cod-depos-dest = p-cod-depos */
/*                  /*  and dc-movto-pto-controle.cod-localiz-dest = p-cod-localiz */ */
/*                 no-lock. */
/*    */
/*                 if dt-data = ? then */
/*                     assign dt-data = dc-movto-pto-controle.data */
/*                            hr-hora = dc-movto-pto-controle.hora */
/*                            c-rg-item = dc-rg-item.rg-item */
/*                            c-nr-rack = dc-rg-item.nr-rack. */
/*    */
/*                 if dt-data >= dc-movto-pto-controle.data THEN DO: */
/*                     IF dt-data = dc-movto-pto-controle.data THEN DO: */
/*                         IF hr-hora > dc-movto-pto-controle.hora THEN */
/*                             assign dt-data = dc-movto-pto-controle.data */
/*                                hr-hora = dc-movto-pto-controle.hora */
/*                                c-rg-item = dc-rg-item.rg-item */
/*                                c-nr-rack = dc-rg-item.nr-rack. */
/*                     END. */
/*                     ELSE */
/*                         assign dt-data = dc-movto-pto-controle.data */
/*                                hr-hora = dc-movto-pto-controle.hora */
/*                                c-rg-item = dc-rg-item.rg-item */
/*                                c-nr-rack = dc-rg-item.nr-rack. */
/*                 END. */
/*             end. */
/*         END. */
/*    */
/*         IF lGerarLog THEN */
/*         MESSAGE "TOTVS 4 " ETIME SKIP */
/*             'c-rg-item  ' c-rg-item SKIP */
/*             'c-rg-item-1 ' c-rg-item-1 SKIP */
/*             'c-nr-rack ' c-nr-rack SKIP */
/*             'c-nr-rack-1 ' c-nr-rack-1. */
/*    */
/*         IF c-rg-item <> c-rg-item-1 AND */
/*            c-nr-rack <> c-nr-rack-1 THEN */
/*             ASSIGN p-fifo = c-rg-item */
/*                    p-fifo = p-fifo + */
/*                                 if c-nr-rack <> "" then */
/*                                     " com data " + STRING(YEAR(dt-data),'9999') + '/' + STRING(dt-data - DATE('01/01/' + STRING(YEAR(dt-data),'9999')) + 1) + */
/*                                     " no rack " + string(c-nr-rack,"RC99999999999") + ".." */
/*                                  else */
/*                                     "..". */
/*          aaaaaaa*/ */
         
        ASSIGN 
            dt-data = ?
            hr-hora = ''.

        FOR EACH dc-rg-item
            WHERE dc-rg-item.it-codigo = c-it-codigo
            AND dc-rg-item.cod-depos-orig = p-cod-depos
            /*  AND dc-rg-item.cod-localiz-orig = p-cod-localiz*/
            /*   and dc-rg-item.cod-pto-controle begiNS 'RE' */
            AND (dc-rg-item.tipo-pto = 1 OR
            dc-rg-item.tipo-pto = 2 OR             
            dc-rg-item.tipo-pto = 3)    
            AND dc-rg-item.situacao = 2 NO-LOCK,
            EACH dc-movto-pto-controle USE-INDEX idx-rg-item-local
            WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item
            AND dc-movto-pto-controle.cod-depos-dest = p-cod-depos
            /* and dc-movto-pto-controle.cod-localiz-dest = p-cod-localiz*/ NO-LOCK
            BREAK BY dc-movto-pto-controle.it-codigo
            BY dc-movto-pto-controle.data 
            BY dc-movto-pto-controle.hora 
            BY dc-movto-pto-controle.nr-seq.
    
            FIND tt-rg-fifo WHERE 
                tt-rg-fifo.rg-item = dc-rg-item.rg-item AND
                tt-rg-fifo.nr-rack = dc-rg-item.nr-rack NO-LOCK NO-ERROR.
            IF NOT AVAILABLE tt-rg-fifo THEN
            DO:
                CREATE tt-rg-fifo.
                ASSIGN 
                    tt-rg-fifo.rg-item   = dc-rg-item.rg-item
                    tt-rg-fifo.it-codigo = dc-rg-item.it-codigo
                    tt-rg-fifo.nr-rack   = dc-rg-item.nr-rack
                    tt-rg-fifo.data      = dc-movto-pto-controle.data
                    tt-rg-fifo.hora      = dc-movto-pto-controle.hora.
            
                IF dt-data = ? THEN
                    ASSIGN dt-data = dc-movto-pto-controle.data
                        hr-hora = dc-movto-pto-controle.hora.
    
    
    
    
                MESSAGE "dtahr " dt-data  dc-movto-pto-controle.data
                    hr-hora  dc-movto-pto-controle.hora.
    
    
    
                IF dt-data >= dc-movto-pto-controle.data THEN 
                DO:
                    IF dt-data = dc-movto-pto-controle.data THEN 
                    DO:
                        IF hr-hora > dc-movto-pto-controle.hora THEN
                            ASSIGN dt-data = dc-movto-pto-controle.data
                                hr-hora = dc-movto-pto-controle.hora.
                    END.
                    ELSE
                        ASSIGN dt-data = dc-movto-pto-controle.data
                            hr-hora = dc-movto-pto-controle.hora.
                END.
            END.
        END.
    
        FOR EACH tt-rg-fifo. 
            IF tt-rg-fifo.data = dt-data /*and
               tt-rg-fifo.hora = hr-hora*/ THEN 
            DO:
            END.
            ELSE DELETE tt-rg-fifo.
        END.

        FIND tt-rg-fifo WHERE 
            tt-rg-fifo.rg-item = c-rg-item-1 AND
            tt-rg-fifo.nr-rack = c-nr-rack-1 NO-LOCK NO-ERROR.
        IF NOT AVAILABLE tt-rg-fifo THEN 
        DO:
            ASSIGN 
                p-fifo = c-rg-item-1
                p-fifo = ". FIFO na data " + STRING(YEAR(dt-data),'9999') + '/' + STRING(dt-data - DATE('01/01/' + STRING(YEAR(dt-data),'9999')) + 1) +
                           " - " + STRING(dt-data,'99/99/9999') + "..".
        END.
        
    END.
    
END PROCEDURE.

PROCEDURE pi-sac.
    /* param */
    DEFINE INPUT PARAMETER p-cod-depos AS CHARACTER.
    DEFINE INPUT PARAMETER p-cod-localiz AS CHARACTER.
    DEFINE INPUT PARAMETER TABLE FOR tt-rg-item.
    DEFINE OUTPUT PARAMETER p-sac AS CHARACTER.

    /* vars */
    DEFINE VARIABLE chora  AS CHARACTER NO-UNDO.
    DEFINE VARIABLE ihra   AS INTEGER   NO-UNDO.
    DEFINE VARIABLE imin   AS INTEGER   NO-UNDO.
    DEFINE VARIABLE isec   AS INTEGER   NO-UNDO.
    DEFINE VARIABLE ihora  AS INTEGER   NO-UNDO.
    DEFINE VARIABLE cdocto AS CHARACTER NO-UNDO.

    /* main */
    IF lGerarLog THEN
        MESSAGE "pi-sac " ETIME.

    ASSIGN 
        p-sac = "".

    /*
    FIND dc-param-depos NO-LOCK
        WHERE dc-param-depos.cod-depos = p-cod-depos NO-ERROR.

    IF NOT AVAIL dc-param-depos OR
        (AVAIL dc-param-depos AND NOT dc-param-depos.ind-fifo) THEN
        RETURN.
        */

    IF lGerarLog THEN MESSAGE "inicio pi-sac " ETIME.

    FOR EACH tt-rg-item NO-LOCK.

        IF tt-rg-item.codigo BEGINS "RC" THEN
        DO:
            FIND FIRST dc-rack-itens
                WHERE dc-rack-itens.nr-rack = SUBSTRING(tt-rg-item.codigo,3,11)
                NO-LOCK NO-ERROR.
            IF AVAILABLE dc-rack-itens THEN
            DO:
                FIND dc-rg-item
                    WHERE dc-rg-item.rg-item = dc-rack-itens.rg-item
                    NO-LOCK NO-ERROR.
            END.
        END.
        ELSE
            FIND dc-rg-item
                WHERE dc-rg-item.rg-item = tt-rg-item.codigo
                NO-LOCK NO-ERROR.

        /**
        *  Rotina implementada para avliar erro no log
        *  RZO - 27/09/2007
        **/
        IF NOT AVAILABLE dc-rg-item THEN
        DO:
            ASSIGN 
                p-sac = "RG Item nao avaliado no SAC: " + tt-rg-item.codigo.
            RETURN.
        END.

        ASSIGN
            p-cod-depos   = dc-rg-item.cod-depos-orig
            p-cod-localiz = dc-rg-item.cod-localiz-orig
            .

        IF lGerarLog THEN
            MESSAGE "TOTVS 21" ETIME SKIP
                'dc-rg-item.rg-item ' dc-rg-item.rg-item SKIP
                'p-cod-depos ' p-cod-depos SKIP 
                'p-cod-localiz ' p-cod-localiz SKIP
                .
        
        FIND dc-param-depos NO-LOCK
            WHERE dc-param-depos.cod-depos = p-cod-depos NO-ERROR.
    
        IF NOT AVAILABLE dc-param-depos OR
            (AVAILABLE dc-param-depos AND NOT dc-param-depos.ind-fifo) THEN
            RETURN.


        IF dc-rg-item.nr-rack <> "" THEN
            ASSIGN cdocto = "RC" + dc-rg-item.nr-rack.
        ELSE
            ASSIGN cdocto = dc-rg-item.rg-item.

        FIND FIRST movto-estoq NO-LOCK
            WHERE movto-estoq.it-codigo = dc-rg-item.it-codigo
            AND movto-estoq.nro-docto = cdocto
            AND movto-estoq.cod-depos = p-cod-depos
            AND movto-estoq.cod-localiz = p-cod-localiz
            AND movto-estoq.tipo-trans = 1 /* entrada*/ NO-ERROR.

        IF AVAILABLE movto-estoq THEN
        DO:
            ASSIGN 
                chora = movto-estoq.hr-trans
                ihra  = INT(ENTRY(1,chora,":"))
                imin  = INT(ENTRY(2,chora,":"))
                isec  = INT(ENTRY(1,chora,":"))
                ihora = (ihra * 60 * 60) + (imin * 60) + isec
                ihora = ((TODAY - movto-estoq.dt-trans) * 24) + (((TIME - ihora) / 60) / 60).

            IF ihora < dc-param-depos.tempo-sac THEN
            DO:
                ASSIGN 
                    p-sac = dc-rg-item.rg-item.

                IF dc-rg-item.nr-rack <> "" THEN
                    ASSIGN p-sac = p-sac + " no rack RC" + dc-rg-item.nr-rack.

                ASSIGN 
                    p-sac = p-sac + " / Tempo = " + STRING(ihora).
                RETURN.
            END.
        END.
    END.

    IF lGerarLog THEN MESSAGE "fim sac " ETIME SKIP 'p-sac ' p-sac.

END PROCEDURE.



// 23/08/2023
PROCEDURE pi-set-permissao.

    // param
    DEFINE input param p-tipo AS CHARACTER NO-UNDO.
    
    // main
    c-tipo-permissao = p-tipo.

END procedure.