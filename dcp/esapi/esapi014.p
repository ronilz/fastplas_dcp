&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Procedure 
/*------------------------------------------------------------------------
    File        : 
    Purpose     :

    Syntax      :

    Description :

    Author(s)   :
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
DEFINE VARIABLE c-ret AS CHARACTER   NO-UNDO.

/* ***************************  Parameter  ************************** */

DEFINE INPUT PARAMETER p-item AS CHAR NO-UNDO.
DEFINE INPUT PARAMETER p-ordem AS INTEGER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Procedure
&Scoped-define DB-AWARE no



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Procedure
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: CODE-ONLY COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Procedure ASSIGN
         HEIGHT             = 15
         WIDTH              = 60.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Procedure 


/* ***************************  Main Block  *************************** */

    /**
    *  verificar pl-it-calc
    **/
    
    ASSIGN c-ret = "OK":U.
    for each pl-prod FIELDS (cd-plano pl-estado log-multi-estabel) 
        where pl-prod.pl-estado = 1 no-lock.
    
        ASSIGN c-ret = "OK":U.

        FIND FIRST pl-it-calc 
             where pl-it-calc.it-codigo = p-item
               and pl-it-calc.cd-plano  = pl-prod.cd-plano EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
    
        if locked(pl-it-calc) then DO:
             ASSIGN c-ret = "NOK":U.
             LEAVE.
        END.
        RELEASE pl-it-calc.
    end. 

    /**
    *  Analisar ordem de producao 
    **/
    if p-ordem <> 0 then do:
        FIND FIRST ord-prod
            WHERE ord-prod.nr-ord-produ = p-ordem
            EXCLUSIVE-LOCK NO-ERROR NO-WAIT.
        IF LOCKED(ord-prod) THEN DO:
            ASSIGN c-ret = "NOK":U.
            LEAVE.
        END.
        RELEASE ord-prod.
    end.
    
    RETURN c-ret.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


