/*------------------------------------------------------------------------
    File        : estrutura.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : ronil
    Created     :
    Notes       :
  ----------------------------------------------------------------------*/
/* ***************************  Parameters   ************************** */
DEFINE INPUT PARAM p-item-cor AS CHAR NO-UNDO.
DEFINE INPUT PARAM p-item-semi AS CHAR NO-UNDO.
DEFINE OUTPUT PARAM p-lista AS CHAR NO-UNDO.

/* ***************************  Definitions  ************************** */
DEFINE TEMP-TABLE tt-estrutura
    FIELD it-codigo LIKE estrutura.it-codigo
    FIELD es-codigo LIKE estrutura.es-codigo.

DEFINE VARIABLE c-itens AS CHARACTER  NO-UNDO.  
DEFINE VARIABLE c-item-semi AS CHARACTER  NO-UNDO.  
DEFINE VARIABLE i-cont AS INTEGER NO-UNDO.



/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
/**
*  Buscar em quais estruturas o item pertence (itens pais)
**/ 
ASSIGN c-itens = ""
       p-lista = "".



FOR EACH estrutura 
    WHERE estrutura.es-codigo = p-item-semi
    NO-LOCK.
    
    MESSAGE estrutura.it-codigo
            'teste'
        VIEW-AS ALERT-BOX INFO BUTTONS OK.
    
    ASSIGN c-item-semi = estrutura.it-codigo.

    RUN pi-estrutura-1 (INPUT-OUTPUT c-item-semi).
    
    IF c-itens = "" THEN
        ASSIGN c-itens = c-item-semi.
    ELSE
        ASSIGN c-itens = c-itens + "," + c-item-semi.
END.    



/**
*  varrer a estrutura dos itens pais buscando o componente da cor
**/
DO i-cont = 1 TO NUM-ENTRIES(TRIM(c-itens),","):
    FOR EACH estrutura
        WHERE estrutura.it-codigo = ENTRY(i-cont,c-itens,",")
        NO-LOCK.
        
        MESSAGE estrutura.es-codigo  SKIP            
                p-item-cor           SKIP
                estrutura.es-codigo = p-item-cor
                
            VIEW-AS ALERT-BOX INFO BUTTONS OK.

        IF estrutura.es-codigo = p-item-cor THEN 
            RUN pi-add.
        else    
            RUN pi-estrutura-2 (INPUT estrutura.es-codigo).
    END.
END.

MESSAGE p-lista
    VIEW-AS ALERT-BOX INFO BUTTONS OK.
/* **********************  Internal Procedures  *********************** */


PROCEDURE pi-add :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/



    IF p-lista = "" THEN 
        ASSIGN p-lista = ENTRY(i-cont,c-itens,",").
    ELSE
        ASSIGN p-lista = p-lista + "," + ENTRY(i-cont,c-itens,",").

            MESSAGE "plista" SKIP
             p-lista
        VIEW-AS ALERT-BOX INFO BUTTONS OK.


END PROCEDURE.

PROCEDURE pi-estrutura-2 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEFINE INPUT PARAM p-item AS CHAR NO-UNDO.
    
    /* vars */
    
    /* main */
    FOR EACH estrutura
        WHERE estrutura.it-codigo = p-item 
        NO-LOCK.
        
        IF estrutura.es-codigo = p-item-cor THEN 
            RUN pi-add.
        else    
            RUN pi-estrutura-2 (INPUT estrutura.es-codigo).
    END.
END PROCEDURE.


PROCEDURE pi-estrutura-1 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    DEFINE INPUT-OUTPUT PARAMETER p-item AS CHAR NO-UNDO.
    
    /* vars */
    DEFINE VARIABLE c-item AS CHARACTER  NO-UNDO.
    DEFINE VARIABLE c-item1 AS CHARACTER  NO-UNDO.
    DEFINE VARIABLE c-itens AS CHARACTER  NO-UNDO.

    /* main */
    ASSIGN c-item = ""
           c-itens = "".

    FOR EACH estrutura 
        WHERE estrutura.es-codigo = p-item
        NO-LOCK.
        
        ASSIGN c-item1 = estrutura.it-codigo
               c-item = c-item1.
        RUN pi-estrutura-1 (INPUT-OUTPUT c-item).

        /**
        *  Fim da estrutura
        **/
        IF c-item1 = c-item THEN DO:
            IF c-itens = "" THEN 
                ASSIGN c-itens = c-item.
            ELSE        
                ASSIGN c-itens = c-itens + "," + c-item.           
        END.
    END.
    
    IF c-item <> "" THEN 
        ASSIGN p-item = c-itens.
END PROCEDURE.
