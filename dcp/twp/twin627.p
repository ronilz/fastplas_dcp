/*********************************************************************
** Cliente : FastPlas
** Programa: twin627.p
** Objetivo: Trigger no Usuario Material para criar registro na tabela
**           de autoriza��es.
** Tabelas : usuar-mater
** Data    : 07/03/2007
** Autor   : 
***********************************************************************/

/* Parametros da Trigger */
DEFINE PARAMETER BUFFER p-table     FOR usuar-mater.
DEFINE PARAMETER BUFFER p-old-table FOR usuar-mater.


    FIND dc-usuar-mater 
        WHERE dc-usuar-mater.cod-usuario = p-table.cod-usuario NO-ERROR.
    IF NOT AVAIL dc-usuar-mater THEN
    DO:

        CREATE dc-usuar-mater.
        ASSIGN dc-usuar-mater.cod-usuario    = p-table.cod-usuario
               dc-usuar-mater.ind-sac        = NO
               dc-usuar-mater.ind-retrabalho = NO
               dc-usuar-mater.ind-fifo       = NO.
    END.

    



