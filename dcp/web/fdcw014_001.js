


/* ######################################################################
fdcw014_001 - Apontamento Invent rio
#######################################################################*/

function fLoadRoutine () {
	fInitializeActions()
	fCreateListener()
    fStartTimer()
    document.forms[0].leitura.focus();
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
    document.forms[0].valApurado.onkeyup = fTrapKeys2
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
        fRefreshFicha()
	}
}

function fTrapKeys2(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
        fDoAction('confirmar')
	}
}

function fStartTimer(){
	if (document.forms[0].leitura.value.length >= 14) {
		fRefreshFicha()
		document.forms[0].leitura.value = ''
	}
	//document.forms[0].leitura.focus();
	setTimeout(fStartTimer,50);
}


function fRefreshFicha(){
	fDisableElement (document.forms[0].confirmar)
	// /scripts/cgiip.exe/WService=" + Broker + "/
	pUrl="fdcw014_001a"
    //alert(document.forms[0].leitura)
	fExecuteAjax (pUrl, '', fOpenConfirm, document.forms[0].leitura)
}


function fOpenConfirm (hasError, pElement, pReturnData){
	if (!(hasError)) {
		fEnableElement (document.forms[0].confirmar)
        document.forms[0].valApurado.focus();
	}else{
		fDisableElement (document.forms[0].confirmar)
	}	
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":
			// 	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			// 	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw014_001b"
			fDisableElement (document.forms[0].confirmar)
			fExecuteAjax (pUrl, '', fTrataEnvio, pElement)
		break;
	}	
}

function fTrataEnvio (hasError, pElement, pReturnData){
    if (!(hasError)) {
        document.forms[0].dtSaldo.value="";
        document.forms[0].nrFicha.value="";
        document.forms[0].nrContagem.value="";
        document.forms[0].leitura.value="";
        document.forms[0].itCodigo.value="";
        document.forms[0].descItem.value="";
        document.forms[0].codDepos.value="";
        document.forms[0].un.value="";
        document.forms[0].codLocaliz.value="";
        document.forms[0].contagem.value="";
        document.forms[0].valApurado.value="";
        fEnableElement (document.forms[0].confirmar)
        document.forms[0].leitura.focus();
    }
}

function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
}










