/**
 *  FDCW015_001 - Geração automática de Rack, montagem, apontamento de cor e reporte de producao 
 */

function fLoadRoutine () {
	fInitializeActions()
	document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
	fCreateListener()
	fStartTimer()
	
	fDisableElement (document.forms[0].readCor);
}

function fCreateListener(){
	document.forms[0].readRG.onkeyup = fTrapKeys;
	
	if (document.forms[0].readCor.className.search (/s_on/) == -1){
		document.forms[0].readCor.onkeyup = fTrapKeys;
	}
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].confirmar.className.search (/s_off/) == -1){
			fDoAction('confirmar')
		}
	}
}

function fStartTimer(){

	// se for colorido
    if (document.forms[0].tpReporte.value === "2"){
    	
		if (document.forms[0].readRG.className.search(/s_on/) > -1){
			if (document.forms[0].readRG.value.length >= 13) {
				fEnableElement (document.forms[0].readCor)
				document.forms[0].readCor.focus()
				fDisableElement (document.forms[0].readRG)
				setTimeout(fStartTimer,50);
			}else{
				document.forms[0].readRG.focus()
				setTimeout(fStartTimer,50);
			}
		}
		
		if (document.forms[0].readCor.className.search(/s_on/) > -1){
			if (document.forms[0].readCor.value.length >= 6) {
				fEnableElement (document.forms[0].readRG)
				document.forms[0].readRG.focus()
				fDisableElement (document.forms[0].readCor)
				setTimeout(fStartTimer,50);
				fRefreshItemList()
				document.forms[0].readRG.value = ''
				document.forms[0].readCor.value = ''
			}else{
				document.forms[0].readCor.focus()
				setTimeout(fStartTimer,50);
			}
		}
		
	}else{
		if (document.forms[0].readRG.value.length >= 13) {
			fRefreshItemList()
			document.forms[0].readRG.value = ''
		}
		document.forms[0].readRG.focus()
		setTimeout(fStartTimer,50);
	}
}

function fRefreshItemList(){

	// Verifica se o item ja existe no browser	
	if (document.getElementById('browserrow_' + document.forms[0].readRG.value)){
	
		fRegistraErro (1, 'Item ja foi selecionado')
		
	}else{		

        // Adiciona o item na lista
		if (document.forms[0].readRG.value != ''){	
			// add new line
			newrow = browser_produto.insertRow(1)
			newrow.name = 'browserrow_' + document.forms[0].readRG.value
			newrow.id = 'browserrow_' + document.forms[0].readRG.value
			newrow.valid = "no"
			newrow.pRetValue = document.forms[0].readRG.value
			// add cell item
			newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].readRG.value
			newcell.appendChild (document.createTextNode(document.forms[0].readRG.value))
			// add cell desc
			newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].readRG.value
			newcell.appendChild (document.createTextNode(""))		
			// add cell cor	
            newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].readCor.value
			newcell.appendChild (document.createTextNode(""))  
            
		}
	
		document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
		
		// Invoca a validacao
		fDisableElement (document.forms[0].confirmar)
		// /scripts/cgiip.exe/WService=" + Broker + "/
		pUrl = "fdcw015_001a"
		pNVPs = ''
		pCallback = fValidateBrowser
		pElement = newrow
		fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
	}
		
}

function fValidateBrowser (hasError, pElement, pReturnData){
    if (pReturnData[0]){
		if (pReturnData[0].childNodes.length > 0) {
			
			if (!(hasError)) {
				// Altera textos, cor e propriedade do item no browser

                itemTR = pElement
                itemTR.valid = "yes"	
                itemTR.cells[0].className = "browser_row b1"	

				if (pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild) {
					pCodigo = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
				}else{
					pCodigo = ''
				}
				if (pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild) {
					pDescricao = pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild.data
				}else{
					pDescricao = ''
				}

                if (pReturnData[0].getElementsByTagName('COR')[0].firstChild) {
					pCor = pReturnData[0].getElementsByTagName('COR')[0].firstChild.data
				}else{
					pCor = ''
				}

                itemTR.pRetValue = itemTR.pRetValue + ';' + pDescricao	
                itemTR.pRetValue = itemTR.pRetValue + ';' +  pCor  

				itemTR.cells[1].innerHTML = pDescricao  
                itemTR.cells[2].innerHTML = pCor
				fApplyActions (itemTR.cells[0])
				fApplyActions (itemTR.cells[1])
				fApplyActions (itemTR.cells[2])

			}else{
				document.getElementById('browser_produto').deleteRow(pElement.rowIndex)				
			}
			
			// Reativa ou n�o o bot�o de confirma��o
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
			
			document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length) - 1;
			
		}
	}
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
        
		case "cancelar":	
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			fDisableElement (document.forms[0].confirmar)
			fDisableElement (document.forms[0].cancelar)
			
			// Monta a lista de itens
			pNVPs = ""
			for (var i=0; i<document.getElementById('browser_produto').rows.length; i++){
			  if (document.getElementById('browser_produto').rows[i].pRetValue) {
				  if (pNVPs == "") {	
				    pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
				  } else {
			        pNVPs = pNVPs + "," + document.getElementById('browser_produto').rows[i].pRetValue
				  }
			  }
			}
			
			if (pNVPs == '') {
				
				fEnableElement (document.forms[0].confirmar)
				fEnableElement (document.forms[0].cancelar)
				
				break;
			}
			
			pNVPs= "&ItemCodes=" + pNVPs;
			
			pUrl = "dcw015_001b"
			
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw015_001b"
			pCallback = fTrataRetorno
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fTrataRetorno (hasError, pElement, pReturnData, pErrors){
		
	hasSuccess = true;
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data != "0") {
			hasSuccess = false;
		}
	}
	
	if (hasSuccess) {
		for (i=0; i<pErrors.length; i++) {
			
			if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild){
				
				if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
					
					var pUrl="fdcw015_001c"
					pUrl = pUrl + '?ptocontrole=' + document.getElementById('pto_controle').value
					pUrl = pUrl + '&depositosaida=' + document.getElementById('depSaida').value
					pUrl = pUrl + '&localizacaosaida=' + document.getElementById('locSaida').value
					pUrl = pUrl + '&depositoentrada=' + document.getElementById('depEntrada').value
					pUrl = pUrl + '&localizacaoentrada=' + document.getElementById('locEntrada').value
					pUrl = pUrl + '&tpreporte=' + document.getElementById('tpReporte').value
					pUrl = pUrl + '&tprack=' + document.getElementById('tprack').value
					
					// Monta a lista de itens
					var pNVPs = ""
					for (var i=0; i<document.getElementById('browser_produto').rows.length; i++){
					  if (document.getElementById('browser_produto').rows[i].pRetValue) {
						  if (pNVPs == "") {	
							pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
						  } else {
							pNVPs = pNVPs + "," + document.getElementById('browser_produto').rows[i].pRetValue
						  }
					  }
					}
					pNVPs= "&ItemCodes=" + pNVPs;
					
					pUrl = pUrl + pNVPs
					document.location.href=pUrl 
				}
			}
		}
	}
	fEnableElement (document.forms[0].confirmar)
	fEnableElement (document.forms[0].cancelar)
}


function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
	
	if (pElement.pRetValue){
		if (confirm("Por favor, confirme a exclusao do item " + pElement.pRetValue)){
			
			fDisableElement (document.forms[0].confirmar)
			pElement.parentNode.parentNode.deleteRow(pElement.parentNode.rowIndex)
			document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 1)
			
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
		}
			
	}
}
