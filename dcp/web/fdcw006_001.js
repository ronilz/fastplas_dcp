


/* ######################################################################
fdcw006_001 - Montagem de Rack
#######################################################################*/


function fLoadRoutine () {
	fInitializeActions()
	document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
	fCreateListener()
	fStartTimer()
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}
}

function fStartTimer(){
	if (document.forms[0].leitura.value.length >= 13) {
		fIncluiItem()
		document.forms[0].leitura.value = ''
	}
	document.forms[0].leitura.focus()
	setTimeout(fStartTimer,50);
}


function fIncluiItem(){

	// Verifica se o item ja existe no browser	
	TipoErro = 0
	hasError = false
	
	
	if (document.getElementById('browserrow_' + document.forms[0].leitura.value)){
		fRegistraErro (1, 'Item ja foi selecionado')
	}else{
		// Adiciona o item na lista
		if (document.forms[0].leitura.value != ''){	
			newrow = browser_produto.insertRow(1)
			newrow.name = 'browserrow_' + document.forms[0].leitura.value
			newrow.id = 'browserrow_' + document.forms[0].leitura.value
			newrow.valid = "no"
			newrow.pRetValue = document.forms[0].leitura.value
			newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].leitura.value
			newcell.appendChild (document.createTextNode(document.forms[0].leitura.value))
			newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].leitura.value
			newcell.appendChild (document.createTextNode(""))		
		}
	
		document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
		
		// Invoca a validacao do item
		fDisableElement (document.forms[0].confirmar)
		// /scripts/cgiip.exe/WService=" + Broker + "/
		pUrl = "fdcw006_001a"
		pNVPs = ''
	
		// alteracao: passar todos os itens do browser
		pNVPs = '&ItemCodes='
		var isFirst = true
		for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
			if (document.getElementById('browser_produto').rows[i].pRetValue){
				if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
				pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
			}
		}
			
		pCallback = fValidateBrowser
		pElement = document.getElementById('browser_produto')
		fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
	}
}

function fValidateBrowser (hasError, pElement, pReturnData, pErrors){
	/* validar senha */
	if (document.forms[0].auth_login.value != "" &&
        document.forms[0].auth_pass.value != "") {
			pUrl = "fdcw006_001c"
			pNVPs = ''
			pCallback = fValidateAuth
			//pElement = document.getElementById('leitura')
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		}

	/* outras validacoes */
	if (pReturnData[0]){
		if (pReturnData[0].childNodes.length > 0) {
			
			if (!(hasError)) {
				
				if (TipoErro == 2) {
					var pergunta = confirm("Deseja incluir o item fora do FIFO?")
					if (!(pergunta)) {
						
						itemCode = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
						itemTR = document.getElementById ('browserrow_' + itemCode)
						browser_produto.deleteRow(itemTR.rowIndex)
						
						fEnableElement (document.forms[0].confirmar)
					
						document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
						
						return ;
					} 
				}
				
				// Altera textos, cor e propriedade do item no browser
				itemCode = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
				itemTR = document.getElementById ('browserrow_' + itemCode)
				itemTR.valid = "yes"						
				itemTR.cells[0].className = "browser_row b1"	
				itemTR.cells[1].className = "browser_row b1"	
				if (pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild){
					itemTR.cells[1].childNodes[0].data = pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild.data
				}else{
					itemTR.cells[1].childNodes[0].data = ''
				}
				fApplyActions (itemTR.cells[0])
				fApplyActions (itemTR.cells[1])
			}else{
				// Remove item errado
				itemCode = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
				itemTR = document.getElementById ('browserrow_' + itemCode)
				browser_produto.deleteRow(itemTR.rowIndex)				
			}
			
			// Reativa ou nao o botaoo de confirmacao
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
			
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
			
		}
	}
}

function fValidateAuth (hasError, pElement, pReturnData, pErrors){

	if (pElement.rows[1] && hasError){
		// Remove item errado
		itemTR = document.getElementById (pElement.rows[1].name)
		browser_produto.deleteRow(itemTR.rowIndex)
		document.forms[0].auth_login.value = ""
		document.forms[0].auth_pass.value = ""
	} else {
		document.forms[0].auth_login.value = pReturnData[0].getElementsByTagName('USUARIO')[0].firstChild.data
		document.forms[0].auth_pass.value = pReturnData[0].getElementsByTagName('SENHA')[0].firstChild.data
	}

}


function fTrataEnvio(hasError, pElement, pReturnData, pErrors){
	hasSuccess = false
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
			if (document.forms[0].contagem.value > 0 ) { 
				alert ('Montagem Realizada com Sucesso')
			}
			
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl				
		}
	}
	fEnableElement (document.forms[0].confirmar)
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":	
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			fDisableElement (document.forms[0].confirmar)	
			// Monta a lista de itens
			pNVPs = '&ItemCodes='
			isFirst = true
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].pRetValue){
					if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
					pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
				}
			}
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw006_001b"
			pCallback = fTrataEnvio
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	} else {
		
		/*
		if (pElement.innerHTML)
		
		var pergunta = confirm("Deseja retirar o RG" + pElement.innerHTML + " da selecao do RACK?")
		
		if (pergunta) {
			itemTR = document.getElementById ('browserrow_' + pElement.innerHTML)
			
			if (!(hasError)) {
				browser_produto.deleteRow(itemTR.rowIndex)
			}else{
				itemTR.cells[0].className = "browser_row b1"		
			}
			
			fEnableElement (document.forms[0].confirmar)
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
		}
		*/
	}

}
