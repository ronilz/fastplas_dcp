

/* ####################<%################################################
fdcw001_002b - AJAX Efetua Logoff
#######################################################################*/


CREATE WIDGET-POOL.
DEF VAR c-user AS CHAR NO-UNDO.
DEF VAR c-pass AS CHAR NO-UNDO.
DEF VAR l-ok AS LOGICAL INITIAL TRUE NO-UNDO.
def STREAM str-1.

{src/web2/wrap-cgi.i}


RUN process-web-request.
 


PROCEDURE process-web-request :
    DEF VAR c-ret AS CHAR NO-UNDO.
    DEF VAR i-cont AS INTEGER NO-UNDO.
    RUN outputHeader.  
    RUN fdcw001_002.
END PROCEDURE.


PROCEDURE outputHeader :

       RUN setCookie IN web-utilities-hdl (INPUT "cod_usu_websession":U,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
       RUN setCookie IN web-utilities-hdl (INPUT "sessionID",
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
       RUN setCookie IN web-utilities-hdl (INPUT "psw_usu_websession",
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
										   
   output-content-type ("text/html":U).  
   
END PROCEDURE.













