


/* ######################################################################
fdcw011_001 - Reporte de Sucata por Item
#######################################################################*/

function fLoadRoutine () {
	fInitializeActions()
	fCreateListener()
	fStartTimer()
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].confirmar.className.search (/s_off/) == -1){
			fDoAction('confirmar')
		}
	}
}

function fStartTimer(){
	if (document.forms[0].leitura.value.length >= 13) {
		fRefreshItem()
		document.forms[0].leitura.value = ''
	}
	//document.forms[0].leitura.focus();
	setTimeout(fStartTimer,50);
}

function fRefreshItem(){
	fDisableElement (document.forms[0].confirmar)
	// /scripts/cgiip.exe/WService=" + Broker + "/
	pUrl="fdcw011_001a"
    //alert(document.forms[0].leitura)
	fExecuteAjax (pUrl, '', fOpenConfirm, document.forms[0].leitura)
}


function fOpenConfirm (hasError, pElement, pReturnData){
	if (!(hasError)) {
		fEnableElement (document.forms[0].confirmar)
	}else{
		fDisableElement (document.forms[0].confirmar)
	}	
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":
			// 	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			// 	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw011_001b"
			fDisableElement (document.forms[0].confirmar)
			fExecuteAjax (pUrl, '', fTrataEnvio, pElement)
		break;
	}	
}

function fTrataEnvio (hasError, pElement, pReturnData){
    if (!(hasError)) {        
        document.forms[0].leitura.value="";
        document.forms[0].qtde_refugada.value="";
        document.forms[0].cod_item.value="";
        document.forms[0].desc_item.value="";
        document.forms[0].depositosaida.value="";
        document.forms[0].depositoentrada.value="";
        document.forms[0].rgrack.value="";
        document.forms[0].leitura.focus();
    }
    fEnableElement (document.forms[0].confirmar)
}

//function fTrataEnvio(){	
//	fEnableElement (document.forms[0].confirmar)
//}



function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
}










