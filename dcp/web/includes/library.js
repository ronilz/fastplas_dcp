
onerror=fHandleError
var gDebugMode = false
var cRegExpDot = /\./
var cRegExpDotGlobal = /\./g
var cRegExpCommaGlobal = /,/g
var cRegExpSlash = /\//
var cRegExpDoubleDots = /\:/
var cRegExpMinus = /\-/
var cRegExpNumbers = /[0-9]/
var cRegExpSeparators = /[\.\,\;\:\\\/]/
var cRegExpTodayKeys = /[tThH]/
var gOverrideKeyPress = false
var gZoomOpener = ""
var hasError = false

var TipoErro = 0

// Gerencia os erros e gera mensagens de alerta informativas
function fHandleError(msg,url,l){
	txt="Debug de Erros.\n\n"
	txt+="Error: " + msg + "\n"
	txt+="URL: " + url + "\n"
	txt+="Line: " + l + "\n\n"
	txt+="Click OK to continue.\n\n"
	alert(txt)
	return true
}

// Adiciona um erro ao DIV previamente preparado
function fRegistraErro (pTipoErro, pMensagem){
	pTipoErro = parseInt(pTipoErro)
	messagetable = document.getElementById('messagediv').firstChild
	newrow = messagetable.insertRow(messagetable.rows.length)
	newcell = newrow.insertCell (newrow.cells.length)
	newcell.name = 'message'
	newcell.id = 'message'
	newcell.BrowserClick = "yes"
	if (pTipoErro == 1){ newcell.className = 'notification nerror' }
	if (pTipoErro == 2){ newcell.className = 'notification nadvert' }
	if (pTipoErro == 0){ newcell.className = 'notification nsuccess' }
	if (pTipoErro == 3){ newcell.className = 'notification' }
	newcell.appendChild (document.createTextNode(pMensagem))		
	fRemoveClass (document.getElementById('messagediv'), 'noff')
	fAppendClass (document.getElementById('messagediv'), 'non')
	fApplyActions (newcell)	
}

// Elimina todas as notificacoes de erro
function fClearErrors(){
	rowsCount = document.getElementById('messagediv').firstChild.rows.length
	for (i=rowsCount - 1; i>=0; i--){
		document.getElementById('messagediv').firstChild.deleteRow(i)
	}
	fRemoveClass (document.getElementById('messagediv'), 'non')
	fAppendClass (document.getElementById('messagediv'), 'noff')
}

// Cria um Objeto XmlHttp
function fAjaxGetXmlHttpObject() {
	var objXMLHttp=null
	if (window.XMLHttpRequest){
		objXMLHttp=new XMLHttpRequest()
	} else if (window.ActiveXObject){
		objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP")
	} return objXMLHttp
}

// Executa todas as chamadas AJAX
function fExecuteAjax (pUrl, pNVPs, pCallback, pElement){
	// Acumula as informacoes do formulario
	NVPs = ""
	for (iCounter=0; iCounter < document.forms[0].elements.length; iCounter++){
		if (iCounter>0){ NVPs = NVPs + "&" }
		NVPs = NVPs + document.forms[0].elements[iCounter].name + "=" + escape(document.forms[0].elements[iCounter].value)
	}
	// Adiciona outras informacoes passadas para a funcao
	NVPs = NVPs + pNVPs
	// Cria o objeto
	var xmlHttp1 = null
	var xmlHttp1 = fAjaxGetXmlHttpObject()
	// Executa a chamada
	xmlHttp1.onreadystatechange = function () { fAjaxCallBack(xmlHttp1, pCallback, pElement) }
	xmlHttp1.open("POST", pUrl, true) 
	xmlHttp1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');  
	xmlHttp1.send(NVPs)	
}

// Gerencia o retorno das chamadas de ajax
function fAjaxCallBack (pxmlHttp, pCallback, pElement) {
	
	if (pxmlHttp.readyState == 4) {
		
		if (pxmlHttp.status == 200) {	
			//alert (pxmlHttp.responseText)
			xmlDoc = pxmlHttp.responseXML
			hasError = false	
			TipoErro = 0
			
			// Trata mensagens de erro
			ErrorMessages = xmlDoc.getElementsByTagName('ERROR')	
			if (ErrorMessages.length > 0){
				for (i=0;i<ErrorMessages.length;i++){
					if (ErrorMessages[i].getElementsByTagName('TYPE')[0].firstChild.data == '1'){
						hasError = true
					}
					if (ErrorMessages[i].getElementsByTagName('TYPE')[0].firstChild){
						TipoErro = ErrorMessages[i].getElementsByTagName('TYPE')[0].firstChild.data
					}else{
						TipoErro = 3
					}
					if (ErrorMessages[i].getElementsByTagName('MESSAGE')[0].firstChild){
						MsgErro = ErrorMessages[i].getElementsByTagName('MESSAGE')[0].firstChild.data
					}else{
						MsgErro = "Erro Desconhecido"
					}
					fRegistraErro (TipoErro, MsgErro)
				}
			}
			
			// Realimenta campos do formulario
			formdata = xmlDoc.getElementsByTagName('FORMDATA')
			if (formdata[0]){
				if (formdata[0].childNodes.length > 0) {
					for (iCounter = 0; iCounter < formdata[0].childNodes.length ; iCounter++){
						try {
						element = document.getElementById(formdata[0].childNodes[iCounter].tagName)			
						data = (formdata[0].childNodes[iCounter].firstChild)?formdata[0].childNodes[iCounter].firstChild.data:""
						element.value = data
						}catch(pError){}
					}								
				}
			}	
			
			// mostra popup de autorizazaoo se necessario
			browserdata = xmlDoc.getElementsByTagName('AUTH')
			if (browserdata[0]) {
				strFeatures = 'dialogHeight:140px;'+
							  'dialogWidth:240px;' +
							  'center:yes;' +
							  'help:no;' +
							  'resizable:no;'+
							  'status:no;'+
							  'scroll:no';
				//pUrl = '/scripts/cgiip.exe/WService=' + Broker + '/fdcw001_006'
				pUrl = 'fdcw001_006'
				pUrl = pUrl + '?' + fGenerateAbsolutelyRandomKey () + '=1'
				window.showModalDialog(pUrl, self, strFeatures)
			}				 

			pReturnData = xmlDoc.getElementsByTagName('RETURNDATA')
			pErrors = ErrorMessages

			/* verificar se houve cancelamento */
			var mountRack = xmlDoc.getElementsByTagName('MOUNTRACK')
			if (mountRack[0]) {
				if (self.document.forms[0].auth_login.value == "") {
					hasError = true
				} else {
					hasError = false
				}
			}

			// veirifica se houve problema de fifo
			var embarque = xmlDoc.getElementsByTagName('EMBARQUE')
			if (embarque[0]) {
				if (self.document.forms[0].auth_login.value == "") {
					hasError = true
				} else {
					hasError = false
				}
			}

			pCallback(hasError, pElement, pReturnData, pErrors)
		}
	}
}

// Adiciona uma classe ao elemento
function fAppendClass (pElement, pClass) {
	var vRegExp = new RegExp(pClass)
	if (pElement.className.search(vRegExp) < 0){
		pElement.className = pElement.className + " " + pClass
	}
}

// Remove uma classe do elemento
function fRemoveClass (pElement, pClass) {
	var vRegExp = new RegExp(pClass)
	var vRegExpG = new RegExp(pClass, "g")
	if (pElement.className.search(vRegExp) >= 0){
		newClass = pElement.className.replace (vRegExpG, " ")
		pElement.className = newClass
	}
}
	
// gera um numero aleatorio com 16 posicoes hexadecimais
function fGenerateAbsolutelyRandomKey (){
	vTmp = ""
	for (i=1;i<=16;i++){
		decnumber = Math.floor(Math.random() * 16)
		vTmp = vTmp + decnumber.toString(16)
	}
	return vTmp
}

// Desabilita um elemento
function fDisableElement (pElement){
	// Verifica se elemento e um array de elementos
	if (document.all(pElement.id).length > 1 && pElement.tagName.toLowerCase()!='select'){
		for (eachElement = 0; eachElement < document.all(pElement.id).length; eachElement++){
			vElm = document.all(pElement.id)[eachElement]
			// Desabilita Elementos
			if (vElm.className.search (/s_off/) == -1){
				fRemoveClass (vElm, "s_on")
				fAppendClass (vElm, "s_off")			
				// Zooms Vinculados
				if (vElm.zoom){
					ButtonElem = document.getElementById(vElm.zoom)
					if (ButtonElem){
						fDisableElement(ButtonElem)
					}
				}					
				// desvincula acoes
				fApplyActions (vElm)
			}			
		}
	}else{
		// Desabilita Elementos
		if (pElement.className.search (/s_off/) == -1){
			fRemoveClass (pElement, "s_on")
			fAppendClass (pElement, "s_off")			
			// Zooms Vinculados
			if (pElement.zoom){
				ButtonElem = document.getElementById(pElement.zoom)
				if (ButtonElem){
					fDisableElement(ButtonElem)
				}
			}					
			// desvincula acoes
			fApplyActions (pElement)
		}
	}
}

// Habilita um elemento
function fEnableElement (pElement) {
	// Verifica se elemento e um array de elementos
	if (document.all(pElement.id).length > 1 && pElement.tagName.toLowerCase()!='select'){
		for (eachElement = 0; eachElement < document.all(pElement.id).length; eachElement++){
			vElm = document.all(pElement.id)[eachElement]
			// Habilita Elementos
			if (vElm.className.search (/s_on/) == -1){
				fRemoveClass (vElm, "s_off")
				fAppendClass (vElm, "s_on")			
				// Zooms Vinculados
				if (vElm.zoom){
					ButtonElem = document.getElementById(vElm.zoom)
					if (ButtonElem){
						fEnableElement(ButtonElem)
					}
				}			
				// Re-vincula acoes
				fApplyActions (vElm)
			}
		}
	}else{
		// Habilita Elementos
		if (pElement.className.search (/s_on/) == -1){
			fRemoveClass (pElement, "s_off")
			fAppendClass (pElement, "s_on")			
			// Zooms Vinculados
			if (pElement.zoom){
				ButtonElem = document.getElementById(pElement.zoom)
				if (ButtonElem){
					fEnableElement(ButtonElem)
				}
			}			
			// Re-vincula acoes
			fApplyActions (pElement)
		}
	}
}
	
	


// Aplica aos elementos as acoes e comportamentos, com base nos atributos HTML customizados
function fApplyActions (pElement){
	
	
	// on	
	if (pElement.className.search(/s_on/) > -1){
		pElement.readOnly = false
		pElement.tabIndex = 0		
		
		pElement.onkeydown = null
		pElement.onkeyup = null
		pElement.onkeypress = null
		pElement.onclick = null
		pElement.onchange = null
		pElement.onblur = null
		pElement.onfocus = null
		
		
	}
		
	
	if (pElement.format){
	
	
		// Character Fields
		if (pElement.format.toLowerCase() == 'character'){	
			pElement.maxLength = pElement.extension
		}
		
		
	}
	
	// Campos com Transaction
	if ((pElement.transaction) && (pElement.className.search(/s_off/) == -1)){
		if (pElement.format){
			if (pElement.format.toLowerCase() == 'dropdown'){
				try {
					pElement.onchange = fDoAction
				}	catch (pError)	{}
			}
		}
		if (pElement.tagName.toLowerCase() == 'input'&&pElement.type.toLowerCase() == 'text'){
			try {
				pElement.onchange = fDoAction
			}	catch (pError)	{}
		}
		if (pElement.tagName.toLowerCase() == 'input'&&pElement.type.toLowerCase() == 'button'){
			try {
				pElement.onclick = fDoAction
			}	catch (pError)	{}
		}		
	}
	
	
			
		
	// off
	if (pElement.className.search(/s_off/) > -1){
		pElement.readOnly = true
		pElement.tabIndex = -1	
		
		pElement.onkeydown = function (){ return false }
		pElement.onkeyup = function (){ return false }
		pElement.onkeypress = function (){ return false }
		pElement.onclick = function (){ return false }
		pElement.onchange = function (){ return false }
		pElement.onblur = function (){ return false }
		pElement.onfocus = function (){ return false }
		
		
	}
		
	if (pElement.BrowserClick){
		try {

            pElement.onclick = fClickTable
			pElement.style.cursor = 'hand'
		}	catch (pError)	{}		
	}			
	
	
}


function fLocaliz (pElement){

    if (document.forms[0].AltLocaliz.value == 'yes') {
    
        if (pElement.BrowserClick){
    
            try {
                pElement.onclick = fChangeLocaliz
                pElement.style.cursor = 'hand'
    		}	catch (pError)	{}		
    	}			
    }
	
	
}



// Inclui acoes e comportamentos para todos os elementos da pagina
function fInitializeActions () {
	allFormElements = document.forms[0].elements
	for (cElem = 0; cElem < allFormElements.length; cElem++){
		fApplyActions (allFormElements[cElem])
	}
	allTDs = document.getElementsByTagName('TD')
	for (cElem = 0; cElem < allTDs.length; cElem++){
		if (allTDs[cElem].BrowserClick){
			try {
				allTDs[cElem].onclick = fClickTable
				allTDs[cElem].style.cursor = 'hand'
			}	catch (pError)	{}		
		}
	}	
}




