/*<%*/


/* DEFINE VARIABLE hServer AS HANDLE NO-UNDO. */
/* DEFINE VARIABLE ret AS LOGICAL    NO-UNDO. */
DEFINE VARIABLE aux AS CHARACTER    NO-UNDO.
DEFINE VARIABLE counter AS INTEGER    NO-UNDO.

/* Get Environment Variables from Webspeed Broker */
/*
DEFINE VARIABLE AppServerName AS CHARACTER NO-UNDO.
DEFINE VARIABLE NameServerPort AS CHARACTER NO-UNDO.
DEFINE VARIABLE HostServerIP AS CHARACTER NO-UNDO.
DEFINE VARIABLE MessageLogFile AS CHARACTER NO-UNDO.
*/
DEFINE VARIABLE Broker AS CHARACTER NO-UNDO.
DEFINE VARIABLE MessageLogFile AS CHARACTER NO-UNDO.
ASSIGN MessageLogFile =  ("C:\totvs\wrk\messagelog_teste.log").

/*
AppServerName = OS-GETENV ( "AppServerName" ).	
NameServerPort = OS-GETENV ( "NameServerPort" ).
MessageLogFile = OS-GETENV ( "MessageLogFile" ). /* C:/datasul/clientes/FastPlas/Logs/fastplas.messages.log */
HostServerIP = SERVER_NAME.
*/
Broker = ENTRY (2, AppURL, '=') /* + '10'*/ .

/* Output Messages Header*/ 
/*OUTPUT TO VALUE (MessageLogFile) APPEND.*/
PUT SKIP.
PUT SKIP.
PUT UNFORMATTED "===| " TODAY " | " STRING(TIME, "hh:mm:ss") " |=======================================================" SKIP.
OUTPUT CLOSE.

/* PROCEDURES e FUNCTIONS de Tratamento de Erro */
EMPTY TEMP-TABLE tt-erro-api.

FUNCTION fRegistraErro RETURNS LOGICAL 
	(INPUT p_error_type AS INTEGER, INPUT p_error_msg AS CHARACTER ).
	
	CREATE tt-erro-api.
	ASSIGN tt-erro-api.tipo = p_error_type
		tt-erro-api.mensagem = p_error_msg.
END FUNCTION.

PROCEDURE pOutputErrorMessages:
	IF CAN-FIND (FIRST tt-erro-api NO-LOCK) THEN DO:
		
		{&OUT} '	<div id="messagediv" name="messagediv"  BrowserClick="yes" class="pop_notify w19 non" > ' SKIP.
		{&OUT} '	<table cellpadding="0" cellspacing="0" border="0" > ' SKIP.
		{&OUT} '	<tr> ' SKIP.
		{&OUT} '	<td align="center" valign="middle" id="message" name="message"  BrowserClick="yes"> ' SKIP.
		{&OUT} '	<table cellpadding="0" cellspacing="0" border="0" class="notify_table">	' SKIP.				
		
		FOR EACH tt-erro-api NO-LOCK:						
		
			{&OUT} '	<tr> ' SKIP.
			{&OUT} '	<td id="message" name="message" align="center" valign="middle"  BrowserClick="yes" '.
			
			CASE tt-erro-api.tipo :
				WHEN 0 THEN DO:
					{&OUT} ' class="notification nsuccess" '.
				END.
				WHEN 1 THEN DO:
					{&OUT} ' class="notification nerror" '.
				END.
				WHEN 2 THEN DO:
					{&OUT} ' class="notification nadvert" '.
				END.
				OTHERWISE DO:
					{&OUT} ' class="notification" '.
				END.
			END CASE.
			
			{&OUT} ' >' tt-erro-api.mensagem '</td>' SKIP.
			{&OUT} ' </tr> ' SKIP.
			
		END.
		
		{&OUT} ' </table></td></tr></table></div> ' SKIP.
		
	END. ELSE DO:
		
		{&OUT} ' <div class="pop_notify w19 noff" id="messagediv" name="messagediv" BrowserClick="yes"> ' SKIP.
		{&OUT} ' <table cellpadding="0" cellspacing="0" border="0" class="notify_table"> ' SKIP.
		{&OUT} ' </table> ' SKIP.
		{&OUT} ' </div> ' SKIP.
		
	END.
END PROCEDURE.


/* PROCEDURES e FUNCTIONS de Conex�o de Servidor */

/*
FUNCTION fConectaServidor RETURNS LOGICAL:	
	
	CREATE SERVER hServer.
	
	ret = hServer:CONNECT("-AppService " + AppServerName + " -H " + HostServerIP + " -S " + NameServerPort) NO-ERROR.
	IF NOT ret THEN DO:
		IF ERROR-STATUS:ERROR OR ERROR-STATUS:NUM-MESSAGES > 0 THEN DO:
			fRegistraErro (1, AppServerName + "," + HostServerIP + "," + NameServerPort).
            DO counter = 1 TO ERROR-STATUS:NUM-MESSAGES:
				fRegistraErro (1, ERROR-STATUS:GET-MESSAGE(counter)).
		    END.
	    END.
	END. ELSE DO:
		IF ERROR-STATUS:ERROR OR ERROR-STATUS:NUM-MESSAGES > 0 THEN DO:
			fRegistraErro (1, AppServerName + "," + HostServerIP + "," + NameServerPort).
            DO counter = 1 TO ERROR-STATUS:NUM-MESSAGES:
				fRegistraErro (1, ERROR-STATUS:GET-MESSAGE(counter)).
		    END.
	    END. ELSE DO:
			PUT UNFORMATTED "Servidor de Execucao Conectado" SKIP.
		END.
	END.
END FUNCTION.

FUNCTION fDesconectaServidor RETURNS LOGICAL:
	IF VALID-HANDLE(hServer) THEN DO:
		IF hServer:CONNECTED() THEN DO:
			hServer:DISCONNECT() NO-ERROR.
		END.
		DELETE OBJECT hServer.
		PUT UNFORMATTED "Servidor de Execucao Desconectado" SKIP.
	END.
END FUNCTION.
*/

/* PROCEDURES E FUNCTIONS AUXILIARES */

FUNCTION fEncrypt RETURNS CHARACTER (
	INPUT pString AS CHARACTER
	):
	
    /****
	DEFINE VARIABLE vPositions AS INTEGER NO-UNDO.
	DEFINE VARIABLE eachPosition AS INTEGER NO-UNDO.
	DEFINE VARIABLE newString AS CHARACTER INITIAL "" NO-UNDO.
	DEFINE VARIABLE eachCharacter AS CHARACTER NO-UNDO.
	DEFINE VARIABLE eachCharCode AS INTEGER NO-UNDO.
	DEFINE VARIABLE eachHexCode AS INTEGER NO-UNDO.
	DEFINE VARIABLE charCodeVariation AS INTEGER INITIAL 1 NO-UNDO.
	
	vPositions = LENGTH(pString).
	
	REPEAT eachPosition = 1 TO vPositions:
		eachCharacter = SUBSTRING (pString, eachPosition, 1).
		eachCharCode = ASC(eachCharacter).
		eachCharacter = CHR (eachCharCode + charCodeVariation).
		newString = newString + eachCharacter.
		charCodeVariation = charCodeVariation + 1.
		IF charCodeVariation > 7 THEN charCodeVariation = 1.
	END .
	
	RETURN  newString + "0".
    *****/
    RETURN pString.
END FUNCTION. 

FUNCTION fDecrypt RETURNS CHARACTER (
	INPUT pString AS CHARACTER
	):
	
    /****
	DEFINE VARIABLE vPositions AS INTEGER NO-UNDO.
	DEFINE VARIABLE eachPosition AS INTEGER NO-UNDO.
	DEFINE VARIABLE newString AS CHARACTER INITIAL "" NO-UNDO.
	DEFINE VARIABLE eachCharacter AS CHARACTER NO-UNDO.
	DEFINE VARIABLE eachCharCode AS INTEGER NO-UNDO.
	DEFINE VARIABLE eachHexCode AS INTEGER NO-UNDO.
	DEFINE VARIABLE charCodeVariation AS INTEGER INITIAL 1 NO-UNDO.
	
	vPositions = LENGTH(pString) - 1.
	
	REPEAT eachPosition = 1 TO vPositions:
		eachCharacter = SUBSTRING (pString, eachPosition, 1).
		eachCharCode = ASC(eachCharacter).
		eachCharacter = CHR (eachCharCode - charCodeVariation).
		newString = newString + eachCharacter.
		charCodeVariation = charCodeVariation + 1.
		IF charCodeVariation > 7 THEN charCodeVariation = 1.
	END .
	
	RETURN  newString.
    *****/
    RETURN pString.
END FUNCTION. 


FUNCTION DEC2BASE RETURNS CHARACTER
	( pDecValue AS DECIMAL, pBase AS INTEGER, pFixed AS INTEGER ) :
	
	/* BASE MAXIMA = 48 */

	DEFINE VARIABLE REMAINDER AS INTEGER NO-UNDO.
	DEFINE VARIABLE cTT AS CHARACTER NO-UNDO.  
	DEFINE VARIABLE vChars AS CHARACTER NO-UNDO.
	
	vChars = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,m,n,o,p,q,r,s,t,u,v,w,x,y,z,�,_,$,#,-,@,&,%,!,^,~,+,(,),[,],|".
	
	cTT = "".
	REMAINDER = 1.
    
	DO WHILE (pDecValue > 0 OR pFixed > 0):        
    		REMAINDER =  INTEGER( 
                        TRUNCATE((
                                (
                                 (pDecValue /  pBase) 
                                 - TRUNCATE ((pDecValue /  pBase),0) 
                                  ) * pBase
                            ),0)
                    ).

    
		pDecValue = TRUNCATE( (pDecValue / pBase) ,0).
		MESSAGE pDecValue.
		cTT = ENTRY( REMAINDER + 1, vChars, ",") + cTT.
   		pFixed = pFixed - 1 .
	END.
	RETURN cTT.
END FUNCTION.

/*
DEFINE VARIABLE cRoot AS CHARACTER NO-UNDO.

PUT UNFORMATTED PATH_INFO SKIP. /* /WService=fastplas/esdcw001b */
PUT UNFORMATTED SelfURL SKIP. /* /scripts/cgiip.exe/WService=fastplas/esdcw001b */
PUT UNFORMATTED HostURL SKIP. /* http://10.1.30.25 */
PUT UNFORMATTED AppProgram SKIP. /* esdcw001b */
PUT UNFORMATTED AppURL SKIP. /* /scripts/cgiip.exe/WService=fastplas */
*/



