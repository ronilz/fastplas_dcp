
/* ######################################################################
fdcw001_006 - Autorizacao
#######################################################################*/

function fLoadRoutine () {
	fInitializeActions()
	fCreateListener()
	fStartTimer()
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}
}


//tib 30/04/2022
function fStartTimer(){
	if (document.forms[0].leitura.value.search (/\!/) > -1 ) {
		// Separa as partes do login e alimenta a tela caller
		
		if (document.forms[0].leitura.value.indexOf("#") > -1) { 
			pList = document.forms[0].leitura.value.split('#')
		} else { 
			pList = ""
		}
		
		varWinName = window.dialogArguments
		if (varWinName){
			with (varWinName){
				document.forms[0].auth_login.value = pList[0]
				document.forms[0].auth_pass.value = pList[1].replace (/!/, '')
			}
			window.close()
		}	
	} else {
		document.forms[0].leitura.focus()
		setTimeout(fStartTimer,50);
	}
}


function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}	
	switch (pName){
		case "cancelar":	
			window.close()
		break;
	}	
}
