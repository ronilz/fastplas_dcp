
/* ######################################################################
fdcw001_005 - Selecao de Rack
#######################################################################*/

function fLoadRoutine () {
	fInitializeActions()
	fCreateListener()
	fStartTimer()
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}
}


function fStartTimer(){
	if (document.forms[0].leitura.value.length >= 13) {
		fDisableElement (document.forms[0].leitura)
		fValidateRack()
		document.forms[0].leitura.value=''
	}else{
		document.forms[0].leitura.focus()
		setTimeout(fStartTimer,50);
	}
}

function fValidateRack(){
	// Valida o Rack para prosseguir
	// /scripts/cgiip.exe/WService=" + Broker + "/
	pUrl = "fdcw001_005a"
	pNVPs = ''
	pCallback = fValidateCallback
	pElement = document.getElementById('leitura')
	fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
}

function fValidateCallback (hasError, pElement, pReturnData){
	if (!(hasError)){
		if (pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild){
			// "/scripts/cgiip.exe/WService=" + Broker + "/"
			pUrl = ''
			pUrl = pUrl + document.forms[0].action.value
			if (document.forms[0].action.value.search(/\?/) == -1){
				pUrl = pUrl + "?"
			}else{
				pUrl = pUrl + "&"
			}
			pUrl = pUrl + "rack=" + pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
			document.location.href = pUrl
		}
	}else{
		document.forms[0].leitura.value = ''
		fEnableElement (document.forms[0].leitura)
		fStartTimer()
	}
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}	
	switch (pName){
		case "cancelar":
			//	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
	}	
}

function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
		
	if ( pName == "message" ) {
		fClearErrors()
	}
}







