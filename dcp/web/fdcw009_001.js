


/* ######################################################################
fdcw009_001 - Apontamento de Pintura
#######################################################################*/


function fLoadRoutine () {
	fInitializeActions()
	//ocument.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
	fCreateListener()
	fStartTimer()
}


function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].confirmar.className.search (/s_off/) == -1){
			fDoAction('confirmar')
		}
	}
}


function fStartTimer(){
	if (document.forms[0].leitura.value.length >= 13) {
		fRefreshItemList()
		document.forms[0].leitura.value = ''
	}else{
		document.forms[0].leitura.focus()
	}
	setTimeout(fStartTimer,50);
}
*/


function fRefreshItemList(){
	// Verifica se o item ja existe no browser	
	if (document.getElementById('browserrow_' + document.forms[0].leitura.value)){
		fRegistraErro (1, 'Item j� foi selecionado')
	}else{
	
		// Adiciona o item na lista
		if (document.forms[0].leitura.value != ''){	
			newrow = browser_produto.insertRow(1)
			newrow.name = 'browserrow_' + document.forms[0].leitura.value
			newrow.id = 'browserrow_' + document.forms[0].leitura.value
			newrow.valid = "no"
			newrow.pRetValue = document.forms[0].leitura.value
			newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].leitura.value
			newcell.appendChild (document.createTextNode(document.forms[0].leitura.value))
			newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = document.forms[0].leitura.value
			newcell.appendChild (document.createTextNode(""))		
		}
	
		document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
		
		// Invoca a valida��o do item
		fDisableElement (document.forms[0].confirmar)
		// /scripts/cgiip.exe/WService=" + Broker + "/
		pUrl = "fdcw009_001a"
		pNVPs = ''
		pCallback = fValidateBrowser
		pElement = document.getElementById('browser_produto')
		fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
	}
}


function fValidateBrowser (hasError, pElement, pReturnData){
	if (pReturnData[0]){
		if (pReturnData[0].childNodes.length > 0) {
			if (!(hasError)) {
				// Altera textos, cor e propriedade do item no browser
				itemCode = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
				itemTR = document.getElementById ('browserrow_' + itemCode)
				itemTR.valid = "yes"						
				itemTR.cells[0].className = "browser_row b1"	
				itemTR.cells[1].className = "browser_row b1"	
				if (pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild){
					itemTR.cells[1].childNodes[0].data = pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild.data
				}else{
					itemTR.cells[1].childNodes[0].data = ''
				}
				fApplyActions (itemTR.cells[0])
				fApplyActions (itemTR.cells[1])
			}else{
				// Remove item errado
				itemCode = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
				itemTR = document.getElementById ('browserrow_' + itemCode)
				browser_produto.deleteRow(itemTR.rowIndex)				
			}
			
			// Reativa ou n�o o bot�o de confirma��o
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
			
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
			
		}
	}
}


function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":
			// 	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			fDisableElement (document.forms[0].confirmar)	
			// Monta a lista de itens
			pNVPs = '&ItemCodes='
			isFirst = true
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].pRetValue){
					if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
					pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
				}
			}
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw009_001b"
			pCallback = fTrataEnvio
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fTrataEnvio(hasError, pElement, pReturnData, pErrors){
	hasSuccess = false
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
			alert ('Apontamento Realizado com Sucesso')
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl				
		}
	}
	fEnableElement (document.forms[0].confirmar)
}

function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
	
	if (pElement.pRetValue){
		if (confirm("Por favor, confirme a exclus�o do item " + pElement.pRetValue)){
			
			fDisableElement (document.forms[0].confirmar)
			
			pElement.parentNode.parentNode.deleteRow(pElement.parentNode.rowIndex)
			
			
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
				
			
		}
	}
}




