


/* ######################################################################
fdcw004_001 - Reporte de Produ��o
#######################################################################*/


function fLoadRoutine () {
	fInitializeActions()
	
	if (document.forms[0].juncao){
		document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 2)/2
	} else {
		document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
	}


    if (document.forms[0].itemAlt.value != '') {

        pValue = document.forms[0].ListItem.value

        pValue1 = ''
        pValue2 = ''

           
        for (iCont=0; iCont<pValue.length; iCont++){

            if (pValue.charAt(iCont) == ',') {
                pValue2 = pValue1 + ',' + pValue2
                pValue1 = ''
            }
            else {
                
                pValue1 = pValue1 + pValue.charAt(iCont)
                if (pValue1=='') {
                    document.forms[0].itemAlt.value = ''
                    document.forms[0].NewLocaliz.value = ''
                    break;
                }
            }
        }
            
        pValue = pValue2
        
        pValue1 = ''
        iJuncao = 0
        cJuncao = ''
        
        for (iCont=0; iCont<pValue.length; iCont++){

            if (pValue.charAt(iCont) == ',') {

                if (document.forms[0].tipo_pto.value == 'Jun��o') {

                    if (pValue1!='') {
                    
                       iJuncao = iJuncao + 1
                                
                       if (iJuncao == 1) {
                          cJuncao = pValue1
                            
                        }
                        else {
                            document.forms[0].leitura.value = pValue1
                            fStartTimer()
                            document.forms[0].juncao.value = cJuncao
                            fStartTimer()
                            cJuncao = ''
                            iJuncao = 0
                        }
                        
                    }

                }
                else {
                    document.forms[0].leitura.value = pValue1
                }

                fStartTimer()
                pValue1 = ''
            }
            else {
                
                pValue1 = pValue1 + pValue.charAt(iCont)
                if (pValue1=='') {
                    document.forms[0].itemAlt.value = ''
                    document.forms[0].NewLocaliz.value = ''
                    break;
                }
            }
        }
    }

	fCreateListener()
	fStartTimer()
}


function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
	if (document.forms[0].juncao) { document.forms[0].juncao.onkeyup = fTrapKeys }
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].confirmar.className.search (/s_off/) == -1){
			fDoAction('confirmar')
		}
	}
}



function fStartTimer(){

    if (document.forms[0].juncao){
		if (document.forms[0].leitura.className.search(/s_on/) > -1){
			if (document.forms[0].leitura.value.length >= 13) {
				fEnableElement (document.forms[0].juncao)
				document.forms[0].juncao.focus()
				fDisableElement (document.forms[0].leitura)
				setTimeout(fStartTimer,50);
				// fRefreshItemList()
				// document.forms[0].leitura.value = ''
			}else{
				document.forms[0].leitura.focus()
				setTimeout(fStartTimer,50);
			}
		}
		if (document.forms[0].juncao.className.search(/s_on/) > -1){
			if (document.forms[0].juncao.value.length >= 13) {
				fEnableElement (document.forms[0].leitura)
				document.forms[0].leitura.focus()
				fDisableElement (document.forms[0].juncao)
				setTimeout(fStartTimer,50);
				fRefreshItemList()
				document.forms[0].leitura.value = ''
				document.forms[0].juncao.value = ''
			}else{
				document.forms[0].juncao.focus()
				setTimeout(fStartTimer,50);
			}
		}
	}else{
		if (document.forms[0].leitura.value.length >= 13) {
			fRefreshItemList()
			document.forms[0].leitura.value = ''
		}
		document.forms[0].leitura.focus()
		setTimeout(fStartTimer,50);
	}
}

function fRefreshItemList(){

    if (!document.forms[0].juncao){
		// Verifica se o item ja existe no browser	
		if (document.getElementById('browserrow_' + document.forms[0].leitura.value)){
		
			fRegistraErro (1, 'Item j� foi selecionado')
			
		}else{		

            // Adiciona o item na lista
			if (document.forms[0].leitura.value != ''){	
				newrow = browser_produto.insertRow(1)
				newrow.name = 'browserrow_' + document.forms[0].leitura.value
				newrow.id = 'browserrow_' + document.forms[0].leitura.value
				newrow.valid = "no"
				newrow.pRetValue = document.forms[0].leitura.value
				newcell = newrow.insertCell (newrow.cells.length)
				newcell.BrowserClick = "yes"
				newcell.className = "browser_row b2"	
				newcell.pRetValue = document.forms[0].leitura.value
				newcell.appendChild (document.createTextNode(document.forms[0].leitura.value))
				newcell = newrow.insertCell (newrow.cells.length)
				newcell.BrowserClick = "yes"
				newcell.className = "browser_row b2"	
				newcell.pRetValue = document.forms[0].leitura.value
				newcell.appendChild (document.createTextNode(""))		

                newcell = newrow.insertCell (newrow.cells.length)
				newcell.BrowserClick = "yes"
				newcell.className = "browser_row b2"	
				newcell.pRetValue = document.forms[0].leitura.value
				newcell.appendChild (document.createTextNode(""))  
                
			}
		
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 2
			
			// Invoca a valida��o do item
			fDisableElement (document.forms[0].confirmar)
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl = "fdcw004_001a"
			pNVPs = ''
			pCallback = fValidateBrowser
			pElement = newrow
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		} 
	} else {
		if (
			(document.getElementById('browserrow_' + document.forms[0].leitura.value + document.forms[0].juncao.value))||
			(document.getElementById('browserrow_' + document.forms[0].juncao.value + document.forms[0].leitura.value))||
			(document.getElementById('browserrow_' + document.forms[0].juncao.value))||
			(document.getElementById('browserrow_' + document.forms[0].leitura.value))
			){                       
			fRegistraErro (1, 'Jun��o j� foi selecionada...')
		}else{		

			// Adiciona o item na lista
			if ((document.forms[0].leitura.value != '')&&(document.forms[0].juncao.value != '')){	

                lowerrow = browser_produto.insertRow(1)	
				upperrow = browser_produto.insertRow(1)			
				upperrow.name = 'browserrow_' + document.forms[0].leitura.value + document.forms[0].juncao.value
				lowerrow.name = 'browserrow_' + document.forms[0].leitura.value + document.forms[0].juncao.value
				upperrow.id = 'browserrow_' + document.forms[0].leitura.value + document.forms[0].juncao.value
				lowerrow.id = 'browserrow_' + document.forms[0].leitura.value + document.forms[0].juncao.value
				upperrow.valid = "no"
				lowerrow.valid = "no"
				upperrow.pRetValue = document.forms[0].leitura.value + ';' + document.forms[0].juncao.value
				lowerrow.pRetValue = document.forms[0].leitura.value + ';' + document.forms[0].juncao.value
				uppercell = upperrow.insertCell (upperrow.cells.length)
				lowercell = lowerrow.insertCell (lowerrow.cells.length)
				middlecell = upperrow.insertCell (upperrow.cells.length)
                
                localizcell = upperrow.insertCell (upperrow.cells.length)

				middlecell.rowSpan = 2
                localizcell.rowSpan = 2
								
				uppercell.BrowserClick = "yes"
				lowercell.BrowserClick = "yes"
				middlecell.BrowserClick = "yes"
                localizcell.BrowserClick = "yes"
				
				uppercell.className = "browser_row b2"
				lowercell.className = "browser_row b2"
				middlecell.className = "browser_row b2"
                localizcell.className = "browser_row b2"
				
				uppercell.pRetValue = document.forms[0].leitura.value + ',' + document.forms[0].juncao.value
				lowercell.pRetValue = document.forms[0].leitura.value + ',' + document.forms[0].juncao.value
				middlecell.pRetValue = document.forms[0].leitura.value + ',' + document.forms[0].juncao.value
                localizcell.pRetValue = document.forms[0].leitura.value + ',' + document.forms[0].juncao.value
				
				uppercell.appendChild (document.createTextNode(document.forms[0].leitura.value))
				lowercell.appendChild (document.createTextNode(document.forms[0].juncao.value))
				middlecell.appendChild (document.createTextNode(" "))
                localizcell.appendChild (document.createTextNode(" "))
                
			}
			
			document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 2)/2
	
			// Invoca a valida��o do item
			fDisableElement (document.forms[0].confirmar)
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl = "fdcw004_001a"
			pNVPs = ''
			pCallback = fValidateBrowser
			pElement = upperrow
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)		
		}
	}
}

function fValidateBrowser (hasError, pElement, pReturnData){
	if (document.forms[0].juncao){

        if (pReturnData[0]){
			if (pReturnData[0].childNodes.length > 0) {
				if (!(hasError)) {
					// Altera textos, cor e propriedade do item no browser


                    itemTR = pElement
					itemTR2 = document.getElementById('browser_produto').rows[pElement.rowIndex + 1]
                    itemTR3 = document.getElementById('browser_produto').rows[pElement.rowIndex + 2]

                    itemTR.valid = "yes"	
					itemTR2.valid = "yes"											
                    itemTR3.valid = "yes"											


                    itemTR.cells[0].className = "browser_row b1"	
					itemTR.cells[1].className = "browser_row b1"  
                    itemTR.cells[2].className = "browser_row b1"
                    /* mpn */
                    itemTR2.cells[0].className = "browser_row b1"	
                    itemTR3.cells[0].className = "browser_row b1"	
                        

					if (pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild) {
						pCodigo = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
					}else{
						pCodigo = ''
					}
					if (pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild) {
						pDescricao = pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild.data
					}else{
						pDescricao = ''
					}

                    if (pReturnData[0].getElementsByTagName('LOCALIZ')[0].firstChild) {
						pLocaliz = pReturnData[0].getElementsByTagName('LOCALIZ')[0].firstChild.data
					}else{
						pLocaliz = ''
					}

                    itemTR.pRetValue = itemTR.pRetValue + ';' + pCodigo	
					itemTR2.pRetValue = itemTR2.pRetValue + ';' + pCodigo	

                    itemTR.pRetValue = itemTR.pRetValue + ';;' +  pLocaliz  
                    itemTR2.pRetValue = itemTR2.pRetValue + ';;' +  pLocaliz  

					itemTR.cells[1].innerHTML = pCodigo + "<BR>" + pDescricao  
                    itemTR.cells[2].innerHTML =  pLocaliz
					fApplyActions (itemTR.cells[0])
					fApplyActions (itemTR.cells[1])
					fApplyActions (itemTR2.cells[0])


                    if (typeof itemTR.cells[2] != "undefined") {
                        fLocaliz (itemTR.cells[2]) 
                    }

                    
                    

				}else{
					// Remove item errado
					document.getElementById('browser_produto').deleteRow(pElement.rowIndex + 1)	
					document.getElementById('browser_produto').deleteRow(pElement.rowIndex)				
				}
				
				// Reativa ou n�o o bot�o de confirma��o
				allOK = false
				for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
					if (document.getElementById('browser_produto').rows[i].valid){
						if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
						if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
					}
				}
				if (allOK) { fEnableElement (document.forms[0].confirmar) }
				
				document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 2)/2
				
			}
		}
	}else{
        
		if (pReturnData[0]){
			if (pReturnData[0].childNodes.length > 0) {
				if (!(hasError)) {


                    // Altera textos, cor e propriedade do item no browser
					itemTR = pElement
					itemTR.valid = "yes"	
					if (pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild) {
						pCodigo = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
					}else{
						pCodigo = ''
					}	

                    if (pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild) {
						pDescricao = pReturnData[0].getElementsByTagName('DESCRICAO')[0].firstChild.data
					}else{
						pDescricao = ''
					}

                    if (pReturnData[0].getElementsByTagName('LOCALIZ')[0].firstChild) {
						pLocaliz = pReturnData[0].getElementsByTagName('LOCALIZ')[0].firstChild.data
					}else{
						pLocaliz = ''
					}


                    itemTR.pRetValue = itemTR.pRetValue + ';;' + pCodigo + ';;' +  pLocaliz   
					itemTR.cells[0].className = "browser_row b1"	
					itemTR.cells[1].className = "browser_row b1"	
                    itemTR.cells[2].className = "browser_row b1"
                                /* mpn */
                    
                    
                    itemTR.cells[1].childNodes[0].data = pDescricao	
                    itemTR.cells[2].childNodes[0].data = pLocaliz	
					fApplyActions (itemTR.cells[0])
					fApplyActions (itemTR.cells[1])


                    if (typeof itemTR.cells[2] != "undefined") {
                        fLocaliz (itemTR.cells[2])
                    }

                    

                    
				}else{
					// Remove item errado
					document.getElementById('browser_produto').deleteRow(pElement.rowIndex)				
				}
				
				// Reativa ou n�o o bot�o de confirma��o
				allOK = false
				for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
					if (document.getElementById('browser_produto').rows[i].valid){
						if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
						if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
					}
				}
				if (allOK) { fEnableElement (document.forms[0].confirmar) }
				
				document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 2)
				
			}
		}
	}
}



function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
        
		case "cancelar":	
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			fDisableElement (document.forms[0].confirmar)	
			// Monta a lista de itens
			pNVPs = '&ItemCodes='
			isFirst = true
			LastValue = ''
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){

                if (document.getElementById('browser_produto').rows[i].pRetValue){
					if (document.getElementById('browser_produto').rows[i].pRetValue != LastValue) {
						if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
						pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
                            
					}
					LastValue = document.getElementById('browser_produto').rows[i].pRetValue
				}
			}
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw004_001b"
			pCallback = fTrataRetorno
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fTrataRetorno (hasError, pElement, pReturnData, pErrors){
	hasSuccess = false
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild){
			if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
				alert ('Reporte Realizado com Sucesso')
				// /scripts/cgiip.exe/WService=" + Broker + "/
				pUrl="fdcw001_001"
				document.location.href=pUrl 
			}
		}
	}
	fEnableElement (document.forms[0].confirmar)
}


function fChangeLocaliz (pObjRef){
    
    if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
	
    if (pElement.pRetValue){


        pItms = '&ItemList='
        isFirst = true
        LastValue = ''
        for (i=0; i<document.getElementById('browser_produto').rows.length; i++){

            if (document.getElementById('browser_produto').rows[i].pRetValue){
                if (document.getElementById('browser_produto').rows[i].pRetValue != LastValue) {
                    if (isFirst){ isFirst = false }else{ pItms = pItms + ',' }
                    pItms = pItms + document.getElementById('browser_produto').rows[i].pRetValue
                        
                }
                LastValue = document.getElementById('browser_produto').rows[i].pRetValue
            }
        }

        pUrl = "fdcw004_001c"
        pNVPs = ''
        pCallback = fRetornoChangeLocaliz
        fExecuteAjax (pUrl, pItms, pCallback, pElement)

        pNVPs = ''
        isFirst = true
        LastValue = ''

        pValue1 = ''

        for (i=0; i<document.getElementById('browser_produto').rows.length; i++){

            if (document.getElementById('browser_produto').rows[i].pRetValue){
                if (document.getElementById('browser_produto').rows[i].pRetValue != LastValue) {
                    if (isFirst){ isFirst = false} else{ pNVPs = pNVPs + ',' }

                    pValue = document.getElementById('browser_produto').rows[i].pRetValue
                    iEnt = 0
                        
                    for (iCont=0; iCont<pValue.length; iCont++){
                        
                        if (pValue.charAt(iCont) == ';') {
                            if (document.forms[0].tipo_pto.value == 'Jun��o') {
                                iEnt = iEnt + 1

                               if (iEnt == 2)  {
                                   break;
                               }

                               pValue1 = pValue1 + ','
                               
                            }
                            else {
                                break;
                            }
                            
                        }
                        
                        else {
                            
                            pValue1 = pValue1 + pValue.charAt(iCont)
                        }
                    }
                    
                }
                LastValue = document.getElementById('browser_produto').rows[i].pRetValue
                pValue1 = pValue1 + ','
                    
            }
        }

        pUrl="fdcw004_001"
        pUrl = pUrl + document.forms[0].pParam.value
        pUrl = pUrl + '&DisableDepos=yes&ItemAlt=' + pElement.pRetValue + '&ListItem=' + pValue1
        document.location.href=pUrl
    }
 
	
}

function fRetornoChangeLocaliz () {
}


function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
	
	if (pElement.pRetValue){
		if (confirm("Por favor, confirme a exclus�o do item " + pElement.pRetValue)){
			
			fDisableElement (document.forms[0].confirmar)
            document.forms[0].itemAlt.value = ''
			
			if (document.forms[0].juncao){
				eachId = pElement.parentNode.id
				
				eachRow = document.getElementById(eachId)
				eachIndex = eachRow.rowIndex
				document.getElementById('browser_produto').deleteRow(eachIndex)
				
				eachRow = document.getElementById(eachId)
				eachIndex = eachRow.rowIndex
				document.getElementById('browser_produto').deleteRow(eachIndex)
				
				document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 2)/2
		
			}else{
				pElement.parentNode.parentNode.deleteRow(pElement.parentNode.rowIndex)
				
				document.forms[0].contagem.value = (document.getElementById('browser_produto').rows.length - 2)
			}
			
			
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
		}
			
	}
}




