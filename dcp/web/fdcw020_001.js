


/* ######################################################################
fdcw009_001 - Apontamento de Pintura
#######################################################################*/


function fLoadRoutine () {
    fInitializeActions()
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].confirmar.className.search (/s_off/) == -1){
			fDoAction('confirmar')
		}
	}
}


function fStartTimer(){
	
}

function fRefreshItemList(){
	
}


function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":
			// 	/scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
        case "confirmar":
                
            fDisableElement (document.forms[0].confirmar)	
			// Monta a lista de itens
			
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pNVPs=''
            pUrl="fdcw020_002"
			pCallback = fTrataEnvio
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fTrataEnvio(hasError, pElement, pReturnData, pErrors){

    hasSuccess = false
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
			alert ('Processamento Realizado com Sucesso. Verifique a situa��o dos registros.')
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl				
		}
	}
	fEnableElement (document.forms[0].confirmar)
}


function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}

}
