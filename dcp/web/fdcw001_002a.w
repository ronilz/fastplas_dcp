/* ####################################################################
fdcw001_002a - AJAX Efetua Login
#######################################################################*/

{esapi\esapi001.p}

CREATE WIDGET-POOL.
DEFINE VARIABLE c-user AS CHARACTER NO-UNDO.
DEFINE VARIABLE c-pass AS CHARACTER NO-UNDO.
DEFINE VARIABLE l-ok AS LOGICAL INITIAL TRUE NO-UNDO.
DEFINE VARIABLE counter AS INTEGER NO-UNDO.
DEFINE STREAM str-1.

{src/web2/wrap-cgi.i}


DEFINE VARIABLE Broker AS CHARACTER NO-UNDO.

Broker = ENTRY (2, AppURL, '=').

FUNCTION fRegistraErro RETURNS LOGICAL 
	(INPUT p_error_type AS INTEGER, INPUT p_error_msg AS CHARACTER ).
	
	CREATE tt-erro-api.
	ASSIGN tt-erro-api.tipo = p_error_type
		tt-erro-api.mensagem = p_error_msg.
END FUNCTION.

RUN valida-usuario.

RUN process-web-request.

IF l-ok THEN DO:
	
	{&OUT} '<script language="JavaScript">' SKIP.
     {&OUT} 'parent.document.location.href = "fdcw001_001"' SKIP.
	{&OUT} '</script>' SKIP.
	
END. ELSE DO:

	{&OUT} '<script language="JavaScript">' SKIP.
	FOR EACH tt-erro-api NO-LOCK:
		{&OUT} "parent.fRegistraErro (" + string(tt-erro-api.tipo) + ", '" + tt-erro-api.mensagem + "')" SKIP.
	END.	
	{&OUT} 'parent.fEnableElement(parent.document.forms[0].leitura)' SKIP.
	{&OUT} 'parent.fStartTimer()' SKIP.
	{&OUT} '</script>' SKIP.

END.

FUNCTION fEncrypt RETURNS CHARACTER (
	INPUT pString AS CHARACTER
	):
 
	RETURN pString.
	
END FUNCTION. 

FUNCTION fDecrypt RETURNS CHARACTER (
	INPUT pString AS CHARACTER
	):
	
    RETURN pString.
END FUNCTION. 


PROCEDURE valida-usuario :

	IF NUM-ENTRIES (GET-FIELD('leitura'), '#') = 2 THEN DO:
		c-user = ENTRY (1, GET-FIELD('leitura'), '#').
		c-pass =  TRIM ( ENTRY (2, GET-FIELD('leitura'), '#') , '~!').	
		DO ON ERROR UNDO, LEAVE:  
			
            RUN pi-valida-usuario (INPUT c-user, INPUT c-pass, OUTPUT TABLE tt-erro-api).

		END.
		
	END. ELSE DO:
		fRegistraErro (1, 'Autenticação Inválida').
		l-ok = FALSE.
	END.
			
	IF CAN-FIND (FIRST tt-erro-api WHERE tt-erro-api.tipo = 1 NO-LOCK) THEN l-ok = FALSE.
        
END PROCEDURE.


PROCEDURE process-web-request :
    DEFINE VARIABLE c-ret AS CHARACTER NO-UNDO.
    DEFINE VARIABLE i-cont AS INTEGER NO-UNDO.

    RUN outputHeader.
  
    {&OUT}
        "<HTML>":U SKIP
        "<HEAD>":U SKIP
        "<TITLE> {&FILE-NAME} </TITLE>":U SKIP
        "</HEAD>":U SKIP
        "<BODY>":U SKIP
        "</BODY>":U SKIP
        "</HTML>":U SKIP
        .  
END PROCEDURE.


PROCEDURE outputHeader :
       
   IF l-ok THEN
   DO: 
       fRegistraErro (0, "Login OK").
       RUN setCookie IN web-utilities-hdl (INPUT "cod_usu_websession":U,
                                           INPUT c-user,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
       RUN setCookie IN web-utilities-hdl (INPUT "sessionID",
                                           INPUT ENTRY(2,WEB-CONTEXT:EXCLUSIVE-ID,"="),
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
       RUN setCookie IN web-utilities-hdl (INPUT "psw_usu_websession":U,
                                           INPUT fEncrypt(c-pass),
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
										   
	/*									   
	SECURITY-POLICY:SYMMETRIC-ENCRYPTION-ALGORITHM = "AES_CBC_128".
	SECURITY-POLICY:PBE-KEY-ROUNDS = 1000. 
	SECURITY-POLICY:PBE-HASH-ALGORITHM = "MD5".
	DEFINE VARIABLE vEncryptionKey AS CHARACTER NO-UNDO.
	DEFINE VARIABLE vEncryptedPassword AS CHARACTER NO-UNDO.
	vEncryptionKey = GENERATE-PBE-KEY('fastplas') .
	vEncryptedPassword = ENCRYPT ( c-pass, vEncryptionKey ) .
	PUT UNFORMATTED DECRYPT ( c-pass, vEncryptionKey ).
	*/

   END.
   ELSE DO:
       RUN setCookie IN web-utilities-hdl (INPUT "cod_usu_websession":U,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
       RUN setCookie IN web-utilities-hdl (INPUT "sessionID",
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
       RUN setCookie IN web-utilities-hdl (INPUT "psw_usu_websession":U,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?,
                                           INPUT ?).
   	
   END.
   output-content-type ("text/html":U).
  
END PROCEDURE.

