

/* ######################################################################
fdcw030_008 - Sele��o de RACK COR
#######################################################################*/

function fLoadRoutine () {
	fInitializeActions()
	fCreateListener()
}

function fCreateListener(){
	document.forms[0].rack.onkeyup = fTrapRackKeys
    document.forms[0].cor.onkeyup = fTrapCorKeys
}

function fTrapRackKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}

    if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}

    if (document.forms[0].rack.value.length >= 13) {
        fValidateRack('rack')
	}
}

function fTrapCorKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}

    if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}

    if (document.forms[0].cor.value.length >= 6) {
        fValidateRack('cor')
	}
}



function fDoAction (pObjRef){

    if (!pObjRef){

        pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{

        pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}	
	switch (pName){
        case "cancelar":	

            // /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
    
        
	}	
}



function fValidateRack(cObj){

    
	// Valida o Rack para prosseguir
	// /scripts/cgiip.exe/WService=" + Broker + "/
	pUrl = "fdcw030_008a"
	pNVPs = ''
    if (cObj=='rack') {
        pCallback = fValidateCallbackRack
    }
    else {
        pCallback = fValidateCallbackCor
    }
	
	pElement = document.getElementById(cObj)
	fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
}



function fValidateCallbackRack (hasError, pElement, pReturnData){
    
	if (!(hasError)){
		document.forms[0].cor.focus()
		}
	else{
		document.forms[0].rack.value = ''
        
	}
    
}   


function fValidateCallbackCor (hasError, pElement, pReturnData){
    
	if (!(hasError)){
		
        // "/scripts/cgiip.exe/WService=" + Broker + "/"
			pUrl = ''
			pUrl = pUrl + document.forms[0].action.value
			if (document.forms[0].action.value.search(/\?/) == -1){
				pUrl = pUrl + "?"
			}else{
				pUrl = pUrl + "&"
			}
            
            pUrl = pUrl + "cor=" + document.forms[0].cor.value + '&rack=' + document.forms[0].rack.value
            
            document.location.href = pUrl

		}
	else{
		document.forms[0].cor.value = ''
        document.forms[0].cor.focus = ''
        
	}
    
} 

function fClickTable (pObjRef) {

    if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}		
	if ( pName == "message" ) {
		fClearErrors()
	}
}
 



