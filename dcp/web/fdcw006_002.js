


/* ######################################################################
fdcw006_002 - Desmontagem de Rack
#######################################################################*/


function fLoadRoutine () {
	fInitializeActions()
	document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
	fCreateListener()
	fStartTimer()
}

function fCreateListener(){
	document.forms[0].leitura.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}
}

function fStartTimer(){
	if (document.forms[0].leitura.value.length >= 13) {
		fIncluiItem()
		document.forms[0].leitura.value = ''
	}
	document.forms[0].leitura.focus()
	setTimeout(fStartTimer,50);
}


function fIncluiItem(){

	// Verifica se o item ja existe no browser	
	itemLine = document.getElementById('browserrow_' + document.forms[0].leitura.value)
	if (!itemLine){
		fRegistraErro (1, 'Item nao existe no Rack')
	}else{
		// excluir o item da lista
		itemLine.className = "browser_row b2"
			
		// Invoca a validao do item
		document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
		fDisableElement (document.forms[0].confirmar)
		// /scripts/cgiip.exe/WService=" + Broker + "/
		pUrl = "fdcw006_002a"
		pNVPs = ''
		pCallback = fValidateBrowser
		pElement = document.getElementById('browser_produto')
		fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
	}
}

function fValidateBrowser (hasError, pElement, pReturnData, pErrors){
	if (pReturnData[0]){
		if (pReturnData[0].childNodes.length > 0) {
			itemCode = pReturnData[0].getElementsByTagName('CODIGO')[0].firstChild.data
			itemTR = document.getElementById ('browserrow_' + itemCode)
			if (!(hasError)) {
				// Remove item do browser
				browser_produto.deleteRow(itemTR.rowIndex)
			}else{
				// Desfaz ameaca de exclusao
				itemTR.cells[0].className = "browser_row b1"		
			}
			
			// Reativa ou nao o botao de confirmacao
			/*
			allOK = false
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].valid){
					if (document.getElementById('browser_produto').rows[i].valid == "yes"){allOK = true}
					if (document.getElementById('browser_produto').rows[i].valid == "no"){allOK = false}
				}
			}
			if (allOK) { fEnableElement (document.forms[0].confirmar) }
			*/
			fEnableElement (document.forms[0].confirmar)
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
			
		}
	}
}


function fTrataEnvio(hasError, pElement, pReturnData, pErrors){
	hasSuccess = false
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
			alert ('Desmontagem Realizada com Sucesso')
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl				
		}
	}
	fEnableElement (document.forms[0].confirmar)
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":	
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			fDisableElement (document.forms[0].confirmar)	
			// Monta a lista de itens
			pNVPs = '&ItemCodes='
			isFirst = true
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].pRetValue){
					if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
					pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
				}
			}
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw006_002b"
			pCallback = fTrataEnvio
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	}
	
}

