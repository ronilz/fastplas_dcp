


/* ######################################################################
fdcw006_001 - Montagem de Rack
#######################################################################*/


function fLoadRoutine () {
	fInitializeActions()
	document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
	fCreateListener()
	fStartTimer()
}

function fCreateListener(){
	document.forms[0].leitura1.onkeyup = fTrapKeys
	document.forms[0].leitura2.onkeyup = fTrapKeys
}

function fTrapKeys(){
	if (event.keyCode==27){
		fDoAction('cancelar')
	}
	if (event.keyCode==13){
		if (document.forms[0].cancelar.className.search (/s_off/) == -1){
			fDoAction('cancelar')
		}
	}
}

function fStartTimer(){
	
	if (document.forms[0].leitura1.value.length >= 13 && document.forms[0].leitura2.value == '') {
		document.forms[0].leitura2.focus()
	}
	
	if (document.forms[0].leitura2.value.length >= 13) {
		fIncluiItem()
		document.forms[0].leitura2.value = ''
		document.forms[0].leitura1.value = ''
		document.forms[0].leitura1.focus()
	}
	setTimeout(fStartTimer,50);
	
}


function fIncluiItem(){

	// Verifica se o item ja existe no browser	
	TipoErro = 0
	hasError = false
	
	pUrl = "fdcw006a_001a"
	pNVPs = ''

	// alteracao: passar todos os itens do browser
	pNVPs = '&ItemCodes='
	var isFirst = true
	for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
		if (document.getElementById('browser_produto').rows[i].pRetValue){
			if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
			pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
		}
	}
		
	pCallback = fListaItens
	pElement = document.getElementById('browser_produto')
	fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
	
	}

function fListaItens (hasError, pElement, pReturnData, pErrors){
	
	// se não houver erros
	if (pErrors.length == 0){
		
		// buscar todo conteúdo de rgs
		for (var i = 0; i<pReturnData.length; i++) {
			
			// incluir rg no browser de produtos
			var rgrow = pReturnData[i].getElementsByTagName('CODIGO')[0].firstChild.data
			var rgdesc = pReturnData[i].getElementsByTagName('DESCRICAO')[0].firstChild.data
			
			//
			
			var newrow = browser_produto.insertRow(1)
			newrow.name = 'browserrow_' + rgrow
			newrow.id = 'browserrow_' + rgrow
			newrow.valid = "yes"
			newrow.pRetValue = rgrow
			
			var newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"	
			newcell.pRetValue = rgrow
			newcell.appendChild (document.createTextNode(rgrow))
			
			var newcell = newrow.insertCell (newrow.cells.length)
			newcell.BrowserClick = "yes"
			newcell.className = "browser_row b2"
			newcell.pRetValue = rgdesc
			newcell.appendChild (document.createTextNode(rgdesc))
			
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
			fEnableElement (document.forms[0].confirmar)
			
		}
		
	}
	
}


function fTrataEnvio(hasError, pElement, pReturnData, pErrors){
	
	hasSuccess = false
	
	for (i=0; i<pErrors.length; i++) {
		if (pErrors[i].getElementsByTagName('TYPE')[0].firstChild.data == "0"){
			if (document.forms[0].contagem.value > 0 ) { 
				alert ('Montagem Realizada com Sucesso'); 
			}
			
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl				
		}
	}
	
	fEnableElement (document.forms[0].confirmar)
}

function fDoAction (pObjRef){
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	switch (pName){
		case "cancelar":	
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw001_001"
			document.location.href=pUrl	
		break;
		case "confirmar":
			fDisableElement (document.forms[0].confirmar)	
			// Monta a lista de itens
			pNVPs = '&ItemCodes='
			isFirst = true
			for (i=0; i<document.getElementById('browser_produto').rows.length; i++){
				if (document.getElementById('browser_produto').rows[i].pRetValue){
					if (isFirst){ isFirst = false }else{ pNVPs = pNVPs + ',' }
					pNVPs = pNVPs + document.getElementById('browser_produto').rows[i].pRetValue
				}
			}
			// Realiza a chamada
			// /scripts/cgiip.exe/WService=" + Broker + "/
			pUrl="fdcw006_001b"
			pCallback = fTrataEnvio
			fExecuteAjax (pUrl, pNVPs, pCallback, pElement)
		break;
	}	
}

function fClickTable (pObjRef) {
	if (!pObjRef){
		pElement = event.srcElement
		pAction = event.type
		pName = event.srcElement.name
	}else{
		pName = pObjRef
		pAction = event.type
		pElement = document.getElementById(pName)
	}
	
	if ( pName == "message" ) {
		fClearErrors()
	} else {
		
		/*
		if (pElement.innerHTML)
		
		var pergunta = confirm("Deseja retirar o RG" + pElement.innerHTML + " da selecao do RACK?")
		
		if (pergunta) {
			itemTR = document.getElementById ('browserrow_' + pElement.innerHTML)
			
			if (!(hasError)) {
				browser_produto.deleteRow(itemTR.rowIndex)
			}else{
				itemTR.cells[0].className = "browser_row b1"		
			}
			
			fEnableElement (document.forms[0].confirmar)
			document.forms[0].contagem.value = document.getElementById('browser_produto').rows.length - 1
		}
		*/
	}

}
