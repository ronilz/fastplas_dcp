&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i V99XX999 9.99.99.999}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES dc-pto-controle
&Scoped-define FIRST-EXTERNAL-TABLE dc-pto-controle


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR dc-pto-controle.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS dc-pto-controle.local-pto ~
dc-pto-controle.tipo-pto dc-pto-controle.desc-pto-controle ~
dc-pto-controle.cod-depos-orig dc-pto-controle.cod-depos-dest ~
dc-pto-controle.cod-localiz-orig dc-pto-controle.cod-localiz-dest ~
dc-pto-controle.cod-pto-controle-ativ dc-pto-controle.utiliz-loc-pad-item ~
dc-pto-controle.altera-localiz dc-pto-controle.utiliz-fifo 
&Scoped-define ENABLED-TABLES dc-pto-controle
&Scoped-define FIRST-ENABLED-TABLE dc-pto-controle
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold 
&Scoped-Define DISPLAYED-FIELDS dc-pto-controle.cod-pto-controle ~
dc-pto-controle.local-pto dc-pto-controle.tipo-pto ~
dc-pto-controle.desc-pto-controle dc-pto-controle.cod-depos-orig ~
dc-pto-controle.cod-depos-dest dc-pto-controle.cod-localiz-orig ~
dc-pto-controle.cod-localiz-dest dc-pto-controle.cod-pto-controle-ativ ~
dc-pto-controle.utiliz-loc-pad-item dc-pto-controle.altera-localiz ~
dc-pto-controle.utiliz-fifo 
&Scoped-define DISPLAYED-TABLES dc-pto-controle
&Scoped-define FIRST-DISPLAYED-TABLE dc-pto-controle
&Scoped-Define DISPLAYED-OBJECTS c-desc-local 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-CREATE-FIELDS dc-pto-controle.cod-pto-controle 
&Scoped-define ADM-ASSIGN-FIELDS dc-pto-controle.cod-pto-controle 
&Scoped-define ADM-MODIFY-FIELDS dc-pto-controle.cod-pto-controle 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE c-desc-local AS CHARACTER FORMAT "X(40)":U 
     VIEW-AS FILL-IN 
     SIZE 40 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 1.25.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88.57 BY 8.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     dc-pto-controle.cod-pto-controle AT ROW 1.17 COL 19 COLON-ALIGNED
          LABEL "Ponto Controle"
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     dc-pto-controle.local-pto AT ROW 2.75 COL 19 COLON-ALIGNED
          LABEL "Local Ponto"
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     c-desc-local AT ROW 2.75 COL 28.29 COLON-ALIGNED NO-LABEL
     dc-pto-controle.tipo-pto AT ROW 3.75 COL 19 COLON-ALIGNED
          VIEW-AS COMBO-BOX INNER-LINES 10
          LIST-ITEM-PAIRS "Normal",1,
                     "Jun��o",2,
                     "Pintura",3,
                     "Re-Trabalho",4,
                     "Expedi��o",5,
                     "Sucata",6,
                     "Impress�o",7,
                     "Pint. PA",8,
                     "Cancelamento NF",9
          DROP-DOWN-LIST
          SIZE 20 BY 1
     dc-pto-controle.desc-pto-controle AT ROW 4.75 COL 19 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 51.14 BY .88
     dc-pto-controle.cod-depos-orig AT ROW 5.75 COL 19 COLON-ALIGNED
          LABEL "C�d. Depos. Orig"
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     dc-pto-controle.cod-depos-dest AT ROW 5.75 COL 56 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     dc-pto-controle.cod-localiz-orig AT ROW 6.75 COL 19 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     dc-pto-controle.cod-localiz-dest AT ROW 6.75 COL 56 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     dc-pto-controle.cod-pto-controle-ativ AT ROW 7.75 COL 19 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     dc-pto-controle.utiliz-loc-pad-item AT ROW 7.75 COL 58 WIDGET-ID 2
          VIEW-AS TOGGLE-BOX
          SIZE 27 BY .83
     dc-pto-controle.altera-localiz AT ROW 8.5 COL 58 WIDGET-ID 4
          VIEW-AS TOGGLE-BOX
          SIZE 29 BY .83
     dc-pto-controle.utiliz-fifo AT ROW 9.25 COL 58 WIDGET-ID 8
          VIEW-AS TOGGLE-BOX
          SIZE 11.57 BY .83
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 2.25 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 1.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.dc-pto-controle
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 9.25
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN c-desc-local IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-pto-controle.cod-depos-orig IN FRAME f-main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN dc-pto-controle.cod-pto-controle IN FRAME f-main
   NO-ENABLE 1 2 3 EXP-LABEL                                            */
/* SETTINGS FOR FILL-IN dc-pto-controle.local-pto IN FRAME f-main
   EXP-LABEL                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dc-pto-controle.cod-depos-dest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-depos-dest V-table-Win
ON F5 OF dc-pto-controle.cod-depos-dest IN FRAME f-main /* C�d. Depos. Dest */
DO:

    {include/zoomvar.i &prog-zoom=inzoom/z01in084.w
                       &campo=dc-pto-controle.cod-depos-dest
                       &campozoom=cod-depos}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-depos-dest V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-pto-controle.cod-depos-dest IN FRAME f-main /* C�d. Depos. Dest */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-pto-controle.cod-depos-orig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-depos-orig V-table-Win
ON F5 OF dc-pto-controle.cod-depos-orig IN FRAME f-main /* C�d. Depos. Orig */
DO:
   {include/zoomvar.i &prog-zoom=inzoom/z01in084.w
                       &campo=dc-pto-controle.cod-depos-orig
                       &campozoom=cod-depos}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-depos-orig V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-pto-controle.cod-depos-orig IN FRAME f-main /* C�d. Depos. Orig */
DO:
    APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-pto-controle.cod-localiz-dest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-localiz-dest V-table-Win
ON F5 OF dc-pto-controle.cod-localiz-dest IN FRAME f-main /* C�d. Local. Dest */
DO:

    {include/zoomvar.i &prog-zoom=inzoom/z02in189.w
                       &campo=dc-pto-controle.cod-localiz-dest
                       &campozoom=cod-localiz}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-localiz-dest V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-pto-controle.cod-localiz-dest IN FRAME f-main /* C�d. Local. Dest */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-pto-controle.cod-localiz-orig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-localiz-orig V-table-Win
ON F5 OF dc-pto-controle.cod-localiz-orig IN FRAME f-main /* C�d. Local. Orig */
DO:

    {include/zoomvar.i &prog-zoom=inzoom/z02in189.w
                       &campo=dc-pto-controle.cod-localiz-orig
                       &campozoom=cod-localiz}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-localiz-orig V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-pto-controle.cod-localiz-orig IN FRAME f-main /* C�d. Local. Orig */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-pto-controle.cod-pto-controle-ativ
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-pto-controle-ativ V-table-Win
ON F5 OF dc-pto-controle.cod-pto-controle-ativ IN FRAME f-main /* Pto. Contr. Ativ. */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es117.w
                                &campo=dc-pto-controle.cod-pto-controle-ativ
                                &campozoom=cod-pto-controle}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.cod-pto-controle-ativ V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-pto-controle.cod-pto-controle-ativ IN FRAME f-main /* Pto. Contr. Ativ. */
DO:
  APPLY 'f5':U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-pto-controle.local-pto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.local-pto V-table-Win
ON F5 OF dc-pto-controle.local-pto IN FRAME f-main /* Local Ponto */
DO:

    {include/zoomvar.i &prog-zoom=eszoom/z01es126.w
                       &campo=dc-pto-controle.local-pto
                       &campozoom=local-pto
                       &campo2=c-desc-local
                       &campozoom2=desc-local}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.local-pto V-table-Win
ON LEAVE OF dc-pto-controle.local-pto IN FRAME f-main /* Local Ponto */
DO:

    FIND dc-local-pto NO-LOCK
        WHERE dc-local-pto.local-pto = INPUT FRAME {&FRAME-NAME} dc-pto-controle.local-pto NO-ERROR.

    ASSIGN c-desc-local:SCREEN-VALUE = IF AVAIL dc-local-pto THEN dc-local-pto.desc-local ELSE "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-pto-controle.local-pto V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-pto-controle.local-pto IN FRAME f-main /* Local Ponto */
DO:
  APPLY "F5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

dc-pto-controle.local-pto:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .
dc-pto-controle.cod-depos-orig:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .
dc-pto-controle.cod-depos-dest:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .
dc-pto-controle.cod-localiz-orig:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .
dc-pto-controle.cod-localiz-dest:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .
dc-pto-controle.cod-pto-controle-ativ:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "dc-pto-controle"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "dc-pto-controle"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}

    RUN pi-validate.

    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    /*:T Ponha na pi-validate todas as valida??es */
    /*:T N?o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    /*:T Todos os assign?s n?o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record V-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    FIND FIRST dc-pto-programa NO-LOCK
        WHERE dc-pto-programa.cod-pto-controle = dc-pto-controle.cod-pto-controle:SCREEN-VALUE IN FRAME {&FRAME-NAME}
        NO-ERROR.
    IF AVAIL dc-movto-pto-controle THEN
    DO:
        RUN utp/ut-msgs.p (INPUT 'show',
                           INPUT 3,
                           INPUT 'Movimento RG').
        RETURN 'NOK':U.
    END.

    MESSAGE "Se possuir movimenta��o pode ocasionar erros no sistema. Confirma?"
        VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO-CANCEL
        TITLE "" UPDATE choice AS LOGICAL.
    CASE choice:
         WHEN TRUE THEN /* Yes */
          DO:
             .
          END.
         WHEN FALSE THEN /* No */
          DO:
             RETURN.
          END.
         OTHERWISE /* Cancel */
             RETURN.
         END CASE.
    
  /*
  FIND FIRST dc-movto-pto-controle
      WHERE dc-movto-pto-controle.cod-pto-controle = dc-pto-controle.cod-pto-controle:SCREEN-VALUE IN FRAME {&FRAME-NAME}
      NO-LOCK NO-ERROR.
  IF AVAIL dc-movto-pto-controle THEN
  DO:
      RUN utp/ut-msgs.p (INPUT 'show',
                         INPUT 3,
                         INPUT 'Movimento RG').
      RETURN 'NOK':U.
  END.
  */
      

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

    APPLY "leave" TO dc-pto-controle.local-pto IN FRAME {&FRAME-NAME}.


    



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N?o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida??es, pois neste ponto do programa o registro 
  ainda n?o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida??o de dicion�rio */
    
/*:T    Segue um exemplo de valida??o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

        IF adm-new-record THEN
        DO:

             FIND dc-pto-controle NO-LOCK
                WHERE dc-pto-controle.cod-pto-controle = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-pto-controle NO-ERROR.
            IF AVAIL dc-pto-controle THEN
            DO:

                RUN utp/ut-msgs.p (INPUT "show":U, 
                                   INPUT 7, 
                                   INPUT "Rack").

                APPLY "choose" TO dc-pto-controle.cod-pto-controle.
                RETURN 'ADM-ERROR':U.

            END.

        END.


        FIND dc-local-pto NO-LOCK
            WHERE dc-local-pto.local-pto = INPUT FRAME {&FRAME-NAME} dc-pto-controle.local-pto NO-ERROR.
        IF NOT AVAIL dc-local-pto THEN
        DO:

            RUN utp/ut-msgs.p (INPUT "show":U, 
                               INPUT 2, 
                               INPUT "Local Ponto").
            APPLY "choose" TO dc-pto-controle.local-pto.
            RETURN 'ADM-ERROR':U.

        END.

        IF INPUT FRAME {&FRAME-NAME} dc-pto-controle.tipo-pto = 0 THEN
        DO:

            RUN utp/ut-msgs.p (INPUT "show":U, 
                               INPUT 17006, 
                               INPUT "Tipo Ponto~~N?o pode ser igual a branco!").

            APPLY "choose" TO dc-pto-controle.tipo-pto.
            RETURN 'ADM-ERROR':U.

        END.

        IF INPUT FRAME {&FRAME-NAME} dc-pto-controle.desc-pto-controle = " " THEN
        DO:

            RUN utp/ut-msgs.p (INPUT "show":U, 
                               INPUT 17006, 
                               INPUT "Descri??o~~N?o pode ser igual a branco!").

            APPLY "choose" TO dc-pto-controle.desc-pto-controle.
            RETURN 'ADM-ERROR':U.

        END.

        FIND deposito NO-LOCK
            WHERE deposito.cod-depos = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-depos-orig NO-ERROR.
        IF NOT AVAIL deposito THEN
        DO:

            RUN utp/ut-msgs.p (INPUT "show":U, 
                               INPUT 2, 
                               INPUT "Dep�sito Origem").
            APPLY "choose" TO dc-pto-controle.cod-depos-orig.
            RETURN 'ADM-ERROR':U.


        END.

/*         FIND deposito NO-LOCK                                                                             */
/*             WHERE deposito.cod-depos = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-depos-dest NO-ERROR. */
/*         IF NOT AVAIL deposito THEN                                                                        */
/*         DO:                                                                                               */
/*                                                                                                           */
/*             RUN utp/ut-msgs.p (INPUT "show":U,                                                            */
/*                                INPUT 2,                                                                   */
/*                                INPUT "Dep�sito Destino").                                                 */
/*             APPLY "choose" TO dc-pto-controle.cod-depos-dest.                                             */
/*             RETURN 'ADM-ERROR':U.                                                                         */
/*                                                                                                           */
/*                                                                                                           */
/*         END.                                                                                              */

        FIND FIRST dc-param NO-LOCK NO-ERROR.

        /**
        *  Verificar se o dep�sito ? controlado por dep�sito
        **/
        FIND FIRST mgcad.localiz
            WHERE localiz.cod-estabel = dc-param.cod-estabel
              AND localiz.cod-depos = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-depos-orig
            NO-LOCK NO-ERROR.
        IF AVAIL localiz THEN
        DO:
            FIND FIRST localiz NO-LOCK
                WHERE localiz.cod-localiz = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-localiz-orig
                  AND localiz.cod-estabel = dc-param.cod-estabel
                  AND localiz.cod-depos   = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-depos-orig NO-ERROR.
            IF NOT AVAIL localiz THEN
            DO:
                RUN utp/ut-msgs.p (INPUT "show":U, 
                                   INPUT 2, 
                                   INPUT "Localiza??o Origem").
                APPLY "choose" TO dc-pto-controle.cod-localiz-orig.
                RETURN 'ADM-ERROR':U.
            END.
        END.


        /**
        *  Verificar se o dep�sito ? controlado por dep�sito
        **/
/*         FIND FIRST localiz                                                                                     */
/*             WHERE localiz.cod-estabel = dc-param.cod-estabel                                                   */
/*               AND localiz.cod-depos = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-depos-dest                 */
/*             NO-LOCK NO-ERROR.                                                                                  */
/*         IF AVAIL localiz THEN                                                                                  */
/*         DO:                                                                                                    */
/*             FIND FIRST localiz NO-LOCK                                                                         */
/*                 WHERE localiz.cod-localiz = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-localiz-dest         */
/*                   AND localiz.cod-estabel = dc-param.cod-estabel                                               */
/*                   AND localiz.cod-depos   = INPUT FRAME {&FRAME-NAME} dc-pto-controle.cod-depos-dest NO-ERROR. */
/*             IF NOT AVAIL localiz THEN                                                                          */
/*             DO:                                                                                                */
/*                 RUN utp/ut-msgs.p (INPUT "show":U,                                                             */
/*                                    INPUT 2,                                                                    */
/*                                    INPUT "Localiza??o Destino").                                               */
/*                 APPLY "choose" TO dc-pto-controle.cod-localiz-dest.                                            */
/*                 RETURN 'ADM-ERROR':U.                                                                          */
/*             END.                                                                                               */
/*         END.                                                                                                   */

        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "dc-pto-controle"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

