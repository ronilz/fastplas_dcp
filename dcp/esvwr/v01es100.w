&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i V99XX999 9.99.99.999}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

DEF VAR d-rg-item AS INT.
DEF VAR l-ativo-item AS LOG.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES dc-juncao
&Scoped-define FIRST-EXTERNAL-TABLE dc-juncao


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR dc-juncao.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS dc-juncao.it-codigo-pai dc-juncao.ind-juncao ~
dc-juncao.ind-ativo 
&Scoped-define ENABLED-TABLES dc-juncao
&Scoped-define FIRST-ENABLED-TABLE dc-juncao
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold rt-mold-2 rt-mold-3 
&Scoped-Define DISPLAYED-FIELDS dc-juncao.it-codigo-1 dc-juncao.it-codigo-2 ~
dc-juncao.it-codigo-pai dc-juncao.ind-juncao dc-juncao.ind-ativo 
&Scoped-define DISPLAYED-TABLES dc-juncao
&Scoped-define FIRST-DISPLAYED-TABLE dc-juncao
&Scoped-Define DISPLAYED-OBJECTS desc-2 desc-3 desc-1 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-CREATE-FIELDS dc-juncao.it-codigo-1 ~
dc-juncao.it-codigo-2 
&Scoped-define ADM-ASSIGN-FIELDS dc-juncao.it-codigo-1 ~
dc-juncao.it-codigo-2 
&Scoped-define ADM-MODIFY-FIELDS dc-juncao.it-codigo-1 ~
dc-juncao.it-codigo-2 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "",
     Keys-Supplied = ""':U).
/**************************
</EXECUTING-CODE> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE desc-1 AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 50.43 BY .88 NO-UNDO.

DEFINE VARIABLE desc-2 AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 50.43 BY .88 NO-UNDO.

DEFINE VARIABLE desc-3 AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 50.43 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88.57 BY 2.75.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88.57 BY 1.79.

DEFINE RECTANGLE rt-mold-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88.57 BY 1.79.

DEFINE RECTANGLE rt-mold-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 88.57 BY 2.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     desc-2 AT ROW 1.38 COL 35.57 COLON-ALIGNED NO-LABEL
     dc-juncao.it-codigo-1 AT ROW 1.42 COL 18 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     desc-3 AT ROW 2.38 COL 35.57 COLON-ALIGNED NO-LABEL
     dc-juncao.it-codigo-2 AT ROW 2.42 COL 18 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     dc-juncao.it-codigo-pai AT ROW 4.79 COL 13.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     desc-1 AT ROW 4.79 COL 30.86 COLON-ALIGNED NO-LABEL
     dc-juncao.ind-juncao AT ROW 7.25 COL 7 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Primeiro Semi Acabado", 1,
"Segundo Semi Acabado", 2
          SIZE 59 BY 1
     dc-juncao.ind-ativo AT ROW 9.54 COL 7.29
          VIEW-AS TOGGLE-BOX
          SIZE 9 BY .83
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 6.71 COL 1
     rt-mold-2 AT ROW 8.96 COL 1
     rt-mold-3 AT ROW 4.25 COL 1
     "RG do Item" VIEW-AS TEXT
          SIZE 12 BY .67 AT ROW 6.42 COL 3.72
     "Item Jun��o Ativa" VIEW-AS TEXT
          SIZE 18 BY .67 AT ROW 8.63 COL 4
     "Item Pai" VIEW-AS TEXT
          SIZE 9.29 BY .67 AT ROW 3.92 COL 3.72
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.dc-juncao
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 9.79
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN desc-1 IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN desc-2 IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN desc-3 IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-juncao.it-codigo-1 IN FRAME f-main
   NO-ENABLE 1 2 3                                                      */
/* SETTINGS FOR FILL-IN dc-juncao.it-codigo-2 IN FRAME f-main
   NO-ENABLE 1 2 3                                                      */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dc-juncao.it-codigo-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-1 V-table-Win
ON F5 OF dc-juncao.it-codigo-1 IN FRAME f-main /* Semi Acabado 1 */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in172.w
                        &campo=dc-juncao.it-codigo-1
                        &campozoom=it-codigo
                        &campo2=desc-2
                        &campozoom2=desc-item}   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-1 V-table-Win
ON LEAVE OF dc-juncao.it-codigo-1 IN FRAME f-main /* Semi Acabado 1 */
DO:
    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-1 NO-LOCK NO-ERROR.
    IF AVAIL ITEM THEN
       ASSIGN desc-2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-1 V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-juncao.it-codigo-1 IN FRAME f-main /* Semi Acabado 1 */
DO:
  APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-juncao.it-codigo-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-2 V-table-Win
ON F5 OF dc-juncao.it-codigo-2 IN FRAME f-main /* Semi Acabado 2 */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in172.w
                        &campo=dc-juncao.it-codigo-2
                        &campozoom=it-codigo
                        &campo2=desc-3
                        &campozoom2=desc-item}   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-2 V-table-Win
ON LEAVE OF dc-juncao.it-codigo-2 IN FRAME f-main /* Semi Acabado 2 */
DO:
    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-2 NO-LOCK NO-ERROR.
    IF AVAIL ITEM THEN
       ASSIGN desc-3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-2 V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-juncao.it-codigo-2 IN FRAME f-main /* Semi Acabado 2 */
DO:
  APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-juncao.it-codigo-pai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-pai V-table-Win
ON F5 OF dc-juncao.it-codigo-pai IN FRAME f-main /* Item Pai */
DO:
  {include/zoomvar.i &prog-zoom=inzoom/z02in172.w
                        &campo=dc-juncao.it-codigo-pai
                        &campozoom=it-codigo
                        &campo2=desc-1
                        &campozoom2=desc-item}        
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-pai V-table-Win
ON LEAVE OF dc-juncao.it-codigo-pai IN FRAME f-main /* Item Pai */
DO:
    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-pai NO-LOCK NO-ERROR.
    IF AVAIL ITEM THEN
       ASSIGN desc-1:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-juncao.it-codigo-pai V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-juncao.it-codigo-pai IN FRAME f-main /* Item Pai */
DO:
  APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "dc-juncao"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "dc-juncao"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    
    RUN pi-validate.

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.

    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record V-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  
/*   FIND FIRST dc-reporte-item                                                                                      */
/*        WHERE dc-reporte-item.it-codigo-semi-1 = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-1                   */
/*          AND dc-reporte-item.it-codigo-semi-2 = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-2 NO-LOCK NO-ERROR. */
/*                                                                                                                   */
/*   IF AVAIL dc-reporte-item THEN DO:                                                                               */
/*         run utp/ut-msgs.p (input "show":U, input 3, "Item Jun��o").                                               */
/*         return 'ADM-ERROR':U.                                                                                     */
/*   END.                                                                                                            */
  
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-pai NO-LOCK NO-ERROR.
    IF AVAIL ITEM THEN
       ASSIGN desc-1:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.

    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-1 NO-LOCK NO-ERROR.
    IF AVAIL ITEM THEN
       ASSIGN desc-2:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.

    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-2 NO-LOCK NO-ERROR.
    IF AVAIL ITEM THEN
       ASSIGN desc-3:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .
  
  dc-juncao.it-codigo-pai:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.
  dc-juncao.it-codigo-1:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.
  dc-juncao.it-codigo-2:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.
  /* Code placed here will execute AFTER standard behavior.    */

  RUN local-display-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    {include/i-vldprg.i}
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

    IF adm-new-record THEN
    DO:
        FIND dc-juncao NO-LOCK
            WHERE dc-juncao.it-codigo-1 = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-1
              AND dc-juncao.it-codigo-2 = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-2 NO-ERROR.
        IF AVAIL dc-juncao THEN
        DO:
            RUN utp/ut-msgs.p (input "show":U, input 7, "Item jun��o").
            APPLY "choose" TO dc-juncao.it-codigo-pai.
            RETURN 'ADM-ERROR':U. 
        END.
    END.

    IF INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-pai = "" THEN DO:
        run utp/ut-msgs.p (input "show":U, input 17006, "Campo em branco~~Campos 'Item Pai' n�o pode ser igual a branco.").
        return 'ADM-ERROR':U.
    END.
    ELSE DO:
        IF INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-1 = "" THEN DO:
            run utp/ut-msgs.p (input "show":U, input 17006, "Campo em branco~~Campos 'Semi Acabado 1' n�o pode ser igual a branco.").
            return 'ADM-ERROR':U.
        END.
        ELSE DO:
            IF INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-2 = "" THEN DO:
                run utp/ut-msgs.p (input "show":U, input 17006, "Campo em branco~~Campos 'Semi Acabado 2' n�o pode ser igual a branco.").
                return 'ADM-ERROR':U.
            END.
        END.
    END.


    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-pai NO-LOCK NO-ERROR.
    IF NOT AVAIL ITEM THEN DO:
        run utp/ut-msgs.p (input "show":U, input 17006, "N�o encontrado item~~N�o encontrado item para o campo 'Item Pai'.").
        return 'ADM-ERROR':U.
    END.

    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-1 NO-LOCK NO-ERROR.
    IF NOT AVAIL ITEM THEN DO:
        run utp/ut-msgs.p (input "show":U, input 17006, "N�o encontrado item~~N�o encontrado item para o campo 'Semi Acabado 1'.").
        return 'ADM-ERROR':U.
    END.

    FIND ITEM WHERE item.it-codigo = INPUT FRAME {&FRAME-NAME} dc-juncao.it-codigo-2 NO-LOCK NO-ERROR.
    IF NOT AVAIL ITEM THEN DO:
        run utp/ut-msgs.p (input "show":U, input 17006, "N�o encontrado item~~N�o encontrado item para o campo 'Semi Acabado 2'.").
        return 'ADM-ERROR':U.
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "dc-juncao"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

