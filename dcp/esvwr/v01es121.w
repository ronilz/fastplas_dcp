&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i V99XX999 9.99.99.999}
define buffer empresa     for mgcad.empresa.
define buffer localizacao for mgcad.localizacao.
DEFINE BUFFER estabelec              FOR mgadm.estabelec.
DEFINE BUFFER cidade                 FOR mgdis.cidade.   
DEFINE BUFFER pais                   FOR mguni.pais. 
define buffer unid-feder             for mgcad.unid-feder. 

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES dc-movto-pto-controle
&Scoped-define FIRST-EXTERNAL-TABLE dc-movto-pto-controle


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR dc-movto-pto-controle.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS dc-movto-pto-controle.cod-cor ~
dc-movto-pto-controle.cod-depos-dest dc-movto-pto-controle.cod-depos-orig ~
dc-movto-pto-controle.cod-localiz-dest ~
dc-movto-pto-controle.cod-localiz-orig ~
dc-movto-pto-controle.cod-pto-controle ~
dc-movto-pto-controle.cod-pto-controle-ant dc-movto-pto-controle.it-codigo ~
dc-movto-pto-controle.it-codigo-ant dc-movto-pto-controle.local-pto ~
dc-movto-pto-controle.nr-rack dc-movto-pto-controle.nr-seq-ant ~
dc-movto-pto-controle.nro-docto dc-movto-pto-controle.rg-item ~
dc-movto-pto-controle.tipo-pto 
&Scoped-define ENABLED-TABLES dc-movto-pto-controle
&Scoped-define FIRST-ENABLED-TABLE dc-movto-pto-controle
&Scoped-Define ENABLED-OBJECTS rt-key rt-mold 
&Scoped-Define DISPLAYED-FIELDS dc-movto-pto-controle.nr-seq ~
dc-movto-pto-controle.cod-cor dc-movto-pto-controle.cod-depos-dest ~
dc-movto-pto-controle.cod-depos-orig dc-movto-pto-controle.cod-localiz-dest ~
dc-movto-pto-controle.cod-localiz-orig ~
dc-movto-pto-controle.cod-pto-controle ~
dc-movto-pto-controle.cod-pto-controle-ant dc-movto-pto-controle.it-codigo ~
dc-movto-pto-controle.it-codigo-ant dc-movto-pto-controle.local-pto ~
dc-movto-pto-controle.nr-rack dc-movto-pto-controle.nr-seq-ant ~
dc-movto-pto-controle.nro-docto dc-movto-pto-controle.rg-item ~
dc-movto-pto-controle.tipo-pto 
&Scoped-define DISPLAYED-TABLES dc-movto-pto-controle
&Scoped-define FIRST-DISPLAYED-TABLE dc-movto-pto-controle
&Scoped-Define DISPLAYED-OBJECTS c-desc-cor c-nome-dest c-nome-orig ~
c-descricao-dest c-descricao-orig c-desc-pto-controle ~
c-desc-pto-controle-ant c-desc-item c-desc-item-ant c-desc-local ~
c-desc-rack 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-ASSIGN-FIELDS dc-movto-pto-controle.nr-seq 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
nr-seq|y|y|mgfas.dc-movto-pto-controle.nr-seq
it-codigo||y|mgfas.dc-movto-pto-controle.it-codigo
cod-cor||y|mgfas.dc-movto-pto-controle.cod-cor
nro-docto||y|mgfas.dc-movto-pto-controle.nro-docto
rg-item||y|mgfas.dc-movto-pto-controle.rg-item
local-pto||y|mgfas.dc-movto-pto-controle.local-pto
cod-pto-controle||y|mgfas.dc-movto-pto-controle.cod-pto-controle
nr-rack||y|mgfas.dc-movto-pto-controle.nr-rack
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "nr-seq",
     Keys-Supplied = "nr-seq,it-codigo,cod-cor,nro-docto,rg-item,local-pto,cod-pto-controle,nr-rack"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE c-desc-cor AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 50 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-item AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 60 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-item-ant AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 60 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-local AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 50 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-pto-controle AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 50 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-pto-controle-ant AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 50 BY .88 NO-UNDO.

DEFINE VARIABLE c-desc-rack AS CHARACTER FORMAT "X(2000)":U 
     VIEW-AS FILL-IN 
     SIZE 65.72 BY .88 NO-UNDO.

DEFINE VARIABLE c-descricao-dest AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY .88 NO-UNDO.

DEFINE VARIABLE c-descricao-orig AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 30 BY .88 NO-UNDO.

DEFINE VARIABLE c-nome-dest AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 40 BY .88 NO-UNDO.

DEFINE VARIABLE c-nome-orig AS CHARACTER FORMAT "X(50)":U 
     VIEW-AS FILL-IN 
     SIZE 40 BY .88 NO-UNDO.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 100 BY 1.25.

DEFINE RECTANGLE rt-mold
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL 
     SIZE 100 BY 16.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     dc-movto-pto-controle.nr-seq AT ROW 1.13 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8 BY .88
     dc-movto-pto-controle.cod-cor AT ROW 2.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 16.14 BY .88
     c-desc-cor AT ROW 2.75 COL 36.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.cod-depos-dest AT ROW 3.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     c-nome-dest AT ROW 3.75 COL 27.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.cod-depos-orig AT ROW 4.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     c-nome-orig AT ROW 4.75 COL 27.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.cod-localiz-dest AT ROW 5.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     c-descricao-dest AT ROW 5.75 COL 34.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.cod-localiz-orig AT ROW 6.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     c-descricao-orig AT ROW 6.75 COL 34.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.cod-pto-controle AT ROW 7.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     c-desc-pto-controle AT ROW 7.75 COL 29.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.cod-pto-controle-ant AT ROW 8.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     c-desc-pto-controle-ant AT ROW 8.75 COL 29.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.it-codigo AT ROW 9.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     c-desc-item AT ROW 9.75 COL 37.43 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.it-codigo-ant AT ROW 10.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     c-desc-item-ant AT ROW 10.75 COL 37.43 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.local-pto AT ROW 11.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     c-desc-local AT ROW 11.75 COL 29.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.nr-rack AT ROW 12.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 11.14 BY .88
     c-desc-rack AT ROW 12.75 COL 31.29 COLON-ALIGNED NO-LABEL
     dc-movto-pto-controle.nr-seq-ant AT ROW 13.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 8 BY .88
     dc-movto-pto-controle.nro-docto AT ROW 14.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     dc-movto-pto-controle.rg-item AT ROW 15.75 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 16.14 BY .88
     dc-movto-pto-controle.tipo-pto AT ROW 17 COL 22 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Normal", 1,
"Jun��o", 2
          SIZE 12 BY 1.75
     rt-key AT ROW 1 COL 1
     rt-mold AT ROW 2.5 COL 1
     "Tipo Ponto:" VIEW-AS TEXT
          SIZE 8 BY .67 AT ROW 17 COL 13.43
          FONT 4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.dc-movto-pto-controle
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 18
         WIDTH              = 100.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE Size-to-Fit                                              */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN c-desc-cor IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-item IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-item-ant IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-local IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-pto-controle IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-pto-controle-ant IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-desc-rack IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-descricao-dest IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-descricao-orig IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-nome-dest IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN c-nome-orig IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-movto-pto-controle.nr-seq IN FRAME f-main
   NO-ENABLE 2                                                          */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dc-movto-pto-controle.cod-cor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-cor V-table-Win
ON F5 OF dc-movto-pto-controle.cod-cor IN FRAME f-main /* C�d. Cor */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es104.w
                       &campo=dc-movto-pto-controle.cod-cor
                       &campozoom=cod-cor}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-cor V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-cor IN FRAME f-main /* C�d. Cor */
DO:
  Find dc-cor Where
       dc-cor.cod-cor = dc-movto-pto-controle.cod-cor:screen-value In Frame {&frame-name}
       No-lock No-error.
  Assign c-desc-cor:screen-value In Frame {&frame-name} = If Avail dc-cor Then dc-cor.desc-cor Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-cor V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-cor IN FRAME f-main /* C�d. Cor */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.cod-depos-dest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-depos-dest V-table-Win
ON F5 OF dc-movto-pto-controle.cod-depos-dest IN FRAME f-main /* C�d. Depos. Dest */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es125.w
                       &campo=dc-movto-pto-controle.cod-depos-dest
                       &campozoom=cod-depos}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-depos-dest V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-depos-dest IN FRAME f-main /* C�d. Depos. Dest */
DO:
    Find deposito Where
         deposito.cod-depos = dc-movto-pto-controle.cod-depos-dest:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-nome-dest:screen-value In Frame {&frame-name} = If Avail deposito Then deposito.nome Else "".

  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-depos-dest V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-depos-dest IN FRAME f-main /* C�d. Depos. Dest */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.cod-depos-orig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-depos-orig V-table-Win
ON F5 OF dc-movto-pto-controle.cod-depos-orig IN FRAME f-main /* C�d. Depos. Orig */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es125.w
                       &campo=dc-movto-pto-controle.cod-depos-orig
                       &campozoom=cod-depos}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-depos-orig V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-depos-orig IN FRAME f-main /* C�d. Depos. Orig */
DO:
    Find deposito Where
         deposito.cod-depos = dc-movto-pto-controle.cod-depos-orig:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-nome-orig:screen-value In Frame {&frame-name} = If Avail deposito Then deposito.nome Else "".
End.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-depos-orig V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-depos-orig IN FRAME f-main /* C�d. Depos. Orig */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.cod-localiz-dest
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-localiz-dest V-table-Win
ON F5 OF dc-movto-pto-controle.cod-localiz-dest IN FRAME f-main /* C�d. Local. Dest */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in189.w
                       &campo=dc-movto-pto-controle.cod-localiz-dest
                       &campozoom=cod-localiz}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-localiz-dest V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-localiz-dest IN FRAME f-main /* C�d. Local. Dest */
DO:
    Find localizacao Where
         localizacao.cod-localiz     = dc-movto-pto-controle.cod-localiz-dest:screen-value In Frame {&frame-name} And
         localizacao.cod-depos       = dc-movto-pto-controle.cod-depos-dest:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-descricao-dest:screen-value In Frame {&frame-name} = If Avail localizacao Then localizacao.descricao Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-localiz-dest V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-localiz-dest IN FRAME f-main /* C�d. Local. Dest */
DO:
  APPLY "f5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.cod-localiz-orig
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-localiz-orig V-table-Win
ON F5 OF dc-movto-pto-controle.cod-localiz-orig IN FRAME f-main /* C�d. Local. Orig */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in189.w
                       &campo=dc-movto-pto-controle.cod-localiz-orig
                       &campozoom=cod-localiz}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-localiz-orig V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-localiz-orig IN FRAME f-main /* C�d. Local. Orig */
DO:
    Find localizacao Where
         localizacao.cod-localiz     = dc-movto-pto-controle.cod-localiz-orig:screen-value In Frame {&frame-name} And
         localizacao.cod-depos       = dc-movto-pto-controle.cod-depos-orig:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-descricao-orig:screen-value In Frame {&frame-name} = If Avail localizacao Then localizacao.descricao Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-localiz-orig V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-localiz-orig IN FRAME f-main /* C�d. Local. Orig */
DO:
  APPLY "f5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.cod-pto-controle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-pto-controle V-table-Win
ON F5 OF dc-movto-pto-controle.cod-pto-controle IN FRAME f-main /* C�d. Pto. Contr */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es117.w
                       &campo=dc-movto-pto-controle.cod-pto-controle
                       &campozoom=cod-pto-controle}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-pto-controle V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-pto-controle IN FRAME f-main /* C�d. Pto. Contr */
DO:
    Find dc-pto-controle Where
         dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-desc-pto-controle:screen-value In Frame {&frame-name} = If Avail dc-pto-controle Then dc-pto-controle.desc-pto-controle Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-pto-controle V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-pto-controle IN FRAME f-main /* C�d. Pto. Contr */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.cod-pto-controle-ant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-pto-controle-ant V-table-Win
ON F5 OF dc-movto-pto-controle.cod-pto-controle-ant IN FRAME f-main /* C�d. Pto. Contr. Ant */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es117.w
                       &campo=dc-movto-pto-controle.cod-pto-controle-ant
                       &campozoom=cod-pto-controle}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-pto-controle-ant V-table-Win
ON LEAVE OF dc-movto-pto-controle.cod-pto-controle-ant IN FRAME f-main /* C�d. Pto. Contr. Ant */
DO:
    Find dc-pto-controle Where
         dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle-ant:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-desc-pto-controle-ant:screen-value In Frame {&frame-name} = If Avail dc-pto-controle Then dc-pto-controle.desc-pto-controle Else "".
End.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.cod-pto-controle-ant V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.cod-pto-controle-ant IN FRAME f-main /* C�d. Pto. Contr. Ant */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.it-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.it-codigo V-table-Win
ON F5 OF dc-movto-pto-controle.it-codigo IN FRAME f-main /* Item */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in172.w
                       &campo=dc-movto-pto-controle.it-codigo
                       &campozoom=it-codigo}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.it-codigo V-table-Win
ON LEAVE OF dc-movto-pto-controle.it-codigo IN FRAME f-main /* Item */
DO:
  Find Item Where
       Item.it-codigo = dc-movto-pto-controle.it-codigo:screen-value In Frame {&frame-name}
       No-lock No-error.
  Assign c-desc-item:screen-value In Frame {&frame-name} = If Avail Item Then Item.desc-item Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.it-codigo V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.it-codigo IN FRAME f-main /* Item */
DO:
  APPLY "f5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.it-codigo-ant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.it-codigo-ant V-table-Win
ON F5 OF dc-movto-pto-controle.it-codigo-ant IN FRAME f-main /* Item Anterior */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in172.w
                       &campo=dc-movto-pto-controle.it-codigo-ant
                       &campozoom=it-codigo}
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.it-codigo-ant V-table-Win
ON LEAVE OF dc-movto-pto-controle.it-codigo-ant IN FRAME f-main /* Item Anterior */
DO:
    Find Item Where
         Item.it-codigo = dc-movto-pto-controle.it-codigo-ant:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-desc-item-ant:screen-value In Frame {&frame-name} = If Avail Item Then Item.desc-item Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.it-codigo-ant V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.it-codigo-ant IN FRAME f-main /* Item Anterior */
DO:
  APPLY "f5" TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.local-pto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.local-pto V-table-Win
ON F5 OF dc-movto-pto-controle.local-pto IN FRAME f-main /* Local Pto. */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es126.w
                       &campo=dc-movto-pto-controle.local-pto
                       &campozoom=local-pto}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.local-pto V-table-Win
ON LEAVE OF dc-movto-pto-controle.local-pto IN FRAME f-main /* Local Pto. */
DO:
  Find dc-local-pto Where
       dc-local-pto.local-pto = dc-movto-pto-controle.local-pto:screen-value In Frame {&frame-name}
       No-lock No-error.
  Assign c-desc-local:screen-value In Frame {&frame-name} = If Avail dc-local-pto Then dc-local-pto.desc-local Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.local-pto V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.local-pto IN FRAME f-main /* Local Pto. */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.nr-rack
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.nr-rack V-table-Win
ON F5 OF dc-movto-pto-controle.nr-rack IN FRAME f-main /* Nr. Rack */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es101.w
                       &campo=dc-movto-pto-controle.nr-rack
                       &campozoom=nr-rack}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.nr-rack V-table-Win
ON LEAVE OF dc-movto-pto-controle.nr-rack IN FRAME f-main /* Nr. Rack */
DO:
    FIND FIRST dc-rack Where
         dc-rack.nr-rack = dc-movto-pto-controle.nr-rack:screen-value In Frame {&frame-name}
         No-lock No-error.
    Assign c-desc-rack:screen-value In Frame {&frame-name} = If Avail dc-rack Then dc-rack.desc-rack Else "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.nr-rack V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.nr-rack IN FRAME f-main /* Nr. Rack */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-movto-pto-controle.rg-item
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.rg-item V-table-Win
ON F5 OF dc-movto-pto-controle.rg-item IN FRAME f-main /* Rg. Item */
DO:
    {include/zoomvar.i &prog-zoom=eszoom/z01es116.w
                       &campo=dc-movto-pto-controle.rg-item
                       &campozoom=rg-item}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-movto-pto-controle.rg-item V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-movto-pto-controle.rg-item IN FRAME f-main /* Rg. Item */
DO:
  Apply "f5" To Self.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/
dc-movto-pto-controle.cod-cor:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .              
dc-movto-pto-controle.cod-depos-dest:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .       
dc-movto-pto-controle.cod-depos-orig:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .       
dc-movto-pto-controle.cod-localiz-dest:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .     
dc-movto-pto-controle.cod-localiz-orig:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .     
dc-movto-pto-controle.cod-pto-controle:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .     
dc-movto-pto-controle.cod-pto-controle-ant:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} . 
dc-movto-pto-controle.it-codigo:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .            
dc-movto-pto-controle.it-codigo-ant:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .        
dc-movto-pto-controle.local-pto:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .            
dc-movto-pto-controle.nr-rack:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .              
dc-movto-pto-controle.rg-item:load-mouse-pointer ("image/lupa.cur") in frame {&FRAME-NAME} .

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'nr-seq':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = dc-movto-pto-controle
           &WHERE = "WHERE dc-movto-pto-controle.nr-seq eq INTEGER(key-value)"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "dc-movto-pto-controle"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "dc-movto-pto-controle"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  Find Last dc-movto-pto-controle No-lock No-error.

  Assign dc-movto-pto-controle.nr-seq:screen-value In Frame {&frame-name} = If Avail dc-movto-pto-controle Then string(dc-movto-pto-controle.nr-seq + 1) Else "1"
         dc-movto-pto-controle.nro-docto:screen-value In Frame {&frame-name} = If Avail dc-movto-pto-controle Then string(dc-movto-pto-controle.nr-seq + 1) Else "1".


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */
    Run pi-validate.
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

    Assign dc-movto-pto-controle.origem = 2.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-copy-record V-table-Win 
PROCEDURE local-copy-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'copy-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Find Last dc-movto-pto-controle No-lock No-error.

  Assign dc-movto-pto-controle.nr-seq:screen-value In Frame {&frame-name} = If Avail dc-movto-pto-controle Then string(dc-movto-pto-controle.nr-seq + 1) Else "1"
         dc-movto-pto-controle.nro-docto:screen-value In Frame {&frame-name} = If Avail dc-movto-pto-controle Then string(dc-movto-pto-controle.nr-seq + 1) Else "1".


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record V-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  if adm-new-record = No Then Do:

      Find dc-movto-pto-controle Where
           dc-movto-pto-controle.nr-seq = int(dc-movto-pto-controle.nr-seq:screen-value In Frame {&frame-name})
           No-lock No-error.
      If Avail dc-movto-pto-controle And dc-movto-pto-controle.origem = 1 Then
      Do:
          run utp/ut-msgs.p (input "show":U, input 17006, input "Hist�rico Gerado Autom�tico. Elimina��o N�o Permitida"). 
          return 'ADM-ERROR':U.                                            
      End.
  End.
  

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  Do With Frame {&frame-name}:
    Apply "leave" To dc-movto-pto-controle.cod-cor .
    Apply "leave" To dc-movto-pto-controle.cod-depos-dest.
    Apply "leave" To dc-movto-pto-controle.cod-depos-orig.
    Apply "leave" To dc-movto-pto-controle.cod-localiz-dest.
    Apply "leave" To dc-movto-pto-controle.cod-localiz-orig.
    Apply "leave" To dc-movto-pto-controle.cod-pto-controle.
    Apply "leave" To dc-movto-pto-controle.cod-pto-controle-ant.
    Apply "leave" To dc-movto-pto-controle.it-codigo.
    Apply "leave" To dc-movto-pto-controle.it-codigo-ant.
    Apply "leave" To dc-movto-pto-controle.local-pto.
    Apply "leave" To dc-movto-pto-controle.nr-rack.
  End.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  ASSIGN dc-movto-pto-controle.nr-seq:FORMAT IN FRAME {&FRAME-NAME} = ">>>>>>>>>9"
         dc-movto-pto-controle.nr-seq-ant:FORMAT IN FRAME {&FRAME-NAME} = ">>>>>>>>>9".

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */


  if adm-new-record = No Then Do:

      Find dc-movto-pto-controle Where
           dc-movto-pto-controle.nr-seq = int(dc-movto-pto-controle.nr-seq:screen-value In Frame {&frame-name})
           No-lock No-error.
      If Avail dc-movto-pto-controle And dc-movto-pto-controle.origem = 1 Then
      Do:
          run utp/ut-msgs.p (input "show":U, input 17006, input "Hist�rico Gerado Autom�tico. Altera��o N�o Permitida"). 
          return 'ADM-ERROR':U.                                            
      End.
  End.

  Find dc-cor Where
       dc-cor.cod-cor = dc-movto-pto-controle.cod-cor:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail dc-cor Then
  Do:
       run utp/ut-msgs.p (input "show":U, input 17006, input "Cor N�o Cadastrada."). 
       return 'ADM-ERROR':U.                                            
  End.

  Find deposito Where
       deposito.cod-depos = dc-movto-pto-controle.cod-depos-dest:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail deposito Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Dep�sito Destino N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find deposito Where
       deposito.cod-depos = dc-movto-pto-controle.cod-depos-orig:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail deposito Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Dep�sito Origem N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find localizacao Where
       localizacao.cod-localiz     = dc-movto-pto-controle.cod-localiz-dest:screen-value In Frame {&frame-name} And
       localizacao.cod-depos       = dc-movto-pto-controle.cod-depos-dest:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail localizacao Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Localiza��o Destino N�o Cadastrada."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find localizacao Where
       localizacao.cod-localiz     = dc-movto-pto-controle.cod-localiz-orig:screen-value In Frame {&frame-name} And
       localizacao.cod-depos       = dc-movto-pto-controle.cod-depos-orig:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail localizacao Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Localiza��o Origem N�o Cadastrada."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find dc-pto-controle Where
       dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail dc-pto-controle Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Ponto Controle N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.
                              
  Find dc-pto-controle Where
       dc-pto-controle.cod-pto-controle = dc-movto-pto-controle.cod-pto-controle-ant:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail dc-pto-controle Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Ponto Controle Anterior N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find Item Where
       Item.it-codigo = dc-movto-pto-controle.it-codigo:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail Item Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Item N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find Item Where
       Item.it-codigo = dc-movto-pto-controle.it-codigo-ant:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail Item Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Item Anterior N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find dc-local-pto Where
       dc-local-pto.local-pto = dc-movto-pto-controle.local-pto:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail dc-local-pto Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Local Ponto N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find dc-rack Where
       dc-rack.nr-rack = dc-movto-pto-controle.nr-rack:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail dc-rack Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Rack N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

  Find dc-rg-item Where
       dc-rg-item.rg-item = dc-movto-pto-controle.rg-item:screen-value In Frame {&frame-name}
       No-lock No-error.
  If Not Avail dc-rg-item Then
  Do:
      run utp/ut-msgs.p (input "show":U, input 17006, input "Rg Item N�o Cadastrado."). 
      return 'ADM-ERROR':U.                                            
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "nr-seq" "dc-movto-pto-controle" "nr-seq"}
  {src/adm/template/sndkycas.i "it-codigo" "dc-movto-pto-controle" "it-codigo"}
  {src/adm/template/sndkycas.i "cod-cor" "dc-movto-pto-controle" "cod-cor"}
  {src/adm/template/sndkycas.i "nro-docto" "dc-movto-pto-controle" "nro-docto"}
  {src/adm/template/sndkycas.i "rg-item" "dc-movto-pto-controle" "rg-item"}
  {src/adm/template/sndkycas.i "local-pto" "dc-movto-pto-controle" "local-pto"}
  {src/adm/template/sndkycas.i "cod-pto-controle" "dc-movto-pto-controle" "cod-pto-controle"}
  {src/adm/template/sndkycas.i "nr-rack" "dc-movto-pto-controle" "nr-rack"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "dc-movto-pto-controle"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

