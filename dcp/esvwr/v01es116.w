&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          mgfas            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*:T *******************************************************************************
** Copyright DATASUL S.A. (1997)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da DATASUL, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i V99XX999 9.99.99.999}

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/viewerd.w

/* global variable definitions */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def var v-row-parent as rowid no-undo.

DEF VAR i-seq AS INT.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME f-main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES dc-rg-item
&Scoped-define FIRST-EXTERNAL-TABLE dc-rg-item


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR dc-rg-item.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS dc-rg-item.nr-ord-prod dc-rg-item.situacao 
&Scoped-define ENABLED-TABLES dc-rg-item
&Scoped-define FIRST-ENABLED-TABLE dc-rg-item
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 RECT-3 rt-key 
&Scoped-Define DISPLAYED-FIELDS dc-rg-item.rg-item dc-rg-item.it-codigo ~
dc-rg-item.cod-pto-controle dc-rg-item.local-pto dc-rg-item.tipo-pto ~
dc-rg-item.cod-estabel dc-rg-item.serie dc-rg-item.nr-nota-fis ~
dc-rg-item.nr-prod-produ dc-rg-item.nr-ord-prod dc-rg-item.it-codigo-juncao ~
dc-rg-item.cod-depos-orig dc-rg-item.nr-rack dc-rg-item.cod-localiz-orig ~
dc-rg-item.cod-cor dc-rg-item.situacao 
&Scoped-define DISPLAYED-TABLES dc-rg-item
&Scoped-define FIRST-DISPLAYED-TABLE dc-rg-item
&Scoped-Define DISPLAYED-OBJECTS desc-item desc-contr 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,ADM-MODIFY-FIELDS,List-4,List-5,List-6 */
&Scoped-define ADM-CREATE-FIELDS dc-rg-item.rg-item 
&Scoped-define ADM-ASSIGN-FIELDS dc-rg-item.rg-item 
&Scoped-define ADM-MODIFY-FIELDS dc-rg-item.rg-item 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" V-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
THIS-PROCEDURE
</KEY-OBJECT>
<FOREIGN-KEYS>
rg-item|y|y|mgfas.dc-rg-item.rg-item
it-codigo||y|mgfas.dc-rg-item.it-codigo
cod-cor||y|mgfas.dc-rg-item.cod-cor
local-pto||y|mgfas.dc-rg-item.local-pto
cod-pto-controle||y|mgfas.dc-rg-item.cod-pto-controle
nr-rack||y|mgfas.dc-rg-item.nr-rack
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = "rg-item",
     Keys-Supplied = "rg-item,it-codigo,cod-cor,local-pto,cod-pto-controle,nr-rack"':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE VARIABLE desc-contr AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 58.57 BY .88 NO-UNDO.

DEFINE VARIABLE desc-item AS CHARACTER FORMAT "X(60)":U 
     VIEW-AS FILL-IN 
     SIZE 50.57 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 86 BY 1.75.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 17.57 BY 5.83.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 87.86 BY 12.17.

DEFINE RECTANGLE rt-key
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 88 BY 1.5.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME f-main
     dc-rg-item.rg-item AT ROW 1.29 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 16.14 BY .88
     dc-rg-item.it-codigo AT ROW 3.08 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     desc-item AT ROW 3.08 COL 34.14 COLON-ALIGNED NO-LABEL
     dc-rg-item.cod-pto-controle AT ROW 4.08 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     desc-contr AT ROW 4.08 COL 26.14 COLON-ALIGNED NO-LABEL
     dc-rg-item.local-pto AT ROW 5.08 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     dc-rg-item.tipo-pto AT ROW 5.58 COL 72.43 NO-LABEL
          VIEW-AS RADIO-SET VERTICAL
          RADIO-BUTTONS 
                    "Normal", 1,
"Jun��o", 2,
"Pintura", 3,
"Re-Trabalho", 4,
"Expedi��o", 5,
"Sucata", 6,
"Impress�o", 7
          SIZE 15 BY 5.25
     dc-rg-item.cod-estabel AT ROW 6.08 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     dc-rg-item.serie AT ROW 6.08 COL 29.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.14 BY .88
     dc-rg-item.nr-nota-fis AT ROW 6.08 COL 50.72 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     dc-rg-item.nr-prod-produ AT ROW 7.08 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 13.72 BY .88
     dc-rg-item.nr-ord-prod AT ROW 7.08 COL 51 COLON-ALIGNED WIDGET-ID 2
          VIEW-AS FILL-IN 
          SIZE 13.72 BY .88
     dc-rg-item.it-codigo-juncao AT ROW 8.08 COL 16 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17.14 BY .88
     dc-rg-item.cod-depos-orig AT ROW 10 COL 17 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 7.14 BY .88
     dc-rg-item.nr-rack AT ROW 10 COL 48.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     dc-rg-item.cod-localiz-orig AT ROW 11 COL 17 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 14.14 BY .88
     dc-rg-item.cod-cor AT ROW 11 COL 48.29 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 16.14 BY .88
     dc-rg-item.situacao AT ROW 13 COL 3.14 NO-LABEL
          VIEW-AS RADIO-SET HORIZONTAL
          RADIO-BUTTONS 
                    "Gerado":U, 1,
"Ativo":U, 2,
"Suspenso":U, 3,
"Cancelado":U, 4,
"Juntado":U, 5,
"Faturado":U, 6
          SIZE 70 BY 1
     "Tipo Pto Controle" VIEW-AS TEXT
          SIZE 18 BY .67 AT ROW 5 COL 52
     "Situa��o" VIEW-AS TEXT
          SIZE 10 BY .67 AT ROW 12.25 COL 3.72
     RECT-1 AT ROW 12.67 COL 2
     RECT-2 AT ROW 5.25 COL 70.86
     RECT-3 AT ROW 2.58 COL 1.14
     rt-key AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE .


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: mgfas.dc-rg-item
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 14.92
         WIDTH              = 88.57.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{src/adm/method/viewer.i}
{include/c-viewer.i}
{utp/ut-glob.i}
{include/i_dbtype.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME f-main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME f-main:SCROLLABLE       = FALSE
       FRAME f-main:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN dc-rg-item.cod-cor IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.cod-depos-orig IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.cod-estabel IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.cod-localiz-orig IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.cod-pto-controle IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN desc-contr IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN desc-item IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.it-codigo IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.it-codigo-juncao IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.local-pto IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.nr-nota-fis IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.nr-prod-produ IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.nr-rack IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN dc-rg-item.rg-item IN FRAME f-main
   NO-ENABLE 1 2 3                                                      */
/* SETTINGS FOR FILL-IN dc-rg-item.serie IN FRAME f-main
   NO-ENABLE                                                            */
/* SETTINGS FOR RADIO-SET dc-rg-item.tipo-pto IN FRAME f-main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME f-main
/* Query rebuild information for FRAME f-main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME f-main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME dc-rg-item.cod-pto-controle
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-rg-item.cod-pto-controle V-table-Win
ON F5 OF dc-rg-item.cod-pto-controle IN FRAME f-main /* C�d. Pto. Contr */
DO:
        {include/zoomvar.i &prog-zoom=eszoom/z01es117.w
                        &campo=dc-rg-item.cod-pto-controle
                        &campozoom=cod-pto-controle
                        &campo2=desc-contr
                        &campozoom2=desc-pto-controle}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-rg-item.cod-pto-controle V-table-Win
ON LEAVE OF dc-rg-item.cod-pto-controle IN FRAME f-main /* C�d. Pto. Contr */
DO:
     FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = INPUT FRAME {&FRAME-NAME} dc-rg-item.cod-pto-controle NO-LOCK NO-ERROR.

    IF AVAIL dc-pto-controle THEN
        ASSIGN desc-contr:SCREEN-VALUE IN FRAME {&FRAME-NAME} = dc-pto-controle.desc-pto-controle.
    ELSE
        ASSIGN desc-contr:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-rg-item.cod-pto-controle V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-rg-item.cod-pto-controle IN FRAME f-main /* C�d. Pto. Contr */
DO:
  APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME dc-rg-item.it-codigo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-rg-item.it-codigo V-table-Win
ON F5 OF dc-rg-item.it-codigo IN FRAME f-main /* Item */
DO:
    {include/zoomvar.i &prog-zoom=inzoom/z02in172.w
                        &campo=dc-rg-item.it-codigo
                        &campozoom=it-codigo
                        &campo2=desc-item
                        &campozoom2=desc-item}   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-rg-item.it-codigo V-table-Win
ON LEAVE OF dc-rg-item.it-codigo IN FRAME f-main /* Item */
DO:
    FIND ITEM
        WHERE ITEM.it-codigo = INPUT FRAME {&FRAME-NAME} dc-rg-item.it-codigo NO-LOCK NO-ERROR.

    IF AVAIL ITEM THEN
        ASSIGN desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.
    ELSE
        ASSIGN desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL dc-rg-item.it-codigo V-table-Win
ON MOUSE-SELECT-DBLCLICK OF dc-rg-item.it-codigo IN FRAME f-main /* Item */
DO:
  APPLY 'F5' TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         
  
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-find-using-key V-table-Win  adm/support/_key-fnd.p
PROCEDURE adm-find-using-key :
/*------------------------------------------------------------------------------
  Purpose:     Finds the current record using the contents of
               the 'Key-Name' and 'Key-Value' attributes.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR key-value AS CHAR NO-UNDO.
  DEF VAR row-avail-enabled AS LOGICAL NO-UNDO.

  /* LOCK status on the find depends on FIELDS-ENABLED. */
  RUN get-attribute ('FIELDS-ENABLED':U).
  row-avail-enabled = (RETURN-VALUE eq 'yes':U).
  /* Look up the current key-value. */
  RUN get-attribute ('Key-Value':U).
  key-value = RETURN-VALUE.

  /* Find the current record using the current Key-Name. */
  RUN get-attribute ('Key-Name':U).
  CASE RETURN-VALUE:
    WHEN 'rg-item':U THEN
       {src/adm/template/find-tbl.i
           &TABLE = dc-rg-item
           &WHERE = "WHERE dc-rg-item.rg-item eq key-value"
       }
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "dc-rg-item"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "dc-rg-item"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME f-main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

    /* Code placed here will execute PRIOR to standard behavior. */
    {include/i-valid.i}
    
    /*:T Ponha na pi-validate todas as valida��es */
    /*:T N�o gravar nada no registro antes do dispatch do assign-record e 
       nem na PI-validate. */

    RUN pi-validate.

/*     FIND LAST dc-rg-item NO-ERROR.           */
/*     IF AVAIL dc-rg-item THEN                 */
/*        ASSIGN i-seq = dc-rg-item.nr-seq + 1. */
/*     ELSE                                     */
/*        ASSIGN i-seq = 1.                     */

    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
    if RETURN-VALUE = 'ADM-ERROR':U then 
        return 'ADM-ERROR':U.
    
/*     ASSIGN dc-rg-item.nr-seq = i-seq. */
    /*:T Todos os assign�s n�o feitos pelo assign-record devem ser feitos aqui */  
    /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record V-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  FIND FIRST dc-movto-pto-controle
      WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item:SCREEN-VALUE IN FRAME {&FRAME-NAME}
        AND dc-movto-pto-controle.tipo-pto <> 7
      NO-LOCK NO-ERROR.
  IF AVAIL dc-movto-pto-controle THEN
  DO:
      RUN utp/ut-msgs.p (INPUT 'show',
                         INPUT 3,
                         INPUT 'Movimento RG').
      RETURN 'NOK':U.
  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  FOR EACH dc-movto-pto-controle
      WHERE dc-movto-pto-controle.rg-item = dc-rg-item.rg-item:SCREEN-VALUE IN FRAME {&FRAME-NAME}
      EXCLUSIVE-LOCK.
      DELETE dc-movto-pto-controle.
  END.
      


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    disable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

    FIND ITEM
        WHERE ITEM.it-codigo = INPUT FRAME {&FRAME-NAME} dc-rg-item.it-codigo NO-LOCK NO-ERROR.

    IF AVAIL ITEM THEN
        ASSIGN desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = ITEM.desc-item.
    ELSE
        ASSIGN desc-item:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".

   FIND dc-pto-controle
        WHERE dc-pto-controle.cod-pto-controle = INPUT FRAME {&FRAME-NAME} dc-rg-item.cod-pto-controle NO-LOCK NO-ERROR.

    IF AVAIL dc-pto-controle THEN
        ASSIGN desc-contr:SCREEN-VALUE IN FRAME {&FRAME-NAME} = dc-pto-controle.desc-pto-controle.
    ELSE
        ASSIGN desc-contr:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
    /* Code placed here will execute PRIOR to standard behavior. */
    
    /* Dispatch standard ADM method.                             */
    RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .
    
    /* Code placed here will execute AFTER standard behavior.    */
    &if  defined(ADM-MODIFY-FIELDS) &then
    if adm-new-record = yes then
        enable {&ADM-MODIFY-FIELDS} with frame {&frame-name}.
    &endif

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .
  dc-rg-item.it-codigo:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.
  dc-rg-item.cod-pto-controle:load-mouse-pointer ("image/lupa.cur") in frame {&frame-name}.
  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-atualiza-parent V-table-Win 
PROCEDURE pi-atualiza-parent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    define input parameter v-row-parent-externo as rowid no-undo.
    
    assign v-row-parent = v-row-parent-externo.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pi-validate V-table-Win 
PROCEDURE Pi-validate :
/*:T------------------------------------------------------------------------------
  Purpose:Validar a viewer     
  Parameters:  <none>
  Notes: N�o fazer assign aqui. Nesta procedure
  devem ser colocadas apenas valida��es, pois neste ponto do programa o registro 
  ainda n�o foi criado.       
------------------------------------------------------------------------------*/
    {include/i-vldfrm.i} /*:T Valida��o de dicion�rio */
    
/*:T    Segue um exemplo de valida��o de programa */
/*       find tabela where tabela.campo1 = c-variavel and               */
/*                         tabela.campo2 > i-variavel no-lock no-error. */
      
      /*:T Este include deve ser colocado sempre antes do ut-msgs.p */
/*       {include/i-vldprg.i}                                             */
/*       run utp/ut-msgs.p (input "show":U, input 7, input return-value). */
/*       return 'ADM-ERROR':U.                                            */

    IF INPUT FRAME {&FRAME-NAME} dc-rg-item.rg-item = "" THEN DO:
          run utp/ut-msgs.p (input "show":U, input 17006, "C�d. Rg. Item~~Campo 'Rg. Item' n�o pode ser igual a branco."). 
          return 'ADM-ERROR':U.
    END.

    IF adm-new-record THEN DO:
        FIND dc-rg-item
        WHERE dc-rg-item.rg-item = INPUT FRAME {&FRAME-NAME} dc-rg-item.rg-item NO-LOCK NO-ERROR.
    
        IF AVAIL dc-rg-item THEN DO:
              run utp/ut-msgs.p (input "show":U, input 7, "Rg. Item"). 
              return 'ADM-ERROR':U.
        END.
    END.

    FIND ITEM
    WHERE ITEM.it-codigo = INPUT FRAME {&FRAME-NAME} dc-rg-item.it-codigo NO-LOCK NO-ERROR.

    IF NOT AVAIL ITEM THEN DO:
          run utp/ut-msgs.p (input "show":U, input 2, "Item"). 
          return 'ADM-ERROR':U.
    END.

    FIND dc-pto-controle
    WHERE dc-pto-controle.cod-pto-controle = INPUT FRAME {&FRAME-NAME} dc-rg-item.cod-pto-controle NO-LOCK NO-ERROR.

    IF NOT AVAIL dc-pto-controle THEN DO:
          run utp/ut-msgs.p (input "show":U, input 2, "Ponto de Controle"). 
          return 'ADM-ERROR':U.
    END.
    ELSE DO:
/*         IF dc-pto-controle.tipo-pto <> 7 THEN DO:                                                                                          */
/*             run utp/ut-msgs.p (input "show":U, input 17006, "Tipo Ponto de Controle~~Tipo de ponto de controle � diferente de impress�o"). */
/*             return 'ADM-ERROR':U.                                                                                                          */
/*         END.                                                                                                                               */
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key V-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "rg-item" "dc-rg-item" "rg-item"}
  {src/adm/template/sndkycas.i "it-codigo" "dc-rg-item" "it-codigo"}
  {src/adm/template/sndkycas.i "cod-cor" "dc-rg-item" "cod-cor"}
  {src/adm/template/sndkycas.i "local-pto" "dc-rg-item" "local-pto"}
  {src/adm/template/sndkycas.i "cod-pto-controle" "dc-rg-item" "cod-pto-controle"}
  {src/adm/template/sndkycas.i "nr-rack" "dc-rg-item" "nr-rack"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "dc-rg-item"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/vstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

